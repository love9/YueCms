package com.markbro.base.utils.jsoup;


import com.markbro.base.utils.file.FileUtil;
import com.markbro.base.utils.string.StringUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * 解析爬取html
 * @author wujiyue
 */
public class CaptureHtmlUtil {
	protected final static Logger log = LoggerFactory
			.getLogger(CaptureHtmlUtil.class);
	private static final int timeout = 1500;

	public enum ImgSavePathRules {
		title(0), // 网页标题作为文件夹名
		id(1);// 网页id,用page对象getId()方法得到,作为文件夹名
		private int _index;

		private ImgSavePathRules(int i) {
			this._index = i;
		}

		public int get_Index() {
			return this._index;
		}
	}

	public enum SubTitleRules {
		none(0), // 不截取
		width(1), // 截取一定宽度的标题
		strBefore(2);// 截取指定字符串以前的标题（包含）
		private int _index;

		private SubTitleRules(int i) {
			this._index = i;
		}

		public int get_Index() {
			return this._index;
		}
	}
	public static Map getCookies(URLConnection con) {
		Map cookie = new HashMap();
		String headerName = null;
		for (int i = 1; (headerName = con.getHeaderFieldKey(i)) != null; i++) {
		//	System.out.println(headerName + ":" + con.getHeaderField(i));
			if (headerName.equalsIgnoreCase("Set-Cookie")) {
				StringTokenizer st = new StringTokenizer(con.getHeaderField(i), ";");
				if (st.hasMoreElements()) {
					String token = st.nextToken();
					String name = token.substring(0, token.indexOf('='));
					String value = token.substring(token.indexOf('=') + 1);
					cookie.put(name, value);
				}
			}
		}
		//System.out.println(cookie.toString());
		//return cookie.toString();
		return cookie;
	}
	/**
	 * @param url
	 *            要抓取的url地址
	 * @throws IOException
	 * @return String 网页源代码
	 */
	public static String captureHtml(String url) throws IOException {
		// 创建URL
		URL urlObj = new URL(url);
		// 到开URL链接
		HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
		conn.setRequestProperty("User-Agent", "java");// 伪装浏览器
		// 输入流
		InputStreamReader in = new InputStreamReader(conn.getInputStream(),"UTF-8");
		// 放入缓冲
		BufferedReader bfr = new BufferedReader(in);
		String line = null;
		// 存储网页源代码
		StringBuffer sb = new StringBuffer();
		while ((line = bfr.readLine()) != null) {
			sb.append(line).append("\n");
		}
		return sb.toString();
	}



	/**
	 * 在指定ID的dom获得tag元素集合
	 * 
	 * @param dom
	 * @param tag
	 * @param id
	 * @return
	 */
	public static Elements getElementsByTagSelectID(Document dom, String tag,
			String id) {
		Elements es = dom.getElementById(id).select(tag);
		return es;
	}

	/**
	 * 在指定class的dom获得tag元素集合
	 * 
	 * @param dom
	 * @param tag
	 * @param cl
	 * @return
	 */
	public static Elements getElementsByTagSelectClass(Document dom,
			String tag, String cl) {
		Elements es = dom.getElementsByClass(cl).select(tag);
		return es;
	}

	/**
	 * 获取Elements的html文本内容
	 * @param es
	 * @return
	 */
	public static List<String> getElementsHtml(Elements es) {
		List<String> list = null;
		if (es != null && es.size() > 0) {
			list = new ArrayList<String>();
			String temp = "";
			for (Element e : es) {
				temp = e.html();
				list.add(temp);
			}
		}
		return list;
	}

	/**
	 * 获得Elements中属性为attrName的集合列表
	 * 
	 * @param es
	 * @param attrName
	 * @return List<String>
	 */
	public static List<String> getAttributeList(Elements es, String attrName) {
		List<String> list = new ArrayList<String>();
		for (Element e : es) {
			String temp = "";
			// 判断如果属性名是href或者src
			if ("href".equals(attrName) || "src".equals(attrName)) {
				// 因为要获取他们绝对路径
				temp = e.attr("abs:" + attrName);
			} else {
				//不是href或者src
				temp = e.attr(attrName);
			}
			if (!"".equals(temp))
				list.add(temp);
		}
		return list;
	}

	/**
	 * 将img标签信息封装到ImageBean对象
	 * 
	 * @param es
	 * @return List<Image>
	 */
	public static List<ImageBean> getImageList(Elements es) {
		List<ImageBean> list = null;
		if (es != null && es.size() > 0) {
			list = new ArrayList<ImageBean>();
			for (Element e : es) {
				String src = e.attr("abs:src");
				if (!"".equals(src)) {
					String alt = e.attr("alt");
					String w = e.attr("width");
					String h = e.attr("height");
					ImageBean t = new ImageBean();
					t.setUrlAddress(src);
					t.setAlt(alt);
					t.setWidth(w);
					t.setHeight(h);
					list.add(t);
				}
			}
		}
		return list;
	}

	/**
	 * 获得Dom对象
	 * 
	 * @param htmlContent
	 * @return Document
	 */
	public static Document getDom(String htmlContent) {
		Document dom = Jsoup.parse(htmlContent);
		return dom;
	}

	/**
	 * 通过Jsoup获得Dom
	 * 
	 * @param url
	 * @return Document
	 */
	public static Document getDomByURL(String url) {
		if (StringUtil.isEmpty(url)) {
			log.error("参数{}{}", "url", "不能为空!");
		}
		Document dom = null;
		try {
			dom = Jsoup.connect(url).header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0").get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dom;
	}

	/**
	 * 获得a标签href集合
	 * 
	 * @param dom
	 * @return List<String>
	 */
	public static List<String> get_A_listByDom(Document dom) {
		List<String> list = new ArrayList<String>();
		// Elements es=dom.select("a");
		Elements es = dom.getElementsByTag("a");
		list = getAttributeList(es, "href");
		return list;
	}

	/**
	 * 获得该URL下所有链接
	 * 
	 * @param url
	 * @return List<String>
	 */
	public static List<String> get_A_listByURL(String url) {
		List<String> list = null;
		try {
			Document dom = getDomByURL(url);
			list = get_A_listByDom(dom);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 获得url下指定ID元素下的a标签href集合
	 * 
	 * @param url
	 * @return List<String>
	 */
	public static List<String> get_A_listByURLSelectID(String url, String id) {
		Document dom = getDomByURL(url);
		List<String> list = get_A_listByDomSelectID(dom, id);
		return list;
	}

	/**
	 * 获得url下指定class元素下的a标签href集合
	 * 
	 * @param url
	 * @param cl
	 * @return List<String>
	 */
	public static List<String> get_A_listByURLSelectClass(String url, String cl) {
		Document dom = getDomByURL(url);
		List<String> list = get_A_listByDomSelectClass(dom, cl);
		return list;
	}

	/**
	 * 获得title
	 * 
	 * @param dom
	 * @return String
	 */
	public static String getTitleByDom(Document dom) {
		return dom.title();
	}

	/**
	 * 获得网页关键字
	 * 
	 * @param dom
	 * @return String
	 */
	public static String getKeywordsByDom(Document dom) {
		Element e = dom.getElementsByAttributeValue("name", "keywords").first();
		String s = e.attr("content");
		return s;
	}

	/**
	 * 获得网页description
	 * 
	 * @param dom
	 * @return
	 */
	public static String getDescriptionByDom(Document dom) {
		Element e = dom.getElementsByAttributeValue("name", "description")
				.first();
		String s = e.attr("content");
		return s;
	}

	/**
	 * 扫描url目录直到得到该url下所有url,该url只是个目录 而不是具体某个网页
	 * 
	 * @param url
	 * @return
	 */
	public static List<String> scanURL(String url) {
		if (!"".equals(url) && !url.endsWith("/")) {
			url += "/";
		}
		List<String> list = new ArrayList<String>();
		list = get_A_listByURL(url);
		for (String s : list) {
			try {
				if (s.indexOf(".htm") < 0) {// 路径不含有.htm,认为是目录
					List<String> temp = scanURL(s);
					list.addAll(temp);
				}
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}
		return list;
	}

	/**
	 * 获取Dom中img标签src属性集合
	 * 
	 * @param dom
	 * @return List<String>
	 */
	public static List<String> getImgListByDom(Document dom) {
		List<String> list = new ArrayList<String>();
		Elements es = dom.select("img");
		list = getAttributeList(es, "src");
		return list;
	}

	/**
	 * 获取dom中img标签信息集合
	 * 
	 * @param dom
	 * @return List<ImageBean>
	 */
	public static List<ImageBean> getImageListByDom(Document dom) {

		List<ImageBean> list = new ArrayList<ImageBean>();
		Elements es = dom.select("img");
		list = getImageList(es);
		return list;
	}

	/**
	 * 获得指定ID元素的Dom中img标签的src属性集合
	 * 
	 * @param dom
	 * @param id
	 * @return List<String>
	 */
	public static List<String> getImgListByDomSelectID(Document dom, String id) {
		List<String> list = new ArrayList<String>();
		Elements es = getElementsByTagSelectID(dom, "img", id);
		list = getAttributeList(es, "src");
		return list;
	}

	/**
	 * 获取指定ID元素的Dom中img标签的信息集合
	 * 
	 * @param dom
	 * @param id
	 * @return List<Image>
	 */
	public static List<ImageBean> getImageListByDomSelectID(Document dom, String id) {
		List<ImageBean> list = new ArrayList<ImageBean>();
		Elements es = getElementsByTagSelectID(dom, "img", id);
		list = getImageList(es);
		return list;
	}

	/**
	 * 获得指定class元素的Dom中img标签的src属性集合
	 * 
	 * @param dom
	 * @param cl
	 * @return List<String>
	 */
	public static List<String> getImgListByDomSelectClass(Document dom,
			String cl) {
		List<String> list = new ArrayList<String>();
		Elements es = getElementsByTagSelectClass(dom, "img", cl);
		list = getAttributeList(es, "src");
		return list;
	}

	/**
	 * 获得指定class元素的Dom中img标签的信息集合
	 * 
	 * @param dom
	 * @param cl
	 * @return List<Image>
	 */
	public static List<ImageBean> getImageListByDomSelectClass(Document dom,
			String cl) {
		List<ImageBean> list = null;
		try {
			list = new ArrayList<ImageBean>();
			Elements es = getElementsByTagSelectClass(dom, "img", cl);
			list = getImageList(es);
		} catch (Exception e) {
			log.error("根据dom获取指定class元素中图片信息list错误!");
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 获得指定ID元素的Dom中的a标签href属性集合
	 * 
	 * @param dom
	 * @param id
	 * @return List<String>
	 */
	public static List<String> get_A_listByDomSelectID(Document dom, String id) {
		List<String> list = null;
		try {
			list = new ArrayList<String>();
			Elements es = getElementsByTagSelectID(dom, "a", id);
			list = getAttributeList(es, "href");
		} catch (Exception e) {
			log.error("根据dom获取指定id元素中a标签href属性list错误!");
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 获得指定class元素的Dom中的a标签href属性集合
	 * 
	 * @param dom
	 * @param cl
	 * @return List<String>
	 */
	public static List<String> get_A_listByDomSelectClass(Document dom,
			String cl) {
		List<String> list = null;
		try {
			list = new ArrayList<String>();
			Elements es = getElementsByTagSelectClass(dom, "a", cl);
			list = getAttributeList(es, "href");
		} catch (Exception e) {
			log.error("根据dom获取指定class元素中a标签href属性list错误!");
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 根据url获得所有图片地址集合
	 * 
	 * @param url
	 * @return List<String>
	 */
	public static List<String> getImgListByURL(String url) {
		List<String> list = null;
		try {
			Document dom = getDomByURL(url);
			list = new ArrayList<String>();
			Elements es = dom.select("img");
			list = getAttributeList(es, "src");
		} catch (Exception e) {
			log.error("根据url获取图片地址list错误!");
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 根据url获得所有图片信息集合
	 * 
	 * @param url
	 * @return List<ImageBean>
	 */
	public static List<ImageBean> getImageListByURL(String url) {
		List<ImageBean> list = null;
		try {
			Document dom = getDomByURL(url);
			list = new ArrayList<ImageBean>();
			Elements es = dom.select("img");
			list = getImageList(es);
		} catch (Exception e) {
			log.error("根据url获取图片信息list错误!");
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 根据图片url地址下载图片保存到本地
	 * 
	 * @param picURL
	 *            图片url地址
	 * @param filePath
	 *            本地保存图片路径
	 * @return boolean
	 */
	public static boolean download(String picURL, String filePath) {
		boolean result = false;
		// 输入流
		InputStream is = null;
		// 输出的文件流
		OutputStream os = null;
		try {
			// 构造URL
			URL url = new URL(picURL);
			// 打开连接
			URLConnection con = url.openConnection();
			// 设置连接超时时间
			con.setConnectTimeout(timeout);

			is = con.getInputStream();
			// 1K的数据缓冲
			byte[] bs = new byte[1024];
			// 读取到的数据长度
			int len;
			os = new FileOutputStream(filePath);
			// 开始读取
			while ((len = is.read(bs)) != -1) {
				os.write(bs, 0, len);
			}
			result = true;
			System.out.println("图片下载完毕！");
		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			// 完毕，关闭所有链接
			try {
				if (os != null)
					os.close();
				if (is != null)
					is.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 传入一个图片地址集合和图片保存路径下载所有图片到该路径下
	 * 
	 * @param imgs
	 *            图片(绝对)地址集合
	 * @param saveDirPath
	 *            保存目录路径
	 */
	public static void downloadImgs(List<String> imgs, String saveDirPath) {
		int count = imgs.size();
		File f = new File(saveDirPath);
		if (!f.exists()) {
			FileUtil.createDir(saveDirPath);
		}
		for (int i = 0; i < count; i++) {
			String temp = imgs.get(i);
			if (!"".equals(temp)) {// 对每一个图片地址进行检查
				String imgType = temp.substring(temp.lastIndexOf(".") + 1,
						temp.length());
				// System.out.println(imgType);
				System.out.println("下载进度：" + String.valueOf(i + 1) + "/"
						+ count);
				// 图片名称默认为从1到n
				download(temp, saveDirPath + "\\" + String.valueOf(i + 1) + "."
						+ imgType);
			}
		}
	}


	// 方法测试入口
	public static void main(String[] args) throws Exception {
		// List<String> urls=FileUtil.readTxtFile(new File("d:\\urls.txt"));
		// captureHtmlImageBatch(urls, "d:\\imgs2", 5);
		//List<String> urls = scanURL("http://tech.qq.com/a");
		// List<String> urls=get_A_listByURL("http://tech.qq.com/a/");
		//List<String> urls = get_A_listByURL("http://tech.qq.com/a/");
	List<ImageBean> ls=	getImageListByURL("http://tech.qq.com/a/");
		ls.stream().filter(b -> {
			return b.getUrlAddress().toLowerCase().endsWith(".jpg");
		}).forEach(b -> {
			System.out.println(b.toString());
		});

	}

}
