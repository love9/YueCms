package com.markbro.base.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	 private static final SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	 private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
	 private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");  
	 /**
	  * 获得当前时间对象
	  * @return java.util.Date
	  */
	 public static Date now(){
		 return new Date();
	 }
	 /**
	  * 获得当前时间戳
	  * @return long
	  */
	 public static long getMillis() {  
	        return System.currentTimeMillis();  
	 }
	 /**
	  * 获得当前时间的时间戳字符串
	  * 获得long时间戳3种方法:
	  * 1.System.currentTimeMillis();
	  * 2.new Date().getTime(); 
	  * 3.Calendar.getInstance().getTimeInMillis();
	  * 其中2比1慢1倍,3比1慢几十倍!
	  * @return String
	  */
	 public static String getTimestamp(){
		 return String.valueOf(getMillis());
	 }
	 /**
	  * long时间戳毫秒数转换成日期
	  * @param millis
	  * @return Date
	  */
	 public static Date millis2Date(long millis){
		 return new Date(millis);
	 }
	 /**
	  * 时间戳字符串转换成日期
	  * @param timestamp
	  * @return Date
	  */
	 public static Date timestamp2Date(String timestamp){
		 return new Date(Long.parseLong(timestamp));
	 }
	 /**
	  * 获得日期时间字符串
	  * 默认日期时间格式"yyyy-MM-dd HH:mm:ss"
	  * @return String
	  */
	 public static String getDatetime() {  
	        return datetimeFormat.format(now());  
	 }  
	 /**
	  * 格式化日期时间对象
	  * 默认日期时间格式"yyyy-MM-dd HH:mm:ss"
	  * @param date
	  * @return String
	  */
	 public static String formatDatetime(Date date) {  
	        return datetimeFormat.format(date);  
	 }
	 public static String formatDatetime(Long longDate) {
		Date date=new Date(longDate);
		return datetimeFormat.format(date);
	 }
	 /**
	  * 按照自定义模式格式化日期时间对象
	  * @param date
	  * @param pattern
	  * @return String
	  */
	 public static String formatDatetime(Date date, String pattern) {  
		  SimpleDateFormat customFormat = (SimpleDateFormat) datetimeFormat.clone();  
		  customFormat.applyPattern(pattern);  
		  return customFormat.format(date);  
	 }
	public static String formatDatetime(String date, String pattern) {
		SimpleDateFormat customFormat = (SimpleDateFormat) datetimeFormat.clone();
		customFormat.applyPattern(pattern);
		return customFormat.format(date);
	}
	public static String formatDatetime(Long longDate, String pattern) {
		SimpleDateFormat customFormat = (SimpleDateFormat) datetimeFormat.clone();
		customFormat.applyPattern(pattern);
		Date date=new Date(longDate);
		return customFormat.format(date);
	}
	 /**
	  * 获得当前日期字符串
	  * 默认日期格式"yyyy-MM-dd"
	  * @return
	  */
	 public static String getDate() {  
	      return dateFormat.format(now());  
	 }
	 /**
	  * 格式化日期对象
	  * 默认日期格式"yyyy-MM-dd"
	  * @param date
	  * @return
	  */
	 public static String formatDate(Date date){
		 return dateFormat.format(date);  
	 }
	 /**
	  * 自定义模式格式化日期对象
	  * @param date
	  * @param pattern
	  * @return String
	  */
	 public static String formatDate(Date date,String pattern){
		 SimpleDateFormat customFormat =new SimpleDateFormat(pattern);
		 return customFormat.format(date);
	 }
	 /**
	  * 获得当前时间字符串
	  * 默认时间格式"HH:mm:ss"
	  * @return String
	  */
	 public static String getTime(){
		 return timeFormat.format(now());
	 }
	 /**
	  * 格式化时间对象
	  * 默认时间格式"HH:mm:ss"
	  * @param date
	  * @return String
	  */
	 public static String formatTime(Date date){
		 return timeFormat.format(date);
	 }
	 /**
	  * 自定义模式格式化时间
	  * @param date
	  * @param pattern
	  * @return String
	  */
	 public static String formatTime(Date date,String pattern){
		 SimpleDateFormat customFormat = (SimpleDateFormat) timeFormat.clone();  
		 customFormat.applyPattern(pattern);  
		 return timeFormat.format(date);
	 }
	 /**
	  * dateTime字符串解析成java.util.Date类型
	  * @param dateTime
	  * @return Date
	  */
	 public static Date parseDatetime(String dateTime){  
		 Date date=null;
		 try {
			date= datetimeFormat.parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		 return date;
	 }
	 /**
	  * dateStr字符串解析成java.util.Date类型
	  * @param dateStr
	  * @return Date
	  */
	 public static Date parseDate(String dateStr){  
		 Date date=null;
		try {
			date= dateFormat.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		return date;
	 }
	 /**
	  * 将time字符串解析成java.util.Date类型
	  * @param time
	  * @return Date
	  */
	 public static Date parseTime(String time){  
		 Date date=null;
			try {
				date=timeFormat.parse(time); 
			} catch (ParseException e) {
				e.printStackTrace();
			} 
			return date;
	 }
	 /**
	  * 将日期时间字符串解析成java.util.Date类型
	  * @param dateTime
	  * @param pattern
	  * @return Date
	  */
	 public static Date parseDatetime(String dateTime, String pattern){  
		 SimpleDateFormat format = (SimpleDateFormat) datetimeFormat.clone();  
		 format.applyPattern(pattern);
		 Date date=null;
		 try {
			date= format.parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}  
		 return date;
	}  
	
	 /**
	  * 获得日历对象
	  * @return Calendar
	  */
	 public static Calendar getCalendar() {
		 Calendar cal = GregorianCalendar.getInstance(); 
		 return cal;  
	 }
	 /**
	  * 获得当前月份
	  * @return int
	  */
	 public static int getMonth() {  
		 return getCalendar().get(Calendar.MONTH) + 1;  
	 }
	 /**
	  * 获得今天是当前月份的第几天
	  * @return int
	  */
	 private static int dayOfMonth() {  
		return getCalendar().get(Calendar.DAY_OF_MONTH);  
	 }
	 /**
	  * 获得今天是星期的第几天
	  * 1-7 对应 日-六
	  * @return int
	  */
	 private static int dayOfWeek() {  
		 
	    return getCalendar().get(Calendar.DAY_OF_WEEK);  
	 } 
	 /**
	  * 获得今天是今年的第几天
	  * @return int
	  */
	 private static int dayOfYear() {  
		 return getCalendar().get(Calendar.DAY_OF_YEAR);  
	 }
	 /**
	  * 判断源日期是否在目标日期之前
	  * @param src
	  * @param dst
	  * @return boolean
	  */
	 public static boolean isBefore(Date src, Date dst) {
		 return src.before(dst);
	 }
	 /**
	  * 判断源日期是否在目标日期之后
	  * @param src
	  * @param dst
	  * @return boolean
	  */
	 public static boolean isAfter(Date src, Date dst) {
		  return src.after(dst);
	 }
	 /**
	  * 判断2个日期是否相等
	  * @param date1
	  * @param date2
	  * @return boolean
	  */
	 public static boolean isEqual(Date date1, Date date2) {  
		 return date1.compareTo(date2) == 0;  
	 }  
	 /**
	  * 判断某个日期是否在一个时间范围内
	  * @param beginDate
	  * @param endDate
	  * @param src
	  * @return boolean
	  */
	 public static boolean between(Date beginDate, Date endDate, Date src) {  
		 return beginDate.before(src) && endDate.after(src);  
	 }

	/**
	 * 通过时间秒毫秒数判断两个时间的间隔
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int differentDaysByMillisecond(Date date1,Date date2)
	{
		int days = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
		return days;
	}
	 /**
	  * 获得当前月的最后一天
	  * 比如现在时间是2014年8月12日 15:19:54,该方法得到的时间是2014年08月30日 23时59分59秒999毫秒
	  * @return Date
	  */
	 public static Date lastDayOfMonth() {  
		 Calendar cal = getCalendar();  
		 cal.set(Calendar.DAY_OF_MONTH, 0); // M月置零  
		 cal.set(Calendar.HOUR_OF_DAY, 0);// H置零  
		 cal.set(Calendar.MINUTE, 0);// m置零  
		 cal.set(Calendar.SECOND, 0);// s置零  
		 cal.set(Calendar.MILLISECOND, 0);// S置零  
		 cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);// 月份+1  
		 cal.set(Calendar.MILLISECOND, -1);// 毫秒-1  
		 return cal.getTime();  
	 }
	 /**
	  * 获得当前月的第一天
	  * HH:mm:ss SS为零 
	  * @return Date
	  */
	 public static Date firstDayOfMonth() {  
		 Calendar cal = getCalendar();  
		 cal.set(Calendar.DAY_OF_MONTH, 1); // M月置1  
		 cal.set(Calendar.HOUR_OF_DAY, 0);// H置零  
		 cal.set(Calendar.MINUTE, 0);// m置零  
		 cal.set(Calendar.SECOND, 0);// s置零  
		 cal.set(Calendar.MILLISECOND, 0);// S置零  
		 return cal.getTime();  
	 }
	 /**
	  * 返回本周开始时间.注:周一认为是第一天.
	  * @return
	  */
	 public static Date firstDayOfWeek() {  
		 Calendar cal = getCalendar();  
		 cal.set(Calendar.DAY_OF_WEEK, 2); // W周置2.周日对应1,周一对应2. 
		 cal.set(Calendar.HOUR_OF_DAY, 0);// H置零  
		 cal.set(Calendar.MINUTE, 0);// m置零  
		 cal.set(Calendar.SECOND, 0);// s置零  
		 cal.set(Calendar.MILLISECOND, 0);// S置零  
		 return cal.getTime();  
	 }
	 /**
	  * 返回本周结束时间.注:周日认为是最后一天.
	  * @return
	  */
	 public static Date lastDayOfWeek() {  
		 Calendar cal = getCalendar();  
		 cal.set(Calendar.DAY_OF_WEEK, 2); // W置2.因为2对应(下)周一,这里把周日看作一周最后一天.  
		 cal.set(Calendar.HOUR_OF_DAY, 0);// H置零  
		 cal.set(Calendar.MINUTE, 0);// m置零  
		 cal.set(Calendar.SECOND, 0);// s置零  
		 cal.set(Calendar.MILLISECOND, 0);// S置零  
		 cal.set(Calendar.WEEK_OF_MONTH, cal.get(Calendar.WEEK_OF_MONTH) + 1);// 周+1  
		 cal.set(Calendar.MILLISECOND, -1);// 毫秒-1  
		 return cal.getTime();  
	 }
	 /**
	  * 获得与当前时间相隔amount天的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addDate(int amount){
		 Calendar c=getCalendar();
		 c.add(Calendar.DATE, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与某个时间相隔amount天的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addDate(Date date,int amount){
		 Calendar c=getCalendar();
		 c.setTime(date);
		 c.add(Calendar.DATE, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与当前时间相隔amount月的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addMonth(int amount){
		 Calendar c=getCalendar();
		 c.add(Calendar.MONTH, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与某个时间相隔amount月的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addMonth(Date date,int amount){
		 Calendar c=getCalendar();
		 c.setTime(date);
		 c.add(Calendar.MONTH, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与当前时间相隔amount年的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addYear(int amount){
		 Calendar c=getCalendar();
		 c.add(Calendar.YEAR, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与某个时间相隔amount年的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addYear(Date date,int amount){
		 Calendar c=getCalendar();
		 c.setTime(date);
		 c.add(Calendar.YEAR, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与当前时间相隔amount时的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addHour(int amount){
		 Calendar c=getCalendar();
		 c.add(Calendar.HOUR, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与某个时间相隔amounts时的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addHour(Date date,int amount){
		 Calendar c=getCalendar();
		 c.setTime(date);
		 c.add(Calendar.HOUR, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与当前时间相隔amount分的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addMinute(int amount){
		 Calendar c=getCalendar();
		 c.add(Calendar.MINUTE, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与某个时间相隔amount分的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addMinute(Date date,int amount){
		 Calendar c=getCalendar();
		 c.setTime(date);
		 c.add(Calendar.MINUTE, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与当前时间相隔amount秒的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addSecond(int amount){
		 Calendar c=getCalendar();
		 c.add(Calendar.SECOND, amount);
		 return c.getTime();
	 }
	 /**
	  * 获得与某个时间相隔amount秒的日期
	  * @param amount
	  * @return Date
	  */
	 public static Date addSecond(Date date,int amount){
		 Calendar c=getCalendar();
		 c.setTime(date);
		 c.add(Calendar.SECOND, amount);
		 return c.getTime();
	 }
	 
	public static int getDaysOfMonth(int year,int month){
		Calendar time=Calendar.getInstance();
		time.clear();
		time.set(Calendar.YEAR,year);
		time.set(Calendar.MONTH,month-1);//Calendar对象默认一月为0,month月
		int day=time.getActualMaximum(Calendar.DAY_OF_MONTH);//本月份的天数
		return day;
	}
	 public static String simpleformatIntervalTime(Date start,Date end){
		IntervalDate idate=new IntervalDate(start, end);
		String flag=null;
		if(idate.getFlag()){
			flag="后";
		}else{
			flag="前";
		}
		int dayCount=idate.getIntervalDays();
		if(dayCount>0){//大于1天
			if(dayCount<30){
				return dayCount+"天"+flag;
			}
			else if(dayCount<365){
				return (dayCount/30)+"月"+flag;
			}else{
				return (dayCount/365)+"年"+flag;
			}
		}else{//小于1天
			if(idate.getIntervalHours()>0){//大于1小时，小于1天
				return idate.getIntervalHours()+"小时"+flag;
			}else{//小于1小时
				if(idate.getIntervalMinutes()>0){//-----小于1小时大于1分钟
					return idate.getIntervalMinutes()+"分钟"+flag;
				}else{//0天0小时0分钟.---小于1分钟.
					return "1秒"+flag;
				}
			}
		}
		 
	 }
	 public static String simpleformatIntervalTime(String start,String end){
		 Date d1=parseDatetime(start);
		 Date d2=parseDatetime(end);
		 return simpleformatIntervalTime(d1, d2);
	 }
	 /**
	  * 获得一个日期与当前时间的差距
	  * @param date
	  * @return
	  */
	 public static String simpleformatIntervalTime(Date date){
		 return simpleformatIntervalTime(now(),date);
	 }
	 public static String simpleformatIntervalTime(String str){
		 Date d=parseDatetime(str);
		 return simpleformatIntervalTime(now(), d);
	 }
	//根据时间字符串09:00设置时间返回
	public static Date getDateByTime(String timeStr){
		String[] arr=timeStr.split(":");
		String hourStr=arr[0].startsWith("0")?arr[0].substring(1):arr[0];
		String minuStr=arr[1].startsWith("0")?arr[1].substring(1):arr[1];
		int hour=Integer.valueOf(hourStr);
		int minu=Integer.valueOf(minuStr);
		Calendar cal=Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), hour, minu, 0);
		return cal.getTime();
	}
	 public static void main(String[] args) {
		//String t=getTimestamp();
		Date t=addSecond(now(),60*60*24*356);

		IntervalDate idate=new IntervalDate(now(), t);
		

		//String dstr=formatDatetime(d, "yyyy年MM月dd日 HH时mm分ss秒SS毫秒");
		//System.out.println(t);
		//System.out.println(dstr);
		System.out.println(simpleformatIntervalTime(now(),t));
		 //System.out.println(t);
		//System.out.println(formatDatetime(t, "yyyy年MM月dd日 HH时mm分ss秒SS毫秒"));
	}

}
