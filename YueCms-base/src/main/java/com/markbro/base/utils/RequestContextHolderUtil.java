package com.markbro.base.utils;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author wujiyue
 */
public class RequestContextHolderUtil {
    public static HttpServletRequest getRequest() {
        return getRequestAttributes().getRequest();
    }
    public static HttpServletResponse getResponse() {
        return getRequestAttributes().getResponse();
    }
    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    public static ServletRequestAttributes getRequestAttributes() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
    }

    public static ServletContext getServletContext() {
        return ContextLoader.getCurrentWebApplicationContext().getServletContext();
    }

    /**
     * 获取session的Attribute
     *
     * @param name
     *         session的key
     * @return Object
     */
    public static Object getSession(String name) {
        return getRequestAttributes().getAttribute(name, RequestAttributes.SCOPE_SESSION);
    }

    /**
     * 添加session
     * @param name
     * @param value
     */
    public static void setSession(String name, Object value) {
        getRequestAttributes().setAttribute(name, value, RequestAttributes.SCOPE_SESSION);
    }

    /**
     * 清除指定session
     * @param name
     * @return void
     */
    public static void removeSession(String name) {
        getRequestAttributes().removeAttribute(name, RequestAttributes.SCOPE_SESSION);
    }

    /**
     * 获取所有session key
     * @return String[]
     */
    public static String[] getSessionKeys() {
        return getRequestAttributes().getAttributeNames(RequestAttributes.SCOPE_SESSION);
    }
}
