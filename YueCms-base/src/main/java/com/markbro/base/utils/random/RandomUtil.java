package com.markbro.base.utils.random;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 获得跟随机数有关的工具类
 * @author wujiyue
 * @date 2014年9月4日 21:37:59
 * @version 1.0
 */
public class RandomUtil {
	private static Random r=new Random();
	private static char[] default_allowedCodes={'1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k'
	,'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'
	,'P','Q','R','S','T','U','V','W','X','Y','Z'};
	/**
	 * 获得n个随机字符(可能重复)
	 * @param n 个数
	 * @param allowedCodes 允许出现的字符
	 * @return char[]
	 */
	public static char[] getRanCodes(int n,char[] allowedCodes){
		if(allowedCodes==null){
			allowedCodes=default_allowedCodes;
		}
		char[] chs=new char[n];
		int len=allowedCodes.length;
		for(int i=0;i<n;i++){
			int t=r.nextInt(len);
			chs[i]=allowedCodes[t];
		}
		return chs;
	}
	/**
	 * 获得n个随机字符(可能重复)
	 * @param n
	 * @return
	 */
	public static char[] getRanCodes(int n){
		return getRanCodes(n,default_allowedCodes);
	}
	/**
	 * 获得一个不重复的随机字符序列
	 * @param n
	 * @param allowedCodes
	 * @return
	 */
	public static char[] getRanCodesSeq(int n,char[] allowedCodes){
		if(allowedCodes==null){
			allowedCodes=default_allowedCodes;
		}
		char[] chs=new char[n];
		int len=allowedCodes.length;
		int[] temp=getRanNumsSeq(0, len-1);
		for(int i=0;i<n;i++){
			int t=temp[i];
			chs[i]=allowedCodes[t];
		}
		return chs;
	}
	/**
	 * 获得一个不重复的随机字符序列
	 * @param n
	 * @return
	 */
	public static char[] getRanCodesSeq(int n){
		return getRanCodesSeq(n, default_allowedCodes);
	}
	/**
	 * 获得一个n-m的随机数
	 * @param n 随机数下限
	 * @param m 随机数上限
	 * @return
	 */
	public static int getARanNum(int n,int m){
		if(n>m){
			try {
				throw new Exception("随机数下限必须小于上限！");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return r.nextInt(m-n+1)+n;
	}
	/**
	 * 获得一个1-m的随机数
	 * @param m 随机数上限
	 * @return
	 */
	public static int getARanNum(int m){
		return r.nextInt(m)+1;
	}
	/**
	 * 获得k个0-9的随机数组成的字符串
	 * @param k
	 * @return
	 */
	public static String getNumsString(int k){
		StringBuilder sb=new StringBuilder();		
		for(int i=0;i<k;i++){
			sb.append(getARanNum(0, 9));			
		}
		return sb.toString();
	}
	/**
	 * 获得k个(n-m)的随机数(可能重复)
	 * @param k
	 * @param n
	 * @param m
	 * @return
	 */
	public static int[] getRanNums(int k,int n,int m){
		//long start=System.currentTimeMillis();
		int[] arr=new int[k];
		for(int i=0;i<k;i++){
			arr[i]=getARanNum(n, m);
		}
		//long end=System.currentTimeMillis();
		//System.out.println("共耗时："+(end-start)+"毫秒");
		return arr;
	}
	/**
	 * 获得一个随机不重复的(n-m)随机数序列(包含n和m)
	 * @param n 下限
	 * @param m 上限
	 * @return
	 */
	public static int[] getRanNumsSeq(int n,int m){
		if(n>m){
			try {
				throw new Exception("随机数下限必须小于上限！");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		//long start=System.currentTimeMillis();
		List<Integer> list=new ArrayList<Integer>();
		for(int i=n;i<=m;i++){
			list.add(i);
		}
		//long end1=System.currentTimeMillis();
		//System.out.println("共耗时："+(end1-start)+"毫秒");
		int len=m-n+1;
		int[] arr=new int[len];
		for(int i=0;i<len;i++){
			int tindex=r.nextInt(len-i);
			arr[i]=list.get(tindex);
			list.remove(tindex);
		}
		//long end=System.currentTimeMillis();
		//System.out.println("共耗时："+(end-start)+"毫秒");
		return arr;
	}
	/**
	 * 获得一个(1-m)随机数序列
	 * @param m
	 * @return
	 */
	public static int[] getRanNumsSeq(int m){
		return getRanNumsSeq(1,m);
	}
	/**
	 * 模拟抛硬币,得到正面向上的情况是否发生
	 * @return
	 */
	public static boolean isUp(){
		int i=getARanNum(2);
		if(i==1)
			return true;
		else
		return false;
	}
	/**
	 * 获得随机颜色
	 * @param min
	 * @param max
	 * @return
	 */
	public static Color getRandColor(int min,int max){
		if(min>255)
			min=255;
		if(max>255)
			max=255;
		int R=getARanNum(min, max);
		int G=getARanNum(min, max);
		int B=getARanNum(min, max);
		return new Color(R, G, B);
	}
	public static void main(String[] args) {
		
		System.out.println(getARanNum(0, 9));
	}

}
