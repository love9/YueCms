package com.markbro.base.utils.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiyue
 * 这个是列表树形式显示的实体,
 * 树节点信息
 */
public class TreeNode {
	private Integer id;
	private Integer parentId;
	private String name;
	private String code;
	private String url;
	private Integer level;
	private String description;
	private List<TreeNode> children = new ArrayList<TreeNode>();

	@Override
	public String toString() {
		return "TreeNode{" +
				"id=" + id +
				", parentId=" + parentId +
				", name='" + name + '\'' +
				", code='" + code + '\'' +
				", url='" + url + '\'' +
				", level=" + level +
				", description='" + description + '\'' +
				", children=" + children +
				'}';
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
}
