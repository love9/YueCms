package com.markbro.base.utils;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 使用该分页控件控制返回的数据格式为：total:总记录数，rows:记录集合。符合easyUI的datagrid希望的格式
 * @author wujiyue
 */
public class MyPageListJsonSerializer extends JsonSerializer<PageList> {
    ObjectMapper mapper;

    public MyPageListJsonSerializer(){
        mapper = new ObjectMapper();
    }

    public MyPageListJsonSerializer(ObjectMapper mapper){
        this.mapper = mapper;
    }
    @Override
    public void serialize(PageList value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        Map<String,Object> map = new HashMap<String, Object>();
        Paginator paginator = value.getPaginator();
        map.put("total", paginator.getTotalCount());//总记录数
        map.put("rows" , new ArrayList(value));
        map.put("totalPages", paginator.getTotalPages());//总页数
        map.put("totalCount" , paginator.getTotalCount());//也是总记录数
        map.put("pageNo", paginator.getPage());//当前页
        map.put("pageSize", paginator.getLimit());//每页记录数

        map.put("hasNextPage", paginator.isHasNextPage());//是否有下一页
        map.put("hasPrePage", paginator.isHasPrePage());//是否有上一页
        map.put("prePage", paginator.getPrePage());
        map.put("nextPage", paginator.getNextPage());

       Integer[] slider= paginator.getSlider();

        map.put("slider", slider);//显示的分页条

        map.put("firstPage", paginator.isFirstPage());//是否首页
        map.put("lastPage", paginator.isLastPage());//是否尾页
        mapper.writeValue(jgen, map);
    }
}
