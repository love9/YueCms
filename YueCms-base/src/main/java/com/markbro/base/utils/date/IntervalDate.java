package com.markbro.base.utils.date;



import java.util.Date;

/**
 * 处理一个时间段
 * @author Administrator
 *
 */
public class IntervalDate {
	private Date start;//开始日期
	private Date end;//结束日期
	
	private int intervalDays;
	private int intervalHours;
	private int intervalMinutes;
	private int intervalSeconds;
	
	public IntervalDate(Date end) {
		super();
		this.start = new Date();
		this.end = end;
		getDatas();
	}
	public IntervalDate(Date start, Date end) {
		super();
		this.start = start;
		this.end = end;
		getDatas();
	}
	public void getDatas(){
		long millis=getIntervalMillis();
		if(millis<1000*60){//如果相隔时间小于1分钟		
			this.setIntervalSeconds((int)millis/1000);
		}
		else if(millis<1000*3600){//如果相隔时间小于1小时			
			this.setIntervalMinutes((int)millis/60000);
			this.setIntervalSeconds((int)(millis/1000)%60);
		}
		else if(millis<1000*3600*24){//如果相隔时间小于1天			
			this.setIntervalHours((int)(millis/(1000*3600)));
			this.setIntervalMinutes((int)((millis%(1000*3600))/60000));
			this.setIntervalSeconds((int)((millis/1000)%60));
		}else{//相隔时间大于1天			
			this.setIntervalDays((int)(millis/(1000*3600*24)));
			this.setIntervalHours((int)((millis%(1000*3600*24))/3600000));
			this.setIntervalMinutes((int)(((millis%(1000*3600*24))%3600000)/60000));
			this.setIntervalSeconds((int)((millis/1000)%60));
			
		}
	}
	public String getString(){
		return getIntervalDays()+"天"+getIntervalHours()+"小时"+getIntervalMinutes()+"分钟"+getIntervalSeconds()+"秒";
	}
	 /**
	  * 如果end时间在start时间之后返回true否则返回false
	  * @return
	  */
	public boolean getFlag(){
		
		return DateUtil.isAfter(end, start)? true:false;
	}
	public  long getIntervalMillis() {
		return Math.abs(end.getTime()-start.getTime());
	 }
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}

	public int getIntervalDays() {
		return intervalDays;
	}
	public void setIntervalDays(int intervalDays) {
		this.intervalDays = intervalDays;
	}
	public int getIntervalHours() {
		return intervalHours;
	}
	public void setIntervalHours(int intervalHours) {
		this.intervalHours = intervalHours;
	}
	public int getIntervalMinutes() {
		return intervalMinutes;
	}
	public void setIntervalMinutes(int intervalMinutes) {
		this.intervalMinutes = intervalMinutes;
	}
	public int getIntervalSeconds() {
		return intervalSeconds;
	}
	public void setIntervalSeconds(int intervalSeconds) {
		this.intervalSeconds = intervalSeconds;
	}
	public static void main(String[] args) {
		Date d=DateUtil.addHour(-1);
		// d=DateUtil.addDate(d,-5);
		d= DateUtil.addMinute(d, 56);
		//d=DateUtil.addSecond(d,5);

		System.out.println(DateUtil.simpleformatIntervalTime(new Date(), d));
	}
}
