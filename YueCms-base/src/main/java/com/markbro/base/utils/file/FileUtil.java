package com.markbro.base.utils.file;


import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wujiyue
 */
public class FileUtil {
	public static String[] IMAGE_TYPES={".jpg",".png",".bmp",".jpeg",".gif"};
	//-------------TmFileUtil----------start
	public static final long ONE_KB = 1024;
	public static final long ONE_MB = ONE_KB * ONE_KB;
	public static final long ONE_GB = ONE_KB * ONE_MB;
	public static final DateFormat pathDf = new SimpleDateFormat("yyyyMM");
	public static final DateFormat nameDf = new SimpleDateFormat("ddHHmmss");
	static int countFiles = 0;// 声明统计文件个数的变量
	static int countFolders = 0;// 声明统计文件夹的变量

	/* [文件删除] */

	// 验证字符串是否为正确路径名的正则表达式
	private static String matches = "[A-Za-z]:\\\\[^:?\"><*]*";
	// 通过spath.matches(matches)方法的返回值判断是否正确
	static boolean flag = false;
	static File file;

	public static String getType(String file){
		if(new File(file).isDirectory()){
			return "floder";
		}else{
			return "file";
		}
	}

	public static File[] getFolders(String dir){
		File dirFile=new File(dir);
		return dirFile.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if(pathname.isDirectory()){
					return true;
				}
				return false;
			}
		});
	}
	public static String getTime(String file){
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(new File(file).lastModified()));
	}

	// 删除文件
	public static boolean deleteFile(String[] filePaths) {
		for (String string : filePaths) {
			File file = new File(string);
			deleteFile(file);
		}
		return true;
	}

	// 删除目录及目录下的文件
	public static boolean deleteDirectory(String filePath) {
		// 如果spath不以文件分隔结尾，自动添加文件分隔符
		if (!filePath.endsWith(File.separator)) {
			filePath = filePath + File.separator;
		}
		return deleteDirectory(new File(filePath));
	}

	// 删除目录及目录下的文件
	public static boolean deleteDirectory(File dirFile) {
		// 如果文件或者文件夹不存在，则退出
		if (!dirFile.exists() && !dirFile.isDirectory()) {
			return false;
		}
		flag = true;
		File[] files = dirFile.listFiles();
		// 遍历删除文件夹中的文件
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) {
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag) {
					break;
				}
			} else {
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag) {
					break;
				}
			}
		}
		if (!flag) {
			return false;
		}
		if (dirFile.delete()) {
			return true;
		} else {
			return false;
		}
	}

	public static void deleteDirectorys(File directory) throws IOException {
		if (!directory.exists()) {
			return;
		}

		cleanDirectory(directory);
		if (!directory.delete()) {
			String message = "Unable to delete directory " + directory + ".";
			throw new IOException(message);
		}
	}

	public static void forceDelete(File file) throws IOException {
		if (file.isDirectory()) {
			deleteDirectory(file);
		} else {
			boolean filePresent = file.exists();
			if (!file.delete()) {
				if (!filePresent) {
					throw new FileNotFoundException("File does not exist: "
							+ file);
				}
				String message = "Unable to delete file: " + file;
				throw new IOException(message);
			}
		}
	}

	public static boolean deleteQuietly(File file) {
		if (file == null) {
			return false;
		}
		try {
			if (file.isDirectory()) {
				cleanDirectory(file);
			}
		} catch (Exception e) {
		}

		try {
			return file.delete();
		} catch (Exception e) {
			return false;
		}
	}

	public static void cleanDirectory(File directory) throws IOException {
		if (!directory.exists()) {
			String message = directory + " does not exist";
			throw new IllegalArgumentException(message);
		}

		if (!directory.isDirectory()) {
			String message = directory + " is not a directory";
			throw new IllegalArgumentException(message);
		}

		File[] files = directory.listFiles();
		if (files == null) { // null if security restricted
			throw new IOException("Failed to list contents of " + directory);
		}

		IOException exception = null;
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			try {
				forceDelete(file);
			} catch (IOException ioe) {
				exception = ioe;
			}
		}

		if (null != exception) {
			throw exception;
		}
	}

	private static void deleteDirectoryOnExit(File directory)
			throws IOException {
		if (!directory.exists()) {
			return;
		}

		cleanDirectoryOnExit(directory);
		directory.deleteOnExit();
	}

	public static void cleanDirectoryOnExit(File directory) throws IOException {
		if (!directory.exists()) {
			String message = directory + " does not exist";
			throw new IllegalArgumentException(message);
		}

		if (!directory.isDirectory()) {
			String message = directory + " is not a directory";
			throw new IllegalArgumentException(message);
		}

		File[] files = directory.listFiles();
		if (files == null) { // null if security restricted
			throw new IOException("Failed to list contents of " + directory);
		}

		IOException exception = null;
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			try {
				forceDeleteOnExit(file);
			} catch (IOException ioe) {
				exception = ioe;
			}
		}

		if (null != exception) {
			throw exception;
		}
	}

	public static void forceDeleteOnExit(File file) throws IOException {
		if (file.isDirectory()) {
			deleteDirectoryOnExit(file);
		} else {
			file.deleteOnExit();
		}
	}



	/**
	 * 删除单个文件(通过命令的方式删除)
	 *
	 * @param path
	 *            包含文件的绝对路径.
	 * @return
	 */
	public static int deleteFileByDoc(String path) {
		path = path.replaceAll("/", "\\\\");
		path = path.replaceAll("\\\\\\\\", "\\\\");
		if (new File(path).exists()) {
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec("cmd /c del " + path);
				return 0;
			} catch (IOException e) {
				return 1;
			}
		} else {
			return 2;
		}
	}

	/**
	 * 强制删除一个目录以及该目录下的子文件和目录(通过命令的方式删除)
	 *
	 * @param path
	 *            文件的路径
	 * @return
	 */
	public static int deleteDirByDoc(String path) {
		path = path.replaceAll("/", "\\\\");
		path = path.replaceAll("\\\\\\\\", "\\\\");
		if (new File(path).exists()) {
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec("cmd /c rd /s/q " + path);
				return 0;
			} catch (IOException e) {
				return 1;
			}
		} else {
			return 2;
		}
	}

	/**
	 * 文件删除
	 *
	 * @param pathName
	 * @return
	 */
	public static int delete(String pathName) {
		return delete(new File(pathName));
	}

	/**
	 * 文件删除
	 *
	 * @param filename
	 * @return
	 */
	public static int delete(File filename) {
		if (filename.isFile()) {
			return deleteFileByDoc(filename.getAbsolutePath());
		} else {
			return deleteDirByDoc(filename.getAbsolutePath());
		}
	}

	/* [/文件删除结束] */

	/* [文件移动] */
	// 将文件移动一个目录下.
	public static int moveByDoc(String sourceFile, String targetPath) {
		sourceFile = sourceFile.replaceAll("/", "\\\\");
		sourceFile = sourceFile.replaceAll("\\\\\\\\", "\\\\");

		targetPath = targetPath.replaceAll("/", "\\\\");
		targetPath = targetPath.replaceAll("\\\\\\\\", "\\\\");

		Runtime runtime = Runtime.getRuntime();
		if (new File(sourceFile).exists()) {
			try {
				runtime.exec("cmd /c move " + sourceFile + " " + targetPath);
				return 0;
			} catch (IOException e) {
				return 1;
			}
		} else {
			return 2;
		}
	}

	// 将文件移动一个目录下.
	public static int copyByDoc(String sourceFile, String targetPath) {
		sourceFile = sourceFile.replaceAll("/", "\\\\");
		sourceFile = sourceFile.replaceAll("\\\\\\\\", "\\\\");

		targetPath = targetPath.replaceAll("/", "\\\\");
		targetPath = targetPath.replaceAll("\\\\\\\\", "\\\\");

		Runtime runtime = Runtime.getRuntime();
		if (new File(sourceFile).exists()) {
			try {
				runtime.exec("cmd /c copy " + sourceFile + " " + targetPath);
				return 0;
			} catch (IOException e) {
				return 1;
			}
		} else {
			return 2;
		}
	}

	/* [/文件移动] */

	/**
	 * 打开系统记事本
	 *
	 * @throws IOException
	 */
	public static void openSystemTxt() throws IOException {
		Runtime runtime = Runtime.getRuntime();
		runtime.exec("notepad");
	}

	/**
	 * 用记事本打开
	 * @param sourceFile
	 * @throws IOException
	 */
	public static void openSystemTxt(String sourceFile) throws IOException {
		sourceFile = sourceFile.replaceAll("/", "\\\\");
		sourceFile = sourceFile.replaceAll("\\\\\\\\", "\\\\");
		Runtime runtime = Runtime.getRuntime();
		runtime.exec("notepad " + sourceFile);
	}

	/**
	 * 执行批处理文件
	 * @param batFile
	 * @throws IOException
	 */

	public static void executeBatFile(String batFile) throws IOException {
		Runtime runtime = Runtime.getRuntime();
		runtime.exec(batFile);
	}

	public static void copyDirectory(File srcDir, File destDir)
			throws IOException {
		copyDirectory(srcDir, destDir, true);
	}

	public static void copyDirectory(File srcDir, File destDir,
									 boolean preserveFileDate) throws IOException {
		copyDirectory(srcDir, destDir, null, preserveFileDate);
	}

	@SuppressWarnings("unchecked")
	public static void copyDirectory(File srcDir, File destDir,
									 FileFilter filter, boolean preserveFileDate) throws IOException {
		if (srcDir == null) {
			throw new NullPointerException("Source must not be null");
		}
		if (destDir == null) {
			throw new NullPointerException("Destination must not be null");
		}
		if (srcDir.exists() == false) {
			throw new FileNotFoundException("Source '" + srcDir
					+ "' does not exist");
		}
		if (srcDir.isDirectory() == false) {
			throw new IOException("Source '" + srcDir
					+ "' exists but is not a directory");
		}
		if (srcDir.getCanonicalPath().equals(destDir.getCanonicalPath())) {
			throw new IOException("Source '" + srcDir + "' and destination '"
					+ destDir + "' are the same");
		}

		// Cater for destination being directory within the source directory
		// (see IO-141)
		List exclusionList = null;
		if (destDir.getCanonicalPath().startsWith(srcDir.getCanonicalPath())) {
			File[] srcFiles = filter == null ? srcDir.listFiles() : srcDir
					.listFiles(filter);
			if (srcFiles != null && srcFiles.length > 0) {
				exclusionList = new ArrayList(srcFiles.length);
				for (int i = 0; i < srcFiles.length; i++) {
					File copiedFile = new File(destDir, srcFiles[i].getName());
					exclusionList.add(copiedFile.getCanonicalPath());
				}
			}
		}
		doCopyDirectory(srcDir, destDir, filter, preserveFileDate,
				exclusionList);
	}

	@SuppressWarnings("unchecked")
	private static void doCopyDirectory(File srcDir, File destDir,
										FileFilter filter, boolean preserveFileDate, List exclusionList)
			throws IOException {
		if (destDir.exists()) {
			if (destDir.isDirectory() == false) {
				throw new IOException("Destination '" + destDir
						+ "' exists but is not a directory");
			}
		} else {
			if (destDir.mkdirs() == false) {
				throw new IOException("Destination '" + destDir
						+ "' directory cannot be created");
			}
			if (preserveFileDate) {
				destDir.setLastModified(srcDir.lastModified());
			}
		}
		if (destDir.canWrite() == false) {
			throw new IOException("Destination '" + destDir
					+ "' cannot be written to");
		}
		File[] files = filter == null ? srcDir.listFiles() : srcDir
				.listFiles(filter);
		if (files == null) {
			throw new IOException("Failed to list contents of " + srcDir);
		}
		for (int i = 0; i < files.length; i++) {
			File copiedFile = new File(destDir, files[i].getName());
			if (exclusionList == null
					|| !exclusionList.contains(files[i].getCanonicalPath())) {
				if (files[i].isDirectory()) {
					doCopyDirectory(files[i], copiedFile, filter,
							preserveFileDate, exclusionList);
				} else {
					doCopyFile(files[i], copiedFile, preserveFileDate);
				}
			}
		}
	}

	private static void doCopyFile(File srcFile, File destFile,
								   boolean preserveFileDate) throws IOException {
		if (destFile.exists() && destFile.isDirectory()) {
			throw new IOException("Destination '" + destFile
					+ "' exists but is a directory");
		}

		FileInputStream input = new FileInputStream(srcFile);
		try {
			FileOutputStream output = new FileOutputStream(destFile);
			try {
				IOUtils.copy(input, output);
			} finally {
				IOUtils.closeQuietly(output);
			}
		} finally {
			IOUtils.closeQuietly(input);
		}

		if (srcFile.length() != destFile.length()) {
			throw new IOException("Failed to copy full contents from '"
					+ srcFile + "' to '" + destFile + "'");
		}
		if (preserveFileDate) {
			destFile.setLastModified(srcFile.lastModified());
		}
	}

	public static void copyFileToDirectory(File srcFile, File destDir)
			throws IOException {
		copyFileToDirectory(srcFile, destDir, true);
	}

	public static void copyFileAll(String srcFile, String targetFile)
			throws Exception {
		File src_File = new File(srcFile);
		File tar_File = new File(targetFile);
		boolean isTargetFile = true;
		if (targetFile.substring(targetFile.lastIndexOf("/")).contains(".")) {
			isTargetFile = false;
		}
		if (src_File.isFile() && !isTargetFile) {
			copyFile(src_File, tar_File, true);
		} else if (src_File.isDirectory() && isTargetFile) {
			if (!tar_File.exists())
				tar_File.mkdirs();
			copyDirectory(src_File, tar_File);
		} else if (src_File.isFile() && isTargetFile) {
			if (!tar_File.exists())
				tar_File.mkdirs();
			copyFileToDirectory(src_File, tar_File);
		}
	}

	public static void copyFile(File srcFile, File destFile,
								boolean preserveFileDate) throws IOException {
		if (srcFile == null) {
			throw new NullPointerException("Source must not be null");
		}
		if (destFile == null) {
			throw new NullPointerException("Destination must not be null");
		}
		if (srcFile.exists() == false) {
			throw new FileNotFoundException("Source '" + srcFile
					+ "' does not exist");
		}
		if (srcFile.isDirectory()) {
			throw new IOException("Source '" + srcFile
					+ "' exists but is a directory");
		}
		if (srcFile.getCanonicalPath().equals(destFile.getCanonicalPath())) {
			throw new IOException("Source '" + srcFile + "' and destination '"
					+ destFile + "' are the same");
		}
		if (destFile.getParentFile() != null
				&& destFile.getParentFile().exists() == false) {
			if (destFile.getParentFile().mkdirs() == false) {
				throw new IOException("Destination '" + destFile
						+ "' directory cannot be created");
			}
		}
		if (destFile.exists() && destFile.canWrite() == false) {
			throw new IOException("Destination '" + destFile
					+ "' exists but is read-only");
		}
		doCopyFile(srcFile, destFile, preserveFileDate);
	}

	public static void copyFileToDirectory(File srcFile, File destDir,
										   boolean preserveFileDate) throws IOException {
		if (destDir == null) {
			throw new NullPointerException("Destination must not be null");
		}
		if (destDir.exists() && destDir.isDirectory() == false) {
			throw new IllegalArgumentException("Destination '" + destDir
					+ "' is not a directory");
		}
		copyFile(srcFile, new File(destDir, srcFile.getName()),
				preserveFileDate);
	}

	/* ExmayFileCount.java */
	/**
	 * 统计文档的数量
	 *
	 * @param dirPath
	 * @return
	 */
	public static int countFiles(String dirPath) {
		return countFiles(new File(dirPath));
	}

	/**
	 * 统计文档的数量
	 *
	 * @param file
	 * @return
	 */
	public static int countFiles(File file) {
		if (file.exists()) {
			if (file.isDirectory()) {
				String[] files = file.list();
				if (files.length == 0) {
					return 0;
				} else {
					int filenum = 0;
					for (int i = 0; i < files.length; i++) {
						filenum += countFiles(new File(file, files[i]));
					}
					return filenum;
				}
			} else {
				return 1;
			}
		} else {
			return 0;
		}
	}

	public long getlist(File f) {// 递归求取目录文件个数
		long size = 0;
		File flist[] = f.listFiles();
		size = flist.length;
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getlist(flist[i]);
				size--;
			}
		}
		return size;
	}

	/* [/ExmayFileCount.java] */

	/* [ExmayFileCountSize.java] */
	/**
	 * 统计文件大小
	 *
	 * @param pathname
	 * @return
	 */
	public static String countFileSize(String pathname) {
		String fileSizeString = "";
		try {
			File file = new File(pathname);
			DecimalFormat df = new DecimalFormat("#.00");
			long fileS = file.length();
			if (fileS < 1024) {
				fileSizeString = "0byte";
			} else if (fileS < 1048576) {
				fileSizeString = df.format((double) fileS / 1024) + "KB";
			} else if (fileS < 1073741824) {
				fileSizeString = df
						.format(((double) fileS / 1024 / 1024) - 0.01)
						+ "MB";
			} else {
				fileSizeString = df.format((double) fileS / 1024 / 1024 / 1024)
						+ "G";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileSizeString;
	}

	/**
	 * 递归获取目录的大小
	 *
	 * @param directory
	 *            目录
	 * @return 返回目录大小
	 */
	public static long sizeofDirectory(String directory) {
		return sizeOfDirectory(new File(directory));
	}

	/**
	 * 统计一个文件夹的大小
	 *
	 * @param directory
	 * @return
	 */
	public static long sizeOfDirectory(File directory) {
		if (!directory.exists()) {
			throw new IllegalArgumentException(directory + " does not exist");
		}
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException(directory
					+ " is not a directory");
		}

		long size = 0;

		File[] files = directory.listFiles();
		if (files == null) {
			return 0L;
		}
		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			if (file.isDirectory()) {
				size += sizeOfDirectory(file);
			} else {
				size += file.length();
			}
		}
		return size;
	}

	/**
	 *统计MP3文件时长方法
	 *
	 * @param mp3File
	 *            音频文件
	 * @return
	 */
//	public staticjsp String sizeOfMp3(String mp3File) {
//		return sizeOfMp3(new File(mp3File));
//	}

//	public staticjsp String sizeOfMp3(File mp3File) {
//		if (!mp3File.exists())
//			return "";
//		StringBuffer buffer = new StringBuffer();
//		if (getExtNoPoint(mp3File.getName()).equals("mp3")) {
//			try {
//				FileInputStream fis = new FileInputStream(mp3File);
//				int b = fis.available();
//				Bitstream bt = new Bitstream(fis);
//				Header h = bt.readFrame();
//				int time = (int) h.total_ms(b);
//
//				int i = time / 1000;
//				String m = "";
//				String s = "";
//				if ((i / 60) < 10) {
//					m = "0" + i / 60 + ":";
//				} else {
//					m = i / 60 + ":";
//				}
//				if (i % 60 < 10) {
//					s = "0" + i % 60;
//				} else {
//					s = i % 60 + "";
//				}
//				buffer.append(m + s);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		} else {
//			buffer.append("00:00");
//		}
//		return buffer.toString();
//	}

	/* [/ExmayFileCountSize.java] */

	/* [ExmayFileCreate.java] */
	/**
	 * 新建文件
	 *
	 * @param filePathAndName
	 *            String 文件路径及名称 如c:/fqf.txt
	 * @param fileContent
	 *            String 文件内容
	 * @return boolean
	 */
	public static void newFile(String filePathAndName, String fileContent) {

		try {
			String filePath = filePathAndName;
			filePath = filePath.toString();
			File myFilePath = new File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.createNewFile();
			}
			FileWriter resultFile = new FileWriter(myFilePath);
			PrintWriter myFile = new PrintWriter(resultFile);
			String strContent = fileContent;
			myFile.println(strContent);
			resultFile.close();
		} catch (Exception e) {
			System.out.println("新建目录操作出错");
			e.printStackTrace();

		}
	}

	public static void createFile(String path, boolean isFile) {
		createFile(new File(path), isFile);
	}

	public static void createFile(File file, boolean isFile) {
		if (!file.exists() && file.isFile()) {
			if (!file.getParentFile().exists()) {
				createFile(file.getParentFile(), false);
			} else {
				if (isFile) {
					try {
						file.mkdirs();
						file.createNewFile();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			file.mkdirs();
		}
	}



	/* [/ExmayFileCreate.java] */
	/* [ExmayFileDownNetFile.java] */
	// 根据一个图片的url，通过url的方式保存到本地。
	public static boolean downImg(String filePath, String imageUrl,
								  String fileName) {
		try {
			File files = new File(filePath);
			if (!files.exists()) {
				files.mkdirs();
			}
			URL url = new URL(imageUrl);
			URLConnection con = url.openConnection();
			InputStream is = con.getInputStream();
			if (!filePath.endsWith("/")) {
				filePath = filePath + "/";
			}
			File file = new File(filePath + fileName);
			FileOutputStream out = new FileOutputStream(file);
			int i = 0;
			while ((i = is.read()) != -1) {
				out.write(i);
			}
			is.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// 根据一个图片的url，通过url的方式保存到本地。
	public static Map<String, Object> downPicture(String filePath,
												  String imageUrl, String fileName) {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer size = null;
		String mime = null;
		try {
			File files = new File(filePath);
			if (!files.exists()) {
				files.mkdirs();
			}
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			InputStream is = connection.getInputStream();
			if (!filePath.endsWith("/")) {
				filePath = filePath + "/";
			}
			File file = new File(filePath + fileName);
			FileOutputStream out = new FileOutputStream(file);
			int i = 0;
			while ((i = is.read()) != -1) {
				out.write(i);
			}
			size = connection.getContentLength();
			mime = connection.getContentType();
			is.close();
		} catch (Exception e) {

		}
		map.put("size", size);
		map.put("mime", mime);
		return map;
	}

	public static int getLength(String htmlUrl) throws IOException {
		URL url = new URL(htmlUrl);
		URLConnection con = url.openConnection();
		return con.getContentLength();
	}

	public static String downNetImg(String filePath,String remotePath, String htmlUrl,
									String fileName) {
		File file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		URL url = null;
		OutputStream os = null;
		try {
			url = new URL(htmlUrl);
			URLConnection con = url.openConnection();
			int lenss = con.getContentLength();
			InputStream is = url.openStream();
			os = new FileOutputStream(filePath + fileName);
			int length = 0;
			byte[] buffer = new byte[5 * 1024];
			int len = 0;
			while ((length = is.read(buffer, 0, 5 * 1024)) != -1) {
				len = length + len;
				double percent = Math.round((len / (double) lenss) * 100D);
				os.write(buffer, 0, length);
			}
			return remotePath + fileName;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		} finally {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getNetFileSize(String urlFile) {
		URL url;
		try {
			url = new URL(urlFile);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			return connection.getContentType();
		} catch (Exception e) {
			return "";
		}
	}

	public static String getMineType(String urlFile) {
		URL url;
		try {
			url = new URL(urlFile);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			return countFileSize(connection.getContentLength());
		} catch (Exception e) {
			return "0";
		}
	}

	public static void supportURLType(String host, String file) {
		String[] schemes = { "http", "https", "ftp", "mailto", "telent",
				"file", "ldap", "gopher", "jdbc", "rmi", "jar", "doc",
				"netdoc", "nfs", "verbatim", "finger", "daytime",
				"systemresouce" };
		for (int i = 0; i < schemes.length; i++) {
			try {
				@SuppressWarnings("unused")
				URL url = new URL(schemes[i], host, file);
				System.out.println(schemes[i] + "是Java所支持的URL类型\r\n");
			} catch (Exception e) {
				System.out.println(schemes[i] + "不是Java所支持的URL类型\r\n");
			}
		}
	}

	/* [/ExmayFileDownNetFile.java] */

	/* [ExmayFileEncodingTransfer.java] */
	/**
	 * xiaoer 文本文件转换为制定编码的字符串?记得要做成批量的。尚未完成。
	 *
	 * @param filePath
	 *            文件的路径含有文件.
	 * @param encoding
	 *            要转换后的编码.
	 * @return
	 */
	public static String fileToString(String filePath, String encoding) {
		InputStreamReader reader = null;
		StringWriter writer = new StringWriter();
		try {
			if (encoding == null || "".equals(encoding)) {
				reader = new InputStreamReader(new FileInputStream(new File(
						filePath)), encoding);
			} else {
				reader = new InputStreamReader(new FileInputStream(new File(
						filePath)));
			}
			char[] buffer = new char[1024];
			int n = 0;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}

			}
		}
		if (writer != null) {
			return writer.toString();
		} else {
			return null;
		}
	}



	/**
	 * 获取需要过滤的后缀
	 *
	 * @param fileTypes
	 *            从数据库获取的后缀
	 * @return
	 */
	public static String[] getFileExts(String fileTypes) {
		if (fileTypes.indexOf(";") == -1) {
			return new String[] { fileTypes };
		}
		String[] exts = fileTypes.split(";");
		String[] fileExts = new String[exts.length];
		for (int i = 0; i < exts.length; i++) {
			fileExts[i] = getExtNoPoint(exts[i]);
		}
		return fileExts;
	}

	public static void sortByTime(File[] files) {
		Arrays.sort(files, new Comparator<File>() {
			public int compare(File file1, File file2) {
				long diff = file1.lastModified() - file2.lastModified();
				if (diff > 0)
					return -1;
				else if (diff == 0)
					return 0;
				else
					return 1;
			}
		});
	}

	/**
	 * 获取当前目录下的目录和文件（名称）
	 *
	 * @param path
	 * @return
	 */
	public static List<String> getDirectoryAndFile(String path) {
		List<String> list = new ArrayList<String>();
		File file = new File(path);
		if (file.isFile()) {
			list.add(file.getName());
			return list;
		}
		File[] files = file.listFiles();
		/** 按文件的修改时间排序 **/
		sortByTime(files);
		for (File file2 : files) {
			list.add(file2.getName());
		}
		return list; // 将ArrayList集合对象返回list
	}

	/**
	 * 获取当前目录下的目录和文件（绝对路径）
	 *
	 * @param path
	 * @return
	 */
	public static List<String> getDirAbsolutePathAndFile(String path) {
		List<String> list = new ArrayList<String>();
		File file = new File(path);
		if (file.isFile()) {
			list.add(file.getAbsolutePath());
			return list;
		}
		File[] files = file.listFiles();
		sortByTime(files);
		for (File file2 : files) {
			list.add(file2.getAbsolutePath());
		}
		return list;
	}

	/**
	 * 获取当前目录下属于exts后缀的文件名。不包含目录名
	 *
	 * @param path
	 * @param exts
	 * @return
	 */
	public static List<String> getDirectoryFileNameByExt(String path,
														 String[] exts) {
		List<String> list = new ArrayList<String>();
		for (String file : getDirAbsolutePathAndFile(path)) {
			if (new File(file).isFile()) {
				String suffix = getExtNoPoint(file);
				String filename = getFileName(file);
				for (String ext : exts) {
					if (suffix.equalsIgnoreCase(ext)) {
						list.add(filename);
					}
				}
			}
		}
		return list;
	}

	/**
	 * 获取当前目录下的目录名称和文件名称
	 *
	 * @param dir
	 * @return
	 */
	public static String[] getDirectoryFirstFileAndDIR(String dir) {
		return new File(dir).list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.charAt(0) != '.';
			}
		});
	}

	/**
	 * 获取一个文件下的第一级目录名称和文件名
	 *
	 * @param path
	 * @return
	 */
	public static List<String> getFirstDirAndFile(String path) {
		List<String> list = new ArrayList<String>();
		File file = new File(path);
		File[] files = file.listFiles();
		for (File file2 : files) {
			if (file2.isDirectory()) {
				list.add(file2.getAbsolutePath().replaceAll(" ", "_"));
				renameFile(file2.getAbsolutePath(), file2.getAbsolutePath()
						.replaceAll(" ", "_"));
			} else {
				renameFile(file2.getAbsolutePath(), file2.getAbsolutePath()
						.replaceAll(" ", "_"));
				list.add(file2.getAbsolutePath().replaceAll(" ", "_"));
			}
		}
		return list;
	}



	public static long getDeleteTime(String filePath) {
		Pattern pattern = Pattern.compile("\\[exmay-delete-(\\d*)\\]");
		Matcher matcher = pattern.matcher(filePath);
		if (matcher.find()) {
			return Long.parseLong(matcher.group(1));
		}
		return 0;
	}

	// 获取磁盘所有文件方法
	public static List<String> collectionsFiles(String path) {
		LinkedList<File> list = new LinkedList<File>();
		ArrayList<String> listPath = new ArrayList<String>();
		File dir = new File(path);
		File file[] = dir.listFiles();
		for (int i = 0; i < file.length; i++) {
			if (file[i].isDirectory()) {
				list.add(file[i]);
			} else {
				listPath.add(file[i].getAbsolutePath());
			}
		}
		File tmp;
		while (!list.isEmpty()) {
			tmp = list.removeFirst();
			if (tmp.isDirectory()) {
				file = tmp.listFiles();
				if (file == null) {
					continue;
				}
				for (int i = 0; i < file.length; i++) {
					if (file[i].isDirectory()) {
						list.add(file[i]);
					} else {
						listPath.add(file[i].getAbsolutePath());
					}
				}
			}
		}
		return listPath;
	}

	/**
	 * 获取当前目录下的文件夹名称 docs<br>
	 * lib<br>
	 * META-INF<br>
	 * ooxml-lib<br>
	 * org<br>
	 *
	 * @param filepath
	 * @return
	 */
	public static List<String> getDirName(String filepath) {
		List<String> directorys = new ArrayList<String>();
		File file = new File(filepath);
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				directorys.add(files[i].getName());
			}
		}
		return directorys;
	}

	/**
	 * 返回当前目录下的文件名(绝对路径)
	 *
	 * @param path
	 *            D:\poi-bin-3.8-beta2-20110408\poi-3.8-beta2\docs
	 *            D:\poi-bin-3.8-beta2-20110408\poi-3.8-beta2\lib
	 *            D:\poi-bin-3.8-beta2-20110408\poi-3.8-beta2\META-INF
	 *            D:\poi-bin-3.8-beta2-20110408\poi-3.8-beta2\ooxml-lib
	 *            D:\poi-bin-3.8-beta2-20110408\poi-3.8-beta2\org
	 * @return
	 */
	public static List<String> getDirAbsolutePath(String path) {
		List<String> list = new ArrayList<String>();
		File file = new File(path);
		if (file.isFile()) {
			list.add(file.getAbsolutePath());
			return list;
		}
		File[] files = file.listFiles();
		sortByTime(files);
		for (File file2 : files) {
			if (file2.isDirectory()) {
				list.add(file2.getAbsolutePath());
			}
		}

		return list;
	}




	public static void moveDirectory(File srcDir, File destDir)
			throws IOException {
		if (srcDir == null) {
			throw new NullPointerException("Source must not be null");
		}
		if (destDir == null) {
			throw new NullPointerException("Destination must not be null");
		}
		if (!srcDir.exists()) {
			throw new FileNotFoundException("Source '" + srcDir
					+ "' does not exist");
		}
		if (!srcDir.isDirectory()) {
			throw new IOException("Source '" + srcDir + "' is not a directory");
		}
		if (destDir.exists()) {
			throw new IOException("Destination '" + destDir
					+ "' already exists");
		}
		boolean rename = srcDir.renameTo(destDir);
		if (!rename) {
			copyDirectory(srcDir, destDir);
			deleteDirectory(srcDir);
			if (srcDir.exists()) {
				throw new IOException("Failed to delete original directory '"
						+ srcDir + "' after copy to '" + destDir + "'");
			}
		}
	}

	public static void moveDirectoryToDirectory(File src, File destDir,
												boolean createDestDir) throws IOException {
		if (src == null) {
			throw new NullPointerException("Source must not be null");
		}
		if (destDir == null) {
			throw new NullPointerException(
					"Destination directory must not be null");
		}
		if (!destDir.exists() && createDestDir) {
			destDir.mkdirs();
		}
		if (!destDir.exists()) {
			throw new FileNotFoundException("Destination directory '" + destDir
					+ "' does not exist [createDestDir=" + createDestDir + "]");
		}
		if (!destDir.isDirectory()) {
			throw new IOException("Destination '" + destDir
					+ "' is not a directory");
		}
		moveDirectory(src, new File(destDir, src.getName()));

	}

	public static void moveFile(File srcFile, File destFile) throws Exception {
		if (srcFile == null) {
			throw new NullPointerException("Source must not be null");
		}
		if (destFile == null) {
			throw new NullPointerException("Destination must not be null");
		}
		if (!srcFile.exists()) {
			throw new FileNotFoundException("Source '" + srcFile
					+ "' does not exist");
		}
		if (srcFile.isDirectory()) {
			throw new IOException("Source '" + srcFile + "' is a directory");
		}
		if (destFile.exists()) {
			throw new IOException("Destination '" + destFile
					+ "' already exists");
		}
		if (destFile.isDirectory()) {
			throw new IOException("Destination '" + destFile
					+ "' is a directory");
		}
		boolean rename = srcFile.renameTo(destFile);
		if (!rename) {
			copyFile(srcFile, destFile, true);
			if (!srcFile.delete()) {
				deleteQuietly(destFile);
				throw new IOException("Failed to delete original file '"
						+ srcFile + "' after copy to '" + destFile + "'");
			}
		}
	}

	public static String genPathName() {
		return pathDf.format(new Date());
	}

	/**
	 * 获取一个文件的后缀(带有点)
	 *
	 * @param fileName
	 *            文件名
	 * @return 返回文件的后缀
	 */
	public static String getExt(String fileName) {
		int pos = fileName.lastIndexOf(".");
		if (pos == -1)
			return "";
		return fileName.substring(pos, fileName.length());
	}

	/**
	 * 获取一个文件的后缀(不带有点)
	 *
	 * @param fileName
	 *            文件名
	 * @return 返回文件的后缀
	 */
	public static String getExtNoPoint(String fileName) {
		if (fileName.lastIndexOf(".") == -1)
			return "";
		int pos = fileName.lastIndexOf(".") + 1;
		return fileName.substring(pos, fileName.length());
	}

	/**
	 * 获取一个文件最后一个斜线前的内容(带有点)
	 * resource/system/upload/xuchengfeifei/2011/01/01/20110101144251319.jpg ==
	 * >resource/system/upload/xuchengfeifei/2011/01/01/20110101144251319
	 *
	 * @param fileName
	 *            文件名
	 * @return 返回文件的后缀
	 */
	public static String getName(String fileName) {
		int pos = fileName.lastIndexOf(".");
		if (pos == -1) {
			return fileName;
		} else {
			return fileName.substring(0, pos);
		}
	}

	/**
	 * 获取一个文件的文件名(带有后缀)
	 * resource/system/upload/xuchengfeifei/2011/01/01/20110101144251319.jpg ==
	 * >20110101144251319.jpg
	 *
	 * @param fileName
	 *            文件
	 * @return 返回文件的文件名
	 */
	public static String getFileName(String fileName) {
		fileName = transfer(fileName);
		int pos = fileName.lastIndexOf("/") + 1;
		if (pos == -1) {
			return fileName;
		} else {
			return fileName.substring(pos, fileName.length());
		}
	}

	private static String transfer(String fileName) {
		if (fileName.indexOf("\\") != -1) {
			fileName = fileName.replaceAll("\\\\", "/");
		}
		return fileName;
	}

	/**
	 * 获取一个文件的文件名(带有不带后缀)
	 * resource/system/upload/xuchengfeifei/2011/01/01/20110101144251319.jpg ==
	 * >20110101144251319
	 *
	 * @param fileName
	 *            文件
	 * @return 返回文件的文件名
	 */
	public static String getNotExtName(String fileName) {
		fileName = transfer(fileName);
		String fString = getFileName(fileName);
		if (fString.lastIndexOf(".") != -1) {
			return fString.substring(0, fString.lastIndexOf("."));
		} else {
			return fString;
		}
	}

	/**
	 * 获取一个文件的文件名(带有不带后缀) resource/system/upload/xuchengfeifei/2011/01/01 <br>=
	 * = >resource/system/upload/xuchengfeifei/2011/01<br>
	 * resource/system/upload/xuchengfeifei/2011/01/01/ ==<br>
	 * resource/system/upload/xuchengfeifei/2011/01/01/ * @param fileName 文件
	 *
	 * @return 返回文件的文件名
	 */
	public static String getLastShap(String fileName, int count) {
		fileName = transfer(fileName);
		if (fileName.lastIndexOf("/") != -1) {
			return fileName.substring(count, fileName.lastIndexOf("/"));
		} else {
			return fileName;
		}
	}

	public static String getLastShap(String fileName, int count, String pattern) {
		fileName = transfer(fileName);
		if (fileName.lastIndexOf(pattern) != -1) {
			return fileName.substring(count, fileName.lastIndexOf(pattern));
		} else {
			return fileName.substring(count, fileName.length());
		}
	}

	/**
	 * 返回一个文件除了文件名以前的路径(不带有最好斜线)
	 * resource/system/upload/xuchengfeifei/2011/01/01/20110101144251319.jpg <br>=
	 * =转换后<br>
	 * resource/system/upload/xuchengfeifei/2011/01/01 <br>
	 * 如果是：resource/system/upload/xuchengfeifei/2011/01/01
	 * ==resource/system/upload/xuchengfeifei/2011/01
	 *
	 * @param pathName
	 *            文件路径+文件名
	 * @return
	 */
	public static String getLastBeforePath(String pathName) {
		pathName = transfer(pathName);
		if (pathName.lastIndexOf("/") != -1) {
			pathName = pathName.substring(0, pathName.length() - 1);
		}
		pathName = pathName.substring(0, pathName.lastIndexOf("/"));
		return pathName;
	}


	private static String getEntryName(String baseDirPath, File file) {
		if (!baseDirPath.endsWith(File.separator)) {
			baseDirPath += File.separator;
		}
		// 获取绝对路径
		String filepath = file.getAbsolutePath();
		if (file.isDirectory()) {
			filepath += "/";
		}
		int index = filepath.indexOf(baseDirPath);
		return filepath.substring(index + baseDirPath.length());
	}


	private static String conversionSpecialCharacters(String string) {
		return string.replaceAll("\\\\", "/");
	}

	public static boolean renameFile(String path, String oldname, String newname) {
		boolean result = false;
		if (!oldname.equals(newname)) {// 新的文件名和以前文件名不同时,才有必要进行重命名
			File oldfile = new File(path + "/" + oldname);
			File newfile = new File(path + "/" + newname);
			if (newfile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
				result = false;
			else {
				oldfile.renameTo(newfile);
				result = true;
			}

		}
		return result;
	}

	public static boolean renameFile(String oldname, String newname) {
		boolean result = false;
		if (!oldname.equals(newname)) {// 新的文件名和以前文件名不同时,才有必要进行重命名
			File oldfile = new File(oldname);
			File newfile = new File(newname);
			if (newfile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
				result = false;
			else {
				oldfile.renameTo(newfile);
				result = true;
			}

		}
		return result;
	}

	// 重命名
	public static boolean rename(String filepath, String fname, String newname) {
		File fl = new File(filepath); // 这里写上发替换的文件夹路径,注意使用双斜杠
		if (!fl.exists() || !fl.isDirectory()) {
			return false;
		}
		String[] files = fl.list();
		File f = null;
		String filename = "";
		for (String file : files) {
			f = new File(fl, file);// 注意,这里一定要写成File(fl,file)如果写成File(file)是行不通的,一定要全路径
			filename = f.getName();
			f.renameTo(new File(fl.getAbsolutePath() + "\\"
					+ filename.replace(fname, newname)));// 这里可以反复使用replace替换,当然也可以使用正则表达式来替换了

		}
		return true;
	}

	public static File[] searchFile(String file, String key) {
		return searchFile(new File(file), key);
	}

	@SuppressWarnings("unchecked")
	public static File[] searchFile(File folder, final String keyWord) {// 递归查找包含关键字的文件
		File[] subFolders = folder.listFiles(new FileFilter() {// 运用内部匿名类获得文件
			public boolean accept(File pathname) {// 实现FileFilter类的accept方法
				if (pathname.isFile())// 如果是文件
					countFiles++;
				else
					// 如果是目录
					countFolders++;
				if (pathname.isDirectory()
						|| (pathname.isFile() && pathname.getName()
						.contains(keyWord)))// 目录或文件包含关键字
					return true;
				return false;
			}
		});
		List result = new ArrayList();// 声明一个集合
		for (int i = 0; i < subFolders.length; i++) {// 循环显示文件夹或文件
			if (subFolders[i].isFile()) {// 如果是文件则将文件添加到结果列表中
				result.add(subFolders[i]);
			} else {// 如果是文件夹，则递归调用本方法，然后把所有的文件加到结果列表中
				File[] foldResult = searchFile(subFolders[i], keyWord);
				for (int j = 0; j < foldResult.length; j++) {// 循环显示文件
					result.add(foldResult[j]);// 文件保存到集合中
				}
			}
		}
		File files[] = new File[result.size()];// 声明文件数组，长度为集合的长度
		result.toArray(files);// 集合数组化
		return files;
	}

	String Filename = null;
	long FileSize = 0;
	long BlockNum = 0;

	private void getFileAttribute(String fileAndPath) {
		File file = new File(fileAndPath);
		Filename = file.getName();
		FileSize = file.length();
	}

	private long getBlockNum(long blockSize) {
		long fileSize = FileSize;
		if (fileSize <= blockSize) {
			return 1;
		} else {
			if (fileSize % blockSize > 0) {
				return (fileSize / blockSize) + 1;
			} else {
				return fileSize / blockSize;
			}
		}
	}

	@SuppressWarnings("unused")
	private String generateSeparatorFileName(String fileAndPath,
											 int currentBlock) {
		String separatorFile = fileAndPath + ".part" + currentBlock;
		System.out.println("文件拆分成：" + separatorFile);
		return separatorFile;
	}

	@SuppressWarnings("unused")
	private boolean writeFile(String fileAndPath, String fileSeparateName,
							  long blockSize, long beginPos) {
		RandomAccessFile raf = null;
		FileOutputStream fos = null;
		byte[] bt = new byte[2048];
		long writeByte = 0;
		int len = 0;
		try {
			raf = new RandomAccessFile(fileAndPath, "r");
			raf.seek(beginPos);
			fos = new FileOutputStream(fileSeparateName);
			while ((len = raf.read(bt)) > 0) {
				if (writeByte < blockSize) {
					writeByte = writeByte + len;
					if (writeByte <= blockSize) {
						fos.write(bt, 0, len);
					} else {
						len = len - (int) (writeByte - blockSize);
					}
				}
			}
			fos.close();
			raf.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (Exception e2) {
			}
			return false;
		}
		return true;
	}

	public boolean separatoFile(String fileAndPath, long blockSize) {
		getFileAttribute(fileAndPath);
		BlockNum = getBlockNum(blockSize);
		System.out.println("共拆分成" + BlockNum + "个文件");
		if (BlockNum == 1) {
			blockSize = FileSize;
		}
		@SuppressWarnings("unused")
		long writeSize = 0;
		long writeTotal = 0;
		for (int i = 0; i <= BlockNum; i++) {
			if (i < BlockNum) {
				writeSize = blockSize;
			} else {
				writeSize = FileSize - writeTotal;
			}
		}
		return true;
	}

	public  void splitDir(File dir, String type, int splitNum) {
		try {
			List<String> filesList = new ArrayList<String>();
			String[] files =(String[])getFilesPathByFilter(dir,null).toArray();
			for (String string : files) {
				String ext = getExtNoPoint(new File(string).getName());
				if (ext.equals(type)) {
					filesList.add(string);
				}
			}

			int j = splitNum;
			String filename = dir + "\\exmaytemp";
			String newName = filename;
			for (int i = 0; i < filesList.size(); i++) {
				File file = new File(filesList.get(i));
				copyFileToDirectory(new File(newName), file);
				file.delete();
				if (i % j == 0 && i != 0) {
					newName = filename + i / 200;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static File characterFileProcess(File file, String destPath) {
		// 创建一个输出流,用于写新的文件
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(destPath));
			// 创建一个输入流，用于读取文件
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			while (line != null) {
				String newLine = replace(line);
				// 将替换后的内容写入新的文件中
				writer.write(newLine);
				line = reader.readLine();
			}
			reader.close();
			writer.close();
			return new File(destPath);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String replace(String line) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("，", ",");
		map.put("。", ".");
		map.put("〈", "<");
		map.put("〉", ">");
		map.put("‖", "|");
		map.put("《", "<");
		map.put("》", ">");
		map.put("〔", "[");
		map.put("〕", "]");
		map.put("﹖", "?");
		map.put("？", "?");
		map.put("“", "\"");
		map.put("”", "\"");
		map.put("：", ":");
		map.put("、", ",");
		map.put("（", "(");
		map.put("）", ")");
		map.put("【", "[");
		map.put("】", "]");
		map.put("—", "-");
		map.put("～", "~");
		map.put("！", "!");
		map.put("‵", "'");
		map.put("①", "1");
		map.put("②", "2");
		map.put("③", "3");
		map.put("④", "4");
		map.put("⑤", "5");
		map.put("⑥", "6");
		map.put("⑦", "7");
		map.put("⑧", "8");
		map.put("⑨", "9");

		int length = line.length();
		for (int i = 0; i < length; i++) {
			String charat = line.substring(i, i + 1);
			if (map.get(charat) != null) {
				line = line.replace(charat, map.get(charat));
			}
		}
		return line;
	}

	public static void spiltToSmallFiles(File file, String outputFile)
			throws IOException {
		int fileCount = 0;
		// 定义单个文件的最大长度
		int MAX_SIZE = 10;
		// 创建文件输出流
		BufferedWriter writer = null;

		// 创建文件输入流
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = reader.readLine();
		StringBuffer buffer = new StringBuffer();
		while (line != null) {
			buffer.append(line).append("\r\n");
			// 判断缓存区长度是否达到文件的最大长度
			if (buffer.toString().getBytes().length > MAX_SIZE) {
				writer = new BufferedWriter(new FileWriter(outputFile
						+ "output" + fileCount + ".txt"));
				writer.write(buffer.toString());
				writer.close();
				fileCount++;
				// 清空缓冲区数据
				buffer = new StringBuffer();
			}
			// 如果没有达到文件最大长度，则继续读取下一行
			line = reader.readLine();

		}
		// 如果太大的文件已经读取完毕，直接将缓冲区数据写入文件
		writer = new BufferedWriter(new FileWriter(outputFile + "output"
				+ fileCount + ".txt"));
		writer.write(buffer.toString());
		writer.close();
	}

	public static void preprocess(File file, String outputDir) {
		try {
			spiltToSmallFiles(file, outputDir);
		} catch (Exception e) {

		}
	}

	@SuppressWarnings("unchecked")
	public static void readJARList(String filename) throws IOException {
		JarFile jarFile = new JarFile(filename);
		Enumeration en = jarFile.entries();
		System.out.println("文件名\t文件大小\t压缩后的大小");
		while (en.hasMoreElements()) {
			process(en.nextElement());
		}
	}

	public static void process(Object obj) {
		JarEntry entry = (JarEntry) obj;
		String name = entry.getName();
		long size = entry.getSize();
		long compressedSize = entry.getCompressedSize();
		System.out.println(name + "\t" + size + "\t" + compressedSize);
	}

	public static void readJARFile(String jarFileName, String filename)
			throws IOException {
		JarFile jarFile = new JarFile(jarFileName);
		JarEntry entry = jarFile.getJarEntry(filename);
		InputStream inputStream = jarFile.getInputStream(entry);
		readFileFromInputStream(inputStream);
		jarFile.close();
	}

	public static void readFileFromInputStream(InputStream inputStream) throws IOException {
		InputStreamReader in = new InputStreamReader(inputStream);
		BufferedReader reader = new BufferedReader(in);
		String line;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
		reader.close();
	}

	public static void writeFileByBytes(String content, String filename)
			throws IOException {
		File file = new File(filename);
		OutputStream out = null;
		try {
			out = new FileOutputStream(file, true);
			byte[] bytes = content.getBytes();
			out.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e2) {
				}
			}
		}
	}

	/**
	 * 操作文件 以字节的方式进行文件写入
	 *
	 * @param content
	 *            写入的内容
	 * @param filename
	 *            写入的文件
	 * @param encoding
	 *            写入的文件时的编码，可以为空
	 */
	public static void writeFileByChar(String content, String filename,
									   String encoding) {
		File file = new File(filename);
		Writer writer = null;
		try {
			if (null == encoding || "".equals(encoding)) {
				writer = new OutputStreamWriter(new FileOutputStream(file));
			} else {
				writer = new OutputStreamWriter(new FileOutputStream(file),
						encoding);
			}
			writer.write(content);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * 操作文件 以行为单位进行写入文件
	 *
	 * @param content
	 *            写入文件的内容
	 * @param filename
	 *            文件
	 */
	public static void writeFileByLine(String content, String filename,
									   boolean append) {
		File file = new File(filename);
		String pathname = new File(filename).getParent();
		if (!new File(pathname).exists()) {
			new File(pathname).mkdirs();
		}
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new FileOutputStream(file, append));
			writer.print(content);
			writer.println();
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public static void writeContentByLine(String content, String filename,
										  String encoding, boolean append) {
		File file = new File(filename);
		File pFile = new File(file.getParent());
		if (!pFile.exists()) {
			pFile.mkdirs();
		}
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(file, append), encoding));
			writer.print(content);
			writer.println();
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * 将内容一行行写入文件中
	 *
	 * @param content
	 * @param filename
	 */
	public static void writeFileByLine(String content, String filename) {
		File file = new File(filename);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new FileOutputStream(file));
			writer.print(content);
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * 将内容写入文件中
	 *
	 * @param content
	 * @param filename
	 */
	public static void writeFileByFileWriter(String content, String filename) {
		File file = new File(filename);
		Writer writer = null;
		try {
			writer = new FileWriter(file);
			writer.write(content);
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * 将内容写入文件中
	 *
	 * @param content
	 * @param filename
	 */
	public static void writeFileByBufferFileWriter(String content,
												   String filename) {
		File file = new File(filename);
		Writer writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(content);
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * 将内容写入到文件中。
	 *
	 * @param soruce
	 * @param destFile
	 * @param encoding
	 */
	public static void writerContent(String soruce, String destFile,
									 String encoding) {
		try {
			FileOutputStream outputStream = new FileOutputStream(new File(
					destFile));
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					outputStream, encoding);
			BufferedWriter writer = new BufferedWriter(outputStreamWriter);
			writer.write(soruce);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将内容追加到一个文件中.
	 *
	 * @param soruce
	 * @param destFile
	 * @param encoding
	 * @param isAppend
	 */
	public static void writerContentToFile(String soruce, File destFile,
										   String encoding, boolean isAppend) {
		try {
			FileOutputStream outputStream = new FileOutputStream(destFile,
					isAppend);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					outputStream, encoding);
			BufferedWriter writer = new BufferedWriter(outputStreamWriter);
			writer.write(soruce);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writerContentToFile(String soruce, String destFile,
										   String encoding, boolean isAppend) {
		writerContentToFile(soruce, new File(destFile), encoding, isAppend);
	}

	/**
	 * 操作文件之追加文件内容
	 *
	 * @param content
	 *            追加的内容
	 * @param filename
	 *            追加的文件
	 * @param charsetName
	 *            编码 尽量使用utf-8
	 */
	public static void writerContentToFile(String content, String filename,
										   String charsetName) {
		try {
			// "r", "rw", "rws", or "rwd"
			RandomAccessFile randomAccessFile = new RandomAccessFile(filename,
					"rw");
			long fileLength = randomAccessFile.length();// 获取文件字节数。
			randomAccessFile.seek(fileLength);// 将文件指针移动文件的尾部
			randomAccessFile.write(content.getBytes(charsetName));
			randomAccessFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 操作文件之追加文件内容
	 *
	 * @param content
	 *            追加的内容
	 * @param filename
	 *            追加的文件
	 * @param flag
	 *            是否追加，false标识：覆盖之前的内容,true是追加
	 */
	public static void writerContentToFile(String content, String filename,
										   boolean flag) {
		try {
			FileWriter writer = new FileWriter(filename, flag);
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将一个文件的内容复制给另一个文件
	 *
	 * @param srcFile
	 * @param destFile
	 * @param encoding
	 * @param isAppend
	 */
	public static void writerFileLinesToFile(File srcFile, File destFile,
											 String encoding, boolean isAppend) {
		try {
			FileInputStream inputStream = new FileInputStream(srcFile);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream, encoding));
			FileOutputStream outputStream = new FileOutputStream(destFile,
					isAppend);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					outputStream, encoding);
			BufferedWriter writer = new BufferedWriter(outputStreamWriter);
			String line;
			while ((line = reader.readLine()) != null) {
				writer.write(line);
			}
			writer.close();
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writerFileLinesToFile(String srcFile, String destFile,
											 String encoding, boolean isAppend) {
		writerFileLinesToFile(new File(srcFile), new File(destFile), encoding,
				isAppend);
	}



	public static String getUUIDFileName(String fileName){
		int position = fileName.lastIndexOf(".");
		String extension = fileName.substring(position);
		return getUUID()+extension;
	}

	public static String getUUID(){
		String s = UUID.randomUUID().toString();
		return s.substring(0,8)+s.substring(9,13)+s.substring(14,18)+s.substring(19,23)+s.substring(24);
	}

	// 为上传文件自动分配文件名称，避免重复
	public static String getSmallFileName(String smallFileName,String fileName) {
		return "small_"+smallFileName;
	}

	// 为上传文件自动分配文件名称，避免重复
	public static String generateFileName(String fileName,int randomNum,String datePattern) {
		// 获得当前时间
		DateFormat format = new SimpleDateFormat(datePattern);
		// 转换为字符串
		String formatDate = format.format(new Date());
		// 随机生成文件编号
		int random = new Random().nextInt(randomNum);
		// 获得文件后缀名称
		int position = fileName.lastIndexOf(".");
		String extension = fileName.substring(position);
		// 组成一个新的文件名称
		return formatDate + random + extension;
	}

	// 为上传文件自动分配文件名称，避免重复
	public static String generateDirName(String fileName) {
		// 获得当前时间
		DateFormat format = new SimpleDateFormat("HHmmss");
		// 转换为字符串
		String formatDate = format.format(new Date());
		// 随机生成文件编号
		int random = new Random().nextInt(10000);
		// 组成一个新的文件名(目录名)
		return "exmay_"+formatDate + random ;
	}


	public static String readFileByChars(String fileName, String encoding) {
		return readFileByChars(new File(fileName), encoding);
	}

	public static String readFileByChars(File fileName, String encoding) {
		StringBuffer buffer = new StringBuffer();
		Reader read = null;
		try {
			char[] tempchars = new char[30];
			int charread = 0;
			read = new InputStreamReader(new FileInputStream(fileName),
					encoding);
			while ((charread = read.read(tempchars)) != -1) {
				if ((charread == tempchars.length)
						&& (tempchars[tempchars.length - 1] != 'r')) {
					System.out.print(tempchars);
				} else {
					for (int i = 0; i < charread; i++) {
						if (tempchars[i] == 'r') {
							continue;
						} else {
							buffer.append(tempchars[i]);
						}
					}
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (read != null) {
				try {
					read.close();
				} catch (IOException e1) {
				}
			}
		}
		return buffer.toString();
	}

	public static String readFileByLines(String fileName) {
		File file = new File(fileName);
		BufferedReader reader = null;
		StringBuffer buffer = new StringBuffer();
		try {
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				buffer.append(tempString + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
		return buffer.toString();
	}

	@SuppressWarnings("unused")
	private static void showAvailableBytes(InputStream in) {// 显示输入流中还剩的字节数
		try {
			System.out.println("当前字节输入流中的字节数为:" + in.available());
		} catch (IOException e) {// 捕获异常
			e.printStackTrace();
		}
	}

	public static String readFile(File file) {
		StringBuffer buffer = new StringBuffer();
		try {
			FileInputStream inputStream = new FileInputStream(file);
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream);
			BufferedReader reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				buffer.append(tempString + "\n");
			}
			reader.close();
		} catch (Exception e) {
		}
		return buffer.toString();
	}

	public static String readFile(String filename) {
		return readFile(new File(filename));
	}

	public static String readFilesByLine(File[] files, String encoding) {
		StringBuffer buffer = new StringBuffer();
		for (File file : files) {
			buffer.append(readFileByLine(file, encoding));
		}
		return buffer.toString();
	}

	public static String readFilesByLine(String[] files, String encoding) {
		StringBuffer buffer = new StringBuffer();
		for (String pathname : files) {
			File file = new File(pathname);
			buffer.append(readFileByLine(file, encoding));
		}
		return buffer.toString();
	}

	public static boolean isExist(String filePath){
		File file = new File(filePath);
		return file.exists();
	}



	//-------------TmFileUtil----------end
	protected final static Logger log = LoggerFactory.getLogger(FileUtil.class);

	/**
	 * 获得一个目录以及所有子目录下某类文件的路径集合
	 * @param dirPath
	 * @param filter
	 * @return
	 */
	public static List<String> getFilesPathByFilter(String dirPath, FilenameFilter filter){

		return getFilesPathByFilter(new File(dirPath),filter);
	}

	public static List<String> getFilesPathByFilter(File dir, FilenameFilter filter){
		List<String> list=null;
		list =getFilterFilesPath(dir, filter);//获得本目录下文件路径
		//File[] fs =dir.listFiles(filter);
		File[] fs =null;
		fs =filter==null?dir.listFiles():dir.listFiles(filter);
		for (File t : fs) {
			if (t.isDirectory()) {
				String tpath = t.getAbsolutePath();
				//迭代调用本方法
				List<String> temp = getFilesPathByFilter(new File(tpath), filter);
				if(temp!=null)
					list.addAll(temp);
			}
		}
		return list;
	}
	public static String countFileSize(long fileSize) {
		String fileSizeString = "";
		try {
			DecimalFormat df = new DecimalFormat("#.00");
			long fileS = fileSize;
			if (fileS == 0) {
				fileSizeString = "0KB";
			} else if (fileS < 1024) {
				fileSizeString = df.format((double) fileS) + "B";
			} else if (fileS < 1048576) {
				fileSizeString = df.format((double) fileS / 1024) + "KB";
			} else if (fileS < 1073741824) {
				fileSizeString = df
						.format(((double) fileS / 1024 / 1024) - 0.01)
						+ "MB";
			} else {
				fileSizeString = df.format((double) fileS / 1024 / 1024 / 1024)
						+ "G";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileSizeString;
	}
	/**
	 * 将文件相关信息封装到实体类中
	 * 
	 * @param file
	 * @throws IOException
	 */
	public static FileInfo getFileInfo(File file) throws IOException {
		FileInfo info = null;
		if (file.exists()) {
			info = new FileInfo();
			info.setAbsolute(file.isAbsolute());
			info.setAbsolutePath(file.getAbsolutePath());
			info.setCanonicalPath(file.getCanonicalPath());
			info.setDir(file.isDirectory());
			info.setFile(file.isFile());
			info.setLastModified(DateUtil.formatDatetime(file.lastModified(), "yyyy年MM月dd日 HH:mm:mm"));
			String name = file.getName();
			info.setName(name);
			info.setParentPath(file.getParent());
			info.setPath(file.getPath());
			info.setSize(countFileSize(file.length()));
			if (file.isFile()) {
				info.setSize(countFileSize(file.length()));
				info.setSuffix(file.getPath().substring(file.getPath().lastIndexOf(".")));
			}
		} else {
			try {
				throw new RuntimeException("指定的文件不存在！");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return info;
	}

	/**
	 * 创建文件夹
	 * 
	 * @param file
	 */
	public static void createDir(File file) {
		if (!file.exists()&&file.isDirectory()) {
			file.mkdirs();
			log.info("create dir [" + file.getPath() + "] success!");
		}
	}

	/**
	 * 创建文件夹
	 * 
	 * @param path
	 */
	public static void createDir(String path) {
		File file = new File(path);
		if (!file.exists()) {
			new File(path).mkdirs();
			log.info("create dir [" + file.getPath() + "] success!");
		}
	}

	/**
	 * 创建文件
	 * @param path
	 */
	public static boolean createFile(String path) {
		return createFile(new File(path));
	}

	/**
	 * 创建文件.如果文件是路径或者已经存在不执行该方法。 如果该文件父路径不存在，会自动创建父路径。
	 * 
	 * @param file
	 */
	public static boolean createFile(File file) {
		try {
			if (!file.exists()) {
			String p = file.getParent();
			createDir(file.getParent());
				file.createNewFile();
				log.info("create file [" + file.getPath() + "] 成功！");
			}
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除文件(夹)
	 * @param path
	 * @return boolean
	 */
	public static boolean deleteFile(String path) {
		return deleteFile(new File(path));
	}

	/**
	 * 删除文件(夹)
	 * @param file
	 * @return boolean
	 */
	public static boolean deleteFile(File file) {
		boolean result = false;
		try {
			if (file.exists()) {//文件存在
				if (file.isDirectory()) {//文件是个目录
					File[] fs = file.listFiles();
					for (File tempFile : fs) {
						if (tempFile.isDirectory()) {
							deleteFile(tempFile);
							tempFile.delete();
						} else {
							tempFile.delete();
							log.info("delete file [" + tempFile.getPath()+ "] success!");
						}
					}
					file.delete();
					log.info("delete dir [" + file.getPath()+ "] success!");
				} else {
					file.delete();
					log.info("delete file [" + file.getPath() + "] success!");
				}
			}
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("delete dir [" + file.getPath() + "] failure!");
		}
		return result;
	}

	
	/**
	 * 我的文件过滤器过滤规则
	 * 
	 * @author Administrator
	 * 
	 */
	static class MyFileNameFilterTypes {
		static final String NAME_EQUALS = "NAME_EQUALS";
		static final String NAME_LIKE = "NAME_LIKE";
		static final String NAME_ENDWITH = "NAME_ENDWITH";
		static final String DIR = "DIR";
	}

	/**
	 * 我的文件过滤器.包含3中文件筛选规则:name_equals、name_like、name_endwith.
	 * 使用这个过滤器，可以根据用户指定的过滤规则和value筛选出文件
	 * 
	 * @author wujiyue
	 */
	static class MyFileNameFilter implements FilenameFilter {
		// 我的文件过滤器过滤规则。默认为 NAME_ENDWITH
		private String myFileNameFilterType;
		// 过滤规则值
		private String value;

		/**
		 * 一个参数的构造方法.
		 * 
		 * @param value
		 */
		public MyFileNameFilter(String value) {
			this.value = value;
			this.myFileNameFilterType = MyFileNameFilterTypes.NAME_ENDWITH;
		}

		/**
		 * 两个参数构造方法
		 * 
		 * @param myFileNameFilterType
		 * @param value
		 */
		public MyFileNameFilter(String myFileNameFilterType, String value) {

			this.value = value;
			this.myFileNameFilterType = myFileNameFilterType;
		}

		@Override
		public boolean accept(File dir, String name) {
			boolean result = false;
			if (MyFileNameFilterTypes.NAME_ENDWITH.equals(myFileNameFilterType)) {
				if (name.endsWith(value))
					result = true;
			} else if (MyFileNameFilterTypes.NAME_EQUALS
					.equals(myFileNameFilterType)) {
				if (name.equals(value))
					result = true;
			} else if (MyFileNameFilterTypes.NAME_LIKE
					.equals(myFileNameFilterType)) {
				if (name.indexOf(value) >= 0)
					result = true;
			}else if (MyFileNameFilterTypes.DIR
					.equals(myFileNameFilterType)) {
				if (dir.isDirectory())
					result = true;
			}
			return result;
		}

	}

	/**
	 * 获得本目录下文件路径集合
	 * 
	 * @param dirPath
	 *            目录路径
	 * @param filenameFilter
	 *            文件名过滤器
	 * @return List<String>
	 */
	public static List<String> getFilterFilesPath(String dirPath,
			FilenameFilter filenameFilter) {
	return getFilterFilesPath(new File("dirPath"),filenameFilter);
	}
	public static List<String> getFilterFilesPath(File dir,
												  FilenameFilter filenameFilter) {
		List<String> list = null;
		try {
			File[] files =null;
			if (dir.isDirectory()) {// 是目录，便利它所有文件
				if(filenameFilter!=null)
					files = dir.listFiles(filenameFilter);
				else
					files = dir.listFiles();
				if (files.length > 0) {
					list = new ArrayList<String>();
					for (File f : files) {
						//if(f.isFile())//确保集合里面都是文件路径,而没有目录路径
						list.add(f.getPath());
					}
				}
			} else {
				throw new RuntimeException("该路径不存在或者这不是一个目录!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	/**
	 * 获取本目录下文件路径集合
	 * 
	 * @param dirPath
	 * @param types
	 * @return
	 */
	public static List<String> getFilesPathByTypes(String dirPath,String... types) {
		if(types!=null) {
			FilenameFilter filter = new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return StringUtil.toList(types).stream().anyMatch(s -> {
						if (name.toLowerCase().endsWith(s.toLowerCase())) return true;
						else return false;
					});
				}
			};
			return  getFilterFilesPath(dirPath, filter);
		}
		return getFilterFilesPath(dirPath, null);
	}


	/**
	 * 文件的复制 如果目标文件是一个目录，则利用该目录和源文件的文件名称创建出目标文件然后再复制
	 * 
	 * @param src
	 *            源文件
	 * @param tar
	 *            目标文件或者目录
	 * @return boolean
	 */
	public static boolean copyFile(File src, File tar) {
		boolean result = false;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			if (tar.isDirectory()) {// 目标是个目录
				String fileName = src.getName();
				String tarDirPath = tar.getAbsolutePath();
				String tarFilePath = tarDirPath + File.separator + fileName;
				File tarFile = new File(tarFilePath);
				fis = new FileInputStream(src);
				fos = new FileOutputStream(tarFile);
				byte[] bytes = new byte[1024]; // 1kb
				int temp = 0;
				while ((temp = fis.read(bytes)) != -1) {
					// 将byte数组中的内容直接写入对应的目录中 就是实现了文件的粘贴功能
					fos.write(bytes, 0, temp);
				}
			}
			if (!tar.exists()) {// 目标是个文件
				// 如果目标文件不存在
				createFile(tar);
				fis = new FileInputStream(src);
				fos = new FileOutputStream(tar);
				byte[] bytes = new byte[1024]; // 1kb
				int temp = 0;
				while ((temp = fis.read(bytes)) != -1) {
					// 将byte数组中的内容直接写入对应的目录中 就是实现了文件的粘贴功能
					fos.write(bytes, 0, temp);
				}
			}
			result = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					// 刷新输出流
					fos.flush();
					// 关闭输入输出流
					fos.close();
				}
				if (fis != null)
					fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 文件的复制
	 * 
	 * @param srcPath
	 *            源文件路径
	 * @param tarPath
	 *            目标文件路径或者目录路径
	 * @return boolean
	 */
	public static boolean copyFile(String srcPath, String tarPath) {
		return copyFile(new File(srcPath), new File(tarPath));
	}

	/**
	 * 文件夹复制
	 * 
	 * @param sourceDir
	 *            源目录
	 * @param targetDir
	 *            目标目录
	 */
	public static void copyDirectiory(File sourceDir, File targetDir) {
		// 新建目标目录
		if (!targetDir.exists()) {
			targetDir.mkdirs();
		}
		// 获取源文件夹当前下的文件或目录
		File[] file = sourceDir.listFiles();
		for (int i = 0; i < file.length; i++) {
			if (file[i].isFile()) {
				// 源文件
				File sourceFile = file[i];
				// 目标文件
				File targetFile = new File(targetDir.getAbsolutePath()
						+ File.separator + file[i].getName());
				System.out.println("正在复制文件" + sourceFile.getAbsolutePath());
				copyFile(sourceFile, targetFile);
			}
			if (file[i].isDirectory()) {
				// 准备复制的源文件夹
				String dir1 = sourceDir.getAbsolutePath() + "\\"
						+ file[i].getName();
				// 准备复制的目标文件夹
				String dir2 = targetDir.getAbsolutePath() + "\\"
						+ file[i].getName();
				System.out.println("正在复制文件夹" + dir1);
				copyDirectiory(new File(dir1), new File(dir2));
			}
		}
	}

	/**
	 * 文件夹复制.sourceDirPath里面的内容复制一份到targetDirPath文件夹里面.而sourceDirPath文件夹本身不包含在内.
	 * 
	 * @param sourceDirPath
	 *            源文件夹路径
	 * @param targetDirPath
	 *            目标文件夹路径
	 */
	public static void copyDirectiory(String sourceDirPath, String targetDirPath) {
		copyDirectiory(new File(sourceDirPath), new File(targetDirPath));
	}

	/**
	 * 文件或者文件夹移动到目标目录中去 复制一份到目标文件夹，然后删除源文件
	 * 
	 * @param src
	 * @param tar
	 * @return boolean
	 */
	public static boolean move(File src, File tar) {
		boolean result = false;
		if (!tar.exists())
			createDir(tar);
		if (tar.isDirectory() && src.exists()) {// 源文件存在并且目标文件是一个目录才能执行以下操作
			if (!tar.exists()) {
				// 目标文件夹不存在就创建它
				createDir(tar);
			}
			if (src.isFile()) {// 如果源文件是个文件
				// 复制文件到目标目录
				copyFile(src, tar);
				// 删除源文件
				deleteFile(src);
			}
			if (src.isDirectory()) {// 如果源文件是个目录,那么连同文件夹以及内容复制到目标目录中去.
				// 源文件夹名称
				String srcDirName = src.getName();
				// 利用源文件夹名称和目标目录路径创建新路径
				String tarDirPath = tar.getAbsolutePath() + File.separator
						+ srcDirName;
				File tarDir = new File(tarDirPath);
				if (!tarDir.exists()) {// 目标路径不存在
					// 创建目标文件夹
					createDir(tarDir);
				}
				// 复制文件夹内容
				copyDirectiory(src, tarDir);
				// 删除源文件夹
				deleteFile(src);
			}
			result = true;
		} else {
			try {
				throw new Exception("源文件不存在或者目标文件不是一个目录!");
			} catch (Exception e) {

				e.printStackTrace();
			}
			result = false;
		}
		return result;
	}

	/**
	 * 文件或者文件夹的移动 复制一份到目标文件夹，然后删除源文件
	 * 
	 * @param srcPath
	 *            源文件或者源文件夹路径
	 * @param tarPath
	 *            目标文件夹路径
	 */
	public static boolean move(String srcPath, String tarPath) {
		return move(new File(srcPath), new File(tarPath));
	}

	/**
	 * 读取文件内容存储到String返回
	 * 
	 * @param filePath
	 *            文件路径
	 * @param charset
	 *            源文件字符集,如果为null或者""就使用默认字符集
	 * @return String 文件内容
	 */
	public static String readFileByLine(String filePath, String charset) {
		return readFileByLine(new File(filePath), charset);
	}

	/**
	 * 读取文件内容并返回,每一行内容根据regexs匹配成功则剔除该行
	 * 
	 * @param filePath
	 *            文件路径
	 * @return String 文件内容
	 */
	public static String readFileByLine(String filePath, String[] regexs) {
		return readFileByLine(new File(filePath), regexs);
	}

	/**
	 * 读取文件内容存储到String返回
	 * 
	 * @param file
	 *            要读的文件
	 * @param charset
	 *            源文件字符集,如果为null或者""就使用默认字符集
	 * @return String 文件内容
	 */
	public static String readFileByLine(File file, String charset) {
		InputStreamReader isr = null;
		StringBuffer sb = new StringBuffer();
		try {
			if (StringUtil.notEmpty(charset))
				isr = new InputStreamReader(new FileInputStream(file), charset);
			else
				isr = new InputStreamReader(new FileInputStream(file));
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				// if (!"".equals(line))// 这种写法不全面,换下面的
				if (!StringUtil.isMatch(line, "", "\\s+"))// 默认地把空行剔除
					sb.append(line).append("\r\n");
			}
			br.close();
			isr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * 读取文件内容并返回,每一行内容根据regexs匹配成功则剔除该行
	 * 
	 * @param file
	 *            要读的文件
	 * @param regexs
	 *            剔除规则
	 * @return String 返回内容
	 */
	public static String readFileByLine(File file, String[] regexs) {
		InputStreamReader isr = null;
		StringBuffer sb = new StringBuffer();
		try {
			isr = new InputStreamReader(new FileInputStream(file));
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				if (!StringUtil.isMatch(line, regexs))// 判断是否把该行剔除
					sb.append(line).append("\r\n");
			}
			br.close();
			isr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	/**
	 * 获得一个文件行数
	 * @param file
	 * @param regexs 过滤规则
	 * @return
	 */
	public static int getFileLineNum(File file, String[] regexs) {
		InputStreamReader isr = null;
		int num=0;
		try {
			isr = new InputStreamReader(new FileInputStream(file));
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				if (!StringUtil.isMatch(line, regexs))// 判断是否把该行剔除
					num++;
			}
			br.close();
			isr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return num;
	}
	/**
	 * 将String内容写入到文件中
	 * 
	 * @param content
	 * @param file
	 * @return
	 */
	public static boolean writeContentToFile(String content, File file) {
		PrintWriter writer = null;
		boolean r = false;
		try {
			writer = new PrintWriter(new FileOutputStream(file));
			writer.print(content);
			r = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		}
		return r;
	}

	/**
	 * 将内容写入到文件中
	 * 
	 * @param content
	 *            要写入的内容
	 * @param filePath
	 *            要写入的文件路径
	 * @return
	 */
	public static boolean writeContentToFile(String content, String filePath) {
		File f = new File(filePath);
		createFile(new File(filePath));// 如果写入的文件不存在,会创建
		return writeContentToFile(content, f);
	}


	/**
	 * 压缩（java）文件内容
	 * 
	 * @param file
	 *            要处理的文件
	 * @param type
	 *            内容压缩类型,0-只压缩空白行.1-也压缩掉代码注释内容
	 * @return
	 */
	public static final String REGEX_BLANK_LINE="\\s*";
	// 匹配java注释行的正则表达式
	public static final String[] REGEX_JAVA_NOTE ={ "", "\\s*/\\*+\\s*",
			"\\s*\\*/\\s*", "\\s*\\*\\s*",
			"\\s*\\*(\\s*\\S*\\s*)*",
			"\\s*/{2,}?(\\s*\\S*\\s*)*", "\\s*" };
	public static boolean compressFileContent(File file, int type) {
		try {
				String content = "";
				if (type == 0) {// 只压缩掉空白行
					// 读取内容
					content = readFileByLine(file,REGEX_BLANK_LINE);// 读取内容的过程中默认已经剔除了空白行
				}
				if (type == 1) {
					// 匹配java注释行的正则表达式
					/*String[] regexs ={ "", "\\s*//*\\*+\\s*",
							"\\s*\\*//*\\s*", "\\s*\\*\\s*",
							"\\s*\\*(\\s*\\S*\\s*)*",
							"\\s*//*{2,}?(\\s*\\S*\\s*)*", "\\s+" };*/
					content = readFileByLine(file, REGEX_JAVA_NOTE);
				}
				// 将内容写入原来的文件
				writeContentToFile(content, file);
				return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 压缩java文件
	 */
	public static void compressJavaFile(File file, int type) {

		if(file.isDirectory()){
			File[] fs=	file.listFiles();
			for(File a:fs){
				compressJavaFile(a,type);
			}

		}
		if(file.isFile()){
			if(file.getName().endsWith(".java")){
			boolean b=	compressFileContent(file,1);
				if(b){
					System.out.println("压缩java文件"+file.getName()+"成功！");
				}
			}
		}
	}
	/**
	 * 将file文件中的所有的from字符串替换为to字符串
	 * @param file
	 * @param from
	 * @param to
	 * @return
	 */
	public static boolean replaceFileContent(File file,String charset,String from,String to) {
		String temp=readFileByLine(file,charset);
		temp=temp.replaceAll(from, to);
		return writeContentToFile(temp, file);
	}

	/**
	 * 按照行的方式读取文件，每一行都根据filter判断，符合就剔除改行
	 * @param file
	 * @param filter
	 * @return
	 */
	public static String readFileByLine(File file,Predicate filter){

		InputStreamReader isr = null;
		StringBuffer sb = new StringBuffer();
		try {
			isr = new InputStreamReader(new FileInputStream(file));
			BufferedReader br = new BufferedReader(isr);
			String line = null;

			while ((line = br.readLine()) != null) {
				if (!filter.test(line))// 判断是否把该行剔除
					sb.append(line).append("\r\n");
			}
			br.close();
			isr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static void main(String[] args) throws IOException {
		String basePath="C:\\Users\\Administrator\\IdeaProjects-SpringBoot\\dzd-build\\dzd\\src\\main\\resources\\templates";
		//compressJavaFile(new File("D:\\ip1"),1);
		File[] files=getFolders(basePath);
		for(File f:files){
			System.out.println(f.getName());
		}
	}
}
