package com.markbro.base.utils.ip;
/**
 * 一条IP的范围记录,不仅包括ip的国家和地区,而且包括起始ip和结束ip
 * @author wjy
 *
 */
public class IPEntry {
	public String beginIp;
	public String endIp;
	public String country;
	public String area;
	public IPEntry(){
		beginIp=endIp=country=area="";
	}
}
