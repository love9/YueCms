package com.markbro.base.utils.file;

import java.io.File;

/**
 * 存储文件相关信息的实体类
 * @author wujiyue
 * @date 2014年8月8日 12:31:54
 * @version 1.0
 *
 */
public class FileInfo {
	/**
	 * 文件名
	 */
	private String name;
	private String size;
	//后缀(如：.txt)
	private String suffix;
	private String lastModified;
	//是文件？
	private boolean isFile;
	//是目录？
	private boolean isDir;
	//路径
	private String path;
	//绝对路径
	private String absolutePath;
	//规范路径名(是绝对路径的基础上去掉去系统相关的转义符)
	private String canonicalPath;
	//文件父路径
	private String parentPath;
	//判断文件getPath得到的路径是否为绝对路径
	private boolean isAbsolute;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public boolean isFile() {
		return isFile;
	}
	public void setFile(boolean isFile) {
		this.isFile = isFile;
	}
	public boolean isDir() {
		return isDir;
	}
	public void setDir(boolean isDir) {
		this.isDir = isDir;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getAbsolutePath() {
		return absolutePath;
	}
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	public String getCanonicalPath() {
		return canonicalPath;
	}
	public void setCanonicalPath(String canonicalPath) {
		this.canonicalPath = canonicalPath;
	}
	public String getParentPath() {
		return parentPath;
	}
	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}
	public boolean isAbsolute() {
		return isAbsolute;
	}
	public void setAbsolute(boolean isAbsolute) {
		this.isAbsolute = isAbsolute;
	}

	@Override
	public String toString() {
		return "FileInfo{" +
				"name='" + name + '\'' +
				", size='" + size + '\'' +
				", suffix='" + suffix + '\'' +
				", lastModified=" + lastModified +
				", isFile=" + isFile +
				", isDir=" + isDir +
				", path='" + path + '\'' +
				", absolutePath='" + absolutePath + '\'' +
				", canonicalPath='" + canonicalPath + '\'' +
				", parentPath='" + parentPath + '\'' +
				", isAbsolute=" + isAbsolute +
				'}';
	}

	public static void main(String[] args) throws Exception{
		String n="ddda.java";
		FileInfo f=FileUtil.getFileInfo(new File("F:\\ROOT.rar"));
		System.out.println(f.toString());
		System.out.println(n.substring(n.lastIndexOf(".")));
	}
}
