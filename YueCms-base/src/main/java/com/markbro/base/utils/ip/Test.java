package com.markbro.base.utils.ip;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;

/**
 * 测试类
 * @author wjy
 *
 */
public class Test {
	/**
	 * 获取类的class文件位置的URL
	 *
	 * @param cls
	 * @return
	 */
	private static URL getClassLocationURL(final Class<?> cls) {
		if (cls == null)
			throw new IllegalArgumentException("null input: cls");
		URL result = null;
		final String clsAsResource = cls.getName().replace('.', '/').concat(".class");
		final ProtectionDomain pd = cls.getProtectionDomain();
		if (pd != null) {
			final CodeSource cs = pd.getCodeSource();
			if (cs != null)
				result = cs.getLocation();
			if (result != null) {
				if ("file".equals(result.getProtocol())) {
					try {
						if (result.toExternalForm().endsWith(".jar") || result.toExternalForm().endsWith(".zip"))
							result = new URL("jar:".concat(result.toExternalForm()).concat("!/").concat(clsAsResource));
						else if (new File(result.getFile()).isDirectory())
							result = new URL(result, clsAsResource);
					} catch (MalformedURLException ignore) {
					}
				}
			}
		}
		if (result == null) {
			final ClassLoader clsLoader = cls.getClassLoader();
			result = clsLoader != null ? clsLoader.getResource(clsAsResource)
					: ClassLoader.getSystemResource(clsAsResource);
		}
		return result;
	}

	/** 初始化设置默认值 */
	public static final <K> K ifNull(K k, K defaultValue) {
		if (k == null) {
			return defaultValue;
		}
		return k;
	}
	/**
	 * 获取class文件所在绝对路径
	 *
	 * @param cls
	 * @return
	 * @throws IOException
	 */
	public static final String getPathFromClass(Class<?> cls) {
		String path = null;
		if (cls == null) {
			throw new NullPointerException();
		}
		URL url = getClassLocationURL(cls);
		if (url != null) {
			path = url.getPath();
			if ("jar".equalsIgnoreCase(url.getProtocol())) {
				try {
					path = new URL(path).getPath();
				} catch (MalformedURLException e) {
				}
				int location = path.indexOf("!/");
				if (location != -1) {
					path = path.substring(0, location);
				}
			}
			File file = new File(path);
			try {
				path = file.getCanonicalPath();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return path;
	}
	public static void main(String[] args) throws Exception{
		//指定纯真数据库的文件名，所在文件夹
		IPSeeker ip=IPSeeker.getInstance();
		//测试IP 58.20.43.13
		System.out.println(ip.getIPLocation("219.72.203.1").getCountry()+":"+ip.getIPLocation("219.72.203.1").getArea());
		System.out.println("缓存中记录数为:"+ip.getIpCache().size());
		System.out.println(ip.getIPLocation("112.232.16.49").getCountry()+":"+ip.getIPLocation("112.232.16.49").getArea());
		System.out.println("缓存中记录数为:"+ip.getIpCache().size());
		IPSeeker ip2=IPSeeker.getInstance();
		System.out.println(ip2.getIPLocation("58.20.43.13").getCountry()+":"+ip.getIPLocation("58.20.43.13").getArea());
		System.out.println("缓存中记录数为:"+ip2.getIpCache().size());
		System.out.println(ip.getIPLocation("112.229.111.190").getCountry()+":"+ip.getIPLocation("112.229.111.190").getArea());
		System.out.println("缓存中记录数为:"+ip.getIpCache().size());
	}

}
