package com.markbro.base.utils.string;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 字符串处理相关工作类
 * 
 * @author wujiyue
 * 
 */
public class StringUtil {
	// main
	public static void main(String[] args) {
		/*String regex = "\\{[0-9]\\}";// [//u4E00-//u9FFF]为汉字
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern
				.matcher("<result property='{0}' column='{1}' />");
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			sb.append(matcher.group());
		}
		System.out.println(sb);
		String str="<result property='{0}' column='{1}' />";
		List<String> list1=new ArrayList<String>();
		list1.add("name");
		list1.add("pwd");
		list1.add("age");
		List<String> list2=new ArrayList<String>();
		list2.add("t_name");
		list2.add("t_pwd");
		list2.add("t_age");
		List[] list={list1,list2};
		list1=completeList(str, list);
//		printList(list1);
		System.out.println(listToString(list1, "\n"));*/
		/*System.out.println(getFileType("%2Fimages%2F%E4%BD%A09.jpg"));
		System.out.println(URLDecoder.decode(URLEncoder.encode("/images/你9.jpg")));*/
		/*List<String> list= Lists.newArrayList(null,"ss","ss","aa",null);
		list.stream().distinct().filter(s->{return  s!=null;}).forEach(s->{
			System.out.println(s);
		});*/
		System.out.println(subEndStr("/login.jsp",".jsp"));
	}
	//如果list中还有targetStr返回true
	public static boolean isContains(String targerStr,List<String> list){
		for(String s:list){
			if(s.equals(targerStr)){
				return true;
			}
		}
		return false;
	}

	//如果原来的str是以endStr结尾的，去掉endStr
	public  static String subEndStr(String str,String endStr){
		if(str.endsWith(endStr)){
			str=str.substring(0,str.lastIndexOf(endStr));
		}
		return str;
	}
	public static String isNull(String value){
		if(value == null || value.isEmpty()
				|| value.equals("[]") || value.equalsIgnoreCase("null")
				|| value.equalsIgnoreCase("undefined")){
			return "";
		}else{
			return value;
		}
	}
	/**
	 * 如果target不为空返回target,否则返回一个默认值
	 * @param target
	 * @param defaultValue
	 * @return
	 */
	public static String assertNotNullOrEmpty(String target,String defaultValue){
		if(notEmpty(target)){
			return target;
		}else{
			return defaultValue;
		}
	}
	/**
	 * 将属性样式字符串转成驼峰样式字符串<br>
	 * (例:branchNo -> branch_no)<br>
	 *
	 * @param inputString
	 * @return
	 */
	public static String toUnderlineString(String inputString) {
		if (inputString == null)
			return null;
		StringBuilder sb = new StringBuilder();
		boolean upperCase = false;
		for (int i = 0; i < inputString.length(); i++) {
			char c = inputString.charAt(i);

			boolean nextUpperCase = true;

			if (i < (inputString.length() - 1)) {
				nextUpperCase = Character.isUpperCase(inputString.charAt(i + 1));
			}

			if ((i >= 0) && Character.isUpperCase(c)) {
				if (!upperCase || !nextUpperCase) {
					if (i > 0)
						sb.append('_');
				}
				upperCase = true;
			} else {
				upperCase = false;
			}

			sb.append(Character.toLowerCase(c));
		}

		return sb.toString();
	}

	/**
	 * 将驼峰字段转成属性字符串<br>
	 * (例:branch_no -> branchNo )<br>
	 *
	 * @param inputString
	 *            输入字符串
	 * @return
	 */
	public static String toCamelCaseString(String inputString) {
		return toCamelCaseString(inputString, false);
	}

	/**
	 * 将驼峰字段转成属性字符串<br>
	 . * (例:branch_no -> branchNo )<br>
	 *
	 * @param inputString
	 *            输入字符串
	 * @param firstCharacterUppercase
	 *            是否首字母大写
	 * @return
	 */
	public static String toCamelCaseString(String inputString, boolean firstCharacterUppercase) {
		if (inputString == null)
			return null;
		StringBuilder sb = new StringBuilder();
		boolean nextUpperCase = false;
		for (int i = 0; i < inputString.length(); i++) {
			char c = inputString.charAt(i);
			switch (c) {
				case '_':
				case '-':
				case '@':
				case '$':
				case '#':
				case ' ':
				case '/':
				case '&':
					if (sb.length() > 0) {
						nextUpperCase = true;
					}
					break;
				default:
					if (nextUpperCase) {
						sb.append(Character.toUpperCase(c));
						nextUpperCase = false;
					} else {
						sb.append(Character.toLowerCase(c));
					}
					break;
			}
		}
		if (firstCharacterUppercase) {
			sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
		}
		return sb.toString();
	}

	/**
	 * 得到标准字段名称
	 *
	 * @param inputString
	 *            输入字符串
	 * @return
	 */
	public static String getValidPropertyName(String inputString) {
		String answer;
		if (inputString == null) {
			answer = null;
		} else if (inputString.length() < 2) {
			answer = inputString.toLowerCase(Locale.US);
		} else {
			if (Character.isUpperCase(inputString.charAt(0)) && !Character.isUpperCase(inputString.charAt(1))) {
				answer = inputString.substring(0, 1).toLowerCase(Locale.US) + inputString.substring(1);
			} else {
				answer = inputString;
			}
		}
		return answer;
	}

	/**
	 * 将属性转换成标准set方法名字符串<br>
	 *
	 * @param property
	 * @return
	 */
	public static String getSetterMethodName(String property) {
		StringBuilder sb = new StringBuilder();
		sb.append(property);
		if (Character.isLowerCase(sb.charAt(0))) {
			if (sb.length() == 1 || !Character.isUpperCase(sb.charAt(1))) {
				sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
			}
		}
		sb.insert(0, "set");
		return sb.toString();
	}

	/**
	 * 将属性转换成标准get方法名字符串<br>
	 *
	 * @param property
	 * @return
	 */
	public static String getGetterMethodName(String property) {
		StringBuilder sb = new StringBuilder();
		sb.append(property);
		if (Character.isLowerCase(sb.charAt(0))) {
			if (sb.length() == 1 || !Character.isUpperCase(sb.charAt(1))) {
				sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
			}
		}
		sb.insert(0, "get");
		return sb.toString();
	}

	/**
	 * 返回资源的二进制数据
	 * @param inputStream
	 * @return
	 */
	public static byte[] readInputStream(InputStream inputStream) {

		// 定义一个输出流向内存输出数据
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// 定义一个缓冲区
		byte[] buffer = new byte[1024];
		// 读取数据长度
		int len = 0;
		// 当取得完数据后会返回一个-1
		try {
			while ((len = inputStream.read(buffer)) != -1) {
				// 把缓冲区的数据 写到输出流里面
				byteArrayOutputStream.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				byteArrayOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		// 得到数据后返回
		return byteArrayOutputStream.toByteArray();

	}
	/**
	 * 用来去掉List中空值和相同项的。
	 * @param list
	 * @return
	 */
	public static List<String> removeNullAndSameItem(List<String> list) {
		/*List<String> difList = new ArrayList<String>();
		for (String t : list) {
			if (t != null && !difList.contains(t)) {
				difList.add(t);
			}
		}
		return difList;*/
		return  list.stream().distinct().filter(s->{return  s!=null;}).collect(Collectors.toList());
	}
	/**
	 * 根据一个路径获得文件类型
	 * @param path
	 * @return
	 */
	public static String getFileType(String path){
		if(path!=null) {
			int index=path.lastIndexOf(".");
			if (index>= 0) {
				return path.substring(index+1);
			}
		}
		return null;
	}
	/**
	 * 根据一个路径获得文件名
	 * @param path
	 * @return
	 */
	public static String getFileName(String path){
		if(path!=null){
			if(path.indexOf("/")>=0){
				return path.substring(path.lastIndexOf("/")+1);
			}
			if(path.indexOf("%2F")>=0){
				return URLDecoder.decode(path.substring(path.lastIndexOf("%2F")+3));
			}
		}
		return null;
	}
	/**
	 * 过滤特殊字符
	 * @param content
	 * @return
	 */
	public   static   String filterSpecial(String content){
		Pattern   p   =   Pattern.compile(REGEX_SPECIAL);
		Matcher   m   =   p.matcher(content);
		return   m.replaceAll("").trim();
	}

	public static final String REGEX_SIMPLE_CHINESE="[\\u4e00-\\u9fa5]+";//简体中文
	public static final String REGEX_CHINESE="[\u4E00-\u9FA5\uFE30-\uFFA0]+";//简繁中文
	public static final String REGEX_SPECIAL="[`~!@#$%^&*()+=|{}':;',//[//].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
	/**
	 * String数组转List
	 * @param strs
	 * @return
	 */
	public static List<String> toList(String... strs){
		List<String> list=null;
		if(strs!=null&&strs.length>0){
			list=new ArrayList<String>();
			for(String s:strs){
				list.add(s);
			}
			return list;
		}
		return null;
	}
	/**
	 * 格式化小数，保留n位小数
	 * 
	 * @param num
	 * @param n
	 * @return
	 */
	public static String formatNum(Object num, int n) {
		String partern = ".0";
		partern = rapRight(partern, n + 1, '0');
		DecimalFormat df = new DecimalFormat(partern);
		return df.format(num);
	}

	/**
	 * 如果target字符串长度不足len,在左边补充ch直到长度达到len
	 * 
	 * @param target
	 * @param len
	 * @param c
	 * @return
	 */
	public static String rapLeft(String target, int len, char c) {
		StringBuffer sb = new StringBuffer();
		if (target != null) {
			int n = target.length();
			if (n >= len) {
				return target;
			} else {
				int num = len - n;// 需要补充的字符数
				for (int i = 0; i < num; i++) {
					sb.append(c);
				}
				sb.append(target);
			}
		}
		return sb.toString();
	}

	/**
	 * 如果target字符串长度不足len,在右边补充ch直到长度达到len
	 * 
	 * @param target
	 * @param len
	 * @param c
	 * @return
	 */
	public static String rapRight(String target, int len, char c) {
		StringBuffer sb = new StringBuffer();
		if (target != null) {
			int n = target.length();
			if (n >= len) {
				return target;
			} else {
				sb.append(target);
				int num = len - n;// 需要补充的字符数
				for (int i = 0; i < num; i++) {
					sb.append(c);
				}

			}
		}
		return sb.toString();
	}

	/**
	 * 格式化数字
	 * 
	 * @param num
	 * @param partern
	 *            如#.0000， 00.000 ，.000000
	 * @return
	 */
	public static String formatNumber(Object num, String partern) {
		DecimalFormat df = new DecimalFormat(partern);
		return df.format(num);
	}

	/**
	 * 对list中每一个字符串执行wrapString函数 用传说中的java8的回调函数会不会让代码抽象度和通用度更高？
	 * 
	 * @param list
	 * @param left
	 * @param right
	 * @return
	 */
	public static List<String> wrapList(List<String> list, String left,
			String right) {
		String temp = null;
		List<String> result = new ArrayList<String>();
		for (String s : list) {
			temp = wrapString(left, s, right);
			result.add(temp);
		}
		return result;
	}

	/**
	 * 将字符串target用left和right字符串包裹起来，三个参数都不能为null
	 * 
	 * @param target
	 * @param left
	 * @param right
	 * @return
	 */
	public static String wrapString(String left, String target, String right) {

		return left.concat(target.concat(right));

	}

	/**
	 * 对module字符串的{}位置用list[]中元素填充
	 * 
	 * @param module
	 *            例如“ <result property="{0}" column="{1}" />”
	 * @param list
	 * @return
	 */
	public static List<String> completeList(String module, List<String>... list) {
		String regex = "\\{[0-9]\\}";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern
				.matcher(module);
		List<String> res=new ArrayList<String>();
		int size=list[0].size();
		for(int i=0;i<size;i++){
			res.add(module);
		}
		List<String> temp=new ArrayList<String>();
		while (matcher.find()) {
			temp.add(matcher.group());
		}
		
		//检查参数。检查在module找到的{}与传入list个数是否相等
		if(temp.size()==list.length){
			
			for(int i=0;i<list.length;i++){
				res=replaceList(list[i], null, res, "\\{"+i+"\\}");
			}
		}else{
			try {
				throw new Exception("参数module里的占位符与list个数不相等！");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res;

	}
	/**
	 * moduleList中的每一个字符串中的regex位置内容用（tarList-excludList）中对应位置的字符串替换
	 * @param tarList
	 * @param excludList
	 * @param moduleList
	 * @param regex
	 * @return
	 */
	public static List<String> replaceList(List<String> tarList,List<String> excludList,List<String> moduleList,String regex){
		if(excludList!=null){
			tarList.removeAll(excludList);
		}
		List<String> res=new ArrayList<String>();
		int tarLen=tarList.size();
		int moduleLen=moduleList.size();
		String temp=null;
		if(tarLen==moduleLen){
			for(int i=0;i<tarLen;i++){
				temp=moduleList.get(i).replaceAll(regex, tarList.get(i));
				res.add(temp);
			}
		}else{
			try {
				throw new Exception("参数异常！");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	public static List<String> replaceList(List<String> tarList,List<String> excludList,String module,String regex){
		if(excludList!=null){
			tarList.removeAll(excludList);
		}
		List<String> res=new ArrayList<String>();
		String temp=null;
		for(String s:tarList){
			temp = module.replaceAll(regex, s);
			res.add(temp);
		}
		return res;
	}
	/**
	 * 对于tarList中排除excludList每个String,把模板字符串module中的src子串替换
	 * 
	 * @param tarList
	 * @param excludList
	 * @param module
	 * @param regex
	 * @param offset
	 *            从第二行开始的偏移，如果每行都是顶格则可以为空或者空字符串
	 * @return
	 */
	public static String replaceList(List<String> tarList,
			List<String> excludList, String module, String regex, String offset) {
		if (excludList != null) {
			tarList.removeAll(excludList);
		}
		StringBuffer sb = new StringBuffer();
		String temp = "";
		if (offset != null) {
			for (String s : tarList) {
				temp = module.replaceAll(regex, s);
				sb.append(temp).append("\n");
				sb.append(offset);
			}
		}
		return sb.toString();
	}

	// ------↓-----TMStringUtil-------↓-----------
	public static enum ENCODING {
		GBK, UTF_8, ISO_8859_1
	}

	public static int DEFAULT_BUFFER_SIZE = 1024;

	public static String conversionSpecialCharacters(String string) {
		return string.replaceAll("\\\\", "/");
	}

	public static Boolean stringToBoolean(String booleanString) {
		if (StringUtil.notEmpty(booleanString) && booleanString.equals("true")) {
			return true;
		} else {
			return false;
		}
	}

	public static String listToString(List<String> params, String sepator) {
		if (params.size() > 0) {
			StringBuffer buffer = new StringBuffer();
			for (String string : params) {
				buffer.append(string + sepator);
			}
			String result = buffer.toString();
			return result.substring(0, result.length() - 1);
		} else {
			return "";
		}
	}

	public static String getString(Object string, String replaceWord) {
		if (string == null) {
			return replaceWord;
		} else {
			return String.valueOf(string);
		}
	}

	public static String getString(Object object) {
		if (object == null)
			return "";
		if (object instanceof Integer) {
			return String.valueOf(object);
		} else if (object instanceof Float) {
			return String.valueOf(object);
		} else if (object instanceof Double) {
			return String.valueOf(object);
		} else if (object instanceof Float) {
			return String.valueOf(object);
		} else if (object instanceof Date) {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(object);
		} else if (object instanceof String) {
			return String.valueOf(object);
		} else {
			return "";
		}
	}

	public static boolean isEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	public static Boolean isQq(String string) {
		String check = "^[1-9][0-9]{4,11}$";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(string);
		boolean flag = matcher.matches();
		return flag;
	}

	public static boolean isMobile(String mobiles) {
		boolean flag = false;
		try {
			Pattern p = Pattern
					.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
			Matcher m = p.matcher(mobiles);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	public static boolean isHomepage(String str) {
		String regex = "http://(([a-zA-z0-9]|-){1,}\\.){1,}[a-zA-z0-9]{1,}-*";
		return isMatch(str, regex);
	}

	public static String transferEncoding(String string, String encoding)
			throws UnsupportedEncodingException {
		return new String(string.getBytes("ISO8859-1"), encoding);
	}

	public String StringtoSql(String str) {
		if (str == null) {
			return "";
		} else {
			try {
				str = str.trim().replace('\'', (char) 32);
			} catch (Exception e) {
				return "";
			}
		}
		return str;
	}

	public static String encodeByMD5(String originString) {
		if (originString != null) {
			try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				byte[] results = md.digest(originString.getBytes());
				String resultstring = byteArrayToHexStr(results);
				return resultstring.toUpperCase();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String startIntercept(String string, String charRemove) {
		int len = string.length();
		if (isEmpty(string)) {
			return string;
		}
		int start = 0;
		if (charRemove == null) {
			while ((start != len)
					&& Character.isWhitespace(string.charAt(start))) {
				start++;
			}
		} else if (charRemove.length() == 0) {
			return string;
		} else {
			while ((start != len)
					&& (charRemove.indexOf(string.charAt(start)) != -1)) {
				start++;
			}
		}
		return string.substring(start);
	}

	public static String endIntercept(String str, String stripChars) {
		int end;
		if (str == null || (end = str.length()) == 0) {
			return str;
		}
		if (stripChars == null) {
			while ((end != 0) && Character.isWhitespace(str.charAt(end - 1))) {
				end--;
			}
		} else if (stripChars.length() == 0) {
			return str;
		} else {
			while ((end != 0)
					&& (stripChars.indexOf(str.charAt(end - 1)) != -1)) {
				end--;
			}
		}
		return str.substring(0, end);
	}

	public static String[] interceptAll(String[] strs, String interceptChars) {
		int strsLen;
		if (strs == null || (strsLen = strs.length) == 0) {
			return strs;
		}
		String[] newArr = new String[strsLen];
		for (int i = 0; i < strsLen; i++) {
			newArr[i] = intercept(strs[i], interceptChars);
		}
		return newArr;
	}

	public static String intercept(String str, String stripChars) {
		if (isEmpty(str)) {
			return str;
		}
		str = startIntercept(str, stripChars);
		return endIntercept(str, stripChars);
	}

	public static int indexOf(String str, char strChar) {
		if (isEmpty(str)) {
			return -1;
		}
		return str.indexOf(strChar);
	}

	public static int indexOf(String str, String searchStr) {
		if (str == null || searchStr == null) {
			return -1;
		}
		return str.indexOf(searchStr);
	}

	public static int indexOf(String str, char searchChar, int startPos) {
		if (isEmpty(str)) {
			return -1;
		}
		return str.indexOf(searchChar, startPos);
	}

	public static boolean containsAny(String str, char[] searchChars) {
		if (str == null || str.length() == 0 || searchChars == null
				|| searchChars.length == 0) {
			return false;
		}
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			for (int j = 0; j < searchChars.length; j++) {
				if (searchChars[j] == ch) {
					return true;
				}
			}
		}
		return false;
	}

	public static int indexOfDifference(String[] strs) {
		if (strs == null || strs.length <= 1) {
			return -1;
		}
		boolean anyStringNull = false;
		boolean allStringsNull = true;
		int arrayLen = strs.length;
		int shortestStrLen = Integer.MAX_VALUE;
		int longestStrLen = 0;
		for (int i = 0; i < arrayLen; i++) {
			if (strs[i] == null) {
				anyStringNull = true;
				shortestStrLen = 0;
			} else {
				allStringsNull = false;
				shortestStrLen = Math.min(strs[i].length(), shortestStrLen);
				longestStrLen = Math.max(strs[i].length(), longestStrLen);
			}
		}
		if (allStringsNull || (longestStrLen == 0 && !anyStringNull)) {
			return -1;
		}
		if (shortestStrLen == 0) {
			return 0;
		}
		int firstDiff = -1;
		for (int stringPos = 0; stringPos < shortestStrLen; stringPos++) {
			char comparisonChar = strs[0].charAt(stringPos);
			for (int arrayPos = 1; arrayPos < arrayLen; arrayPos++) {
				if (strs[arrayPos].charAt(stringPos) != comparisonChar) {
					firstDiff = stringPos;
					break;
				}
			}
			if (firstDiff != -1) {
				break;
			}
		}
		if (firstDiff == -1 && shortestStrLen != longestStrLen) {
			return shortestStrLen;
		}
		return firstDiff;
	}

	public static String getCommonPrefix(String[] strs) {
		if (strs == null || strs.length == 0) {
			return "";
		}
		int smallestIndexOfDiff = indexOfDifference(strs);
		if (smallestIndexOfDiff == -1) {
			if (strs[0] == null) {
				return "";
			}
			return strs[0];
		} else if (smallestIndexOfDiff == 0) {
			return "";
		} else {
			return strs[0].substring(0, smallestIndexOfDiff);
		}
	}

	public static String replace(String text, String searchString,
			String replacement, int max) {
		if (isEmpty(text) || isEmpty(searchString) || replacement == null
				|| max == 0) {
			return text;
		}
		int start = 0;
		int end = text.indexOf(searchString, start);
		if (end == -1) {
			return text;
		}
		int replLength = searchString.length();
		int increase = replacement.length() - replLength;
		increase = (increase < 0 ? 0 : increase);
		increase *= (max < 0 ? 16 : (max > 64 ? 64 : max));
		StringBuffer buf = new StringBuffer(text.length() + increase);
		while (end != -1) {
			buf.append(text.substring(start, end)).append(replacement);
			start = end + replLength;
			if (--max == 0) {
				break;
			}
			end = text.indexOf(searchString, start);
		}
		buf.append(text.substring(start));
		return buf.toString();
	}

	public static boolean isHave(String s) {
		String[] strs = { "gbk", "gb2312", "utf-8", "big5" };
		for (int i = 0; i < strs.length; i++) {
			if (strs[i].equalsIgnoreCase(s)) {
				return true;
			}
		}
		return false;
	}

	public static String doHtm(String str) {
		str = str.replace("&", "&amp;");
		str = str.replace("<", "&lt;");
		str = str.replace(">", "&gt;");
		str = str.replace(" ", "&nbsp;");
		str = str.replace("\n", "<br>");
		return str;
	}

	public static String arrToString(String[] strings, String separtor) {
		StringBuffer buffer = new StringBuffer();
		if (strings != null) {
			for (String string : strings) {
				buffer.append(string + separtor);
			}
			String result = buffer.toString();
			return result.substring(0, result.length() - 1);
		} else {
			return "";
		}
	}
	public static String arrToString(Integer[] integers, String separtor) {
		StringBuffer buffer = new StringBuffer();
		if (integers != null) {
			for (Integer string : integers) {
				buffer.append(string + separtor);
			}
			String result = buffer.toString();
			return result.substring(0, result.length() - 1);
		} else {
			return "";
		}
	}
	public static String arrToString(String[] strings, String prefix,
			String separtor) {
		StringBuffer buffer = new StringBuffer();
		if (strings != null) {
			for (String string : strings) {
				buffer.append(prefix + string + separtor);
			}
			String result = buffer.toString();
			return result.substring(0, result.length() - 1);
		} else {
			return "";
		}
	}

	public static String listToString(List<String> strings) {
		StringBuffer buffer = new StringBuffer();
		if (strings != null) {
			for (String string : strings) {
				buffer.append("\"" + string + "\",");
			}
			return buffer.toString().substring(0,
					buffer.toString().length() - 1);
		}
		return buffer.append(" ").toString();
	}

	public static final byte[] BOM = { -17, -69, -65 };
	public static final char[] HexDigits = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	public static final Pattern PTitle = Pattern.compile(
			"<title>(.+?)</title>", 34);

	public static byte[] md5(String src) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] md = md5.digest(src.getBytes());
			return md;
		} catch (Exception e) {
		}
		return null;
	}



	public static String byteToBin(byte[] bs) {
		char[] cs = new char[bs.length * 9];
		for (int i = 0; i < bs.length; ++i) {
			byte b = bs[i];
			int j = i * 9;
			cs[j] = (((b >>> 7 & 0x1) == 1) ? 49 : '0');
			cs[(j + 1)] = (((b >>> 6 & 0x1) == 1) ? 49 : '0');
			cs[(j + 2)] = (((b >>> 5 & 0x1) == 1) ? 49 : '0');
			cs[(j + 3)] = (((b >>> 4 & 0x1) == 1) ? 49 : '0');
			cs[(j + 4)] = (((b >>> 3 & 0x1) == 1) ? 49 : '0');
			cs[(j + 5)] = (((b >>> 2 & 0x1) == 1) ? 49 : '0');
			cs[(j + 6)] = (((b >>> 1 & 0x1) == 1) ? 49 : '0');
			cs[(j + 7)] = (((b & 0x1) == 1) ? 49 : '0');
			cs[(j + 8)] = ',';
		}
		return new String(cs);
	}



	public static String javaEncode(String txt) {
		if ((txt == null) || (txt.length() == 0)) {
			return txt;
		}
		txt = replaceEx(txt, "\\", "\\\\");
		txt = replaceEx(txt, "\r\n", "\n");
		txt = replaceEx(txt, "\n", "\\n");
		txt = replaceEx(txt, "\"", "\\\"");
		txt = replaceEx(txt, "'", "\\'");
		return txt;
	}

	public static String javaDecode(String txt) {
		if ((txt == null) || (txt.length() == 0)) {
			return txt;
		}
		txt = replaceEx(txt, "\\\\", "\\");
		txt = replaceEx(txt, "\\n", "\n");
		txt = replaceEx(txt, "\\r", "\r");
		txt = replaceEx(txt, "\\\"", "\"");
		txt = replaceEx(txt, "\\'", "'");
		return txt;
	}

	public static String[] splitEx(String str, String spilter) {
		if (str == null) {
			return null;
		}
		if ((spilter == null) || (spilter.equals(""))
				|| (str.length() < spilter.length())) {
			String[] t = { str };
			return t;
		}
		ArrayList al = new ArrayList();
		char[] cs = str.toCharArray();
		char[] ss = spilter.toCharArray();
		int length = spilter.length();
		int lastIndex = 0;
		for (int i = 0; i <= str.length() - length;) {
			boolean notSuit = false;
			for (int j = 0; j < length; ++j) {
				if (cs[(i + j)] != ss[j]) {
					notSuit = true;
					break;
				}
			}
			if (!(notSuit)) {
				al.add(str.substring(lastIndex, i));
				i += length;
				lastIndex = i;
			} else {
				++i;
			}
		}
		if (lastIndex <= str.length()) {
			al.add(str.substring(lastIndex, str.length()));
		}
		String[] t = new String[al.size()];
		for (int i = 0; i < al.size(); ++i) {
			t[i] = ((String) al.get(i));
		}
		return t;
	}

	public static String replaceEx(String str, String subStr, String reStr) {
		if (str == null) {
			return null;
		}
		if ((subStr == null) || (subStr.equals(""))
				|| (subStr.length() > str.length()) || (reStr == null)) {
			return str;
		}
		StringBuffer sb = new StringBuffer();
		String tmp = str;
		int index = -1;
		while (true) {
			index = tmp.indexOf(subStr);
			if (index < 0) {
				break;
			}
			sb.append(tmp.substring(0, index));
			sb.append(reStr);
			tmp = tmp.substring(index + subStr.length());
		}
		sb.append(tmp);
		return sb.toString();
	}

	public static String replaceAllIgnoreCase(String source, String oldstring,
			String newstring) {
		Pattern p = Pattern.compile(oldstring, 34);
		Matcher m = p.matcher(source);
		return m.replaceAll(newstring);
	}

	public static String quotEncode(String txt) {
		if ((txt == null) || (txt.length() == 0)) {
			return txt;
		}
		txt = replaceEx(txt, "&", "&amp;");
		txt = replaceEx(txt, "\"", "&quot;");
		return txt;
	}

	public static String quotDecode(String txt) {
		if ((txt == null) || (txt.length() == 0)) {
			return txt;
		}
		txt = replaceEx(txt, "&quot;", "\"");
		txt = replaceEx(txt, "&amp;", "&");
		return txt;
	}

	public static String escape(String src) {
		StringBuffer sb = new StringBuffer();
		sb.ensureCapacity(src.length() * 6);
		for (int i = 0; i < src.length(); ++i) {
			char j = src.charAt(i);
			if ((Character.isDigit(j)) || (Character.isLowerCase(j))
					|| (Character.isUpperCase(j))) {
				sb.append(j);
			} else if (j < 256) {
				sb.append("%");
				if (j < '\16') {
					sb.append("0");
				}
				sb.append(Integer.toString(j, 16));
			} else {
				sb.append("%u");
				sb.append(Integer.toString(j, 16));
			}
		}
		return sb.toString();
	}

	public static String leftPad(String srcString, char c, int length) {
		if (srcString == null) {
			srcString = "";
		}
		int tLen = srcString.length();
		if (tLen >= length) {
			return srcString;
		}
		int iMax = length - tLen;
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < iMax; ++i) {
			sb.append(c);
		}
		sb.append(srcString);
		return sb.toString();
	}

	public static String subString(String src, int length) {
		if (src == null) {
			return null;
		}
		int i = src.length();
		if (i > length) {
			return src.substring(0, length);
		}
		return src;
	}




	public static String rightPad(String srcString, char c, int length) {
		if (srcString == null) {
			srcString = "";
		}
		int tLen = srcString.length();
		if (tLen >= length)
			return srcString;
		int iMax = length - tLen;
		StringBuffer sb = new StringBuffer();
		sb.append(srcString);
		for (int i = 0; i < iMax; ++i) {
			sb.append(c);
		}
		return sb.toString();
	}

	public static String fromEncodingToAnotherEncoding(String content,
			ENCODING fromEncoding, ENCODING toEncoding) {
		String from = fromEncoding.toString().replace("_", "-");
		String to = toEncoding.toString().replace("_", "-");
		if (null != content)
			try {
				byte[] tmpbyte = content.getBytes(from);
				content = new String(tmpbyte, to);
			} catch (Exception e) {
				System.out.println("Error: from: " + from + " To: " + to + " :"
						+ e.getMessage());
			}
		return content;
	}

	public static String htmlEncode(String txt) {
		if (null != txt) {
			txt = txt.replace("&", "&amp;").replace("&amp;amp;", "&amp;")
					.replace("&amp;quot;", "&quot;").replace("\"", "&quot;")
					.replace("&amp;lt;", "&lt;").replace("<", "&lt;")
					.replace("&amp;gt;", "&gt;").replace(">", "&gt;")
					.replace("&amp;nbsp;", "&nbsp;");
		}
		return txt;
	}

	public static String unHtmlEncode(String txt) {
		if (null != txt) {
			txt = txt.replace("&amp;", "&").replace("&quot;", "\"")
					.replace("&lt;", "<").replace("&gt;", ">")
					.replace("&nbsp;", " ");
		}
		return txt;
	}

	public static String getUserDir() {
		String dir = System.getProperty("user.dir");
		return conversionSpecialCharacters(dir);
	}

	public static String getUserDir(String path) {
		String dir = System.getProperty("user.dir");
		return conversionSpecialCharacters(dir) + path;
	}

	public static String getPercent(double percent) {
		return new DecimalFormat("#.##").format(percent);
	}

	public static String getPercent(float percent) {
		return new DecimalFormat("#.##").format(percent);
	}

	public static boolean isImage(String ext) {
		return ext.toLowerCase().matches("jpg|gif|bmp|png|jpeg");
	}

	public static boolean isDoc(String ext) {
		return ext.toLowerCase().matches("doc|docx|xls|xlsx|pdf|txt|ppt|pptx");
	}

	public static boolean isVideo(String ext) {
		return ext.toLowerCase().matches("mp4|flv|mp3");
	}

	public static String encryption(String str, int k) {
		String string = "";
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c >= 'a' && c <= 'z') {
				c += k % 26;
				if (c < 'a') {
					c += 26;
				}
				if (c > 'z') {
					c -= 26;
				}
			} else if (c >= 'A' && c <= 'Z') {
				c += k % 26;
				if (c < 'A') {
					c += 26;
				}
				if (c > 'Z') {
					c -= 26;
				}
			}
			string += c;
		}
		return string;
	}

	public static String dencryption(String str, int n) {
		String string = "";
		int k = Integer.parseInt("-" + n);
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c >= 'a' && c <= 'z') {
				c += k % 26;
				if (c < 'a') {
					c += 26;
				}
				if (c > 'z') {
					c -= 26;
				}
			} else if (c >= 'A' && c <= 'Z') {
				c += k % 26;
				if (c < 'A') {
					c += 26;
				}
				if (c > 'Z') {
					c -= 26;
				}
			}
			string += c;
		}
		return string;
	}

	// ------↑-----TMStringUtil-------↑-----------
	/**
	 * 将字符串s第一个字符转换成大写后,返回s
	 * 
	 * @param s
	 * @return
	 */
	public static String firstCharToUpperCase(String s) {
		if (notEmpty(s)) {
			s = s.trim();
			return s.substring(0, 1).toUpperCase() + s.substring(1);
		} else
			return "";
	}

	/**
	 * 将字符串s第一个字符转换成小写后,返回s
	 * 
	 * @param s
	 * @return
	 */
	public static String firstCharToLowerCase(String s) {
		if (notEmpty(s)) {
			s = s.trim();
			return s.substring(0, 1).toLowerCase() + s.substring(1);
		} else {
			return "";
		}
	}

	/**
	 * 判断字符串是否非空
	 * 
	 * @param s
	 * @return boolean
	 */
	public static boolean notEmpty(String s) {
		return !isEmpty(s);
	}

	/**
	 * 判断字符串是否为空
	 * 
	 * @param s
	 * @return
	 */
	public static boolean isEmpty(String s) {
		return s == null || s.length() == 0 || "".equals(s) ||"null".equals(s)
				|| s.matches("\\s*");
	}

	/**
	 * 从开始截取len长度的字符串
	 * 
	 * @param s
	 * @param len
	 * @return String
	 */
	public static String trim_width(String s, int len) {

		return len > 0 && s.length() > len ? s.substring(0, len) : s;
	}

	/**
	 * 截取endStr之前的内容，包含endStr
	 * 
	 * @param s
	 * @param endStr
	 * @return String
	 */
	public static String trim_before(String s, String endStr) {
		int index = s.indexOf(endStr);
		int len = endStr.length();
		return index < 0 ? s : s.substring(0, index + len);
	}

	/**
	 * 截取beginStr之后的内容(包含beginStr)
	 * 
	 * @param s
	 * @param beginStr
	 * @return String
	 */
	public static String trim_end(String s, String beginStr) {
		int index = s.indexOf(beginStr);
		return index > 0 ? s.substring(index) : s;
	}

	/**
	 * 截取endStr之前的内容(不包含endStr)
	 * 
	 * @param s
	 * @param endStr
	 * @return String
	 */
	public static String trim_before_exclu(String s, String endStr) {
		int index = s.indexOf(endStr);
		return index < 0 ? s : s.substring(0, index);
	}

	/**
	 * 截取beginStr之后的内容(不包含beginStr)
	 * 
	 * @param s
	 * @param beginStr
	 * @return String
	 */
	public static String trim_end_exclu(String s, String beginStr) {
		int index = s.indexOf(beginStr);
		int len = beginStr.length();
		return index > 0 ? s.substring(index + len) : s;
	}

	/**
	 * 返回beginStr和endStr之间的字符串(包含)
	 * 
	 * @param s
	 * @param beginStr
	 * @param endStr
	 * @return String
	 */
	public static String trim_mid_inclu(String s, String beginStr, String endStr) {
		s = trim_before(s, endStr);
		return trim_end(s, beginStr);
	}

	/**
	 * 返回beginStr和endStr之间的字符串(不包含)
	 * 
	 * @param s
	 * @param beginStr
	 * @param endStr
	 * @return String
	 */
	public static String trim_mid_exclu(String s, String beginStr, String endStr) {
		s = trim_before_exclu(s, endStr);
		return trim_end_exclu(s, beginStr);
	}

	/**
	 * 给List中每个String最前面拼接一个相同的String
	 * 
	 * @param list
	 * @param str
	 * @return List<String>
	 */
	public static List<String> addStringBefore(List<String> list, String str) {
		List<String> result = null;
		int count = list.size();
		if (count > 0) {
			result = new ArrayList<String>();
			for (String s : list) {
				StringBuffer sb = new StringBuffer(s);
				sb.insert(0, str);
				result.add(sb.toString());
			}
		}
		return result;
		/*if(str==null)
			return list;
		return  list.stream().map(s->{return str.concat(s);}).collect(Collectors.toList());*/
	}

	/**
	 * 给List中每个String最后面拼接一个相同的String
	 * 
	 * @param list
	 * @param str
	 * @return List<String>
	 */
	public static List<String> addStringBehind(List<String> list, String str) {
		List<String> result = null;
		int count = list.size();
		if (count > 0) {
			result = new ArrayList<String>();
			for (String s : list) {
				int len = s.length();
				StringBuffer sb = new StringBuffer(s);
				sb.insert(len, str);
				result.add(sb.toString());
			}
		}
		return result;
	}

	public static String replaceAll(String srcStr, String replace, String regex) {
		return srcStr.replaceAll(regex, replace);
	}

	/**
	 * 以相同的截取规则截取字符串list获得新的list
	 * 
	 * @param list
	 * @param beginStr
	 *            起始字符串
	 * @param b
	 *            结果是否包含beginStr
	 * @return List<String>
	 */
	public static List<String> substringList(List<String> list,
			String beginStr, boolean b) {
		List<String> result = null;
		int len = beginStr.length();
		if (list.size() > 0) {
			result = new ArrayList<String>();
			String temp = "";
			for (String t : list) {
				if (b) {
					temp = t.substring(t.lastIndexOf(beginStr));
				} else {
					temp = t.substring(t.lastIndexOf(beginStr) + len);
				}
				result.add(temp);
			}
		}
		return result;
	}

	/**
	 * 字节转换为十六进制字符串
	 * 
	 * @param bByte
	 * @return
	 */
	public static String byteToHexStr(byte bByte) {
		String[] digits = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"a", "b", "c", "d", "e", "f" };
		int iRet = bByte;
		if (iRet < 0) {
			iRet += 256;
		}
		int iD1 = iRet / 16;
		int iD2 = iRet % 16;
		return digits[iD1] + digits[iD2];
	}

	/**
	 * 转换字节数组为16进制字串
	 * 
	 * @param bByte
	 * @return
	 */
	public static String byteArrayToHexStr(byte[] bByte) {
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < bByte.length; i++) {
			sBuffer.append(byteToHexStr(bByte[i]));
		}
		return sBuffer.toString();
	}

	/**
	 * char数组转换成字符串
	 * 
	 * @param chs
	 * @return
	 */
	public static String chars2String(char[] chs) {
		StringBuilder sb = new StringBuilder();
		for (char c : chs) {
			sb.append(c);
		}
		return sb.toString();
	}

	/**
	 * 判断content是否与正则表达式regex的描述相匹配
	 * 
	 * @param content
	 *            要检测的内容
	 * @param regex
	 *            正则表达式
	 * @return
	 */
	public static boolean isMatch(String content, String regex) {
		if (regex == null) {
			return false;
		}
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(content);
		return m.matches();
	}

	/**
	 * 判断content是否与正则表达式数组regexs的描述相匹配,如果符合其中一项,则返回true,如果都不符合,返回false
	 * 
	 * @param content
	 *            要检测的内容
	 * @param regexs
	 *            正则表达式数组
	 * @return
	 */
	public static boolean isMatch(String content, String... regexs) {
		boolean rst = false;
		if (regexs == null) {
			return false;
		}
		// System.out.println("要匹配的内容:"+content);
		for (String r : regexs) {
			if (isMatch(content, r)) {
				rst = true;
				break;
			}
		}
		return rst;
	}

	/**
	 * 对于list中每一个字符串,判断与表达式regex匹配的放入新的list并返回
	 * 
	 * @param list
	 * @param regex
	 * @return
	 */
	public static List<String> isMatchList(List<String> list, String regex) {
		if (list != null && regex != null) {
			List<String> newList = null;
			newList = new ArrayList<String>();
			for (String s : list) {
				if (isMatch(s, regex)) {
					newList.add(s);
				}
			}
			return newList;
		} else if (regex == null) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * 对于list中每一个字符串,判断与表达式regexs中任意一项匹配的item放入新的list并返回
	 * 
	 * @param list
	 * @param regexs
	 * @return
	 */
	public static List<String> isMatchList(List<String> list, String... regexs) {
		if (list != null && regexs != null) {
			List<String> newList = null;
			newList = new ArrayList<String>();
			for (String s : list) {
				if (isMatch(s, regexs)) {
					newList.add(s);
				}
			}
			return newList;
		} else if (regexs == null) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * 对于list中每一个字符串,判断与表达式regexs中任意一项都不匹配的item放入新的list并返回
	 * 即从原来list中剔除与regexs匹配的item
	 * 
	 * @param list
	 * @param regexs
	 * @return
	 */
	public static List<String> notMatchList(List<String> list, String... regexs) {
		if (list != null && regexs != null) {
			List<String> newList = null;
			newList = new ArrayList<String>();
			for (String s : list) {
				if (!isMatch(s, regexs)) {
					newList.add(s);
				}
			}
			return newList;
		} else if (regexs == null) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * 对于list中每一个字符串,判断与表达式regex不匹配的放入新的list并返回 即从原来list中剔除与regex匹配的item
	 * 
	 * @param list
	 * @param regex
	 * @return
	 */
	public static List<String> notMatchList(List<String> list, String regex) {
		if (list != null && regex != null) {
			List<String> newList = null;
			newList = new ArrayList<String>();
			for (String s : list) {
				if (!isMatch(s, regex)) {
					newList.add(s);
				}
			}
			return newList;
		} else if (regex == null) {
			return list;
		} else {
			return null;
		}
	}

	// -----------------------------------
	/**
	 * 格式化输出方法
	 * 
	 * @param msg
	 * @param args
	 */
	public static void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}



	/**
	 * 打印一个List,主要供测试使用
	 * 
	 * @param list
	 */
	public static void printList(List<?> list) {
		for (Object o : list) {
			System.out.println(o.toString());
		}
	}

	public static void printArray(String[] strs) {
		for (String s : strs) {
			System.out.println(s);
		}

	}

}
