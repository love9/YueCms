package com.markbro.base.utils.jsoup;


/**
 * 图片信息实体类
 * @author wujiyue
 */
public class ImageBean {
	private String id;
	//图片名称（含后缀）
	private String name;
	//图片后缀
	private String type;
	//图片大小
	private int size;
	//图片的宽
	private String width;
	//图片高
	private String height;
	//图片的alt属性值,图片描述信息
	private String alt;
	//图片在网络上的url地址(大图)
	private String urlAddress;
	//图片在网络上的url地址(小图)
	private String urlAddressSmall;
	//图片在本地存储路径
	private String localAddress;

	@Override
	public String toString() {
		return "ImageBean{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", type='" + type + '\'' +
				", size=" + size +
				", width='" + width + '\'' +
				", height='" + height + '\'' +
				", alt='" + alt + '\'' +
				", urlAddress='" + urlAddress + '\'' +
				", urlAddressSmall='" + urlAddressSmall + '\'' +
				", localAddress='" + localAddress + '\'' +
				'}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getUrlAddress() {
		return urlAddress;
	}

	public void setUrlAddress(String urlAddress) {
		this.urlAddress = urlAddress;
	}

	public String getUrlAddressSmall() {
		return urlAddressSmall;
	}

	public void setUrlAddressSmall(String urlAddressSmall) {
		this.urlAddressSmall = urlAddressSmall;
	}

	public String getLocalAddress() {
		return localAddress;
	}

	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
}
