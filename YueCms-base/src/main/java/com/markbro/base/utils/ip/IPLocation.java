package com.markbro.base.utils.ip;
/**
 * 用来封装IP相关信息的实体类
 * @author wjy
 *
 */
public class IPLocation {
	private String country;
	private String area;
	public IPLocation(){
		country=area="";
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		if(area.trim().equals("CZ88.NET")){
			this.area="本机或者本网络";
		}else{
		this.area = area;
		}
	}
	public IPLocation getCopy(){
		IPLocation ret=new IPLocation();
		ret.country=this.country;
		ret.area=this.area;
		return ret;
	}
}
