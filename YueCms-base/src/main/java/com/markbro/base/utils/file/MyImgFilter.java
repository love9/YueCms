package com.markbro.base.utils.file;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 我的图片过滤器
 * @author wujiyue
 *
 */
public class MyImgFilter implements FilenameFilter {
	private String[] suffix=new String[]{".gif",".jpg",".jpeg",".png",".bmp"};
	
	public MyImgFilter() {
		
	}
	public MyImgFilter(String[] suffix) {
		if(suffix!=null){
		this.suffix = suffix;
		}
	}
	private boolean  isImage(String name){
		for(String s:suffix){
			if(name.toLowerCase().endsWith(s)){
				return true;
			}
		}
		return false;
	}
	@Override
	public boolean accept(File dir, String name) {
		return  isImage(name);
	}


}
