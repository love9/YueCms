package com.markbro.base.utils.tree;

import java.util.List;
import java.util.Map;

/**
 * @author wujiyue
 */
public class EasyUITreeNode {
    /**
     * 主键
     */
    private String id;
    /**
     * 父亲主键
     */
    private String pid;
    /**
     * 名称
     */
    private String text;
    /**
     * 图标
     */
    private String iconCls;
    /**
     * 是否叶子
     */
    private boolean leaf;
    /**
     * 是否展开
     */
    private boolean expanded;
    /**
     * 序号
     */
    private int sort;
    /**
     * 是否选中
     */
    private boolean checked;
    private String state = "";
    private Map<String, Object> attributes;
    /**
     * 子节点
     */
    private List<EasyUITreeNode> children;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public EasyUITreeNode() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public List<EasyUITreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<EasyUITreeNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "EasyUITreeNode{" +
                "id='" + id + '\'' +
                ", pid='" + pid + '\'' +
                ", text='" + text + '\'' +
                ", iconCls='" + iconCls + '\'' +
                ", leaf=" + leaf +
                ", expanded=" + expanded +
                ", sort=" + sort +
                ", checked=" + checked +
                ", state='" + state + '\'' +
                ", attributes=" + attributes +
                ", children=" + children +
                '}';
    }
}
