package com.markbro.base.utils.test;


import com.markbro.base.utils.file.FileUtil;

import java.util.List;
import java.util.Map;

/**
 * @author wujiyue
 */
public class TestUtil {
    /**
     * 打印一个List,主要供测试使用
     *
     * @param list
     */
    public static void printList(List<?> list) {

        for (Object o : list) {
            System.out.println(o.toString());
        }
    }
    public static void printMap(Map<?,?> map) {
        for(Map.Entry<?,?> entry:map.entrySet()){
            System.out.println(entry.getKey()+"---------"+entry.getValue());
        }
    }

    public static void printArray(String[] strs) {
        for (String s : strs) {
            System.out.println(s);
        }

    }
    public static void main(String[]  args){
    //  List ls=  FileUtil.getDirectoryFirstFiles("D:\\cmd");
        List ls= FileUtil.getFilterFilesPath("D:\\cmd", null);
        printList(ls);
    }
}
