/**
 * Copyright (c) 2005-2012 springside.org.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.markbro.base.utils;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.Map.Entry;

/**
 * Http与Servlet工具类.
 */
public class ServletUtil {

	/**
	 * 把一个bean中的属性转化到map中
	 *
	 * @param bean       bean对象
	 * @param properties 存放bean中属性的map对象
	 */
	public static void beanToMap(Object bean, Map properties)
	{
		if (bean == null || properties == null)
		{
			return;
		}
		try
		{
			Map map = BeanUtils.describe(bean);
			map.remove("class");
			for(Iterator iter = map.keySet().iterator(); iter.hasNext();)
			{
				String key = (String)iter.next();
				Object value = map.get(key);
				properties.put(key,value);
			}
		}
		catch (Exception ex)
		{
			System.out.println("读取bean属性出错");
			ex.printStackTrace();
		}
	}

	public static Map<String, String> getMap(HttpServletRequest req) {
		Map<String, String> map=new HashMap<String, String>();
		Enumeration enu = req.getParameterNames();
		while(enu.hasMoreElements()) {
			String paramName = (String)enu.nextElement();
			String[] paramValues = req.getParameterValues(paramName);
			if (paramValues.length == 1) {
				String paramValue = paramValues[0];
				if (paramValue.length() != 0) {
					map.put(paramName, paramValue);
				}
			}
		}
		if(GlobalConfig.isDevMode()){
			Set<Entry<String, String>> set = map.entrySet();
			for (Entry entry : set) {
			  System.out.println(entry.getKey() + ":" + entry.getValue());
			}
		}
		return map;
	}

	/**
	 * 获取当前请求对象
	 * @return
	 */
	public static HttpServletRequest getRequest(){
		try{
			return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		}catch(Exception e){
			return null;
		}
	}

}
