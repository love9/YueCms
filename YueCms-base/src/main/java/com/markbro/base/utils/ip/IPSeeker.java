package com.markbro.base.utils.ip;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * 解析IP的核心类
 *
 * @author wjy
 */
public class IPSeeker {
    Logger log = LoggerFactory.getLogger(this.getClass());
    //设计成单例模式(懒汉式)
    private static volatile IPSeeker instance = null;

    //对外的获得实例方法
    public static IPSeeker getInstance() {
        if (instance == null) {
            synchronized (IPSeeker.class) {
                if (instance == null) {
                    instance = new IPSeeker();
                }
            }
        }
        return instance;
    }

    //数据库文件名称
    private String IP_FILE = "qqwry.dat";
    //数据库文件存放路径
    private String INSTALL_DIR = "c:\\ip";

    //一些固定常量
    private static final int IP_RECORD_LENGTH = 7;
    private static final byte REDIRECT_MODE_1 = 0x01;
    private static final byte REDIRECT_MODE_2 = 0x02;

    //用来作为cache,查询数据库文件之前先在这里找,找不到再去查ip数据库文件
    private Map<String, IPLocation> ipCache;
    //随机文件访问类
    private RandomAccessFile ipFile;
    //内存映射文件
    private MappedByteBuffer mbb;
    //起始地区的开始和结束绝对偏移
    private long ipBegin, ipEnd;

    //临时变量
    private IPLocation loc;
    private byte[] buf;
    private byte[] b4;
    private byte[] b3;

    private void init() {
        ipCache = new HashMap<String, IPLocation>();
        loc = new IPLocation();
        buf = new byte[100];
        b4 = new byte[4];
        b3 = new byte[3];
        //首先去classpath下找install.properties获取数据库文件名称和安装路径
        // 如果没有，默认使用jar包里classpath的qqwry.dat数据库文件
            Properties p = new Properties();
        try {
            String path=this.INSTALL_DIR+"\\"+this.IP_FILE;
            ipFile = new RandomAccessFile(path, "r");
        } catch (Exception e) {
            log.info("读取install.properties文件初始化ip数据库文件失败!",e);
            try {
                // String classPath = new File(this.getClass().getResource("/").getPath()).getCanonicalPath();
                //String classPath = new File(IPSeeker.class.getResource("/").getPath()).getCanonicalPath();
                String classPath= new File(IPSeeker.class.getClassLoader().getResource("").getPath()).getCanonicalPath();
                String ipFilePath = classPath + "\\qqwry.dat";//classpath下的qqwry.dat文件的路径
                System.out.println(ipFilePath);
                ipFile = new RandomAccessFile(ipFilePath, "r");

            } catch (Exception e1) {
                ipFile = null;
                log.info("初始化classpath下qqwry.dat数据库文件错误!",e1);
            }
        }
        //如果打开ip数据库文件成功,读取文件头信息
        if (ipFile != null) {
            try {
                ipBegin = readLong4(0);
                ipEnd = readLong4(4);
                if (ipBegin == -1 || ipEnd == -1) {
                    ipFile.close();
                    ipFile = null;
                }
            } catch (IOException e) {
                log.info("ip数据库文件格式错误,不能读取信息,IP显示功能将无法使用!",e);
            }
        } else {
            System.out.println("ipFile对象为null!");
        }


    }

    /**
     * 私有的构造方法
     */
    private IPSeeker() {
        init();


    }

    /**
     * 给定一个地点的不完全名字,得到一系列包含s字符串的IP范围记录
     *
     * @param s 地点字符串
     * @return 包含IPEntry类型的List
     */
    public List<IPEntry> getIPEntriesDebug(String s) {
        List<IPEntry> ret = new ArrayList<IPEntry>();
        long endOffset = ipEnd + 4;
        for (long offset = ipBegin + 4; offset <= endOffset; offset += IP_RECORD_LENGTH) {
            //读取结束ip偏移
            long temp = readLong3(offset);
            //如果temp不等于-1,读取ip地点信息
            if (temp != -1) {
                IPLocation ipLoc = getIPLocation(temp);
                //判断这个地点里面是否包含s字符串,包含就加入到List中去
                if (ipLoc.getCountry().indexOf(s) != -1 || ipLoc.getArea().indexOf(s) != -1) {
                    IPEntry entry = new IPEntry();
                    entry.country = ipLoc.getCountry();
                    entry.area = ipLoc.getArea();
                    //得到起始IP
                    readIP(offset - 4, b4);
                    entry.beginIp = IpUtil.getIpStringFromBytes(b4);
                    //得到结束IP
                    readIP(temp, b4);
                    entry.endIp = IpUtil.getIpStringFromBytes(b4);
                    //添加该条记录
                    ret.add(entry);
                }
            }
        }
        return ret;
    }

    public IPLocation getIPLocation(String ip) {
        IPLocation location = new IPLocation();
        location.setArea(this.getArea(ip));
        location.setCountry(this.getCountry(ip));
        return location;
    }

    /**
     * 给定一个地点的不完全名字，得到一系列包含s子串的IP范围记录
     *
     * @param s 地点子串
     * @return 包含IPEntry类型的List
     */
    public List<IPEntry> getIPEntries(String s) {
        List<IPEntry> ret = new ArrayList<IPEntry>();
        try {
            // 映射IP信息文件到内存中
            if (mbb == null) {
                FileChannel fc = ipFile.getChannel();
                mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, ipFile.length());
                mbb.order(ByteOrder.LITTLE_ENDIAN);
            }

            int endOffset = (int) ipEnd;
            for (int offset = (int) ipBegin + 4; offset <= endOffset; offset += IP_RECORD_LENGTH) {
                int temp = readInt3(offset);
                if (temp != -1) {
                    IPLocation ipLoc = getIPLocation(temp);
                    // 判断是否这个地点里面包含了s子串，如果包含了，添加这个记录到List中，如果没有，继续
                    if (ipLoc.getCountry().indexOf(s) != -1 || ipLoc.getArea().indexOf(s) != -1) {
                        IPEntry entry = new IPEntry();
                        entry.country = ipLoc.getCountry();
                        entry.area = ipLoc.getArea();
                        // 得到起始IP
                        readIP(offset - 4, b4);
                        entry.beginIp = IpUtil.getIpStringFromBytes(b4);
                        // 得到结束IP
                        readIP(temp, b4);
                        entry.endIp = IpUtil.getIpStringFromBytes(b4);
                        // 添加该记录
                        ret.add(entry);
                    }
                }
            }
        } catch (IOException e) {
            LogFactory.log("", Level.ERROR, e);
        }
        return ret;
    }

    /**
     * 从内存映射文件的offset位置开始的3个字节读取一个int
     *
     * @param offset
     * @return
     */
    private int readInt3(int offset) {
        mbb.position(offset);
        return mbb.getInt() & 0x00FFFFFF;
    }

    /**
     * 从内存映射文件的当前位置开始的3个字节读取一个int
     *
     * @return
     */
    private int readInt3() {
        return mbb.getInt() & 0x00FFFFFF;
    }

    /**
     * 根据IP得到国家名
     *
     * @param ip ip的字节数组形式
     * @return 国家名字符串
     */
    public String getCountry(byte[] ip) {
        // 检查ip地址文件是否正常
        if (ipFile == null)
            return Message.bad_ip_file;
        // 保存ip，转换ip字节数组为字符串形式
        String ipStr = IpUtil.getIpStringFromBytes(ip);
        // 先检查cache中是否已经包含有这个ip的结果，没有再搜索文件
        if (ipCache.containsKey(ipStr)) {
            IPLocation ipLoc = ipCache.get(ipStr);
            return ipLoc.getCountry();
        } else {
            IPLocation ipLoc = getIPLocation(ip);
            ipCache.put(ipStr, ipLoc.getCopy());
            return ipLoc.getCountry();
        }
    }

    /**
     * 根据IP得到国家名
     *
     * @param ip IP的字符串形式
     * @return 国家名字符串
     */
    public String getCountry(String ip) {
        return getCountry(IpUtil.getIpByteArrayFromString(ip));
    }

    /**
     * 根据IP得到地区名
     *
     * @param ip ip的字节数组形式
     * @return 地区名字符串
     */
    public String getArea(byte[] ip) {
        // 检查ip地址文件是否正常
        if (ipFile == null)
            return Message.bad_ip_file;
        // 保存ip，转换ip字节数组为字符串形式
        String ipStr = IpUtil.getIpStringFromBytes(ip);
        // 先检查cache中是否已经包含有这个ip的结果，没有再搜索文件
        if (ipCache.containsKey(ipStr)) {
            IPLocation ipLoc = ipCache.get(ipStr);
            return ipLoc.getArea();
        } else {
            IPLocation ipLoc = getIPLocation(ip);
            ipCache.put(ipStr, ipLoc.getCopy());
            return ipLoc.getArea();
        }
    }

    /**
     * 根据IP得到地区名
     *
     * @param ip IP的字符串形式
     * @return 地区名字符串
     */
    public String getArea(String ip) {
        return getArea(IpUtil.getIpByteArrayFromString(ip));
    }

    /**
     * 根据ip搜索ip信息文件，得到IPLocation结构，所搜索的ip参数从类成员ip中得到
     *
     * @param ip 要查询的IP
     * @return IPLocation结构
     */
    private IPLocation getIPLocation(byte[] ip) {
        IPLocation info = null;
        long offset = locateIP(ip);
        if (offset != -1)
            info = getIPLocation(offset);
        if (info == null) {
            info = new IPLocation();
            info.setCountry(Message.unknown_country);
            info.setArea(Message.unknown_area);
        }
        return info;
    }

    /**
     * 从offset位置读取4个字节的ip地址放入ip数组,读取后的ip为big-endian格式,但是文件中是little-endian形式,将会进行转换
     *
     * @param offset
     * @param ip
     */
    private void readIP(long offset, byte[] ip) {
        try {
            ipFile.seek(offset);
            ipFile.readFully(ip);
            byte temp = ip[0];
            ip[0] = ip[3];
            ip[3] = temp;
            temp = ip[1];
            ip[1] = ip[2];
            ip[2] = temp;
        } catch (IOException e) {
            LogFactory.log("", Level.ERROR, e);
        }


    }

    /**
     * 从offset位置读取四个字节的ip地址放入ip数组中，读取后的ip为big-endian格式，但是
     * 文件中是little-endian形式，将会进行转换
     *
     * @param offset
     * @param ip
     */
    private void readIP(int offset, byte[] ip) {
        mbb.position(offset);
        mbb.get(ip);
        byte temp = ip[0];
        ip[0] = ip[3];
        ip[3] = temp;
        temp = ip[1];
        ip[1] = ip[2];
        ip[2] = temp;
    }

    /**
     * 把类成员ip和beginIp比较，注意这个beginIp是big-endian的
     *
     * @param ip      要查询的IP
     * @param beginIp 和被查询IP相比较的IP
     * @return 相等返回0，ip大于beginIp则返回1，小于返回-1。
     */
    private int compareIP(byte[] ip, byte[] beginIp) {
        for (int i = 0; i < 4; i++) {
            int r = compareByte(ip[i], beginIp[i]);
            if (r != 0)
                return r;
        }
        return 0;
    }

    /**
     * 把两个byte当作无符号数进行比较
     *
     * @param b1
     * @param b2
     * @return 若b1大于b2则返回1，相等返回0，小于返回-1
     */
    private int compareByte(byte b1, byte b2) {
        if ((b1 & 0xFF) > (b2 & 0xFF)) // 比较是否大于
            return 1;
        else if ((b1 ^ b2) == 0)// 判断是否相等
            return 0;
        else
            return -1;
    }

    /**
     * 这个方法将根据ip的内容，定位到包含这个ip国家地区的记录处，返回一个绝对偏移
     * 方法使用二分法查找。
     *
     * @param ip 要查询的IP
     * @return 如果找到了，返回结束IP的偏移，如果没有找到，返回-1
     */
    private long locateIP(byte[] ip) {
        long m = 0;
        int r;
        // 比较第一个ip项
        readIP(ipBegin, b4);
        r = compareIP(ip, b4);
        if (r == 0) return ipBegin;
        else if (r < 0) return -1;
        // 开始二分搜索
        for (long i = ipBegin, j = ipEnd; i < j; ) {
            m = getMiddleOffset(i, j);
            readIP(m, b4);
            r = compareIP(ip, b4);
            // log.debug(Utils.getIpStringFromBytes(b));
            if (r > 0)
                i = m;
            else if (r < 0) {
                if (m == j) {
                    j -= IP_RECORD_LENGTH;
                    m = j;
                } else
                    j = m;
            } else
                return readLong3(m + 4);
        }
        // 如果循环结束了，那么i和j必定是相等的，这个记录为最可能的记录，但是并非
        //     肯定就是，还要检查一下，如果是，就返回结束地址区的绝对偏移
        m = readLong3(m + 4);
        readIP(m, b4);
        r = compareIP(ip, b4);
        if (r <= 0) return m;
        else return -1;
    }

    /**
     * 得到begin偏移和end偏移中间位置记录的偏移
     *
     * @param begin
     * @param end
     * @return
     */
    private long getMiddleOffset(long begin, long end) {
        long records = (end - begin) / IP_RECORD_LENGTH;
        records >>= 1;
        if (records == 0) records = 1;
        return begin + records * IP_RECORD_LENGTH;
    }

    /**
     * 给定一个国家记录的偏移,返回IPLocation对象
     *
     * @param offset
     * @return
     */
    private IPLocation getIPLocation(long offset) {

        try {
            //跳过4字节ip
            ipFile.seek(offset + 4);
            //读取第一个字节判断是否标志字节
            byte b = ipFile.readByte();
            if (b == REDIRECT_MODE_1) {
                //读取国家偏移
                long countryOffset = readLong3();
                //跳转至偏移处
                ipFile.seek(countryOffset);
                //在检查一次标志字节
                b = ipFile.readByte();
                if (b == REDIRECT_MODE_2) {
                    loc.setCountry(readString(readLong3()));
                    ipFile.seek(countryOffset + 4);
                } else
                    loc.setCountry(readString(countryOffset));

                //读取地区标志
                loc.setArea(readArea(ipFile.getFilePointer()));

            } else if (b == REDIRECT_MODE_2) {
                loc.setCountry(readString(readLong3()));
                loc.setArea(readArea(offset + 8));
            } else {
                loc.setCountry(readString(ipFile.getFilePointer() - 1));
                loc.setArea(readArea(ipFile.getFilePointer()));
            }
            return loc;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * 给定一个ip国家地区记录的偏移，返回一个IPLocation结构，此方法应用与内存映射文件方式
     *
     * @param offset 国家记录的起始偏移
     * @return IPLocation对象
     */
    private IPLocation getIPLocation(int offset) {
        // 跳过4字节ip
        mbb.position(offset + 4);
        // 读取第一个字节判断是否标志字节
        byte b = mbb.get();
        if (b == REDIRECT_MODE_1) {
            // 读取国家偏移
            int countryOffset = readInt3();
            // 跳转至偏移处
            mbb.position(countryOffset);
            // 再检查一次标志字节，因为这个时候这个地方仍然可能是个重定向
            b = mbb.get();
            if (b == REDIRECT_MODE_2) {
                loc.setCountry(readString(readInt3()));
                mbb.position(countryOffset + 4);
            } else
                loc.setCountry(readString(countryOffset));
            // 读取地区标志
            loc.setArea(readArea(mbb.position()));
        } else if (b == REDIRECT_MODE_2) {
            loc.setCountry(readString(readInt3()));
            loc.setArea(readArea(offset + 8));
        } else {
            loc.setCountry(readString(mbb.position() - 1));
            loc.setArea(readArea(mbb.position()));
        }
        return loc;
    }

    /**
     * 从offset偏移开始解析后面的字节，读出一个地区名
     *
     * @param offset 地区记录的起始偏移
     * @return 地区名字符串
     * @throws IOException
     */
    private String readArea(long offset) throws IOException {
        ipFile.seek(offset);
        byte b = ipFile.readByte();
        if (b == REDIRECT_MODE_1 || b == REDIRECT_MODE_2) {
            long areaOffset = readLong3(offset + 1);
            if (areaOffset == 0)
                return Message.unknown_area;
            else
                return readString(areaOffset);
        } else
            return readString(offset);
    }

    /**
     * @param offset 地区记录的起始偏移
     * @return 地区名字符串
     */
    private String readArea(int offset) {
        mbb.position(offset);
        byte b = mbb.get();
        if (b == REDIRECT_MODE_1 || b == REDIRECT_MODE_2) {
            int areaOffset = readInt3();
            if (areaOffset == 0)
                return Message.unknown_area;
            else
                return readString(areaOffset);
        } else
            return readString(offset);
    }

    /**
     * 从offset偏移处读取一个以0结束的字符串
     *
     * @param offset 字符串起始偏移
     * @return 读取的字符串，出错返回空字符串
     */
    private String readString(long offset) {
        try {
            ipFile.seek(offset);
            int i;
            for (i = 0, buf[i] = ipFile.readByte(); buf[i] != 0; buf[++i] = ipFile.readByte()) ;
            if (i != 0)
                return IpUtil.getString(buf, 0, i, "GBK");
        } catch (IOException e) {
            LogFactory.log("", Level.ERROR, e);
        }
        return "";
    }

    /**
     * 从内存映射文件的offset位置得到一个0结尾字符串
     *
     * @param offset 字符串起始偏移
     * @return 读取的字符串，出错返回空字符串
     */
    private String readString(int offset) {
        try {
            mbb.position(offset);
            int i;
            for (i = 0, buf[i] = mbb.get(); buf[i] != 0; buf[++i] = mbb.get()) ;
            if (i != 0)
                return IpUtil.getString(buf, 0, i, "GBK");
        } catch (IllegalArgumentException e) {
            LogFactory.log("", Level.ERROR, e);
        }
        return "";
    }

    /**
     * 从offset位置读取4个字节为一个long
     *
     * @param offset
     * @return
     */
    private long readLong4(long offset) {
        long ret = 0;
        try {
            ipFile.seek(offset);
            ret |= (ipFile.readByte() & 0xFF);
            ret |= ((ipFile.readByte() << 8) & 0xFF00);
            ret |= ((ipFile.readByte() << 16) & 0xFF0000);
            ret |= ((ipFile.readByte() << 24) & 0xFF000000);
            return ret;

        } catch (IOException e) {
            return -1;
        }


    }

    /**
     * 从offset位置读取3个字节为一个long
     *
     * @param offset
     * @return
     */
    private long readLong3(long offset) {
        long ret = 0;
        try {
            ipFile.seek(offset);
            ipFile.readFully(b3);
            ret |= (b3[0] & 0xFF);
            ret |= ((b3[1] << 8) & 0xFF00);
            ret |= ((b3[2] << 16) & 0xFF0000);

            return ret;

        } catch (IOException e) {
            return -1;
        }
    }

    /**
     * 从当前位置读取3个字节为一个long
     *
     * @return
     */
    private long readLong3() {
        long ret = 0;
        try {
            ipFile.readFully(b3);
            ret |= (b3[0] & 0xFF);
            ret |= ((b3[1] << 8) & 0xFF00);
            ret |= ((b3[2] << 16) & 0xFF0000);

            return ret;

        } catch (IOException e) {
            return -1;
        }
    }

    //ipCache的get方法,开放出来主要是供测试
    public Map<String, IPLocation> getIpCache() {
        return ipCache;
    }

    //获得ipCache的set方法不提供
	/*public void setIpCache(Map<String, IPLocation> ipCache) {
		this.ipCache = ipCache;
	}*/
    public String getIP_FILE() {
        return IP_FILE;
    }

    public void setIP_FILE(String iP_FILE) {
        IP_FILE = iP_FILE;
    }

    public String getINSTALL_DIR() {
        return INSTALL_DIR;
    }

    public void setINSTALL_DIR(String iNSTALL_DIR) {
        INSTALL_DIR = iNSTALL_DIR;
    }


}
