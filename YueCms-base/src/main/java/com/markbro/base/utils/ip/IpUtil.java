package com.markbro.base.utils.ip;

import org.apache.log4j.Level;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

/**
 * 工具类
 * 
 * @author wjy
 * 
 */
public class IpUtil {

	private static StringBuilder sb = new StringBuilder();

	/**
	 * 从ip的字符串形式得到字节数组形式
	 * 
	 * @param ip
	 * @return
	 */
	public static byte[] getIpByteArrayFromString(String ip) {
		byte[] ret = new byte[4];
		StringTokenizer st = new StringTokenizer(ip, ".");
		try {
			ret[0] = (byte) (Integer.parseInt(st.nextToken()) & 0xFF);
			ret[1] = (byte) (Integer.parseInt(st.nextToken()) & 0xFF);
			ret[2] = (byte) (Integer.parseInt(st.nextToken()) & 0xFF);
			ret[3] = (byte) (Integer.parseInt(st.nextToken()) & 0xFF);
		} catch (NumberFormatException e) {
			LogFactory.log("从ip的字符串形式得到字节数组形式报错", Level.ERROR, e);
		}
		return ret;
	}

	/**
	 * 从ip的字节数组形式得到ip的字符串形式
	 * 
	 * @param ip
	 * @return
	 */
	public static String getIpStringFromBytes(byte[] ip) {
		sb.delete(0, sb.length());
		sb.append(ip[0] & 0xFF);
		sb.append('.');
		sb.append(ip[1] & 0xFF);
		sb.append('.');
		sb.append(ip[2] & 0xFF);
		sb.append('.');
		sb.append(ip[3] & 0xFF);
		return sb.toString();

	}

	/**
	 * 根据某种编码方式将字节数组转换成字符串
	 * 
	 * @param b
	 *            要转换的字节数组
	 * @param offset
	 *            要转换的起始位置
	 * @param len
	 *            要转换的长度
	 * @param encoding
	 *            编码方式
	 * @return 如果encoding不支持,则返回一个缺省编码的字符串
	 */
	public static String getString(byte[] b, int offset, int len,
			String encoding) {
		try {
			return new String(b, offset, len, encoding);
		} catch (UnsupportedEncodingException e) {
			return new String(b, offset, len);
		}
	}

	/**
	 * 获得本地IP地址
	 * 
	 * @return String
	 */
	public static String getLocalIp() {
		String ip = null;
		try {
			InetAddress addr = InetAddress.getLocalHost();
			ip = addr.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ip;
	}

	/**
	 * 获得本地主机名
	 * 
	 * @return
	 */
	public static String getLocalHostName() {
		String hostName = null;
		try {
			InetAddress addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
		} catch (UnknownHostException e) {

			e.printStackTrace();
		}
		return hostName;
	}

	/**
	 * 根据域名获得IP地址
	 * 
	 * @param hostName
	 * @return String
	 */
	public static String getIpByHostName(String hostName) {
		String ip = null;
		try {
			InetAddress addr = InetAddress.getByName(hostName);
			ip = addr.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ip;
	}

	/**
	 * 获得域名对应的所有IP地址数组
	 * 
	 * @param hostName
	 * @return String[]
	 */
	public static String[] getAllIpByHostName(String hostName) {
		String[] ips = null;
		try {
			InetAddress[] addr = InetAddress.getAllByName(hostName);
			int len = addr.length;
			if (len > 0) {
				ips = new String[len];
				for (int i = 0; i < len; i++) {
					ips[i] = addr[i].getHostAddress();
				}
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ips;
	}

	/**
	 * 通过http请求获得客户端真实ip地址
	 * 
	 * @param request
	 * @return String
	 */
	public static String getIpByHttpRequest(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;

	}
	/**
	 * 将字符串类型的ip转换成long类型
	 * @param strIp
	 * @return
	 */
	public static long ipToLong(String strIp){
		long[] ip = new long[4];
		// 先找到IP地址字符串中.的位置
		int position1 = strIp.indexOf(".");
		int position2 = strIp.indexOf(".", position1 + 1);
		int position3 = strIp.indexOf(".", position2 + 1);
		// 将每个.之间的字符串转换成整型
		ip[0] = Long.parseLong(strIp.substring(0, position1));
		ip[1] = Long.parseLong(strIp.substring(position1 + 1, position2));
		ip[2] = Long.parseLong(strIp.substring(position2 + 1, position3));
		ip[3] = Long.parseLong(strIp.substring(position3 + 1));
		return (ip[0] << 24) + (ip[1] << 16) + (ip[2] << 8) + ip[3];
	}
	/**
	 * 将long类型ip转换成字符串类型ip
	 * @param longIp
	 * @return String
	 */
	public static String longToIP(long longIp){
        StringBuilder sb = new StringBuilder("");
        //直接右移24位
        sb.append(String.valueOf((longIp >>> 24)));
        sb.append(".");
        //将高8位置0，然后右移16位
        sb.append(String.valueOf((longIp & 0x00FFFFFF) >>> 16));
        sb.append(".");
        //将高16位置0，然后右移8位
        sb.append(String.valueOf((longIp & 0x0000FFFF) >>> 8));
        sb.append(".");
        //将高24位置0
        sb.append(String.valueOf((longIp & 0x000000FF)));
        return sb.toString();
}
	public static void main(String[] args) {
		System.out.println(longToIP(3232235779L));
	}

}
