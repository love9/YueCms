package com.markbro.base.utils;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.base.utils.string.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 全局常量
 * @author wujiyue
 */
public class GlobalConfig {
    private static  final Logger logger= LoggerFactory.getLogger(GlobalConfig.class);
    private static Properties props = System.getProperties();
    public static Date buildWebsiteDate = Date.from(LocalDate.of(2019,6,1).atStartOfDay(ZoneId.systemDefault()).toInstant());

    /**
     * 保存配置文件属性值
     */
    private static Map<String, String> propertiesMap = Maps.newHashMap();

    //存放sys_param表配置的系统参数
    private static Map<String, String> sysParaMap = Maps.newHashMap();
    //存放字典数据 value是json格式字符串结构如List<Map<String,Object>>
    private static Map<String,String > dictMap = new HashMap<>();

    public static boolean SSO_ENABLE;//SSO是否启用
    static {
        propertiesMap= PropertiesListenerConfig.getAllProperty();
        Properties prop = new Properties();
        try {
            InputStream inputStream = GlobalConfig.class.getClassLoader().getResourceAsStream("config/sso.properties");
            prop.load(inputStream);
            String str = prop.getProperty("sso.enable");
            if(StringUtils.isEmpty(str)){
                SSO_ENABLE = false;
            }else{
                SSO_ENABLE = Boolean.parseBoolean(str);
            }
        } catch (IOException e) {
            logger.error("参数文件{}读取出错","config/sso.properties");
        }
    }



    /**
     * 获取properties配置
     */
    public static String getConfig(String key) {
        if(key.contains(".")){
            key=key.replaceAll("\\.","_");
        }
        String value = propertiesMap.get(key);
        return value;
    }
    /**
     * 获取sys_para配置
     */
    public static String getSysPara(String key) {
        String value = sysParaMap.get(key);
        if(StringUtil.isEmpty(value)){
            value = SysPara.getValue(key,"");
        }
        if(StringUtil.isEmpty(value)){
            throw new ApplicationException("sysParam key["+key+"] is null!");
        }
        return value;
    }
    public static String getSysPara(String key,String defaultValue) {
        String value = sysParaMap.get(key);
        if(StringUtil.isEmpty(value)){
            value = SysPara.getValue(key,defaultValue);
        }
        return value;
    }
    public static void putSysPara(String key,String value) {
        sysParaMap.put(key,value);
    }

    public static String getDict(String type) {
        return dictMap.get(type);
    }
    public static void putDict(String type,String value) {
        dictMap.put(type,value);
    }
    /**
     * 获取配置文件所有属性值
     * @return
     */
    public static Map<String, String> getAllProperty() {
        return propertiesMap;
    }

    public static Map<String, String> getAllSysPara() {
        return sysParaMap;
    }

    public static Map<String, String> getAllDict() {
        return dictMap;
    }
    /**
     * 获得系统配置
     * @param request
     * @return
     */
    public static Map<String,String> getSysConfig(HttpServletRequest request){
        //java版本
        String javaVersion = props.getProperty("java.version");
        //操作系统名称
        String osName = props.getProperty("os.name") + props.getProperty("os.version");
        //用户的主目录
        String userHome = props.getProperty("user.home");
        //用户的当前工作目录
        String userDir = props.getProperty("user.dir");
        //CPU个数
        String cpu = Runtime.getRuntime().availableProcessors() + "核";
        //虚拟机内存总量
        String totalMemory = (Runtime.getRuntime().totalMemory() / 1024 / 1024) + "M";
        //虚拟机空闲内存量
        String freeMemory = (Runtime.getRuntime().freeMemory() / 1024 / 1024) + "M";
        //虚拟机使用的最大内存量
        String maxMemory = (Runtime.getRuntime().maxMemory() / 1024 / 1024) + "M";
        //String mysqlVersion=DbUtil.getJtN().queryForObject("select version()",String.class);
        String mysqlVersion="";
        mysqlVersion=(String) EhCacheUtils.getSysInfo("mysqlVersion");
        if(StringUtil.isEmpty(mysqlVersion)){
            mysqlVersion= DbUtil.getJtN().queryForObject("select version()",String.class);
        }

        String systemVersion="";
        systemVersion=(String)EhCacheUtils.getSysInfo("system_version");
        String systemName=(String)EhCacheUtils.getSysInfo("system_name");
        String systemUpdateTime=(String)EhCacheUtils.getSysInfo("system_update_time");
        String clientIP="",serverIP="",webVersion="",webRootPath="";
        if(request!=null){
            //服务器IP
            serverIP = request.getLocalAddr();
            //客户端IP
            clientIP = request.getRemoteHost();
            //WEB服务器
            webVersion = request.getServletContext().getServerInfo();
            webRootPath = request.getSession().getServletContext().getRealPath("");
        }

        Map<String, String> propsMap = ImmutableMap.<String, String>builder() .put("javaVersion", javaVersion)
                .put("osName", osName)
                .put("userHome", userHome)
                .put("userDir", userDir)
                .put("clientIP", clientIP)
                .put("serverIP", serverIP)
                .put("cpu", cpu)
                .put("totalMemory", totalMemory)
                .put("freeMemory", freeMemory)
                .put("maxMemory", maxMemory)
                .put("webVersion", webVersion)
                .put("mysqlVersion", mysqlVersion)
                .put("webRootPath", webRootPath)
                .put("systemVersion", systemVersion)
                .put("systemName", systemName)
                .put("systemUpdateTime", systemUpdateTime)
                .build();
        return propsMap;
    }

    /**
     * 登录是否启用验证码
     */
    public static Boolean loginWithValidateCode() {
        String s= getConfig("loginWithValidateCode");
        if("true".equals(s)||"1".equals(s)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 是否是演示模式，演示模式下不能修改用户、角色、密码、菜单、授权
     */
    public static Boolean isDemoMode() {
        String dm = getConfig("demoMode");
        return "true".equals(dm) || "1".equals(dm);
    }
    /**
     * 是否是开发模式
     */
    public static Boolean isDevMode() {
        String dm = getConfig("devMode");
        return "true".equals(dm) || "1".equals(dm);
    }
}
