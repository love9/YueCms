package com.markbro.base.utils.random;


import java.math.BigDecimal;

/**
 * 有关数据处理和转换的工具类
 * @author wujiyue
 * @date 2014年9月9日 00:12:32
 * @version 1.0
 */
public class NumberUtil {
	//默认除法运算精度
	private static final int DEF_DIV_SCALE = 10;
	/**
	 * 提供精确的减法运算。
	 * @param v1 被减数
	 * @param v2 减数
	 * @return 两个参数的差
	 */
	public static double sub(double v1,double v2){
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.subtract(b2).doubleValue();
	}
	/**
	 * 提供精确的加法运算。
	 * @param v1 被加数
	 * @param v2 加数
	 * @return 两个参数的和
	 */
	public static double add(double v1,double v2){
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.add(b2).doubleValue();
	}
	/**
	 * 提供精确的乘法运算。
	 * @param v1 被乘数
	 * @param v2 乘数
	 * @return 两个参数的积
	 */
	public static double mul(double v1,double v2){
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.multiply(b2).doubleValue();
	}

	/**
	 * 提供（相对）精确的除法运算，当发生除不尽的情况时，精确到
	 * 小数点以后10位，以后的数字四舍五入。
	 * @param v1 被除数
	 * @param v2 除数
	 * @return 两个参数的商
	 */
	public static double div(double v1,double v2){
		return div(v1,v2,DEF_DIV_SCALE);
	}
	/**
	 * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指
	 * 定精度，以后的数字四舍五入。
	 * @param v1 被除数
	 * @param v2 除数
	 * @param scale 表示表示需要精确到小数点以后几位。
	 * @return 两个参数的商
	 */
	public static double div(double v1,double v2,int scale){
		if(scale<0){
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.divide(b2,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	/**
	 * 获得一个数小数点后面有几位小数
	 * @param num 数字
	 * @return int 小数点后面位数
	 */
	public static int getAccuracy(Object num){
		String t=String.valueOf(num);
		return t.length()-1-t.indexOf('.');
	}
	/**
	 * String转换double
	 * @param dataStr
	 * @return double
	 */
	public static double convertSourData(String dataStr){
		if(dataStr!=null&&!"".equals(dataStr)){
			return Double.valueOf(dataStr);
		}
		throw new NumberFormatException("convert error!");
	}
	/**
	 * 使用率计算
	 *
	 * @return
	 */
	public static String getUsage(long free, long total) {
		Double d = new BigDecimal(free * 100 / total).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
		return String.valueOf(d);
	}
	public static void main(String[] args) {
//		int[] temp=RandomUtil.getRanNums(4, 0, 9);
		double d=1.2;
		
		//System.out.println(StringUtil.chars2String(chs));

	}

}
