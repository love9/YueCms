package com.markbro.base.utils;


import com.markbro.base.utils.string.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.incrementer.MySQLMaxValueIncrementer;
import org.springframework.jdbc.support.incrementer.OracleSequenceMaxValueIncrementer;
import org.springframework.jdbc.support.incrementer.SqlServerMaxValueIncrementer;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * 数据库工具类
 * @author wujiyue
 */
@SuppressWarnings("static-access")
public class DbUtil {
	private static Logger logger = LoggerFactory.getLogger(DbUtil.class);
	private static DataSource ds=SpringContextHolder.getBean("dataSource");

	private static MySQLMaxValueIncrementer mysqlInc;
	private static SqlServerMaxValueIncrementer sqlserverInc;
	private static OracleSequenceMaxValueIncrementer oracleInc;


	/**
	 * 获取数据库 Catalog
	 * @return
	 */
	public static List<String> getAllCatalog() {
		List<String> catalogs = new ArrayList<String>();
		Connection conn=null;
		try {
			conn=ds.getConnection();
			DatabaseMetaData meta = conn.getMetaData();
			ResultSet rs = meta.getCatalogs();
			while (rs.next()) {
				catalogs.add(rs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return catalogs;
	}

	/**
	 * 获取当前数据库的Catalog
	 * @return
	 */
	public static String getCatalog() {
		StringBuffer catalog = new StringBuffer();
		Connection conn=null;
		try {
			conn=ds.getConnection();
			catalog.append(conn.getCatalog());
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return catalog.toString();
	}
	/**
	 * 获取当前连接数据库的Schema
	 * @return
	 */
	public static String getSchema() {
		final StringBuffer schema = new StringBuffer();
		Connection conn=null;
		try {
			conn=ds.getConnection();
			schema.append(conn.getSchema());
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "";
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return schema.toString();
	}

	/**
	 * 获取数据库表
	 * @param catalog
	 * @return
	 */
	public static List<String> getTables( String catalog,  String schema) {
		List<String> tableNames = new ArrayList<String>();
		DatabaseMetaData meta = null;
		Connection conn=null;
		try {
			conn=ds.getConnection();
			meta = conn.getMetaData();
			if(StringUtil.isEmpty(schema)){
				schema=getSchema();
			}
			ResultSet rs = meta.getTables(catalog == null ? conn.getCatalog() : catalog, StringUtil.isEmpty(schema) ?  null: schema, "%", new String[]{"TABLE"});
			while (rs.next()) {
				tableNames.add(rs.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return tableNames;
	}

	/**
	 * 获取当前数据库所有表名
	 * @return
	 */
	public static List<String> getTables() {
		return getTables(null, null);
	}


	/**
	 * 获取外键
	 *
	 * @param catalog
	 * @param schema
	 * @param table
	 * @return Map<表名,列名>
	 */
	public static Map<String, String> getForgetKeys(  String catalog,   String schema,   String table) {
		  Map<String, String> map = new HashMap<>();

		Connection conn= null;
		try {
			conn = ds.getConnection();
			DatabaseMetaData databaseMetaData = conn.getMetaData();
			//获取外键信息
			ResultSet rs_for = databaseMetaData.getImportedKeys(catalog == null ? conn.getCatalog() : catalog, schema == null ? conn.getSchema() : schema, table);
			while (rs_for.next()) {
				map.put(rs_for.getString(8), rs_for.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}
	/**
	 * 获取当前数据库中的表的外键
	 * @param table
	 * @return
	 */
	public static Map<String, String> getForgetKeys(final String table) {
		return getForgetKeys(null, null, table);
	}

	/**
	 * 获取主键
	 * @param catalog
	 * @param schema
	 * @param table
	 * @return
	 */
	public static Map<String, String> getPrimaryKeys( String catalog,  String schema,  String table) {

		Map<String, String> map = new HashMap<>();
		Connection conn= null;
		try {
			conn = ds.getConnection();
			DatabaseMetaData databaseMetaData = conn.getMetaData();
			//获取外键信息
			ResultSet rs_for = databaseMetaData.getPrimaryKeys(catalog == null ? conn.getCatalog() : catalog, schema == null ? conn.getSchema() : schema, table);
			while (rs_for.next()) {
				map.put(rs_for.getString(8), rs_for.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return map;
	}

	/**
	 * 获取主键
	 *
	 * @param table
	 * @return
	 */
	public static Map<String, String> getPrimaryKeys( String table) {
		return getPrimaryKeys(null, null, table);
	}


	/**
	 * 获取表字段
	 *
	 * @param catalog
	 * @param schema
	 * @param table
	 * @return
	 */
	public static List<String> getColumns( String catalog,  String schema,  String table) {
		List<String> columns = new ArrayList<>();
		Connection conn = null;
		try {
			conn = ds.getConnection();
			//获取列备注信息
			DatabaseMetaData databaseMetaData = conn.getMetaData();
			ResultSet rs = databaseMetaData.getColumns(catalog == null ? conn.getCatalog() : catalog, schema == null ? conn.getSchema() : schema, table, "%");
			while (rs.next()) {
				String name = rs.getString("COLUMN_NAME");
				columns.add(name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return columns;
	}

	/**
	 * 获取表列名
	 *
	 * @param table
	 * @return
	 */
	public static List<String> getColumns(final String table) {
		return getColumns(null, null, table);
	}

	/**
	 * 获取表列名和备注
	 * @param catalog
	 * @param schema
	 * @param table
	 * @return
	 */
	public static Map<String, String> getColumnRemarks( String catalog,  String schema,  String table) {
		Map<String, String> columns = new LinkedHashMap<String, String>();
		Connection conn= null;
		try {
			conn = ds.getConnection();
			//获取列备注信息
			DatabaseMetaData databaseMetaData = conn.getMetaData();
			ResultSet rs = databaseMetaData.getColumns(catalog == null ? conn.getCatalog() : catalog, schema == null ? conn.getSchema() : schema, table, "%");
			while (rs.next()) {
				String name = rs.getString("COLUMN_NAME");
				String remarks = rs.getString("REMARKS");
				columns.put(name, remarks);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return columns;
	}

	/**
	 * 获取当前数据库中的表的列名
	 *
	 * @param table
	 * @return K：字段名 V：备注
	 */
	public static Map<String, String> getColumnRemarks(final String table) {
		return getColumnRemarks(null, null, table);
	}

	/**
	 * 获取表字段
	 *
	 * @param catalog
	 * @param schema
	 * @param table
	 */
	public static Map<String, Map<String, Object>> getColumnFull(  String catalog,   String schema,   String table) {
		Map<String, Map<String, Object>> mapInfo = new LinkedHashMap<>();
		Connection conn= null;
		try {
			conn = ds.getConnection();
			String sql = "select * from " + table;
			String newCatalog = catalog == null ? conn.getCatalog() : catalog;
			String newSchema = schema == null ? conn.getSchema() : schema;

			DatabaseMetaData dbMetaData = conn.getMetaData();
			//获取主键，用于判断字段是否是主键
			List<String> primaryKeys = new ArrayList<>();
			ResultSet rs = dbMetaData.getPrimaryKeys(newCatalog, newSchema, table);
			while (rs.next()) {
				primaryKeys.add(rs.getString(4));
			}
			//获取表结构信息
			String oldCatalog = conn.getCatalog();
			conn.setCatalog(newCatalog);
			Statement stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData metaData = rs.getMetaData();
			int count = metaData.getColumnCount();
			for (int i = 1; i <= count; i++) {
				Map<String, Object> map = new LinkedHashMap<>();
				String columnName = metaData.getColumnName(i);//名称
				String columnType = dbType2JavaType(metaData.getColumnTypeName(i),metaData.getPrecision(i),metaData.getScale(i));//对应Java类型
				boolean nullable = metaData.isNullable(i) == ResultSetMetaData.columnNullable;//是否可以为null
				boolean primary = primaryKeys.contains(columnName);
				//首字母大写字段名
				String name = StringUtil.firstCharToUpperCase(getFieldName(columnName));
				//首字母小写字段名
				String fieldName = StringUtil.firstCharToLowerCase(name);
				map.put("name", name);
				map.put("fieldName", fieldName);//用于实体字段
				map.put("columnName", columnName);//用于@Column
				map.put("type", columnType.replaceAll("java.lang.", ""));
				map.put("nullable", nullable);
				map.put("primary", primary);
				map.put("length", metaData.getColumnDisplaySize(i));//长度
				map.put("readOnly", metaData.isReadOnly(i));//是否只读
				map.put("writable", metaData.isWritable(i));//是否可写
				map.put("autoIncrement", metaData.isAutoIncrement(i));//是否自动增长

				mapInfo.put(columnName, map);
			}
			//还原
			conn.setCatalog(oldCatalog);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return mapInfo;
	}
	/**
	 * 获取当前数据库中的表的列
	 *
	 * @param table
	 * @return
	 */
	public static Map<String, Map<String, Object>> getColumnFull(final String table) {
		return getColumnFull(null, null, table);
	}

	/**
	 * 获取数据库相关信息
	 * @return
	 */
	public static Map<String, String> getDBInfo() {
		Map<String, String> metaMap = new HashMap<>();
		Connection conn= null;
		try {
			conn = ds.getConnection();
			DatabaseMetaData meta = conn.getMetaData();
			metaMap.put("url", meta.getURL());//连接
			metaMap.put("user", meta.getUserName());//用户名
			metaMap.put("driver", meta.getDriverName());//驱动
			metaMap.put("type", meta.getDatabaseProductName());//数据库类型
			metaMap.put("version", meta.getDatabaseProductVersion());//数据库版本
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return metaMap;
	}
	/**
	 * 获取字段名
	 * 将下划线风格的命名方式，转换为驼峰式命名方式
	 *
	 * @param columnName 表字段名
	 * @return
	 */
	private static String getFieldName(String columnName) {
		String[] names = columnName.split("_");
		if (names.length > 1) {
			StringBuffer sb = new StringBuffer();
			for (String n : names) {
				sb.append(StringUtil.firstCharToUpperCase(n));
			}
			columnName = sb.toString();
		}
		return columnName;
	}
	/**
	 * 获取字段类型
	 *
	 * @param columnType
	 * @return
	 */
	private static String getColumnType(String columnType) {
		if (columnType.equals("java.lang.Double")) {
			columnType = "java.math.BigDecimal";
		} else if (columnType.equals("java.lang.Long")) {
			columnType = "java.math.BigDecimal";
		} else if (columnType.equals("java.sql.Timestamp")) {
			columnType = "java.util.Date";
		} else if (columnType.equals("java.sql.Date")) {
			columnType = "java.util.Date";
		} else if (columnType.equals("java.sql.Time")) {
			columnType = "java.util.Date";
		} else if (columnType.equals("java.lang.Short")) {
			columnType = "java.lang.Integer";
		} else if (columnType.equals("[B")) {//BLOB类型
			columnType = "java.lang.String";
		} else if (columnType.equals("java.lang.Boolean")) {
			columnType = "boolean";
		}
		return columnType;
	}

	//数据库类型转java类型
	public static String dbType2JavaType(String dataType, int precision, int scale) {
		dataType = dataType.toLowerCase();
		if(dataType.contains("char")) {
			dataType = "java.lang.String";
		} else if(dataType.contains("varchar")) {
			dataType = "java.lang.String";
		}
		else if(dataType.contains("int")) {
			dataType = "java.lang.Integer";
		} else if(dataType.contains("float")) {
			dataType = "java.lang.Float";
		} else if(dataType.contains("double")) {
			dataType = "java.lang.Double";
		} else if(dataType.contains("number")) {
			if(scale > 0) {
				dataType = "java.math.BigDecimal";
			} else if(precision > 6) {
				dataType = "java.lang.Long";
			} else {
				dataType = "java.lang.Integer";
			}
		} else if(dataType.contains("decimal")) {
			dataType = "BigDecimal";
		} else if(dataType.contains("date")) {
			dataType = "java.lang.String";
		} else if(dataType.contains("time")) {
			dataType = "java.lang.String";
		} else if(dataType.contains("clob")) {
			dataType = "java.sql.Clob";
		} else if(dataType.equals("bit")) {
			dataType = "java.lang.Boolean";
		} else {
			dataType = "java.lang.Object";
		}

		return dataType;
	}
	public static int getNextId() throws Exception {
		return getNextId(ds);
	}


	public static SimpleJdbcInsert getSji() {
		SimpleJdbcInsert sji = new SimpleJdbcInsert(ds);
		return sji;
	}
	public static int getNextId(DataSource ds) throws Exception {
		if (SysPara.getValue("db_type").equals("mysql")) {
			if (mysqlInc == null) {
				mysqlInc = new MySQLMaxValueIncrementer(ds, "sys_sequence", "id");
				mysqlInc.setCacheSize(10);
			}
			return mysqlInc.nextIntValue();
		}
		if (SysPara.getValue("db_type").equals("sqlserver")) {
			if (sqlserverInc == null) {
				sqlserverInc = new SqlServerMaxValueIncrementer(ds, "sys_sequence", "id");
				sqlserverInc.setCacheSize(10);
			}
			return sqlserverInc.nextIntValue();
		}
		if (SysPara.getValue("db_type").equals("oracle")) {
			if (oracleInc == null) {
				oracleInc = new OracleSequenceMaxValueIncrementer(ds, "sys_sequence");
			}
			return oracleInc.nextIntValue();
		}
		throw new Exception("sys_para.csdm>db_type不匹配");
	}

	public static String getStrNextId() throws Exception {
		return getStrNextId(ds);
	}
	public static Integer getIntNextId() throws Exception {
		return getIntNextId(ds);
	}

	public static Integer getIntNextId(DataSource ds) throws Exception {

		if (SysPara.getValue("db_type").equals("mysql")) {
			if (mysqlInc == null) {
				mysqlInc = new MySQLMaxValueIncrementer(ds, "sys_sequence", "id");
				mysqlInc.setCacheSize(10);
			}
			return mysqlInc.nextIntValue();
		}
		if (SysPara.getValue("db_type").equals("sqlserver")) {
			if (sqlserverInc == null) {
				sqlserverInc = new SqlServerMaxValueIncrementer(ds, "sys_sequence", "id");
				sqlserverInc.setCacheSize(10);
			}
			return sqlserverInc.nextIntValue();
		}
		if (SysPara.getValue("db_type").equals("oracle")) {
			if (oracleInc == null) {
				oracleInc = new OracleSequenceMaxValueIncrementer(ds, "sys_sequence");
			}
			return oracleInc.nextIntValue();
		}
		throw new Exception("sys_para.csdm>db_type不匹配");
	}
	public static String getStrNextId(DataSource ds) throws Exception {

		String xzqh = SysPara.getValue("sys_xzqh","");

		xzqh = xzqh + ":";
		if (SysPara.getValue("db_type").equals("mysql")) {
			if (mysqlInc == null) {
				mysqlInc = new MySQLMaxValueIncrementer(ds, "sys_sequence", "id");
				mysqlInc.setCacheSize(10);
			}
			return xzqh + mysqlInc.nextIntValue();
		}
		if (SysPara.getValue("db_type").equals("sqlserver")) {
			if (sqlserverInc == null) {
				sqlserverInc = new SqlServerMaxValueIncrementer(ds, "sys_sequence", "id");
				sqlserverInc.setCacheSize(10);
			}
			return xzqh + sqlserverInc.nextIntValue();
		}
		if (SysPara.getValue("db_type").equals("oracle")) {
			if (oracleInc == null) {
				oracleInc = new OracleSequenceMaxValueIncrementer(ds, "sys_sequence");
			}
			return xzqh + oracleInc.nextIntValue();
		}
		throw new Exception("sys_para.csdm>db_type不匹配");
	}


	public static JdbcTemplate getJt(DataSource ds) {
		JdbcTemplate jt = new JdbcTemplate();
		jt.setDataSource(ds);
		return jt;
	}

	public static NamedParameterJdbcTemplate getNpjt(DataSource ds) {
		NamedParameterJdbcTemplate jt = new NamedParameterJdbcTemplate(ds);
		return jt;
	}

	public static JdbcTemplate getJtN() {
		return getJt(ds);
	}

	public static NamedParameterJdbcTemplate getNpjtN() {
		return getNpjt(ds);
	}

	public static DataSource getDs() {
		return ds;
	}

}
