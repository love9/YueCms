package com.markbro.base.utils;


import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 系统参数
 * @author wujiyue
 */
public class SysPara {

	public void reload() {
		EhCacheUtils.clear(EhCacheUtils.SYS_CACHE);
		try {
			getFromDb();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void clear() {
		EhCacheUtils.clear(EhCacheUtils.SYS_CACHE);
	}

	public static String getValue(String csdm) throws Exception {
		//如果缓存还没有db_type信息，说明系统还没加载系统参数信息
		if (EhCacheUtils.getSysInfo("db_type") == null) {
			getFromDb();
		}
		if (EhCacheUtils.getSysInfo(csdm) != null) {
			return (String) EhCacheUtils.getSysInfo(csdm);
		}
		throw new Exception("系统参数：没有查找到" + csdm + "对应的值");
	}
	public static String getValue(String csdm,String defaultValue){
		try{
			return getValue(csdm);
		}catch (Exception ex){
			ex.printStackTrace();
			return defaultValue;
		}
	}
	public static boolean compareValue(String csdm, String cvalue) {
		try {
			return (cvalue != null) && (cvalue.equals(getValue(csdm)));
		} catch (Exception e) {
		}
		return false;
	}

	public static boolean compareValue(String csdm, String cvalue, String dvalue) {
		String value = "";
		try {
			value = getValue(csdm);
		} catch (Exception e) {
			value = dvalue;
		}
		return (cvalue != null) && (cvalue.equals(value));
	}

	public static void updateSysPara(String key, String value) {
		EhCacheUtils.putSysInfo(key, value);
	}

	public static String getDbType() throws Exception {
		return getValue("db_type").toLowerCase();
	}
	@SuppressWarnings("rawtypes")
	private static void getFromDb() throws Exception {
		try {
			//从数据库查询需要放到系统缓存中的键、值
			List<Map<String,Object>> list = DbUtil.getJtN().queryForList("select csdm,csz from sys_para where deleted=0");
			if (list.size() == 0) {
				throw new Exception("系统参数信息没有配置");
			}
			for (Iterator iter = list.iterator(); iter.hasNext();) {
				Map map = (Map) iter.next();
				EhCacheUtils.putSysInfo((String) map.get("csdm"), map.get("csz"));
			}
		} catch (Exception e) {
			throw new Exception("获取sys_para表数据时出现异常:" + e.getMessage());
		}
	}

}
