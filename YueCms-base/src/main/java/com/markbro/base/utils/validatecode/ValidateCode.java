package com.markbro.base.utils.validatecode;


import com.markbro.base.utils.random.RandomUtil;
import com.markbro.base.utils.string.StringUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * 验证码图片生产工具
 * 
 * @author wujiyue
 * 
 */
public class ValidateCode {
	private Random r = new Random();
	private int CODE_LENGTH;// 验证码长度
	private int OvalLine_Num;// 椭圆干扰线数目
	private int Line_num;// 直线干扰线数目
	private int width, height = 40;// 验证码图片宽高
	private boolean Rotate_Flag;// 字符旋转标志
	private String[] FontFamily = { "Times New Roman", "宋体", "黑体",
			"Arial Unicode MS", "Lucida Sans" };
	private LEVEL level = LEVEL.MIDDLE;

	// 验证码3个难度级别
	public enum LEVEL {
		EASY, MIDDLE, HARD
	}

	private static volatile ValidateCode instance = null;

	public static ValidateCode getInstance() {
		if (instance == null) {
			synchronized (ValidateCode.class) {
				if (instance == null) {
					instance = new ValidateCode();
				}
			}
		}
		return instance;
	}

	private ValidateCode() {

	}

	/**
	 * 当改变LEVEL级别的时候调用重置属性值
	 */
	private void reset() {
		CODE_LENGTH = OvalLine_Num = Line_num = 0;
		Rotate_Flag = false;
	}

	/**
	 * 获取验证码字符串
	 * 
	 * @return
	 */
	public String getCodeString() {
		char[] codeChars = null;// 存储验证码字符数组
		switch (this.level) {
		case EASY:
		case MIDDLE:
			String temp = RandomUtil.getNumsString(CODE_LENGTH);
			codeChars = temp.toCharArray();
			break;
		case HARD:
			codeChars = RandomUtil.getRanCodes(CODE_LENGTH);
			break;
		}
		String res = StringUtil.chars2String(codeChars);
		return res;
	}

	/**
	 * 绘制椭圆形干扰线
	 * 
	 * @param g
	 */
	private void drawOvalLine(Graphics2D g) {
		Random r = new Random();
		// 绘制椭圆形干扰线
		for (int i = 0; i < OvalLine_Num; i++) {
			g.setColor(RandomUtil.getRandColor(50, 100));
			int x = r.nextInt(width);
			int y = r.nextInt(height);
			g.drawOval(x, y, width, height);// 画椭圆
		}
	}

	/**
	 * 绘制直线干扰线
	 * 
	 * @param g
	 */
	private void drawLine(Graphics2D g) {
		// 生成干扰线
		for (int i = 0; i < Line_num; i++) {
			int x1 = r.nextInt(width);
			int y1 = r.nextInt(height);

			int x2 = r.nextInt(width);
			int y2 = r.nextInt(height);

			g.drawLine(x1, y1, x2, y2);
		}

	}

	/**
	 * 设置字体
	 * 
	 * @param g
	 */
	private void setFont(Graphics2D g) {
		switch (this.level) {
		case HARD:
			g.setFont(new Font(FontFamily[r.nextInt(FontFamily.length)], Font.BOLD ,22+r.nextInt(8)));
			break;
		default:
			g.setFont(new Font(FontFamily[r.nextInt(FontFamily.length)], Font.PLAIN, 30));
		}
	}

	/**
	 * 获得验证码图片
	 * 
	 * @return
	 */
	public BufferedImage getImage(String codeString) {
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) image.getGraphics();
		// 填充背景颜色
		g.setColor(RandomUtil.getRandColor(180, 250));
		g.fillRect(0, 0, width, height);
		drawOvalLine(g);
		setFont(g);
		int tempx = 0, tempy = 0;
		for (int i = 0; i < CODE_LENGTH; i++) {
			String temp = String.valueOf(codeString.charAt(i));
			g.setColor(RandomUtil.getRandColor(20, 120));
			tempx = 8 + 17 * i;
			tempy = 30;
			if (this.Rotate_Flag) {// 设置字体旋转
				int degree = RandomUtil.getARanNum(8);// 获得一个随机数作为旋转角度
				// 得到旋转角度的正反.正反个一半概率
				if (r.nextInt(2) == 1) {
					g.rotate(Math.toRadians(degree), tempx, 20);
				} else {
					g.rotate(-Math.toRadians(degree), tempx, 20);
				}
			}
			drawLine(g);
			g.drawString(temp, tempx, tempy);

		}
		g.dispose();// 图片生效
		return image;

	}

	private int getCODE_LENGTH() {
		return CODE_LENGTH;
	}

	private void setCODE_LENGTH(int cODE_LENGTH) {
		CODE_LENGTH = cODE_LENGTH;
	}

	private int getOvalLine_Num() {
		return OvalLine_Num;
	}

	private void setOvalLine_Num(int ovalLine_Num) {
		OvalLine_Num = ovalLine_Num;
	}

	private int getLine_num() {
		return Line_num;
	}

	private void setLine_num(int line_num) {
		Line_num = line_num;
	}

	private boolean isRotate_Flag() {
		return Rotate_Flag;
	}

	private void setRotate_Flag(boolean rotate_Flag) {
		Rotate_Flag = rotate_Flag;
	}

	public LEVEL getLevel() {
		return level;
	}

	public void setLevel(LEVEL level) {
		reset();
		this.level = level;
		switch (level) {
		case EASY:
			this.setCODE_LENGTH(3);
			this.setLine_num(1);
			this.setRotate_Flag(false);
			break;
		case MIDDLE:
			this.setCODE_LENGTH(4);
			this.setLine_num(2);
			this.setOvalLine_Num(2);
			this.setRotate_Flag(true);
			break;
		case HARD:
			this.setCODE_LENGTH(5);
			this.setLine_num(3);
			this.setOvalLine_Num(3);
			this.setRotate_Flag(true);
		}
		width = CODE_LENGTH * 18 + 15;
	}

}
