package com.markbro.base.utils.tree;


import com.markbro.base.utils.string.StringUtil;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import java.util.*;

/**
 * @author wujiyue
 */
public class TreeUtils {
    /**
     * List转树的方法
     * column = [id, mc, parentid]
     * 创建时间　2014-1-3
     * 创建人　yangsl
     *
     * @param m2tList
     * @param column
     * @return
     */
    public static String getTreeJsonString(final List<Map<String, Object>> m2tList,
                                           final String[] column, final String rootName) {
        if(column.length < 4){
            return "";
        }
        Map<String, List<EasyUITreeNode>> arrayListMap = getTreeMap(m2tList, column, rootName);
        return listToString(arrayListMap);
    }
    public static String getTreeJsonString(final List<Map<String, Object>> m2tList,
                                           final String[] column, final String rootName,boolean closeSecondLevel) {
        if(column.length < 4){
            return "";
        }
        Map<String, List<EasyUITreeNode>> arrayListMap = getTreeMap(m2tList, column, rootName,closeSecondLevel);
        return listToString(arrayListMap);
    }
    /**
     * 生成层次形式的JSON
     *
     * 创建时间  2014-2-21 上午10:12:04
     * 创建人 yangsl
     * @param arrayListMap
     * @return
     *
     */
    private static String listToString(Map<String, List<EasyUITreeNode>> arrayListMap, String... args){
        for (Map.Entry<String, List<EasyUITreeNode>> entry : arrayListMap.entrySet()) {
            List<EasyUITreeNode> smallTreeList = new ArrayList<EasyUITreeNode>();
            smallTreeList = entry.getValue();
            int nodeListSize = smallTreeList.size();
            for (int i = 0; i < nodeListSize; i++) {
                String findID = smallTreeList.get(i).getId();
                List<EasyUITreeNode> findList = arrayListMap.get(findID);
                smallTreeList.get(i).setChildren(findList);
            }
        }
        String parentKey = "";
        if(args != null && args.length > 0 && !args[0].equals("")){
            parentKey = args[0];
        }else{
            parentKey = "-1";
        }
        List<EasyUITreeNode> rootNodeList = arrayListMap.get(parentKey);//TmConstant.NUM_UNONE
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);//自动为我排除circle。
        JSONArray jsonArray = JSONArray.fromObject(rootNodeList, jsonConfig);
        return jsonArray.toString();
    }
    /**
     * List转map的方法
     * column = [id, text, sjid, checked, othermc....]
     * 创建时间　2014-1-3
     * 创建人　yangsl
     *
     * @param m2tList
     * @param column
     * @return
     */
    public static Map<String, List<EasyUITreeNode>> getTreeMap(final List<Map<String, Object>> m2tList,
                                                                        final String[] column, final String rootName) {
        Map<String, List<EasyUITreeNode>> arrayListMap = new LinkedHashMap<String, List<EasyUITreeNode>>();
        if(!rootName.equals("")){
            List<EasyUITreeNode> rootlist = new ArrayList<EasyUITreeNode>();
            EasyUITreeNode root = new EasyUITreeNode();
            root.setId("0");
            root.setText(rootName);
            root.setState(m2tList.size() > 0 ? "closed" : "open");
            root.setLeaf(true);
            root.setExpanded(true);
            rootlist.add(root);
            arrayListMap.put("-1", rootlist);
        }
        for (int i=m2tList.size()-1;i>=0;i--) {
            Map<String, Object> e = m2tList.get(i);
            EasyUITreeNode e2t = new EasyUITreeNode();
            e2t.setId(getString(e, column[0]));
            String text = getString(e, column[1]);
            String check = column[3].equals("") ? "0": getString(e, column[3]);
            e2t.setChecked(check.equals("") || check.equals("0") ? false : true);
            String textTmp = "";
            if(column.length > 4){
                textTmp = getOtherText(e, column, 4);
            }
            e2t.setText(text + (textTmp.equals("") ? "" : "[" + textTmp + "]"));
            e2t.setLeaf(true);
            e2t.setExpanded(true);
            String fatherId = getString(e, column[2]);
            if (arrayListMap.get(fatherId) == null) {
                List<EasyUITreeNode> list = new ArrayList<EasyUITreeNode>();
                list.add(e2t);
                arrayListMap.put(fatherId, list);
            } else {
                List<EasyUITreeNode> valueList = arrayListMap.get(fatherId);
                valueList.add(e2t);
                arrayListMap.put(fatherId, valueList);
            }
        }
        return arrayListMap;
    }
    public static Map<String, List<EasyUITreeNode>> getTreeMap(final List<Map<String, Object>> m2tList,
                                                               final String[] column, final String rootName,boolean closeSecondLevel) {
        Map<String, List<EasyUITreeNode>> arrayListMap = new LinkedHashMap<String, List<EasyUITreeNode>>();
        if(!rootName.equals("")){
            List<EasyUITreeNode> rootlist = new ArrayList<EasyUITreeNode>();
            EasyUITreeNode root = new EasyUITreeNode();
            root.setId("0");
            root.setText(rootName);
            root.setState(m2tList.size() > 0 ? "closed" : "open");
            root.setLeaf(true);
            root.setExpanded(true);
            rootlist.add(root);
            arrayListMap.put("-1", rootlist);
        }
        for (int i=m2tList.size()-1;i>=0;i--) {
            Map<String, Object> e = m2tList.get(i);
            EasyUITreeNode e2t = new EasyUITreeNode();
            e2t.setId(getString(e, column[0]));
            String text = getString(e, column[1]);
            String check = column[3].equals("") ? "0": getString(e, column[3]);
            e2t.setChecked(check.equals("") || check.equals("0") ? false : true);
            String textTmp = "";
            if(column.length > 4){
                textTmp = getOtherText(e, column, 4);
            }
            e2t.setText(text + (textTmp.equals("") ? "" : "[" + textTmp + "]"));
            e2t.setLeaf(true);
            if(closeSecondLevel){
                e2t.setExpanded(false);
            }else{
                e2t.setExpanded(true);
            }
            String fatherId = getString(e, column[2]);
            if (arrayListMap.get(fatherId) == null) {
                List<EasyUITreeNode> list = new ArrayList<EasyUITreeNode>();
                list.add(e2t);
                arrayListMap.put(fatherId, list);
            } else {
                List<EasyUITreeNode> valueList = arrayListMap.get(fatherId);
                valueList.add(e2t);
                arrayListMap.put(fatherId, valueList);
            }
        }
        return arrayListMap;
    }
    private static String getOtherText(Map<String, Object> m, String[] column, int start){
        String textTmp = "";
        for(int il=start;il<column.length;il++){
            String tmp = getString(m, column[il]);
            textTmp += textTmp.equals("") ? tmp : ":" + tmp;
        }
        return textTmp;
    }
    private static String getString(Map<String, Object> m, String key){
        String tmp = String.valueOf(m.get(key));
        return StringUtil.isNull(tmp);
    }

    /**
     * 带属性的List转JSON树
     * [id, text, sjid, checked, iconCls, attributes{v1:v2:v3}, othermc...]
     * 创建时间  2014-2-21 上午10:12:25
     * 创建人 yangsl
     * @param m2tList
     * @param column
     * @param rootName
     * @return
     *
     */
    public static String getTreeJsonStringFull(final List<Map<String, Object>> m2tList,
                                               final String[] column, final String rootName) {
        if(column.length < 6){
            return "";
        }

        Map<String, List<EasyUITreeNode>> arrayListMap = getTreeMapFull(m2tList, column, rootName);
        return listToString(arrayListMap);
    }
    /**
     * List转map的方法
     * column = [id, mc, parentid]
     * 创建时间　2014-1-3
     * 创建人　yangsl
     *
     * @param m2tList
     * @param column
     * @return
     */
    public static Map<String, List<EasyUITreeNode>> getTreeMapFull(final List<Map<String, Object>> m2tList,
                                                                            final String[] column, final String rootName) {
        LinkedHashMap<String, List<EasyUITreeNode>> arrayListMap = new LinkedHashMap<String, List<EasyUITreeNode>>();
        if(!rootName.equals("")){
            List<EasyUITreeNode> rootlist = new ArrayList<EasyUITreeNode>();
            EasyUITreeNode root = new EasyUITreeNode();
            root.setId("0");
            root.setText(rootName);
            rootlist.add(root);
            root.setState(m2tList.size() > 0 ? "closed" : "open");
            root.setLeaf(true);
            root.setExpanded(true);
            arrayListMap.put("-1", rootlist);
        }
        for (int i=0;i<m2tList.size();i++) {
            Map<String, Object> e = m2tList.get(i);
            EasyUITreeNode e2t = new EasyUITreeNode();
            e2t.setId(getString(e, column[0]));//id
            String text = getString(e, column[1]);//text
            String check = column[3].equals("") ? "0" : getString(e, column[3]);//checked
            e2t.setChecked(check.equals("") || check.equals("0") ? false : true);
            //other text
            String textTmp = "";
            if(column.length > 6){
                textTmp = getOtherText(e, column, 6);
            }
            e2t.setText(text + (textTmp.equals("") ? "" : "[" + textTmp + "]"));
            e2t.setAttributes(getColumnAttributes(e, column[5]));
            e2t.setLeaf(true);
            e2t.setExpanded(true);
            String fatherId = getString(e, column[2]);
            if (arrayListMap.get(fatherId) == null) {
                List<EasyUITreeNode> list = new ArrayList<EasyUITreeNode>();
                list.add(e2t);
                arrayListMap.put(fatherId, list);
            } else {
                List<EasyUITreeNode> valueList = arrayListMap.get(fatherId);
                valueList.add(e2t);
                arrayListMap.put(fatherId, valueList);
            }
        }
        return arrayListMap;
    }
    private static Map<String, Object> getColumnAttributes(Map<String, Object> m, String attr){
        Map<String, Object> map = new HashMap<String, Object>();
        if(StringUtil.isNull(attr).equals("")){
            return map;
        }
        String[] val = attr.split(":");
        for(int i=0;i<val.length;i++){
            map.put(val[i], getString(m, val[i]));
        }
        return map;
    }

    //递归实现树型结构
    public final static List<EasyUITreeNode> getParentNode( List<EasyUITreeNode> treeNodeList) {
        List<EasyUITreeNode> newTreeNodeList = new ArrayList<EasyUITreeNode>();
        for (EasyUITreeNode node : treeNodeList) {

            if(node.getPid() == null||node.getPid().equals("-1")||"0".equals(node.getPid())) {
                //获取父节点下的子节点
                node.setChildren(getChildrenNode(node.getId(),treeNodeList));
                node.setState("open");
                newTreeNodeList.add(node);
            }
        }
        return newTreeNodeList;
    }

    private final static List<EasyUITreeNode> getChildrenNode(String pid , List<EasyUITreeNode> treeDataList) {
        List<EasyUITreeNode> newTreeDataList = new ArrayList<EasyUITreeNode>();
        for (EasyUITreeNode jsonTreeData : treeDataList) {
            if(jsonTreeData.getPid() == null)  continue;
            //这是一个子节点
            if(jsonTreeData.getPid().equals(pid)){
                //递归获取子节点下的子节点
                jsonTreeData.setChildren(getChildrenNode(jsonTreeData.getId() , treeDataList));
                newTreeDataList.add(jsonTreeData);
            }
        }
        return newTreeDataList;
    }
}
