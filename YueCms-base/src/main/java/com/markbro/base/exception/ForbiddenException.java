package com.markbro.base.exception;


public class ForbiddenException extends ApplicationException{
	
	private static final long serialVersionUID = 814374471148743412L;

	public ForbiddenException() {
        super("Forbidden access");
        error.setCode("403");
    }

    public ForbiddenException(String message) {
        super(message);
        error.setCode("403");
    }

    public ForbiddenException(String message, Throwable cause) {
        super(message, cause);
        error.setCode("403");
    }

    public ForbiddenException(Throwable cause) {
        super(cause);
        error.setCode("403");
    }

    public ForbiddenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setCode("403");
    }
}
