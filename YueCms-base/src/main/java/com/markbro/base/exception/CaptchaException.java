package com.markbro.base.exception;

/**
 * Created by Administrator on 2015/8/17.
 * 验证码异常
 */
public class CaptchaException extends ApplicationException {

    /**
     * Creates a new CaptchaException.
     */
    public CaptchaException() {
        super();
    }

    /**
     * Constructs a new CaptchaException.
     *
     * @param message the reason for the exception
     */
    public CaptchaException(String message) {
        super(message);
    }

    /**
     * Constructs a new CaptchaException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public CaptchaException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new CaptchaException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public CaptchaException(String message, Throwable cause) {
        super(message, cause);
    }
}
