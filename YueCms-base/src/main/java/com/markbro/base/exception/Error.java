package com.markbro.base.exception;



public class Error {

    private String code = "500";
    private String error = "啊哦,服务器开了个小差";
    private String uri = "";

    public Error() {
    }
    public Error(String error) {
        this.error = error;
    }
    public Error(String code, String error) {
        this.code = code;
        this.error = error;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "code=" + code +
                ", error='" + error + '\'' +
                '}';
    }
}
