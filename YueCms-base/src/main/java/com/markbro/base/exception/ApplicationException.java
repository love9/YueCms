package com.markbro.base.exception;


public class ApplicationException extends RuntimeException{
    
	private static final long serialVersionUID = 468567361409287980L;
    Error error=new Error();

    public ApplicationException() {
        super("啊哦,服务器开了个小差");
    }

    public ApplicationException(String message) {
        super(message);
        error.setError(message);
    }
    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
        error.setError(message);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
        error.setError(cause.getMessage());
    }

    public ApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setError(message);
    }

    public ApplicationException(String code, String message) {
        super(message);
        error.setCode(code);
        error.setError(message);
    }

    public ApplicationException(String code,String message, Throwable cause) {
        super(message, cause);
        error.setCode(code);
        error.setError(message);
    }

    public Error getError() {
        return error;
    }
}
