package com.markbro.base.exception;

public class NoLoginException extends ApplicationException{

	private static final long serialVersionUID = 6264824410093676645L;


    public NoLoginException() {
        super("您还没有登录,请先登录!");
        error.setCode("NoLogin");
    }

    public NoLoginException(String message) {
        super(message);
        error.setCode("NoLogin");
    }
    public NoLoginException(String message, Throwable cause) {
        super(message, cause);
        error.setCode("NoLogin");
    }

    public NoLoginException(Throwable cause) {
        super(cause);
        error.setCode("NoLogin");
    }

    public NoLoginException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setCode("NoLogin");
    }
}
