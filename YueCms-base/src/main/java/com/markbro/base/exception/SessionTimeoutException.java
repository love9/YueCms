package com.markbro.base.exception;

public class SessionTimeoutException extends ApplicationException{

	private static final long serialVersionUID = 6264824410093676645L;


    public SessionTimeoutException() {
        super("SessionTimeout");
        error.setCode("SessionTimeout");
    }

    public SessionTimeoutException(String message) {
        super(message);
        error.setCode("SessionTimeout");
    }
    public SessionTimeoutException(String message, Throwable cause) {
        super(message, cause);
        error.setCode("SessionTimeout");
    }

    public SessionTimeoutException(Throwable cause) {
        super(cause);
        error.setCode("SessionTimeout");
    }

    public SessionTimeoutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setCode("SessionTimeout");
    }
}
