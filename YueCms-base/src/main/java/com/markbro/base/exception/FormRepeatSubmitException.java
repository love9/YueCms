package com.markbro.base.exception;

public class FormRepeatSubmitException extends ApplicationException{

	private static final long serialVersionUID = 6264824410093676645L;

	public FormRepeatSubmitException() {
        super("FormRepeatSubmit");
        error.setCode("FormRepeatSubmit");
    }

    public FormRepeatSubmitException(String message) {
        super(message);
        error.setCode("FormRepeatSubmit");
    }
    public FormRepeatSubmitException(String message, Throwable cause) {
        super(message, cause);
        error.setCode("FormRepeatSubmit");
    }

    public FormRepeatSubmitException(Throwable cause) {
        super(cause);
        error.setCode("FormRepeatSubmit");
    }

    public FormRepeatSubmitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setCode("FormRepeatSubmit");
    }
}
