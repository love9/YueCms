package com.markbro.base.exception;

public class UnAuthorizedException extends ApplicationException{
	
	private static final long serialVersionUID = 6264824410093676645L;

	public UnAuthorizedException() {
        super("UnAuthorized");
        error.setCode("401");
    }

    public UnAuthorizedException(String message) {
        super(message);
        error.setCode("401");
    }

    public UnAuthorizedException(String message, Throwable cause) {
        super(message, cause);
        error.setCode("401");
    }

    public UnAuthorizedException(Throwable cause) {
        super(cause);
        error.setCode("401");
    }

    public UnAuthorizedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setCode("401");
    }
}
