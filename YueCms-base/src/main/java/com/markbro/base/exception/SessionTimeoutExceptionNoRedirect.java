package com.markbro.base.exception;

public class SessionTimeoutExceptionNoRedirect extends ApplicationException{

	private static final long serialVersionUID = 6264824410093676645L;

    private  boolean noRedirect=true;

    public boolean isNoRedirect() {
        return noRedirect;
    }

    public void setNoRedirect(boolean noRedirect) {
        this.noRedirect = noRedirect;
    }

    public SessionTimeoutExceptionNoRedirect() {
        super("SessionTimeout");
        error.setCode("SessionTimeout");
    }

    public SessionTimeoutExceptionNoRedirect(String message) {
        super(message);
        error.setCode("SessionTimeout");
    }
    public SessionTimeoutExceptionNoRedirect(String message, boolean noRedirect) {
        super(message);
        error.setCode("SessionTimeout");
        this.noRedirect=noRedirect;
    }
    public SessionTimeoutExceptionNoRedirect(String message, Throwable cause) {
        super(message, cause);
        error.setCode("SessionTimeout");
    }

    public SessionTimeoutExceptionNoRedirect(Throwable cause) {
        super(cause);
        error.setCode("SessionTimeout");
    }

    public SessionTimeoutExceptionNoRedirect(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error.setCode("SessionTimeout");
    }
}
