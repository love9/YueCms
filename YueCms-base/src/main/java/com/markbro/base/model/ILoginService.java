package com.markbro.base.model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wujiyue
 */
public interface ILoginService {

    public  LoginBean loginChecked(HttpServletRequest request, HttpServletResponse response);

    public boolean isInvalidToken(HttpServletRequest request);

    public String getYhidByToken(String token);

    public LoginBean cacheInfo(String yhid);

}
