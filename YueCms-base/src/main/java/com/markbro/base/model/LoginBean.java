package com.markbro.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 登录用户信息
 * @author wjy
 *
 */
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 部门id
	 */
	private String bmid;
	/**
	 * 岗位id
	 */
	private String gwid;
	/**
	 * 用户id
	 */
	private String yhid;
	/**
	 * 角色id
	 */
	private String jsid;
	/**
	 * 组织机构id
	 */
	private String orgid;
	/**
	 * 显示姓名
	 */
	private String xm;
	/**
	 * 用户状态
	 */
	private String state;
	/**
	 * 特殊的超级管理员，yhid=1
	 */
	private boolean isSystemAdmin;
	/**
	 * 以admin角色定义的超级管理员
	 */
	private boolean isAdmin;
	/**
	 * 是否手机登录
	 */
	private boolean mobileLogin;

	/**
	 * 登录用户组织信息
	 */
	private Map<String, Object> orgMap =new HashMap<String, Object>();
	/**
	 * 登录用户信息
	 */
	private Map<String, Object> userMap =new HashMap<String, Object>();

	/**
	 * 登录用户角色信息
	 */
	private List<Map<String, String>> jsList = new ArrayList<Map<String, String>>();
	/**
	 * 登录用户部门信息
	 */
	private List<Map<String, String>> bmList = new ArrayList<Map<String, String>>();
	/**
	 * 登录用户岗位信息
	 */
	private List<Map<String, String>> gwList = new ArrayList<Map<String, String>>();
	/**
	 * 角色对应登录后跳转的页面
	 */
	private String loginpage;
	/**
	 * 角色对应的首页
	 */
	private String mainpage;
	/**
	 * 登录次数
	 */
	private  int loginCount;
	/**
	 * 上次登录地址
	 */
	private  String lastLoginAddress;

	/**
	 * 上次登录时间
	 */
	private  String lastLoginTime;
	/**
	 * 登录账户
	 */
	private String dlmc;


	public String getDlmc() {
		return dlmc;
	}

	public void setDlmc(String dlmc) {
		this.dlmc = dlmc;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginAddress() {
		return lastLoginAddress;
	}

	public void setLastLoginAddress(String lastLoginAddress) {
		this.lastLoginAddress = lastLoginAddress;
	}

	public String getLoginpage() {
		return loginpage;
	}
	public void setLoginpage(String loginpage) {
		this.loginpage = loginpage;
	}
	public int getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public String getMainpage() {
		return mainpage;
	}

	public void setMainpage(String mainpage) {
		this.mainpage = mainpage;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public  Map<String, Object> getOrgMap() {
		return orgMap;
	}
	public void setOrgMap( Map<String, Object> orgMap) {
		this.orgMap = orgMap;
	}
	public String getBmid() {
		return bmid;
	}
	public void setBmid(String bmid) {
		this.bmid = bmid;
	}
	public String getGwid() {
		return gwid;
	}
	public void setGwid(String gwid) {
		this.gwid = gwid;
	}
	public String getYhid() {
		return yhid;
	}
	public void setYhid(String yhid) {
		this.yhid = yhid;
	}
	public String getOrgid() {
		return orgid;
	}
	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}
	public String getXm() {
		return xm;
	}
	public void setXm(String xm) {
		this.xm = xm;
	}

	public String getJsid() {
		return jsid;
	}

	public void setJsid(String jsid) {
		this.jsid = jsid;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public List<Map<String, String>> getJsList() {
		return jsList;
	}

	public void setJsList(List<Map<String, String>> jsList) {
		this.jsList = jsList;
	}

	public List<Map<String, String>> getBmList() {
		return bmList;
	}

	public void setBmList(List<Map<String, String>> bmList) {
		this.bmList = bmList;
	}

	public List<Map<String, String>> getGwList() {
		return gwList;
	}

	public void setGwList(List<Map<String, String>> gwList) {
		this.gwList = gwList;
	}

	public boolean isMobileLogin() {
		return mobileLogin;
	}

	public void setMobileLogin(boolean mobileLogin) {
		this.mobileLogin = mobileLogin;
	}

	public Map<String, Object> getUserMap() {
		return userMap;
	}

	public void setUserMap(Map<String, Object> userMap) {
		this.userMap = userMap;
	}

	public boolean isSystemAdmin() {
		return isSystemAdmin;
	}

	public void setSystemAdmin(boolean isSystemAdmin) {
		this.isSystemAdmin = isSystemAdmin;
	}
}
