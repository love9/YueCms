package com.markbro.base.model;

import java.io.Serializable;
import java.util.*;

/**
 * @author wjy
 * 返回后台返回前端的消息对象
 */
public class Msg implements Serializable{

    private static final long serialVersionUID = -6532366534692890978L;
    private HashMap<String, Object> pageMap = new HashMap<String, Object>();

    private HashMap<String, List<Map<String, Object>>> selectMap = new HashMap<String, List<Map<String, Object>>>();
    private HashMap<String, Object> formMap = new HashMap<String, Object>();

    public enum MsgType {
        //执行成功
        success,
        //提示信息
        info,
        //错误
        error,
        block,
        //危险
        danger
    }
    /**
     * 消息类型
     */
    private MsgType type;
    /**
     * 消息内容
     */
    private String content;

    public Msg() {
    }
    public Msg(MsgType type, String content) {
        this.type = type;
        this.content = content;
    }
    public static Msg success(String msg) {
       return new Msg(MsgType.success,msg);
    }
    public static Msg error(String msg) {
        return new Msg(MsgType.error,msg);
    }
    public MsgType getType() {
        return type;
    }

    public void setType(MsgType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSelect(String selectId, List<Map<String, Object>> list) {
        try {
            this.selectMap.put(selectId, convertList(list));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public HashMap<String, Object> getData() {
        this.pageMap.put("selectData", this.selectMap);
        this.pageMap.put("formData", this.formMap);
        return this.pageMap;
    }
    @SuppressWarnings("rawtypes")
    public Map<String, Object> convertMap(Map<String, Object> map) {
        Map<String, Object> map1 = new HashMap<String, Object>();
        try {
            Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry m = (Map.Entry) it.next();
                if ((m.getValue() != null)
                        && ((m.getValue() instanceof String))){
                    map1.put(m.getKey().toString().toLowerCase(),
                            ((String) m.getValue()).replace("\\", "\\\\"));
                } else{
                    map1.put(m.getKey().toString().toLowerCase(), m.getValue());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return map1;
    }
    @SuppressWarnings("rawtypes")
    public List<Map<String, Object>> convertList(List<Map<String, Object>> list) {
        List<Map<String, Object>> listTemp = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = null;
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> tempMap =  list.get(i);
            Iterator<Map.Entry<String, Object>> it = tempMap.entrySet().iterator();
            map = new HashMap<String, Object>();
            while (it.hasNext()) {
                Map.Entry m = (Map.Entry) it.next();
                if ((m.getValue() != null)
                        && ((m.getValue() instanceof String))) {
                    map.put(m.getKey().toString().toLowerCase(),
                            ((String) m.getValue()).replace("\\", "\\\\"));
                }  else{
                    map.put(m.getKey().toString().toLowerCase(), m.getValue());
                }
            }
            listTemp.add(map);
        }
        return listTemp;
    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void setExtend(String extId, Object data) {
        if ((data instanceof List)) {
            try {
                this.pageMap.put(extId, convertList((List) data));
            } catch (Exception localException) {
                this.pageMap.put(extId, data);
            }
        } else {
            this.pageMap.put(extId, data);
        }
    }
}
