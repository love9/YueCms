package com.markbro.base.annotation;

import java.lang.annotation.*;

/**
 * 给Controller的getMap方法附加一些参数值.配置参照表sys_data_scope表.可以配合DataScopeInterceptor实现数据授权
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScope {
    String code() default "";
    boolean userLrr() default false;
}
