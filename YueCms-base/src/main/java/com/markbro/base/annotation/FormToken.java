package com.markbro.base.annotation;

import java.lang.annotation.*;

/**
 * 防止表单重复提交而用于给表单产生一个随机的token
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FormToken {
    //生成token，跳转到表单页面时置为true
    boolean get() default false;
    //移除token，真正保存数据到数据库的方法调用时置为true
    boolean remove() default false;
}
