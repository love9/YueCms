package com.markbro.base.annotation;

import java.lang.annotation.*;

/**
 * 执行方法之前根据页面来源referer附加一些参数值.配置参照表sys_request_referer表
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Referer {
    String methodCode() default "";
}
