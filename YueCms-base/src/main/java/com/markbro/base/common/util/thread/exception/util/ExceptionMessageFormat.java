package com.markbro.base.common.util.thread.exception.util;

/**
 * 异常信息格式化
 * @author Administrator
 *
 */
public interface ExceptionMessageFormat {
	public String formate(Exception e);
}
