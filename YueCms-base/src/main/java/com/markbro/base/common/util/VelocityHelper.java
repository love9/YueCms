package com.markbro.base.common.util;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeConstants;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VelocityHelper {
    protected VelocityContext context;
    protected Template template;
    private String  templateFile;
    private String  templateDirPath;//模版文件根目录
    private String directoryOfOutput;//输出的文件夹路径
    private String fileNameOfOutput;//输出文件名
    private String fileExtensionOfOutput;//输出文件后缀
    public String getTemplateFile() {
        return templateFile;
    }

    public void setTemplateFile(String templateFile) {
        this.templateFile = templateFile;
        InitTemplateEngine();
    }

    public String getTemplateDirPath() {
        return templateDirPath;
    }

    public void setTemplateDirPath(String templateDirPath) {
        this.templateDirPath = templateDirPath;
    }

    public String getDirectoryOfOutput() {
        return directoryOfOutput;
    }

    public void setDirectoryOfOutput(String directoryOfOutput) {
        this.directoryOfOutput = directoryOfOutput;
    }

    public String getFileNameOfOutput() {
        return fileNameOfOutput;
    }

    public void setFileNameOfOutput(String fileNameOfOutput) {
        this.fileNameOfOutput = fileNameOfOutput;
    }

    public String getFileExtensionOfOutput() {
        return fileExtensionOfOutput;
    }

    public void setFileExtensionOfOutput(String fileExtensionOfOutput) {
        this.fileExtensionOfOutput = fileExtensionOfOutput;
    }

    private Map<String, Object> KeyObjDict = new HashMap<String, Object>();
    public  VelocityHelper(String templateDirPath)
    {
        this.templateDirPath = templateDirPath;//模版文件根目录
    }
    public  VelocityHelper AddKeyValue(String key, Object value)
    {
        if (!KeyObjDict.containsKey(key))
        {
            KeyObjDict.put(key, value);
        }
        return this;
    }
    //初始化模板引擎
    protected  boolean InitTemplateEngine()
    {
        boolean flag=false;
        try{
            VelocityEngine templateEngine = new VelocityEngine();
            templateEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "file");
            templateEngine.setProperty(RuntimeConstants.INPUT_ENCODING, "utf-8");
            templateEngine.setProperty(RuntimeConstants.OUTPUT_ENCODING, "utf-8");
            if (templateDirPath!=null&&!templateDirPath.equals(""))
            {
                templateEngine.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, templateDirPath);
                templateEngine.init();
                template = templateEngine.getTemplate(templateFile);
                flag=true;
            }
            else
            {

            }

        } catch (ResourceNotFoundException re)
        {
            String error = String.format("InitTemplateEngine初始化模版引擎失败！模版未找到！" + templateFile);
            System.out.println(error);
        }
        catch (ParseErrorException pee)
        {
            String error = String.format("InitTemplateEngine初始化模版引擎失败！" + templateFile + ":" + pee.toString());
            System.out.println(error);
        }catch(Exception ex){
            String error = String.format("InitTemplateEngine初始化模版引擎失败！" + templateFile + ":" + ex.toString());
            System.out.println(error);
        }
        return flag;
    }
    // 初始化上下文的内容,在生成内容之前，需要把相关的对象属性绑定到模板引擎的上下文对象里面。
    private void InitContext()
    {
        context = new VelocityContext();
        for (Map.Entry<String, Object> entry : KeyObjDict.entrySet()) {
            context.put(entry.getKey(),entry.getValue());
        }
    }
    //根据模板创建输出的文件,并返回生成的文件路径
    public String ExecuteFile(){
        String fileName = "";//输出文件的全路径
        try{

            if (template != null)
            {
                if(!directoryOfOutput.endsWith("\\"))
                {
                    directoryOfOutput=directoryOfOutput+"\\";
                }
                //输出文件的全路径=输出根目录+输出文件名+输出文件后缀
                fileName = directoryOfOutput + fileNameOfOutput + fileExtensionOfOutput;

                if (!StringUtils.isNotEmpty(directoryOfOutput))
                {
                    File dir=new File(directoryOfOutput);
                    if(!dir.exists()&&dir.isDirectory()){
                        dir.mkdirs();
                    }

                }

                //LogTextHelper.Debug(string.Format("Class file output path:{0}", fileName));
                InitContext();
                File srcFile = new File(fileName);//输出路径
                FileOutputStream fos = new FileOutputStream(srcFile);
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos, "utf-8"));
                template.merge(context, writer);
                writer.flush();
                writer.close();
                fos.close();

            }

        }catch(Exception ex)
        {
            fileName="";
        }
        return fileName;
    }
    // 根据模板输出字符串内容
    public String ExecuteString()
    {
        InitContext();
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.getBuffer().toString();
    }
    // 合并字符串的内容
    public String ExecuteMergeString(String inputString)
    {
        VelocityEngine templateEngine = new VelocityEngine();
        templateEngine.init();
        InitContext();
        StringWriter writer = new StringWriter();
        templateEngine.evaluate(context, writer, "mystring", inputString);
        return writer.getBuffer().toString();
    }
    public static void main(String[] args) {
        VelocityHelper he=new VelocityHelper("C:\\Users\\Administrator\\Desktop\\q");
        he.setDirectoryOfOutput("C:\\Users\\Administrator\\Desktop\\q");
        he.setFileNameOfOutput("123");
        he.setFileExtensionOfOutput(".txt");
        he.setTemplateFile("test.txt");

        List<String> ls=new ArrayList<String>();
        ls.add("苹果1");
        ls.add("苹果2");
        ls.add("苹果3");
        he.AddKeyValue("username", "wjy").AddKeyValue("ls", ls);
        String s=he.ExecuteFile();
        //String s=he.ExecuteMergeString("123qq ${username}");
        System.out.println(s);
    }
}

