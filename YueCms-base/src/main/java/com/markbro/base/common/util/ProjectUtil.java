package com.markbro.base.common.util;

/**
 * @author wujiyue
 */
public class ProjectUtil {

    /**
     * 获得程序根目录
     * @return
     */
    public static String getProjectPath(){
        String path=ProjectUtil.class.getResource("/").getPath().replaceFirst("/", "").replaceAll("WEB-INF/classes/", "").replaceAll("target/classes/", "");
        if(path.contains("out")){
            path=path.substring(0,path.indexOf("out"));
        }
        return path;
    }
}
