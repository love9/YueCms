package com.markbro.base.common.util;
public class TmConstant {
    public static final Integer TOKEN_illegal = -1;//不合法token，伪造的
    public static final Integer TOKEN_EXPIRES =0;//token过期无效
    public static final Integer TOKEN_OK = 1;//token正常有效
    public static final String TOKEN = "token";
    public static final String USER_ONLINE = "user_online";//在线用户列表
    public static final String NUM_ONLINE = "num_online";//在线用户数量
    public static final String pageInputDd = "account";//登录页面帐户名
    public static final String pageInputMm = "password";//登录页面密码
    public static final String pageInputCode = "verycode";//登录页面验证码

    public static final String pageRememberMeKey = "RememberMe";
    public static final String cookieNameKey = "markbro_username";//cookie中用户名key
    public static final String cookiePassKey = "markbro_password";

    public static final String sysTokenKey = "sys_token";
    public static final String TokenKey = "markbro_token";


    //0仅个人  1全部推送  2仅推送本部门
    public static final String PUSH_SCOPE_RECENT_NEWS_ONLYME="0";
    public static final String PUSH_SCOPE_RECENT_NEWS_ALL="1";
    public static final String PUSH_SCOPE_RECENT_NEWS_DEPT="2";

    //1	文本消息2	图文消息3	附件消息（文档） 4	申请消息 5	工作动态  0系统通知
    public static final String RECENT_NEWS_SYSTEM="0";
    public static final String RECENT_NEWS_TEXT="1";
    public static final String RECENT_NEWS_TEXT_IMG="2";
    public static final String RECENT_NEWS_TEXT_ATTACH="3";
    public static final String RECENT_NEWS_APPLAY="4";
    public static final String RECENT_NEWS_WORK="5";


    public static final String KEY_USER_DEFAULT_HEAD_ICON="/resources/images/user.png";
    public static final String KEY_WX_TOKEN="markbro_token";
    public static final String Request_Referer_Name = "Request_Referer_Name";
    public static final String YHID_KEY = "yhid";
    public static final String YHMC_KEY = "yhmc";
    //匹配空白行的正则表达式
    public static final String REGEX_BLANK_LINE="\\s*";
    // 匹配java注释行的正则表达式
    public static final String[] REGEX_JAVA_NOTE ={ "", "\\s*/\\*+\\s*",
            "\\s*\\*/\\s*", "\\s*\\*\\s*",
            "\\s*\\*(\\s*\\S*\\s*)*",
            "\\s*/{2,}?(\\s*\\S*\\s*)*", "\\s*" };
    public static final String REGEX_SPECIAL="[`~!@#$%^&*()+=|{}':;',//[//].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
    //登录用户权限在session中的key
    public static final String KEY_SESSION_USER_PERMISSION="user_permission";
    //登陆用户对象在作用域中的KEY
	public static final String KEY_LOGIN_USER="loginuser";
	//错误信息在作用域中的KEY
	public static final String KEY_ERROR_MSG="errormsg";
	//验证码存放到作用域中的key
	public static final String KEY_VERYCODE="verycode";
	//错误-参数错误
	public static final String ERROR_ARGSERROR="Args Error";
	
    public static final String HTTP = "http";
    public static final String COLON = ":";
    public static final String SLASH = "/";
    public static final String BACKSLASH = "\\";
    public static final String HYPHEN = "-";
    public static final String UNDERSCORE = "_";
    public static final String DOUBLE_SLASH = "//";
    public static final String DOUBLE_BACKSLASH = "\\\\";
    public static final String QUESTION_MARK = "?";
    public static final String HTTP_PREFIX = HTTP + COLON + DOUBLE_SLASH;
    public static final String WWW = "www";
    public static final String DOT = ".";
    public static final String DOTS = "...";
    public static final String AT = "@";
    public static final String ASTERISK = "*";
    public static final String COMMA = ",";
    public static final String WWW_DOT = WWW + DOT;
    public static final Integer DEFAULT_PORT = 80;
    public static final String EMPTY = "";
    public static final String NUMBER_SIGN = "#";
    public static final String UTF8 = "UTF-8";

    // xuchengfeifei
    public static final String SEPERTOR = "\"";
    public static final String DOUBLE_SEPERTOR = "\\\\\"";
    public static final String SINGLE_SEPERTOR = "\\\"";
    public static final String REGEX_PIPER = "(.*?)";
    public static final String SPILT_STRING = "#";
    public static final String COMPARE_CONTENT = "#CONTENT#";
    public static final String COMPARE_IMAGE = "#IMAGE#";
    public static final String COMPARE_TITLE = "#TITLE#";
    public static final String COMPARE_PAGE_FLAG = "#PAGE#";
    public static final String COMPARE_PAGE = "@PAGE@";
    public static final String PREFIX_HTML = "<label class=exmay-label>";
    public static final String END_HTML = "</label>";
    public static final String A_START = "<a target='_blank' href='";
    public static final String A_HREF = "/search/?keyword=";
    public static final String A_MID = "'>";
    public static final String A_END = "</a>";
    public static final String HTML_SUFFIX = "html";
    public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String KEYWORD_REGEX = "[\\s|,|;|\\#]+";


    /*iBase4J*/

    /**
     * 异常信息统一头信息<br>
     * 非常遗憾的通知您,程序发生了异常
     */
    public static final String Exception_Head = "OH,MY GOD! SOME ERRORS OCCURED! AS FOLLOWS :";
    /** 客户端语言 */
    public static final String USERLANGUAGE = "userLanguage";
    /** 客户端主题 */
    public static final String WEBTHEME = "webTheme";
    /** 当前用户 */
    public static final String CURRENT_USER = "CURRENT_USER";
    /** 在线用户数量 */
    public static final String ALLUSER_NUMBER = "ALLUSER_NUMBER";
    /** 登录用户数量 */
    public static final String USER_NUMBER = "USER_NUMBER";
    /** 上次请求地址 */
    public static final String PREREQUEST = "PREREQUEST";
    /** 上次请求时间 */
    public static final String PREREQUEST_TIME = "PREREQUEST_TIME";
    /** 非法请求次数 */
    public static final String MALICIOUS_REQUEST_TIMES = "MALICIOUS_REQUEST_TIMES";



    public static final String GIFT_SIGN_IN="signin";//签到礼包
    public static final String GIFT_MX_USER_SCORES="user_scores";//用于增加用户积分的礼物明细
    public static final String GIFT_MX_USER_GOLD="user_gold";

    public static final String PIC_ADRR="/UnigoService/unigo/mobile/file?id=";

    public static final String XML_CONTENTTYPE = "text/xml;charset=UTF-8";
    public static final String XML_APP_CONTYPE = "application/xml;charset=UTF-8";

    public static final String HTML_CONTENTTYPE = "text/html;charset=UTF-8";

    public static final String JSON_CONTENTTYPE = "text/json;charset=UTF-8";
    public static final String JSON_APP_CONTYPE = "text/javascript;charset=UTF-8";

    public static final String CHARCODE = "UTF-8";

    public static final String POST_JSON_VIEW = "jsonView";


    public static final String ID_KEY = "id";
    public static final String GUID_KEY = "guid";
    public static final String ZZID_KEY = "zzid";
    public static final String ORGID_KEY = "orgid";
    //public static final String YHID_KEY = "yhid";
    public static final String BMID_KEY = "bmid";
    public static final String JSID_KEY = "jsid";
    public static final String ORG_ZZID_KEY = "org_zzid";
    public static final String ORG_BMID_KEY = "org_bmid";
    public static final String ORG_GWID_KEY = "org_gwid";
    public static final String ORG_YHID_KEY = "org_yhid";
    public static final String ORG_SJID_KEY = "org_sjid";

    public static final String BM_KEY = "bm";
    public static final String GW_KEY = "gw";
    public static final String YH_KEY = "yh";
    public static final String GWID_KEY = "gwid";
    public static final String ZTDM_KEY = "zt_dm";
    public static final String CJSJ_KEY = "cjsj";
    public static final String NULL_KEY = "null";
    public static final String XM_KEY = "xm";
    public static final String DM_KEY = "dm";
    public static final String MC_KEY = "mc";
    public static final String ROLE_KEY = "role";
    public static final String IP_KEY = "ip";
    public static final String XH_KEY = "xh";
    public static final String LXID_KEY = "lxid";
    public static final String EXTENDSVALUE ="kzz";
    public static final String KZ_KEY = "kz";
    public static final String KZDM_KEY = "kz_dm";

    public static final String OTHER_LX_DM = "200";
    public static final String DEPT_LX_DM = "201";
    public static final String POSITION_LX_DM = "201";
    public static final String USER_LX_DM = "204";

    public static final String USER_DM_303 = "303";
    public static final String USER_DM_302 = "302";

    public static final String PAGE_GOTO_KEY = "page_goto";
    public static final String PAGE_COUNT_KEY = "page_count";
    public static final String PAGE_PAGE_KEY = "page";
    public static final String PAGE_ROWS_KEY = "rows";
    public static final String PAGE_SORT_KEY = "sort";//按照哪个字段排序
    public static final String PAGE_DIR_KEY = "dir";//排序方式，ase/dec
    public static final String PAGE_TOTAL_KEY = "total";
    public static final String PAGE_TOTAL_PAGES_KEY = "totalPages";
    public static final String PAGE_SHOW_ROWS = "10";

    public static final String TABLE_LIST_KEY = "table_list";
    public static final String TABLE_LIST_KEY2 = "table_list2";

    public static final String NUM_ZERO = "0";
    public static final String NUM_ONE = "1";
    public static final String NUM_TWO = "2";
    public static final String NUM_THREE = "3";
    public static final String NUM_UNONE = "-1";
    public static final String NUM_TEN = "10";

    public static final boolean TRUE_FLAG = true;
    public static final String TRUE_FLAG_STR = "true";
    public static final boolean FALSE_FLAG = false;
    public static final String FALSE_FLAG_STR = "false";

    public static final String SYMB_KGE = "";
    public static final String SYMB_DYH = "'";
    public static final String SYMB_DOUH = ",";
    public static final String SYMB_DYH_LDOU = ",'";
    public static final String SYMB_DYH_RDOU = "',";
    public static final String SYMB_DYH_DDOU = "','";
    public static final String SYMB_DYH_YKHYH="') ";
    public static final String SYMB_DYH_FHAO = ";";
    public static final String SYMB_LINE = "_";
    public static final String SYMB_BL = "~";
    public static final String SYMB_MH = ":";
    public static final String SYMB_SHUXIAN = "|";
    public static final String SYMB_FENHAO = ";";
    public static final String SYMB_XIEXIAN = "/";
    public static final String SYMB_SHUXIAN_ZHANYI = "\\|";
    public static final String SYMB_BAIFENHAO = "%";

    public static final String MSG_SAVE = "保存成功";
    public static final String MSG_ADD = "添加数据";
    public static final String MSG_DEL = "删除数据";
    public static final String MSG_UPD = "修改数据";
    public static final String MSG_QUR = "查询数据";
    public static final String[] MSG_TRUE_FALSE = {"成功","失败"};
    public static final String MSG_ERROR = "操作出现异常：";
    public static final String MSG_METHOD = "method:";


    public static final String CON_ADMIN = "admin";
    public static final String CON_ADMIN_MC = "系统管理员";

    public static final String CODE_UNKNOWN = "unknown";
    public static final String CODE_WARNING = "warning";
    public static final String CODE_FAILING = "failing";
    public static final String CODE_SUCCESS = "success";
    public static final String CODE_ERROR = "error";
    public static final String CODE_RESULT = "result";


    public static final String CACHE_YH_USERBEAN = "userbean";
    public static final String CACHE_YH_JS_LIST = "jslist";
    public static final String CACHE_YH_BM_LIST = "bmlist";
    public static final String CACHE_YH_GW_LIST = "gwlist";
    public static final String CACHE_YH_ORG = "org";
    public static final String CACHE_YH_GW_SEL = "gw_sel";
    public static final String CACHE_YH_URL = "url";
    public static final String CACHE_YH_CODE = "auth_code";
    public static final String CACHE_YH_METHOD = "method";
    public static final String CACHE_YH_SYS_BTN_AUTH = "sys_btn_auth";
    public static final String CACHE_YH_DLMC = "dlmc";
    public static final String CACHE_YH_ISMANAGER = "isManager";

    public static final String CACHE_GLOBAL_METHOD = "method";
    public static final String CACHE_GLOBAL_URL = "url";
    public static final String CACHE_GLOBAL_ISGROUP = "isgroup";
    public static final String CACHE_GLOBAL_ITOKEN = "invalid_token";
    public static final String CACHE_LOGIN_TIME = "login_time";
    public static final String CACHE_ACTIVE_TIME = "active_time";
    public static final String CACHE_TOKEN = "token";

    public static final String MD5_DEFAULT_PASS = "e10adc3949ba59abbe56e057f20f883e";

}
