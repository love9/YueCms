package com.markbro.base.common.util.cache;

public class TestCache {
    public static void main(String[] args){
        try {
        Cache<String,String> fifoCache = CacheUtil.newFIFOCache(3);

        //加入元素，每个元素可以设置其过期时长，DateUnit.SECOND.getMillis()代表每秒对应的毫秒数，在此为3秒
        fifoCache.put("key1", "value1", 3000);
        fifoCache.put("key2", "value2", 3000);
        fifoCache.put("key3", "value3", 3000);

        //由于缓存容量只有3，当加入第四个元素的时候，根据FIFO规则，最先放入的对象将被移除
        fifoCache.put("key4", "value4", 3000);

        //value1为null
        String value1 = fifoCache.get("key2");
        System.out.println(value1);

        Thread.sleep(1000);

           value1 = fifoCache.get("key2");
            System.out.println(value1);
            Thread.sleep(2000);

            value1 = fifoCache.get("key2");
            System.out.println(value1);
            Thread.sleep(1000);

            value1 = fifoCache.get("key2");
            System.out.println(value1);
            Thread.sleep(1000);

            value1 = fifoCache.get("key2");
            System.out.println(value1); Thread.sleep(1000);

            value1 = fifoCache.get("key2");
            System.out.println(value1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
