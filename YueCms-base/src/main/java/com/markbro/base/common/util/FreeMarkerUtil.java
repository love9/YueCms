package com.markbro.base.common.util;


import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.NullCacheStorage;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FreeMarkerUtil  {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private TemplateLoaderType templateLoaderType= TemplateLoaderType.ClassTemplateLoader;
    private Map<String,String> stringTemplateMap;//当模式为StringTemplateLoader时，从这里加载模板名和对应的模板内容

    private String  templateBasePackagePath="/templates";//模版文件目录

    public FreeMarkerUtil(){
        init();
    }
    public FreeMarkerUtil(String templateBasePackagePath){
        this.templateBasePackagePath=templateBasePackagePath;
        init();
    }
    public FreeMarkerUtil(TemplateLoaderType templateLoaderType,Map<String,String> stringTemplateMap){
        this.templateLoaderType=templateLoaderType;
        this.stringTemplateMap=stringTemplateMap;
        init();
    }

    private static final Configuration CONFIGURATION = new Configuration(Configuration.VERSION_2_3_22);



    public enum TemplateLoaderType{
        ClassTemplateLoader,
        StringTemplateLoader
    }

    private void init(){
        initTemplateLoader();
        CONFIGURATION.setDefaultEncoding("UTF-8");
        CONFIGURATION.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        CONFIGURATION.setCacheStorage(NullCacheStorage.INSTANCE);
    }
    private void initTemplateLoader(){ //用来指定加载模板所在的路径
        logger.info("当前TemplateLoader为:{}",templateLoaderType.name());
        if(this.templateLoaderType== TemplateLoaderType.StringTemplateLoader){
            StringTemplateLoader stringLoader = new StringTemplateLoader();
            if(stringTemplateMap==null){
                logger.error("类{}，方法{},参数{}为空！",getClass().getName(),"initTemplateLoader","stringTemplateMap");
                return;
            }
            for (Map.Entry<String, String> entry : stringTemplateMap.entrySet()) {
                stringLoader.putTemplate(entry.getKey(), entry.getValue());
            }
            CONFIGURATION.setTemplateLoader(stringLoader);
        }else{
            CONFIGURATION.setTemplateLoader(new ClassTemplateLoader(FreeMarkerUtil.class, templateBasePackagePath));
        }
    }
    public static Template getTemplate(String templateName) throws IOException {
        try {
            return CONFIGURATION.getTemplate(templateName);
        } catch (IOException e) {
            throw e;
        }
    }
    /**
     * 输出到输出到文件
     * @param templateName   ftl文件名
     * @param data		传入的map
     * @param outFileFullPath	输出后的文件全部路径
     */
    public  boolean ExecuteFile(String templateName, Map<String,Object> data, String outFileFullPath){
        try {
            File file = new File(outFileFullPath);
            if(!file.getParentFile().exists()){				//判断有没有父路径，就是判断文件整个路径是否存在
                file.getParentFile().mkdirs();				//不存在就全部创建
            }
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
            Template template = getTemplate(templateName);
            template.process(data, out);					//模版输出
            out.flush();
            out.close();
            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public  String ExecuteString(String templateName, Map<String,Object> data) throws Exception{
            StringWriter writer = new StringWriter();
            Template template = getTemplate(templateName);
            template.process(data, writer);					//模版输出
            return writer.getBuffer().toString();
    }

    public static void clearCache() {
        CONFIGURATION.clearTemplateCache();
    }

    public static void main(String[] args){
        //第一种方式：使用字符串模板
        /*Map templateMap=new HashMap();
        templateMap.put("test.ftl","hello ${name}!");
        FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(TemplateLoaderType.StringTemplateLoader,templateMap);
        String templateName="test.ftl";
        Map data=new HashMap();
        data.put("name","wjy");
        try {
            freeMarkerUtil.ExecuteFile(templateName,data,"d:\\test.html");
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        //第二种方式：使用文件模板（默认）
        FreeMarkerUtil freeMarkerUtil2=new FreeMarkerUtil();
        String templateName2="test2.ftl";
        Map data2=new HashMap();
        data2.put("name","wjy");
        try {
          //freeMarkerUtil2.ExecuteFile(templateName2,data2,"d:\\test2.html");
          String str=  freeMarkerUtil2. ExecuteString(templateName2,data2);
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
