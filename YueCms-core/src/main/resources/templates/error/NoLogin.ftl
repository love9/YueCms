﻿<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
  <meta http-equiv="Cache-Control" content="no-siteapp" />

  <title>请先登录</title>
</head>
<script>
//判断当前窗口是否有顶级窗口，如果有就让当前的窗口的地址栏发生变化，
function loadTopWindow(){
  if (window.top!=null && window.top.document.URL!=document.URL){
    window.top.location= document.URL; //这样就可以让登陆窗口显示在整个窗口了
  }
}
</script>

<body onload="loadTopWindow()">

  <style>
    *{margin:0;padding:0;}
    .cy-my-page {
      margin:0 auto;
      position: relative;
      top: 0;
      left: 0;
      width: 630px;
      overflow: hidden;
      overflow-y: auto;
      display:block;
    }

    .cy-my-page .cy-my-comment-list {
      display: block;
      text-align: center;
      position: relative;
      top: calc(50% - 333px);
    }
    .cy-my-page>ul{display:inline-block;

      vertical-align: middle;}
    .cy-my-page .cy-my-comment-list .empty-hold-place {
      display: block;
      text-align: center;
      line-height: 16px;
      font-size: 16px;
      font-family: "Microsoft YaHei";
      width: 345px;
      margin: 0 auto;
      padding-bottom: 50px;
    }
    .cy-my-page .cy-my-comment-list .empty-hold-place .pet-pic {
      margin-top: 100px;
      width: 345px;
      height: 293px;
      background: url(/resources/images/notice-empty.png);
      background-repeat: no-repeat;
    }
    .comment-empty-txt {
      /* background-image: url(https://changyan.itc.cn/mdevp/extensions/cy-user-info/063/image/title-nocomment.png);*/
      color:#333;
      font-size:22px;
      font-family:"Comic Sans MS", cursive;
      margin-top: 40px;
    }
    .power-by-cy-txt {
      width: 345px;
      height: 16px;
      /*background: url(https://changyan.itc.cn/mdevp/extensions/cy-user-info/063/image/power-by.png);*/
      background-repeat: no-repeat;
      margin-top: 30px;
      color:#d2cccc;
      font-size: 14px;
    }
    .go-front {
      font-weight: 400;
      width: 130px;
      height: 38px;
      background:rgb(78, 183, 229);
      line-height: 38px;
      font-size: 16px;
      color: rgb(255, 255, 255);
      text-align: center;
      display: inline-block;
      border-radius: 2px;
      cursor:pointer;
    }
    a {
      color: rgb(25, 25, 25);
      text-decoration: none;
    }
  </style>
  <div  class="cy-my-page" style="height: 100%;width: 100%;">
    <ul class="cy-my-comment-list">

      <div class="empty-hold-place">
        <div class="pet-pic"></div>
        <div class="empty-txt comment-empty-txt">${info}</div>
        <div class="power-by-cy-txt">—————&nbsp;&nbsp;Powdered By YueCms&nbsp;&nbsp;—————</div>
      </div>
      <h5><a onclick="toLogin()" class="go-front">立即登录</a></h5>
    </ul>

  </div>
  <script>
    var sys_ctx="";
    function toLogin(){
      var url=  window.location.href;
      var host=window.location.host;
      var protocol=window.location.protocol;
      var uri=url.replace(protocol+"//"+host,"");
      if(uri.indexOf("/myblog")>=0){
        top.window.location.href='/myblog/login?redirectUrl='+uri;
      }else{//后台登录
        top.window.location=sys_ctx+"/admin/login";
      }
    }
    var redirectUrl='${redirectUrl}';
    var noRedirect='${noRedirect}';//不需要重定向
    var url=sys_ctx+"/login";

    if(redirectUrl!=''&&redirectUrl!="null"&&redirectUrl!="undefined"){
      if(noRedirect=='1'){
      }else{
        url+="?redirectUrl="+redirectUrl;
      }
    }
  </script>

</body>
</html>