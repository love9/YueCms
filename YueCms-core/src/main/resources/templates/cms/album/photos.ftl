
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>相册</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="/resources/lib/layui-v1.0.7/layui/css/layui.css"   >
    <link href="/resources/css/animate.min.css" rel="stylesheet">
    <link href="/resources/css/index/index.css" rel="stylesheet">
    <link href="/resources/css/admui/site.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/lib/layer-v3.1.1/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>


    <style>

    .pagebar:after{clear:both;}
    </style>
<script type="text/javascript">
    var page=1;//当前页
    var rows=6;//每页的记录数
    var totalPages=0;
    function showPhotos(obj)
    {
        var id=$(obj).data("value");
        //alert(id);
        $.getJSON('/cms/album/json/findImagesByAlbumId?id='+id, function(json){
           // alert(JSON.stringify(json));
            layer.ready(function(){
                layer.photos({
                    photos: json
                    ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
                });
            });

        });

    }
    /**
    *  findPhotos接口，获取cms_album表类型是photos
     *  返回数据json的格式：
     *  ｛
     *      [
     *          id:专辑相册的id。放到html 的button的data-value内
     *          name:相册专辑的名称 放到h3标签内
     *          frontpath:封面图片路径
     *          time:创建时间放到span标签内
     *      ]
     *  ｝
     */
    $(function(){
        list();


    })

    function list()
    {
      //  alert(page);
        //?page=1&rows=2
        $.getJSON('/cms/album/json/findPhotos?page='+page+"&rows="+rows, function(json){
            //  alert(JSON.stringify(json));
            var html="";
            $.each(json,function(index, item){
                totalPages=item.totalPages;
                html+=" <li>";
                html+=" <div class=\"cover overlay overlay-hover\">";
                html+=" <img class=\"cover-image overlay-scale\" src=\""+item.frontpath+"\" >";
                html+="<div class=\"overlay-panel overlay-fade overlay-background overlay-background-fixed text-center vertical-align\">";
                html+="<div class=\"vertical-align-middle\">";
                html+="<div class=\"widget-time widget-divider\">";
                html+=" <span>"+item.time+"</span>";
                html+="</div>";
                html+="<h3 class=\"widget-title margin-bottom-20\">"+item.name+"</h3>";
                html+="<button type=\"button\" data-value=\""+item.id+"\" class=\"btn btn-outline btn-inverse\" onclick=\"showPhotos(this)\">更多信息</button>";
                html+="</div>";
                html+=" </div>";
                html+=" </div>";
                html+=" </li>";
            });

            $("#myphotos").html(html);
            pager(totalPages);
        });
    }
    function pager(totalPages){
        var $pagebar=$("body").find(".pagebar");
        laypage({
            cont:$pagebar, //容器。值支持id名、原生dom对象，jquery对象,
            pages: totalPages, //总页数
            curr:page,
            jump:function(e,first){
                if(!first){ //一定要加此判断，否则初始时会无限刷新
                    page=e.curr;
                    list();
                }

            },
            skin: 'molv', //皮肤
            first: '首页', //若不显示，设置false即可
            last: '尾页', //若不显示，设置false即可
            prev: '上一页', //若不显示，设置false即可
            next: '下一页' //若不显示，设置false即可
        });
    }
</script>
</head>

<body class="gray-bg" style="padding:0">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-content" >



                </div>

                <div class="ibox-content">
                    <div class="page animation-fade page-gallery-grid">
                        <div class="page-content padding-0">
                    <ul id="myphotos" class="blocks no-space blocks-100 blocks-xlg-3 blocks-md-2">
                     <#--   <li class="">
                            <div class="cover overlay overlay-hover">
                                <img class="cover-image overlay-scale" src="/resources/images/photos/view-1.jpg" alt="...">
                                <div class="overlay-panel overlay-fade overlay-background overlay-background-fixed text-center vertical-align">
                                    <div class="vertical-align-middle">
                                        <div class="widget-time widget-divider">
                                            <span>2016年8月18日</span>
                                        </div>
                                        <h3 class="widget-title margin-bottom-20">View-1</h3>
                                        <button type="button" data-value="1" class="btn btn-outline btn-inverse" onclick="showPhotos(this)">更多信息</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="">

                            <div class="cover overlay overlay-hover">
                                <img class="cover-image overlay-scale" src="/resources/images/photos/view-2.jpg" alt="...">
                                <div class="overlay-panel overlay-fade overlay-background overlay-background-fixed text-center vertical-align">
                                    <div class="vertical-align-middle">
                                        <div class="widget-time widget-divider">
                                            <span>2016年8月18日</span>
                                        </div>
                                        <h3 class="widget-title margin-bottom-20">View-2</h3>
                                        <button type="button" class="btn btn-outline btn-inverse">更多信息</button>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="">

                            <div class="cover overlay overlay-hover">
                                <img class="cover-image overlay-scale" src="/resources/images/photos/view-3.jpg" alt="...">
                                <div class="overlay-panel overlay-fade overlay-background overlay-background-fixed text-center vertical-align">
                                    <div class="vertical-align-middle">
                                        <div class="widget-time widget-divider">
                                            <span>2016年8月18日</span>
                                        </div>
                                        <h3 class="widget-title margin-bottom-20">View-3</h3>
                                        <button type="button" class="btn btn-outline btn-inverse">更多信息</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="">

                            <div class="cover overlay overlay-hover">
                                <img class="cover-image overlay-scale" src="/resources/images/photos/view-4.jpg" alt="...">
                                <div class="overlay-panel overlay-fade overlay-background overlay-background-fixed text-center vertical-align">
                                    <div class="vertical-align-middle">
                                        <div class="widget-time widget-divider">
                                            <span>2016年8月18日</span>
                                        </div>
                                        <h3 class="widget-title margin-bottom-20">View-4</h3>
                                        <button type="button" class="btn btn-outline btn-inverse">更多信息</button>
                                    </div>
                                </div>
                            </div>
                        </li>-->
                    </ul>
                        </div>
                    </div>
                </div>

                <!--分页条-->
                <div class="pagebar"  ></div>

            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="/resources/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/blueimp/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layui-v1.0.7/layui/layui.js"></script>
<script type="text/javascript">
    layui.use('flow', function(){
        var $ = layui.jquery; //不用额外加载jQuery，flow模块本身是有依赖jQuery的，直接用即可。
        var flow = layui.flow;
        flow.lazyimg({
            elem: '#LAY_div img'
            ,scrollElem: '#LAY_div' //一般不用设置，此处只是演示需要。
        });
    });
</script>
</body>
</html>
