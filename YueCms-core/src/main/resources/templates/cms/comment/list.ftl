<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>评论管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">



</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> cms <span class="c-gray en">&gt;</span>评论管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                    <a onclick="addNew()" class="btn  btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                    <a onclick="list_del('tableList')" class="btn  btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                    <a onclick="query('form_query');" class="btn  btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                </div>
                <div class="alert alert-warning" style="display: inline-block; margin-bottom: 0px;margin-left:8px;">
                    <strong>提示：</strong> 双击行进入编辑界面
                </div>
        </form>

    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
<script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'target_id',align:"center",sortable:true,order:"asc",visible:false,title: '文章ID',width:"120px"},
            {field: 'title',align:"center",sortable:true,order:"asc",align:'left',visible:true,title: '标题'},
            {field: 'type',align:"center",sortable:true,order:"asc",visible:true,title: '类型',width:"120px",formatter:function(v){
                    if(v=='article'){
                        return "<font color='red'>文章</font>";
                    }else if(v=='book'){
                        return "<font color='green'>书籍</font>";
                    }else if(v=='chapter'){
                        return "<font color='orange'>章节</font>";
                    }else{
                        return "<font color='gray'>其它</font>";
                    }
                }
            },
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:true,title: '用户ID',width:"120px"},
            {field: 'username',align:"center",sortable:true,order:"asc",visible:true,title: '用户名称',width:"120px"},
            {field: 'usericon',align:"center",sortable:true,order:"asc",visible:true,title: '用户头像',width:"120px",formatter:function(v){
                if(isNotEmpty(v)){
                    return "<img onclick='preViewImg(this)' width='50px' height='50px' src='"+v+"'/>";
                }else{
                    return "-";
                }
            }},
            {field: 'comment',align:"center",sortable:true,order:"asc",align:'left',visible:true,title: '评论内容'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.onDblClickRow=onClickRow;
        table_list_Params.url='/cms/comment/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    var preViewImg=function(obj){
        var src=$(obj).attr("src");
        var img="<img src='"+src+"'/>";
        layer_openHtml("",img);
    }
    var onClickRow=function(row,tr){
        $.layer.open_page("编辑评论",sys_ctx+"/cms/comment/edit?id="+row.id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        })
    }
    function addNew(){
        $.layer.open_page("新增评论",sys_ctx+"/cms/comment/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        })
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/comment/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
