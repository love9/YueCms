<!DOCTYPE html>
<html>
<head>
  <title>编辑评论</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body  class="body-bg-edit">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
    <input type="hidden" value="${comment.id!}" id="id" name="id"/>
		<h2 class="text-center">编辑评论</h2>
        <div class="form-group">
              <label class="col-sm-2 control-label">外键。指向文章id：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="target_id" name="target_id" placeholder="外键。指向文章id" value="${comment.target_id!}" class="form-control" datatype="*" nullmsg="请输入外键。指向文章id" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">评论对象类型：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="type" name="type" placeholder="评论对象类型" value="${comment.type!}" class="form-control" datatype="*" nullmsg="请输入评论对象类型" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">标题：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="title" name="title" placeholder="标题" value="${comment.title!}" class="form-control" datatype="*" nullmsg="请输入标题" />
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">评论用户id：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="yhid" name="yhid" placeholder="评论用户id" value="${comment.yhid!}" class="form-control" datatype="*" nullmsg="请输入评论用户id" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">评论用户的名称：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="username" name="username" placeholder="评论用户的名称" value="${comment.username!}" class="form-control" datatype="*" nullmsg="请输入评论用户的名称" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">用户头像：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="usericon" name="usericon" placeholder="用户头像" value="${comment.usericon!}" class="form-control" datatype="*" nullmsg="请输入用户头像" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">评论内容：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="comment" name="comment" placeholder="评论内容" value="${comment.comment!}" class="form-control" datatype="*" nullmsg="请输入评论内容" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
    </form></div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/cms/comment/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
         validform=$("#form_show").Validform({
                  btnReset:"#reset",
                 tiptype:2,
                 postonce:true,//至提交一次
                 ajaxPost:false,//ajax方式提交
                 showAllError:true //默认 即逐条验证,true验证全部
         });
	})
</script>

</body>
</html>
