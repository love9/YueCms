<footer>
    <div class="ui black inverted vertical footer segment">
        <div class="ui center aligned container">
            <div class="ui stackable inverted grid">
                <div class="column">
                    <p><span style="color:#666;">站点声明：本站转载作品版权归原作者及来源网站所有，原创内容作品版权归作者所有，任何内容转载、商业用途等均须联系原作者并注明来源。</span></p>
                    <p> <span style="color:#666;">   相关侵权、举报、投诉及建议等，请联系747506908@qq.com，我们定将及时处理。</span> </p>
                    <p style="color:#888;">友情链接：

                        <a style="padding-right:20px; color:#888;" href=" http://www.kuaidi.com/all/yunda.html" target="_blank">韵达快递查询</a>
                        <a style="padding-right:20px; color:#888;" href="//www.aikuaidi.cn/" target="_blank" >快递查询</a>
                        <a style="padding-right:20px; color:#888;" href="http://www.yangqq.com/" target="_blank">杨青博客</a>
                        <a style="padding-right:20px; color:#888;" href="http://nec.netease.com/" target="_blank">NEC</a>
                        <a style="padding-right:20px; color:#888;" href="http://amazeui.org/" target="_blank">amazeUi</a>
                    </p>
                </div>
            </div>

            <div class="ui inverted section divider" style="margin:16px auto;"></div>
            <img src="/resources/images/logo/logo.png" class="ui centered mini image">
            <div class="ui horizontal inverted small divided link list">
                <a style="color:#FFF" class="item" >@2016-2018 我的博客 All Rights Reserved</a>
            </div>
        </div>
    </div>
</footer>