
<div id="outerdiv" style="position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0.7); z-index: 2; width: 100%; height: 100%; display: none;">
    <div id="innerdiv" style="position: absolute; top: 80.8px; left: 446.079px;">
        <img id="bigimg" style="border: 5px solid rgb(255, 255, 255); width: 969.842px;" src="">
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("article img,.articleDetail img").click(function(){
            var _this = $(this);
            imgshow("#outerdiv", "#innerdiv", "#bigimg", _this);
        });
    });

    function imgshow(outerdiv, innerdiv, bigimg, _this){
        var src = _this.attr("src");
        $(bigimg).attr("src", src);

        $("<img/>").attr("src", src).load(function(){
            var windowW = $(window).width();
            var windowH = $(window).height();
            var realWidth = this.width;
            var realHeight = this.height;
            var imgWidth, imgHeight;
            var scale = 0.8;

            if(realHeight>windowH*scale) {
                imgHeight = windowH*scale;
                imgWidth = imgHeight/realHeight*realWidth;
                if(imgWidth>windowW*scale) {
                    imgWidth = windowW*scale;
                }
            } else if(realWidth>windowW*scale) {
                imgWidth = windowW*scale;
                imgHeight = imgWidth/realWidth*realHeight;
            } else {
                imgWidth = realWidth;
                imgHeight = realHeight;
            }
            $(bigimg).css("width",imgWidth);

            var w = (windowW-imgWidth)/2;
            var h = (windowH-imgHeight)/2;
            $(innerdiv).css({"top":h, "left":w});
            $(outerdiv).fadeIn("fast");
        });

        $(outerdiv).click(function(){
            $(this).fadeOut("fast");
        });
    }
</script>