
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>
  <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>

<body  class="pushable">
<!--手机试图导航-->
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
    <div class="ui basic segment" id="mainScreen">
       <div class="ui container" style="background: #fff;">
           <div class="ui internally grid web-blog">
           <div class="row" >

               <div class="two wide column computer only left-channel">
<!-- 侧边导航栏 -->
               <#include "/cms/myblog/semantic/admin/personal_menu.ftl"/>
               </div>

            <!-- 内容区域 -->
            <div class="fourteen wide computer eleven wide tablet sixteen wide mobile column">

                   <div class="ui list">
                       <div class="item">
                           <div class="ui right floated content">
                               <#--<a href="https://www.oschina.net/blog/recommend">更多</a>-->
                           </div>
                           <h3 class="ui header">文章列表</h3>
                       </div>
                   </div>
                   <div class="ui divider"></div>
                    <div class="row">
                        <form class="ui basic segment form">
                            <script>
                                $(function(){
                                    $('.dropdown.selection').dropdown();
                                    $('.dropdown.button').dropdown({on:"hover"});
                                })
                            </script>

                            <div class="three fields">

                                <div class="inline field">
                                    <label>类别</label>
                                    <div class="ui dropdown selection" tabindex="0">
                                        <select id="available"  name="available">
                                            <option value="">所有类别</option>
                                            <option value="1">已发布</option>
                                            <option value="0">草稿</option>
                                        </select>
                                        <i class="dropdown icon"></i>
                                        <div class="default text">-请选择类别-</div>
                                        <div class="menu transition hidden" tabindex="-1">
                                            <div class="item" data-value="">所有类别</div>
                                            <div class="item" data-value="1">已发布</div>
                                            <div class="item" data-value="0">草稿</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="inline field">
                                    <label>标题</label>
                                    <input placeholder="文章标题" id="title" name="title" type="text">
                                </div>
                                <div class="field">
                                    <button onclick="postlist()"  class="ui primary submit button">查询</button>
                                </div>
                            </div>
                        </form>

                        <div class="ui divider"></div>

                        <div class="ui divided items">

                        <#if articles.rows??>

                        <#list articles.rows as item>

                            <div class="item" id="item_${item.id}">
                                <#if item.cover_image?? && item.cover_image!="">
                                    <div class="image">
                                        <img src="${item.cover_image!}">
                                    </div>
                                </#if>
                                <div class="content">
                                    <a class="header" target="_blank" href="/myblog/article/detail/${item.id!}">${item.title!}</a>
                                    <div class="meta">
                                        <span class="cinema">${item.createTime!}</span>
                                    </div>
                                    <div class="description">
                                        <p></p>
                                    </div>
                                    <div class="extra">
                                        <div class="right floated  mini ui buttons bottom">
                                            <button class="ui twitter button" onclick="editContent('${item.id!}')">
                                                <i class="pencil icon"></i>
                                                编辑
                                            </button>
                                            <#if item.comment_flag?? && item.comment_flag==1>
                                                <button class="ui orange button" data-type="1"  onclick="updateCommentFlag(this,'${item.id!}')">
                                                    <i class="chat icon"></i>
                                                    <span>禁止评论</span>
                                                </button>
                                            <#else>
                                                <button class="ui orange button" data-type="0" onclick="updateCommentFlag(this,'${item.id!}')">
                                                    <i class="chat icon"></i>
                                                    <span>允许评论</span>
                                                </button>
                                            </#if>

                                            <button class="ui green button" onclick="editContent('${item.id}')">
                                                <i class="arrow up icon"></i>
                                                    <#if item.top_flag?? && item.top_flag=='1'>
                                                        取消置顶
                                                    <#else>
                                                        置顶
                                                    </#if>
                                            </button>
                                            <button class="ui red button"  onclick="deleteBlog('${item.id!}')" >
                                                <i class="delete   icon"></i>
                                               删除
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            </#list>

                            </#if>

                            <div class="ui divider"></div>
                            <div style="position:relative;bottom:0px;text-align: center;" id="pagebar" class="pagebar"></div>

                            <script type="text/javascript">
                                var sys_ctx="";
                                var pageNo=${articles.page!};
                                var pageSize=10;
                                var totalPages =${articles.totalPages!};
                                initPager();
                                function initPager() {
                                    var $pagebar = $("#pagebar");
                                    laypage({
                                        cont: $pagebar, //容器。值支持id名、原生dom对象，jquery对象,
                                        pages: totalPages, //总页数
                                        curr: pageNo,
                                        jump:jumpPage,
                                        skin: 'molv', //皮肤
                                        first: '首页', //若不显示，设置false即可
                                        last: '尾页', //若不显示，设置false即可
                                        prev: '上一页', //若不显示，设置false即可
                                        next: '下一页' //若不显示，设置false即可
                                    });
                                }

                                function jumpPage(obj,first){
                                    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                    console.log(obj.limit); //得到每页显示的条数
                                    //alert("共"+context.option.pages+"页，当前第"+context.option.curr+"页");
                                    pageNo=obj.curr;
                                    //alert(pageNo);
                                    if(!first){
                                        var uri=Fast.getUri();
                                        window.location.href=uri+"?page="+pageNo+"&rows="+pageSize+"&myblog=1";
                                    }
                                }

                                function postlist(){
                                    window.location.href="/myblog/admin/postlist?available="+$("#available").val()+"&title="+$("#title").val();
                                }
                                function editContent(id){
                                    layer.msg("该功能尚未实现!^_^");
                                }
                                function updateCommentFlag(obj,id){
                                    var v=$(obj).data("type");
                                    var o=$(".commentFlag.modal");
                                    o.find(".content p b").text(v==1?"禁止":"允许"),
                                    o.modal({onApprove:function(){
                                        sys_ajaxPost("/myblog/admin/post_updateCommentFlag",{id:id},function(data){
                                            var json=typeof data=='string'?JSON.parse(data):data;
                                            var type=json.type;
                                            if(type=='success'){
                                                layer.msg(json.content);
                                                //$(obj).data("type",v==1?1:0);
                                                //$(obj).find("span").html(v==1?"允许评论":"禁止评论");
                                                setTimeout(function(){
                                                    window.location.href="/myblog/admin/postlist";
                                                },1000);
                                            }else{
                                                $.layer.msg_cry(json.content);
                                            }
                                        });
                                    }
                                    }).modal("show");

                                    return;


                                }
                                function setTop(id){
                                    sys_ajaxPost("/myblog/admin/post_setTop",{id:id},function(data){
                                        var json=typeof data=='string'?JSON.parse(data):data;
                                        var type=json.type;
                                        if(type=='success'){
                                            layerMsg_rightIcon(json.content);
                                            setTimeout(function(){
                                                window.location.href="/myblog/admin/postlist";
                                            },1000);
                                        }else{
                                            $.layer.msg_cry(json.content);
                                        }
                                    });
                                }
                                function deleteBlog(id){

                                    var o=$(".delete-blog.modal");
                                    //o.find("span.catalog").text(e.data("name")),
                                    o.modal({onApprove:function(){
                                        sys_ajaxPost("/myblog/admin/post_delete",{id:id},function(data){
                                            var json=typeof data=='string'?JSON.parse(data):data;
                                            var type=json.type;
                                            if(type=='success'){
                                                layer.msg(json.content);
                                                //移除dom
                                                $("#item_"+id).remove();
                                                //layerAlert(json.content,function(){
                                                //    window.location.href="/myblog/admin/postlist";
                                                //});
                                            }else{
                                                layerAlert(json.content);
                                            }
                                        });
                                    }
                                    }).modal("show");

                                    return;
                                    Fast.confirm("确定要删除该文章吗?",function(){
                                        sys_ajaxPost("/myblog/admin/post_delete",{id:id},function(data){
                                            var json=typeof data=='string'?JSON.parse(data):data;
                                            var type=json.type;
                                            if(type=='success'){
                                                layerAlert(json.content,function(){
                                                    window.location.href="/myblog/admin/postlist";
                                                });
                                            }else{
                                                layerAlert(json.content);
                                            }
                                        });
                                    });
                                }
                            </script>

</div>


                    </div>


               </div>

            </div>
           </div>
       </div>
    </div>
</div>
<!-- content end -->
<#include "/cms/myblog/semantic/block/footer.ftl"/>

<div class="ui small delete-blog modal transition hidden">
    <div class="header">询问</div>
    <div class="content">
        <p>确定要删除该文章吗?</p>
    </div>
    <div class="actions">
        <div class="ui negative button">否</div>
        <div class="ui positive right labeled icon button">是<i class="checkmark icon"></i>
        </div>
    </div>
</div>
<div class="ui small commentFlag  modal transition hidden">
    <div class="header">询问</div>
    <div class="content">
        <p>该文章是否要<b>禁止</b>评论吗?</p>
    </div>
    <div class="actions">
        <div class="ui negative button">否</div>
        <div class="ui positive right labeled icon button">是<i class="checkmark icon"></i>
        </div>
    </div>
</div>
</body>
</html>