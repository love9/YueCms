
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>reward</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <style>
        /*本demo 样式*/
        ul.demo{display: block;position: absolute;padding-left:calc(50% - 60px);margin-top:85px;}
        ul.demo li{display: block;float: left;width: 25px;height: 25px;background-color: #ccc;margin-right: 8px;cursor: pointer;}
        ul.demo li.green{background-color: #5fbf5f;}
        ul.demo li.red{background-color: #e74851;}
        ul.demo li.blue{background-color: #60b4e9;}
        ul.demo li.orange{background-color: #fca222;}
        .clear{clear: both;}
    </style>
</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer   sixteen wide mobile column  ">
                    <#include "/cms/myblog/semantic/reward/reward.ftl"/>
                  <ul class="demo">
                      <li class="green"></li> <li  class="red"></li> <li  class="blue"></li> <li  class="orange"></li>
                      <div class="clear"></div>
                  </ul>
                  <p style="position: absolute;display:block;text-align: center;font-size: 16px;color: #333;margin-top:140px;margin-left:calc(50% - 100px)">点击颜色块，切换按钮样式</p>
                  <script type="text/javascript">
                      $(".demo li").click(function(){
                          var type=$(this).attr("class");
                          layer.msg(type);
                          var add="";
                          if(type=='green'){
                              add="cy-reward-btn-green"
                          }else if(type=='red'){
                              add="cy-reward-btn-red"
                          }else if(type=='blue'){
                              add="cy-reward-btn-blue"
                          }else if(type=='orange'){
                              add="cy-reward-btn-orange"
                          }
                          $(document).find("#cy-reward-click").removeAttr("class").addClass("cy-reward-btn").addClass(add);
                      });
                  </script>

              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>