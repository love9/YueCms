
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>新增文章</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <link rel="stylesheet" href="/resources/lib/editormd/css/editormd.css"/>
    <script type="text/javascript"  src="/resources/lib/editormd/editormd.js"></script>
    <link rel="stylesheet" href="/resources/lib/simple-switch/jquery.simple-switch.css">
    <script type="text/javascript" src="/resources/lib/simple-switch/jquery.simple-switch.js"></script>
</head>

<body style="" class="pushable">
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher">
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->
    <div class="ui basic segment" id="mainScreen">
        <!-- content srart -->
        <form class="ui form">
            <div class="field">
                <label></label>
                <div class="fields">
                    <div class="twelve wide field">
                        <input id="blogTitle"  type="text" name="blogTitle" placeholder="请输入文章标题">
                    </div>
                    <div class="four wide field">
                        <a class="ui basic button" href="/cms/article/add">切换版本</a>&nbsp;&nbsp;&nbsp;<a class="ui primary button" onclick="publishSave()">发布</a>
                    </div>
                </div>
            </div>
            <div class="field">

                <div class="inline  fields">
                    <div class="twelve wide field">
                        <label>文章标签</label>
                        <input id="bkbq"  type="text" name="bkbq" placeholder="请输入文章标签">
                    </div>
                    <div class="four wide field">
                        <label>是否私有</label>
                        <div id="privateSwitch" class="simple-switch">
                            <input type="checkbox" />
                            <span class="switch-handler"></span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="field">
                <div id="layout" style="width: 100%;height: 100%;position: relative;z-index: 10;">
                    <div id="test-editormd">
                        <textarea style="display:none;" name="content"></textarea>
                    </div>
                </div>
            </div>
        </form>

        <!-- content end -->
    </div>
</div>
<script type="text/javascript">
    var testEditor;
    function getSourceValue(){
        var v=testEditor.getMarkdown();
        return v;
    }
    function getHtmlValue(){
        var v=testEditor.getHTML();
        alert(v);
        return v;
    }
    function getPreviewedHTML(){
        var v=testEditor.getPreviewedHTML();
        //alert(v);
        return v;
    }
    function closeModal(){
        var $modal = $('#your-modal');
        $modal.modal("close");
    }

    /*   function showPublish(){
           //var fl=$("#blogCate").val();
           var title=$("#blogTitle").val();
           var content=getPreviewedHTML();

           if($.trim(title)==''){
               layer.msg("标题不能为空!");
               return;
           }
           if($.trim(content)==''){
               layer.msg("文章内容不能为空!");
               return;
           }
           //alert("OK!");
           var $modal = $('#your-modal');
           $modal.modal();
           $("#privateSwitch").bootstrapSwitch();
       }*/
    function publishSave(){
        var title=$("#blogTitle").val();
        var content=getPreviewedHTML();

        if($.trim(title)==''){
            layer.msg("标题不能为空!");
            return;
        }
        if($.trim(content)==''){
            layer.msg("文章内容不能为空!");
            return;
        }

        var sourceContent=getSourceValue();
        var content=getPreviewedHTML();

        var bkbq=$("#bkbq").val();
        var tags=[];
        var v=  $("#privateSwitch").simpleSwitch('state');
        var params="&title="+title+"&isprivate="+v;
        $.ajax({
            type: 'POST',
            url: '/myblog/blog/save?1=1'+params,
            data:{'content':content,'sourceContent':sourceContent,'bkbq':bkbq},
            contentType: 'application/x-www-form-urlencoded',
            //contentType: 'application/json',
            success:function(data){
                var json=data;
                // alert(JSON.stringify(json));
                if(json.type=="success"){
                    ajaxReturnMsg(data);
                    setTimeout(function(){
                        window.location.href="/myblog/newblogSuccess?title="+title+"&id="+json.data.id;
                    },800);

                }else{
                    layerAlert(json.content);
                }

            }
        });

    }
    /*function draftSave(){
        alert("草稿"+fl+"=="+title+"=="+content+"=="+bkfl+"=="+bkbq);
    }*/
    $(function () {
        testEditor = editormd("test-editormd", {
            width: "100%",
            height: 640,
            emoji:true,
            syncScrolling: "single",
            saveHTMLToTextarea : true,
            path: "/resources/lib/editormd/lib/",
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "/myblog/uploadSingle"
            /*// 上传的后台只需要返回一个 JSON 数据，结构如下：
             {
             success : 0 | 1,           // 0 表示上传失败，1 表示上传成功
             message : "提示的信息，上传成功或上传失败及错误信息等。",
             url     : "图片地址"        // 上传成功时才返回
             }
             */
        });

    });
</script>
<!-- content end -->
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>