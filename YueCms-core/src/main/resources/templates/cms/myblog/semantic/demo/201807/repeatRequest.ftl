
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>模拟重复发送ajax请求</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer  sixteen wide mobile column  ">

                  <form class="ui basic form">
                      <div class="field">
                          <label for="url">URL</label>
                          <input type="url" class="" id="url" value="http://localhost:8099/cms/report/json/doReport" placeholder="输入请求URL">
                      </div>
                      <div class="field">
                          <label for="timespan">请求间隔：</label>
                          <select id="timespan">
                              <option selected value="2">2秒</option>
                              <option value="4">4秒</option>
                              <option value="5">5秒</option>
                          </select>
                      </div>
                      <div class="field">
                          <label for="data">请求结果：</label>
                          <textarea class="" rows="15" id="data"></textarea>
                      </div>
                      <a class="ui primary button" onclick="start()"  >发送请求</a>
                  </form>


                    <script type="text/javascript">

                        function start(){
                            var url=$("#url").val();
                            sys_ajaxPost(url,null,function(data){
                                var str=JSON.stringify(data);
                                var time = new Date().Format("yyyy-MM-dd HH:mm:ss");
                                str=time+" : "+str+"\r\n";
                                $("#data").append(str);
                                var psconsole = $('#data');
                                if(psconsole.length)
                                    psconsole.scrollTop(psconsole[0].scrollHeight - psconsole.height());//定位到最后一行
                            })
                            return;

                            if(isEmpty(url)){
                                layer.msg("请输入正确的url!");
                                return;
                            }
                            var timespan=$("#timespan").val();
                            var interval = setInterval(function(){
                            },timespan*1000000)
                        }
                        Date.prototype.Format = function (fmt) {
                            var o = {
                                "M+": this.getMonth() + 1, //月份
                                "d+": this.getDate(), //日
                                "H+": this.getHours(), //小时
                                "m+": this.getMinutes(), //分
                                "s+": this.getSeconds(), //秒
                                "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                                "S": this.getMilliseconds() //毫秒
                            };
                            if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
                            for (var k in o)
                                if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
                            return fmt;
                        }
                    </script>

              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>