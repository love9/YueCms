
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>MyBlog首页</title>
  <meta name="renderer" content="webkit">


    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <link href="/resources/lib/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script src="/resources/lib/owlCarousel/owl.carousel.min.js"></script>

</head>
<body id="myblog-index"  class="pushable ">
<script>
    var sys_ctx="";
</script>
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<!--  置顶 -->
<#include "/cms/myblog/semantic/block/topFullPitch.ftl"/>
<!-- fullTabs 置顶热门标签 -->
    <#include "/cms/myblog/semantic/block/index_fullTabs.ftl"/>

<!-- content srart -->
    <div class="ui basic segment" id="mainScreen"  >
        <div class="ui container grid" style="background: #fff;">
            <div class="row" >
                <div class="sixteen wide tablet eleven wide computer column"  >
        <!--banner-->
<#include "/cms/myblog/semantic/block/index_banner.ftl"/>
        <!--头条专区-->
<#include "/cms/myblog/semantic/block/index_article_top.ftl"/>

        <!--特别推荐ad-->
        <div class="itemBox " style="padding:0;margin:0 -1.5rem;margin-bottom: 10px;">
            <div class="adGrid homeMid" style="margin:0;" ><a><img src="/resources/myblog/images/tbtj.jpg"></a></div>
        </div>

        <!--首页文章列表精选-->
<#include "/cms/myblog/semantic/block/index_article_recommend.ftl"/>
            </div>
    <!--左侧内容 end-->

<!--右侧内容 start-->
   <div class="sixteen wide tablet five wide computer column">
<!--最新专区-->
<#include "/cms/myblog/semantic/block/index_article_new.ftl"/>
<!--最新专区-->
<#include "/cms/myblog/semantic/block/index_siteInfo.ftl"/>
   </div>
<!--右侧内容 end-->
    </div>
<!-- content end -->
        </div>
    </div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
<a class="to-top" title=""  style="position: fixed; right: 25px; bottom: 50px; cursor: pointer; display: block;" data-original-title="点击返回顶部"  ></a>
</body>
</html>