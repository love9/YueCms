<!-- nav start -->

<script>
    var sys_ctx="";
</script>
<nav id="nav">
<div class="ui  inverted menu" style="height: 58px;margin: 0;">

        <div class="header item">
            <div style="display: block;float: left;margin-right: 20px;">
                <img style="width: 50px;height:50px;display: inline-block;" src="/resources/images/logo/logo.png">
                <span style="display: inline-block;color:#fff;font-size: 1.3rem;position: relative;bottom: 18px;">我的博客</span>
            </div>
        </div>
        <a id="nav_index" class="item" href="/myblog/index">首页</a>
        <#--<a class="item" href="/myblog/aboutMe">关于我</a>-->
        <a class="item" href="/myblog/bloglist">博客</a>

        <#if front_yhmc??>
            <a class="item" href="/myblog/img">相册</a>
        </#if>
        <a id="nav_book" class="item" href="/myblog/booklist">读书</a>
        <a class="item" href="/myblog/updateLog">更新日志</a>
        <a class="item" href="/myblog/demo/201807/liuyan">留言</a>
        <div class="right menu">
            <div class="item" id="div_search">
                <div class="ui transparent inverted icon input">
                    <input type="text" class="searchInput"  placeholder="Search">
                    <div id="searchbtn"> <i class="search icon" style="cursor:pointer;color:#666;"></i></div>
                </div>
            </div>
            <div id="newArticle"  class="item">
                <a class="ui primary button" onclick="writeBlog()">
                    写博客
                </a>
            </div>
<#if front_yhmc??>
            <div class="ui dropdown item" tabindex="0">
                ${front_yhmc!}<i class="dropdown icon"></i>
                <div class="menu" tabindex="-1">
                    <a class="item" href="/myblog/personalBlog">个人博客</a>
                    <#--<a class="item" href="/myblog/admin/postlist">管理博客</a>-->
                    <a class="item" href="/admin/login">登录后台</a>
                    <a class="item" href="/myblog/logout">退出</a>
                </div>
            </div>
<#else>
            <div class="item" id="login_form">
                <div class="ui tiny buttons">
                    <button class="ui positive basic button" onclick="headerLogin()">登录</button>
                    <button class="ui orange  basic button" onclick="javascript:alert('功能在开发中...');">注册</button>
                </div>
            </div>
</#if>

        </div>

    <script>
        $(document).ready(function() {
            // 鼠标放到 dropdown 时显示下拉菜单，默认只有点击后才显示
            $('.dropdown.item').dropdown({
                on: 'hover'
            });
            var n=($("#mobileNavSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
            $(".toggle-mobile-nav-sidebar").on("click",function(e){n.sidebar("toggle"),e.preventDefault()});
            var a=($("#mobileNavRightSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
            $(".current-user-avatar").on("click",function(e){a.sidebar("toggle"),e.preventDefault()});
        });

        var url=  window.location.href;
        var host=window.location.host;
        var protocol=window.location.protocol;
        var uri=url.replace(protocol+"//"+host,"");
        function headerLogin(){
            window.location.href='/myblog/login?redirectUrl='+uri;
            //window.location.href='/sso/login?redirect_url='+uri;
        }
        function writeBlog(){
            window.location.href='/myblog/newblog';
        }
        function getCookie(name){
            //获取cookie字符串
            var strCookie=document.cookie;
            //将多cookie切割为多个名/值对
            var arrCookie=strCookie.split("; ");
            var value="";
            //遍历cookie数组，处理每个cookie对
            for(var i=0;i<arrCookie.length;i++){
                var arr=arrCookie[i].split("=");
                if(name==arr[0]){
                    value=arr[1];
                    break;
                }
            }
            return value;
        }
        function removeNavClass(){
            var lis = document.getElementById("nav").getElementsByTagName("a");
            for (var i = 0; i < lis.length; i++) {
                removeClass(lis[i],"active");
            }
        }
        function showHide(uri){
            uri=uri.replace("/myblog","");
            if(uri.indexOf("?")>0){
                uri=uri.substring(0,uri.indexOf("?"));
            }
            if(uri.indexOf("/admin")>=0){
                $("#div_search").hide();
            }else{
                $("#div_search").show();
            }
            if(uri.indexOf("blog")>=0||uri.indexOf("personalBlog")>=0||uri.indexOf("/admin")>=0){//写博客按钮 显示
                document.getElementById("newArticle").style.display="block";
            }else{
                document.getElementById("newArticle").style.display="none";
            }
        }
        function addNavClass(uri){
            uri=uri.replace("/myblog","");
            if(uri.indexOf("?")>0){
                uri=uri.substring(0,uri.indexOf("?"));
            }
            var lis = document.getElementById("nav").getElementsByTagName("a");
            for (var i = 0; i < lis.length; i++) {

                if(lis[i].href.indexOf(uri)>=0){
                    lis[i].className="item active";
                    break;
                }
            }
        }
        function removeClass(elem,classname){
            if(elem.className != ""){
                var allClassName = elem.className.split(" ");
                //console.log("第一次赋值后class数量为：" + allClassName);
                var result;//完成操作后保存类名（在以后调用）
                var listClass;//保存修改过后的数组
                for (var i = 0; i < allClassName.length; i++) {
                    if(allClassName[i] == classname){
                        allClassName.splice(i,1);
                    }
                }
                listClass = allClassName;
                for (var j = 0; j < listClass.length; j++) {
                    //之后加上空格因为类名的存在形式就是用空格分隔的
                    if (j == 0) {
                        result = listClass[j];
                        result += " ";
                    }else{
                        result += listClass[j];
                        result += " ";
                    }
                }
                // console.log("处理后的listClass数量" + listClass);
                elem.className = result;//将目标元素的类名重新被 result 覆盖掉
            }else{
                //console.log("目标元素没有存在的类名")
            }
        }
        setTimeout(function(){
            showHide(uri);
        },5);
        setTimeout(function(){
            removeNavClass();
            addNavClass(uri)
            if(uri=="/login"||uri=='/myblog/index'||uri=='/myblog'){
                document.getElementById("nav_index").className="active item";
            }
            if(uri.indexOf("book/detail")>0){
                document.getElementById("nav_book").className="active item";
            }
        },50);



    </script>

</div>
</nav>
<div class="ui secondary inverted  menu" id="mobileHeaderNavMenu" style="display: none;">
    <a class="icon item toggle-mobile-nav-sidebar">
        <i class="large content icon"></i>
    </a>
    <div class="logo back-to-top-toggle"> <div style="display: block;float: left;margin-right: 20px;">
        <img style="width: 40px;height:40px;display: inline-block;" src="/resources/images/logo/logo.png">
        <span style="display: inline-block;color:#fff;font-size: 1.3rem;position: relative;bottom: 12px;">我的博客</span>
    </div>
    </div>
    <a class="icon item toggle-mobile-user-sidebar">

        <#if front_yhmc??>
        <div class="osc-avatar small-portrait _28x28 ui avatar image current-user-avatar">
            <#if front_headerPath??>
                    <img src="${front_headerPath!}">
            <#else>
                    <img src="/resources/images/headicons/girl-1.png">
            </#if>
        </div>
        </#if>
    </a>
</div>
<!-- nav end -->