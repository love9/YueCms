<div class="ui text compact vertical menu" id="sideMenu">

    <h4 class="header">全部</h4>
    <a href="/myblog/admin/postlist" class="item ">
        <i class="am-icon-newspaper-o sidebar-nav-link-logo"></i> 文章管理
    </a>
    <a href="/myblog/admin/category_list" class="item " >
        <i class="am-icon-bookmark-o sidebar-nav-link-logo"></i> 个人分类管理
    </a>
    <a href="/myblog/admin/personal_settings" class="item" >
        <i class="am-icon-bookmark-o sidebar-nav-link-logo"></i> 个人设置
    </a>
<script>


    $(function(){
        $("#sideMenu a").removeClass("active");
        $("#sideMenu a").each(function(){
            var href=$(this).attr("href");
            var uri=Fast.getUri();
            if(href.indexOf(uri)>=0){
                $(this).addClass("active");
                return false;
            }
        })
    })
</script>
</div>