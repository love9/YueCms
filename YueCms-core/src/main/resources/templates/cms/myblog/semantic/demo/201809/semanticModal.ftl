
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>semanticModal</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer  sixteen wide mobile column  ">

                    <div class="body-wrapper">
                       <div class="ui buttons">
                           <button id="modal1" class="ui red basic button">modal1</button>
                           <button id="modal2" class="ui orange basic button">modal2</button>
                           <button id="modal3" class="ui yellow basic button">modal3</button>
                           <button id="modal4" class="ui olive basic button">modal4</button>
                           <button id="modal5" class="ui green basic button">modal5</button>
                           <button id="modal6" class="ui teal basic button">modal6</button>
                           <button id="modal7" class="ui blue basic button">Scrolling Long Modal</button>
                           <button id="modal8"  class="ui violet basic button">modal8</button>

                       </div>
                        <div class="ui animation selection dropdown" tabindex="0">
                            <input type="hidden" name="transition" value="vertical flip">
                            <div class="text">Vertical Flip</div>
                            <i class="dropdown icon"></i>
                            <div class="menu transition hidden" tabindex="-1">
                                <div class="item">Horizontal Flip</div>
                                <div class="item active selected">Vertical Flip</div>
                                <div class="item">Fade Up</div>
                                <div class="item">Fade</div>
                                <div class="item">Scale</div>
                            </div>
                        </div>

                    </div>
                  <script type="text/javascript">
                      $(".ui.dropdown").dropdown();
                      $('.selection.dropdown').dropdown({
                                  onChange: function(value) {
                                      $('#modal_modal2') .modal('setting', 'transition', value).modal('show') ;
                                  }
                      }) ;

                      $("#modal1").click(function(){
                          $('#modal_modal1').modal('show');
                      })
                      $("#modal2").click(function(){
                          $('#modal_modal2').modal('show');
                      })
                      $("#modal3").click(function(){
                          $('#modal_modal3').modal('show');
                      })
                      $("#modal4").click(function(){
                          $('#modal_modal4').modal('show');
                      })
                      $("#modal5").click(function(){
                          $('#modal_modal5').modal('show');
                      })
                      $("#modal6").click(function(){
                          $('#modal_modal6').modal('show');
                      })
                      $("#modal7").click(function(){
                          $('#modal_modal7').modal('show');
                      })
                      $("#modal8").click(function(){
                          $('#modal_modal8').modal({
                              blurring: true
                          }).modal('show');
                      })


                  </script>

                  <div class="ui modal"  id="modal_modal8">
                      <div class="header">标题</div>
                      <div class="content">
                          <p>    123</p>
                      </div>


                  </div>


                  <div class="ui modal"  id="modal_modal1">
                      <div class="header">标题</div>
                      <div class="content">
                          <p></p>
                      </div>
                      <div class="actions">
                          <div class="ui approve button">批准</div>
                          <div class="ui button">中性</div>
                          <div class="ui cancel button">取消</div>
                      </div>
                  </div>

                  <div class="ui standard test modal transition hidden" id="modal_modal2">
                      <div class="header">
                          选择图片
                      </div>
                      <div class="image content">
                          <div class="ui medium image">
                              <img src="/resources/myblog/images/30.jpg">
                          </div>
                          <div class="description">
                              <div class="ui header">默认的图像轮廓</div>
                              <p>我们从<a href="https://www.gravatar.com" target="_blank">gravatar</a>抓取的下面这些图片，图像与你注册的邮箱地址相关.</p>
                              <p>可以使用这张照片吗?</p>
                          </div>
                      </div>
                      <div class="actions">
                          <div class="ui black deny button">
                              不可以
                          </div>
                          <div class="ui positive right labeled icon button">
                              是的，那是我
                              <i class="checkmark icon"></i>
                          </div>
                      </div>
                  </div>

                  <div class="ui small basic test modal transition"  id="modal_modal3">
                      <div class="ui icon header">
                          <i class="archive icon"></i>
                          存档的旧信息
                      </div>
                      <div class="content">
                          <p>你的收件箱已经满了，你要我们将旧信息自动归档？</p>
                      </div>
                      <div class="actions">
                          <div class="ui red basic cancel inverted button">
                              <i class="remove icon"></i>
                              否
                          </div>
                          <div class="ui green ok inverted button">
                              <i class="checkmark icon"></i>
                              是
                          </div>
                      </div>
                  </div>

                  <div class="ui fullscreen modal transition hidden"  id="modal_modal4">
                      <i class="close icon"></i>
                      <div class="header">
                          更新你的设置
                      </div>
                      <div class="content">
                          <div class="ui form">
                              <h4 class="ui dividing header">给我们您的反馈</h4>
                              <div class="field">
                                  <label>反馈</label>
                                  <textarea></textarea>
                              </div>
                              <div class="field">
                                  <div class="ui checkbox">
                                      <input type="checkbox" checked="checked" name="contact-me" tabindex="0" class="hidden">
                                      <label>可以联系我</label>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="actions">
                          <div class="ui button">取消</div>
                          <div class="ui green button">发送</div>
                      </div>
                  </div>


                  <div class="ui small test modal transition hidden"  id="modal_modal5">
                      <div class="header">
                          删除你的账户
                      </div>
                      <div class="content">
                          <p>你确定删除你的账户吗？</p>
                      </div>
                      <div class="actions">
                          <div class="ui negative button">
                              否
                          </div>
                          <div class="ui positive right labeled icon button">
                              是
                              <i class="checkmark icon"></i>
                          </div>
                      </div>
                  </div>


                  <div class="ui large test modal transition hidden"  id="modal_modal6">
                      <div class="header">
                          改变你的事
                      </div>
                      <div class="content">
                          <p>你想把它变成其他事物吗</p>
                      </div>
                      <div class="actions">
                          <div class="ui negative button">
                              否
                          </div>
                          <div class="ui positive right labeled icon button">
                              是
                              <i class="checkmark icon"></i>
                          </div>
                      </div>
                  </div>


                  <div class="ui long test modal scrolling transition hidden"  id="modal_modal7">
                      <div class="header">
                          资料图片
                      </div>
                      <div class="image content">
                          <div class="ui medium image">
                              <img src="https://semantic-ui.qyears.com/images/wireframe/image.png">
                          </div>
                          <div class="description">
                              <div class="ui header">模态框标题</div>
                              <p>这是一个例子，扩展的内容会导致模态框的调光器滚动</p>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                              <div class="ui divider"></div>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                              <div class="ui divider"></div>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                              <div class="ui divider"></div>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                              <div class="ui divider"></div>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                              <div class="ui divider"></div>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                              <div class="ui divider"></div>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                              <div class="ui divider"></div>
                              <img class="ui wireframe image" src="https://semantic-ui.qyears.com/images/wireframe/paragraph.png">
                          </div>
                      </div>
                      <div class="actions">
                          <div class="ui primary approve button">
                              继续
                              <i class="right chevron icon"></i>
                          </div>
                      </div>
                  </div>



              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
</body>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</html>