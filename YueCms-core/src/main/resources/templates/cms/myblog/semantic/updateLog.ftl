
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>博客更新列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">
  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>

<body  class="demo pushable">

<#include "/cms/myblog/semantic/block/updateLog_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
  <!-- nav start -->
  <#include "/cms/myblog/semantic/block/nav.ftl"/>
  <!-- nav end -->
  <div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
    <div class="ui container" style="background: #fff;">
      <div class="ui internally grid web-blog" id="tabContext">
        <div class="row" >
          <div class="four wide column computer only">
            <!--菜单-->
            <div class="ui vertical menu">
              <a class="header item active" data-tab="tab_demo">Demo列表</a>
              <a class="header  item" data-tab="tab_api">API接口</a>
              <a class="header  item" data-tab="tab_log">更新日志</a>
            </div>

          </div>

  <!-- 内容区域 -->
          <div class="twelve wide computer  sixteen wide mobile column  ">
            <div class="active ui tab segment update_content" data-tab="tab_demo">
              <style>
                .update_content ul li{list-style: disc;margin-left: 20px;color: #337ab7 !important; line-height: 30px;}
                .update_content ul li a{text-decoration: underline;}
                .update_content ul li a:hover {  color: #337ab7 !important;  text-decoration: underline;  }
                .update_content ul li span {  color: #939393;  margin-left: 10px;  }
              </style>
              <ul>
                <li><span>空白页面</span>&nbsp;&nbsp;<a href="/myblog/demo/201807/blank" target="_blank">blank</a></li>

                <li><span>simple-switch简单易用的开关</span>&nbsp;&nbsp;<a href="/myblog/demo/201807/simple-switch" target="_blank">simple-switch</a></li>
                <li><span>jBox-notice实现消息提示</span>&nbsp;&nbsp;<a href="/myblog/demo/201807/jBox-notice" target="_blank">jBox-notic</a></li>
                <li><span>文章打赏功能，仿畅言打赏插件</span>&nbsp;&nbsp;<a href="/myblog/demo/201807/reward" target="_blank">reward</a></li>
                <li><span>举报弹框，仿畅言评论的举报功能</span>&nbsp;&nbsp;<a href="/myblog/demo/201807/jubao" target="_blank">jubao</a></li>

                <!--<li><span>仿畅言评论的印章功能</span>&nbsp;&nbsp;<a href="/myblog/demo/201807/yinzhang" target="_blank">yinzhang</a></li>-->
                <li><span>页面重复ajax请求</span>&nbsp;&nbsp;<a href="/myblog/demo/201807/repeatRequest" target="_blank">repeatRequest</a></li>
                <li><span>checkbox美化</span>&nbsp;&nbsp;<a href="/myblog/demo/201808/beautifulCheckbox" target="_blank">beautifulCheckbox</a></li>
                <li><span>跳转站点提示</span>&nbsp;&nbsp;<a href="/myblog/demo/201808/jumptGrid" target="_blank">跳转站点提示</a></li>
                <!--<li><span>弹出抽奖广告</span>&nbsp;&nbsp;<a href="/myblog/demo/201808/awardAd" target="_blank">弹出抽奖广告</a></li>-->

                <li><span>弹出微信二维码</span>&nbsp;&nbsp;<a href="/myblog/demo/201809/showWeixin" target="_blank">弹出微信二维码</a></li>
                <li><span>SemanticUI</span>&nbsp;&nbsp;<a href="/myblog/demo/201809/SemanticUI" target="_blank">SemanticUI</a></li>
                <li><span>canvas绘制炫酷背景图</span>&nbsp;&nbsp;<a href="/myblog/demo/201809/canvas1" target="_blank">canvas绘制炫酷背景图</a></li>
                <li><span>canvas绘制炫酷背景图2</span>&nbsp;&nbsp;<a href="/myblog/demo/201809/canvas2" target="_blank">canvas绘制炫酷背景图2</a></li>
                <li><span>canvas绘制炫酷背景图2</span>&nbsp;&nbsp;<a href="/myblog/demo/201809/canvas3" target="_blank">canvas绘制炫酷背景图3</a></li>
                <li><span>canvas绘制炫酷背景图4</span>&nbsp;&nbsp;<a href="/myblog/demo/201809/canvas4" target="_blank">canvas绘制炫酷背景图4</a></li>
                <li><span>semanticModal</span>&nbsp;&nbsp;<a href="/myblog/demo/201809/semanticModal" target="_blank">semanticModal</a></li>

                <li><span>onlineEdit</span>&nbsp;&nbsp;<a href="/myblog/demo/201810/onlineEdit" target="_blank">onlineEdit</a></li>

                <li><span>jquery-layout</span>&nbsp;&nbsp;<a href="/myblog/demo/201811/jquery-layout" target="_blank">jquery-layout</a></li>
                <li><span>love</span>&nbsp;&nbsp;<a href="/myblog/demo/201811/love" target="_blank">love</a></li>
              </ul>
            </div>
            <div class="ui tab segment" data-tab="tab_api">
                <form class="ui basic form">
                  <div class="field">
                    <label for="url">URL</label>
                    <input type="url" class="" id="url" value="http://localhost:8099/cms/report/json/doReport" placeholder="输入请求URL">
                  </div>
                  <div class="field">
                    <label for="data">请求结果：</label>
                    <textarea class="" rows="4" id="data"></textarea>
                  </div>
                  <a class="ui primary button" onclick="sendRequest()"  >发送请求</a>
                </form>

                  <style>
                   .apiList .item a{text-decoration: underline;color: #666;}
                   .apiList .item a.on{color: red !important;}
                  </style>
                  <script type="text/javascript">
                    var host=window.location.host;
                    var protocol=window.location.protocol;
                    var baseUrl=protocol+"//"+host;
                    $(document).on("click",".apiList a",function(){
                      $(".apiList a").removeClass("on");
                      var uri = $(this).data("url");
                      $(this).addClass("on");
                      var url=baseUrl+uri;
                      $("#url").val(url);
                    })
                    function sendRequest(){
                      var url=$("#url").val();
                      sys_ajaxPost(url,null,function(data){
                        if(typeof data=='String'){
                          data=data.replaceAll("\r\n","");
                        }
                        var str=JSON.stringify(data);
                        $("#data").val(str);
                      })
                    }
                  </script>
             <h3 style="margin-top: 13px;">SSO接口列表</h3>
              <div class="ui horizontal list apiList" >
                <div class="item">
                  <div class="content">
                    <a data-url="/sso/api/loginState">检测用户登录状态</a>
                  </div>
                </div>

                <div class="item">
                    <a data-url="/sso/api/invalidSession?sso_sessionid=">人工清除session(踢出登录用户)</a>
                </div>

              </div>

              <h3 style="margin-top: 13px;">接口列表</h3>
              <div class="ui horizontal list apiList" >
                <div class="item">
                  <div class="content">
                    <a data-url="/api/loginState">检测用户登录状态</a>
                  </div>
                </div>
                <div class="item">
                  <div class="content">
                    <a data-url="/base/area/json/getPositionProvinceList">获得省份列表</a>
                  </div>
                </div>
                <div class="item">
                  <div class="content">
                    <a data-url="/base/area/json/getPositionCityByProvinceId/{province_id}">获得城市列表</a>
                  </div>
                </div>
                <div class="item">
                  <a data-url="/base/area/json/getPositionCountryByCityId/{city_id}">获得乡镇列表</a>
                </div>
                <div class="item">
                  <a data-url="/base/area/json/getPositionTownByCountryId/{country_id}">获得村庄列表</a>
                </div>
              </div>

            </div>
            <div class="ui tab segment" data-tab="tab_log">
              tab_log
            </div>
<!-- content end -->
          </div>
        </div>
      </div>
    </div>
  </div>

<#include "/cms/myblog/semantic/block/footer.ftl"/>
</div>

<script type="text/javascript">
  $(function(){

    $('#tabContext .menu .item').tab({context: $("#tabContext")});

    $('.sidebar-nav-sub-title').on('click', function() {
      $(this).siblings('.sidebar-nav-sub').slideToggle(80)
              .end()
              .find('.sidebar-nav-sub-ico').toggleClass('sidebar-nav-sub-ico-rotate');
    })
    $('.data a').on('click', function() {
      $(this).parents(".sidebar-nav").find(".sidebar-nav-link a").removeClass("active");
      $(this).addClass("active");

      var id=$(this).attr("id");
      id=id.replace("item_","");
      //alert(id);
      $("#update_content").find("#show_"+id).show().siblings().hide();
    })
  })

</script>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>