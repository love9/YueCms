
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>
<style>

</style>
<body style="" class="pushable">
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher" style="background:url(/resources/myblog/assets/i/f18.jpg) no-repeat fixed ; background-size:100% 100%;">
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->
    <div class="ui basic segment" id="mainScreen">
        <div class="ui container grid">
                <div class="sixteen wide mobile five wide tablet five wide computer column">
                    <div class="ui raised segment blog-sidebar-widget">
                        <div class="header_icon ">
                            <dl>
                                <dd>

                    <#if author_info.headerPath?? &&author_info.headerPath!="">
                                    <img src="${author_info.headerPath!}">
                    <#else>
                                    <img src="/resources/images/headicons/girl-1.png">
                    </#if>
                                </dd>
                            </dl>
                            <dl>
                                <dd> <h3><a href="" target="_blank" id="uid">${author_info.name!}</a></h3></dd>
                            </dl>
                        </div>
                        <div class="inf_number_box ">
                            <dl>
                                <dt>原创</dt>
                                <dd>${author_info.num_yuanchuang!}</dd>
                            </dl>
                            <dl>
                                <dt>粉丝</dt>
                                <dd id="fan">${author_info.num_fans!}</dd>
                            </dl>
                            <dl>
                                <dt>喜欢</dt>
                                <dd>${author_info.num_like!}</dd>
                            </dl>
                        </div>

                        <div class="ui divider"></div>
                        <div class="ui container center aligned">
                            <button class="ui twitter button">
                                <i class="plus icon"></i>&nbsp;加关注
                            </button>
                            <button class="ui google plus button" onclick="showSendMessage()">
                                <i class="wechat icon"></i>&nbsp;发私信
                            </button>
                        </div>
                        <div class="ui divider"></div>

                        <div class="ui list" style="margin: 5px 25px;">
                            <div class="item">今日访问：${author_info.visit_today!}</div>
                            <div class="item">昨日访问：${author_info.visit_yesterday!}</div>
                            <div class="item">本周访问：${author_info.visit_week!}</div>
                            <div class="item">本月访问：${author_info.visit_month!}</div>
                            <div class="item">所有访问：${author_info.visit_total!}</div>
                            <div class="item">加入时间：${author_info.join_time!}</div>
                            <div class="item">最近登录：${author_info.last_login_time!}</div>
                        </div>
                    </div>


                    <#if personal_categorys?? && (personal_categorys?size>0)>

                    <div class="ui raised segment user-hotArticle sort">
                        <h3 class="left aligned">文章分类</h3>
                        <!--高度为单条记录的高度乘以想显示的个数   （36*2）-->
                        <ul id="personal_categorys" style="height: 72px; overflow-y: hidden;margin: 5px 25px;">
                            <#list personal_categorys as ca>
                                <li class="clearfix">
                                    <a href="/myblog/personalBlog/category/${ca.id!}" style="float: left;" target="_blank">${ca.name!}</a>
                                    <div style="float: right;"><span>${velocityCount!}篇</span></div>
                                </li>
                            </#list>
                        </ul>
                        <#if personal_categorys?? && (personal_categorys?size>2)>
                            <div class="unfold-btn">
                                <span>展开</span>&nbsp;<i class="angle down icon" style="line-height: 0.8em;"></i>
                            </div>
                        </#if>
                    </div>
                    </#if>

                <#if article_archives?? && (article_archives?size>2)>

                    <div class="ui raised segment user-hotArticle sort">
                        <h3 class="left aligned">文章存档</h3>
                        <!--高度为单条记录的高度乘以想显示的个数   （36*2）-->
                        <ul id="" style="height: 72px; overflow-y: hidden;margin: 5px 25px;">
                            <#list article_archives as item >
                                <li class="clearfix">
                                    <a href="/myblog/${userName!}/articles/month/$item.month" style="float: left;"
                                       target="_blank">${item.name!}</a>

                                    <div style="float: right;"><span>0篇</span></div>
                                </li>
                            </#list>
                        </ul>
                        <div class="unfold-btn" style="display: none;">
                            <span>展开</span>&nbsp;<i class="angle down icon" style="line-height: 0.8em;"></i>
                        </div>
                    </div>
                </#if>

                    <div class="ui raised segment user-hotArticle">
                        <h3 class="left aligned">么么哒</h3>
                        <!--高度为单条记录的高度乘以想显示的个数   （36*2）-->
                        <ul id="" style="margin: 5px 25px;">
                            <li class="clearfix">
                                <a href="" style="float: left;" target="_blank">每个人都有一个死角， 自己走不出来，别人也闯不进去。</a>
                            </li>
                            <li class="clearfix">
                                <a href="" style="float: left;" target="_blank">我把最深沉的秘密放在那里。</a>
                            </li>
                            <li class="clearfix">
                                <a href="" style="float: left;" target="_blank">你不懂我，我不怪你。</a>
                            </li>
                            <li class="clearfix">
                                <a href="" style="float: left;" target="_blank">每个人都有一道伤口， 或深或浅，盖上布，以为不存在。</a>
                            </li>
                        </ul>
                        <div class="unfold-btn" style="display: none;">
                            <span>展开</span>&nbsp;<i class="angle down icon" style="line-height: 0.8em;"></i>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {

                            if ($(".user-hotArticle ul").height() > 36 * 2) {
                                $(".user-hotArticle ul").attr("style", "height:72px;overflow-y:hidden;");
                                $(".unfold-btn").show();
                            }

                            $(".unfold-btn").click(function () {
                                $(this).parents().find("ul").attr("style", "");
                                $(this).hide();
                            });
                        })
                    </script>
                </div>
                <!--左侧菜单结束-->
                <!--右侧内容开始-->
                <div class="sixteen wide mobile eleven wide tablet eleven wide computer column" style="background-color: #fff;margin-top: 1rem;">
                    <article class="am-article " style="background-color: #fff;">
                        <div class="am-article-hd">
                            <h1 class="am-article-title">${article.title!}</h1>
                            <p class="am-article-meta article_info" style="margin:0px 0 30px 0;">
                                <span class="author am-fl" style="margin-right: 30px;">${article.author!}</span>

                                <#if article.publishTime?? &&article.publishTime!="">
                                    <span class="date am-fl">${article.publishTime!}</span>
                                <#else>
                                    <span class="date am-fl">${article.createTime!}</span>
                                </#if>

                                <#if article.description??>
                                    <div class="article_desc">
                                        <strong>简介</strong>
                                        ${article.description!}
                                    </div>
                                </#if>
                            </p>
                        </div>
                        <div class="am-article-bd" style="line-height: 1.8;">
                            ${article.content!}
                        </div>
                    </article>
                    <style type="text/css">
                    </style>

                    <div class="readall_box " style="display: none;">
                        <div class="read_more_mask"></div>
                        <a class="ui button orange  unfold-btn" style="top:-38px;position:relative;">阅读全文</a>
                        <script type="text/javascript">
                            $(function () {
                                if ($("article .am-article-bd").height() >= 1200) {
                                    $("article .am-article-bd").attr("style", "height:1200px;overflow-y:hidden;");
                                    $(".readall_box").show();
                                }
                                $(".unfold-btn").click(function () {
                                    $(this).parents().find("article .am-article-bd").attr("style", "");
                                    //$(this).fadeOut();
                                    $(this).parents(".readall_box").hide();
                                });
                            })
                        </script>
                    </div>
<#include "/cms/myblog/semantic/reward/reward.ftl"/>
<!--评论区域 start-->
<#include "/cms/myblog/semantic/comment/comment-area.ftl"/>
<!--评论区域 end-->
                </div>
        </div>
    </div>

</div>
<!-- content end -->
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>