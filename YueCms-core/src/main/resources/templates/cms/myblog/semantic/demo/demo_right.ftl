<div class="aboutme">
    <h2 class="hometitle">关于我</h2>
    <div class="avatar"> <img src="/resources/images/portraits/5.jpg"> </div>
    <div class="ab_con">
        <p>网名：MakBos | 武继跃</p>
        <p>职业：Java工程师、Web前端、网页设计 </p>
        <p>个人微信：MakBos</p>
        <p>邮箱：747506908@qq.com</p>
    </div>
</div>
<div class="weixin">
    <h2 class="hometitle">微信关注</h2>
    <ul>
        <img class="ui medium rounded image" src="/resources/myblog/images/my_wx.png">
    </ul>
</div>