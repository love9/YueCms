
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/lib/fontawesome/css/font-awesome.min.css">
  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <link rel="stylesheet" href="/resources/lib/jquery-layout/jquery.layout-latest.css">
    <script src="/resources/lib/jquery-layout/jquery.layout-latest.js"></script>
</head>
<body>

<div class="left">
    left
</div>
<div class="main">
    main
</div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('body').layout({
                //applyDemoStyles:true,//是否采用默认样式
                west__paneSelector: ".left",
                center__paneSelector: ".main"
            });
        });
    </script>
</body>
</html>