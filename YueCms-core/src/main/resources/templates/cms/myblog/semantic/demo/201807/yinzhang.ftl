
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>仿畅言评论印章</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="icon" type="image/png" href="/resources/myblog/assets/i/favicon.png">
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="/resources/myblog/assets/i/app-icon72x72@2x.png">

    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>

    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>
<body id="demo">
<#include "/myblog/nav.ftl"/>
<!-- content srart -->

<article>
  <style>
    .clear-g {
      zoom: 1;
    }
    .yinzhang {
      position:relative;overflow:hidden;
      width: 258px;
      height: 58px;
      border: 1px solid #ccd4d9;
     /* position: absolute;
      top: -160px;
      right: 0;*/
      z-index: 12;
      border-radius: 4px;
      background: #fff;
    }
    .prop-wrappar {
      text-align: center;
      overflow: hidden;
      position: relative;
      padding-bottom: 0px;
    }
    .item-content-wrap {
      overflow: hidden;
      margin: 0 auto;
      width: 228px;
    }
    .prop-item {
      border:1px solid #fff;
      width: 56px;
      /*height: 90px;*/
      height: 56px;
      float: left;
      position: relative;
      overflow: visible;
      cursor: pointer;
      box-sizing: border-box;
    }
    .prop-item:hover{border:1px solid #dadada;}
    .prop-item .prop-ico {
      width: 40px;
      height: 40px;
      display: block;
      margin: 8px auto 6px;
    }
   .prop-item span.prop-ico-geili {

      background: url(/resources/myblog/images/icon/icon_window_geili_0.png) no-repeat;
    }
   .prop-item span.prop-ico-pei {
      background: url(/resources/myblog/images/icon/icon_window_pei_0.png) no-repeat;
    }
    .prop-item span.prop-ico-dou {
      background: url(/resources/myblog/images/icon/icon_window_dou_0.png) no-repeat;
    }
    .prop-item span.prop-ico-penzi {
      background: url(/resources/myblog/images/icon/icon_window_penzi_0.png) no-repeat;
    }
    .prop-item:hover .prop-ico-geili {
      background: url(/resources/myblog/images/icon/icon_window_geili_1.png) no-repeat;transform:scale(1.1,1.1);
      background-position: 3px 3px;
    }
    .prop-item:hover span.prop-ico-pei {
      background: url(/resources/myblog/images/icon/icon_window_pei_1.png) no-repeat;transform:scale(1.05,1.05);
    }
    .prop-item:hover  span.prop-ico-dou {
      background: url(/resources/myblog/images/icon/icon_window_dou_1.png) no-repeat;transform:scale(1.05,1.05);
    }
    .prop-item:hover  span.prop-ico-penzi {
      background: url(/resources/myblog/images/icon/icon_window_penzi_1.png) no-repeat;transform:scale(1.05,1.05);
    }
 </style>
  <div class="ab_box">
    <h1 class="t_nav">
      <span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span>
      <a href="/myblog" class="n1">网站首页</a>
      <a href="/myblog/updateLog" class="n2">更新日志</a>
      <a href="javascript:;" class="n3">评论印章</a>
    </h1>
    <div class="leftbox">
      <div class="body-wrapper">
                <div class="am-form-group am-text-center">
                  <a class="am-btn am-btn-secondary am-round" onclick="yinzhang()" style="line-height: 0.8;" onclick="">点我弹框</a>
                </div>
                  <div class="yinzhang" >
                    <div class="clear-g prop-wrappar">
                        <div class="item-content-wrap">
                            <div data-type="geili" class="prop-item">
                              <span class="prop-ico prop-ico-oppose prop-ico-geili"></span>
                            </div>
                          <div data-type="pei" class="prop-item">
                            <span class="prop-ico prop-ico-oppose prop-ico-pei"  ></span>
                          </div>
                          <div data-type="dou" class="prop-item">
                            <span class="prop-ico prop-ico-oppose prop-ico-dou"  ></span>
                          </div>
                          <div data-type="penzi" class="prop-item">
                            <span class="prop-ico prop-ico-oppose prop-ico-penzi"  ></span>
                          </div>
                        </div>
                    </div>
                  <!--  <div style="text-align: center;display: inline-block;width: 100%;">  <a class="am-btn am-btn-secondary am-round"  style="line-height: 0.8;" onclick="jubaoSubmit()">提交</a></div>-->
                  </div>

      </div>
</div>
      <script type="text/javascript">
        $(document).on('click', '.yinzhang .prop-item', function() {
            var type=$(this).data("type");
          alert("您点击了：" + type);
        });

        function yinzhang(){

          layer.open({
            type: 1,
            title:false,
            skin: '123', //加上边框
            area: ['258px','58px'],
            closeBtn:false,
            shadeClose: true,
            content: $(".yinzhang")
          });
        }

      </script>
    </div>
    <div class="rightbox">
      <div class="aboutme">
        <h2 class="hometitle">关于我</h2>
        <div class="avatar"> <img src="/resources/images/portraits/5.jpg"> </div>
        <div class="ab_con">
          <p>网名：MakBos | 武继跃</p>
          <p>职业：Java工程师、Web前端、网页设计 </p>
          <p>个人微信：MakBos</p>
          <p>邮箱：747506908@qq.com</p>
        </div>
      </div>
      <div class="weixin">
        <h2 class="hometitle">微信关注</h2>
        <ul>
          <img src="/resources/myblog/images/my_wx.png">
        </ul>
      </div>
    </div>
  </div>
</article>

<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>

</body>
</html>