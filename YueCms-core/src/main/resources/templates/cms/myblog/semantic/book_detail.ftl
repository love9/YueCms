
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>

<body  style="background: #e9ecf3;">

<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>

<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
    <div class="ui basic segment" id="mainScreen" >
        <div class="ui container bg-white">
            <div class="ui grid" >
                <div class="row" >
                    <div class="sixteen wide tablet eleven wide computer column">
                        <div class="ui items">
                            <div class="item">
                                <div class="big image">
                                    <img src="${book.sheet_url!}">
                                </div>
                                <div class="middle aligned content">
                                    <h4 class="header">${book.bookname!}</h4>
                                    <div class="meta" style="margin: 1.2rem auto;">
                                        <span>作者：&nbsp;${book.author!}</span>
                                        <span class="right floated time">添加时间:&nbsp;&nbsp;
                                        ${book.createTime?datetime("yyyy-MM-dd HH:mm:ss")}
                                        </span>
                                    </div>
                                    <div class="description">
                                        <p>${book.description!}</p>
                                    </div>
                                    <div class="extra">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="ui divider"></div>
                         <#if book.chapterDetails?? &&  (book.chapterDetails?size>0)>
                            <div class="ui two columns grid chapterDetails">
                                <#list book.chapterDetails as item>
                                    <div class="column">
                                        <a style="text-align: left;" class="fluid ui button primary"  href="/myblog/chapterDetail/${item.id!}">${item.sort!} . ${item.title!}</a>
                                    </div>
                                </#list>
                            </div>
                        <#else>
                        <p style="text-align: center">暂无内容!</p>
                        </#if>
                        <div class="ui divider"></div>
<#include "/cms/myblog/semantic/comment/comment-area-book.ftl"/>
                    </div>

                    <!--右侧-->
                    <div class="sixteen wide tablet five wide computer column"  style="background:#fff;border-left: 1px dashed rgba(34,36,38,.1);">
                       <#-- <div class="ui divided items">
                            <div class="item">
                                <div class="ui tiny image">
                                    <img src="https://semantic-ui.qyears.com/images/wireframe/image.png">
                                </div>
                                <div class="middle aligned content">
                                    内容 A
                                </div>
                            </div>
                            <div class="item">
                                <div class="ui tiny image">
                                    <img src="https://semantic-ui.qyears.com/images/wireframe/image.png">
                                </div>
                                <div class="middle aligned content">
                                    内容 B
                                </div>
                            </div>
                            <div class="item">
                                <div class="ui tiny image">
                                    <img src="https://semantic-ui.qyears.com/images/wireframe/image.png">
                                </div>
                                <div class="middle aligned content">
                                    <div class="description">
                                       11111111
                                    </div>
                                    <div class="extra">
                                        <div class="ui tiny right floated orange basic button">
                                            Action
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->


                    </div>
                </div>

            </div>
        </div>
    </div>



<!-- content end -->

<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
</body>
</html>