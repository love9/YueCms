
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>跳转网页弹框</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

  <link rel="stylesheet" href="/resources/css/channelSite.css">
  <script type="text/javascript" src="/resources/js/common/channelSite.js"></script>
</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer  sixteen wide mobile column  ">

                    <div class="body-wrapper">
                        <p>在离开本网站跳转到另一网站，如果加个弹出层提示：即将离开本站，跳转至xxx,并设有倒计时，自动跳转。这样的设置更加人性化!</p>
                        <div class="channelSiteInfo">
                            <a href="javascript:;" data-href="http://36kr.com/p/5139746.html?form=duoguyu.com" title="36 氪" data-site="36 氪" data-site-name="36 氪" id="channelSite" class="iconChannelSite jumpHref kr36"></a>
                            <a href="javascript:;" data-href="https://www.pingwest.com/a/174106?form=duoguyu.com" title="PingWest 品玩" data-site="PingWest 品玩" data-site-name="PingWest 品玩" id="channelSite" class="iconChannelSite jumpHref pinwan"></a>
                            <a href="javascript:;" data-href="https://www.toutiao.com/a6590920649944859150/?form=duoguyu.com" title="今日头条" data-site="今日头条" data-site-name="今日头条" id="channelSite" class="iconChannelSite jumpHref toutiao"></a>
                            <a href="javascript:;" data-href="https://m.huanqiu.com/r/MV8wXzEyMjQ0NjgwXzE4NDlfMTUyODg1NDc4MA==?form=duoguyu.com" title="简书" data-site="简书" data-site-name="简书" id="channelSite" class="iconChannelSite jumpHref jianshu"></a>
                            <a href="javascript:;" data-href="https://movie.douban.com/review/7520681/?form=duoguyu.com" title="豆瓣" data-site="豆瓣" data-site-name="豆瓣" id="channelSite" class="iconChannelSite jumpHref douban"></a>
                            <a href="javascript:;" data-href="https://www.guokr.com/article/442948/?form=duoguyu.com" title="果壳" data-site="果壳" data-site-name="果壳" id="channelSite" class="iconChannelSite jumpHref guoke"></a>
                        </div>
                    </div>
                    <script type="text/javascript">

                    </script>

              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>