
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>弹出抽奖广告</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/resources/myblog/assets/i/favicon.png">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="/resources/myblog/assets/i/app-icon72x72@2x.png">

    <link rel="stylesheet" href="/resources/myblog/assets/css/amazeui.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/app.css">

    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

    <script type="text/javascript" src="/resources/myblog/amazeui/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/myblog/amazeui/amazeui.min.js"></script>
    <script type="text/javascript" src="/resources/lib/template.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>
<body id="demo">
<#include "/myblog/nav.ftl"/>
<!-- content srart -->

<article>
    <style>
        @-webkit-keyframes tada {
            5% {
                -webkit-transform: scale3d(1, 1, 1) rotate3d(0, 0, 1, -10deg);
                transform: scale3d(1, 1, 1) rotate3d(0, 0, 1, -10deg);
            }
            6%,
            8%,
            10%,
            12% {
                -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 10deg);
                transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 10deg);
            }

            7%,
            9%,
            11% {
                -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -10deg);
                transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -10deg);
            }

            13% {
                -webkit-transform: scale3d(1, 1, 1);
                transform: scale3d(1, 1, 1);
            }
        }

        @keyframes tada {
            5% {
                -webkit-transform: scale3d(1, 1, 1) rotate3d(0, 0, 1, -10deg);
                transform: scale3d(1, 1, 1) rotate3d(0, 0, 1, -10deg);
            }
            6%,
            8%,
            10%,
            12% {
                -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 10deg);
                transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 10deg);
            }

            7%,
            9%,
            11% {
                -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -10deg);
                transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -10deg);
            }

            13% {
                -webkit-transform: scale3d(1, 1, 1);
                transform: scale3d(1, 1, 1);
            }

        }
        @-webkit-keyframes skip {
            from {
                -webkit-transform: scale(0.88);
                transform: scale(0.88)
            }
            100% {
                -webkit-transform: scale(1.07);
                transform: scale(1.07)
            }
        }

        @-moz-keyframes skip {
            from {
                -moz-transform: scale(0.88);
                transform: scale(0.88)
            }
            100% {
                -moz-transform: scale(1.07);
                transform: scale(1.07)
            }
        }

        @keyframes skip {
            from {
                -webkit-transform: scale(0.88);
                -moz-transform: scale(0.88);
                transform: scale(0.88)
            }
            100% {
                -webkit-transform: scale(1.07);
                -moz-transform: scale(1.07);
                transform: scale(1.07)
            }
        }
        @keyframes circle {
            from {
                transform: rotate(0);
            }
            to {
                transform: rotate(360deg);
            }
        }

        @-webkit-keyframes circle {
            from {
                -webkit-transform: rotate(0);
            }
            to {
                -webkit-transform: rotate(360deg);
            }
        }

        .QIHOO__INTERACTIVE_PLUGIN1535350341302 info-div, .QIHOO__INTERACTIVE_PLUGIN1535350341302info-div {
            display: block;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize {
            position: fixed;
            z-index: 10000;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            padding: 0 10px;
            box-sizing: border-box;
            overflow: hidden;
            background-color: rgba(0, 0, 0, .65);
            display: none;
        }

        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .card-sunshine {
            width: 500px;
            height: 500px;
            background: url(/resources/myblog/demo/bg_sun.png) no-repeat center;
            background-size: 100% 100%;
            position: absolute;
            top: 30%;
            left: 50%;
            margin-top: -250px;
            margin-left: -250px;
            animation: circle 10s linear infinite;
            -webkit-animation: circle 18s linear infinite;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .m-box {
            width: 500px;
            height: 500px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -250px;
            margin-left: -250px;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog {
            width: 500px;
            height: 450px;
            padding-top: 10px;
            position: absolute;
            top: 30%;
            left: 50%;
            z-index: 2;
            overflow: visible;
            margin-left: -250px;
            margin-top: -225px;
            -webkit-animation: showModal .5s ease-in-out;
            animation: showModal .5s ease-in-out;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .card-bg {
            width: 450px;
            height: 370px;
            background-image: url(/resources/myblog/demo/bg_ward.png);
            background-size: 100% 85%;
            background-position: center top;
            background-repeat: no-repeat;
            overflow: hidden;
            margin: auto;
            padding-top: 0;
            transform: translate(0, 100px);
            -webkit-transform: translate(0, 100px);
            animation: move 0.5s linear 0.4s forwards;
            -webkit-animation: move 0.5s linear 0.4s forwards;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .card-bg .imgAD {
            width: 346px;
            height: 173px;
            display: block;
            margin: 128px auto 0;
            background-repeat: no-repeat;
            background-size: 100% auto;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .red-bg {
            width: 384px;
            height: 195px;
            background-position: center;
            background-repeat: no-repeat;
            position: relative;
            margin: -3.89rem auto 0;
            z-index: -1;
            animation: scale 0.2s linear forwards;
            -webkit-animation: scale 0.2s linear forwards;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .detail {
            width: 384px;
            height: 190px;
            background-image: url(/resources/myblog/demo/bg_detail.png);
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            position: relative;
            margin: -192px auto 0;
            animation: scale 0.2s linear forwards;
            -webkit-animation: scale 0.2s linear forwards;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .detail {
            width: 384px;
            height: 190px;
            background-image: url(/resources/myblog/demo/bg_detail.png);
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            position: relative;
            margin: -192px auto 0;
            animation: scale 0.2s linear forwards;
            -webkit-animation: scale 0.2s linear forwards;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .ownPrize {
            width: 350px;
            text-align: center;
            position: absolute;
            top: 68px;
            color: #fff;
            font-size: 16px;
            line-height: 25px;
            margin: 0 15px;
            font-weight: 700;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .detail .topic {
            width: 350px;
            height: 40px;
            border-radius: 4px;
            text-align: center;
            position: absolute;
            top: 15px;
            color: #fff;
            font-size: 22px;
            font-weight: 500;
            line-height: 40px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            margin: 0 15px;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .detail .goto {
            width: 320px;
            height: 50px;
            background-image: url(/resources/myblog/demo/yeallow_bar.png);
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            position: absolute;
            left: 50%;
            margin-left: -160px;
            bottom: 20px;
            text-align: center;
            line-height: 45px;
            color: #ef4511;
            font-size: 18px;
            font-weight: 500;
            text-shadow: 1px 1px 2px #fff;
            -webkit-text-shadow: 1px 1px 2px #fff;
            cursor: pointer;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .showPrize-dialog .detail .goto:after {
            content: '';
            width: 20px;
            height: 50px;
            position: absolute;
            left: 0;
            top: 0;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .close-btn {
            display: block;
            text-align: center;
            position: absolute;
            top: 80px;
            right: -18px;
            font-size: 16px;
            z-index: 1000;
            cursor: pointer;
        }


        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .closetc {
            display: block;
            width: 2.5rem;
            height: 2.5rem;
            text-align: center;
            line-height: 2.5rem;
            font-size: 2.5rem;
            position: absolute;
            top: 20px;
            right: 20px;
            background: url(/resources/myblog/demo/btn_close_big.png) no-repeat center;
            background-size: 100% 100%;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .prizeWrap {
            width: 700px;
            height: 700px;
            position: absolute;
            left: 50%;
            margin-left: -350px;
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .goto, .QIHOO__INTERACTIVE_PLUGIN1535350341302.popShowPrize .again {
            width: 12.2rem;
            height: 2rem;
            margin: 0 auto;
            line-height: 2rem;
            text-align: center;
            color: #FFFFFF;
            font-size: 0.8rem;
            border-radius: 8px;
            margin-bottom: 0.5rem;
            -webkit-animation: skip .8s linear infinite alternate;
            -moz-animation: skip .8s linear infinite alternate;
            animation: skip .8s linear infinite alternate;
            position: relative;
        }
        /*立即抽奖按钮*/
        #_360_interactive > * {
           /* margin-left: -8px;*/
        }
        .QIHOO__INTERACTIVE_PLUGIN1535350341302-thumbnail {
            display: block;
            cursor: pointer;
            width: 60px;
            height: 60px;
            background: url(/resources/myblog/demo/btn_ljcj.png) no-repeat center;
            background-size: 100% 100%;
            -webkit-animation: tada 5s linear infinite backwards;
            -moz-animation: tada 5s linear infinite backwards;
            animation: tada 5s linear infinite backwards;
            }
        /*抽奖界面*/
        #QIHOO__INTERACTIVE_PLUGIN1535350341302-gameBg {
            background-image: url(https://p5.ssl.qhimg.com/t01756f23395b060050.png);
            background-repeat: no-repeat;
            background-size: 100%;
        }
        .main, .page, .page-group {
            position: relative;
            font-size: 19.2533px;
        }
        #myAwards {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 10001;
            background-image: url(//p4.ssl.qhimg.com/t0191fa1512aeb27291.png);
            background-size: 100%;
            height: 2.5rem;
            width: 2.5rem;
        }
        .page-group.page-current, .page.page-current {
            color: #333;
            font-size: .6rem;
            font-family: "Microsoft YaHei";
        }
        .page-main, body {
            width: 100%;
        }
        .main {
            height: 22.65rem;
            overflow: hidden;
            margin: 0 auto -2px;
            background-image: url(https://p4.ssl.qhimg.com/t0131581d5a43510175.png);
            background-size: 17rem 14.98rem;
            background-position: top center;
        }
        .wheel {
            background-image: url(//p1.ssl.qhimg.com/t01899c24e489f625dd.png);
        }
        .wheel {
            width: 15rem;
            height: 14.98rem;
            margin: 0 auto;
            background-size: 100% 100%;
        }
        .wheel, .wheel .turnplate {
            display: block;
            position: relative;
        }
        .wheel .turnplate {
             width: 100%;
             height: 100%;
         }
        .wheel .turnplateborder.default {
            background-image: url(//p1.ssl.qhimg.com/t0136b42f6390277a68.png);
        }
        .wheel .turnplateborder.change {
            background-image: url(//p4.ssl.qhimg.com/t01cc397b36cfaa20d2.png);
        }
        .wheel .turnplateborder.default {
            background-repeat: no-repeat;
            background-position: center;
            animation: blind .5s linear infinite normal;
            -webkit-animation: blind .5s linear infinite normal;
        }
        .wheel .turnplateborder {
            width: 15rem;
            height: 14.98rem;
            background-size: contain;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
            box-sizing: border-box;
        }
        .core {
            background-image: url(//p4.ssl.qhimg.com/t014d132ee0184b89f4.png);
        }

        .core {
            width: 13.7rem;
            height: 13.7rem;
            border-radius: 50%;
            overflow: hidden;
            bottom: 0;
            left: 0;
            margin: auto;
            z-index: 1;
        }

        .activity, .core {
            top: 0;
            background-repeat: no-repeat;
            right: 0;
        }

        .core, .pop-circle, .pop-hand {
            background-size: 100% 100%;
            position: absolute;
        }
        .core-box {
            width: 100%;
            height: 100%;
        }
        .core-box .prize:nth-child(1) {
            -webkit-transform: rotate(30deg) skewY(30deg);
            transform: rotate(30deg) skewY(30deg);
            -moz-transform: rotate(30deg) skewY(30deg);
        }

        .core-box .prize {
            position: absolute;
            width: 50%;
            height: 50%;
            -webkit-transform-origin: right bottom;
            transform-origin: right bottom;
            -moz-transform-origin: right bottom;
        }
        .prize .prize-dialog {
            position: absolute;
            bottom: .3rem;
            left: 2.5rem;
            width: 4.4rem;
            height: 4.4rem;
            -webkit-transform: skewY(-30deg) rotate(-30deg);
            transform: skewY(-30deg) rotate(-30deg);
        }
        .m-prize-btn, .moreAct, .more_header, .needtime i, .prize .prize-dialog, .rule header {
            text-align: center;
        }

        .prize-dialog, .prize-dialog>p {
            font-weight: 500;
        }
        .prize-dialog>p {
            color: #63a0ab;
        }

        .prize-dialog p {
            padding-top: 0.1rem;
        }

        .prize-dialog>p {
            font-size: .8rem;
        }
        .prize-dialog, .prize-dialog>p {
            font-weight: 500;
        }
        .prize-dialog img {
            width: 2.5rem;
            height: 2.5rem;
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .start {
            background-image: url(//p0.ssl.qhimg.com/t017336a164d8c142ba.png);
        }
        .disable, .start {
            width: 4rem;
            height: 5.5rem;
            left: 50%;
            top: 50%;
            transform: translate(-50%,-50%);
            -webkit-transform: translate(-50%,-50%);
            z-index: 999;
            background-size: 100%;
        }
        .hand {
            width: 4rem;
            height: 5.5rem;
        }
        .needtime {
            background-image: url(//p5.ssl.qhimg.com/t01c9b7c9b7bbd87445.png);
        }
        .needtime {
            width: 7.5rem;
            height: 1.2rem;
            margin: .25rem auto 0;
            position: relative;
            background-size: 100%;
        }
        .needtime i {
            color: #fff;
            font-weight: 700;
        }

        .needtime i {
            width: 100%;
            line-height: 100%;
            font-size: .76rem;
            top: 50%;
            transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
        }
    </style>
    <div class="ab_box">
        <h1 class="t_nav">
            <span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span>
            <a href="/myblog" class="n1">网站首页</a>
            <a href="/myblog/updateLog" class="n2">更新日志</a>
            <a href="javascript:;" class="n3">弹出抽奖广告</a>
        </h1>

        <div class="leftbox">
            <div class="body-wrapper">
                <!--立即抽奖按钮-->
                <div id="_360_interactive"><info-div style="width:60px;height:60px" id="QIHOO__INTERACTIVE_PLUGIN1535350341302" class="QIHOO__INTERACTIVE_PLUGIN1535350341302-thumbnail"></info-div></div>
                <!--抽奖界面-->
                <info-div id="QIHOO__INTERACTIVE_PLUGIN1535350341302-gameBg" style="width: 430px; height: 430px; visibility: visible; position: fixed; opacity: 1; left: 10px; top: 100px;display: none;">


                    <div class="page-loading" style="opacity: 0; display: none;"><div><div class="lds-hourglass">

                    </div><p class="loading-tips">福利来袭...</p></div></div>
                    <div class="page-group" style="display: block; opacity: 1;    top: 143px;    display: block;
    margin: 92px auto 0;">
                        <div id="myAwards"></div>
                        <div class="page page-current page-index">
                            <div class="page-main">
                                <!--广告图-->
                                <div class="main">

                                    <div class="wheel">
                                        <div class="wheel-banner"></div>
                                        <div class="turnplate" style="transform: rotate3d(0, 0, 1, 300deg);">
                                            <div class="turnplateborder default"></div>
                                            <div class="turnplateborder change"></div>
                                            <div class="core">
                                                <div class="core-box" id="core-box">
                                                    <div class="prize">
                                                        <div class="prize-dialog">
                                                            <p>8</p>
                                                            <img src="//p1.ssl.qhimg.com/t011f95c8e08528ebd0.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="prize">
                                                        <div class="prize-dialog">
                                                            <p>幸运福袋</p>
                                                            <img src="//p0.ssl.qhimg.com/t013145bb17336827c1.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="prize">
                                                        <div class="prize-dialog">
                                                            <p>谢谢参与</p>
                                                            <img src="//p0.ssl.qhimg.com/t01173dd469b2be30a9.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="prize">
                                                        <div class="prize-dialog">
                                                            <p>888</p>
                                                            <img src="//p1.ssl.qhimg.com/t011f95c8e08528ebd0.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="prize">
                                                        <div class="prize-dialog">
                                                            <p>188</p>
                                                            <img src="//p1.ssl.qhimg.com/t011f95c8e08528ebd0.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="prize">
                                                        <div class="prize-dialog">
                                                            <p>88</p>
                                                            <img src="//p1.ssl.qhimg.com/t011f95c8e08528ebd0.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="start">
                                            <div class="hand" style="display: none;">
                                                <img src="//p0.ssl.qhimg.com/t016db5c68c22cf7558.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="needtime">
                                        <i>今日免费：5 次</i>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <script>

                        $('.wheel').on('mousedown',function(e){
                            if($(e.target).hasClass('start')){
                                e.preventDefault();
                            }else{
                                $('.start').trigger('mousedown');
                            }
                        });
                        $('.wheel').on('mouseup',function(e){
                            if($(e.target).hasClass('start')){
                                e.preventDefault();
                            }else{
                                $('.start').trigger('mouseup');
                            }
                        });
                        $('.wheel').on('click',function(e){
                            if($(e.target).hasClass('start')){
                                e.preventDefault();
                            }else{
                                $('.start').trigger('click');
                            }
                        });

                    </script>

                    <script>
                        function sendLog(){
                            var url = 'https://track.mediav.com/t?type=23&cus=289706_2344700_22610775_5';

                            if(url == 'NaN' || !url){return false;}
                            var img = new Image()
                            img.onload = img.onerror = function(){
                                img = null
                            }
                            img.src = url
                        }

                        try{
                            window.localStorage.getItem('myAward');
                        }catch(e){
                            sendLog();
                        }
                    </script>

                </info-div>

                <!--中奖结果-->
                <info-div class="QIHOO__INTERACTIVE_PLUGIN1535350341302 popShowPrize" id="QIHOO__INTERACTIVE_PLUGIN1535350341302-dialog" style="display: block;">
                    <info-div class="card-sunshine"></info-div>
                    <info-div class="m-box"></info-div>
                    <info-div class="showPrize-dialog modal-body " id="QIHOO__INTERACTIVE_PLUGIN1535350341302-modal-body">
                        <info-div class="card-bg" style="background-image: url(/resources/myblog/demo/bg_ward.png);">
                            <info-div class="imgAD" style="background-image: url(/resources/myblog/demo/imgAd.gif);"></info-div>
                            <info-div class="" style="position: absolute;display: none;height: 12px;width: 22px;bottom:70px;right:55px;background: url(/resources/myblog/demo/word_ad.png) no-repeat top left;"></info-div>
                        </info-div>
                        <info-div class="red-bg"></info-div>
                        <info-div class="detail">
                            <info-div class="ownPrize">第一批赚大钱的机会</info-div>
                            <info-div class="topic">恭喜你获得了 </info-div>
                            <info-div class="goto" id="QIHOO__INTERACTIVE_PLUGIN1535350341302-goto">点击赚钱</info-div>
                        </info-div>
                        <info-div class="close-btn closetc iconfont" id="QIHOO__INTERACTIVE_PLUGIN1535350341302-close"></info-div>
                    </info-div>
                    <info-div class="prizeWrap">
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                        <info-div class="ribbon"></info-div>
                    </info-div>
                </info-div>


            </div>
        </div>
        <script type="text/javascript">

        </script>

        <div class="rightbox">
            <div class="aboutme">
                <h2 class="hometitle">关于我</h2>

                <div class="avatar"><img src="/resources/images/portraits/5.jpg"></div>
                <div class="ab_con">
                    <p>网名：MakBos | 武继跃</p>

                    <p>职业：Java工程师、Web前端、网页设计 </p>

                    <p>个人微信：MakBos</p>

                    <p>邮箱：747506908@qq.com</p>
                </div>
            </div>
            <div class="weixin">
                <h2 class="hometitle">微信关注</h2>
                <ul>
                    <img src="/resources/myblog/images/my_wx.png">
                </ul>
            </div>
        </div>
    </div>
</article>

<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>

</body>
</html>