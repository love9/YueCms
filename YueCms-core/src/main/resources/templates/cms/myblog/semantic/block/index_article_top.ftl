<div class="blog-sidebar-widget" style="margin:1rem -1.5rem;">
    <h3 class="blog-sidebar-widget-title"><b>头条</b></h3>
    <div class="blog-sidebar-widget-content" id="">
        <div class="itemGrid">
            <ul>
<#if tops??>
                <#list tops as item >
                    <li>
                        <div class="article_box">

    <#if item.static_url?? && item.static_url!="">
                            <a href="${item.static_url!}" title="${item.title!}" target="_blank">
    <#else>
                            <a href="/myblog/article/detail/${top.id!}" title="${item.title!}" target="_blank">
    </#if>
                                <div class="tophead">
                                    <i class="iconFlag">${item.tags!}</i>
                                    <img class="lazy" width="294" height="160" data-original="${item.cover_image!}" src="${item.cover_image!}" style="display: block;">
                                </div>
                                <article class="h135">
                                    <h3>
                                        ${item.title!}
                                    </h3>
                                    <p class="h40 cf999 mt10">${item.description!}</p>
                                    <p class="mt15" style="text-align: right;"><span class="cf_green f12">阅读全文 &gt;</span></p>
                                </article>
                            </a>
                        </div>
                    </li>
                </#list>
<#else>
                <li>
                    <div class="article_box">
                        <a href="http://www.duoguyu.com/echo/177.html" title="给我5000万，也别想让我生孩子" target="_blank">
                            <div class="tophead hoverImg">
                                <i class="iconFlag">小酒馆</i>
                                <img class="lazy" width="294" height="160" data-original="http://v3cdn.duoguyu.com/oss/uploads/201808/29/180829123952506.jpg" src="http://v3cdn.duoguyu.com/oss/uploads/201808/29/180829123952506.jpg" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    <span class="iconStatus sm mr5">片刻</span>
                                    给我5000万，也别想让我生孩子                	                            </h3>
                                <!--<p class="description cf999 mt15">上周，有几位专家出来提建议：向全民征收&ldquo;生育基金&rdquo;、生了二胎再返还，向&ldquo;丁克...</p>-->
                                <p class="h40 cf999 mt10">上周，有几位专家出来提建议：向全民征收“生育基金”、生了二胎再返还，向“丁克...</p>
                                <p class="mt15"><span class="cf_green f12">阅读全文 &gt;</span></p>
                            </article>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="article_box">
                        <a href="http://www.duoguyu.com/continent/172.html" title="我的男朋友，迟钝到没有求生欲" target="_blank">
                            <div class="tophead hoverImg">
                                <img class="lazy" width="294" height="160" data-original="http://v3cdn.duoguyu.com/oss/uploads/201808/27/180827110227433.jpg" src="http://v3cdn.duoguyu.com/oss/uploads/201808/27/180827110227433.jpg" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    <span class="iconStatus flesh sm mr5">小酒馆</span>
                                    我的男朋友，迟钝到没有求生欲                	                            </h3>
                                <!--<p class="description cf999 mt15">最近看了《幸福三重奏》。有一集，蒋勤勤给陈建斌做了碗荞麦面，陈建斌囫囵下肚，...</p>-->
                                <p class="h40 cf999 mt10">最近看了《幸福三重奏》。有一集，蒋勤勤给陈建斌做了碗荞麦面，陈建斌囫囵下肚，...</p>
                                <p class="mt15 am-text-right"><span class="cf_green f12">阅读全文 &gt;</span></p>
                            </article>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="article_box">
                        <a href="http://www.duoguyu.com/echo/171.html" title="自如是如何赚钱的" target="_blank">
                            <div class="tophead hoverImg">
                                <img class="lazy" width="294" height="160" data-original="http://v3cdn.duoguyu.com/oss/uploads/201808/27/180827104344807.jpg" src="http://v3cdn.duoguyu.com/oss/uploads/201808/27/180827104344807.jpg" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    <span class="iconStatus sm mr5">片刻</span>
                                    自如是如何赚钱的                	                            </h3>
                                <!--<p class="description cf999 mt15">年初，自如获得40亿元的A轮融资。在长租公寓行业，这是规模最大的一笔融资，被称为...</p>-->
                                <p class="h40 cf999 mt10">年初，自如获得40亿元的A轮融资。在长租公寓行业，这是规模最大的一笔融资，被称为...</p>
                                <p class="mt15 am-text-right"><span class="cf_green f12">阅读全文 &gt;</span></p>
                            </article>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="article_box">
                        <a href="http://www.duoguyu.com/internet/170.html" title="滴滴的救赎" target="_blank">
                            <div class="tophead hoverImg">
                                <img class="lazy" width="294" height="160" data-original="http://v3cdn.duoguyu.com/oss/uploads/201808/27/180827100043722.jpg" src="http://v3cdn.duoguyu.com/oss/uploads/201808/27/180827100043722.jpg" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    <span class="iconStatus orange sm mr5">互联网+</span>
                                    滴滴的救赎                	                            </h3>
                                <!--<p class="description cf999 mt15">位于北京市海淀区上地的滴滴全国总部大楼，从楼外可以看到，在两座颇具设计感的小...</p>-->
                                <p class="h40 cf999 mt10">位于北京市海淀区上地的滴滴全国总部大楼，从楼外可以看到，在两座颇具设计感的小...</p>
                                <p class="mt15 am-text-right"><span class="cf_green f12">阅读全文 &gt;</span></p>
                            </article>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="article_box">
                        <a href="http://www.duoguyu.com/internet/168.html" title="精酿，啤酒界的文艺复兴" target="_blank">
                            <div class="tophead hoverImg">
                                <img class="lazy" width="294" height="160" data-original="http://v3cdn.duoguyu.com/oss/uploads/201808/20/180820100751583.jpg" src="http://v3cdn.duoguyu.com/oss/uploads/201808/20/180820100751583.jpg" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    <span class="iconStatus orange sm mr5">互联网+</span>
                                    精酿，啤酒界的文艺复兴                	                            </h3>
                                <!--<p class="description cf999 mt15">每一个度过的夏夜，都伴随着几瓶啤酒的狂欢。或许我们早已经被燕京、青岛、雪花纯...</p>-->
                                <p class="h40 cf999 mt10">每一个度过的夏夜，都伴随着几瓶啤酒的狂欢。或许我们早已经被燕京、青岛、雪花纯...</p>
                                <p class="mt15 am-text-right"><span class="cf_green f12">阅读全文 &gt;</span></p>
                            </article>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="article_box">
                        <a href="http://www.duoguyu.com/continent/163.html" title="楼底有条朝西的马路" target="_blank">
                            <div class="tophead hoverImg">
                                <img class="lazy" width="294" height="160" data-original="http://v3cdn.duoguyu.com/oss/uploads/201806/22/180622021855597.jpg" src="http://v3cdn.duoguyu.com/oss/uploads/201806/22/180622021855597.jpg" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    <span class="iconStatus flesh sm mr5">小酒馆</span>
                                    楼底有条朝西的马路                	                            </h3>
                                <!--<p class="description cf999 mt15">猫用舌头舔了舔前爪，站立起来，三条腿着地，刚被舔舐过的那只前爪依旧收窝在胸前...</p>-->
                                <p class="h40 cf999 mt10">猫用舌头舔了舔前爪，站立起来，三条腿着地，刚被舔舐过的那只前爪依旧收窝在胸前...</p>
                                <p class="mt15 am-text-right"><span class="cf_green f12">阅读全文 &gt;</span></p>
                            </article>
                        </a>
                    </div>
                </li>
                <div class="clearfix"></div>
</#if>
            </ul>
            <div class="line"></div>
        </div>
    </div>
</div>