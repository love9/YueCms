
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>评论举报</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script type="text/javascript" src="/resources/js/common/jubao.js"></script>
    <style>
        .jubao{background:#fff;position:relative;overflow:hidden;width:404px;height:133px}.radio-item{width:160px;height:28px;position:relative;float:left;cursor:pointer}.radio-box{width:16px;height:16px;background:url(/resources/myblog/images/icon/icon_radio_0.jpg);display:block;float:left;margin-top:5px;margin-right:14px}.radio-checked{background:url(/resources/myblog/images/icon/icon_radio_1.jpg)!important}.radio-item .radio-text{margin-top:4px;display:inline-block;font-family:"Microsoft Yahei";font-weight:400;font-size:14px;color:#333}.rpt-hint{display:none;padding-top:48px}.rpt-hint-image{display:inline-block;width:40px;height:40px;background:url(/resources/myblog/images/icon/icon_ok.png) no-repeat;margin-bottom:24px;margin-left:182px}.rpt-hint-text{font-size:15px;font-weight:600;text-align:center}
    </style>
</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer  sixteen wide mobile column  ">
                  <div class="ui basic segment active">
                      <div class="ui info message">
                          <div class="header">友情提示</div>
                          <ul class="list">
                              <li>jubao(targetId)：用来弹出举报窗口（建议在调用该方法之前先判断用户是否登录），传入targetId是举报对象比如留言评论的id</li>
                              <li>getJuBaoValue()：获得当前选中的举报类型的值  （1：淫秽色情,2：广告垃圾,3：违法信息,4：其它）</li>
                              <li>jubaoSubmit(targetId)这个方法需要用户重写覆盖该方法 ，用来发送ajax提交举报.重写这个方法时，使用第二个方法获得当前必报类型，在根据参数targetId发送ajax请求即可完成举报提交。</li>
                              <li>
                                  jubaoSuccess()：ajax提交成功调用该方法给予用户提示。
                              </li>
                          </ul>
                      </div>
                      <form class="ui publish-link form">
                          <div class="ui center aligned basic segment">
                              <div class="ui primary submit submit button"  onclick="jubao()">弹出举报</div>
                              <div class="ui primary submit submit button"  onclick="jubaoSuccess()">弹出举报成功</div>
                          </div>
                      </form>
                  </div>
                    <script type="text/javascript">

                    </script>

              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</div>
</body>
</html>