<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<title>${title}</title>
	<link href="/resources/css/channelSite.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/js/common/channelSite.js"></script>
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
  
 <style>
    body{font:14px/1.5 "PingFang SC","Lantinghei SC","Microsoft YaHei","HanHei SC","Helvetica Neue","Open Sans",Arial,"Hiragino Sans GB","å¾®è½¯é›…é»‘",STHeiti,"WenQuanYi Micro Hei",SimSun,sans-serif;-webkit-font-smoothing:antialiased;}@media screen and (max-width:768px){.page{width:100%;margin:0 auto;padding:0px;background-color:#fff;box-shadow:#5079d9 2px 1px 5px 3px;}}@media screen and (min-width:768px){.page{width:70%;margin:0 auto;padding:10px 18px;background-color:#fff;}}.articleImg{height:360px;overflow:hidden;position:relative;}.articleImg .textBox{position:absolute;left:0;bottom:40px;z-index:10;padding:0 40px;width:100%;font-size:14px;color:#fff;line-height:28px;vertical-align:bottom;overflow:hidden;}.articleImg .textBox h1{font-size:28px;color:#fff;line-height:32px;vertical-align:bottom;}.articleImg .moreInfo{font-size:14px;color:#fff;line-height:28px;}.overlay{position:absolute;top:0;bottom:0;width:100%;-webkit-transition:all 0.2s ease;-moz-transition:all 0.2s ease;transition:all 0.2s ease;background-image:-webkit-linear-gradient(180deg,rgba(0,0,0,0.05) 5%,rgba(0,0,0,0.85) 100%);background-image:-moz-linear-gradient(180deg,rgba(0,0,0,0.05) 5%,rgba(0,0,0,0.85) 100%);background-image:linear-gradient(180deg,rgba(0,0,0,0.05) 5%,rgba(0,0,0,0.85) 100%);background-size:100%;}.articleImg .img{width:100%;height:360px;overflow:hidden;}.articleDetail{margin-top:20px;padding:20px;border-top:1px dotted #ddd;text-align:justify;}.articleDetail p{color:#4a4a4a;line-height:26px;font-size:15px !important;margin-bottom:15px;text-align:justify;}.articleDetail img{margin:0 0 8px 0;max-width:100%;vertical-align:middle;}.channelSiteInfo{margin:30px 0;text-align:right;}.iconChannelSite{display:inline-block;width:50px;height:50px;background:url(/resources/images/logo/logo_water.png) no-repeat center;background-size:50px;-moz-border-radius:50%;border-radius:50%;overflow:hidden;}.footerInfo.top{text-align:right;border-top:0;margin-top:0;}.footerInfo{margin-top:20px;padding-top:20px;border-top:1px dotted #ddd;}.footerInfo .iconSpan{cursor:pointer;font-size:12px;padding:4px 10px;line-height:12px;border-radius:4px;background:#f4f4f4;color:#999;margin-left:5px;text-decoration:none;}.footerInfo .iconSpan:hover{background:#51e0c1;color:#fff;}
 
 </style>
</head>
<body style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<!-- fullTabs 置顶热门标签 -->
<#include "/cms/myblog/semantic/block/index_fullTabs.ftl"/>
  
<div class="page">
    <div class="articleImg">
         <div class="textBox">
             <h1>${title!}</h1>
             <div class="moreInfo">${author!}</div>
              </div>
              <div class="overlay"></div>
         <div class="img" style="background:url(${cover_image!}) no-repeat center; background-size:cover;"></div>
    </div>
    
    <!--标签-->
    <div class="footerInfo top">
    <#if tagsList??>
      <#list tagsList as tag>
         <a class="iconSpan" data-id="${tag.id!}" target="_blank">${tag.name!}</a>
      </#list>
    </#if>
    </div>
    <!--跳转站点-->
    <div class="channelSiteInfo">
         <#if  extra1=="一个">
             <#assign siteClass="one">
         <#elseif extra1=="豆瓣">
             <#assign siteClass="douban">
         <#elseif extra1=="知乎">
             <#assign siteClass="zhihu">
         <#elseif extra1=="果壳">
             <#assign siteClass="guoke">
         <#elseif extra1=="36 氪">
             <#assign siteClass="kr36">
         <#elseif extra1=="PingWest 品玩">
             <#assign siteClass="pinwan">
         <#elseif extra1=="好奇心日报">
             <#assign siteClass="haoqixin">
         <#elseif extra1=="差评">
             <#assign siteClass="chaping">
         <#elseif extra1=="虎嗅">
             <#assign siteClass="huxiu">
         <#elseif extra1=="人间">
             <#assign siteClass="renjian">
         <#elseif extra1=="真实故事">
             <#assign siteClass="zhenshigushi">
         <#elseif extra1=="NGA">
             <#assign siteClass="NGA">
         <#elseif extra1=="网易">
             <#assign siteClass="wangyi">
         <#elseif extra1=="今日头条">
             <#assign siteClass="toutiao">
         <#elseif extra1=="新浪">
             <#assign siteClass="sina">
         <#elseif extra1=="简书">
             <#assign siteClass="jianshu">
         <#elseif extra1=="少数派">
             <#assign siteClass="sspai">
         <#elseif extra1=="数字尾巴">
             <#assign siteClass="dgtle">
         <#elseif extra1=="十五言">
             <#assign siteClass="yan15">
         <#elseif extra1=="大象公会">
             <#assign siteClass="dxgh">
         <#elseif extra1=="界面">
             <#assign siteClass="jiemian">
         <#elseif extra1=="新世相">
             <#assign siteClass="xinshixiang">
         <#elseif extra1=="极客公园">
             <#assign siteClass="geekpark">
         <#elseif extra1=="站酷">
             <#assign siteClass="zcool">
         </#if>
        <a href="javascript:;" data-href="${extra2!}" data-site-name="${extra1!}" title="查看原文" id="channelSite" class="iconChannelSite jumpHref  ${siteClass!}"></a>
    </div>
    
    <!--文章正文-->
    <div class="articleDetail">
    ${content!}
    </div>

    <!--文章来源版权信息-->
    <div class="copyInfo">
         <p class="theEnd"><span>-</span><b>THE END</b><span>-</span></p> 
          <div class="mt10">
           <p>版权声明：本文来自<a href="javascript:;" data-href="${extra2!}" title="查看原文" data-site-name="${extra1!}" class="channelSite jumpHref">${extra1!}</a>，相关版权归原作者及来源网站所有。</p>
           <p><a href="javascript:;" data-href="${extra2!}" data-site-name="${extra1!}" title="查看原文" class="channelSite jumpHref">[ 查看原文 ]</a></p> 
          </div>
    </div>
               
    <a class="to-top" title=""  style="position: fixed; right: 25px; bottom: 50px; cursor: pointer; display: block;" data-original-title="点击返回顶部"  ></a>
               
    <div id="SOHUCS"  sid="${id!}"></div>
    <script charset="utf-8" type="text/javascript" src="https://changyan.sohu.com/upload/changyan.js" ></script>
    <script type="text/javascript">
          window.changyan.api.config({
               appid: 'cyuiRxL9X',
               conf: '55e9ed6e5af85101aa2df4267ed012cd'
         });
    </script>        
               
</div>
<!--百度自动推送-->
<script>
    (function(){
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        }
        else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
</script>
<!--百度统计-->
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?065d63785bc5969ba048e6178619b603";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
<script type="text/javascript" src="/resources/js/common/viewArticle.js"></script>
</body>
</html>