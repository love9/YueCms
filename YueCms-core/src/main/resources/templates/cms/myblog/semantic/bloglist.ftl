
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>
<body style="background: #e9ecf3 !important;" class="pushable">

<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>

<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
    <div class="ui basic segment" id="mainScreen" >
        <div class="ui container bg-white " >
            <div class="ui grid" >
            <div class="row" >
                <div class="sixteen wide tablet eleven wide computer column">
            <div class="ui divided items" style="padding:1rem;background: #fff;">

                <#if articles.rows?? && (articles.rows?size > 0)>
                    <#list articles.rows as item>
                        <div class="item">

                        <#if item.cover_image?? && item.cover_image!="">
                            <div class="ui medium image">
                                <img src="${item.cover_image!}">
                            </div>
                        </#if>

                        <div class="content">
                            <#if item.static_url?? && item.static_url!="">
                                <a class="header" href="/myblog/article/detail/${item.id!}">${item.title!}</a>
                                <#--<a class="header" href="${item.static_url!}">${item.title!}</a>-->
                            <#else>
                                <a class="header" href="/myblog/article/detail/${item.id!}">${item.title!}</a>
                            </#if>
                            <div class="meta">
                                <span class="cinema"></span>
                            </div>
                            <div class="description">
                                <p>${item.description!}</p>
                            </div>
                            <div class="extra">
                                <div class="ui  horizontal list article_info">
                                    <div class="item">
                                        <span class="icon_tag am-fl"><a href="/myblog/bloglist/tag/$item.tag" title="${item.tag_name!}" target="_blank" class="classname">${item.tag_name!}</a></span>
                                    </div>
                                    <div class="item">
                                        <span class="dtime am-fl">${item.createTime!}</span>
                                    </div>
                                    <div class="item">
                                        <span class="viewnum am-fr">浏览（<a href="javascript:;">${item.hit!}</a>）</span>
                                    </div>
                                    <div class="item">
                                        <span class="pingl am-fr">评论（ ${item.reply_num!} ）</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </#list>
                </#if>
                <div class="ui divider"></div>
                <div style="position:relative;bottom:0px;text-align: center;" id="pagebar" class="pagebar"></div>
            </div>
<script type="text/javascript">
            var pageNo=${articles.page!};
            var pageSize=10;
            var totalPages =${articles.totalPages!};
            initPager();
            function initPager() {
                var $pagebar = $("#pagebar");
                laypage({
                    cont: $pagebar, //容器。值支持id名、原生dom对象，jquery对象,
                    pages: totalPages, //总页数
                    curr: pageNo,
                    jump:jumpPage,
                    skin: 'molv', //皮肤
                    first: '首页', //若不显示，设置false即可
                    last: '尾页', //若不显示，设置false即可
                    prev: '上一页', //若不显示，设置false即可
                    next: '下一页' //若不显示，设置false即可
                });
            }

            function jumpPage(obj,first){
                //console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                //console.log(obj.limit); //得到每页显示的条数
                //alert("共"+context.option.pages+"页，当前第"+context.option.curr+"页");
                pageNo=obj.curr;
                //alert(pageNo);
                if(!first){
                    var uri=Fast.getUri();
                    window.location.href=uri+"?page="+pageNo+"&rows="+pageSize+"&myblog=1";
                }
            }

</script>
                </div>


                <div class="sixteen wide tablet five wide computer column"  style="background:#fff;">

                <#if tags??>
                        <!--标签云 start-->
                        <div class="cloud" id="yunTags">
                            <h3>标签云</h3>
                        <#list tags as tag>
                            <#if tag_index%13==1>
                                <#assign css="red">
                            <#elseif tag_index%13==2>
                                <#assign css="orange">
                            <#elseif tag_index%13==3>
                                <#assign css="yellow">
                            <#elseif tag_index%13==4>
                                <#assign css="olive">
                            <#elseif tag_index%13==5>
                                <#assign css="green">
                            <#elseif tag_index%13==6>
                                <#assign css="teal">
                            <#elseif tag_index%13==7>
                                <#assign css="blue">
                            <#elseif tag_index%13==8>
                                <#assign css="violet">
                            <#elseif tag_index%13==9>
                                <#assign css="purple">
                            <#elseif tag_index%13==10>
                                <#assign css="pink">
                            <#elseif tag_index%13==11>
                                <#assign css="brown">
                            <#elseif tag_index%13==12>
                                <#assign css="grey">
                            <#elseif tag_index%13==13>
                                <#assign css="black">
                            </#if>
                            <a class="ui ${css!}  basic label" href="/myblog/bloglist/tag/${tag.id!}"  data-id="${tag.id!}" style="margin-top: 5px;">
                                ${tag.name!}
                                <div class="detail">${tag_index!}</div>
                            </a>
                </#list>

                            <script type="text/javascript">
                                $(function(){
                                    var url=  window.location.href;
                                    var host=window.location.host;
                                    var protocol=window.location.protocol;
                                    var uri=url.replace(protocol+"//"+host,"");
                                    uri=decodeURIComponent(uri);
                                    $.each($("#yunTags   a"),function(i,item){
                                        if(uri==$(item).attr("href")){
                                            //$(item).addClass("active").siblings().removeClass("active");
                                            $(item).removeClass("basic");
                                            $(item).attr("href","/myblog/bloglist");
                                        }
                                    });
                                })
                            </script>
                        </div>
                        <!--标签云 end-->
                </#if>
                        <!--排行 start-->
                        <div class="moreSelect" id="lp_right_select">
                            <script>
                                window.onload = function ()
                                {
                                    var oLi = document.getElementById("tab").getElementsByTagName("li");
                                    var oUl = document.getElementById("ms-main").getElementsByTagName("div");

                                    for(var i = 0; i < oLi.length; i++)
                                    {
                                        oLi[i].index = i;
                                        oLi[i].onmouseover = function ()
                                        {
                                            for(var n = 0; n < oLi.length; n++) oLi[n].className="";
                                            this.className = "cur";
                                            for(var n = 0; n < oUl.length; n++) oUl[n].style.display = "none";
                                            oUl[this.index].style.display = "block"
                                        }
                                    }
                                }
                            </script>
                            <div class="ms-top">
                                <ul class="hd" id="tab">
                                    <li class="cur"><a href="javascript:;">点击排行</a></li>
                                    <li class=""><a href="javascript:;">最新文章</a></li>
                                    <li class=""><a href="javascript:;">站长推荐</a></li>
                                </ul>
                            </div>
                            <div class="ms-main" id="ms-main" style="font: 12px/1.5 'Microsoft Yahei', 'Simsun';color: #666666;">
                                <div style="display: block;" class="bd bd-news">
                                    <ul>
                                        <li><a href="javascript:;" target="_blank">住在手机里的朋友</a></li>
                                        <li><a href="javascript:;" target="_blank">教你怎样用欠费手机拨打电话</a></li>
                                        <li><a href="javascript:;" target="_blank">原来以为，一个人的勇敢是，删掉他的手机号码...</a></li>
                                        <li><a href="javascript:;" target="_blank">手机的16个惊人小秘密，据说99.999%的人都不知</a></li>
                                        <li><a href="javascript:;" target="_blank">你面对的是生活而不是手机</a></li>
                                        <li><a href="javascript:;" target="_blank">豪雅手机正式发布! 在法国全手工打造的奢侈品</a></li>
                                    </ul>
                                </div>
                                <div class="bd bd-news" style="display: none;">
                                    <ul>
                                        <li><a href="javascript:;" target="_blank">原来以为，一个人的勇敢是，删掉他的手机号码...</a></li>
                                        <li><a href="javascript:;" target="_blank">手机的16个惊人小秘密，据说99.999%的人都不知</a></li>
                                        <li><a href="javascript:;" target="_blank">住在手机里的朋友</a></li>
                                        <li><a href="javascript:;" target="_blank">教你怎样用欠费手机拨打电话</a></li>
                                        <li><a href="javascript:;" target="_blank">你面对的是生活而不是手机</a></li>
                                        <li><a href="javascript:;" target="_blank">豪雅手机正式发布! 在法国全手工打造的奢侈品</a></li>
                                    </ul>
                                </div>
                                <div class="bd bd-news" style="display: none;">
                                    <ul>
                                        <li><a href="javascript:;" target="_blank">手机的16个惊人小秘密，据说99.999%的人都不知</a></li>
                                        <li><a href="javascript:;" target="_blank">你面对的是生活而不是手机</a></li>
                                        <li><a href="javascript:;" target="_blank">住在手机里的朋友</a></li>
                                        <li><a href="javascript:;" target="_blank">豪雅手机正式发布! 在法国全手工打造的奢侈品</a></li>
                                        <li><a href="javascript:;" target="_blank">教你怎样用欠费手机拨打电话</a></li>
                                        <li><a href="javascript:;" target="_blank">原来以为，一个人的勇敢是，删掉他的手机号码...</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
<!--排行 end-->
<#include "/cms/myblog/semantic/block/contactUs.ftl"/>
                </div>
            </div>
            </div>
        </div>
    </div>
<!-- content end -->
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>