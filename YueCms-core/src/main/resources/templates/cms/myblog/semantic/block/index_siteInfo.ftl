<div class="blog-sidebar-widget sidebar-module">
    <h3 class="blog-sidebar-widget-title"><b>网站信息</b></h3>
    <div class="blog-sidebar-widget-content blog-side-bar" id="">

<#--<#if hots??>

    <#list hots as item>
        <div class="item">
            <#if item.static_url?? && item.static_url!="">
                <a href="${item.static_url!}" title="${item.title!}" target="_blank">
            <#else>
                <a href="/myblog/article/detail/${item.id!}" title="${item.title!}" target="_blank">
            </#if>
                    <h3>${item.title!}</h3>
                    <div class="desc mb15">${item.description!}</div>
                </a>
        </div>
    </#list>
<#else>
        <div class="item">
            <a href="http://www.duoguyu.com/continent/157.html" title="北京租房故事" target="_blank">
                <h3>北京租房故事</h3>
                <div class="desc mb15">“如果我能在北京租房，我就能征服世界。” 有人这样改编了英文歌曲《纽约...</div>
            </a>
        </div>
        <div class="item">
            <a href="http://www.duoguyu.com/echo/131.html" title="删除是一种体面的告别" target="_blank">
                <h3>删除是一种体面的告别</h3>
                <div class="desc mb15">“在吗？”对着陌生的发信人头像端详良久，我敲下“您是？”二字，却迟迟没...</div>
            </a>
        </div>
        <div class="item">
            <a href="http://www.duoguyu.com/continent/176.html" title="早熟" target="_blank">
                <h3>早熟</h3>
                <div class="desc mb15">中学时我很想早恋，因为我觉得放学时骑车载着女生回家很酷。

                    那时隔壁班有...</div>
            </a>
        </div>
        <div class="item">
            <a href="http://www.duoguyu.com/echo/175.html" title="程序员漫游指南" target="_blank">
                <h3>程序员漫游指南</h3>
                <div class="desc mb15">这里是你的办公室。这里是你的工位。这是你的手机，你电话值班的时候它会响...</div>
            </a>
        </div>
        <div class="item">
            <a href="http://www.duoguyu.com/continent/173.html" title="滑板少女" target="_blank">
                <h3>滑板少女</h3>
                <div class="desc mb15">中午休息的时候，我在IFC楼下的麦当劳买了一个吉士汉堡套餐，跟着走回到中...</div>
            </a>
        </div>
</#if>-->
    <style>
        .site-info li {
            color: #545c63 !important;
        }
        .sidebar-module {
            z-index: 10;
            padding: 1px 10px 15px 10px;
            /* border: 1px solid #ccc; */
            line-height: 30px;
            margin: 0 0 10px 0;
            background: #fff;
            border-radius: 4px;
        }
    </style>
    <ul class="ul-default site-info">
            <@myTag method="siteInfo">
                <li> <i class="fa fa-file fa-fw"></i>  文章总数：${siteInfo.articleCount!(0)} 篇</li>
                <li> <i class="fa fa-tags fa-fw"></i> 标签总数：${siteInfo.tagCount!(0)} 个</li>

                <li> <i class="fa fa-comments fa-fw"></i> 留言数量：${siteInfo.commentCount!(0)} 条</li>
                <li> <i class="fa fa-users fa-fw"></i> 在线人数：<span class="onlineNum">1</span>人</li>
                <li> <i class="fa fa-calendar fa-fw"></i> 运行天数：${siteInfo.buildSiteDate!(0)}天</li>
                <li> <i class="fa fa-pencil-square fa-fw"></i> 最后更新：${siteInfo.lastUpdateTime}</li>
            </@myTag>
    </ul>
    </div>

    <div class="adGrid" ><a href="http://www.wildaidchina.org" target="_blank" title="野生救援"><img src="/resources/myblog/images/gy.jpg"></a></div>

</div>