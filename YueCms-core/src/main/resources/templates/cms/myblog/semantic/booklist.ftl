
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>

    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <style>

        .paihang {
            background: #FFF;
            overflow: hidden;
            margin-bottom: 20px;
        }

        .paihang ul {
            padding: 10px;
        }
        .paihang ul li {
            border-bottom: solid 1px #EAEAEA;
            font-size: 14px;
            margin: 0 0 10px 0;
            padding: 0 0 10px 0;
            overflow: hidden;
        }
        .paihang ul li b {
            height: 30px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            display: block;
        }
        .paihang ul li p {
            line-height: 24px;
            color: #888;
        }
        .paihang ul li:hover p {
            color: #000;
        }
        .paihang ul li:hover i img {
            transform: scale(1.1);
        }
        .paihang li i {
            width: 100px;
            height: 90px;
            overflow: hidden;
            display: block;
            border: #efefef 1px solid;
            float: left;
            margin-right: 10px;
        }
        .paihang li img {
            border: 0;
            display: block;
            height: 100%;
            margin: auto;
            -moz-transition: all .5s ease;
            -webkit-transition: all .5s ease;
            transition: all .5s ease;
        }
        .picinfo h3 {
            border-bottom: #ccc 1px solid;
            padding: 20px 0;
            margin: 0 20px;
        }
        .picinfo span {
            padding: 20px;
            display: block;
            color: #666;
        }

    </style>
    <script>
        $(function(){
            $('.special.cards .image').dimmer({
                on: 'hover'
            });
        })

    </script>
</head>

<body  style="background: #e9ecf3 !important;" class="pushable">
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
    <div class="ui basic segment" id="mainScreen" >
        <div class="ui container bg-white " >
            <div class="ui grid" >
                <div class="row" >
                    <!--左侧-->
                    <div class="sixteen wide tablet eleven wide computer column">

<#if books?? && (books?size > 0) >
                        <div class="ui two special stackable cards">
                            <#list books.rows as item>
                            <div class="card">
                                <div class="blurring dimmable image">
                                    <div class="ui  dimmer">

                                        <div class="content">
                                            <div class="description">
                                                <p style="text-indent: 2rem;">${item.description!}</p>
                                            </div>
                                            <div class="ui divider"></div>
                                            <div class="center">
                                                <#if item.static_url?? && item.static_url!="">
                                                    <#--<a href="${item.static_url!}" class="ui primary button">查看</a>-->
                                                    <a href="/myblog/book/detail/${item.id!}" class="ui primary button">查看</a>
                                                <#else>
                                                    <a href="/myblog/book/detail/${item.id!}" class="ui primary button">查看</a>
                                                </#if>
                                            </div>
                                        </div>
                                    </div>

                                    <#if item.sheet_url?? && item.sheet_url!="">
                                        <img style="width: 348px;height:448px" src="${item.sheet_url!}" >
                                    <#else>
                                        <img src="/resources/myblog/images/21.jpg">
                                    </#if>
                                </div>
                                <div class="content">
                                    <a class="header">${item.bookname!}</a>
                                    <div class="meta">
                                        <span> <i class="user icon"></i>作者：${item.author!}</span>
                                    </div>

                                </div>
                                <div class="extra content">
                                    <span class="left floated like">
                                      <i class="like icon"></i>
                                      Like
                                    </span>
                                    <span class="right floated star">
                                      <i class="star icon"></i>
                                      Favorite
                                    </span>
                                </div>
                            </div>
                            </#list>
                        </div>
<#else>
                        <div class="ui two special stackable cards">
                            <div class="card">
                                <div class="blurring dimmable image">
                                    <div class="ui inverted dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <div class="ui primary button">Add Friend</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/resources/myblog/images/21.jpg">
                                </div>
                                <div class="content">
                                    <a class="header">Team Hess</a>
                                    <div class="meta">
                                        <span class="date">Create in Aug 2014</span>
                                    </div>
                                    <div class="description">
                                        <p>Cute dogs come in a variety of shapes and sizes. Some cute dogs are cute for their adorable faces, others for their tiny stature, and even others for their massive size.</p>
                                    </div>
                                </div>
                                <div class="extra content">
                                    <span class="left floated like">
                                      <i class="like icon"></i>
                                      Like
                                    </span>
                                    <span class="right floated star">
                                      <i class="star icon"></i>
                                      Favorite
                                    </span>
                                </div>
                            </div>
                            <div class="card">
                                <div class="blurring dimmable image">
                                    <div class="ui inverted dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <div class="ui primary button">Add Friend</div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="/resources/myblog/images/21.jpg">
                                </div>
                                <div class="content">
                                    <a class="header">Team Hess</a>
                                    <div class="meta">
                                        <span class="date">Create in Aug 2014</span>
                                    </div>
                                    <div class="description">
                                        <p>Cute dogs come in a variety of shapes and sizes. Some cute dogs are cute for their adorable faces, others for their tiny stature, and even others for their massive size.</p>
                                    </div>
                                </div>
                                <div class="extra content">
                                    <span class="left floated like">
                                      <i class="like icon"></i>
                                      Like
                                    </span>
                                    <span class="right floated star">
                                      <i class="star icon"></i>
                                      Favorite
                                    </span>
                                </div>
                            </div>
                        </div>
</#if>

                        <div class="ui divider"></div>
                        <div style="position:relative;bottom:0px;text-align: center;" id="pagebar" class="pagebar"></div>
                        <script type="text/javascript">
                            var pageNo=${books.page!};
                            var pageSize=10;
                            var totalPages =${books.totalPages!};
                            initPager();
                            function initPager() {
                                var $pagebar = $("#pagebar");
                                laypage({
                                    cont: $pagebar, //容器。值支持id名、原生dom对象，jquery对象,
                                    pages: totalPages, //总页数
                                    curr: pageNo,
                                    jump:jumpPage,
                                    skin: 'molv', //皮肤
                                    first: '首页', //若不显示，设置false即可
                                    last: '尾页', //若不显示，设置false即可
                                    prev: '上一页', //若不显示，设置false即可
                                    next: '下一页' //若不显示，设置false即可
                                });
                            }

                            function jumpPage(obj,first){
                                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                console.log(obj.limit); //得到每页显示的条数
                                //alert("共"+context.option.pages+"页，当前第"+context.option.curr+"页");
                                pageNo=obj.curr;
                                //alert(pageNo);
                                if(!first){
                                    var uri=Fast.getUri();
                                    window.location.href=uri+"?page="+pageNo+"&rows="+pageSize+"&myblog=1";
                                }
                            }

                        </script>
                    </div>

                    <!--右侧-->
                    <div class="sixteen wide tablet five wide computer column"  style="background:#fff;">

                    <!--标签云 start-->
                    <#if tags??>

                        <div  id="yunTags">
                        <h2 class="hometitle">标签云</h2>
                        <ul>
                        <#list tags as tag>
                            <#if tag_index%13==1>
                                <#assign css="red">
                            <#elseif tag_index%13==2>
                                <#assign css="orange">
                            <#elseif tag_index%13==3>
                                <#assign css="yellow">
                            <#elseif tag_index%13==4>
                                <#assign css="olive">
                            <#elseif tag_index%13==5>
                                <#assign css="green">
                            <#elseif tag_index%13==6>
                                <#assign css="teal">
                            <#elseif tag_index%13==7>
                                <#assign css="blue">
                            <#elseif tag_index%13==8>
                                <#assign css="violet">
                            <#elseif tag_index%13==9>
                                <#assign css="purple">
                            <#elseif tag_index%13==10>
                                <#assign css="pink">
                            <#elseif tag_index%13==11>
                                <#assign css="brown">
                            <#elseif tag_index%13==12>
                                <#assign css="grey">
                            <#elseif tag_index%13==13>
                                <#assign css="black">
                            </#if>
                             <a class="ui ${css!} basic label" href="/myblog/booklist/tags/${tag}"  style="margin-top: 5px;">
                                 ${tag}
                             </a>
                        </#list>
                        </ul>
                        <script type="text/javascript">
                            $(function(){
                                var url=  window.location.href;
                                var host=window.location.host;
                                var protocol=window.location.protocol;
                                var uri=url.replace(protocol+"//"+host,"");
                                uri=decodeURIComponent(uri);
                                $.each($("#yunTags ul a"),function(i,item){
                                    if(uri==$(item).attr("href")){
                                        $(item).removeClass("basic");
                                        $(item).attr("href","/myblog/booklist");
                                    }
                                });
                            })
                        </script>
                    </#if>
                    </div>
                    <!--标签云 end -->

                        <div class="paihang">
                                <h2 class="hometitle">点击排行</h2>
                                <ul>
                                    <li><b><a href="/download/div/2015-04-10/746.html" target="_blank">【活动作品】柠檬绿兔小白个人博客模板30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/t02.jpg"></i>展示的是首页html，博客页面布局格式简单，没有复杂的背景，色彩局部点缀，动态的幻灯片展示，切换卡，标...</p>
                                    </li>
                                    <li><b><a href="/download/div/2014-02-19/649.html" target="_blank"> 个人博客模板（2014草根寻梦）30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/b03.jpg"></i>2014第一版《草根寻梦》个人博客模板简单、优雅、稳重、大气、低调。专为年轻有志向却又低调的草根站长设...</p>
                                    </li>
                                    <li><b><a href="/download/div/2013-08-08/571.html" target="_blank">黑色质感时间轴html5个人博客模板30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/b04.jpg"></i>黑色时间轴html5个人博客模板颜色以黑色为主色，添加了彩色作为网页的一个亮点，导航高亮显示、banner图片...</p>
                                    </li>
                                    <li><b><a href="/download/div/2014-09-18/730.html" target="_blank">情侣博客模板系列之《回忆》Html30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/b05.jpg"></i>Html5+css3情侣博客模板，主题《回忆》，使用css3技术实现网站动画效果，主题《回忆》,分为四个主要部分，...</p>
                                    </li>

                                </ul>
                            </div>
                            <div class="paihang">
                                <h2 class="hometitle">站长推荐</h2>
                                <ul>
                                    <li><b><a href="/download/div/2015-04-10/746.html" target="_blank">【活动作品】柠檬绿兔小白个人博客模板30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/t02.jpg"></i>展示的是首页html，博客页面布局格式简单，没有复杂的背景，色彩局部点缀，动态的幻灯片展示，切换卡，标...</p>
                                    </li>
                                    <li><b><a href="/download/div/2014-02-19/649.html" target="_blank"> 个人博客模板（2014草根寻梦）30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/b03.jpg"></i>2014第一版《草根寻梦》个人博客模板简单、优雅、稳重、大气、低调。专为年轻有志向却又低调的草根站长设...</p>
                                    </li>
                                    <li><b><a href="/download/div/2013-08-08/571.html" target="_blank">黑色质感时间轴html5个人博客模板30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/b04.jpg"></i>黑色时间轴html5个人博客模板颜色以黑色为主色，添加了彩色作为网页的一个亮点，导航高亮显示、banner图片...</p>
                                    </li>
                                    <li><b><a href="/download/div/2014-09-18/730.html" target="_blank">情侣博客模板系列之《回忆》Html30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/b05.jpg"></i>Html5+css3情侣博客模板，主题《回忆》，使用css3技术实现网站动画效果，主题《回忆》,分为四个主要部分，...</p>
                                    </li>
                                    <li><b><a href="/download/div/2014-04-17/661.html" target="_blank">黑色Html5个人博客模板主题《如影随形》30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/b06.jpg"></i>014第二版黑色Html5个人博客模板主题《如影随形》，如精灵般的影子会给人一种神秘的感觉。一张剪影图黑白...</p>
                                    </li>
                                    <li><b><a href="/jstt/bj/2015-01-09/740.html" target="_blank">【匆匆那些年】总结个人博客经历的这四年…30...</a></b>
                                        <p><i><img src="/resources/myblog/images/blog/mb02.jpg"></i>博客从最初的域名购买，到上线已经有四年的时间了，这四年的时间，有笑过，有怨过，有悔过，有执着过，也...</p>
                                    </li>
                                </ul>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- content end -->

<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>

</body>
</html>