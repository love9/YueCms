
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>弹出微信二维码</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer  sixteen wide mobile column  ">

                    <div class="body-wrapper">
                        <style>
                            #activityB {
                                position: fixed;
                                top: 150px;
                                left: calc(100% - 110px);
                                width: 110px;
                                height: 114px;
                                z-index: 1000;
                                background: url(/resources/myblog/demo/hongbaotiaodong.png) no-repeat center;
                                background-size: 100% 100%;
                            }
                            .sideBar-animate {
                                -webkit-animation: two-up-down .8s ease infinite 80ms alternate;
                                animation: two-up-down .8s ease infinite 80ms alternate;
                                position: absolute;
                                width: 64px;
                                height: 49px;
                                top: -60px;
                                left: -6px;
                                background: url(/resources/myblog/demo/sideBar-vip.png) no-repeat center;
                            }
                            #activityB a {
                                color: #666;
                                text-decoration: none;
                                -webkit-transition: all .2s;
                                transition: all .2s;
                            }
                            @keyframes two-up-down{from{-webkit-transform:translate(0,8px);transform:translate(0,8px);}to{-webkit-transform:translate(0,-8px);transform:translate(0,-8px);}}
                        .ewmBox{
                            position: relative; width: 258px;left: calc(50% - 129px); height: 315px;z-index: 2000; display: block;
                        }
                        .ewmBox img.hbewm{
                                position: relative;bottom:217px;left:78px; z-index: 3000;width: 106px;height: 106px;
                        }
                        </style>

                        <!--红包二维码盒子-->
                        <div class="body-wrapper">
                            <div class="ewmBox"　style="">
                                <div class="hbbbb"><img src="/resources/myblog/demo/hbclose.png" alt="" style="cursor:pointer;position: relative;bottom:-9px;left:252px;"   class="close-btn">
                                    <a href="" style="position: relative;"><img src="/resources/myblog/demo/hongbao.png" alt=""></a>
                                    <!--二维码-->
                                    <img class="hbewm" src="/resources/images/front_wx.png" alt="" style="">

                                    <p style="position: relative;bottom:193px;padding-left: 27px;font-size:14px;color: #ffffff;">如扫描无效搜索一下微信公众号</p>

                                    <p style="position: relative;bottom:193px;margin-left:76px;cursor: pointer;color: #ffffff;width: 107px;height: 25px;line-height:25px;border: 1px dashed #ffffff;text-align: center;font-size: 16px;">
                                        访问官网</p></div>
                            </div>
                        </div>

                        <!--弹出按钮-->
                        <a href="javascript:void(0)" id="activityB" class="sideBar-animate limit-width-hide"></a>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#activityB').click(function () {
                                $(".ewmBox").show();
                            });
                            $(".ewmBox .close-btn").click(function () {
                                $(".ewmBox").hide();
                            })
                        })
                    </script>

              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
</body>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</html>