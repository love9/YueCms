
<!doctype html>
<html>
<head>
 <meta charset="utf-8">
<title>博客登录</title>
<link rel="shortcut icon" href="favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=no;">
 <meta name="renderer" content="webkit">
 <meta name="description" content="">
 <meta name="author" content="">

 <link rel="shortcut icon" href="/favicon.ico">

 <link rel="stylesheet" type="text/css" href="/resources/js/login_examstart/ksxFont.css">
 <link rel="stylesheet" type="text/css" href="/resources/lib/bootstrap/css/bootstrap.min.css">

 <link rel="stylesheet" href="/resources/js/login_examstart/login-reg-style.css">

</head>

<body id="login" class="login">
<div class="login-wrap">
 <div class="bg-pic">
  <img class="img-bg" src="/resources/js/login_examstart/images/login-bg-small.jpg">
 </div>
 <div class="login-content-wrap" >
  <div id="login-content"  class="login-content" >
   <canvas id="bg-canvas" width="360" height="580" style="position: absolute;z-index: -1"></canvas>
   <form name="loginform" class="login-form" id="loginform" action="/login" method="post">
    <div style="text-align: center;
    margin-left: 8px;
    font-size: 35px;
    vertical-align: 7px;
    font-family: arial;
    height: 60px;"><span id="logo-text"><label style="    display: inline-block;
    background: #4386f5;
    border-radius: 50%;
    color: #fff;
    padding: 11px 8px;
    margin-left: 6px;
    position: relative;
    box-shadow: 1px 1px 6px #ccc;">My<span class="logo-dot rotate"></span></label>Blog</span></div>
    <div class="main-error-tips" id="errormsg"></div>
    <input type="hidden" id="redirectUrl" name="redirectUrl" value="${redirectUrl!}" >
    <input type="hidden" id="from" name="from" value="myblog" >
    <div class="username">
     <em class="icon icon-ksx-username"></em>
     <span>|</span>
     <input type="text" name="account" id="account" placeholder="账号" class="userNameInput">
    </div>
    <div class="password">
     <em class="icon icon-ksx-password"></em>
     <span>|</span>
     <input type="password" name="password" id="password" placeholder="密码" class="passwordInput">
     <a href="/myblog/forget" style="position: relative;right:0;top:-18px;" class="forget-password">忘记密码</a>
    </div>
    <div class="remember">
     <label for="RememberMe" style="float: left;"><input type="checkbox" id="RememberMe" name="RememberMe">记住密码</label>
    </div>
    <a href="javascript:void(0);" class="btn btn-login btn-primary">登录</a>

    <!--<a href="https://open.weixin.qq.com/connect/qrconnect?appid=wx3dbf3a23e8456f1a&amp;redirect_uri=https://www.kaoshixing.com/account/wechat_login_independent&amp;response_type=code&amp;scope=snsapi_login&amp;state=STATE#wechat_redirect" class="btn btn-wechat btn-default"><em class="icon icon-ksx-wechat"></em>微信快速登录</a>-->
    <a style="float: left;" onclick="javascript:alert('暂时未开放!');" href="#" class="btn-regist">注册账号</a>
    <#--<a  style="float: right;"  href="/reg/developer" class="btn-regist">申请参与系统开发</a>-->
   </form>
   <div id="loginWechet" class="reg-form loginWechet weChatBind" style="display: none;">
    <div class="reg-tips finish-tips">登陆成功！</div>
    <em class="icon icon-ksx-succeed"></em>
    <div class="finish-text">绑定微信，即可扫码一键登录</div>
    <a href="javascript:void(0);" class="btn btn-goto-bind all-right btn-primary">立即绑定微信</a>
    <a href="javascript:void(0);" class="btn btn-goto-login">先不绑定，直接登录</a>
   </div>
  </div>
 </div>
</div>

<script src="/resources/js/jquery.min.js"></script>
<script src="/resources/js/login_examstart/jquery.qrcode.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
 $(".qr-codes").qrcode({
  size:100,
  minVersion:3,
  fill:"#000",
  background:"#fff",
  text:window.location.href,
  radius:0,
  quiet:2
 });

</script>
<script>
 var  domain = "";
 $(".userNameInput,.passwordInput").on("focus",function(){
  $(this).parent().addClass("focus");
 });
 $(".userNameInput,.passwordInput").on("blur",function(){
  $(this).parent().removeClass("focus");
 });
 $(document).ready(function () {
  //登录
  $(".btn-login").click(function (e) {
   userLogin();
  });
  //回车登录
  $('.login-wrap').keypress(function (event) {
   if (event.keyCode == '13') {
    userLogin();
    event.preventDefault();
   }
  });
  //获取当前浏览器地址并赋值给隐含字段
  $("input[name=nextUrl]").val(window.location.href);

 });
 //用户登录
 function userLogin() {
  if (checkWM()) {
   document.getElementById("loginform").submit();
  }
 }
 //检查用户的表单信息
 function checkWM() {
  if ($.trim($("#account").val()) == "") {
   alert("账号不能为空！\r\r请重新填写！");
   $("#account").focus();
   return false;
  }
  if ($("#password").val() == "") {
   alert("用户密码不能为空！\r\r请重新填写！");
   $("#password").focus();
   return false;
  }
  return true;
 }
</script>
<script type="text/javascript">
 var KEY_COOKIE_USERNAME='${sys.cookieNameKey?default("")}'
 var KEY_COOKIE_PASSWORD='${sys.cookiePassKey?default("")}'
 var KEY_COOKIE_REMEMBER='${sys.pageRememberMeKey?default("")}'
 var cookieName=getCookie(KEY_COOKIE_USERNAME);
 var cookiePass=getCookie(KEY_COOKIE_PASSWORD);
 var cookieRm=getCookie(KEY_COOKIE_REMEMBER);

 function getCookie(name){
  //获取cookie字符串
  var strCookie=document.cookie;
  //将多cookie切割为多个名/值对
  var arrCookie=strCookie.split("; ");
  var value="";
  //遍历cookie数组，处理每个cookie对
  for(var i=0;i<arrCookie.length;i++){
   var arr=arrCookie[i].split("=");
   if(name==arr[0]){
    value=arr[1];
    break;
   }
  }
  return value;
 }
 function renderBg() {
  var a = document.getElementById("bg-canvas"),b= document.getElementById("login-content").offsetWidth,c= document.getElementById("login-content").offsetHeight;
         // b = document.documentElement.clientWidth,
         // c = document.documentElement.clientHeight;
  a.width = b,
        a.height = c;
  var d = a.getContext("2d");
  d.strokeStyle = "#f6f6f6",
          d.lineWidth = 1,
          d.beginPath(),
          d.translate(.5, .5);
  for (var e = 20; e <= c; e += 20) d.moveTo(0, e),
          d.lineTo(b, e);
  for (var e = 20; e <= b; e += 20) d.moveTo(e, 0),
          d.lineTo(e, c);
  d.closePath(),
          d.stroke()
 }

 $(function(){
   renderBg();
  window.addEventListener("resize",
          function() {
           renderBg()
  });

  if(cookieRm=="1")
  {
   $("#RememberMe").attr("checked",'true');
  }
  var msg="${msg!}";
  if(msg!=""){
   alert("${msg!}");
   $("#account").focus();
  }
  $("#account").val(cookieName.replace(/\"/g, ""));
  $("#password").val(cookiePass);

 })
</script>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>