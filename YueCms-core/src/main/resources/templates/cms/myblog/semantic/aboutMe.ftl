
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="shortcut icon" href="/favicon.ico">

    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script type="text/javascript" src="/resources/lib/clipboard/clipboard.min.js"></script>
</head>

<body class="pushable">
<!--手机试图导航-->
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->
    <div class="ui basic  segment" id="mainScreen" style="margin-top: 60px;">
        <div class="ui container" style="background: #fff;">
            <div class="ui internally grid web-blog">
                <div class="row">

                    <div class="twelve wide computer sixteen wide tablet sixteen wide mobile column left-channel">

                            <div class="newsview">
                                <div class="news_infos">
                                    <p>
                                        武继跃，男，一个80后IT码农！2012年毕业之后，做过洗衣厂工人，也做过酒店前台。在13年去了青鸟培训，因为大学是计算机专业有一定基础，从青鸟培训几个月后，正式开始了程序员的人生。<br>
                                        程序员是个吃青春饭的行业，虽然我入行比较晚。入了这一行，就深深的喜欢上它。我喜欢一句话“冥冥中该来则来，无处可逃”。<br>
                                        入行几年来，我一边工作，一遍不断学习获取新的知识，增所谓，“活到老，学到老”,而IT行业更加如此，技术更新迭代飞快，三天不学习就会落后!<br>
                                        学习的目的是实践，为了把学到的应用到实践，我把工作几年学习到的知识点整理成了一套个人博客系统（前端）、手机微信端H5
                                        APP（未完成）、以及Java编写的一套成熟的后台管理系统。<br>
                                        我的后台系统是springMVC+mybatis架构从零开始写的，麻雀虽小，五脏俱全。本后台系统可以实现的功能有：<br>
                                        <strong>系统模块：</strong>一键代码生成、自己设计的权限控制(使用SpringMVC的拦截器实现)、自己设计的会话管理，在线用户列表，踢出用户（登录时生成的token存放到t_sesion表中实现）简单的数据范围授权、读写连接池分离、统一异常处理、防止表单重复提交、日志管理、区域管理、省市区选择、通用文件上传管理、字典管理、组织目录管理、角色管理、系统用户管理、系统参数管理、EhCache缓存、SSO<br>
                                        <strong>内容管理模块：</strong>文章管理(博客系统)、书籍管理(博客系统)、书籍章节(博客系统)、评论管理(博客系统)、标签管理(博客系统)、资源分类管理、七牛文件上传管理、模板管理、站内邮件、时间轴等。
                                        <br>
                                        <strong>其他功能：</strong>邮件发送、webmagic爬虫、简单的审核流程、定时任务、JSON Web
                                        Token实现密码找回、封装百度人工智能API、微信公众号启用开发者模式等等。
                                        <br>
                                        <strong>补充：</strong>以上列出的并不是Java后台实现的全部功能,我本人能力和时间有限，有很多功能已经融合进去但并未完整实现使用，还有很多地方并不完美需要调整，就没有列出来。比如ansj中文分词、websocket后台向页面推送消息、ActiveMQ、微信H5页面实现的关于社区管理的Demo，等等。
                                        <br><font color="green" style="font-weight: 600">本系统打算上传开源中国、github免费供大家学习讨论，并十分欢迎大家踊跃参与它的开发</font>。
                                        <br>
                                        <br>

                                    <h2>About MyBlog</h2>
                                    &nbsp;
                                    <p>域 名：http://www.cmsyue.com</p>
                                    <br>

                                    <p>服务器：阿里云服务器&nbsp;&nbsp;<a
                                            href="https://promotion.aliyun.com/ntms/act/ambassador/sharetouser.html?userCode=8smrzoqa&amp;productCode=vm"
                                            target="_blank"><span
                                            style="color:#FF0000;"><strong>前往阿里云官网购买&gt;&gt;</strong></span></a></p>
                                    <br>

                                    <p>备案号：xxx</p>
                                    <br>

                                    <p>程 序：xxx</p>
                                </div>
                            </div>

                    </div>
                    <div class="four wide computer only  column">

                            <#include "/cms/myblog/semantic/block/aboutMe.ftl"/>
                            <div class="weixin">
                                <h2 class="hometitle">QQ交流群</h2>

                                <div class="ui list">
                                    <div class="item">
                                        <img src="/resources/myblog/images/qq_jlq.png">
                                    </div>
                                    <div class="item">
                                        <a style="position: relative;display: inline-block;top:-30px;left:70px;" target="_blank"
                                           href="//shang.qq.com/wpa/qunwpa?idkey=5ae33948cbf7c70af15befff5587d9d815930d3e84759a8895fe3a1b2e0a6424"><img
                                                border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="qq交流群"
                                                title="qq交流群"></a>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <!-- content end -->
            </div>
        </div>

    </div>
    <#include "/cms/myblog/semantic/block/footer.ftl"/>
    <#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>