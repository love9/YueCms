<!--手机试图导航-->
<div class="ui left inverted sidebar vertical menu" id="mobileNavSidebar" style="display: none;">
    <a class="item" href="/myblog/index">首页</a>
    <a class="item" href="/myblog/aboutMe">关于我</a>
    <a class="item" href="/myblog/bloglist">博客</a>

    <#if front_yhmc??>
   <!-- <a class="item" href="/myblog/meitu">关于我</a>-->
    <a class="item" href="/myblog/img">相册</a>
    </#if>
    <a id="nav_book" class="item" href="/myblog/booklist">读书</a>
   <!-- <a class="item" href="/myblog/article-fullwidth">全宽页面</a>-->
    <a class="item" href="/myblog/updateLog">更新日志</a>
    <a class="item" href="/myblog/demo/201807/liuyan">留言</a>
</div>
<div class="ui right inverted sidebar vertical menu" id="mobileNavRightSidebar" style="display: none;">
    <a class="item" href="/myblog/personalBlog">个人博客</a>
    <#--<a class="item" href="/myblog/admin/postlist">管理博客</a>-->
    <a class="item" href="/admin/login">登录后台</a>
    <a class="item" href="/myblog/logout">退出</a>
</div>