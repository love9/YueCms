
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>simple-switch</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <link rel="stylesheet" href="/resources/lib/simple-switch/jquery.simple-switch.css">
    <script type="text/javascript" src="/resources/lib/simple-switch/jquery.simple-switch.js"></script>

    <link href="http://cdn.bootcss.com/highlight.js/8.0/styles/monokai_sublime.min.css" rel="stylesheet">
    <script src="http://cdn.bootcss.com/highlight.js/8.0/highlight.min.js"></script>

    <style>
        /*本demo样式*/
        *
        {  -webkit-box-sizing: border-box;  -moz-box-sizing: border-box;  box-sizing: border-box  }
        :before,:after
        {  -webkit-box-sizing: border-box;  -moz-box-sizing: border-box;  box-sizing: border-box  }
        input[type="button"] {  background-color: #FFF;  border: solid 1px #8E8E8E;  padding: 10px 15px;  border-radius: 5px;  margin: 5px;  font-size: 14px;  }
    </style>
</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer  sixteen wide mobile column  ">

                  <h1>simple-switch示例：</h1>
                  <h2>开关1：</h2>
                  <div id="switch1" class="simple-switch">
                      <input type="checkbox" />
                      <span class="switch-handler"></span>
                  </div>
                  <h2>开关2：</h2>
                  <div id="switch2" class="simple-switch active">
                      <input type="checkbox" checked/>
                      <span class="switch-handler"></span>
                  </div>
                  <input type="button" value="切换所有开关状态" onclick="$('.simple-switch').simpleSwitch('toggle');"/>
                  <input type="button" value="打开开关一" onclick="$('#switch1').simpleSwitch('toggle', true);"/>
                  <input type="button" value="关闭开关一" onclick="$('#switch1').simpleSwitch('toggle', false);"/>
                  <input type="button" value="获取开关一状态" onclick="layer.msg('是否打开：'+$('#switch1').simpleSwitch('state'));"/>

                <script type="text/javascript">
                    // 监听开关2的状态
                    $('#switch2').on('switch-change', function(e)
                    {
                        layer.msg('开关2的状态：'+$('#switch2').simpleSwitch('state'));
                    });
                    $(function(){
                        var html=$("#data_css").html();
                        var data=escapeHTML(html);
                        $("#code_css").html(data);
                        var html=$("#data_js").html();
                        var data=escapeHTML(html);
                        $("#code_js").html(data);
                        $('pre code').each(function(i, block) {
                            hljs.highlightBlock(block);
                        });
                    })
                    function escapeHTML(html) {
                        return html.replace(/</g, "&lt").replace(/>/g, "&gt").replace(/ /g, " ").replace(/"/g, "\"").replace(/'/g, "'");
                    }
                </script>

                  <div class="ui raised segment">
                      <a class="ui red ribbon label">css</a>
                          <xmp style="display: none;" id="data_css">
/**
* 简单的开关插件
*/
.simple-switch {width: 70px;height: 32px;
border: solid 2px #ddd;
border-radius: 30px;
background-color: #FFF;position: relative;padding-left: 28px;
-webkit-transition: background-color 0.3s;
transition: background-color 0.3s;
-webkit-user-select: none;
font-size: 14px;
}
.simple-switch > input[type="checkbox"] {
 display: none;
 }
.simple-switch:before {
content: '关';
text-align: center;
display: block;
width: 100%;
height: 100%;
line-height: 28px;
color: #999;
}
.simple-switch > .switch-handler {
position: absolute;
left: 2px;
top: 0px;
width: 28px;
height: 28px;
background-color: #FFF;
border-radius: 50%;
-webkit-box-shadow: 1px 2px 5px rgba(0, 0, 0, 0.52);
box-shadow: 1px 2px 5px rgba(0, 0, 0, 0.52);
-webkit-transition: all 0.3s;
transition: all 0.3s;
}
.simple-switch.active {
border-color: #4cd964;
background-color: #4cd964;
padding-left: 0;
padding-right: 28px;
}
.simple-switch.active:before {
content: '开';
color: #FFF;
}
.simple-switch.active > .switch-handler {
left: 38px;
}

                          </xmp>
                        <pre>
                        <code id="code_css">

                        </code>
                        </pre>

                  </div>
                  <div class="ui raised segment">
                      <a class="ui red ribbon label">js</a>
<xmp style="display: none;" id="data_js">
    /**
    * 简单的开关插件<br>
    * 切换开关状态：$('#switchId').simpleSwitch('toggle');
    * 打开开关：$('#switchId').simpleSwitch('toggle', true);
    * 关闭开关：$('#switchId').simpleSwitch('toggle', false);
    * 获取开关状态：$('#switchId').simpleSwitch('state');
    * 事件监听：$('#switchId').on('switch-change', function(e){console.log(e.detail);});
    * @date 2016-06-25
    * @author lxa
    */
    ;(function($)
    {
    $.fn.extend(
    {
    simpleSwitch: function(method, value)
    {
    var that = this;
    method = method || 'init';
    function toggle(flag)
    {
    that.each(function()
    {
    var isActive = $(this).hasClass('active');
    if(flag != undefined && flag == isActive) return;
    var value = flag == undefined ? !isActive : flag;
    $(this).toggleClass('active', value);
    $(this).children('input[type="checkbox"]').prop('checked', value);
    var event = document.createEvent("CustomEvent");
    event.initCustomEvent("switch-change", true, true, value);
    this.dispatchEvent(event);
    });
    }
    if(method == 'state') return this.children('input[type="checkbox"]').prop('checked');
    else if(method == 'toggle') toggle(value);
    else if(method == 'init') // 初始化
    {
    this.each(function()
    {
    var checked = $(this).children('input[type="checkbox"]').prop('checked');
    $(this).toggleClass('active', checked)
    .append('<span class="switch-handler"></span>');
    })
    }
    else if(method == 'bindEvent') // 绑定事件
    {
    this.on('click', function()
    {
    $(this).simpleSwitch('toggle');
    });
    }
    return this;
    }
    });
    $(function()
    {
    // 由于初始化效果不太好，暂时屏蔽
    //$('.simple-switch').simpleSwitch('init');
    $('.simple-switch').simpleSwitch('bindEvent');
    });
    })(jQuery);

</xmp>
                        <pre>
                        <code id="code_js">

                        </code>
                        </pre>
                  </div>

              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>