
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="shortcut icon" href="/favicon.ico">

    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>
<body style="overflow: hidden;background-color:#f4f4f4 ">

<div class="ui middle aligned center aligned container" style="border: 1px solid #fff;margin-top:130px;width: 50%;background-color: #fff;padding:50px 0;">
    <div class="column">
        <h1 class="blog-title"> ${title} </h1>
        <div><i style="color:green;" class="check circle outline icon"></i><h1 style="display: inline-block;">&nbsp;发布成功</h1></div>
    </div>
    <div class="column" style="margin-top: 1rem;">
        <a  class="ui button blue" href="/myblog/newblog"><i class="edit icon"></i>&nbsp;写新文章</a>
        <a  class="ui button green" href="/myblog/article/detail/${id}"><i class="newspaper icon"></i>&nbsp;查看文章</a>
    </div>
</div>


</body>
</html>