
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>semantic</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>
  <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>
<body  style="background: #e9ecf3 !important;" class="pushable">


<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>

<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" >
<div class="ui container grid" >
<div class="row" >
<div class="sixteen wide tablet ten wide computer column" style="height: calc(100% - 1rem);">
<div class="ui divided items" style="padding:1rem;background: #fff;">

<#if articles??>
    <#list articles.rows as item>

    <div class="item">

        <#if item.cover_image??>
            <div class="ui medium image">
                <img src="${item.cover_image!}">
            </div>
        </#if>
        <div class="content">
            <a class="header" href="/myblog/article/detail/${item.id!}">${item.title!}</a>
            <div class="meta">
                <span class="cinema"></span>
            </div>
            <div class="description">
                <p>${item.description!}</p>
            </div>
            <div class="extra">
                <div class="ui  horizontal list article_info">
                    <div class="item">
                        <span class="icon_tag am-fl"><a href="javascript:;" title="个人博客" target="_blank" class="classname">个人博客</a></span>
                    </div>
                    <div class="item">
                        <span class="dtime am-fl">${item.createTime!}</span>
                    </div>
                    <div class="item">
                        <span class="viewnum am-fr">浏览（<a href="javascript:;">${item.hit!}</a>）</span>
                    </div>
                    <div class="item">
                        <span class="pingl am-fr">评论（ ${item.reply_num!} ）</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </#list>
</#if>
    <div class="ui divider"></div>
    <div style="position:relative;bottom:0px;text-align: center;" id="pagebar" class="pagebar"></div>
</div>


        <script type="text/javascript">
            if($(window).width()<975){
                $("#mainScreen .ui.container").removeClass("grid");
            }else{
                $("#mainScreen .ui.container").addClass("grid");
            }
            $(function(){
                $(window).resize(function() {

                    if($(window).width()<975){
                        $("#mainScreen .ui.container").removeClass("grid");
                    }else{
                        $("#mainScreen .ui.container").addClass("grid");
                    }
                });
            })
            var pageNo=${articles.page!};
            var pageSize=10;
            var totalPages =${articles.totalPages!};
            initPager();
            function initPager() {
                var $pagebar = $("#pagebar");
                laypage({
                    cont: $pagebar, //容器。值支持id名、原生dom对象，jquery对象,
                    pages: totalPages, //总页数
                    curr: pageNo,
                    jump:jumpPage,
                    skin: 'molv', //皮肤
                    first: '首页', //若不显示，设置false即可
                    last: '尾页', //若不显示，设置false即可
                    prev: '上一页', //若不显示，设置false即可
                    next: '下一页' //若不显示，设置false即可
                });
            }

            function jumpPage(obj,first){
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                //alert("共"+context.option.pages+"页，当前第"+context.option.curr+"页");
                pageNo=obj.curr;
                //alert(pageNo);
                if(!first){
                    var uri=Fast.getUri();
                    window.location.href=uri+"?page="+pageNo+"&rows="+pageSize+"&myblog=1";
                }
            }

        </script>

</div>
    <div class="sixteen wide tablet six wide computer column"  style="">
        <div class="ui raised center aligned segment  user-info" style="background: #fff;margin: 0 0 1rem 0;">

            <div class="avatar-wrap" style="width: 120px;height: 120px!important;position: relative;margin: 0 auto;">
                <a class="ui small circular image avatar" href="#">
                    <div class="osc-avatar"  style="height: 120px;width: 120px;">
                        <#if author_info.headerPath?? >
                            <img style="height: 120px;width: 120px;" src="${author_info.headerPath!}">
                        <#else>
                            <img style="height: 120px;width: 120px;" src="/resources/images/headicons/girl-1.png">
                        </#if>
                    </div>
                </a>
                <div class="gender ui large teal circular label" style="position: absolute; bottom: 0;right: 10px;z-index: 999!important;">
                    <i class="mars icon" style="margin: 0!important;"></i>
                </div>
            </div>
            <div class="ui header">
                <h3>${author_info.name!}</h3>
            </div>
            <a style="margin-bottom: 14px;" class="ui basic tiny button" href="#"><i class="settings icon"></i>帐号设置</a>
            <div class="ui mini circular images medals">
            </div>
            <div class="ui four tiny statistics user-statistics self-space">

                <a class="statistic" href="https://www.oschina.net/question/2918182_2268983" target="_blank">
                    <div class="value">
                        ${author_info.num_like!}
                    </div>
                    <div class="label">
                        喜欢
                    </div>
                </a>
                <a class="statistic" href="https://my.oschina.net/u/3182546/followers">
                    <div class="value">
                        ${author_info.num_fans!}
                    </div>
                    <div class="label">
                        粉丝
                    </div>
                </a>
                <a class="statistic" href="https://my.oschina.net/u/3182546/following">
                    <div class="value">
                        0
                    </div>
                    <div class="label">
                        关注
                    </div>
                </a>

                <a class="statistic" href="https://my.oschina.net/u/3182546/favorites">
                    <div class="value">
                        0
                    </div>
                    <div class="label">
                        收藏
                    </div>
                </a>
            </div>
            <div class="item" style="margin-top: 14px;text-align: center;">
                <a class="ui green small button" href="#"><i class="edit icon"></i><span class="text">写博客</span></a>
                <a class="ui basic small button" href="#"><i class="folder outline icon"></i><span class="text">草稿箱 (0)</span></a>
            </div>
        </div>

        <div class="ui raised segment "  style="background: #fff;margin: 0 0 1rem 0;padding: 1rem 2rem;">
            <h3 class="header" style="margin-left: -1rem;"><a class="ui red ribbon label">统计</a></h3>
            <div class="ui list">
                <div class="item">今日访问：${author_info.visit_today!}</div>
                <div class="item">昨日访问：${author_info.visit_yesterday!}</div>
                <div class="item">本周访问：${author_info.visit_week!}</div>
                <div class="item">本月访问：${author_info.visit_month!}</div>
                <div class="item">所有访问：${author_info.visit_total!}</div>
                <div class="item">加入时间：${author_info.join_time!}</div>
                <div class="item">最近登录：${author_info.last_login_time!}</div>
            </div>
        </div>

<#--个人分类start-->
    <#if personal_categorys??>
        <div class="ui raised segment"  style="background: #fff;margin: 0 0 1rem 0;padding: 1rem 2rem;" id="personalCategory">
            <style>
                #personalCategory .ui.list .item{line-height: 1.5rem;}
                #personalCategory .ui.list a.item {color:#333;}
                #personalCategory .ui.list a.item:hover {color: #ca0c16 !important;}
                #personalCategory .ui.list .item span{display:block;float: right;font-size: 12px;color: #858585;}
            </style>
            <h3 class="header" style="/*border-left: 3px solid #cf2730;    padding-left: 6px;*/margin-left: -1rem;"><a class="ui red ribbon label">个人分类</a></h3>
            <div class="ui list">
                <#list personal_categorys as ca>
                    <a class="item" href="/myblog/personalBlog/category/${ca.id!}">${ca.name!}<span>1篇</span></a>
                </#list>
            </div>
            <script type="text/javascript">
                $(function(){
                    var url=  window.location.href;
                    var host=window.location.host;
                    var protocol=window.location.protocol;
                    var uri=url.replace(protocol+"//"+host,"");
                    $.each($("#personalCate a.blog-tag"),function(i,item){
                        if(uri==$(item).attr("href")){
                            $(item).addClass("am-active").siblings().removeClass("am-active");
                            $(item).attr("href","/myblog/personalBlog");
                        }
                    });
                })
            </script>
        </div>
    </#if>
<#--个人分类end-->
<!--标签云 start-->
<#if tags??>
        <div class="ui  raised  segment"  style="background: #fff;margin: 0 0 1rem 0;padding: 1rem 2rem;" id="yunTags">
            <h3 class="header" style="margin-left: -1rem;"> <a class="ui red ribbon label">云标签</a></h3>
              <#list tags as tag>
                  <#if tag_index%13==1>
                      <#assign css="red">
                  <#elseif tag_index%13==2>
                      <#assign css="orange">
                  <#elseif tag_index%13==3>
                      <#assign css="yellow">
                  <#elseif tag_index%13==4>
                      <#assign css="olive">
                  <#elseif tag_index%13==5>
                      <#assign css="green">
                  <#elseif tag_index%13==6>
                      <#assign css="teal">
                  <#elseif tag_index%13==7>
                      <#assign css="blue">
                  <#elseif tag_index%13==8>
                      <#assign css="violet">
                  <#elseif tag_index%13==9>
                      <#assign css="purple">
                  <#elseif tag_index%13==10>
                      <#assign css="pink">
                  <#elseif tag_index%13==11>
                      <#assign css="brown">
                  <#elseif tag_index%13==12>
                      <#assign css="grey">
                  <#elseif tag_index%13==13>
                      <#assign css="black">
                  </#if>
                  <a class="ui ${css!} basic label"  data-id="${tag.id!}" style="margin-top: 5px;">
                      ${tag.name!}
                      <div class="detail">${tag_index!}</div>
                  </a>
              </#list>

        </div>
</#if>
<!--标签云 end-->


    </div>
</div>
</div>
</div>
<!-- content end -->
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>