
<#if recommends??>
<div class="blog-sidebar-widget" style="margin:1rem -1.5rem;">
    <h3 class="blog-sidebar-widget-title floated left">
        <b>精选</b>
        <div style="display: inline-block;float: right;margin-right: 20px;">
            <a href="#" style="color:#666;font-size: 10px;">更多&gt;&gt;</a>
        </div>
    </h3>
    <div class="blog-sidebar-widget-content" id="">

        <div class="ui divided items" style="padding:1rem;background: #fff;">

<#list recommends as item>
        <div class="item">

            <#if item.cover_image?? && item.cover_image!="">
                <div class="ui medium image">
                    <img src="${item.cover_image!}">
                </div>
            </#if>
            <div class="content">

                <#if item.static_url?? && item.static_url!="">
                    <a class="header" href="${item.static_url!}">${item.title!}</a>
                <#else>
                    <a class="header" href="/myblog/article/detail/${item.id!}">${item.title!}</a>
                </#if>

                <div class="meta">
                    <span class="cinema"></span>
                </div>
                <div class="description">
                    <p>${item.description!}</p>
                </div>
                <div class="extra">
                    <div class="ui  horizontal list article_info">
                        <div class="item">
                            <span class="icon_tag am-fl"><a href="javascript:;" title="个人博客" target="_blank" class="classname">个人博客</a></span>
                        </div>
                        <div class="item">
                            <span class="dtime am-fl">${item.createTime!}</span>
                        </div>
                        <div class="item">
                            <span class="viewnum am-fr">浏览（<a href="javascript:;">${item.hit!}</a>）</span>
                        </div>
                        <div class="item">
                            <span class="pingl am-fr">评论（ ${item.reply_num!} ）</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
</#list>
        </div>

</div>
</div>
</#if>