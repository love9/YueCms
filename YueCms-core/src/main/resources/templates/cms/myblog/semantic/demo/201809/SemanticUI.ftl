
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>SemanticUI</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
 <!-- <link rel="stylesheet" href="/resources/SemanticUI/components/dropdown.min.css">-->
  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>
  <script src="/resources/SemanticUI/components/dropdown.min.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script>
  $(function(){
    $('.ui.accordion').accordion();
    $('.ui.dropdown').dropdown();
    //$('.menu .item') .tab();
   // $("#publishTabList .item").tab();
  })


</script>
</head>
<body id="demo">
<a class="ui blue basic label">Blue</a>
<a class="ui blue   label">Blue</a>
<div class="ui teal buttons">
  <div class="ui button">保存</div>
  <div class="ui floating dropdown icon button" tabindex="0">
    <i class="dropdown icon"></i>
    <div class="menu transition hidden" tabindex="-1">
      <div class="item"><i class="edit icon"></i> 编辑帖子</div>
      <div class="item"><i class="delete icon"></i> 删除帖子</div>
      <div class="item"><i class="hide icon"></i> 隐藏帖子</div>
    </div>
  </div>
</div>
<div class="ui vertical accordion menu">
  <div class="item">
    <a class="title active">
      <i class="dropdown icon"></i>
      尺寸
    </a>
    <div class="content active">
      <div class="ui form transition visible" style="display: block !important;">
        <div class="grouped fields">
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="size" value="small" tabindex="0" class="hidden">
              <label>小</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="size" value="medium" tabindex="0" class="hidden">
              <label>中</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="size" value="large" tabindex="0" class="hidden">
              <label>大</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox checked">
              <input type="radio" name="size" value="x-large" tabindex="0" class="hidden">
              <label>巨大</label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="ui floating labeled icon dropdown button active visible"  >
  <i class="add user icon"></i>
  <span class="text">添加用户</span>
  <div class="menu transition visible"  style="display: none !important;">

    <div class="item">
      <a href="#"> <img class="ui avatar image" src="../images/avatar/small/jenny.jpg">
        珍妮赫斯</a>

    </div>
    <div class="item">
      <img class="ui avatar image" src="../images/avatar/small/elliot.jpg">
      埃利奥特赋
    </div>
    <div class="item">
      <img class="ui avatar image" src="../images/avatar/small/stevie.jpg">
      史蒂夫
    </div>
  </div>
</div>
<div class="ui basic segment" id="mainScreen">
  <div class="ui container">



    <div class="centered row">

<div class="sixteen wide large screen sixteen wide tablet column body-container">
  <div class="ui red pointing secondary massive menu" id="publishTabList">
    <a class="item" data-tab="publish-news">投递新闻</a>
    <a class="item" data-tab="publish-project">添加软件</a>
    <a class="item active" data-tab="publish-link">投递新闻链接</a>
    <div class="item right help mb-hide"><a href="https://www.oschina.net/question/2918182_2266982" target="_blank" style="font-size:1rem">查看添加软件操作指南，让您更快通过审核</a></div>
  </div>
  <div class="ui tab basic segment" data-tab="publish-news">
    <div class="ui info message">您投递的新闻经过编辑审核通过后将在 <a href="https://www.oschina.net/news" target="_blank">资讯页面</a> 显示</div>
    <form class="ui publish-news form">
      <div class="field">
        <label>选择分类</label>
        <div class="four wide field">
          <div class="ui dropdown selection" tabindex="0"><select name="type">
            <option value="1">软件更新资讯</option>
            <option value="2">综合资讯</option>
          </select><i class="dropdown icon"></i><div class="text">软件更新资讯</div><div class="menu" tabindex="-1"><div class="item active selected" data-value="1">软件更新资讯</div><div class="item" data-value="2">综合资讯</div></div></div>
        </div>
      </div>
      <div class="two fields">
        <div class="required ten wide field">
          <label>新闻标题</label>
          <input type="text" name="title" placeholder="新闻标题，字数控制在 50 个字以内">
        </div>
        <div class="six wide field">
          <label>关联软件</label>
          <div class="ui fluid search selection dropdown project-name">
            <input type="hidden" name="projectId">
            <input type="text" class="search" tabindex="0">
            <div class="default text">关联软件</div>
            <div class="menu" tabindex="-1"></div></div>
        </div>
      </div>
      <div class="required field">
        <label>新闻内容</label>
        <textarea name="newsDetail" class="ckeditor" style="visibility: hidden; display: none;"></textarea><div id="cke_newsDetail" class="cke_1 cke cke_reset cke_chrome cke_editor_newsDetail cke_ltr cke_browser_webkit" dir="ltr" lang="zh-cn" role="application" aria-labelledby="cke_newsDetail_arialbl"><span id="cke_newsDetail_arialbl" class="cke_voice_label">所见即所得编辑器, newsDetail</span><div class="cke_inner cke_reset" role="presentation"><span id="cke_1_top" class="cke_top cke_reset_all" role="presentation" style="height: auto; user-select: none;"><span id="cke_14" class="cke_voice_label">工具栏</span><span id="cke_1_toolbox" class="cke_toolbox" role="group" aria-labelledby="cke_14" onmousedown="return false;"><span id="cke_16" class="cke_toolbar" aria-labelledby="cke_16_label" role="toolbar"><span id="cke_16_label" class="cke_voice_label">基本格式</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_17" class="cke_button cke_button__bold cke_button_off" href="javascript:void('加粗')" title="加粗 (Ctrl+B)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_17_label" aria-describedby="cke_17_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(2,event);" onfocus="return CKEDITOR.tools.callFunction(3,event);" onclick="CKEDITOR.tools.callFunction(4,this);return false;"><span class="cke_button_icon cke_button__bold_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -24px;background-size:auto;">&nbsp;</span><span id="cke_17_label" class="cke_button_label cke_button__bold_label" aria-hidden="false">加粗</span><span id="cke_17_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+B</span></a><a id="cke_18" class="cke_button cke_button__italic cke_button_off" href="javascript:void('倾斜')" title="倾斜 (Ctrl+I)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_18_label" aria-describedby="cke_18_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(5,event);" onfocus="return CKEDITOR.tools.callFunction(6,event);" onclick="CKEDITOR.tools.callFunction(7,this);return false;"><span class="cke_button_icon cke_button__italic_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -48px;background-size:auto;">&nbsp;</span><span id="cke_18_label" class="cke_button_label cke_button__italic_label" aria-hidden="false">倾斜</span><span id="cke_18_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+I</span></a><a id="cke_19" class="cke_button cke_button__underline cke_button_off" href="javascript:void('下划线')" title="下划线 (Ctrl+U)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_19_label" aria-describedby="cke_19_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(8,event);" onfocus="return CKEDITOR.tools.callFunction(9,event);" onclick="CKEDITOR.tools.callFunction(10,this);return false;"><span class="cke_button_icon cke_button__underline_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -144px;background-size:auto;">&nbsp;</span><span id="cke_19_label" class="cke_button_label cke_button__underline_label" aria-hidden="false">下划线</span><span id="cke_19_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+U</span></a><a id="cke_20" class="cke_button cke_button__strike cke_button_off" href="javascript:void('删除线')" title="删除线" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_20_label" aria-describedby="cke_20_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(11,event);" onfocus="return CKEDITOR.tools.callFunction(12,event);" onclick="CKEDITOR.tools.callFunction(13,this);return false;"><span class="cke_button_icon cke_button__strike_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -72px;background-size:auto;">&nbsp;</span><span id="cke_20_label" class="cke_button_label cke_button__strike_label" aria-hidden="false">删除线</span><span id="cke_20_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_21" class="cke_toolbar" aria-labelledby="cke_21_label" role="toolbar"><span id="cke_21_label" class="cke_voice_label">链接</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_22" class="cke_button cke_button__link cke_button_off" href="javascript:void('插入/编辑超链接')" title="插入/编辑超链接 (Ctrl+L)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_22_label" aria-describedby="cke_22_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(14,event);" onfocus="return CKEDITOR.tools.callFunction(15,event);" onclick="CKEDITOR.tools.callFunction(16,this);return false;"><span class="cke_button_icon cke_button__link_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -528px;background-size:auto;">&nbsp;</span><span id="cke_22_label" class="cke_button_label cke_button__link_label" aria-hidden="false">插入/编辑超链接</span><span id="cke_22_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+L</span></a><a id="cke_23" class="cke_button cke_button__unlink cke_button_disabled " href="javascript:void('取消超链接')" title="取消超链接" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_23_label" aria-describedby="cke_23_description" aria-haspopup="false" aria-disabled="true" onkeydown="return CKEDITOR.tools.callFunction(17,event);" onfocus="return CKEDITOR.tools.callFunction(18,event);" onclick="CKEDITOR.tools.callFunction(19,this);return false;"><span class="cke_button_icon cke_button__unlink_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -552px;background-size:auto;">&nbsp;</span><span id="cke_23_label" class="cke_button_label cke_button__unlink_label" aria-hidden="false">取消超链接</span><span id="cke_23_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_24" class="cke_toolbar" aria-labelledby="cke_24_label" role="toolbar"><span id="cke_24_label" class="cke_voice_label">插入</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_25" class="cke_button cke_button__image cke_button_off" href="javascript:void('图像')" title="图像" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_25_label" aria-describedby="cke_25_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(20,event);" onfocus="return CKEDITOR.tools.callFunction(21,event);" onclick="CKEDITOR.tools.callFunction(22,this);return false;"><span class="cke_button_icon cke_button__image_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -360px;background-size:auto;">&nbsp;</span><span id="cke_25_label" class="cke_button_label cke_button__image_label" aria-hidden="false">图像</span><span id="cke_25_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_26" class="cke_button cke_button__flash cke_button_off" href="javascript:void('Flash')" title="Flash" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_26_label" aria-describedby="cke_26_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(23,event);" onfocus="return CKEDITOR.tools.callFunction(24,event);" onclick="CKEDITOR.tools.callFunction(25,this);return false;"><span class="cke_button_icon cke_button__flash_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1080px;background-size:auto;">&nbsp;</span><span id="cke_26_label" class="cke_button_label cke_button__flash_label" aria-hidden="false">Flash</span><span id="cke_26_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_27" class="cke_button cke_button__table cke_button_off" href="javascript:void('表格')" title="表格" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_27_label" aria-describedby="cke_27_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(26,event);" onfocus="return CKEDITOR.tools.callFunction(27,event);" onclick="CKEDITOR.tools.callFunction(28,this);return false;"><span class="cke_button_icon cke_button__table_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -912px;background-size:auto;">&nbsp;</span><span id="cke_27_label" class="cke_button_label cke_button__table_label" aria-hidden="false">表格</span><span id="cke_27_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_28" class="cke_toolbar" aria-labelledby="cke_28_label" role="toolbar"><span id="cke_28_label" class="cke_voice_label">颜色</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_29" class="cke_button cke_button__textcolor cke_button_off" href="javascript:void('文本颜色')" title="文本颜色" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_29_label" aria-describedby="cke_29_description" aria-haspopup="true" onkeydown="return CKEDITOR.tools.callFunction(29,event);" onfocus="return CKEDITOR.tools.callFunction(30,event);" onclick="CKEDITOR.tools.callFunction(31,this);return false;"><span class="cke_button_icon cke_button__textcolor_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1128px;background-size:auto;">&nbsp;</span><span id="cke_29_label" class="cke_button_label cke_button__textcolor_label" aria-hidden="false">文本颜色</span><span id="cke_29_description" class="cke_button_label" aria-hidden="false"></span><span class="cke_button_arrow"></span></a><a id="cke_30" class="cke_button cke_button__bgcolor cke_button_off" href="javascript:void('背景颜色')" title="背景颜色" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_30_label" aria-describedby="cke_30_description" aria-haspopup="true" onkeydown="return CKEDITOR.tools.callFunction(32,event);" onfocus="return CKEDITOR.tools.callFunction(33,event);" onclick="CKEDITOR.tools.callFunction(34,this);return false;"><span class="cke_button_icon cke_button__bgcolor_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1104px;background-size:auto;">&nbsp;</span><span id="cke_30_label" class="cke_button_label cke_button__bgcolor_label" aria-hidden="false">背景颜色</span><span id="cke_30_description" class="cke_button_label" aria-hidden="false"></span><span class="cke_button_arrow"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_31" class="cke_toolbar" aria-labelledby="cke_31_label" role="toolbar"><span id="cke_31_label" class="cke_voice_label">段落</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_32" class="cke_button cke_button__numberedlist cke_button_off" href="javascript:void('编号列表')" title="编号列表" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_32_label" aria-describedby="cke_32_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(35,event);" onfocus="return CKEDITOR.tools.callFunction(36,event);" onclick="CKEDITOR.tools.callFunction(37,this);return false;"><span class="cke_button_icon cke_button__numberedlist_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -648px;background-size:auto;">&nbsp;</span><span id="cke_32_label" class="cke_button_label cke_button__numberedlist_label" aria-hidden="false">编号列表</span><span id="cke_32_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_33" class="cke_button cke_button__bulletedlist cke_button_off" href="javascript:void('项目列表')" title="项目列表" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_33_label" aria-describedby="cke_33_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(38,event);" onfocus="return CKEDITOR.tools.callFunction(39,event);" onclick="CKEDITOR.tools.callFunction(40,this);return false;"><span class="cke_button_icon cke_button__bulletedlist_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -600px;background-size:auto;">&nbsp;</span><span id="cke_33_label" class="cke_button_label cke_button__bulletedlist_label" aria-hidden="false">项目列表</span><span id="cke_33_description" class="cke_button_label" aria-hidden="false"></span></a><span class="cke_toolbar_separator" role="separator"></span><a id="cke_34" class="cke_button cke_button__justifyleft cke_button_on" href="javascript:void('左对齐')" title="左对齐" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_34_label" aria-describedby="cke_34_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(41,event);" onfocus="return CKEDITOR.tools.callFunction(42,event);" onclick="CKEDITOR.tools.callFunction(43,this);return false;" aria-pressed="true"><span class="cke_button_icon cke_button__justifyleft_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1200px;background-size:auto;">&nbsp;</span><span id="cke_34_label" class="cke_button_label cke_button__justifyleft_label" aria-hidden="false">左对齐</span><span id="cke_34_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_35" class="cke_button cke_button__justifycenter cke_button_off" href="javascript:void('居中')" title="居中" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_35_label" aria-describedby="cke_35_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(44,event);" onfocus="return CKEDITOR.tools.callFunction(45,event);" onclick="CKEDITOR.tools.callFunction(46,this);return false;"><span class="cke_button_icon cke_button__justifycenter_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1176px;background-size:auto;">&nbsp;</span><span id="cke_35_label" class="cke_button_label cke_button__justifycenter_label" aria-hidden="false">居中</span><span id="cke_35_description" class="cke_button_label" aria-hidden="false"></span></a><span class="cke_toolbar_separator" role="separator"></span><a id="cke_36" class="cke_button cke_button__blockquote cke_button_off" href="javascript:void('块引用')" title="块引用" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_36_label" aria-describedby="cke_36_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(47,event);" onfocus="return CKEDITOR.tools.callFunction(48,event);" onclick="CKEDITOR.tools.callFunction(49,this);return false;"><span class="cke_button_icon cke_button__blockquote_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -168px;background-size:auto;">&nbsp;</span><span id="cke_36_label" class="cke_button_label cke_button__blockquote_label" aria-hidden="false">块引用</span><span id="cke_36_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_37" class="cke_button cke_button__codesnippet cke_button_off" href="javascript:void('插入代码段')" title="插入代码段" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_37_label" aria-describedby="cke_37_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(50,event);" onfocus="return CKEDITOR.tools.callFunction(51,event);" onclick="CKEDITOR.tools.callFunction(52,this);return false;"><span class="cke_button_icon cke_button__codesnippet_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1056px;background-size:auto;">&nbsp;</span><span id="cke_37_label" class="cke_button_label cke_button__codesnippet_label" aria-hidden="false">插入代码段</span><span id="cke_37_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_38" class="cke_toolbar" aria-labelledby="cke_38_label" role="toolbar"><span id="cke_38_label" class="cke_voice_label">样式</span><span class="cke_toolbar_start"></span><span id="cke_15" class="cke_combo cke_combo__format cke_combo_off" role="presentation"><span id="cke_15_label" class="cke_combo_label">格式</span><a class="cke_combo_button" title="格式" tabindex="-1" href="javascript:void('格式')" hidefocus="true" role="button" aria-labelledby="cke_15_label" aria-haspopup="true" onkeydown="return CKEDITOR.tools.callFunction(54,event,this);" onfocus="return CKEDITOR.tools.callFunction(55,event);" onclick="CKEDITOR.tools.callFunction(53,this);return false;"><span id="cke_15_text" class="cke_combo_text">普通</span><span class="cke_combo_open"><span class="cke_combo_arrow"></span></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_39" class="cke_toolbar cke_toolbar_last" aria-labelledby="cke_39_label" role="toolbar"><span id="cke_39_label" class="cke_voice_label">工具</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_40" class="cke_button cke_button__source cke_button_off" href="javascript:void('源码')" title="源码" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_40_label" aria-describedby="cke_40_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(56,event);" onfocus="return CKEDITOR.tools.callFunction(57,event);" onclick="CKEDITOR.tools.callFunction(58,this);return false;"><span class="cke_button_icon cke_button__source_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -840px;background-size:auto;">&nbsp;</span><span id="cke_40_label" class="cke_button_label cke_button__source_label" aria-hidden="false">源码</span><span id="cke_40_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_41" class="cke_button cke_button__maximize cke_button_off" href="javascript:void('全屏')" title="全屏" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_41_label" aria-describedby="cke_41_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(59,event);" onfocus="return CKEDITOR.tools.callFunction(60,event);" onclick="CKEDITOR.tools.callFunction(61,this);return false;"><span class="cke_button_icon cke_button__maximize_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -672px;background-size:auto;">&nbsp;</span><span id="cke_41_label" class="cke_button_label cke_button__maximize_label" aria-hidden="false">全屏</span><span id="cke_41_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span></span></span><div id="cke_1_contents" class="cke_contents cke_reset" role="presentation" style="height: 350px;"><span id="cke_46" class="cke_voice_label">按 ALT+0 获得帮助</span><iframe src="" frameborder="0" class="cke_wysiwyg_frame cke_reset" title="所见即所得编辑器, newsDetail" aria-describedby="cke_46" tabindex="0" allowtransparency="true" style="width: 100%; height: 100%;"></iframe></div><span id="cke_1_bottom" class="cke_bottom cke_reset_all" role="presentation" style="user-select: none;"><span id="cke_1_resizer" class="cke_resizer cke_resizer_vertical cke_resizer_ltr" title="拖拽以改变大小" onmousedown="CKEDITOR.tools.callFunction(0, event)">◢</span><span id="cke_1_path_label" class="cke_voice_label">元素路径</span><span id="cke_1_path" class="cke_path" role="group" aria-labelledby="cke_1_path_label"><a id="cke_elementspath_11_1" href="javascript:void('body')" tabindex="-1" class="cke_path_item" title="body 元素" hidefocus="true" onkeydown="return CKEDITOR.tools.callFunction(63,1, event );" onclick="CKEDITOR.tools.callFunction(62,1); return false;" role="button" aria-label="body 元素">body</a><a id="cke_elementspath_11_0" href="javascript:void('p')" tabindex="-1" class="cke_path_item" title="p 元素" hidefocus="true" onkeydown="return CKEDITOR.tools.callFunction(63,0, event );" onclick="CKEDITOR.tools.callFunction(62,0); return false;" role="button" aria-label="p 元素">p</a><span class="cke_path_empty">&nbsp;</span></span></span></div></div>
      </div>
      <div class="two fields">
        <div class="four wide field">
          <label>新闻出处</label>
          <input type="text" name="original_name" placeholder="新闻出处">
        </div>
        <div class="twelve wide field">
          <label>原文链接</label>
          <input type="text" name="original_url" placeholder="原文链接">
        </div>
      </div>
      <div class="inline fields">
        <label>请选择新闻的评论权限</label>
        <div class="field">
          <div class="ui radio checkbox">
            <input type="radio" id="allReply" name="flag" value="0" tabindex="0" class="hidden" checked="">
            <label for="allReply">允许评论</label>
          </div>
        </div>
        <div class="field">
          <div class="ui radio checkbox">
            <input type="radio" id="userReply" name="flag" value="2" tabindex="1" class="hidden">
            <label for="userReply">只允许注册用户评论</label>
          </div>
        </div>
        <div class="field">
          <div class="ui radio checkbox">
            <input type="radio" id="banReply" name="flag" value="1" tabindex="2" class="hidden">
            <label for="banReply">禁止评论</label>
          </div>
        </div>
      </div>
      <div class="ui positive mini message osc hidden"></div>
      <div class="ui negative mini message osc hidden"></div>
      <div class="ui primary submit large submit button">投递新闻</div>
    </form>
  </div>
  <div class="ui tab basic segment" data-tab="publish-project">
    <div class="ui info message">
      <div class="header">友情提示</div>
      <ul class="list">
        <li>您添加的软件必须是开源的，而且本站尚未收录的</li>
        <li>您添加的软件经过编辑审核通过后，将收录到 <a href="https://www.oschina.net/project" target="_blank">开源中国软件库</a></li>
        <li>软件源码托管到 <a href="https://gitee.com" target="_blank">码云Gitee</a> 平台可优先获得编辑推荐</li>
      </ul>
    </div>
    <form class="ui publish-project form">
      <div class="required field">
        <label>软件名称</label>
        <input type="text" name="name" placeholder="软件名称，字数控制在 50 个字以内">
      </div>
      <div class="required field">
        <label>软件网址</label>
        <input type="text" name="home_url" placeholder="软件网址">
      </div>
      <div class="two fields">
        <div class="field">
          <label>下载地址</label>
          <input type="text" name="download_url" placeholder="下载地址">
        </div>
        <div class="field">
          <label>文档地址</label>
          <input type="text" name="doc_url" placeholder="文档地址">
        </div>
      </div>
      <div class="required field">
        <label>详细介绍</label>
        <textarea name="projectDetail" class="ckeditor" style="visibility: hidden; display: none;"></textarea><div id="cke_projectDetail" class="cke_2 cke cke_reset cke_chrome cke_editor_projectDetail cke_ltr cke_browser_webkit" dir="ltr" lang="zh-cn" role="application" aria-labelledby="cke_projectDetail_arialbl"><span id="cke_projectDetail_arialbl" class="cke_voice_label">所见即所得编辑器, projectDetail</span><div class="cke_inner cke_reset" role="presentation"><span id="cke_2_top" class="cke_top cke_reset_all" role="presentation" style="height: auto; user-select: none;"><span id="cke_55" class="cke_voice_label">工具栏</span><span id="cke_2_toolbox" class="cke_toolbox" role="group" aria-labelledby="cke_55" onmousedown="return false;"><span id="cke_57" class="cke_toolbar" aria-labelledby="cke_57_label" role="toolbar"><span id="cke_57_label" class="cke_voice_label">基本格式</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_58" class="cke_button cke_button__bold cke_button_off" href="javascript:void('加粗')" title="加粗 (Ctrl+B)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_58_label" aria-describedby="cke_58_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(67,event);" onfocus="return CKEDITOR.tools.callFunction(68,event);" onclick="CKEDITOR.tools.callFunction(69,this);return false;"><span class="cke_button_icon cke_button__bold_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -24px;background-size:auto;">&nbsp;</span><span id="cke_58_label" class="cke_button_label cke_button__bold_label" aria-hidden="false">加粗</span><span id="cke_58_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+B</span></a><a id="cke_59" class="cke_button cke_button__italic cke_button_off" href="javascript:void('倾斜')" title="倾斜 (Ctrl+I)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_59_label" aria-describedby="cke_59_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(70,event);" onfocus="return CKEDITOR.tools.callFunction(71,event);" onclick="CKEDITOR.tools.callFunction(72,this);return false;"><span class="cke_button_icon cke_button__italic_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -48px;background-size:auto;">&nbsp;</span><span id="cke_59_label" class="cke_button_label cke_button__italic_label" aria-hidden="false">倾斜</span><span id="cke_59_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+I</span></a><a id="cke_60" class="cke_button cke_button__underline cke_button_off" href="javascript:void('下划线')" title="下划线 (Ctrl+U)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_60_label" aria-describedby="cke_60_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(73,event);" onfocus="return CKEDITOR.tools.callFunction(74,event);" onclick="CKEDITOR.tools.callFunction(75,this);return false;"><span class="cke_button_icon cke_button__underline_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -144px;background-size:auto;">&nbsp;</span><span id="cke_60_label" class="cke_button_label cke_button__underline_label" aria-hidden="false">下划线</span><span id="cke_60_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+U</span></a><a id="cke_61" class="cke_button cke_button__strike cke_button_off" href="javascript:void('删除线')" title="删除线" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_61_label" aria-describedby="cke_61_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(76,event);" onfocus="return CKEDITOR.tools.callFunction(77,event);" onclick="CKEDITOR.tools.callFunction(78,this);return false;"><span class="cke_button_icon cke_button__strike_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -72px;background-size:auto;">&nbsp;</span><span id="cke_61_label" class="cke_button_label cke_button__strike_label" aria-hidden="false">删除线</span><span id="cke_61_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_62" class="cke_toolbar" aria-labelledby="cke_62_label" role="toolbar"><span id="cke_62_label" class="cke_voice_label">链接</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_63" class="cke_button cke_button__link cke_button_off" href="javascript:void('插入/编辑超链接')" title="插入/编辑超链接 (Ctrl+L)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_63_label" aria-describedby="cke_63_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(79,event);" onfocus="return CKEDITOR.tools.callFunction(80,event);" onclick="CKEDITOR.tools.callFunction(81,this);return false;"><span class="cke_button_icon cke_button__link_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -528px;background-size:auto;">&nbsp;</span><span id="cke_63_label" class="cke_button_label cke_button__link_label" aria-hidden="false">插入/编辑超链接</span><span id="cke_63_description" class="cke_button_label" aria-hidden="false">快捷键 Ctrl+L</span></a><a id="cke_64" class="cke_button cke_button__unlink cke_button_disabled " href="javascript:void('取消超链接')" title="取消超链接" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_64_label" aria-describedby="cke_64_description" aria-haspopup="false" aria-disabled="true" onkeydown="return CKEDITOR.tools.callFunction(82,event);" onfocus="return CKEDITOR.tools.callFunction(83,event);" onclick="CKEDITOR.tools.callFunction(84,this);return false;"><span class="cke_button_icon cke_button__unlink_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -552px;background-size:auto;">&nbsp;</span><span id="cke_64_label" class="cke_button_label cke_button__unlink_label" aria-hidden="false">取消超链接</span><span id="cke_64_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_65" class="cke_toolbar" aria-labelledby="cke_65_label" role="toolbar"><span id="cke_65_label" class="cke_voice_label">插入</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_66" class="cke_button cke_button__image cke_button_off" href="javascript:void('图像')" title="图像" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_66_label" aria-describedby="cke_66_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(85,event);" onfocus="return CKEDITOR.tools.callFunction(86,event);" onclick="CKEDITOR.tools.callFunction(87,this);return false;"><span class="cke_button_icon cke_button__image_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -360px;background-size:auto;">&nbsp;</span><span id="cke_66_label" class="cke_button_label cke_button__image_label" aria-hidden="false">图像</span><span id="cke_66_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_67" class="cke_button cke_button__flash cke_button_off" href="javascript:void('Flash')" title="Flash" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_67_label" aria-describedby="cke_67_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(88,event);" onfocus="return CKEDITOR.tools.callFunction(89,event);" onclick="CKEDITOR.tools.callFunction(90,this);return false;"><span class="cke_button_icon cke_button__flash_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1080px;background-size:auto;">&nbsp;</span><span id="cke_67_label" class="cke_button_label cke_button__flash_label" aria-hidden="false">Flash</span><span id="cke_67_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_68" class="cke_button cke_button__table cke_button_off" href="javascript:void('表格')" title="表格" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_68_label" aria-describedby="cke_68_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(91,event);" onfocus="return CKEDITOR.tools.callFunction(92,event);" onclick="CKEDITOR.tools.callFunction(93,this);return false;"><span class="cke_button_icon cke_button__table_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -912px;background-size:auto;">&nbsp;</span><span id="cke_68_label" class="cke_button_label cke_button__table_label" aria-hidden="false">表格</span><span id="cke_68_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_69" class="cke_toolbar" aria-labelledby="cke_69_label" role="toolbar"><span id="cke_69_label" class="cke_voice_label">颜色</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_70" class="cke_button cke_button__textcolor cke_button_off" href="javascript:void('文本颜色')" title="文本颜色" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_70_label" aria-describedby="cke_70_description" aria-haspopup="true" onkeydown="return CKEDITOR.tools.callFunction(94,event);" onfocus="return CKEDITOR.tools.callFunction(95,event);" onclick="CKEDITOR.tools.callFunction(96,this);return false;"><span class="cke_button_icon cke_button__textcolor_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1128px;background-size:auto;">&nbsp;</span><span id="cke_70_label" class="cke_button_label cke_button__textcolor_label" aria-hidden="false">文本颜色</span><span id="cke_70_description" class="cke_button_label" aria-hidden="false"></span><span class="cke_button_arrow"></span></a><a id="cke_71" class="cke_button cke_button__bgcolor cke_button_off" href="javascript:void('背景颜色')" title="背景颜色" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_71_label" aria-describedby="cke_71_description" aria-haspopup="true" onkeydown="return CKEDITOR.tools.callFunction(97,event);" onfocus="return CKEDITOR.tools.callFunction(98,event);" onclick="CKEDITOR.tools.callFunction(99,this);return false;"><span class="cke_button_icon cke_button__bgcolor_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1104px;background-size:auto;">&nbsp;</span><span id="cke_71_label" class="cke_button_label cke_button__bgcolor_label" aria-hidden="false">背景颜色</span><span id="cke_71_description" class="cke_button_label" aria-hidden="false"></span><span class="cke_button_arrow"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_72" class="cke_toolbar" aria-labelledby="cke_72_label" role="toolbar"><span id="cke_72_label" class="cke_voice_label">段落</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_73" class="cke_button cke_button__numberedlist cke_button_off" href="javascript:void('编号列表')" title="编号列表" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_73_label" aria-describedby="cke_73_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(100,event);" onfocus="return CKEDITOR.tools.callFunction(101,event);" onclick="CKEDITOR.tools.callFunction(102,this);return false;"><span class="cke_button_icon cke_button__numberedlist_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -648px;background-size:auto;">&nbsp;</span><span id="cke_73_label" class="cke_button_label cke_button__numberedlist_label" aria-hidden="false">编号列表</span><span id="cke_73_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_74" class="cke_button cke_button__bulletedlist cke_button_off" href="javascript:void('项目列表')" title="项目列表" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_74_label" aria-describedby="cke_74_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(103,event);" onfocus="return CKEDITOR.tools.callFunction(104,event);" onclick="CKEDITOR.tools.callFunction(105,this);return false;"><span class="cke_button_icon cke_button__bulletedlist_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -600px;background-size:auto;">&nbsp;</span><span id="cke_74_label" class="cke_button_label cke_button__bulletedlist_label" aria-hidden="false">项目列表</span><span id="cke_74_description" class="cke_button_label" aria-hidden="false"></span></a><span class="cke_toolbar_separator" role="separator"></span><a id="cke_75" class="cke_button cke_button__justifyleft cke_button_on" href="javascript:void('左对齐')" title="左对齐" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_75_label" aria-describedby="cke_75_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(106,event);" onfocus="return CKEDITOR.tools.callFunction(107,event);" onclick="CKEDITOR.tools.callFunction(108,this);return false;" aria-pressed="true"><span class="cke_button_icon cke_button__justifyleft_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1200px;background-size:auto;">&nbsp;</span><span id="cke_75_label" class="cke_button_label cke_button__justifyleft_label" aria-hidden="false">左对齐</span><span id="cke_75_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_76" class="cke_button cke_button__justifycenter cke_button_off" href="javascript:void('居中')" title="居中" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_76_label" aria-describedby="cke_76_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(109,event);" onfocus="return CKEDITOR.tools.callFunction(110,event);" onclick="CKEDITOR.tools.callFunction(111,this);return false;"><span class="cke_button_icon cke_button__justifycenter_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1176px;background-size:auto;">&nbsp;</span><span id="cke_76_label" class="cke_button_label cke_button__justifycenter_label" aria-hidden="false">居中</span><span id="cke_76_description" class="cke_button_label" aria-hidden="false"></span></a><span class="cke_toolbar_separator" role="separator"></span><a id="cke_77" class="cke_button cke_button__blockquote cke_button_off" href="javascript:void('块引用')" title="块引用" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_77_label" aria-describedby="cke_77_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(112,event);" onfocus="return CKEDITOR.tools.callFunction(113,event);" onclick="CKEDITOR.tools.callFunction(114,this);return false;"><span class="cke_button_icon cke_button__blockquote_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -168px;background-size:auto;">&nbsp;</span><span id="cke_77_label" class="cke_button_label cke_button__blockquote_label" aria-hidden="false">块引用</span><span id="cke_77_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_78" class="cke_button cke_button__codesnippet cke_button_off" href="javascript:void('插入代码段')" title="插入代码段" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_78_label" aria-describedby="cke_78_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(115,event);" onfocus="return CKEDITOR.tools.callFunction(116,event);" onclick="CKEDITOR.tools.callFunction(117,this);return false;"><span class="cke_button_icon cke_button__codesnippet_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -1056px;background-size:auto;">&nbsp;</span><span id="cke_78_label" class="cke_button_label cke_button__codesnippet_label" aria-hidden="false">插入代码段</span><span id="cke_78_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_79" class="cke_toolbar" aria-labelledby="cke_79_label" role="toolbar"><span id="cke_79_label" class="cke_voice_label">样式</span><span class="cke_toolbar_start"></span><span id="cke_56" class="cke_combo cke_combo__format cke_combo_off" role="presentation"><span id="cke_56_label" class="cke_combo_label">格式</span><a class="cke_combo_button" title="格式" tabindex="-1" href="javascript:void('格式')" hidefocus="true" role="button" aria-labelledby="cke_56_label" aria-haspopup="true" onkeydown="return CKEDITOR.tools.callFunction(119,event,this);" onfocus="return CKEDITOR.tools.callFunction(120,event);" onclick="CKEDITOR.tools.callFunction(118,this);return false;"><span id="cke_56_text" class="cke_combo_text">普通</span><span class="cke_combo_open"><span class="cke_combo_arrow"></span></span></a></span><span class="cke_toolbar_end"></span></span><span id="cke_80" class="cke_toolbar cke_toolbar_last" aria-labelledby="cke_80_label" role="toolbar"><span id="cke_80_label" class="cke_voice_label">工具</span><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_81" class="cke_button cke_button__source cke_button_off" href="javascript:void('源码')" title="源码" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_81_label" aria-describedby="cke_81_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(121,event);" onfocus="return CKEDITOR.tools.callFunction(122,event);" onclick="CKEDITOR.tools.callFunction(123,this);return false;"><span class="cke_button_icon cke_button__source_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -840px;background-size:auto;">&nbsp;</span><span id="cke_81_label" class="cke_button_label cke_button__source_label" aria-hidden="false">源码</span><span id="cke_81_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_82" class="cke_button cke_button__maximize cke_button_off" href="javascript:void('全屏')" title="全屏" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_82_label" aria-describedby="cke_82_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(124,event);" onfocus="return CKEDITOR.tools.callFunction(125,event);" onclick="CKEDITOR.tools.callFunction(126,this);return false;"><span class="cke_button_icon cke_button__maximize_icon" style="background-image:url('https://my.oschina.net/new-osc/js/utils/plugins/ckeditor/plugins/icons.png?t=20180726');background-position:0 -672px;background-size:auto;">&nbsp;</span><span id="cke_82_label" class="cke_button_label cke_button__maximize_label" aria-hidden="false">全屏</span><span id="cke_82_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span></span></span><div id="cke_2_contents" class="cke_contents cke_reset" role="presentation" style="height: 350px;"><span id="cke_87" class="cke_voice_label">按 ALT+0 获得帮助</span><iframe src="" frameborder="0" class="cke_wysiwyg_frame cke_reset" title="所见即所得编辑器, projectDetail" aria-describedby="cke_87" tabindex="0" allowtransparency="true" style="width: 100%; height: 100%;"></iframe></div><span id="cke_2_bottom" class="cke_bottom cke_reset_all" role="presentation" style="user-select: none;"><span id="cke_2_resizer" class="cke_resizer cke_resizer_vertical cke_resizer_ltr" title="拖拽以改变大小" onmousedown="CKEDITOR.tools.callFunction(65, event)">◢</span><span id="cke_2_path_label" class="cke_voice_label">元素路径</span><span id="cke_2_path" class="cke_path" role="group" aria-labelledby="cke_2_path_label"><a id="cke_elementspath_52_1" href="javascript:void('body')" tabindex="-1" class="cke_path_item" title="body 元素" hidefocus="true" onkeydown="return CKEDITOR.tools.callFunction(128,1, event );" onclick="CKEDITOR.tools.callFunction(127,1); return false;" role="button" aria-label="body 元素">body</a><a id="cke_elementspath_52_0" href="javascript:void('p')" tabindex="-1" class="cke_path_item" title="p 元素" hidefocus="true" onkeydown="return CKEDITOR.tools.callFunction(128,0, event );" onclick="CKEDITOR.tools.callFunction(127,0); return false;" role="button" aria-label="p 元素">p</a><span class="cke_path_empty">&nbsp;</span></span></span></div></div>
      </div>
      <div class="field">
        <label>请选择开源许可证</label>
        <div class="fields">
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_GPL" name="license" value="GPL" class="hidden" tabindex="0">
              <label for="license_GPL">GPL</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_LGPL" name="license" value="LGPL" class="hidden" tabindex="0">
              <label for="license_LGPL">LGPL</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_AGPL" name="license" value="AGPL" class="hidden" tabindex="0">
              <label for="license_AGPL">AGPL</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_Apache" name="license" value="Apache" class="hidden" tabindex="0">
              <label for="license_Apache">Apache</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_MIT" name="license" value="MIT" class="hidden" tabindex="0">
              <label for="license_MIT">MIT</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_BSD" name="license" value="BSD" class="hidden" tabindex="0">
              <label for="license_BSD">BSD</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_EPL" name="license" value="EPL" class="hidden" tabindex="0">
              <label for="license_EPL">EPL</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_MPL" name="license" value="MPL" class="hidden" tabindex="0">
              <label for="license_MPL">MPL</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" id="license_other" name="license" value="" class="hidden" checked="" tabindex="0">
              <label for="license_other">其他</label>
            </div>
          </div>
        </div>
      </div>
      <div class="ui positive mini message osc hidden"></div>
      <div class="ui negative mini message osc hidden"></div>
      <div class="ui primary submit large submit button">添加软件</div>
    </form>
  </div>
  <div class="ui tab basic segment active" data-tab="publish-link">
    <div class="ui info message">
      <div class="header">友情提示</div>
      <ul class="list">
        <li>此处仅为投递具体某一篇文章的链接</li>
        <li>您投递的链接经过编辑审核通过后，将在首页推荐文章流中显示</li>
        <li>文章日期太久将不会通过审核</li>
      </ul>
    </div>
    <form class="ui publish-link form">
      <div class="field">
        <div class="ui fluid action input">
          <input name="link" type="text" value="" placeholder="请输入要投递的文章链接">
          <div class="ui orange right labeled icon button check-link">
            <i class="checkmark icon"></i>
            检查网址
          </div>
        </div>
        <div class="ui basic red pointing prompt label transition visible">请输入文章链接</div></div>
      <div class="ui positive mini message osc hidden"></div>
      <div class="ui negative mini message osc hidden"></div>
      <div class="ui center aligned basic segment">
        <div class="ui primary submit large submit button">投递链接</div>
      </div>
    </form>
  </div>
</div>
    </div>
  </div>
  </div>
</body>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</html>