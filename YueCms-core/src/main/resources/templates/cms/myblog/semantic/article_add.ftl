
<!DOCTYPE html>
<html>
<head>
    <title>新增文章</title>
    <link href="/resources/css/admui.css" rel="stylesheet"/>
    <link href="/resources/lib/select2/css/select2.min.css" rel="stylesheet"/>

    <script type="text/javascript" src="/resources/lib/ueditor/1.4.3/ueditor.config.js"></script>
    <script type="text/javascript" src="/resources/lib/ueditor/1.4.3/ueditor.all.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/resources/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>

    <script type="text/javascript" src="/resources/js/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script type="text/javascript" src="/resources/lib/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="/resources/lib/select2/js/i18n/zh-CN.js"></script>

    <style>
        .control-label{width:100px;text-align: left;}
    </style>
</head>
<body style="padding-top: 0;">
<div class="content-wrapper  animated fadeInRight">
    <div class="container-fluid">
        <form id="form_show"  class="form-horizontal">
            <input type="hidden" value="1" name="available" id="available" />
            <input type="hidden" value="myblog" name="from" id="from" />
            <div class="row form-group padding-5" style="border-bottom: 1px dashed #76838f;">
                <div class="col-sm-offset-2  " style="float: right">
                    <div style="display: none;">
                        <input type="checkbox" id="draft">
                        <label class=" " for="draft">草稿</label>
                        <input type="checkbox" checked id="pass">
                        <label class=" " for="pass">审核通过</label>
                    </div>
                    <button type="button" onclick="javascript:window.location.href='/myblog/newblog'" class="btn btn-primary margin-left-5"><i class="icon-ok"></i>切换版本</button>
                    <button type="button" onclick="save()" class="btn btn-success margin-left-5"><i class="icon-ok"></i>保存</button>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">作者：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" class="form-control"  name="yhid" datatype="*" value="${front_yhid}"/>
                    <input type="text" readonly class="form-control"  value="${front_yhmc}"/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">标题：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" class="form-control" style="width: 70%;float:left;" id="title" name="title" datatype="*" placeholder="标题" value=""/>
                    <div style="float: left;">&nbsp;&nbsp;&nbsp;<input type="checkbox" name="isCopy" id="isCopy">
                    <label class=" control-label" for="isCopy">是否转载</label></div>
                </div>
                <div class="col-sm-4">
                     <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group" id="div_isCopy_extend" style="display: none;">
                <label class="col-sm-2 control-label">链接：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" name="copyFlag" id="copyFlag" value="0">
                    <input type="text" class="form-control" id="link" name="link" placeholder="链接"  value=""/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">标签：</label>
                <div class="col-sm-6 formControls">
                        <select class="form-control"  id="tags" multiple class="select2" style="width: 70%;float:left;border-color:#e4eaec;">
                        </select>
                        <input type="text"  class="form-control" style="float: right;width: 30%;" placeholder="新标签" id="newTag">
                </div>
                <div class="col-sm-4">
                     <a class="btn btn-success" onclick="addTags()">增加标签</a>
                    <div class="Validform_checktip"></div>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">关键词：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" class="form-control" name="keywords" placeholder="关键词"  value=""/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">封面图：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" class="form-control" placeholder="封面图" name="cover_image"  value=""/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">描述：</label>
                <div class="col-sm-6 formControls">
                    <textarea rows="4" class="form-control" name="description"  placeholder="描述" id="description" datatype="*"  ></textarea>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

           <!-- <div class="form-group">
                <label class="col-sm-2 control-label">频道栏目ID：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" class="form-control" name="channelids" placeholder="频道栏目ID" datatype="*" value=""/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">个人分类：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" class="form-control" placeholder="个人分类" name="personal_category" value=""/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>-->
            <div class="form-group">
                <label class="col-sm-2 control-label">正文：</label>
                <div class="col-sm-10 formControls">
                    <script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var ue = UE.getEditor('editor');
    function getContent() {
        return UE.getEditor('editor').getContent();
    }
    function getText() {
        //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
        var range = UE.getEditor('editor').selection.getRange();
        range.select();
        var txt = UE.getEditor('editor').selection.getText();
        alert(txt)
    }
    function getContentTxt() {
        return UE.getEditor('editor').getContentTxt();
    }
    function hasContent() {
        return UE.getEditor('editor').hasContents();
    }

    function enableBtn() {
        var div = document.getElementById('btns');
        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
        }
    }
    function getLocalData() {
        alert(UE.getEditor('editor').execCommand("getlocaldata"));
    }
    function clearLocalData() {
        UE.getEditor('editor').execCommand("clearlocaldata");
        alert("已清空草稿箱")
    }
</script>

<script type="text/javascript">
    var sys_ctx="";
    function addTags(){
        if(isNotEmpty($("#newTag").val())){
            var ids = $("#tags").select2('val');
            if(isNotEmpty(ids)&&ids.length>=3){
                layerMsg_wrongIcon("您最多能添加三个标签!");
                $("#newTag").val("");
                return;
            }
            // getTags();
            var id=new Date().getTime();
            var t={id:id,text:$("#newTag").val(),newflag:1};
            dataList.push(t);
            var obj= $("#tags").select2({
                data: dataList,
                language: "zh-CN",
                maximumSelectionLength: 3, //设置最多可以选择多少项
                placeholder: '请选择标签'
            } );
            if(isNotEmpty(ids)){
                ids.push(id);
            }else{
                ids=[];
                ids.push(id);
            }
            obj.val(ids).trigger("change");
            $("#newTag").val("");
        }
    }

    function changeUrlArg(url, arg, val){
        var pattern = arg+'=([^&]*)';
        var replaceText = arg+'='+val;
        return url.match(pattern) ? url.replace(eval('/('+ arg+'=)([^&]*)/gi'), replaceText) : (url.match('[\?]') ? url+'&'+replaceText : url+'?'+replaceText);
    }
    var validform;
    function save() {
        //var d = getTags();
        //alert(JSON.stringify(d));
        //return;
        var b = validform.check(false);
        if (!b) {
            return;
        }
        var tags = getTags();
        if(isEmpty(tags)|| tags.length==0){
            layerAlert_wrongIcon("请输入标签(1-3个)！");
            return;
        }
        var c = getContentTxt();
        if (c == '' || c.length == 0) {
            layerAlert_wrongIcon("请输入正文！");
            return;
        }
        var v = getContent();
        //$("#content").val(v);
        var params = $("#form_show").serialize();
        params=changeUrlArg(params,"editorValue","");//因为不需要传递这个值到后台所以替换成空

        $.ajax({
            type: "post",
            url: '/myblog/article/json/save?' + params,
            data: {content: v,tags:tags},
            success: function (data, textStatus) {

                var json=typeof data=='string'?JSON.parse(data):data;
                if(json.type=="success"){
                    ajaxReturnMsg(data);
                    setTimeout(function(){
                        window.location.href="/myblog/newblogSuccess?title="+$("#title").val()+"&id="+json.data.id;
                    },800);
                }else{
                    layerAlert(json.content);
                }
            }
        });
    }

    function getTags(){
        //var v = $("#tags").select2('val');//只获取id
        var obj=$("#tags").select2('data');
        var res=[];
        $.each(obj,function(i,data){
            var t={id:data.id,name:data.text,newflag:data.newflag};
            res.push(t);
        });
        return JSON.stringify(res);
    }

    var dataList=[];
    function initSelect2(){
        var dataArr;
        sys_ajaxPost("/cms/tag/json/tags",null,function(data){
            var json=typeof data=='string'?JSON.parse(data):data;
            var type=json.type;
            if(type=='success'){
                dataArr=json.data.data;
                $.each(dataArr,function(v,data){
                    var t={id:data.id,text:data.name};
                    dataList.push(t);
                });
                $("#tags").select2({
                    data: dataList,
                    language: "zh-CN",
                    maximumSelectionLength: 3, //设置最多可以选择多少项
                    placeholder: '请选择标签'
                } );
            }
        });
    }
    $(function () {
        initSelect2();
        $("#isCopy").on("change",function(){
            var v=$(this).prop("checked");
            if(v){
                $("#copyFlag").val("1");//转载标志
                $("#div_zyfl").hide();//隐藏资源分类
                $("#articletypes").removeAttr("datatype");
                $("#div_isCopy_extend").show();
                $("#link").attr("datatype","*");
            }else{
                $("#copyFlag").val("0");
                $("#div_zyfl").show();//隐藏资源分类
                $("#articletypes").attr("datatype","*");
                $("#div_isCopy_extend").hide();
                $("#link").removeAttr("datatype");
            }
        });
        $("#draft").on("change",function(){
            var v=$(this).prop("checked");
            if(v){
                $("#available").val("0");
                $("#pass").prop("checked",false);
            }else{
                $("#available").val("1");
                $("#pass").prop("checked",true);
            }
        });
        $("#pass").on("change",function(){
            var v=$(this).prop("checked");
            if(v){
                $("#available").val("1");
                $("#draft").prop("checked",false);
            }else{
                $("#available").val("0");
                $("#draft").prop("checked",true);
            }
        });
        validform = $("#form_show").Validform({
            tiptype: 2,
            postonce: true,//至提交一次
            ajaxPost: false,//ajax方式提交
            showAllError: true //默认 即逐条验证,true验证全部
        });
    })
</script>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>
