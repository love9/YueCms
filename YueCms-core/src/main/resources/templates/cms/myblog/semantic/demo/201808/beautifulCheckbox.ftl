
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>checkbox美化</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="shortcut icon" href="/favicon.ico">

    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>

    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <style>
        .check {
            position: relative;
        }

        input[type=checkbox] + label {
            /*position:absolute;
            width:60px;*/
            height: 20px;
        }

        input[type=checkbox] + label:before {
            content: "";
            position: absolute;
            width: 20px;
            height: 20px;
            background: url(/resources/images/bg_checkbox.png) no-repeat;
        }

        input[type=checkbox] + label span {
            font-size: 14px;
            position: absolute;
            left: 30px;
        }

        input[type=checkbox]:checked + label:before {
            background-position: -28px 0;
        }

        input[type=checkbox] {
            position: absolute;
            left: 0;
            top: 0;
        }
    </style>
</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->
    <div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
        <div class="ui container" style="background: #fff;">
            <div class="ui internally grid web-blog">
                <div class="row">
                    <div class="twelve wide computer  sixteen wide mobile column  ">

<div class="ui raised segment ">
    <a class="ui red ribbon label">CSS</a>
     <pre style="padding:0 1rem;margin:0;">
<code>
<span style="color: #008000">#check</span>{
position:relative;
}
<span style="color: #008000">input[type=checkbox]+label</span>{
position:absolute;
width:60px;
height:20px;}
<span style="color: #008000">input[type=checkbox]+label:before</span>{
content:"";
position:absolute;
width:20px;
height:20px;
background:url(<a href="/resources/images/bg_checkbox.png" target="_blank">/resources/images/bg_checkbox.png</a>)no-repeat;&nbsp;&nbsp;&nbsp;<img src="/resources/images/bg_checkbox.png" style="display: inline-block">
}
<span style="color: #008000">input[type=checkbox]+label span</span>{
 font-size:14px;
position:absolute;
left:30px;}
<span style="color: #008000">input[type=checkbox]:checked+label:before</span>{
background-position:-28px 0;}
<span style="color: #008000">input[type=checkbox]</span>{
position:absolute;
left:0;
top:0;}
</code>
</pre>
</div>
<div class="ui raised segment ">
    <a class="ui red ribbon label">HTML</a>
    <!--<pre>-->
<xmp>
<div class="check">
    <input type="checkbox" id="btn-check">
    <label for="btn-check"><span>选项一</span></label>
</div>
</xmp>
   <!-- </pre>-->
</div>
<div class="ui raised segment " style="min-height:120px;">
    <a class="ui red ribbon label">效果</a>

    <form class="ui basic form">
        <div class="field">
            <div class="check">
                <input type="checkbox" id="btn-check">
                <label for="btn-check"><span>选项一</span></label>
            </div>
        </div>
        <div class="field" style="margin-left:80px;">
            <div class="check" >
                <input type="checkbox" id="btn-check2">
                <label for="btn-check2"><span>选项二</span></label>
            </div>
        </div>
    </form>
</div>
<!--
<div style="width: 50%;float: right;display: inline-block;position: relative;top: -143px;">

    <a class="ui red ribbon label">效果</a>
    <div class="check" style="top:20px;">
                                <input type="checkbox" id="btn-check">
                                <label for="btn-check"><span>选项一</span></label>
    </div>
    <div class="check" style="top:40px;">
                                <input type="checkbox" id="btn-check2">
                                <label for="btn-check2"><span>选项二</span></label>
    </div>
</div>
-->


                </div>
                <div class="four wide column computer only">
                    <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
                </div>
            </div>
        </div>
    </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>