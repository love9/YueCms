
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="shortcut icon" href="/favicon.ico">

    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">
    <link rel="stylesheet" href="/resources/lib/fluidbox/dist/css/fluidbox.min.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>

    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script src="/resources/lib/fluidbox/dist/js/jquery.fluidbox.min.js"></script>
    <script src="/resources/lib/gridalicius/jquery.grid-a-licious.min.js"></script>

</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->
    <div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
        <div class="ui container" style="background: #fff;padding:10px;">
            <!--Begin Gallery-->
            <div class="gallery"><a href="/resources/myblog/images/gallery/images1.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images1.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images2.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images2.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images3.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images3.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images4.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images4.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images5.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images5.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images6.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images6.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images7.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images7.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images8.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images8.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images14.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images14.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images15.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images15.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images11.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images11.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images12.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images12.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images13.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images13.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images9.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images9.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images10.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images10.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images18.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images18.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images16.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images16.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images17.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images17.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images19.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images19.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images207.jpg.html" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images20.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images21.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images21.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images22.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images22.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images23.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images23.jpg" alt="" title="" /></a></div>
            <div class="gallery"><a href="/resources/myblog/images/gallery/images24.jpg" class="overlay-2" title="" data-fluidbox><img src="/resources/myblog/images/gallery/images24.jpg" alt="" title="" /></a></div>
            <!--Finish Gallery-->
        </div>
        <script>
            $(function(){
                $(".gallery a").fluidbox({
                    stackIndexDelta:999

                });//lightbox trigger

                //grid image trigger
                $(".container").gridalicious({
                    animate: true,
                    selector: ".gallery",
                    gutter: 5,
                    width: 250
                });
            })
        </script>
    </div>
    <#include "/cms/myblog/semantic/block/footer.ftl"/>
    <#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</body>
</html>