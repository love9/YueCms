
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>

   <meta name="mobile-web-app-capable" content="yes">
   <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
   <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
   <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">
   <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
   <script src="/resources/SemanticUI/semantic.min.js"></script>
   <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
   <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
   <script type="text/javascript" src="/resources/myblog/js/Lrc.js"></script>
   <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>



</head>

<body id="demo">

<!-- nav start -->
<#include "/myblog/nav.ftl"/>
<!--<hr>-->
<!-- nav end -->
<!-- content srart -->

<article>
    <style>
    </style>
    <div class="ab_box">
        <h1 class="t_nav">
            <span>像“草根”一样，紧贴着地面，低调的存在，冬去春来，枯荣无恙。</span>
            <a href="/myblog" class="n1">网站首页</a>
            <a href="/myblog/updateLog" class="n2">更新日志</a>
            <a href="javascript:;" class="n3">LRC歌词演示</a>
        </h1>
        <div class="leftbox">
            <div class="body-wrapper">
                <h1 class="title">我的将军啊<span></span></h1>
                <audio id="mp" src="/resources/myblog/my_gen.mp3" controls autoplay  loop="loop"></audio>
                <div id="lrc-wrapper" class="lrc-wrapper">

                </div>
            </div>
            <script type="text/javascript">

                // 实例化一个LRC歌词插件
               var mylrc = new Lrc(
                        {
                            wrapperId: 'lrc-wrapper', // 目标容器ID
                            lrcUrl: '/resources/myblog/my_gen.lrc', // 歌词文件地址
                            row: 16, // 歌词行数
                            lineHeight: 40, // 每行歌词的高度，修改高度需要同步修改【.lrc-wrapper ul li】的样式
                            delay: 0, // 延迟秒数，如果歌词文件不同步可以启用这个参数
                            // 获取当前播放时间，针对不同播放器需要自己实现本方法，对于EPG可以是：Epg.Mp.getCurrentPlayTime()
                            getCurrentPlayTime: function(){ return mp.currentTime || 0; }
                            //getCurrentPlayTime: function(){ return Epg.Mp.getCurrentPlayTime(); }
                        });

                $(function(){
                    setTimeout(function(){
                        document.getElementById('mp').volume = 0.3;
                    },200);
                })
            </script>
        </div>
        <div class="rightbox">
            <div class="aboutme">
                <h2 class="hometitle">关于我</h2>
                <div class="avatar"> <img src="/resources/images/portraits/5.jpg"> </div>
                <div class="ab_con">
                    <p>网名：MakBos | 武继跃</p>
                    <p>职业：Java工程师、Web前端、网页设计 </p>
                    <p>个人微信：MakBos</p>
                    <p>邮箱：747506908@qq.com</p>
                </div>
            </div>
            <div class="weixin">
                <h2 class="hometitle">微信关注</h2>
                <ul>
                    <img src="/resources/myblog/images/my_wx.png">
                </ul>
            </div>
        </div>
    </div>
</article>

<!-- content end -->

<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>

</body>
</html>