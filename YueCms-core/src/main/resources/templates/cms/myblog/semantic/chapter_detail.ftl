
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>

    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>

<body  class="pusher" style="background: #fff;">

<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher"  >
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->
    <div class="ui text container" id="mainScreen" style="    margin-top:2em;">
        <h1 class="ui centered header">${detail.title!}</h1>
        <article>
            ${detail.content!}
        </article>

        <div class="ui divider"></div>

        <div class="ui two column grid">
            <div class="left aligned column">
                <#if pre_chapter??>
                    <a style="text-align: left;" class="fluid ui button brown  " href="/myblog/chapterDetail/${pre_chapter.id!}">上一篇：${pre_chapter.sort!} .${pre_chapter.title!}</a>
                <#else>
                    <a style="text-align: left;" class="fluid ui button" href="javascript:void(0);">上一篇：没有了</a>
                </#if>
            </div>
            <div class="right aligned column">
                <#if next_chapter??>
                    <a style="text-align: left;" class="fluid ui button brown  " href="/myblog/chapterDetail/${next_chapter.id!}">下一篇：${next_chapter.sort!} . ${next_chapter.title!}</a>
                <#else>
                    <a style="text-align: left;" class="fluid ui button" href="javascript:void(0);">下一篇：没有了</a>
                </#if>
            </div>
        </div>
        <div class="ui divider"></div>
        <#if chapterDetails??>
            <div class="ui two columns grid chapterDetails">
                <#list chapterDetails as item>
                    <div class="column">
                        <a style="text-align: left;"  class="fluid ui button primary"   href="/myblog/chapterDetail/${item.id!}">${item.sort!} . ${item.title!}</a>
                    </div>
                </#list>
            </div>
        </#if>

<div class="ui divider"></div>
<!--评论区域 start-->
<#include "/cms/myblog/semantic/comment/comment-area-chapter.ftl"/>
<!--评论区域 end-->
</div>
<!-- content end -->
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
</div>
</body>
</html>