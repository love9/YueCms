<script type="text/javascript" src="/resources/js/common/jubao.js"></script>
<div class="ui comments"  id="comment_list">
    <h4 class="ui dividing header">评论列表</h4>
    <script type="text/javascript">
        var dlyh='${front_yhid!}';
        function up_vote(obj,id){
            if(dlyh==''){
                $.layer.msg_cry("请先登录!");
                return;
            }
            $this=$(obj);
            $this.removeAttr("onclick");
            $this.css("color","green");
            // $this.siblings().removeAttr("style");
            upDownVote(id,"up");
            $this.find(".my-add-num").html("<em class='add-animation'>+1</em>").show();
        }
        function show_jubao(targetId){
            jubao(targetId);
        }
        //用户需要重写覆盖jubao.js的提交方法
        function jubaoSubmit(target_id){
            var report_type=getJuBaoValue();
            //alert(target_id+"=="+report_type);
            //ajax 提交
            sys_ajaxPost("/cms/report/json/doReport",{report_type:report_type,target_id:target_id},function(data){
                ajaxReturnMsg(data,function(){
                    layer.closeAll();
                    jubaoSuccess();
                });
            })
        }
        function down_vote(obj,id){
            if(dlyh==''){
                $.layer.msg_cry("请先登录!");
                return;
            }
            $this=$(obj);
            $this.removeAttr("onclick");
            $this.css("color","red");
            upDownVote(id,"down");

        }
        function upDownVote(id,type,obj,a){
            var params="?id="+id+"&type="+type;
            $.ajax({
                type: 'POST',
                url: '/cms/comment/json/votes'+params,
                data: null,
                contentType: 'application/json',
                success:function(data){
                    ajaxReturnMsg(data,function(){//成功的回调
                        $this.find(".my-add-num").html("<em class='add-animation'>+1</em>").show();//+1动画效果
                        if(type=="down"){
                            var num=Number($("#down_num_"+id).text());
                            num+=1;
                            $("#down_num_"+id).text(num);
                            //禁用按钮几秒钟
                        }
                        if(type=="up"){
                            var num=Number($("#up_num_"+id+"").html());
                            num+=1;
                            $("#up_num_"+id).text(num);
                        }
                    });
                }
            })
        }
        function listComments(rows){
            if(rows.length>0){
                //$('#comment_list').html("");
                $.each(rows,function(i,json){
                    var num=(pageNo-1)*limit+i+1;
                    var html="<div class=\"comment\" data-id=\""+json.id+"\">";
                    html+=" <a class=\"avatar\">";
                    html+="<img src=\""+json.usericon+"\">";
                    html+="</a>";
                    html+="<div class=\"content\">";
                    html+="<a class=\"author\">"+json.username+"</a>";
                    html+="<div class=\"metadata\">";
                    html+="<span class=\"date\">"+json.createTime+"</span>";
                    html+="<span class=\"floor\">#"+num+"</span>";
                    html+="</div>";
                    html+="<div class=\"text\">"+json.comment+"</div>";
                    html+="<div class=\"actions\">";
                    html+="<div class=\"ui horizontal list\">";

                    html+="<div class=\"item\">";
                    html+="<a class='comment_tool_btn'  onclick='up_vote(this,\""+json.id+"\")'><i class=\"icon_comment icon_ding\"></i><span class='up_down_num' id=\"up_num_"+json.id+"\">"+json.up_vote+"</span><span  class=\"my-add-num\"></span></a>";
                    html+="</div>";
                    html+="<div class=\"item\">";
                    html+="<a class='comment_tool_btn'  onclick='down_vote(this,\""+json.id+"\")'><i  class=\"icon_comment icon_cai\"></i><span class='up_down_num' id=\"down_num_"+json.id+"\">"+json.down_vote+"</span><span  class=\"my-add-num\"></span></a>";
                    html+="</div>";
                    html+="<div class=\"item\">";
                    html+="<a class='comment_tool_btn'><i class=\"icon_comment icon_yinzhang\"></i></a>";
                    html+="</div>";
                    html+="<div class=\"item\">";
                    html+="<a class='comment_tool_btn comment_tool_btn_flag'   onclick='show_jubao("+json.id+")'><i class=\"icon_comment icon_flag\"></i><span>举报</span></a>";
                    html+="</div>";
                    html+="</div>";
                    html+="</div>";
                    html+="</div>";
                    html+="</div>";

                    $(html).hide().appendTo($('#comment_list')).show();
                })
            }else{
                var html="";
                html+='<li>';
                html+='<div>';
                html+='<p class="am-text-center">暂无评论!</p>';
                html+='</div>';
                html+='</li>';
                $('#comment_list').html(html);
            }
        }
        var pageNo=1;
        var limit=10;
        var totalPages=0;
        var target_id = "${article.id!}";
        function queryComments(successCallBack){
            var params="?page="+pageNo+"&rows="+limit+"&id="+target_id+"&myblog=1";
            $.ajax({
                type: 'POST',
                url: '/myblog/blog/comment/list'+params,
                data: null,
                contentType: 'application/json',
                success:function(data){
                    var json=typeof data=='string'?JSON.parse(data):data;
                    totalPages=json.totalPages;
                    var rows=json.rows;
                    listComments(rows);
                    //alert(JSON.stringify(json.items));
                    if(typeof successCallBack=='function'){
                        successCallBack();
                    }
                }
            })
        }
        function successCallBack(){
            if(totalPages==0){
                $("#btn_more_comments").hide();
                return;
            }
            if(pageNo>=totalPages){
                $("#btn_more_comments").attr("onclick","").removeClass("primary").html("已展开所有评论");
            }else{
                $("#btn_more_comments").attr("onclick","moreComments()").html("<i class=\"angle double down icon\"></i>&nbsp&nbsp更多");
            }
        }

        function moreComments(){
            pageNo++;
            $("#btn_more_comments").attr("onclick","").html("<i class=\"spinner icon\"></i>&nbsp&nbsp加载中");
            queryComments(successCallBack);
        }
        function pager(){
            //alert(totalPages+"==="+pageNo);
            $("#pager").page({ pages: totalPages,theme:"success" ,
                curr: pageNo , //当前页
                groups: 0, //连续显示分页数
                prev: false, //若不显示，设置false即可，默认为上一页
                next: '更多',
                render: function(context, $el, index) { //渲染[context：对this的引用，$el：当前元素，index：当前索引]
                    //逻辑处理
                    if (index == 'last') { //虽然上面设置了last的文字为尾页，但是经过render处理，结果变为最后一页
                        $el.find('a').html('没有更多了');
                        return $el; //如果有返回值则使用返回值渲染
                    }
                    return false; //没有返回值则按默认处理
                },
                jump:function(context,first) {
                    pageNo = context.option.curr;
                    if (!first) {
                        queryComments(successCallBack);
                    }
                }
            });
        }
        $(function(){
            queryComments(successCallBack);
        })
    </script>
</div>
<div id="btn_more_comments"  class="ui bottom primary attached button" onclick="moreComments()">
    <i class="spinner icon"></i>&nbsp&nbsp加载中 <i class="angle double down icon"></i>&nbsp&nbsp更多
</div>