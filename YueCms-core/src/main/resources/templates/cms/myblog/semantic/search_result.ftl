<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title></title>
    <meta name="renderer" content="webkit">

    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <#import "/base/util/macro.ftl" as macro>

</head>
<body id="myblog-index" class="pushable ">
<script>
    var sys_ctx = "";
</script>
<!--手机试图导航 start-->
<!--手机试图导航 end-->
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->
    <!-- fullTabs 置顶热门标签 -->
    <#include "/cms/myblog/semantic/block/index_fullTabs.ftl"/>

    <!-- content srart -->
    <div class="ui basic segment" id="mainScreen">
        <div class="ui container grid" style="background: #fff;">

            <!--左侧内容 start-->
            <div class="sixteen wide tablet eleven wide computer column">
                <!--搜索结果区域 start-->
                <#if msgObj??>
                <#--有错误消息提示，用下面自定义函数展示! 注意前端因为使用semantic UI 所以在后台使用的基础上扩展新增一个ui参数，后台使用该标签ui=""即可-->
                    <@macro.myMsg msgObj=msgObj ui="semantic" ></@macro.myMsg>
                </#if>
                <#if articlesPaginator??>
                <div class="blog-sidebar-widget" style="margin:0 -1.2rem;">

                    <h3 class="blog-sidebar-widget-title"><b>"${searchText!}"找到${articlesPaginator.totalCount?default(0)}个结果</b></h3>
                    <div class="blog-sidebar-widget-content" id="">
                        <div class="itemGrid">
                            <ul>
<#if articles??>
                <#list articles as item >
                    <li>
                        <div class="article_box">

    <#if item.static_url?? && item.static_url!="">
                            <a href="${item.static_url!}" title="${item.title!}" target="_blank">
    <#else>
                            <a href="/myblog/article/detail/${item.id!}" title="${item.title!}" target="_blank">
    </#if>
                            <div class="tophead">
                                <img   width="294" height="160"  src="${item.cover_image!}" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    ${item.title!}
                                </h3>
                                <p class="h40 cf999 mt10">${item.description!}</p>
                                <p class="mt15" style="text-align: right;"><span class="cf_green f12">阅读全文 &gt;</span>
                                </p>
                            </article>
                        </a>
                        </div>
                    </li>
                </#list>
<#else>
</#if>
                            </ul>
                            <div class="line"></div>
                        </div>
                    </div>
                </div>

                <!--搜索结果区域 end-->
                    <#if articlesPaginator.totalCount?? && (articlesPaginator.totalCount>0)>
                      <div id="pageGroup">
                        <#if articlesPaginator.firstPage==false>
                          <a onclick="queryList(1)" class="homepage">首页</a>
                        </#if>
                          <#if articlesPaginator.hasPrePage==true>
                            <a onclick="queryList(${articlesPaginator.prePage})" class="nopage">上页</a>
                          </#if>

                          <#if articlesSlider??>
                              <#list articlesSlider as num>
                                  <#if articlesPaginator.page==num>
                                        <a onclick="queryList(${num!})" class="listpage curpage">${num!}</a>
                                  <#else>
                                        <a onclick="queryList(${num!})" class="listpage">${num!}</a>
                                  </#if>
                              </#list>
                          </#if>

                          <#if articlesPaginator.hasNextPage==true>
                            <a onclick="queryList(${articlesPaginator.nextPage})" class="nextpage">下页</a>
                          </#if>
                        <#if articlesPaginator.lastPage==false>
                          <a onclick="queryList(${articlesPaginator.totalPages!})" class="endpage">尾页</a>
                        </#if>
                          <span class="pageinfo">共<strong>${articlesPaginator.totalPages!}</strong>页<strong>${articlesPaginator.totalCount!}</strong>条记录</span>
                      </div>
                    </#if>
                <script type="text/javascript">
                    var pageNo="${articlesPaginator.page!}";
                    var pageSize="6";
                    var totalPages="${articlesPaginator.totalPages!}";
                    var searchText="${searchText!}";
                    var searchUri="/myblog/search?words="+searchText;
                    $(function(){
                        $("#pageGroup a.listpage").click(function(){
                            pageNo=$(this).html();
                            queryList(pageNo);
                        });
                    })
                    var queryList=function(num){
                        window.location.href=searchUri+"&page="+num;
                    }
                </script>
                </#if>
            </div>

            <!--左侧内容 end-->
            <!--右侧内容 start-->
            <div class="sixteen wide tablet five wide computer column">
                <!--最新专区-->
                    <#include "/cms/myblog/semantic/block/index_article_hot.ftl"/>
            </div>
            <!--右侧内容 end-->
        </div>
    </div>
    <!-- content end -->
    <script>

    </script>

    <#include "/cms/myblog/semantic/block/footer.ftl"/>

</div>
</body>
</html>