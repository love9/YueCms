
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>个人设置</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>
  <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>


</head>

<body  class="pushable">
<!-- header start -->

<!-- header end -->
<!--手机试图导航-->
<#include "/cms/myblog/semantic/block/mobileNavSidebar.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->

    <div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
        <div class="ui container" style="background: #fff;">
            <div class="ui internally grid web-blog">
                <div class="row" >

                    <div class="two wide column computer only left-channel">
<!-- 侧边导航栏 -->
<#include "/cms/myblog/semantic/admin/personal_menu.ftl"/>
                    </div>

                    <!-- 内容区域 -->
                    <div class="fourteen wide computer eleven wide tablet sixteen wide mobile column  ">

                        <div class="ui list">
                            <div class="item">
                                <h3 class="ui header left floated">个人设置</h3>
                            </div>

                        </div>
                        <div class="ui basic padded segment">
                            <form class="ui blog-settings form">
                                <div class="field">
                                    <label>页面UI</label>
                                    <div class="inline fields">
                                        <div class="field">
                                            <div class="ui radio checkbox">
                                                <input type="radio" name="ui_type" checked value="semantic" >
                                                <label>semanticUI</label>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="ui radio checkbox checked">
                                                <input type="radio" name="ui_type" value="" >
                                                <label>默认UI</label>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $(function(){

                                            $("input[type='radio'][name='ui_type']").on("change",function() {
                                                if("localStorage" in window && null !== window.localStorage){
                                                    localStorage.setItem("ui_type", $(this).val());
                                                }
                                            });
                                        });

                                    </script>
                                </div>

                                <div class="field">
                                    <label>默认编辑器</label>
                                    <div class="inline fields">
                                        <div class="field">
                                            <div class="ui radio checkbox">
                                                <input type="radio" name="blog_editor" value="3" class="hidden" checked="" tabindex="0">
                                                <label>Markdown 编辑器</label>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="ui radio checkbox checked">
                                                <input type="radio" name="blog_editor" value="4" class="hidden" tabindex="0">
                                                <label>HTML 编辑器</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label>文章打赏<span class="tip">（为所有博客文章启用、关闭打赏功能）</span></label>
                                    <div class="inline fields">
                                        <div class="field">
                                            <div class="ui radio checkbox checked">
                                                <input type="radio" name="donate_switch" value="on" class="hidden" checked="" tabindex="0">
                                                <label>启用</label>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="ui radio checkbox">
                                                <input type="radio" name="donate_switch" value="off" class="hidden" tabindex="0">
                                                <label>关闭</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ten wide field">
                                    <label>打赏描述<span class="tip">（最多100字）</span></label>
                                    <input type="text" name="donate_message" placeholder="请输入打赏描述" value="如果觉得我的文章对您有用，请随意打赏。您的支持将鼓励我继续创作！">
                                </div>
                                <div class="ui positive mini basic message osc hidden"></div>
                                <div class="ui negative mini basic message osc hidden"></div>
                                <div class="ui primary  submit button">保存修改</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<!-- content end -->

<#include "/cms/myblog/semantic/block/footer.ftl"/>

</body>
</html>