<!--评论区域 start-->
<div id="comment_area" class="ui container " style="background-color: #fff;padding:10px 18px 0px 18px;margin-top:15px;padding-bottom:15px;margin: 0 auto;">
    <h4 class="ui dividing header">评论文章</h4>
    <#if (article?? && article.comment_flag?? && article.comment_flag==1)>
        <#if front_yhmc?? >
            <form class="ui reply form">
                <div class="field">
                    <textarea rows="5" placeholder="说点什么吧..." id="comment_content"></textarea>
                </div>
                <div  onclick="comment_blog()" class="ui blue labeled submit icon button">
                    <i class="icon edit"></i> 发表评论
                </div>
            </form>
        <#else>
        <form class="ui reply form">
            <div class="field">
                <textarea readonly></textarea>
                <div style="margin:0 auto;position: absolute;top:50%;left:50%;transform: translate(-50%,-100%); ">
                    <small> 你需要登录后才能进行评论</small>
                    <div class="ui tiny buttons" style="line-height: 0.8;">
                        <button class="ui positive basic button" onclick="commentLogin()">登录</button>
                        <button class="ui orange  basic button" onclick="javascript:alert('功能在开发中...');">注册</button>
                    </div>
                </div>
            </div>
            <div    class="ui  labeled submit icon button">
                <i class="icon edit"></i> 发表评论
            </div>
        </form>
    </#if>

    <!--评论列表 start-->
    <#include "/cms/myblog/semantic/comment/comment-list.ftl"/>
    <!--评论列表 end-->

    <script type="text/javascript">
        function comment_blog(){
            var comment_content = document.getElementById("comment_content").value;
            var target_id = "${article.id!}";
            if(comment_content==''){
                $.layer.msg("请输入评论内容!");
                return;
            }
            if(comment_content.length<5){
                //layerAlert("不如再多写点!");
                $.layer.msg("不如再多写点!");
                return;
            }
            var params="?id="+target_id+"&content="+comment_content;
            $.ajax({
                type: 'POST',
                url: '/myblog/blog/comment'+params,
                data: null,
                contentType: 'application/json',
                success:function(data){
                    var json=typeof data=='string'?JSON.parse(data):data;
                    console.log(data);
                    if(json.type == "success"){
                        appendComment(json);
                        $("#comment_content").val("");
                        $("#btn_comment").addClass("am-disabled").removeClass("am-btn-primary");
                        setTimeout(function(){
                            $("#btn_comment").removeClass("am-disabled").addClass("am-btn-primary");
                        },5000);
                    }else{
                        layerAlert(json.msg);
                    }
                }
            });
        }

        function appendComment(json){
            var html="<div class=\"comment\" data-id=\""+json.comment_id+"\">";
            html+=" <a class=\"avatar\">";
            html+="<img src=\""+json.headPath+"\">";
            html+="</a>";
            html+="<div class=\"content\">";
            html+="<a class=\"author\">"+json.author+"</a>";
            html+="<div class=\"metadata\">";
            html+="<span class=\"date\">"+json.time+"</span>";
            html+="</div>";
            html+="<div class=\"text\">"+json.comment+"</div>";
            html+="<div class=\"actions\">";

            html+="<a class='comment_tool_btn'  onclick='up_vote(this,"+json.comment_id+")'><i class=\"icon_comment icon_ding\"></i><span class='up_down_num' id=\"up_num_"+json.comment_id+"\">0</span><span  class=\"my-add-num\"></span></a>";
            html+="<a class='comment_tool_btn'  onclick='down_vote(this,"+json.comment_id+")'><i  class=\"icon_comment icon_cai\"></i><span class='up_down_num' id=\"down_num_"+json.comment_id+"\">0</span><span  class=\"my-add-num\"></span></a>";
            html+="<a class='comment_tool_btn'><i class=\"icon_comment icon_yinzhang\"></i></a>";
            html+="<a class='comment_tool_btn comment_tool_btn_flag' onclick='show_jubao("+json.comment_id+")'><i class=\"icon_comment icon_flag\"></i><span>举报</span></a>";
            html+="</div>";
            html+="</div>";
            html+="</div>";

            $(html).hide().prependTo($('#comment_list')).show();
        }
    </script>
<#else>
    <div class="ui container">
        <p style="text-align: center;">评论已经关闭！</p>
    </div>
</#if>



</div>
<!--评论区域 end-->