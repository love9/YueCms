
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script src="/resources/SemanticUI/semantic.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <!--下面两个是使用Code Mirror必须引入的-->
    <link rel="stylesheet" href="/resources/lib/codemirror/codemirror.css">
    <script src="/resources/lib/codemirror/codemirror.js"></script>
    <script src="/resources/lib/codemirror/addon/selection/selection-pointer.js"></script>
    <!--支持那种语言-->

    <script src="/resources/lib/codemirror/mode/javascript/javascript.js"></script>
    <script src="/resources/lib/codemirror/mode/clike/clike.js"></script>
    <script src="/resources/lib/codemirror/mode/css/css.js"></script>
    <script src="/resources/lib/codemirror/mode/xml/xml.js"></script>
    <script src="/resources/lib/codemirror/mode/vbscript/vbscript.js"></script>
    <script src="/resources/lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <!--引入css文件，用以支持主题-->
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/eclipse.css">
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/seti.css">
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/dracula.css">

    <!--支持代码折叠-->
    <link rel="stylesheet" href="/resources/lib/codemirror/addon/fold/foldgutter.css"/>
    <script src="/resources/lib/codemirror/addon/fold/foldcode.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/foldgutter.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/brace-fold.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/comment-fold.js"></script>

    <!--括号匹配-->
    <script src="/resources/lib/codemirror/addon/edit/matchbrackets.js"></script>

    <!--自动补全-->
    <link rel="stylesheet" href="/resources/lib/codemirror/addon/hint/show-hint.css">
    <script src="/resources/lib/codemirror/addon/hint/show-hint.js"></script>
    <script src="/resources/lib/codemirror/addon/hint/anyword-hint.js"></script>

    <style type="text/css">
        .CodeMirror{min-height:300px;;font-family: Menlo,Monaco,Consolas,"Andale Mono","lucida console","Courier New",monospace;}
        #textareaCode{min-height:300px}
        #iframeResult{display: block;overflow: hidden;border:0!important;min-width:100px;width:100%;min-height:300px;background-color:#fff}
        @media screen and (max-width:768px){
            #textareaCode{height:300px}.CodeMirror{height:300px;font-family: Menlo,Monaco,Consolas,"Andale Mono","lucida console","Courier New",monospace;}
            #iframeResult{height:300px}.form-inline{padding:6px 0 2px 0}
        }
</style>

</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">

      <div class="ui  " style="background: #fff;width: 90%;margin:0 auto;">
          <div class="ui internally grid web-blog">

            <div class="row" >
              <div class="eight wide computer  sixteen wide mobile column  ">

                  <div class="ui raised segment">
                      <a class="ui red ribbon label" style="top:-10px;">源代码</a>
                  <textarea id="textareaCode">

&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;head&gt;
&lt;meta charset=&quot;utf-8&quot;&gt;
	&lt;title&gt;菜鸟教程(runoob.com)&lt;/title&gt;

	&lt;link rel=&quot;stylesheet&quot; href=&quot;http://static.runoob.com/assets/js/jquery-treeview/jquery.treeview.css&quot; /&gt;
	&lt;link rel=&quot;stylesheet&quot; href=&quot;http://static.runoob.com/assets/js/jquery-treeview/screen.css&quot; /&gt;

	&lt;script src=&quot;https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js&quot;&gt;&lt;/script&gt;
	&lt;script src=&quot;http://static.runoob.com/assets/js/jquery-treeview/jquery.cookie.js&quot;&gt;&lt;/script&gt;
	&lt;script src=&quot;http://static.runoob.com/assets/js/jquery-treeview/jquery.treeview.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;

	&lt;script type=&quot;text/javascript&quot;&gt;
	$(document).ready(function(){
		$(&quot;#browser&quot;).treeview({
			toggle: function() {
				console.log(&quot;%s was toggled.&quot;, $(this).find(&quot;&gt;span&quot;).text());
			}
		});

		$(&quot;#add&quot;).click(function() {
			var branches = $(&quot;&lt;li&gt;&lt;span class='folder'&gt;New Sublist&lt;/span&gt;&lt;ul&gt;&quot; +
				&quot;&lt;li&gt;&lt;span class='file'&gt;Item1&lt;/span&gt;&lt;/li&gt;&quot; +
				&quot;&lt;li&gt;&lt;span class='file'&gt;Item2&lt;/span&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&quot;).appendTo(&quot;#browser&quot;);
			$(&quot;#browser&quot;).treeview({
				add: branches
			});
		});
	});
	&lt;/script&gt;
	&lt;/head&gt;
	&lt;body&gt;

	&lt;h1 id=&quot;banner&quot;&gt;jQuery Treeview 简单实例&lt;/h1&gt;
	&lt;div id=&quot;main&quot;&gt;



	&lt;ul id=&quot;browser&quot; class=&quot;filetree treeview-famfamfam&quot;&gt;
		&lt;li&gt;&lt;span class=&quot;folder&quot;&gt;Folder 1&lt;/span&gt;
			&lt;ul&gt;
				&lt;li&gt;&lt;span class=&quot;folder&quot;&gt;Item 1.1&lt;/span&gt;
					&lt;ul&gt;
						&lt;li&gt;&lt;span class=&quot;file&quot;&gt;Item 1.1.1&lt;/span&gt;&lt;/li&gt;
					&lt;/ul&gt;
				&lt;/li&gt;
				&lt;li&gt;&lt;span class=&quot;folder&quot;&gt;Folder 2&lt;/span&gt;
					&lt;ul&gt;
						&lt;li&gt;&lt;span class=&quot;folder&quot;&gt;Subfolder 2.1&lt;/span&gt;
							&lt;ul id=&quot;folder21&quot;&gt;
								&lt;li&gt;&lt;span class=&quot;file&quot;&gt;File 2.1.1&lt;/span&gt;&lt;/li&gt;
								&lt;li&gt;&lt;span class=&quot;file&quot;&gt;File 2.1.2&lt;/span&gt;&lt;/li&gt;
							&lt;/ul&gt;
						&lt;/li&gt;
						&lt;li&gt;&lt;span class=&quot;folder&quot;&gt;Subfolder 2.2&lt;/span&gt;
							&lt;ul&gt;
								&lt;li&gt;&lt;span class=&quot;file&quot;&gt;File 2.2.1&lt;/span&gt;&lt;/li&gt;
								&lt;li&gt;&lt;span class=&quot;file&quot;&gt;File 2.2.2&lt;/span&gt;&lt;/li&gt;
							&lt;/ul&gt;
						&lt;/li&gt;
					&lt;/ul&gt;
				&lt;/li&gt;
				&lt;li class=&quot;closed&quot;&gt;&lt;span class=&quot;folder&quot;&gt;Folder 3 (closed at start)&lt;/span&gt;
					&lt;ul&gt;
						&lt;li&gt;&lt;span class=&quot;file&quot;&gt;File 3.1&lt;/span&gt;&lt;/li&gt;
					&lt;/ul&gt;
				&lt;/li&gt;
				&lt;li&gt;&lt;span class=&quot;file&quot;&gt;File 4&lt;/span&gt;&lt;/li&gt;
			&lt;/ul&gt;
		&lt;/li&gt;
	&lt;/ul&gt;

	&lt;button id=&quot;add&quot;&gt;Add!&lt;/button&gt;



&lt;/div&gt;

&lt;/body&gt;&lt;/html&gt;
                  </textarea>

                    <script type="text/javascript">
                        var codeEdit;
                        var mixedMode = {
                            name: "htmlmixed",
                            scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,
                                mode: null},
                                {matches: /(text|application)\/(x-)?vb(a|script)/i,
                                    mode: "vbscript"}]
                        };
                        function createEditor(){
                            codeEdit=CodeMirror.fromTextArea(document.getElementById("textareaCode"),{

                                // mode:"text/javascript",
                                mode:mixedMode,
                                selectionPointer: true,
                                //显示行号
                                lineNumbers:true,
                                // styleActiveLine: true, //line选择是是否加亮
                                selectionPointer: true,
                                //设置主题
                                theme:"default",
                                //绑定Vim
                                // keyMap:"vim",
                                //代码折叠
                                lineWrapping:true,
                                foldGutter: true,
                                gutters:["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
                                //全屏模式
                                fullScreen:true,
                                //括号匹配
                                matchBrackets:true
                                //extraKeys:{"Ctrl-Space":"autocomplete"}//ctrl-space唤起智能提示
                            });
                        }


                        function setValue(v){
                            codeEdit.setValue(v);
                        }
                        function getValue(){
                            var v=  codeEdit.getValue();
                            // alert(v);
                            return v;
                        }

                        function submitTryit() {
                            var text = codeEdit.getValue();
                            var patternHtml = /<html[^>]*>((.|[\n\r])*)<\/html>/im
                            var patternHead = /<head[^>]*>((.|[\n\r])*)<\/head>/im
                            var array_matches_head = patternHead.exec(text);
                            var patternBody = /<body[^>]*>((.|[\n\r])*)<\/body>/im;

                            var array_matches_body = patternBody.exec(text);
                            var basepath_flag = 1;
                            var basepath = '';
                            if(basepath_flag) {
                                basepath = '<base href="http://localhost:8099/" target="_blank">';
                            }
                            if(array_matches_head) {
                                text = text.replace('<head>', '<head>' + basepath );
                            } else if (patternHtml) {
                                text = text.replace('<html>', '<head>' + basepath + '</head>');
                            } else if (array_matches_body) {
                                text = text.replace('<body>', '<body>' + basepath );
                            } else {
                                text = basepath + text;
                            }
                            var ifr = document.createElement("iframe");
                            ifr.setAttribute("frameborder", "0");
                            ifr.setAttribute("id", "iframeResult");
                            document.getElementById("iframewrapper").innerHTML = "";
                            document.getElementById("iframewrapper").appendChild(ifr);

                            var ifrw = (ifr.contentWindow) ? ifr.contentWindow : (ifr.contentDocument.document) ? ifr.contentDocument.document : ifr.contentDocument;
                            ifrw.document.open();
                            ifrw.document.write(text);
                            ifrw.document.close();
                            autodivheight();
                        }

                        function autodivheight(){
                            var winHeight=0;
                            if (window.innerHeight) {
                                winHeight = window.innerHeight;
                            } else if ((document.body) && (document.body.clientHeight)) {
                                winHeight = document.body.clientHeight;
                            }
                            //通过深入Document内部对body进行检测，获取浏览器窗口高度
                            if (document.documentElement && document.documentElement.clientHeight) {
                                winHeight = document.documentElement.clientHeight;
                            }
                            height = winHeight*0.68
                            codeEdit.setSize('100%', height);
                            document.getElementById("iframeResult").style.height= height +"px";
                        }
                        $(function(){

                            createEditor();
                            submitTryit();
                        });
                    </script>
                  </div>
              </div>
              <div class="eight wide computer  sixteen wide mobile column  ">
                  <div class="ui raised segment">
                      <a class="ui green ribbon label" style="top:-10px;">运行效果</a>
                  <div id="iframewrapper"></div>
                      </div>
              </div>
            </div>
              <div class="row centered">
                  <button class="ui green button" onclick="submitTryit()">点击运行</button>
              </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</div>
</body>
</html>