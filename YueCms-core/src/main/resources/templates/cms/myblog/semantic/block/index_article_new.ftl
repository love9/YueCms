<div class="blog-sidebar-widget"  >
    <h3 class="blog-sidebar-widget-title" style="margin-bottom: 0;"><b>最新</b></h3>
    <div class="blog-sidebar-widget-content" id="">
        <div class="ulGrid point">
            <ul>
                
<#if news??>

    <#list news as item>

        <#if item.static_url?? && item.static_url!="">
                <li><a href="${item.static_url!}" title="${item.title!}" target="_blank"><i></i>${item.title!}</a></li>
        <#else>
                <li><a href="/myblog/article/detail/${item.id!}" title="${item.title!}" target="_blank"><i></i>${item.title!}</a></li>
        </#if>
    </#list>
<#else>
                <li><a href="http://www.duoguyu.com/continent/157.html" title="北京租房故事" target="_blank"><i></i>北京租房故事</a></li>
                <li><a href="http://www.duoguyu.com/echo/131.html" title="删除是一种体面的告别" target="_blank"><i></i>删除是一种体面的告别</a></li>
                <li><a href="http://www.duoguyu.com/echo/177.html" title="给我5000万，也别想让我生孩子" target="_blank"><i></i>给我5000万，也别想让我生孩子</a></li>
                <li><a href="http://www.duoguyu.com/continent/176.html" title="早熟" target="_blank"><i></i>早熟</a></li>
                <li><a href="http://www.duoguyu.com/echo/175.html" title="程序员漫游指南" target="_blank"><i></i>程序员漫游指南</a></li>
</#if>

            </ul>
        </div>
    </div>
    <div class="adGrid" ><a href="#"   title="创意大会"><img src="/resources/myblog/images/cydh.jpg"></a></div>
</div>