
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
  <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
  <link rel="stylesheet" href="/resources/myblog/demo/myblogDemo.css">

  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>

  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>

    <link rel="stylesheet" href="/front/demo/jBox/Source/plugins/Notice/jBox.Notice.css">
    <link rel="stylesheet" href="/front/demo/jBox/Source/themes/NoticeFancy.css">
    <link rel="stylesheet" href="/front/demo/jBox/Source/jBox.css">
    <script src="/front/demo/jBox/Source/jBox.js"></script>
    <script src="/front/demo/jBox/Source/plugins/Notice/jBox.Notice.js"></script>
    <script src="/front/demo/jBox/jBoxNitice.js"></script>

    <link href="http://cdn.bootcss.com/highlight.js/8.0/styles/monokai_sublime.min.css" rel="stylesheet">
    <script src="http://cdn.bootcss.com/highlight.js/8.0/highlight.min.js"></script>

</head>
<body class="demo" class="pushable">
<#include "/cms/myblog/semantic/demo/demo_mobileNav.ftl"/>
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
<!-- nav start -->
<#include "/cms/myblog/semantic/block/nav.ftl"/>
<!-- nav end -->
<div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
      <div class="ui container" style="background: #fff;">
          <div class="ui internally grid web-blog">
            <div class="row" >
              <div class="twelve wide computer  sixteen wide mobile column  ">

                  <p id="test_target">封装了2种消息提示方法：jBox_simpleMsg(msg,position)、jBox_Msg(msg,title,position)。第二个方法是第个方法扩展，额外实现了标题、关闭按钮、消息关闭进度条等</p>
                  <form class="ui basic form">
                          <div class="field">
                              <label for="msg">消息内容：</label>
                              <input type="text" class="" id="msg" value="这里是消息内容" >
                          </div>
                          <div class="field">
                              <label for="title">消息标题：</label>
                              <input type="text" class="" id="title" value="这里是消息标题" >
                          </div>
                          <label >消息位置：
                              <label for="lt"><input type="radio" name="position" value="lt" id="lt">左上</label>&nbsp;
                              <label for="lb"><input type="radio" name="position" value="lb" id="lb">左下</label>&nbsp;
                              <label for="rt"><input type="radio" name="position" value="rt" id="rt">右上</label>&nbsp;
                              <label for="rb"><input type="radio" name="position" value="rb" id="rb" checked>右下</label>&nbsp;
                          </label>

                  </form>
                  <p style="margin-top: 15px;">
                      <button id="btn1"  type="button"  class="ui button primary">jBox_simpleMsg</button>
                      <button id="btn2"  type="button"  class="ui button primary">jBox_Msg</button>
                  </p>
<p style="margin-top: 15px;">本示例需要引入的文件：
<xmp style="display: none;" id="data">
    <link rel="stylesheet" href="/front/demo/jBox/Source/jBox.css">
    <link rel="stylesheet" href="/front/demo/jBox/Source/plugins/Notice/jBox.Notice.css">
    <!--美化提示框的css-->
    <link rel="stylesheet" href="/front/demo/jBox/Source/themes/NoticeFancy.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/front/demo/jBox/Source/jBox.js"></script>
    <script src="/front/demo/jBox/Source/plugins/Notice/jBox.Notice.js"></script>
    <!--个人封装的js-->
    <script src="/front/demo/jBox/jBoxNitice.js"></script>
</xmp>
<pre>
<code id="code">

</code>
</pre>
</p>
<script type="text/javascript">

    function escapeHTML(html) {
        return html.replace(/</g, "&lt").replace(/>/g, "&gt").replace(/ /g, " ").replace(/"/g, "\"").replace(/'/g, "'");
    }
    $(function(){
       var html=$("#data").html();
       var data=escapeHTML(html);
        $("#code").html(data);

        $('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
        });


    })

    $("#btn1").click(function(){
        var position=$("input[name='position']:checked").val();

        jBox_simpleMsg($("#msg").val(),position);
    });
    $("#btn2").click(function(){
        var position=$("input[name='position']:checked").val();
        jBox_Msg($("#msg").val(),$("#title").val(),position);
    });
</script>

              </div>
              <div class="four wide column computer only">
                <#include "/cms/myblog/semantic/demo/demo_right.ftl"/>
              </div>
            </div>
          </div>
      </div>
</div>
<#include "/cms/myblog/semantic/block/footer.ftl"/>
<#include "/cms/myblog/semantic/block/goto-top.ftl"/>
<#include "/cms/myblog/semantic/block/baiduTj.ftl"/>
</div>
</body>
</html>