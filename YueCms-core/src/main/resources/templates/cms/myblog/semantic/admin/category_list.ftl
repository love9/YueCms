
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="shortcut icon" href="/favicon.ico">

    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>


</head>

<body  class="pushable">
<!-- header start -->

<!-- header end -->

<!--手机试图导航-->
<div class="ui left inverted sidebar vertical menu" id="mobileNavSidebar" style="display: none;">
    <a class="item" href="/myblog/index">首页</a>
    <a class="item" href="/myblog/aboutMe">关于我</a>
    <a class="item" href="/myblog/bloglist">博客</a>

    <#if front_yhmc??>
    <a class="item" href="/myblog/meitu">关于我</a>
    </#if>
    <a id="nav_book" class="item" href="/myblog/booklist">读书</a>
    <a class="item" href="/myblog/article-fullwidth">全宽页面</a>
    <a class="item" href="/myblog/updateLog">更新日志</a>
    <a class="item" href="/myblog/demo/201807/liuyan">留言</a>
</div>

<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->
    <#include "/cms/myblog/semantic/block/nav.ftl"/>
    <!-- nav end -->

    <div class="ui basic segment" id="mainScreen" style="margin-top: 50px;">
        <div class="ui container" style="background: #fff;">
            <div class="ui internally grid web-blog">
                <div class="row" >
                    <div class="two wide column computer only left-channel">
                        <!-- 侧边导航栏 -->
<#include "/cms/myblog/semantic/admin/personal_menu.ftl"/>
                    </div>

                    <!-- 内容区域 -->
                    <div class="fourteen wide computer eleven wide tablet sixteen wide mobile column blog-list-wrap">

                        <div class="ui list">
                            <div class="item">
                                <h3 class="ui header left floated">个人分类管理</h3>
                                <form class="ui basic segment form right floated" style="margin: 0;padding:0;">
                                    <div class="two fields">
                                        <div class="inline field">
                                            <input placeholder="分类名称" id="name" name="name" type="text">
                                        </div>

                                        <a onclick="addCategory()"  class="ui primary    button">添加分类</a>

                                    </div>

                                </form>
                            </div>

                        </div>
                    <!--    <div class="ui divider" style="margin-top: 0px;top:-5px;"></div>-->
                        <div class="row">
                            <div class="ui divided  items" id="personalCategoryList">
                                <div class="item header">
                                    <div class="middle aligned content">
                                        <span class="sort">序号</span>
                                        <span class="name">类别</span>
                                        <span class="operate">操作</span>
                                        <span class="available">前台是否显示</span>
                                    </div>
                                </div>

                                <#if category_list??>
                                <#list category_list as item>

                                <div class="item" data-id="${item.id!}">
                                    <div class="middle aligned content">
                                        <span class="sort">${velocityCount!}</span>
                                        <span class="name">${item.name!}</span>
                                        <span class="operate" >
                                            <a class="edit" onclick="editCategory('${item.id!}')">编辑</a> | <a class="delete"  onclick="deleteCategory('${item.id!}')">删除</a>
                                        </span>
                                        <span class="available">
                                            <div class="ui  toggle checkbox">
                                                <input type="checkbox" checked name="public">
                                                <label> </label>
                                            </div>
                                        </span>
                                    </div>
                                </div>

                                </#list>
                                </#if>

                            </div>




                                <script type="text/javascript">

                                    function addCategory(){
                                        layer.msg("该功能正在开发中。。");
                                    }

                                    function editCategory(id){
                                        layer.msg("该功能尚未实现!^_^");
                                    }

                                    function deleteCategory(id){
                                        layer.msg("该功能正在开发中。。");
                                        return;
                                        Fast.confirm("确定要删除该文章吗?",function(){
                                            sys_ajaxPost("/myblog/admin/post_delete",{id:id},function(data){
                                                var json=typeof data=='string'?JSON.parse(data):data;
                                                var type=json.type;
                                                if(type=='success'){
                                                    layerAlert(json.content,function(){
                                                        window.location.href="/myblog/admin/postlist";
                                                    });
                                                }else{
                                                    layerAlert(json.content);
                                                }
                                            });
                                        });
                                    }
                                </script>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<!-- content end -->

<#include "/cms/myblog/semantic/block/footer.ftl"/>

</body>
</html>