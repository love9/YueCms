<!-- banner start -->
<style>
    .owl-theme .owl-controls .owl-page span {
        background: #35b871 !important;
    }
    .owl-theme .owl-controls {
        margin-top: 0px!important;
    }
</style>
 <link rel="stylesheet" href="/resources/lib/owlCarousel/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/lib/owlCarousel/css/owl.theme.css">
<script src="/resources/lib/owlCarousel/owl.carousel.min.js"></script>
<div class="focusWrap swiper-container swiper-container-fade swiper-container-horizontal swiper-container-wp8-horizontal" id="indexFocusGrid">
    <div class="item">
        <a href="/front/static/article/b8340e2a9b8cd9d0119a93ea30d9dce2.html" title="滑板少女" target="_blank">
            <img class="img" src="/resources/images/banner/180828012300500.jpg" data-src="/resources/images/banner/180828012300500.jpg">
        </a>
    </div>
    <div class="item">
            <a href="http://www.duoguyu.com/continent/188.html" title="我高中的最后一小时" target="_blank">
                <img class="img" src="/resources/images/banner/180909053041227.jpg" data-src="/resources/images/banner/180909053041227.jpg">
            </a>
</div>
    <div class="item">
            <a href="/front/static\article\682b25402d441f52b41a479e93c50120.html" title="早熟" target="_blank">
                <img class="img" src="/resources/images/banner/180829122443368.jpg" data-src="/resources/images/banner/180829122443368.jpg">
            </a>
    </div>
        <div class="item">
            <a href="/front/static/article/33e53137bcf2ce5477efc7c0c0c5be96.html" title="程序员漫游指南" target="_blank">
                <img class="img" src="/resources/images/banner/180828105347992.jpg" data-src="/resources/images/banner/180828105347992.jpg">
            </a>
        </div>

   <#-- <div class="overlay index"></div>
    <div class="focusNext swiper-button-next swiper-button-white"></div>
    <div class="focusPrev swiper-button-prev swiper-button-white"></div>
    <div class="indexPartyPagination commonSwiperPagination swiper-pagination swiper-pagination-bullets" style="bottom: 0px;" id="indexFocusPagination"><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>-->
</div>

<script>

    $(function(){
        /*var indexFocus = new Swiper('#indexFocusGrid',{
            loop: true, effect: 'fade', autoplay:{delay:5000,disableOnInteraction:false}, navigation: {nextEl:'.focusNext',prevEl:'.focusPrev'}, pagination: {el:'#indexFocusPagination'}
        });*/
        $('#indexFocusGrid').owlCarousel({
            items: 1,
            autoPlay: true,
            navigation: false,
            navigationText: ["上一个", "下一个"],
            autoHeight: true,
            singleItem: true
        });
        /*   var indexParty = new Swiper('#indexParty',{
               loop: true, effect: 'fade', autoplay:{delay: 4000,disableOnInteraction: false}, navigation: {nextEl:'.indexPartyNext',prevEl:'.indexPartyPrev'}, pagination: {el:'#indexPartyPagination'}
           });*/
    });

</script>
<!--<div id="indexGroup"  style="padding: 0 !important;margin: 0 -18px;">
    <div data-am-widget="slider" class="am-slider am-slider-b1" data-am-slider='{&quot;controlNav&quot;:false}' >
        <ul class="am-slides">
#if($banners and $banners.length>0)
    #foreach($banner in $banners)
            <li>
            #if($banner.static_url and $banner.static_url!='')
                <a href="$banner.static_url">
            #else
                <a href="#">
            #end
                    <img src="$banner.cover_image">
                </a>
            </li>
    #end
#else
            <li>
                <a href="#">
                    <img src="/resources/myblog/assets/i/banner/banner_1.jpg">
                    <div class="blog-slider-desc am-slider-desc ">
                        <div class="blog-text-center blog-slider-con">

                            <h1 class="blog-h-margin"><a href="">总在思考一句积极的话</a></h1>
                            <p style="margin: 0.6rem 0;">那时候刚好下着雨，柏油路面湿冷冷的，还闪烁着青、黄、红颜色的灯火。
                            </p>
                        </div>
                    </div>
                </a>
            </li>
            <li><a href="#">
                <img src="/resources/myblog/assets/i/banner/banner_2.jpg">
                <div class="am-slider-desc blog-slider-desc">
                    <div class="blog-text-center blog-slider-con">

                        <h1 class="blog-h-margin"><a href="">总在思考一句积极的话</a></h1>
                        <p style="margin: 0.6rem 0;">那时候刚好下着雨，柏油路面湿冷冷的，还闪烁着青、黄、红颜色的灯火。
                        </p>
                        <span>2015/10/9</span>
                    </div>
                </div></a>
            </li>
            <li><a href="#">
                <img src="/resources/myblog/assets/i/banner/banner_3.jpg">
                <div class="am-slider-desc blog-slider-desc">
                    <div class="blog-text-center blog-slider-con">

                        <h1 class="blog-h-margin"><a href="">总在思考一句积极的话</a></h1>
                        <p style="margin: 0.6rem 0;">那时候刚好下着雨，柏油路面湿冷冷的，还闪烁着青、黄、红颜色的灯火。
                        </p>
                        <span>2015/10/9</span>
                    </div>
                </div></a>
            </li>
            <li><a href="#">
                <img src="/resources/myblog/assets/i/banner/banner_2.jpg">
                <div class="am-slider-desc blog-slider-desc">
                    <div class="blog-text-center blog-slider-con">

                        <h1 class="blog-h-margin"><a href="">总在思考一句积极的话</a></h1>
                        <p style="margin: 0.6rem 0;">那时候刚好下着雨，柏油路面湿冷冷的，还闪烁着青、黄、红颜色的灯火。
                        </p>
                        <span>2015/10/9</span>
                    </div>
                </div></a>
            </li>
#end

        </ul>
    </div>
</div>-->
<!-- banner end -->