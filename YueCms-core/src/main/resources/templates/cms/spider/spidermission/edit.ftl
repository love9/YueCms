<!DOCTYPE html>
<html>
<head>
    <title>编辑趴取任务</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <link rel="stylesheet" href="/resources/lib/simple-switch/jquery.simple-switch.css">
    <script type="text/javascript" src="/resources/lib/simple-switch/jquery.simple-switch.js"></script>

    <#import "/base/util/macro.ftl" as macro>

</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
    <div class="container-fluid">
        <form action="" id="form_show" method="post" class="form-horizontal" role="form">
            <h2 class="text-center">编辑趴取任务</h2>
            <input type="hidden" value="${spidermission.id!}" id="id" name="id"/>

            <div class="form-group">
                <label class="col-sm-2 control-label">任务代码：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="missionCode" readonly style="width: calc(100% - 100px);float:left;" name="missionCode" placeholder="抓取任务代码"  value="${spidermission.missionCode!}" class="form-control" datatype="*" nullmsg="请输入抓取任务代码" />
                    <a style="display: block;width:85px;float:right;" type="button"   class="btn btn-default " ><i class="icon-ok"></i>检验可用</a>
                </div>
                <div class="col-sm-4">

                    <div id="missionCode_check" class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">任务名称：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="missionName" name="missionName" placeholder="抓取任务名称" value="${spidermission.missionName!}" class="form-control" datatype="*" nullmsg="请输入抓取任务名称" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">任务类型:</label>
                <div class="col-sm-6 formControls">
                    <span class="select-box form-control">
                        <select class="select" value="${spidermission.missionType!}"   nullmsg="请选择任务类型" datatype="*"   id="missionType" name="missionType">
                            <option value="">-请选择-</option>
                            <option value="article">文章</option>
                            <option value="book">书籍</option>
                            <option value="chapter">书籍章节</option>
                        </select>
                    </span>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
        <#--<div class="form-group" style="display: none;">
            <label class="col-sm-2 control-label">资源类型：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="resource_type_id"   name="resource_type_id"  value="${spidermission.resource_type_id}" class="form-control"   nullmsg="请输入资源类型" />
                <input type="text" id="resource_type_name" style="width: 75%;display: inline-block;"  readonly   name="resource_type_name"   value="${resource_type_name}" class="form-control"  nullmsg="请输入资源类型" />
                <a onclick="base_openZyflxzPage('resource_type_id','resource_type_name')" style=" width: 20%;float: right;" class="btn btn-block btn-outline btn-primary">选择</a>
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>-->


            <div class="form-group" style="display: none;">
                <label class="col-sm-2 control-label">任务状态：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="status" name="status" placeholder="任务状态" value="${spidermission.status!}" class="form-control"   nullmsg="请输入任务状态" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">入口网址：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="urls" name="urls" placeholder="需要采集的目标网页多个用分号分割" value="${spidermission.urls!}" class="form-control" datatype="*" nullmsg="请输入需要采集的目标网页多个用分号分割" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">目标URL正则：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="targetUrlReg" name="targetUrlReg" placeholder="目标url正则" value="${spidermission.targetUrlReg!}" class="form-control" datatype="*" nullmsg="请输入目标url正则" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">是否翻页：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden"  id="loopFlag" name="loopFlag" value="0" value="${spidermission.loopFlag!}" class="form-control"  />
                    <#if spidermission.loopFlag?? && spidermission.loopFlag=="1">
                        <div id="switch" class="simple-switch active">
                            <input type="checkbox" checked/>
                            <span class="switch-handler"></span>
                        </div>
                    </#if>

                    <#if spidermission.loopFlag?? && spidermission.loopFlag=="0">
                        <div id="switch" class="simple-switch">
                            <input type="checkbox"/>
                            <span class="switch-handler"></span>
                        </div>
                    </#if>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <script type="text/javascript">
                $('#switch').on('switch-change', function(e){
                            var v=$('#switch').simpleSwitch('state');
                            if(v==true||v=='true'){
                                $("#loopFlag").val(1);
                                $(".div_loop").show().find("input").attr("datatype","*");
                                $("#loopNum").attr("datatype","n");
                            }else{
                                $("#loopFlag").val(0);
                                $(".div_loop").hide().find("input").removeAttr("datatype");
                            }
                        }
                );


            </script>
            <div class="form-group div_loop" style="display: none;">
                <label class="col-sm-2 control-label">翻页参数：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="loopParam" name="loopParam" placeholder="翻页参数" value="${spidermission.loopParam!}" class="form-control"   nullmsg="请输入翻页参数" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group div_loop"  style="display: none;">
                <label class="col-sm-2 control-label">循环翻页次数：</label>
                <div class="col-sm-6 formControls">

                    <input type="text" id="loopNum" name="loopNum" placeholder="循环翻页次数" value="${spidermission.loopNum!}" class="form-control"   nullmsg="请输入循环翻页次数" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">休眠(毫秒)：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="sleep" name="sleep" placeholder="趴取每个页面之间的休眠毫秒数" value="${spidermission.sleep!}" class="form-control"  datatype="*" nullmsg="请输入每个页面之间的休眠毫秒数" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">线程数：</label>
                <div class="col-sm-6 formControls">
                <#--   <my:numberSelect  path="thread" beginNum="1" endNum="10" value="${spidermission.thread!}"  nullmsg="请输入线程数"></my:numberSelect>-->
                    <span class="select-box  form-control">
                          <#assign nums=1..10/>
                         <select class="select" value="${spidermission.thread!}" nullmsg="请输入线程数" datatype="*"   id="thread" name="thread">
                                <option value="">-请选择-</option>
                             <#list nums as i>
                                 <#if spidermission.thread==i>
                                         <option selected value="${i}">${i}</option>
                                 <#else >
                                         <option value="${i}">${i}</option>
                                 </#if>
                             </#list>
                        </select>
                    </span>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">数据管道：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" id="pipeline" name="pipeline" placeholder="数据管道" value="${spidermission.pipeline!}" class="form-control" datatype="*" nullmsg="请输入数据管道" />
                <#-- <my:dicSelect id="pipeline" changeCallBack="pipelineChange()" idAttr="pipeline"></my:dicSelect>-->

                    <@macro.dicSelect id="pipeline" value="${spidermission.pipeline!}" changeCallBack="pipelineChange()" idAttr="pipeline" />

                    <script type="text/javascript">
                        function pipelineChange(){
                            var v=$("#pipeline").val();
                            if(v=='TemplateMergerStringFilePipeline'||v=='TemplateFilePipeline'){
                                $("#div_templateName").show();
                                $("#templateName").attr("datatype","*");
                            }else{
                                $("#div_templateName").hide();
                                $("#templateName").removeAttr("datatype");
                            }
                        }
                    </script>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group" id="div_templateName" style="display: none;">
                <label class="col-sm-2 control-label">输出模板：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="templateName" name="templateName" placeholder="页面数据按照该模板输出" value="${spidermission.templateName!}" class="form-control"  nullmsg="请选择输出模板" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">限制取几条：</label>
                <div class="col-sm-6 formControls">
                    <#if spidermission.limitCount?? && (spidermission.limitCount>0)>
                             <label for="spider_limit" style="display: inline-block;width: 180px;"> <input type="checkbox" id="spider_limit"   placeholder="是否只取前几条" checked  />是否只取前几条</label>
                    <#else>
                             <label for="spider_limit" style="display: inline-block;width: 180px;"> <input type="checkbox" id="spider_limit"   placeholder="是否只取前几条"  />是否只取前几条</label>
                    </#if>

                    <input type="text" style="display: inline-block;width:calc(100% - 185px) ;" id="limitCount" name="limitCount" placeholder="请输入数字" value="${spidermission.limitCount!}" class="form-control"  nullmsg="请输入数字" />
                </div>
                <div class="col-sm-4">
                    <div id="limitCount_errorTip" class="Validform_checktip"></div>
                </div>
            </div>

            <script type="text/javascript">
                setTimeout(function(){$("#spider_limit").change();},100);
                $("#spider_limit").change(function(){
                    if($('#spider_limit').is(':checked')) {
                        $("#limitCount").show().attr("datatype","*");
                    }else{
                        $("#limitCount_errorTip").removeClass("Validform_wrong").html("");
                        $("#limitCount").hide().removeAttr("datatype");
                    }
                });
            </script>
            <div class="form-group">
                <label class="col-sm-2 control-label">图片文件夹：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="contentImgFolder" name="contentImgFolder" placeholder="图片文件夹" value="${spidermission.contentImgFolder!}" class="form-control"  />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <#--<div class="form-group">
                <div class=" col-sm-10 col-sm-offset-2">
                    <button type="button" onclick="save()" class="btn btn-primary" ><i class="icon-ok"></i>保存</button>
                </div>
            </div>-->
        </form></div>
</div>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
    var validform;
    function save(){
        var b=validform.check(false);
        if(!b)
        {
            return;
        }
        var params=$("#form_show").serialize();
        //var thread=$("#thread").val();
        //params+="&thread="+thread;
        //alert(params);return;
        $.ajax({
            type:"post",
            url:'/spider/spidermission/json/save?'+params,
            data:null,
            success:function(json,textStatus){
                ajaxReturnMsg(json);
                setTimeout(function(){
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                },1000);
            }
        });
    }
    $(function(){
        var missionType ='${spidermission.missionType}';
        $("#missionType").val(missionType);
        var loopFlag='${spidermission.loopFlag}';
        if(loopFlag==1||loopFlag=='1'){
            $(".div_loop").show().find("input").attr("datatype","*");
        }else{
            $(".div_loop").hide().find("input").removeAttr("datatype");
        }

        validform=$("#form_show").Validform({
            btnReset:"#reset",
            tiptype:2,
            postonce:true,//至提交一次
            ajaxPost:false,//ajax方式提交
            showAllError:true //默认 即逐条验证,true验证全部
        });
    })
</script>

</body>
</html>
