<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>趴取任务管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
   
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
    <script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>


</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> spider <span class="c-gray en">&gt;</span>趴取任务管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
             <div class="btn-group">
                <a onclick="addNew()" class="btn btn-sm btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-sm btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a href="javascript:void(0)"  onClick="sortMenu();" class="btn btn-sm btn-outline btn-info" ><i class="fa fa-sort"></i>&nbsp;排序</a>
                <a onclick="query('form_query');" class="btn btn-sm btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
             </div>
        </form>
        <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>

    <div class="cl">
        <form id="form_query_mx" style="margin: 8px 0px" class="form-inline">
            <div class="btn-group">
                <a onclick="addNew_mx()" class="btn btn-sm btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增明细</a>
                <a onclick="list_del_mx('tableList_mx')" class="btn btn-sm btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除明细</a>
                <a onclick="query_mx('form_query_mx');" class="btn btn-sm btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
        <!--数据列表-->
        <div class="table-responsive"><table id="tableList_mx" class="table table-striped"></table> </div>
    </div>

</div>
    <div id="div_sort" title="菜单排序" style="width:350px;height:400px;display: none;">
        <div class="c_div_tool_bar">
            <a href="javascript:void(0)" class="btn btn-primary size-S" style="margin:4px;" onClick="saveSort()">保存</a>
        </div>
        <div class="c_div_show_content">
            <form id="form_sort">
                <div id="dd" style="width:100%;">
                    <ul id="sortable">

                    </ul>
                </div>
            </form>
        </div>
    </div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var zbguid="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'orgid',align:"center",sortable:true,order:"asc",visible:false,title: '组织id'},
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:false,title: ''},
            {field: 'missionGroupId',align:"center",sortable:true,order:"asc",visible:false,title: '任务组'},
            {field: 'missionCode',align:"left",sortable:true,order:"asc",visible:true,title: '任务代码',width: '100px'},
            {field: 'missionName',align:"left",sortable:true,order:"asc",visible:true,title: '任务名称',width: '100px'},
            {field: 'missionType',align:"center",sortable:true,order:"asc",visible:false,title: '任务类型'},
            {field: 'status',align:"center",sortable:true,order:"asc",visible:true,title: '状态', width: '80px',
                formatter: function(v, row, index){
                    if(v=='1'){
                        return "<span class=\"label label-warning radius\">运行中...</span>";
                    }else if(v=='0'){
                        return "<span class=\"label label-info radius\">空闲</span>";
                    }else{
                        return "-";
                    }
                }
            },
            {field: 'urls',align:"left",sortable:true,order:"asc",visible:true,title: '网页URL'},
            {field: 'targetUrlReg',align:"left",sortable:true,order:"asc",visible:true,title: '目标URL正则'},
            {field: 'loopParam',align:"left",sortable:true,order:"asc",visible:false,title: '翻页正则'},
            {field: 'loopNum',align:"center",sortable:true,order:"asc",visible:false,title: '翻页次数',width: '85px'},
            {field: 'sleep',align:"center",sortable:true,order:"asc",visible:false,title: '休眠（毫秒）'},
            {field: 'thread',align:"center",sortable:true,order:"asc",visible:false,title: '线程数',width: '75px'},
            {field: 'pipeline',align:"left",sortable:true,order:"asc",visible:false,title: '数据管道'},
            {field: 'templateName',align:"left",sortable:true,order:"asc",visible:false,title: '模板'},
            {field: 'page_process_id',align:"center",sortable:true,order:"asc",visible:false,title: '页面数据提取策略'},
            {field: 'operate', align:"center", title: '操作', width: '120px',
                formatter: function(value, row, index){
                    var btns="<div class=\"btn-group\">";
                    btns+="<button class=\"btn btn-xs btn-outline btn-success \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-cart-plus\"></i><span class=\"bold\">编辑</span></button>";
                    btns+="<button class=\"btn btn-xs btn-outline btn-info \" onclick=\"startMission('"+row.id+"')\" type=\"button\"><i class=\"fa fa-cart-plus\"></i><span class=\"bold\">启动</span></button>";
                    btns+="</div>";
                    return btns;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.url='/spider/spidermission/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    var onClickRow=function(row,tr){
        zbguid=row.id;
        //alert(zbguid);
        if(zbguid!=''){
            sys_table_list_mx();
        }
    }
    var editRow=function(id){
        $.layer.open_page_full("编辑趴取任务",sys_ctx+"/spider/spidermission/edit?id="+id,function(){$('#tableList').bootstrapTable('refresh');})
    }
    function addNew(){
        $.layer.open_page_full("新增趴取任务",sys_ctx+"/spider/spidermission/add",function(){$('#tableList').bootstrapTable('refresh');})
    }
    var startMission=function(id){
        $.layer.confirm("确定要启动任务吗?",function(){
            $.ajax({
                type:"post",
                url:'/spider/spidermission/json/startMission/'+id,
                data:null,
                success:function(data,textStatus){
                    ajaxReturnMsg(data);
                    refreshData();
                }
            });
        },function(){});
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/spider/spidermission/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
        zbguid="";
        $('#tableList_mx').bootstrapTable('destroy');

    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
    var sortMenu = function(){
        $.ajax({
            type:"post",
            url:'/spider/spidermission/json/find',
            data:null,
            success:function(data,textStatus){

                var json=typeof data=='string'?JSON.parse(data):data;
                var str = "";
                var rows=json.rows;
                console.log(rows);
                for(var i=0;i<rows.length;i++){
                    str += '<li class="ui-state-default" id="xh_'+rows[i]['id']+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+rows[i]['missionName']+'</li>';
                };
                $("#sortable").html('').append(str);
                $("#sortable").sortable();
                $("#sortable").disableSelection();
            }
        });
        layer_openHtml("排序",$("#div_sort"),null,{width:'350px',height:'400px'})
        $("#div_sort").show();
    }

    var saveSort = function(){
        var arr = $("#sortable").sortable('toArray');
        for(var i=0;i<arr.length;i++){
            arr[i] = arr[i] + "_"+(i+1)
        }
        var sort = arr.join(',');
        $.ajax({
            type:"post",
            url:'/spider/spidermission/json/saveSort',
            data:{sort:sort},
            success:function(data,textStatus){
                ajaxReturnMsg(data);
                query("form_query");
                setTimeout(function(){
                    layer.closeAll();
                },1000);
            }
        });
    }

    var queryStr_mx="?";
    //查询功能的标签说明
    var table_list_query_form_mx = {
    };
    function sys_table_list_mx(){
        $('#tableList_mx').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'zbguid',align:"center",sortable:true,order:"asc",visible:false,title: '父表id'},
            {field: 'field',align:"center",sortable:true,order:"asc",visible:true,title: '字段'},
            {field: 'fieldname',align:"center",sortable:true,order:"asc",visible:true,title: '字段名称'},
            {field: 'datatype',align:"center",sortable:true,order:"asc",visible:true,title: '数据类型'},
            {field: 'extractbytype',align:"center",sortable:true,order:"asc",visible:true,title: '提取类型',
                formatter: function(v, row, index){
                   if(v=='constant'){
                       return "<font color='red'>常量</font>";
                   }else{
                       return v;
                   }

                }},
            {field: 'extractby',align:"left",sortable:true,order:"asc",visible:true,title: '提取规则/常量值',
                formatter: function(v, row, index){
                    if(row.extractbytype=='constant'){
                        return "<font color='red'>"+row.constant_value+"</font>";
                    }else{
                        return v;
                    }

                }
            },

            {field: 'extractby_tag',align:"center",sortable:true,order:"asc",visible:false,title: '标签'},

            {field: 'operate', align:"center", title: '操作', width: '80px',
                formatter: function(value, row, index){
                    var btns="<div class=\"btn-group\">";
                    btns+="<button class=\"btn btn-xs btn-outline  btn-success \" onclick=\"editMxRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-cart-plus\"></i><span class=\"bold\">编辑</span></button>";
                    btns+="</div>";
                    return btns;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/spider/pageProcessMx/json/find'+queryStr_mx+"&zbguid="+zbguid+"&parentid=0";
        $('#tableList_mx').bootstrapTable(table_list_Params);
    }
    var editMxRow=function(id){
        //layer_show_full("编辑页面字段配置",sys_ctx+"/spider/pageProcessMx/edit?id="+id,'','','',function(){sys_table_list_mx();});
        $.layer.open_page_full("编辑页面字段配置",sys_ctx+"/spider/pageProcessMx/edit?id="+id,function(){sys_table_list_mx();})
    }

    function addNew_mx(){
        if(isEmpty(zbguid)){
            layerAlert("请先选择主表记录!");
            return;
        }
        //layer_show_full("新增页面字段配置",sys_ctx+"/spider/pageProcessMx/add?zbguid="+zbguid,'','','',function(){sys_table_list_mx();});
        $.layer.open_page_full("新增页面字段配置",sys_ctx+"/spider/pageProcessMx/add?zbguid="+zbguid,function(){sys_table_list_mx();})
    }
    function list_del_mx(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/spider/pageProcessMx/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list_mx();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query_mx = function(formid){
        if(isEmpty(zbguid)){
            return;
        }
        queryStr_mx="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form_mx[id] != 'undefined')
            {
                table_list_query_form_mx[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr_mx+=queryStrTem;
        sys_table_list_mx();
    }

</script>
</body></html>
