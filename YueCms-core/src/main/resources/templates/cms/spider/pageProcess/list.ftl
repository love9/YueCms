<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>页面提取配置管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
    <script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>--%>


</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> spider <span class="c-gray en">&gt;</span>页面提取配置管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
        <div class="btn-group">
                <a onclick="addNew()" class="btn btn-sm btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-sm btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a onclick="query('form_query');" class="btn btn-sm btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
        </div>
        </form>
        <!--数据列表-->
        <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>
    </div>
    <div class="cl">
        <form id="form_query_mx" style="margin: 8px 0px" class="form-inline">
            <div class="btn-group">
                <a onclick="addNew_mx()" class="btn btn-sm btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增明细</a>
                <a onclick="list_del_mx('tableList_mx')" class="btn btn-sm btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除明细</a>
                <a onclick="query_mx('form_query_mx');" class="btn btn-sm btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
        <!--数据列表-->
        <div class="table-responsive"><table id="tableList_mx" class="table table-striped"></table> </div>
    </div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var zbguid="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'orgid',align:"center",sortable:true,order:"asc",visible:false,title: '组织id'},
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:false,title: ''},
            {field: 'name',align:"left",sortable:true,order:"asc",visible:true,title: '名称'},
            {field: 'description',align:"left",sortable:true,order:"asc",visible:true,title: '说明'},
            {field: 'operate', align:"center", title: '操作', width: '150px',
                formatter: function(value, row, index){
                    var btns="<div class=\"btn-group\">";
                    btns+="<button class=\"btn btn-xs btn-outline btn-success \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-cart-plus\"></i><span class=\"bold\">编辑</span></button>";
                    btns+="</div>";
                    return btns;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.singleSelect=true;
        table_list_Params.url='/spider/pageProcess/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    var onClickRow=function(row,tr){
        zbguid=row.id;
        //alert(zbguid);
       if(zbguid!=''){
           sys_table_list_mx();
       }
    }
    var editRow=function(id){
         layer_show("编辑页面提取配置",sys_ctx+"/spider/pageProcess/edit?id="+id,'','','',function(){$('#tableList').bootstrapTable('refresh');});
    }
    function addNew(){
         layer_show("新增页面提取配置",sys_ctx+"/spider/pageProcess/add",'','','',function(){$('#tableList').bootstrapTable('refresh');});
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/spider/pageProcess/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
        zbguid="";
        $('#tableList_mx').bootstrapTable('destroy');
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
    var queryStr_mx="?";
    //查询功能的标签说明
    var table_list_query_form_mx = {
    };
    function refreshData_mx() {
        $('#tableList_mx').bootstrapTable('refresh');
    }
    $(function(){
        //sys_table_list_mx();
    });
    function sys_table_list_mx(){
        $('#tableList_mx').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'zbguid',align:"center",sortable:true,order:"asc",visible:false,title: '父表id'},
            {field: 'field',align:"center",sortable:true,order:"asc",visible:true,title: '字段'},
            {field: 'fieldname',align:"center",sortable:true,order:"asc",visible:true,title: '字段名称'},
            {field: 'datatype',align:"center",sortable:true,order:"asc",visible:true,title: '数据类型'},
            {field: 'extractbytype',align:"center",sortable:true,order:"asc",visible:true,title: '提取类型'},
            {field: 'extractby',align:"center",sortable:true,order:"asc",visible:true,title: '提取规则'},
            {field: 'extractby_classorid',align:"center",sortable:true,order:"asc",visible:true,title: '元素类或ID'},
            {field: 'extractby_tag',align:"center",sortable:true,order:"asc",visible:true,title: '标签'},
            {field: 'table_name',align:"center",sortable:true,order:"asc",visible:true,title: '保存的表'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow_mx;
        table_list_Params.url='/spider/pageProcessMx/json/find'+queryStr_mx+"&zbguid="+zbguid+"&parentid=0";
        $('#tableList_mx').bootstrapTable(table_list_Params);
    }
    var onClickRow_mx=function(row,tr){
        layer_open_full("编辑页面字段配置",sys_ctx+"/spider/pageProcessMx/edit?id="+row.id,null,function(){sys_table_list_mx();});
    }
    function addNew_mx(){
        if(isEmpty(zbguid)){
            layerAlert("请先选择主表记录!");
            return;
        }
        layer_open_full("新增页面字段配置",sys_ctx+"/spider/pageProcessMx/add?zbguid="+zbguid,null,function(){sys_table_list_mx();});
    }
    function list_del_mx(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/spider/pageProcessMx/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list_mx();
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query_mx = function(formid){
        if(isEmpty(zbguid)){
            return;
        }
        queryStr_mx="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form_mx[id] != 'undefined')
            {
                table_list_query_form_mx[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr_mx+=queryStrTem;
        sys_table_list_mx();
    }
</script>
</body></html>
