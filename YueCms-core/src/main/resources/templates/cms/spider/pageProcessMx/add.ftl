<!DOCTYPE html>
<html>
<head>
  <title>新增页面字段配置</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
    <script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
    <script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">

        <input type="hidden" value="0" id="parentid" name="parentid"/>
		<h2 class="text-center">新增页面字段配置</h2>
         <div class="form-group" style="display: none;">
            <label class="col-sm-2 control-label">父表id：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="zbguid" name="zbguid" placeholder="父表id" value="${zbguid!}" class="form-control" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">字段：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="field" name="field" placeholder="字段" value="" class="form-control" datatype="*" nullmsg="请输入字段" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">字段名称：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="fieldname" name="fieldname" placeholder="字段名称" value="" class="form-control" datatype="*" nullmsg="请输入字段名称" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">数据类型：</label>
            <div class="col-sm-6 formControls">
                  <span class="select-box" style="display: inline-block;">
                           <select id="datatype"  name="datatype" class="select " datatype="*" nullmsg="请输入数据类型" >
                               <option value="text"  selected>文本</option>
                               <option value="list">集合</option>
                           </select>
                  </span>
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
              <script type="text/javascript">
                 $("#datatype").change(function(){
                     var v=$("#datatype").val();
                     if(v=="list"){
                         $(".mxlist").show();
                         $(".div_attr").hide();
                     }else{
                         $(".mxlist").hide();
                         $(".div_attr").show();
                     }
                 });
             </script>
         </div>

         <div class="form-group notlist">
            <label class="col-sm-2 control-label">提取方式：</label>
            <div class="col-sm-6 formControls">
                <span class="select-box" style="display: inline-block;">
                           <select id="extractbytype" value=""   name="extractbytype" class="select " datatype="*" nullmsg="请输入提取类型 " >
                               <option value="">--请选择--</option>
                               <option value="css">css</option>
                               <option value="constant">常量</option>
                               <option value="xpath">xpath</option>
                           </select>
                    <script type="text/javascript">
                        $("#extractbytype").change(function(){
                            var v=$("#extractbytype").val();
                            if(isEmpty(v)){
                                $(this).parent().next().hide();
                            }else{
                                $(this).parent().next().show();
                                if(v=='css'){
                                    $("#extractbytype_des").html("css规则提取数据");
                                }else if(v=='xpath'){
                                    $("#extractbytype_des").html("xpath规则提取数据");
                                }else if(v=='constant'){
                                    $("#extractbytype_des").html("该字段存放固定值");
                                }else{
                                    $("#extractbytype_des").html("");
                                }

                                if(v=='css'||v=='xpath'){
                                    $("#div_constant").hide();
                                    $("#div_extractby").show();

                                   // $(".div_extractby_attr").show();
                                   // $("#extractby_attr").removeAttr("ignored");

                                    $("#div_extractby_attr_flag").show();

                                    $("#extractby").attr("datatype","*");
                                    $("#extractby").attr("nullmsg","请输入提取规则");

                                    $("#constant_value").removeAttr("datatype","*");
                                    $("#constant_value").removeAttr("nullmsg","请输入常量值");
                                }else{//constant 常量
                                    $("#div_constant").show();

                                    $("#constant_value").attr("datatype","*");
                                    $("#constant_value").attr("nullmsg","请输入常量值");

                                    $("#div_extractby").hide();
                                    $("#extractby").removeAttr("datatype","*");
                                    $("#extractby").removeAttr("nullmsg","请输入提取规则");

                                    $("#div_extractby_attr_flag").hide();

                                    $(".div_extractby_attr").hide();
                                    $("#extractby_attr").attr("ignored","ignored");

                                    $("#div_extractby_index").hide();
                                    $("#extractby_index").attr("ignored","ignored");
                                }
                            }
                        });
                    </script>
                  </span>
                   <span id="extractbytype_des"></span>
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group  notlist"  id="div_extractby"  style="display: none">
            <label class="col-sm-2 control-label">提取规则：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="extractby" name="extractby" placeholder="提取规则" value="" class="form-control"   />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>

        <div class="form-group  notlist"  id="div_extractby_index"  >
            <label class="col-sm-2 control-label">索引：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="extractby_index" name="extractby_index" placeholder="索引" value="" class="form-control" datatype="*" nullmsg="请输入目标索引" />
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <div class="form-group mxlist" style="display: none;">
            <label class="col-sm-2 control-label">集合明细：</label>
            <div class="col-sm-10 formControls">
                <!--数据列表-->
                <div class="btn-group">
                    <a class="btn btn-sm btn-success" onclick="show_addline()">增加一行</a>
                </div>
                <div class="table-responsive">
                    <table id="mxlist" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="display: none;">ID</th>
                            <th style='text-align:center;'>字段</th>
                            <th style='text-align:center;'>名称</th>
                            <th  style='text-align:center;'>方式</th>
                            <th  style='text-align:center;'>提取规则/常量值</th>

                            <th style="width:75px; text-align:center; ">索引</th>
                            <th  style='text-align:center;'>获取属性值</th>
                            <th  style='text-align:center;'>属性名</th>
                            <th style="display: none;">描述</th>
                            <th  style='text-align:center;'>操作</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group div_attr" id="div_extractby_attr_flag">
            <label class="col-sm-2 control-label">根据属性获取：</label>
            <div class="col-sm-6 formControls">
                <label for="extractby_attr_flag"> <input type="checkbox" id="extractby_attr_flag"   placeholder="是否根据属性获取" value=""  />是否根据属性获取</label>
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <script type="text/javascript">
            $("#extractby_attr_flag").change(function(){
                if($('#extractby_attr_flag').is(':checked')) {
                    $(".div_extractby_attr").show().find("input").attr("datatype","*");
                }else{
                    $(".div_extractby_attr").hide().find("input").removeAttr("datatype");
                }
            });
        </script>
        <div class="form-group  div_extractby_attr div_attr" style="display: none;">
            <label class="col-sm-2 control-label">属性名称：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="extractby_attr" name="extractby_attr" placeholder="请输入属性名称" value="" class="form-control"   nullmsg="请输入属性名称" />
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <#--<div class="form-group" style="display: none;">
            <label class="col-sm-2 control-label">保存的表：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="table_name" name="table_name" placeholder="保存的表" value="" class="form-control"  />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>-->
        <div class="form-group" id="div_constant" style="display: none;">
            <label class="col-sm-2 control-label">常量值：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="constant_value" name="constant_value" placeholder="请输入常量值" value="" class="form-control"  />
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <div class="form-group" style="display: none">
            <label class="col-sm-2 control-label">数据处理规则：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="fieldprocessrule_id" name="fieldprocessrule_id" placeholder="数据处理规则" value="" class="form-control"  />
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
		<#--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-2">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form></div>
</div>


<div id="modal-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <form id="form_show2" class="form-horizontal" role="form">
            <div class="modal-content">
            <style type="text/css">
                .modal-content .modal-header h3{margin:1px 0;}
            </style>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>对话框标题</h3>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">字段：</label>
                        <div class="col-sm-6 formControls">
                            <input type="text" id="line_field" name="line_field"  placeholder="字段" datatype="*" nullmsg="请输入字段" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>
                    <div class="form-group formControls">
                        <label class="col-sm-2 control-label">名称：</label>
                        <div class="col-sm-6 formControls">
                            <input type="text" id="line_fieldname" name="line_fieldname"  placeholder="名称" datatype="*" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">方式：</label>
                        <div class="col-sm-6 formControls">
                           <span class="select-box" style="display: inline-block;">
                           <select id="line_extractbytype"     name="line_extractbytype" class="select " datatype="*"   nullmsg="请输入提取类型 " >
                               <option value="">--请选择--</option>
                               <option value="css">css</option>
                               <option value="constant">常量</option>

                           </select>
                    <script type="text/javascript">
                        $("#line_extractbytype").change(function(){
                            var v=$("#line_extractbytype").val();
                            if(isEmpty(v)){
                                $(this).parent().next().hide();
                            }else{
                                $(this).parent().next().show();
                                if(v=='css'){
                                    $("#line_extractbytype_des").html("css规则提取数据").show();
                                }else if(v=='xpath'){
                                    $("#line_extractbytype_des").html("xpath规则提取数据").show();
                                }else if(v=='constant'){
                                    $("#line_extractbytype_des").html("该字段存放固定值");
                                }else{
                                    $("#line_extractbytype_des").html("").hide();
                                }
                                if(v=='css'||v=='xpath'){
                                    $("#line_div_constant").hide();
                                    $(".line_div_not_constant").show();

                                    $("#line_extractby").removeAttr("ignored");
                                }else{

                                    $("#line_div_constant").show();
                                    $(".line_div_not_constant").hide();
                                    $("#line_div_extractby_attr").hide();
                                    $("#line_extractby").attr("ignored","ignored");
                                }
                            }
                        });
                    </script>
                  </span>
                            <span id="line_extractbytype_des"></span>
                        </div>

                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>
                    <div class="form-group line_div_not_constant" id="line_div_extractby">
                        <label class="col-sm-2 control-label">规则：</label>
                        <div class="col-sm-6 formControls">
                            <input type="text" id="line_extractby" name="line_extractby"  placeholder="规则" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>
                    <div class="form-group  " id="line_div_constant"  style="display: none;">
                        <label class="col-sm-2 control-label">常量值：</label>
                        <div class="col-sm-6 formControls">
                            <input type="text" id="line_constant_value" name="line_constant_value" placeholder="常量值"  class="form-control"  nullmsg="请输入常量值" />
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>

                    <div class="form-group line_div_not_constant"  >
                        <label class="col-sm-2 control-label">索引：</label>
                        <div class="col-sm-6 formControls">
                            <input type="text" id="line_extractby_index" name="line_extractby_index" placeholder="索引" value="0"   class="form-control"  nullmsg="请输入目标索引" />
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>

                    <div class="form-group line_div_not_constant">
                        <label class="col-sm-2 control-label">根据属性获取：</label>
                        <div class="col-sm-6 formControls">
                            <label for="line_extractby_attr_flag"> <input type="checkbox" id="line_extractby_attr_flag"   placeholder="是否根据属性获取"    />是否根据属性获取</label>
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $("#line_extractby_attr_flag").change(function(){
                            if($('#line_extractby_attr_flag').is(':checked')) {
                                $(".line_div_extractby_attr").show().find("input").attr("datatype","*");
                            }else{
                                $(".line_div_extractby_attr").hide().find("input").removeAttr("datatype");
                            }
                        });

                    </script>

                    <div class="form-group  line_div_extractby_attr" style="display: none;">
                        <label class="col-sm-2 control-label">属性名称：</label>
                        <div class="col-sm-6 formControls">
                            <input type="text" id="line_extractby_attr" name="line_extractby_attr" placeholder="请输入属性名称"   class="form-control"   nullmsg="请输入属性名称" />
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>

                    <div class="form-group" style="display:none;">
                        <label   class="col-sm-2 control-label">描述：</label>
                        <div class="col-sm-6 formControls">
                            <textarea id="line_extractby_description" name="line_extractby_description" rows="3" style="width: 100%" class="form-control"  placeholder="类型的描述"></textarea>
                        </div>
                        <div class="col-sm-4">
                            <div class="Validform_checktip"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center">
                    <a id="saveBtn"  class="btn btn-primary" onclick="addline()">确定</a>
                    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript">
    var sys_ctx="";
	var validform;
    var validform2;
	function save(){
        var zbguid=$("#zbguid").val();
        if(isEmpty(zbguid)){
            $.layer.msg_wrong("表单校验不通过!zbguid为空!");
            return;
        }
        var v=$("#datatype").val();
        console.log(1);
        var mxs;
        if(v=="list"){
             mxs=getMx();
           // alert(JSON.stringify(mxs));
            if(mxs.length==0){
                layerAlert("请添加明细！");
                return;
            }
            mxs=JSON.stringify(mxs)
        }
        console.log(2);
	    var b=validform.check(false);
		if(!b)
		{
            console.log(3);
		    $.layer.msg("表单校验不通过!");
            $("#form_show ").show();
			return;
		}
        console.log(4);
		var params=$("#form_show").serialize();
        if($('#extractby_attr_flag').is(':checked')) {
            params+="&extractby_attr_flag=1"
        }else{
            params+="&extractby_attr_flag=0"
        }
        console.log(params);
		$.ajax({
			type:"post",
			url:'/spider/pageProcessMx/json/save?'+params,
			data:{mxs:mxs},
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}

    //增加一行
    function show_addline(){
        $('#modal-form').on('show.bs.modal', function() {
            $('#modal-form .modal-header h3').html("增加一行");
            validform2.resetForm();
            $('#form_show2')[0].reset();
        });
        $('#modal-form').modal('show');
    }
    function addline(){
        var b=validform2.check(false);
        if(!b)
        {
            return;
        }
       // var line_id=$("#line_id").val();
        var line_field=$("#line_field").val();
        var line_fieldname=$("#line_fieldname").val();
        var line_extractbytype=$("#line_extractbytype").val();
        var line_constant_value=$("#line_constant_value").val();
        var line_extractby=$("#line_extractby").val();


        var line_extractby_index=$("#line_extractby_index").val();
        var line_extractby_attr_flag="";

        var line_extractby_attr=$("#line_extractby_attr").val();
        var line_extractby_description=$("#line_extractby_description").val();
        if($('#line_extractby_attr_flag').is(':checked')) {
            line_extractby_attr_flag="是";
        }else{
            line_extractby_attr_flag="否";
            line_extractby_attr='-';
        }
        if(line_extractbytype=='constant'){
            line_extractby=line_constant_value;
            line_extractby_index='-';
            line_extractby_attr_flag='-';
            line_extractby_attr='-';
            line_extractby_description='-';
        }

        var html="<tr>";
        html+="<td class='line_id' style='display: none;'></td>";
        html+="<td class='line_field'  style='text-align:center;'>"+line_field+"</td>";
        html+="<td class='line_fieldname'  style='text-align:center;'>"+line_fieldname+"</td>";
        html+="<td class='line_extractbytype'  style='text-align:center;'>"+line_extractbytype+"</td>";
        html+="<td class='line_extractby'  style='text-align:center;'>"+line_extractby+"</td>";

        html+="<td class='line_extractby_index'  style='text-align:center;'>"+line_extractby_index+"</td>";
        html+="<td class='line_extractby_attr_flag'  style='text-align:center;'>"+line_extractby_attr_flag+"</td>";
        html+="<td class='line_extractby_attr'  style='text-align:center;'>"+line_extractby_attr+"</td>";
        html+="<td class='line_extractby_description'  style='display: none;'>"+line_extractby_description+"</td>";
        html+="<td  style='text-align:center;'><div class='btn-group'> <a class='btn btn-xs btn-danger' onclick='delMx(this)' >删除</a></div></td>";
        html+="</tr>";
        $("#mxlist tbody").append(html);
        $('#modal-form').modal('hide');
    }
    function delMx(obj){
        $(obj).parent().parent().parent().remove();
    }
    function getMx(){
      var data_mx=[];
      var n=  $("#mxlist tbody").find("tr").size();
        for(var i=0;i<n;i++){
            var $data=$("#mxlist tbody").find("tr").eq(i);
            var line={"line_id":$data.find(".line_id").html(),
            "line_field":$data.find(".line_field").html(),
            "line_fieldname":$data.find(".line_fieldname").html(),
            "line_extractbytype":$data.find(".line_extractbytype").html(),
            "line_extractby":$data.find(".line_extractby").html(),
            //"line_extractby_classorid":$data.find(".line_extractby_classorid").html(),
           // "line_extractby_tag":$data.find(".line_extractby_tag").html(),
            "line_extractby_index":$data.find(".line_extractby_index").html(),
            "line_extractby_attr_flag":$data.find(".line_extractby_attr_flag").html()=='是'?1:0,
            "line_extractby_attr":$data.find(".line_extractby_attr").html(),
            "line_extractby_description":$data.find(".line_extractby_description").html()};
            data_mx.push(line);
        }
        return data_mx;
    }

	$(function(){
         validform=$("#form_show").Validform({
                 btnReset:"#reset",
                 tiptype:2,
                 postonce:true,//至提交一次
                 ajaxPost:false,//ajax方式提交
                 showAllError:true //默认 即逐条验证,true验证全部
         });
        validform2=$("#form_show2").Validform({
            btnReset:"#reset",
            tiptype:2,
            postonce:true,//至提交一次
            ajaxPost:false,//ajax方式提交
            showAllError:true //默认 即逐条验证,true验证全部
        });
	})
</script>

</body>
</html>
