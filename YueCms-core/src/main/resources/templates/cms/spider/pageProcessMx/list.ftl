<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>页面字段配置管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
    <script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>
</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> spider <span class="c-gray en">&gt;</span>页面字段配置管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                    <a onclick="addNew()" class="btn btn-sm btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                    <a onclick="list_del('tableList')" class="btn btn-sm btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                    <a  onclick="query('form_query');" class="btn btn-sm btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                </div>
        </form>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'zbguid',align:"center",sortable:true,order:"asc",visible:true,title: '父表id'},
            {field: 'field',align:"center",sortable:true,order:"asc",visible:true,title: '字段'},
            {field: 'fieldname',align:"center",sortable:true,order:"asc",visible:true,title: '字段名称'},
            {field: 'datatype',align:"center",sortable:true,order:"asc",visible:true,title: '数据类型'},
            {field: 'extractbytype',align:"center",sortable:true,order:"asc",visible:true,title: '提取类型'},
            {field: 'extractby',align:"center",sortable:true,order:"asc",visible:true,title: '提取规则'},
            {field: 'extractby_classorid',align:"center",sortable:true,order:"asc",visible:true,title: '元素类或ID'},
            {field: 'extractby_tag',align:"center",sortable:true,order:"asc",visible:true,title: '标签'},
            {field: 'extractby_index',align:"center",sortable:true,order:"asc",visible:true,title: '索引'},
            {field: 'table_name',align:"center",sortable:true,order:"asc",visible:true,title: '保存的表'},
            {field: 'fieldprocessrule',align:"center",sortable:true,order:"asc",visible:true,title: '数据处理规则'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.url='/spider/pageProcessMx/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
        $.layer.open_page("编辑页面字段配置",sys_ctx+"/spider/pageProcessMx/edit?id="+row.id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增页面字段配置",sys_ctx+"/spider/pageProcessMx/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/spider/pageProcessMx/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
