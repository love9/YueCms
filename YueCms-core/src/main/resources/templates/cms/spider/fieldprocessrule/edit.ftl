<!DOCTYPE html>
<html>
<head>
    <title>编辑字段处理规则</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
    <div class="container-fluid">
        <form action="" id="form_show" method="post" class="form-horizontal" role="form">
            <h2 class="text-center">编辑字段处理规则</h2>
            <input type="hidden" value="${fieldprocessrule.id!}" id="id" name="id"/>
            <div class="form-group">
                <label class="col-sm-2 control-label">数据处理规则：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" id="fieldprocessrule" name="fieldprocessrule" placeholder="数据处理规则" value="${fieldprocessrule.fieldprocessrule!}" class="form-control" datatype="*" nullmsg="请选择数据处理规则" />
                    <#--<my:dicSelect id="fieldprocessrule" idAttr="fieldprocessrule" changeCallBack="aa()"></my:dicSelect>-->
                    <script type="text/javascript">
                        function aa(){
                            var v=$("#fieldprocessrule").val();
                            if(v=='replace'){
                                //$(".div_replace").show().find("input").attr("datatype","*");
                                $(".div_substr").hide().find("input").removeAttr("datatype","*");
                                $(".div_substrlength").hide().find("input").removeAttr("datatype","*");
                            }else if(v=='substrbefore'||v=='substrbeforeExclude'){
                                $(".div_replace").hide().find("input").removeAttr("datatype","*");
                                $(".div_substr").show().find("input").attr("datatype","*");
                                $(".div_substrlength").hide().find("input").removeAttr("datatype","*");
                            }else if(v=='substrafter'||v=='substrafterExclude'){
                                $(".div_replace").hide().find("input").removeAttr("datatype","*");
                                $(".div_substr").show().find("input").attr("datatype","*");
                                $(".div_substrlength").hide().find("input").removeAttr("datatype","*");
                            }else if(v=='substrlength'){
                                $(".div_replace").hide().find("input").removeAttr("datatype","*");
                                $(".div_substr").hide().find("input").removeAttr("datatype","*");
                                $(".div_substrlength").show().find("input").attr("datatype","*");
                            }else{
                                $(".div_replace").hide().find("input").removeAttr("datatype","*");
                                $(".div_substr").hide().find("input").removeAttr("datatype","*");
                                $(".div_substrlength").hide().find("input").removeAttr("datatype","*");
                            }
                        }
                    </script>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group div_replace" style="">
                <label class="col-sm-2 control-label">要替换的正则表达式：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="replaceReg" name="replaceReg" placeholder="要替换的正则表达式" value="${fieldprocessrule.replaceReg!}" class="form-control" datatype="*" nullmsg="请输入要替换的正则表达式" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group div_replace">
                <label class="col-sm-2 control-label">替换成的内容：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="replacement" name="replacement" placeholder="替换成的内容" value="${fieldprocessrule.replacement!}" class="form-control"  nullmsg="请输入替换成的内容" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group div_substr">
                <label class="col-sm-2 control-label">截取字符串目标：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="substrtarget" name="substrtarget" placeholder="截取字符串目标" value="${fieldprocessrule.substrtarget!}" class="form-control" datatype="*" nullmsg="请输入截取字符串目标" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group div_substrlength">
                <label class="col-sm-2 control-label">截取长度：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="substrlength" name="substrlength" placeholder="截取长度" value="${fieldprocessrule.substrlength!}" class="form-control" datatype="*" nullmsg="请输入截取长度" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <div class=" col-sm-10 col-sm-offset-2">
                    <a  onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</a>
                </div>
            </div>
        </form></div>
</div>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
    var validform;
    function save(){
        var b=validform.check(false);
        if(!b)
        {
            $("#form_show").find("div.form-group").show();
            return;
        }
        var params=$("#form_show").serialize();
        $.ajax({
            type:"post",
            url:'/spider/fieldprocessrule/json/save?'+params,
            data:null,
            success:function(json,textStatus){
                ajaxReturnMsg(json);
                setTimeout(function(){
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                },1000);
            }
        });
    }
    $(function(){
        validform=$("#form_show").Validform({
            btnReset:"#reset",
            tiptype:2,
            postonce:true,//至提交一次
            ajaxPost:false,//ajax方式提交
            showAllError:true //默认 即逐条验证,true验证全部
        });
    })
</script>

</body>
</html>
