<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>举报管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">


</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb">  内容管理 <span class="c-gray en">&gt;</span>举报管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
            <div class="form-group">
                <label class="control-label" for="module">模块:</label>
                    <span class="select-box form-control">
                        <select class="select"  id="module" name="module">
                            <option value="" selected>全部</option>
                            <option value="book">书籍</option>
                            <option value="article">文章</option>
                            <option value="chapter">书籍章节</option>
                        </select>
                    </span>
            </div>
            <div class="form-group">
                <label class="control-label" for="report_type">举报类型:</label>
                    <span class="select-box form-control">
                        <select class="select"  id="report_type" name="report_type">
                            <option value="" selected>全部</option>
                            <option value="1">淫秽色情</option>
                            <option value="2">广告垃圾</option>
                            <option value="3">违法信息</option>
                            <option value="4">其他</option>
                        </select>
                    </span>
            </div>
            <div class="form-group">
                <label class="control-label" for="deal_flag">是否处理:</label>
                    <span class="select-box form-control">
                        <select class="select"  id="deal_flag" name="deal_flag">
                            <option value="" selected>全部</option>
                            <option value="1">已处理</option>
                            <option value="0">未处理</option>
                        </select>
                    </span>
            </div>
            <div class="form-group">
            <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a type="button" onclick="search_form_reset('form_query')" class="btn btn-outline btn-warning"><i class="fa fa-reply"></i>&nbsp;重置</a>
                <a onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
        report_type:'',
        deal_flag:'',
        module:''
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'module',align:"center",sortable:true,order:"asc",visible:true,title: '模块',width:"110px"},
            {field: 'target_id',align:"center",sortable:true,order:"asc",visible:true,title: '被举报内容',formatter:function(v,row){
                var btn='';
                    if(row.module=='book'){
                        btn='<a type="button" onclick="viewBook('+v+')" class="btn btn-sm btn-primary" ><i class="fa fa-book"></i>&nbsp;&nbsp;查看</a>';
                    }else if(row.module=='article'){
                        btn='<a type="button" onclick="viewArticle('+v+')" class="btn  btn-sm btn-danger" ><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;查看</a>';
                    }else if(row.module=='chapter'){
                        btn='<a type="button" onclick="viewChapter('+v+')" class="btn  btn-sm btn-warning" ><i class="fa fa-paragraph"></i>&nbsp;&nbsp;查看</a>';
                    }else{

                    }
                return btn;

            }
            },
            /*{field: 'title',align:"center",sortable:true,order:"asc",visible:true,title: '标题'},*/
            {field: 'report_yhid',align:"center",sortable:true,order:"asc",visible:false,title: '举报人ID'},
            {field: 'report_type',align:"center",sortable:true,order:"asc",visible:true,title: '举报类型',width:"120px",formatter:function(v){
                    if(v==1){
                        return "<font color='red'>淫秽色情</font>";
                    }else if(v==2){
                        return "<font color='green'>广告垃圾</font>";
                    }else if(v==3){
                        return "<font color='orange'>违法信息</font>";
                    }else if(v==4){
                        return "<font color='gray'>其他</font>";
                    }else{
                        return v;
                    }
                }
            },
            {field: 'count',align:"center",sortable:true,order:"asc",visible:true,title: '举报次数',width:"110px"},
            {field: 'deal_flag',align:"center",sortable:true,order:"asc",visible:true,title: '是否处理',width:"120px",formatter:function(v){
                if(v==1|v=='1'){
                    return " <font color='green'>已处理</font>"
                }else{
                    return " <font color='red'>未处理</font>"
                }
            }},
        ];
        table_list_Params.pageSize=15;
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/cms/report/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    function viewBook(id){

    }
    function viewArticle(id){

    }
    function viewChapter(id){

    }
    var onClickRow=function(row,tr){
        $.layer.open_page("编辑举报",sys_ctx+"/cms/report/edit?id="+row.id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){

        $.layer.open_page("新增举报",sys_ctx+"/cms/report/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/report/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
