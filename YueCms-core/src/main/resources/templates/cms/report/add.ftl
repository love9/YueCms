<!DOCTYPE html>
<html>
<head>
  <title>新增举报</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center">新增举报</h2>

         <div class="form-group">
            <label class="col-sm-2 control-label">所属模块：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="module" name="module" placeholder="被举报内容所属模块" value="" class="form-control" datatype="*" nullmsg="请输入被举报内容所属模块，比如评论、文章、图片等" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">目标ID：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="target_id" name="target_id" placeholder="被举报目标ID" value="" class="form-control" datatype="*" nullmsg="请输入被举报目标id,一般是评论或文章主键" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>


         <div class="form-group">
            <label class="col-sm-2 control-label">举报人用户ID：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="report_yhid" name="report_yhid" placeholder="举报人用户ID" value="" class="form-control" datatype="*" nullmsg="请输入举报人用户id" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">举报类型：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="report_type" name="report_type" placeholder="举报类型" value="" class="form-control" datatype="*" nullmsg="请输入举报类型" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">被举报次数：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="count" name="count" placeholder="被举报次数,可反映出处理急切程度" value="" class="form-control" datatype="*" nullmsg="请输入被举报次数，说明还未被处理再次被其它人举报，可反映出处理急切程度" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">处理标志：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="deal_flag" name="deal_flag" placeholder="处理标志 0未处理 1已处理" value="" class="form-control" datatype="*" nullmsg="请输入处理标志 0未处理 1已处理" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>

		<#--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-2">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/cms/report/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
