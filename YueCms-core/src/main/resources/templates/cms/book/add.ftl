<!DOCTYPE html>
<html>
<head>
  <title>新增书籍</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center"> </h2>

         <div class="form-group">
            <label class="col-sm-3 control-label">书名：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="bookname" name="bookname" placeholder="书名" value="" class="form-control" datatype="*" nullmsg="请输入书名" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">编码：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="bookcode" name="bookcode" placeholder="编码" value="" class="form-control" datatype="*" nullmsg="请输入编码" />
            </div>
            <div class="col-sm-3">
                <div class="Validform_checktip"></div>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">作者：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="author" name="author" placeholder="作者" value="" class="form-control"  />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">封面URL：</label>
            <div class="col-sm-6 formControls">
                <input type="text" style="display: inline-block;width:calc(100% - 105px);" id="sheet_url" name="sheet_url" placeholder="封面url" value="" class="form-control" datatype="*" nullmsg="请输入封面url" />
                <a type="button" style="display: inline-block;width: 100px;" onclick="previewCoverImg()" class="btn btn-primary " ><i class="icon-eye"></i>预览</a>
            </div>
            <div class="col-sm-3">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">标签：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="tags" name="tags" placeholder="标签" value="" class="form-control" datatype="*" nullmsg="请输入标签" />
            </div>
            <div class="col-sm-3">
                <div class="Validform_checktip"></div>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">关键词：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="keywords" name="keywords" placeholder="关键词" value="" class="form-control"   />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>


         <div class="form-group">
            <label class="col-sm-3 control-label">来源：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="source" name="source" placeholder="来源" value="" class="form-control"   />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">描述：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="description" name="description" placeholder="描述" value="" class="form-control"   />
            </div>
            <div class="col-sm-3">
                <div class="Validform_checktip"></div>
            </div>
        </div>

		<#--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-3">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form></div>
</div>
<div id="outerdiv" style="position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0.7); z-index: 2; width: 100%; height: 100%; display: none;">
    <div id="innerdiv" style="position: absolute; top: 80.8px; left: 446.079px;">
        <img id="bigimg" style="border: 5px solid rgb(255, 255, 255); width: 969.842px;" src="">
    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/cms/book/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);

				},1000);
			}
		});
	}
    var previewCoverImg=function(){
        $("#sheet_url").blur();
	    var url=$("#sheet_url").val();
        if(isNotEmpty(url)){
            var b=Fast.isExist(url);
            if(b){
                imgshow("#outerdiv", "#innerdiv", "#bigimg", url);
            }else{
                Fast.msg_error("图片不存在!");
            }
        }
    }
    function imgshow(outerdiv, innerdiv, bigimg, url){
        // var src = _this.attr("src");
        $(bigimg).attr("src", url);

        $("<img/>").attr("src", url).load(function(){
            var windowW = $(window).width();
            var windowH = $(window).height();
            var realWidth = this.width;
            var realHeight = this.height;
            var imgWidth, imgHeight;
            var scale = 0.8;

            if(realHeight>windowH*scale) {
                imgHeight = windowH*scale;
                imgWidth = imgHeight/realHeight*realWidth;
                if(imgWidth>windowW*scale) {
                    imgWidth = windowW*scale;
                }
            } else if(realWidth>windowW*scale) {
                imgWidth = windowW*scale;
                imgHeight = imgWidth/realWidth*realHeight;
            } else {
                imgWidth = realWidth;
                imgHeight = realHeight;
            }
            $(bigimg).css("width",imgWidth);

            var w = (windowW-imgWidth)/2;
            var h = (windowH-imgHeight)/2;
            $(innerdiv).css({"top":h, "left":w});
            $(outerdiv).fadeIn("fast");
        });

        $(outerdiv).click(function(){
            $(this).fadeOut("fast");
        });
    }
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
