<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>书籍管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">



</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> cms <span class="c-gray en">&gt;</span>书籍管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                    <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                    <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                    <a onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>

                </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>

<div id="outerdiv" style="position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0.7); z-index: 2; width: 100%; height: 100%; display: none;">
    <div id="innerdiv" style="position: absolute; top: 80.8px; left: 446.079px;">
        <img id="bigimg" style="border: 5px solid rgb(255, 255, 255); width: 969.842px;" src="">
    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        //  $.layer.msg_top("11111111","",{oparent:window.top});
        //  $.layer.msg("1133111111");
        //  layerMsg("1133111111");
        //  $.layer.msg_ask("12333")
        //layerMsg_askIcon("1111111111")
        //  layerMsg_top("2222222222")
      //  $.layer.msg_top("11111111",1);
        //$.layer.msg_right("111111111");
       // layer.alert(1);
       // layerAlert_rightIcon(1);
       // $.layer.alert(222,{icon:3,callback:function(){layer.msg(123);}});
       // $.layer.alert_right("1111",{skin:''});

        //  $.layer.alert_cry("1111");
       //$.layer.confirm("2222",function(){layer.msg("ok");},function(){layer.msg("cancle");});
        // $.layer.open_page("cs","/cms/link/add");
       // $.layer.open_page_full("cs","/cms/link/add");
        //   $.layer.loading();
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:false,title: '作者'},
            {field: 'bookcode',align:"center",sortable:true,order:"asc",visible:true,title: '编码'},
            {field: 'bookname',align:"center",sortable:true,order:"asc",visible:true,title: '书名'},
            {field: 'author',align:"center",sortable:true,order:"asc",visible:true,width:"100px",title: '作者'},
            {field: 'keywords',align:"center",sortable:true,order:"asc",visible:false,title: '关键词'},
            {field: 'description',align:"center",sortable:true,order:"asc",visible:false,title: '描述'},
            {field: 'source',align:"center",sortable:true,order:"asc",visible:false,title: '来源'},
            {field: 'sheet_url',align:"center",sortable:true,order:"asc",visible:true,title: '封面图片',width:"110px",
                formatter:function(v,row){
                    if(isNotEmpty(v)){
                         return "<button class=\"btn btn-sm btn-outline btn-success \" onclick=\"previewCoverImg('"+v+"')\" type=\"button\"><i class=\"fa fa-eye\"></i><span class=\"bold\">预览</span></button>";
                    }else{
                        return "-";
                    }
                }
            },
            {field: 'tags',align:"center",sortable:true,order:"asc",visible:true,title: '标签'},
            {field: 'static_url',align:"center",sortable:true,order:"asc",visible:true,title: '静态化',width:"130px",formatter:function(v,row){

                    if(row.status==1||row.status=='1'){
                        if(isNotEmpty(v)){
                            return "<button class=\"btn btn-sm btn-outline btn-warning \" onclick=\"staticBook('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pencil\"></i> 重新静态化</button>";
                        }else{
                            return "<button class=\"btn btn-sm btn-outline btn-success \" onclick=\"staticBook('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pencil\"></i> 执行静态化</button>";
                        }
                    }else{
                        return "-";
                    }

               }
            },
            {field: 'spiderMissionCode',align:"center",sortable:true,order:"asc",visible:true,width:"110px",title: '趴取任务代码'},
            {field: 'status',align:"center",sortable:true,order:"asc",visible:true,title: '趴取状态',width:"100px",formatter:function(v,row){
                    if(v==0||v=='0'){
                        return "<font color='red'>未趴取</font>";
                    }else if(v==2||v=='2'){
                        return "<font color='orange'>进行中...</font>";
                    }else{
                        return "<font color='green'>已趴取</font>";
                    }
                }
             },
            {field: '操作',align:"center",sortable:true,order:"asc",visible:true,title: '操作',width:"170px",formatter:function(v,row){
                    var btn="<div class=\"btn-group\"><button class=\"btn btn-sm btn-outline btn-info \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pencil\"></i> 编辑</button>";
                    if(isNotEmpty(row.spiderMissionCode)){
                        if(row.status=='0'){
                            btn+="<button class=\"btn btn-sm btn-outline btn-success \" onclick=\"spiderBook('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pencil\"></i> 趴取</button>";
                        }else{
                            btn+="<button class=\"btn btn-sm btn-outline btn-warning \" onclick=\"spiderBook('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pencil\"></i> 重新趴取</button>";
                        }
                    }
                    btn+="</div>";
                    return btn;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.pageSize=15;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/cms/book/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var spiderBook=function(id){
        Fast.confirm("确定要爬取该书籍吗?",function(){
            $.ajax({
                type:"post",
                url:'/cms/book/json/spiderBook/'+id,
                data:null,
                success:function(data,textStatus){
                    ajaxReturnMsg(data);
                    refreshData();
                }
            });
        });

    }
    var staticBook=function(id){
        Fast.confirm("确定要执行静态化吗?",function(){
            $.ajax({
                type:"post",
                url:'/cms/book/json/staticBook/'+id,
                data:null,
                success:function(data,textStatus){
                    ajaxReturnMsg(data);
                    refreshData();
                }
            });
        });
    }
    var previewCoverImg=function(v){
        var b=false;
        if(isNotEmpty(v)){
            b=Fast.isExist(v);
        }
        if(b){
            imgshow("#outerdiv", "#innerdiv", "#bigimg", v);
        }else{
            Fast.msg_error("图片不存在!");
        }
    }
    var editRow=function(id){
        $.layer.open_page("编辑书籍",sys_ctx+"/cms/book/edit?id="+id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增书籍",sys_ctx+"/cms/book/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/book/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        refreshData();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
    function imgshow(outerdiv, innerdiv, bigimg, url){
        // var src = _this.attr("src");
        $(bigimg).attr("src", url);

        $("<img/>").attr("src", url).load(function(){
            var windowW = $(window).width();
            var windowH = $(window).height();
            var realWidth = this.width;
            var realHeight = this.height;
            var imgWidth, imgHeight;
            var scale = 0.8;

            if(realHeight>windowH*scale) {
                imgHeight = windowH*scale;
                imgWidth = imgHeight/realHeight*realWidth;
                if(imgWidth>windowW*scale) {
                    imgWidth = windowW*scale;
                }
            } else if(realWidth>windowW*scale) {
                imgWidth = windowW*scale;
                imgHeight = imgWidth/realWidth*realHeight;
            } else {
                imgWidth = realWidth;
                imgHeight = realHeight;
            }
            $(bigimg).css("width",imgWidth);

            var w = (windowW-imgWidth)/2;
            var h = (windowH-imgHeight)/2;
            $(innerdiv).css({"top":h, "left":w});
            $(outerdiv).fadeIn("fast");
        });

        $(outerdiv).click(function(){
            $(this).fadeOut("fast");
        });
    }
</script>
</body></html>
