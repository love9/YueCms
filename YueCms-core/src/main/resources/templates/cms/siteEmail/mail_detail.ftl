
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>H+ 后台主题UI框架 - 查看邮件</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/hplus.css" rel="stylesheet">
    <link href="/resources/css/skin/default.css" rel="stylesheet">
    <link href="/resources/lib/toastr/toastr.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
</head>

<body class="body-bg  main-bg">
    <div class="wrapper wrapper-content">
        <div class="row">

            <#include "/cms/siteEmail/sideMenu.ftl"/>

            <div class="col-sm-10 animated fadeInRight">
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <!--<a href="mail_compose.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="回复"><i class="fa fa-reply"></i> 回复</a>
                        <a href="mail_detail.ftl#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="打印邮件"><i class="fa fa-print"></i> </a>-->
                        <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为垃圾邮件" onclick="trashEamil()"><i class="fa fa-trash-o"></i> </a>
                    </div>
                    <h2>
                    查看邮件
                </h2>
                    <div class="mail-tools tooltip-demo m-t-md">
                        <h3>
                            <input type="hidden" id="id" value="${sendEmail.id!}">
                        <span class="font-noraml">主题： </span>${sendEmail.title}
                    </h3>
                        <h5>
                        <span class="pull-right font-noraml">$!date.format('yyyy年MM月dd HH:mm:ss ',${sendEmail.createTime})</span>
                        <span class="font-noraml">发件人： </span>${sendEmail.fromemail!}
                    </h5>
                    </div>
                </div>
                <div class="mail-box">
                    <div class="mail-body">
                        ${sendEmail.content!}
                    </div>


                    <#if sendEmail.attachfiles != "">
                    <div class="mail-attachment">
                        <p>
                            <span><i class="fa fa-paperclip"></i> 2 个附件 - </span>
                            <a href="mail_detail.ftl#">下载全部</a> |
                            <a href="mail_detail.ftl#">预览全部图片</a>
                        </p>

                        <div class="attachment">
                            <div class="file-box">
                                <div class="file">
                                    <a href="mail_detail.ftl#">
                                        <span class="corner"></span>

                                        <div class="icon">
                                            <i class="fa fa-file"></i>
                                        </div>
                                        <div class="file-name">
                                            Document_2014.doc
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="file-box">
                                <div class="file">
                                    <a href="mail_detail.ftl#">
                                        <span class="corner"></span>

                                        <div class="image">
                                            <img alt="image" class="img-responsive" src="/resources/images/img/p1.jpg">
                                        </div>
                                        <div class="file-name">
                                            Italy street.jpg
                                        </div>
                                    </a>

                                </div>
                            </div>
                            <div class="file-box">
                                <div class="file">
                                    <a href="mail_detail.ftl#">
                                        <span class="corner"></span>

                                        <div class="image">
                                            <img alt="image" class="img-responsive" src="/resources/images/img/p2.jpg">
                                        </div>
                                        <div class="file-name">
                                            My feel.png
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    </#if>
                    <div class="mail-body text-right tooltip-demo">
                        <!--<a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-reply"></i> 回复</a>
                        <a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-arrow-right"></i> 下一封</a>
                        <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="打印这封邮件" class="btn btn-sm btn-white"><i class="fa fa-print"></i> 打印</button>-->
                        <button title="" data-placement="top" data-toggle="tooltip" data-original-title="删除邮件" class="btn btn-sm btn-white"  onclick="trashEamil()"><i class="fa fa-trash-o"></i> 删除</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <script type="">
        function getUrlParam(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        }
        $(function(){
            var eid = getUrlParam('id');
        })
        //把该邮件放入回收站
        function trashEamil(){
            var id=$("#id").val();
            if(isNotEmpty(id)){
                Fast.confirm("确定要将该邮件放入回收站？",function(){
                    $.ajax({
                        type:"post",
                        url:'/cms/sendEmail/json/trash/'+id,
                        data:null,
                        success:function(json,textStatus){
                            ajaxReturnMsg(json);
                            setTimeout(function(){
                                window.location.href="/cms/siteEmail";
                            },1000);
                        }
                    });
                },function(){

                })
            }
        }
    </script>

</body>
</html>
