<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>H+ 后台主题UI框架 - 写信</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico">
    <!--<link href="/resources/css/index/index.css" rel="stylesheet">-->
    <link href="/resources/css/hplus.css" rel="stylesheet">
    <link href="/resources/css/skin/default.css" rel="stylesheet">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <link href="/resources/lib/toastr/toastr.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>


    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <link href="/resources/lib/summernote/summernote.css" rel="stylesheet">
    <link href="/resources/lib/summernote/summernote-bs3.css" rel="stylesheet">
    <script src="/resources/lib/summernote/summernote.min.js"></script>
    <script src="/resources/lib/summernote/summernote-zh-CN.js"></script>


</head>
<script type="text/javascript">
    var sys_ctx='';
</script>
<body class="body-bg  main-bg">
    <div class="wrapper wrapper-content">
        <div class="row">

<#include "/cms/siteEmail/sideMenu.ftl"/>
            <div class="col-sm-10 animated fadeInRight bg-transparent"  >
                <div class="mail-box-header bg-transparent">
                    <div class="pull-right tooltip-demo">
                        <a href="#" onclick="saveDraft()" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="存为草稿"><i class="fa fa-pencil"></i> 存为草稿</a>
                        <a href="/cms/siteEmail" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="放弃"><i class="fa fa-times"></i> 放弃</a>
                    </div>
                    <h2>
                    写信
                </h2>
                </div>
                <div class="mail-box bg-transparent">
                    <div class="mail-body bg-transparent">
                        <form class="form-horizontal  bg-transparent" method="get">
                            <input type="hidden" id="id" name="id" value="${sendEmail.id!}">
                           <!-- <div class="form-group">
                                <label class="col-sm-2 control-label">发送到：</label>
                                <div class="col-sm-10">
                                    <input type="text" id="toYhid" name="toYhid"  class="form-control" value="${sendEmail.toYhid!}">
                                </div>
                            </div>-->
                            <div class="form-group"  >
                                <label class="col-sm-2 control-label">发送到：</label>
                                <div class="col-sm-8 formControls">
                                    <input type="hidden" id="toYhid"    name="toYhid" placeholder="指派给" value="${sendEmail.toYhid!}" class="form-control"  />
                                    <input type="text" id="assignToNames" disabled value="${assignToNames!}"   name="assignToNames" placeholder="指派给"  class="form-control"  />
                                </div>
                                <div class="col-sm-2 formControls">
                                    <button type="button" onclick="base_openYhxzPage('toYhid','assignToNames','limit=5')" style="display: inline-block;" class="btn btn-block btn-outline btn-primary">选择用户</button>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">主题：</label>
                                <div class="col-sm-10">
                                    <input type="text" id="title" name="title" class="form-control" value="${sendEmail.title!}">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="mail-text h-200">
                       <!-- <button id="getv" onclick="alert(getValue())" >获取值</button>
                        <button id="setv" onclick=" setValue('123456asdf') " >设置值</button>-->
                        <div class="summernote">
                            ${sendEmail.content!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="mail-body text-right tooltip-demo">
                        <a href="#" onclick="saveAndSend()" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top"  data-original-title="发送" title="Send"><i class="fa fa-reply"></i> 发送</a>
                        <a href="/cms/siteEmail" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"  data-original-title="放弃" ><i class="fa fa-times"></i> 放弃</a>
                        <a href="#" onclick="saveDraft()" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"  data-original-title="存为草稿" ><i class="fa fa-pencil"></i> 存为草稿</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
           // $(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green"});
            $(".summernote").summernote({
                height: 400,
                minHeight: 300,
                maxHeight: 500,
                focus: true,
                lang:'zh-CN',
                // 重写图片上传
                onImageUpload: function(files, editor, $editable) {

                    sendFile(files[0],editor,$editable);
                }
            });
            setValue('${sendEmail.content!}');
        })
        //图片上传
        function sendFile(file, editor, $editable){
            var filename = false;
            try{
                filename = file['name'];
            } catch(e){
                filename = false;
            }
            if(!filename){
                $(".note-alarm").remove();
            }
            //以上防止在图片在编辑器内拖拽引发第二次上传导致的提示错误
            data = new FormData();
            data.append("file", file);
            data.append("key",filename); //唯一性参数

            $.ajax({
                data: data,
                type: "POST",
                url: "/base/attachment/uploadSingle",
                cache: false,
                contentType: false,
                processData: false,
                success: function(json) {
                    json =JSON.parse(json);
                    alert(JSON.stringify(json[0]));
                    if(isNotEmpty(json[0].path)){
                      Fast.msg_success("上传成功！")
                    }else{
                        Fast.msg_error("上传失败！");
                        return;
                    }
                    var imgpath=json[0].path;
                    alert(JSON.stringify(imgpath));
                    editor.insertImage($editable, imgpath);
                    //setTimeout(function(){$(".note-alarm").remove();},3000);
                },
                error:function(){
                    alert("上传失败！");
                    return;
                    //setTimeout(function(){$(".note-alarm").remove();},3000);
                }
            });
        }
        //给编辑器赋值
        function setValue(text){
            $('.summernote').code(text);
        }
        //取值
        function getValue(){
            var str= $('.summernote').code();
            return str;
        }
        ;
        //var edit=function(){$(".click2edit").summernote({focus:true})};
        //var save=function(){var aHTML=$(".click2edit").code();$(".click2edit").destroy()};
        //存储为草稿邮件
        function saveDraft(){
            //layer.alert("存储为草稿3，正在开发中。。。");
            saveAndSend(3);
        }
        //保存并发送
        function saveAndSend(type){
           // layer.alert("保存并发送，正在开发中。。。");
            if(isEmpty(type)){
                type="1";
            }
            var sendTo=$("#toYhid").val();
            if(isEmpty(sendTo)){
                Fast.msg_error("请输入发送地址！");
                return;
            }
            var title=$("#title").val();
            if(isEmpty(title)){
                Fast.msg_error("请输入主题！");
                return;
            }
            var content=getValue();
            if(isEmpty(content)){
                Fast.msg_error("请编辑内容！");
                return;
            }
            var id=$("#id").val();
            var params="toYhid="+sendTo+"&content="+content+"&emailtype=2&title="+title+"&sendtype=0&status="+type+"&id="+id;
            $.ajax({
                type:"post",
                url:'/cms/sendEmail/json/save?'+params,
                data:null,
                success:function(json,textStatus){
                    ajaxReturnMsg(json);
                    /*setTimeout(function(){
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                    },1000);*/
                }
            });
        }
        function addEmail(){
           Fast.confirm("该操作会清空表单，确认这样做吗？",function(){
                window.location.href="/cms/siteEmail/add";
            },function(){})
        }
    </script>

</body>
</html>
