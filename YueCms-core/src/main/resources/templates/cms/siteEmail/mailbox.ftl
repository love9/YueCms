<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>H+ 后台主题UI框架 - 收件箱</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
    <link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/hplus.css" rel="stylesheet">


    <link href="/resources/css/skin/default.css" rel="stylesheet">
    <!--<link href="/resources/lib/icheck/skins/square/green.css" rel="stylesheet">-->
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <link href="/resources/lib/toastr/toastr.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script>
    var from='${from}';
    var pageNo=1;
    var pageSize=10;
    var totalPages;
    function pager(){
        var $pagebar=$("#pagebar");
        laypage({
            cont:$pagebar, //容器。值支持id名、原生dom对象，jquery对象,
            pages: totalPages, //总页数
            curr:pageNo,
            jump:function(e,first){
                if(!first){ //一定要加此判断，否则初始时会无限刷新
                    pageNo=e.curr;
                    listEmail();
                }
            },
            skin: 'molv', //皮肤
            first: '首页', //若不显示，设置false即可
            last: '尾页', //若不显示，设置false即可
            prev: '上一页', //若不显示，设置false即可
            next: '下一页' //若不显示，设置false即可
        });
    }
    //按照标题搜索邮件
    function searchByTitle(){
        var title=$("#cx_title").val();
        var queryParams="&cx_title="+title;
        listEmail(queryParams);
    }
    function listEmail(queryParams){
        var url='/cms/sendEmail/json/find?emailtype=2&page='+pageNo+"&rows="+pageSize+"&from="+from;
        if(isNotEmpty(queryParams)){
            url='/cms/sendEmail/json/find?emailtype=2&page='+pageNo+"&rows="+pageSize+"&from="+from+queryParams;
        }
        $.getJSON(url, function(json){
            if(!(Number(json.total)>0)){
                var html="<tr class='unread'  ><td  colspan='6' style='text-align: center;'>没有查询到任何记录！</td></tr>";
                $("#listBody").html(html);
                $("#countBox").html("0");
                Fast.msg_error("没有查询到任何记录！");
                return;
            }
            $("#countBox").html(json.total);
            totalPages=Number((Number(json.total)+Number(Number(pageSize)-1)))/pageSize;
                var html="";
                $.each(json.rows,function(index, item){
                        if(isNotEmpty(item.feedback)){
                            html+="<tr class='read' data-id='"+item.id+"'>";
                        }else{
                            html+="<tr class='unread' data-id='"+item.id+"'>";
                        }
                        html+="<td style='width: 35px;'  class=\"check-mail\">";
                        html+="<input type=\"checkbox\" class=\"i-checks\">";
                        html+="</td>";
                        html+="<td  >"+Number(Number(Number(index)+1)+(pageNo-1)*pageSize)+"</td>";
                        if(from=='draft'){
                            html+="<td  class=\"mail-ontact\"><a href=\"/cms/siteEmail/editDraft?id="+item.id+"\">"+item.title+"</a></td>";
                            html+="<td    class=\"mail-subject\"><a href=\"/cms/siteEmail/editDraft?id="+item.id+"\">"+item.title+"</a></td>";
                        }else{
                            html+="<td  class=\"mail-ontact\"><a href=\"/cms/siteEmail/view?id="+item.id+"\">"+item.title+"</a></td>";
                            html+="<td    class=\"mail-subject\"><a href=\"/cms/siteEmail/view?id="+item.id+"\">"+item.title+"</a></td>";
                        }
                        if(isNotEmpty(item.attachfiles)){
                            html+="<td  class=\"\"><i class=\"fa fa-paperclip\"></i></td>";
                        }else{
                            html+="<td class=\"\"></td>";
                        }
                        html+="<td    class=\"text-right mail-date\">"+item.createTime+"</td>";
                        html+="</tr>";
                });
                $("#listBody").html(html);
                pager();
        });
    }
    function isEmpty(val) {
        val = $.trim(val);
        if (val == null)
            return true;
        if (val == undefined || val == 'undefined')
            return true;
        if (val == "")
            return true;
        if (val.length == 0)
            return true;
        if (!/[^(^\s*)|(\s*$)]/.test(val))
            return true;
        return false;
    }
    function isNotEmpty(val) {
        return !isEmpty(val);
    }
    function refreshList(){
        //刷新列表
       $("#cx_title").val("");
        searchByTitle();
    }
    function checkAll(obj){
        $this=$(obj);
        var v=$this.data("data-checked");
        var b=v=="true"?true:false;
        if(b){
            $("#checkAllIcon").hide();
            $this.data("data-checked","false");
        }else{
            $("#checkAllIcon").show();
            $this.data("data-checked","true");
        }
        $("#listBody").find("tr td") .find("input[type='checkbox']").prop("checked",!b);
    }
    function setImportants(){
       //设置为重要
        var idArr = getSelectedRows();
        if(isEmpty(idArr)){
            Fast.msg_warning("请您选择至少1条数据");
            return;
        }
        var ids=idArr.join(",");
        //alert(ids);
        Fast.confirm("确定要标记邮件为重要吗？",function(){
            $.ajax({
                type:"post",
                url:'/cms/sendEmail/json/importants/'+ids,
                data:null,
                success:function(json,textStatus){
                    ajaxReturnMsg(json);
                    searchByTitle();
                }
            });
        },function(){

        });
    }
    function deleteEver(){
      //永久删除
        var idArr = getSelectedRows();
        if(isEmpty(idArr)){
            Fast.msg_warning("请您选择至少1条数据");
            return;
        }
        var ids=idArr.join(",");
        //alert(ids);
        Fast.confirm("确定要永久删除吗？",function(){
            $.ajax({
                type:"post",
                url:'/cms/sendEmail/json/deletes/'+ids,
                data:null,
                success:function(json,textStatus){
                    ajaxReturnMsg(json);
                    searchByTitle();
                }
            });
    });
    function getSelectedRows(){
        var ids=[];
        $("#listBody").find("input[type='checkbox']:checked").each(function(){
            var id= $(this).parent().parent().attr("data-id");/*获得选中行的数据id在该节点上缓存的值*/
            ids.push(id);
        });
        return ids;
    }
    $(function(){
      listEmail();
    });
    </script>
</head>

<body class="body-bg   main-bg">
    <div class="wrapper wrapper-content">
        <div class="row">

<#include "/cms/siteEmail/sideMenu.ftl"/>
            <div class="col-sm-10 animated fadeInRight">
                <div class="mail-box-header">
                    <form method="get" action="http://www.zi-han.net/theme/hplus/index.html" class="pull-right mail-search">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="cx_title" name="cx_title" placeholder="搜索邮件标题，正文等">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-primary" onclick="searchByTitle()">
                                    搜索一下
                                </button>
                            </div>
                        </div>
                    </form>
                    <h2>

                       <#if  from =="important">
                            星级邮件 (<span id="countBox" style="color: #008000">11</span>)
                       <#elseif  from =="draft">
                        草稿箱 (<span id="countBox" style="color: #008000">11</span>)
                        <#elseif  from =="rubbish">
                        回收站 (<span id="countBox" style="color: #008000">11</span>)
                        <#elseif  from =="receive">
                        收件箱 (<span id="countBox" style="color: #008000">11</span>)
                        <#else>
                        已发信件 (<span id="countBox" style="color: #008000">11</span>)
                        </#if>
                </h2>
                    <div class="mail-tools tooltip-demo m-t-md">
                        <div class="btn-group pull-right">
                            <!--<button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i>
                            </button>
                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i>
                            </button>-->
                            <div style="float: right" id="pagebar"  class="pagebar"  ></div>
                        </div>
                        <span class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top"  data-original-title="全选" onclick="checkAll(this)"> <i id="checkAllIcon" class="fa fa-check"></i>全选
                        </span>
                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left"  data-original-title="刷新邮件列表" onclick="refreshList()"><i class="fa fa-refresh"></i> 刷新</button>

                        <#if  from =="">
                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top"  data-original-title="标为重要" onclick="setImportants()"><i class="fa fa-exclamation"></i>
                        </button>
                        </#if>
                        <#if  from =="rubbish">
                        <button href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="永久删除" onclick="deleteEver()"><i class="fa fa-trash-o"></i> </button>
                        </#if>
                        <!--<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top"  data-original-title="标为垃圾邮件"><i class="fa fa-trash-o"></i>
                        </button>
                        <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="打印这封邮件" class="btn btn-sm btn-white"><i class="fa fa-print"></i> 打印</button>-->

                    </div>
                </div>
                <div class="mail-box">

                    <table class="table table-hover table-mail">
                        <!--<thead>
                            <tr class="unread">
                                <td width="35px"><input type='checkbox' id="ck_all" onclick="checkall(this)" > </td>
                                <td width="60px">序号</td>
                                <td width="80px">来源</td>
                                <td width="50%">主题</td>
                                <td></td>
                                <td>接收时间</td>
                            </tr>
                        </thead>-->
                        <tbody id="listBody">
                            <tr class="unread">
                                <td class="check-mail">
                                    <input type="checkbox" class="i-checks">
                                </td>
                                <td class="mail-ontact"><a href="mail_detail.ftl">支付宝</a>
                                </td>
                                <td class="mail-subject"><a href="mail_detail.ftl">支付宝提醒</a>
                                </td>
                                <td class=""><i class="fa fa-paperclip"></i>
                                </td>
                                <td class="text-right mail-date">昨天 10:20</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
