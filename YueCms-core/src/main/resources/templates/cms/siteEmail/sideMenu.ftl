
<style type="text/css" >
    .mailbox-content ul li.active{color: #fff;background-color: rgba(167,167,167,0.25);}
</style>
<div class="col-sm-2">
    <div class="ibox float-e-margins">
        <div class="ibox-content mailbox-content">
            <div class="file-manager">
                <a class="btn btn-block btn-primary compose-mail" href="/cms/siteEmail/add" onclick="">写信</a>
                <div class="space-25"></div>
                <h5>文件夹</h5>
                <ul class="folder-list m-b-md" style="padding: 0">
                    <li>
                        <a href="/cms/siteEmail?from=receive"> <i class="fa fa-inbox "></i> 收件箱 <span id="label_sjx" class="label label-warning pull-right"></span>
                        </a>
                    </li>
                    <li id="add">
                        <a href="/cms/siteEmail"> <i class="fa fa-envelope-o"></i> 已发信件</a>
                    </li>

                    <li id="important">
                        <a href="/cms/siteEmail?from=important"> <i class="fa fa-certificate"></i> 重要信件</a>
                    </li>

                    <li  id="draft">
                        <a href="/cms/siteEmail?from=draft"> <i class="fa fa-file-text-o"></i> 草稿 <span id="label_cg"  class="label label-danger pull-right"></span>
                        </a>
                    </li>
                    <li  id="rubbish">
                        <a href="/cms/siteEmail?from=rubbish"> <i class="fa fa-trash-o"></i> 垃圾箱</a>
                    </li>
                </ul>
                <h5>分类</h5>
                <ul class="category-list" style="padding: 0">
                    <li>
                        <a href="mail_compose.html#"> <i class="fa fa-circle text-navy"></i> 工作</a>
                    </li>
                    <li>
                        <a href="mail_compose.html#"> <i class="fa fa-circle text-danger"></i> 文档</a>
                    </li>
                    <li>
                        <a href="mail_compose.html#"> <i class="fa fa-circle text-primary"></i> 社交</a>
                    </li>
                    <li>
                        <a href="mail_compose.html#"> <i class="fa fa-circle text-info"></i> 广告</a>
                    </li>
                    <li>
                        <a href="mail_compose.html#"> <i class="fa fa-circle text-warning"></i> 客户端</a>
                    </li>
                </ul>

                <h5 class="tag-title">标签</h5>
                <ul class="tag-list" style="padding: 0">
                    <li><a href="mail_compose.html" class="label label-primary"><i class="fa fa-tag"></i> 朋友</a>
                    </li>
                    <li><a href="mail_compose.html"  class="label label-success"><i class="fa fa-tag"></i> 工作</a>
                    </li>
                    <li><a href="mail_compose.html"  class="label label-warning"><i class="fa fa-tag"></i> 家庭</a>
                    </li>
                    <li><a href="mail_compose.html"  class="label label-default"><i class="fa fa-tag"></i> 孩子</a>
                    </li>
                    <li><a href="mail_compose.html"  class="label label-danger"><i class="fa fa-tag"></i> 假期</a>
                    </li>
                    <li><a href="mail_compose.html"  class="label label-info"><i class="fa fa-tag"></i> 音乐</a>
                    </li>
                    <li><a href="mail_compose.html" class="label label-primary"><i class="fa fa-tag"></i> 照片</a>
                    </li>
                    <li><a href="mail_compose.html" class="label label-success"><i class="fa fa-tag"></i> 电影</a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</div>