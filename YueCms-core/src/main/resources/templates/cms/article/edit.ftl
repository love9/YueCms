<!DOCTYPE html>
<html>
<head>
	<title>编辑文章</title>
	<link href="/resources/css/admui.css" rel="stylesheet"/>
	<link href="/resources/lib/select2/css/select2.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script type="text/javascript" src="/resources/lib/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="/resources/lib/select2/js/i18n/zh-CN.js"></script>
    <script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

	<#import "/base/util/macro.ftl" as macro>
	<style>
		.control-label{width:100px;text-align: left;}
	</style>
</head>
<body style="padding-top: 0;">
<div class="content-wrapper  animated fadeInRight">
	<div class="container-fluid">
		<form id="form_show"  class="form-horizontal">
			<input type="hidden" value="1" name="available" id="available" />
			<div class="row form-group padding-5" style="border-bottom: 1px dashed #76838f;">
				<div class="col-sm-offset-2  " style="float: right">
					<input type="checkbox" id="draft">
					<label class=" " for="draft">未发布</label>
					<input type="checkbox" checked id="pass">
					<label class=" " for="pass">审核通过</label>
					<button type="button" onclick="save()" class="btn btn-primary margin-left-5"><i class="icon-ok"></i>保存</button>
				</div>
			</div>
			<input type="hidden" id="id" name="id" value="${article.id!}" />

			<#--<div class="form-group">
                <label class="col-sm-2 control-label">文章专区：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" class="form-control" name="datascope" datatype="*" value="${article.datascope}"/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>-->

			<div class="form-group">
				<label class="col-sm-2 control-label">文章标题：</label>
				<div class="col-sm-6 formControls">
					<input type="text" class="form-control" style="width: 70%;float:left;" name="title" datatype="*" placeholder="文章标题" value="${article.title!}"/>
					<div style="float: left;">&nbsp;&nbsp;&nbsp;<input type="checkbox" name="isCopy" id="isCopy">
						<label class=" control-label" for="isCopy">是否转载</label></div>
				</div>
				<div class="col-sm-4">
					<div class="Validform_checktip"></div>
				</div>
			</div>


			<div class="form-group" id="div_isCopy_extend" style="display: none;">
				<label class="col-sm-2 control-label">链接：</label>
				<div class="col-sm-6 formControls">
					<input type="hidden" name="copy_flag" id="copy_flag" value="0">
					<input type="text" class="form-control" id="link" name="link" placeholder="链接"  value="${article.link!}"/>
				</div>
				<div class="col-sm-4">
					<div class="Validform_checktip"></div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">标签：</label>
				<div class="col-sm-6 formControls">
					<select class="form-control"  id="tags" multiple class="select2" value="${article.tags!}"  style="width: 70%;float:left;border-color:#e4eaec;">
					</select>

					<input type="text"  class="form-control" style="float: right;width: 30%;" placeholder="新标签" id="newTag">

				</div>
				<div class="col-sm-4">
					<a class="btn btn-success" onclick="addTags()">增加标签</a>
					<div class="Validform_checktip"></div>
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label">关键词：</label>
				<div class="col-sm-6 formControls">
					<input type="text" class="form-control" name="keywords" placeholder="关键词"  value="${article.keywords!}"/>
				</div>
				<div class="col-sm-4">
					<div class="Validform_checktip"></div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">封面图：</label>
				<div class="col-sm-6 formControls">
					<input type="text" class="form-control" placeholder="封面图"  name="cover_image" id="cover_image"    value="${article.cover_image!}"/>
				</div>
				<div class="col-sm-4">
                    <a class="btn btn-success" onclick="previewCoverImg()">预览</a>
					<div class="Validform_checktip"></div>
				</div>
			</div>

            <div class="form-group">
                <label class="col-sm-2 control-label">文章模型：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" id="article_model" name="article_model" placeholder="文章模型" value="${article.article_model!}" class="form-control" datatype="*" nullmsg="请选择文章模型" />
                    <@macro.dicSelect id="articleModel" value="${article.article_model!}" changeCallBack="articleModelChange()" idAttr="article_model" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">文章专区：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" id="article_region" name="article_region" placeholder="文章专区" value="${article.article_region!}" class="form-control" datatype="*" nullmsg="请选择文章专区" />
                    <@macro.dicSelect id="articleRegion" value="${article.article_region!}" changeCallBack="" idAttr="article_region" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">来源：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" class="form-control" name="extra2" placeholder="来源"  value="${article.extra2!}"/>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">来源站点：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" id="extra1" name="extra1" placeholder="来源站点" value="${article.extra1!}" class="form-control" datatype="*" nullmsg="请选择来源站点" />
                    <@macro.dicSelect id="siteName" value="${article.article_region!}" changeCallBack="" idAttr="extra1" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

			<div class="form-group">
				<label class="col-sm-2 control-label">描述：</label>
				<div class="col-sm-6 formControls">
					<textarea rows="4" class="form-control" name="description"  placeholder="描述" id="description" datatype="*"  >${article.description!}</textarea>
				</div>
				<div class="col-sm-4">
					<div class="Validform_checktip"></div>
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label">正文：</label>
				<div class="col-sm-10 formControls">
					<script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
				</div>

			</div>

			<textarea hidden="hidden" id="divdata">${article.content!}</textarea>
        </form>
	</div>
</div>

<@macro.ueditorScript />




<script type="text/javascript">

	//实例化编辑器
	//建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
	var ue = UE.getEditor('editor');
	function getAllHtml() {
		alert(UE.getEditor('editor').getAllHtml())
	}
	function getContent() {
		return UE.getEditor('editor').getContent();
	}
	function hasContent() {
		return UE.getEditor('editor').hasContents();
	}

</script>

<script type="text/javascript">
	var sys_ctx="";
    function articleModelChange(){
        var v=$("#article_model").val();
        if(v=="duoguyu"){
            $("#extra1").attr("datatype","*");
		}else{
            $("#extra1").removeAttr("datatype");
		}
	}
    var previewCoverImg=function(){
        $("#cover_image").blur();
        var url=$("#cover_image").val();
        if(isNotEmpty(url)){
            var b=Fast.isExist(url);
            if(b){
                imgshow("#outerdiv", "#innerdiv", "#bigimg", url);
            }else{
                Fast.msg_error("图片不存在!");
            }
        }
    }
    function imgshow(outerdiv, innerdiv, bigimg, url){
        // var src = _this.attr("src");
        $(bigimg).attr("src", url);

        $("<img/>").attr("src", url).load(function(){
            var windowW = $(window).width();
            var windowH = $(window).height();
            var realWidth = this.width;
            var realHeight = this.height;
            var imgWidth, imgHeight;
            var scale = 0.8;

            if(realHeight>windowH*scale) {
                imgHeight = windowH*scale;
                imgWidth = imgHeight/realHeight*realWidth;
                if(imgWidth>windowW*scale) {
                    imgWidth = windowW*scale;
                }
            } else if(realWidth>windowW*scale) {
                imgWidth = windowW*scale;
                imgHeight = imgWidth/realWidth*realHeight;
            } else {
                imgWidth = realWidth;
                imgHeight = realHeight;
            }
            $(bigimg).css("width",imgWidth);

            var w = (windowW-imgWidth)/2;
            var h = (windowH-imgHeight)/2;
            $(innerdiv).css({"top":h, "left":w});
            $(outerdiv).fadeIn("fast");
        });

        $(outerdiv).click(function(){
            $(this).fadeOut("fast");
        });
    }

	function addTags(){
		if(isNotEmpty($("#newTag").val())){
			var ids = $("#tags").select2('val');
			if(isNotEmpty(ids)&&ids.length>=3){
				Fast.msg_error("您最多能添加三个标签!");
				$("#newTag").val("");
				return;
			}
			// getTags();
			var id=new Date().getTime();
			var t={id:id,text:$("#newTag").val(),newflag:1};
			dataList.push(t);
			var obj= $("#tags").select2({
				data: dataList,
				language: "zh-CN",
				maximumSelectionLength: 3, //设置最多可以选择多少项
				placeholder: '请选择标签'
			} );

			if(isNotEmpty(ids)){
				ids.push(id);
			}else{
				ids=[];
				ids.push(id);
			}
			obj.val(ids).trigger("change");
			$("#newTag").val("");

		}
	}

	var validform;
	function save() {
		//var d = getTags();
		//alert(JSON.stringify(d));
		//return;
		var b = validform.check(false);
		if (!b) {
			return;
		}
		var tags = getTags();
		if(isEmpty(tags)|| tags.length==0){
			$.layer.alert_wrong("请输入标签(1-3个)！");
			return;
		}
		//var c = getContentTxt();
		var v = getContent();
		if (v== '' || v.length == 0) {
			$.layer.alert_wrong("请输入正文！");
			return;
		}
		//$("#content").val(v);
		var params = $("#form_show").serialize();
		params=Fast.changeUrlArg(params,"editorValue","");//因为不需要传递这个值到后台所以替换成空
		$.ajax({
			type: "post",
			url: '/cms/article/json/save?' + params,
			data: {content: v,tags:tags},
			success: function (json, textStatus) {
				ajaxReturnMsg(json);
				setTimeout(function () {
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				}, 1000);
			}
		});
	}
	function queryTags(){
		var searchText=$("#tags").val();
		if(isNotEmpty(searchText)&&searchText.length>2){
			sys_ajaxPost("/cms/tag/json/tags",{name:searchText},function(data){
				var json=typeof data=='string'?JSON.parse(data):data;
				var type=json.type;
				if(type=='success'){
					var dataArr=json.data.data;
					alert(JSON.stringify(dataArr));
				}
			});
		}
	}

	function getTags(){
		//var v = $("#tags").select2('val');//只获取id
		var obj=$("#tags").select2('data');
		var res=[];
		$.each(obj,function(i,data){
			var t={id:data.id,name:data.text,newflag:data.newflag};
			res.push(t);
		});
		// alert(typeof text+"===="+JSON.stringify(text));
		return JSON.stringify(res);
	}

	var dataList=[];

	function initSelect2(){

		var dataArr;
		sys_ajaxPost("/cms/tag/json/tags",null,function(data){
			var json=typeof data=='string'?JSON.parse(data):data;
			var type=json.type;
			if(type=='success'){
				dataArr=json.data.data;
				$.each(dataArr,function(v,data){
					//alert(v+"==="+JSON.stringify(data));
					var t={id:data.id,text:data.name};
					// alert(t);
					dataList.push(t);
				});
				//alert(JSON.stringify(dataList));
				var sel2=$("#tags").select2({
					data: dataList,
					language: "zh-CN",
					maximumSelectionLength: 3, //设置最多可以选择多少项
					placeholder: '请选择标签'
				} );


				sel2.val(tagsArr).trigger("change");
				//$("#select2_Id").val("XXXXX").select2()
			}
		});
	}
	var tags='${article.tags!}';
	var tagsArr=tags.split(",");
	$(function () {

		initSelect2();
		$("#isCopy").on("change",function(){
			var v=$(this).prop("checked");
			if(v){
				$("#copy_flag").val("1");//转载标志
				$("#div_zyfl").hide();//隐藏资源分类

				$("#div_isCopy_extend").show();
				$("#link").attr("datatype","*");
			}else{
				$("#copy_flag").val("0");
				$("#div_zyfl").show();//隐藏资源分类

				$("#div_isCopy_extend").hide();
				$("#link").removeAttr("datatype");
			}
		});
		$("#draft").on("change",function(){
			var v=$(this).prop("checked");
			if(v){
				$("#available").val("0");
				$("#pass").prop("checked",false);
			}else{
				$("#available").val("1");
				$("#pass").prop("checked",true);
			}
		});
		$("#pass").on("change",function(){
			var v=$(this).prop("checked");
			if(v){
				$("#available").val("1");
				$("#draft").prop("checked",false);
			}else{
				$("#available").val("0");
				$("#draft").prop("checked",true);
			}
		});
		var available='${article.available!}';
		if(available=='1'){
			$("#pass").prop("checked",true).change();
		}
        if(available=='0'){
            $("#draft").prop("checked",true).change();
        }

		var copy_flag='${article.copy_flag!}';
		if(copy_flag=='1'){
			$("#isCopy").prop("checked",true).change();
		}

		//给UEeditor赋值
		var data=$("#divdata").text();
		var ue = UE.getEditor('editor');
		var proinfo=$("#divdata").text();
		ue.ready(function() {//编辑器初始化完成再赋值
			ue.setContent(data);  //赋值给UEditor
		});

		validform = $("#form_show").Validform({
			tiptype: 2,
			postonce: true,//至提交一次
			ajaxPost: false,//ajax方式提交
			showAllError: true //默认 即逐条验证,true验证全部
		});
	})
</script>
                    <div id="outerdiv" style="position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0.7); z-index: 2; width: 100%; height: 100%; display: none;">
                        <div id="innerdiv" style="position: absolute; top: 80.8px; left: 446.079px;">
                            <img id="bigimg" style="border: 5px solid rgb(255, 255, 255); width: 969.842px;" src="">
                        </div>
                    </div>
</body>
</html>
