<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>文章管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <#import "/base/util/macro.ftl" as macro>
</head>

<body class="body-bg main-bg">
<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i>内容管理<span class="c-gray en">&gt;</span>文章管理</nav>
<div class="page animation-fade">
    <div class="tab-content margin-horizontal-5">
        <div class="animation-slide-right tab-message mainContent">
            <form  class="form-inline"  id="form_query">
                <div class="form-group">
                    <label class="control-label" for="cx_title">标题:</label>
                    <input type="text" class="form-control" name="cx_title" id="cx_title" placeholder="标题">
                </div>

                <div class="form-group">
                    <label class="control-label">文章专区：</label>

                    <input type="hidden" id="article_region" name="article_region" placeholder="文章专区" value="" class="form-control" datatype="*" nullmsg="请选择文章专区" />
                    <@macro.dicSelect id="articleRegion" value="" changeCallBack="" idAttr="article_region" />
                </div>

                <div class="form-group">
                    <label class="control-label" for="available">审核状态:</label>
                    <span class="select-box form-control">
                        <select class="select"  id="available" name="available">
                            <option value="" selected>全部</option>
                            <option value="0">草稿</option>
                            <option value="2">待审核</option>
                            <option value="1">已审核</option>
                        </select>
                    </span>
                </div>

                <div  class="form-group">
                    <div class="btn-group">
                        <a onclick="add()" class="btn btn-outline btn-success"><i class="fa fa-plus"></i>&nbsp;新增</a>
                        <button type="button" onclick="query('form_query')" class="btn btn-outline btn-primary"><i class="fa fa-search"></i>&nbsp;查询</button>
                        <button type="button" onclick="search_form_reset('form_query')" class="btn btn-outline btn-warning"><i class="fa fa-reply"></i>&nbsp;重置</button>
                        <a onclick="staticArticles()" class="btn btn-outline btn-primary"><i class="fa fa-pencil"></i>&nbsp;批量静态化</a>
                        <a onclick="publishArticles()" class="btn btn-outline btn-danger"><i class="fa fa-upload"></i>&nbsp;批量发布</a>
                        <a onclick="pushToBaidu()" class="btn btn-outline btn-warning"><i class="fa fa-upload"></i>&nbsp;推送到百度</a>


                    </div>
                </div>
            </form>
            <!--数据列表-->
            <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

            <script type="text/javascript">
                var sys_ctx="";
                var queryStr="?";
                //查询功能的标签说明
                var table_list_query_form = {
                    available:'',
                    cx_title:'',
                    article_region:''
                };
                function refreshTable() {
                    $('#tableList').bootstrapTable('refresh');
                }
                function add(){
                    layer_open_full("新增文章",sys_ctx+"/cms/article/add",null,function(){
                        refreshTable();
                    });
                }
                function sys_table_list(){
                    $('#tableList').bootstrapTable('destroy');
                    var columns = [
                        {checkbox:true},
                        {field:'copy_flag',title:'转载/原创',align:'center',visible:false,width:"100px",formatter:function(v,row){
                                if(v==1||v=='1'){
                                    return "<font color='red'>转载</font>";
                                }else{
                                    return "<font color='green'>原创</font>";
                                }
                            }
                         },

                        {field:'article_model',title:'文章模型',align:'center',visible:false,width:"100px"},
                        {field:'article_model_name',title:'文章模型',align:'center',width:"100px",formatter:function(v,row){
                            if(row.article_model == 'duoguyu'){
                                return "<font color='green'>多骨鱼</font>";
                            }else if(row.article_model == 'other'){
                                return "<font color='gray'>其他</font>";
                            }else{
                                return v;
                            }
                        }},
                        {field:'title',title:'文章标题',align:'left'},
                        {field:'cover_image',title:'封面图片',align:'left',visible:false},
                        {field:'keywords',title:'关键词',align:'center',visible:false},
                        {field:'description',title:'摘要',align:'center',visible:false},
                        {field:'hit',title:'点击数',align:'center',width:"80px"},
                        {field:'static_url',title:'静态页面',align:'center',width:"100px",formatter:function(v,row){
                                if(isNotEmpty(v)){
                                    return "<a class=\"btn btn-sm btn-outline btn-info \" onclick='return beforePreView(this.href)' target='_blank' href= \""+v+"\" type=\"button\"><i class=\"fa fa-eye\"></i> 查看</a>";
                                }else{
                                    return "-";
                                }
                            }
                        },
                        {field:'操作',title:'动态页面',align:'center',width:"100px",formatter:function(v,row){
                                    return "<a class=\"btn btn-sm btn-outline btn-info \" href='/myblog/article/detail/"+row.id+"' target='_blank' href= \""+v+"\" type=\"button\"><i class=\"fa fa-eye\"></i> 查看</a>";
                            }
                        },
                        {field:'available',title:'状态',align:'center',width:"100px",formatter:function(v){
                            if(v==1|v=='1'){
                                return "<font color='green'>已发布</font>";
                            }else if(v==0||v=='0'){
                                return "<font color='gray'>未发布</font>";
                            }else if(v==2||v=='2'){
                                return "<font color='red'>待审核</font>";
                            }else{
                                return v;
                            }
                        }},
                        {field:'tags',title:'标签',align:'center',width:"10%",visible:false},
                        {field:'tags_name',title:'标签',align:'center',width:"10%"},
                        {field:'操作',title:'操作',align:'center',width:"10%",formatter:function(v,row){
                            var btn="<div class=\"btn-group\">";
                            btn+="<button class=\"btn btn-sm btn-outline btn-info \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pencil\"></i> 编辑</button>";
                            btn+="<button class=\"btn btn-sm btn-outline btn-success \" onclick=\"staticArticle('"+row.id+"')\" type=\"button\"><i class=\"fa fa-clipboard\"></i> 静态化</button>";
                            btn+="</div>";
                            return btn;
                        }}
                    ];
                    table_list_Params.columns=columns;
                    table_list_Params.onClickRow=function(){};
                    table_list_Params.pageSize=15;
                    table_list_Params.url='/cms/article/json/find'+queryStr;
                    $('#tableList').bootstrapTable(table_list_Params);
                }
                var editRow=function(id){
                    layer_open_full("编辑文章",sys_ctx+"/cms/article/edit?id="+id,null,function(){
                        refreshTable();
                    },"",true);
                }
                var beforePreView=function(v){
                    if(isNotEmpty(v)){
                        var b=Fast.isExist(v);
                        if(b){
                            return true;
                        }
                    }
                    Fast.msg_error("页面不存在!");
                    return false;
                }
                var staticArticle=function(id){
                    Fast.confirm("确定要执行静态化吗?",function(){
                        $.ajax({
                            type:"post",
                            url:'/cms/article/json/staticArticle/'+id,
                            data:null,
                            success:function(data,textStatus){
                                ajaxReturnMsg(data);
                                $('#tableList').bootstrapTable('refresh');
                            }
                        });
                    },function(){});
                }
                var staticArticles=function(){
                    var selecRow = $("#tableList").bootstrapTable('getSelections');
                    if(selecRow.length > 0){
                        Fast.confirm("确定批量静态化吗？", function(){
                            var ids = new Array();
                            for(var i=0;i<selecRow.length;i++){
                                ids[ids.length] = selecRow[i]["id"]
                            }
                            $.ajax({
                                type:"post",
                                url:'/cms/article/json/staticArticles/'+ids,
                                data:null,
                                success:function(data,textStatus){
                                    ajaxReturnMsg(data);
                                }
                            });
                        });
                    }else{
                        Fast.msg_warning("请选择要静态化的文章!");
                    }
                }
                var publishArticles=function(){
                    var selecRow = $("#tableList").bootstrapTable('getSelections');
                    if(selecRow.length > 0){
                        Fast.confirm("确定批量发布文章吗？", function(){
                            var ids = new Array();
                            for(var i=0;i<selecRow.length;i++){
                                ids[ids.length] = selecRow[i]["id"]
                            }
                            $.ajax({
                                type:"post",
                                url:'/cms/article/json/batchPublish/'+ids,
                                data:null,
                                success:function(data,textStatus){
                                    ajaxReturnMsg(data);
                                    query('form_query')
                                }
                            });
                        });
                    }else{
                        Fast.msg_warning("请选择要发布的文章!");
                    }
                }
                var pushToBaidu=function(){
                    var selecRow = $("#tableList").bootstrapTable('getSelections');
                    if(selecRow.length > 0){
                        Fast.confirm("确定批量推送文章到百度吗？", function(){
                            var ids = new Array();
                            for(var i=0;i<selecRow.length;i++){
                                ids[ids.length] = selecRow[i]["id"]
                            }
                            $.ajax({
                                type:"post",
                                url:'/cms/article/json/pushToBaidu/urls',
                                traditional: true,
                                data:{"ids":ids
                                },
                                success:function(data,textStatus){
                                    ajaxReturnMsg(data,function(){
                                        query('form_query')
                                    });
                                }
                            });
                        });
                    }else{
                        Fast.msg_warning("请选择要推送的文章!");
                    }
                }
                var query = function(formid){
                    queryStr="?";
                    var qArr = $("#"+formid)[0];//查询表单区域序列化重写
                    var queryStrTem="&";
                    for(var i=0;i<qArr.length;i++){
                        var id = qArr[i].id;
                        if(typeof table_list_query_form[id] != 'undefined')
                        {
                            table_list_query_form[id] = $("#"+id).val();
                            queryStrTem+="&"+id+"="+$("#"+id).val();
                        }
                    }
                    queryStrTem=queryStrTem.substring(1);
                    queryStr+=queryStrTem;

                    sys_table_list();
                }

                function search_form_reset(tableid){
                    $('#'+tableid)[0].reset();

                    $("#article_region").val("");

                }
                $(function(){

                    query("form_query");
                })
            </script>
        </div>
    </div>

</div>


<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>

<script type="text/javascript" >

</script>
</body></html>
