<!DOCTYPE html>
<html>
<head>
  <title>新增标签</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center">新增标签</h2>
        <div class="form-group">
            <label class="col-sm-2 control-label">标签名称：</label>
            <div class="col-sm-6 formControls">
                <span class="select-box form-control">
                        <select class="select"  id="tag_type" name="tag_type" datatype="*" nullmsg="请选择标签类型" >
                            <option value="" selected>--请选择--</option>
                            <option value="system">系统</option>
                            <option value="personal">个人</option>
                        </select>
                </span>
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">标签名称：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="name" name="name" placeholder="标签名称" value="" class="form-control" datatype="*" nullmsg="请输入分类名称" />
            </div>
            <div class="col-sm-4">
              	<div class="Validform_checktip"></div>
            </div>
         </div>

		<#--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-2">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/cms/tag/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
