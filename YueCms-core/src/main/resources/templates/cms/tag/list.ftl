<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>标签管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">



</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 内容管理 <span class="c-gray en">&gt;</span>标签管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a style="display: none;" href="javascript:void(0)"  onClick="sortMenu();" class="btn btn-sm btn-info" ><i class="fa fa-sort"></i>&nbsp;排序</a>
                <a onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                <a onclick="staticTags()" class="btn btn-outline btn-primary"><i class="fa fa-pencil"></i>&nbsp;一键静态化</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
    <div id="div_sort" title="菜单排序" style="width:350px;height:400px;display: none;">
        <div class="c_div_tool_bar">
            <a href="javascript:void(0)" class="btn btn-primary size-S" style="margin:4px;" onClick="saveSort()">保存</a>
        </div>
        <div class="c_div_show_content">
            <form id="form_sort">
                <div id="dd" style="width:100%;">
                    <ul id="sortable">

                    </ul>
                </div>
            </form>
        </div>
    </div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
<script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    var staticTags=function(){
        var selecRow = $("#tableList").bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定批量静态化吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/tag/json/staticTags/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要静态化的标签!");
        }
    }
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {
                field: 'tag_module',
                align: "center",
                sortable: true,
                order: "asc",
                visible: true,
                width:"140px",
                title: '所属模块',
                formatter: function (v) {
                    if (v == "fullTabs") {
                        return "<font color='red'>首页FullTabs</font>";
                    } else {
                        return "<font color='gray'>-</font>";
                    }
                }
            },
            {field: 'tag_type',align:"center",sortable:true,order:"asc",visible:true,title: '标签类型',width:"120px",formatter:function(v){
                if(v=="system"){
                    return "<font color='red'>系统</font>";
                }else if(v=='personal'){
                    return "<font color='green'>个人</font>";
                }else{
                    return "<font color='gray'>-</font>";
                }
             }
             },
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:false,title: '谁增加的该标签'},
            {field: 'id',align:"center",sortable:true,order:"asc",visible:true,title: 'ID',width:"120px"},
            {field: 'name',align:"center",sortable:true,order:"asc",visible:true,title: '名称'},
            {field:'操作',title:'操作',align:'center',width:"10%",formatter:function(v,row){
                    var btn="<div class=\"btn-group\">";
                    btn+="<button class=\"btn btn-sm btn-outline btn-info \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pencil\"></i> 编辑</button>";
                    btn+="<button class=\"btn btn-sm btn-outline btn-success \" onclick=\"staticArticle('"+row.id+"')\" type=\"button\"><i class=\"fa fa-clipboard\"></i> 静态化</button>";
                    btn+="</div>";
                    return btn;
                }}
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/cms/tag/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    var editRow=function(id){
        $.layer.open_page("编辑标签",sys_ctx+"/cms/tag/edit?id="+id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function addNew(){
        $.layer.open_page("新增标签",sys_ctx+"/cms/tag/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    var staticArticle=function(id){
        Fast.confirm("确定要执行静态化吗?",function(){
            $.ajax({
                type:"post",
                url:'/sys/static/tag/'+id,
                data:null,
                success:function(data,textStatus){
                    ajaxReturnMsg(data);
                    $('#tableList').bootstrapTable('refresh');
                }
            });
        },function(){});
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/tag/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
    var sortMenu = function(){
        $.ajax({
            type:"post",
            url:'/cms/tag/json/findByParentid/'+tmp_parentid,
            data:null,
            success:function(data,textStatus){
                var json=typeof data=='string'?JSON.parse(data):data;
                var str = "";
                var rows=json.rows;
                console.log(json);
                for(var i=0;i<rows.length;i++){
                    str += '<li class="ui-state-default" id="xh_'+rows[i]['id']+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+rows[i]['name']+'</li>';
                };
                $("#sortable").html('').append(str);
                $("#sortable").sortable();
                $("#sortable").disableSelection();
            }
        });
        layer_openHtml("排序",$("#div_sort"),null,{width:'350px',height:'400px'})
        $("#div_sort").show();

    }
    var saveSort = function(){
        var arr = $("#sortable").sortable('toArray');
        for(var i=0;i<arr.length;i++){
            arr[i] = arr[i] + "_"+(i+1)
        }
        var sort = arr.join(',');
        $.ajax({
            type:"post",
            url:'/cms/tag/json/saveSort',
            data:{sort:sort},
            success:function(data,textStatus){
                ajaxReturnMsg(data);
                query("form_query");
                setTimeout(function(){
                    layer.closeAll();
                },1000);
            }
        });
    }
</script>
</body></html>
