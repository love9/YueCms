<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>模板管理管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <link href="/resources/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css"/>
    <script src="/resources/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>
</head>

<body class="body-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> cms <span class="c-gray en">&gt;</span>模板管理管理</nav>
<div class="wrapper animated fadeInRight">
<div style="position:relative;width:100%;height:auto;overflow: hidden">
    <div class="clearfix" style="width:200px;height:auto;float:left;overflow-y: auto;" id="west">
        <div style="height:5px;margin:5px 0 0 0px;width:195px;height:30px;">
            <a href="#" class="easyui-linkbutton" onClick="refreshTree()" plain="true">刷新</a>
        </div>
        <div  style="vertical-align: top;margin:0 0 0 0px;width:195px;">
            <div>
                <ul  class="ztree"  id="trees">

                </ul>
            </div>
        </div>
    </div>

    <div id="mainPanle" class=" main-bg" width="100%;float:left;" style="overflow-y: auto;height: 100%;">
    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
            <div class="btn-group">
                <a onclick="addNew()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-sm btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a id="open_search"  onclick="javascript:$('#search_hidden').show();$(this).hide();" class="btn btn-sm btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
            <div id="search_hidden"  style="display: none;margin-top: 8px"  >

                <div class="btn-group">
                    <a onclick="query('form_query');" class="btn btn-sm btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                    <a onclick="search_form_reset('form_query')" class="btn btn-sm btn-warning" ><i class="fa fa-trash"></i>&nbsp;清空</a>
                    <a  onclick="javascript:$('#search_hidden').hide();$('#open_search').show();" class="btn btn-default btn-sm"><i class="fa fa-close"></i>&nbsp;关闭</a>
                </div>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
    </div>
</div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" >
    var sys_ctx="";
    var tmp_parentid='&rootName=模板管理';
    var tmp_parentname='';
    var zTreeObj;
    var treeSetting = {
        data: {
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: 'parentid'
            }
        },
        async:{
            enable:true,
            dataType: "text",
            url: "/cms/resourcetype/json/ztree?parentid="+tmp_parentid
            //autoParam: ["id=parentid"]
        },
        view: {
            showLine: true,
            showIcon: true
        },
        callback: {
            beforeAsync: zTreeBeforeAsync,
            onAsyncSuccess: zTreeAjaxSuccess,
            //onAsyncError: zTreeAjaxError,
            onClick: zTreeOnClick
        }
    };
    function initZtree(){
        $.fn.zTree.init($("#trees"), treeSetting, null);
        zTreeObj = $.fn.zTree.getZTreeObj("trees");
    }
    //加载完成的回调
    function zTreeAjaxSuccess(event, treeId, treeNode, msg){
        var nodes=zTreeObj.getNodes();
        zTreeObj.expandNode(nodes[0], true, false, false);
        tmp_parentid=nodes[0].id;
        tmp_parentname=nodes[0].name;
        sys_table_list();
    }
    function zTreeBeforeAsync(treeId, treeNode){
        if (treeNode) {
            tmp_parentid=	treeNode.id ;
            zTreeObj.setting.async.url="/cms/resourcetype/json/ztree?parentid="+tmp_parentid;
        }
    }
    function zTreeOnClick(event, treeId, treeNode,clickFlag) {
        tmp_parentid=treeNode.id ;
        tmp_parentname=treeNode.name ;
        //alert(tmp_parentid+"=="+tmp_parentname);
        sys_table_list();
    }
    function refreshTree(){
        tmp_parentid='&rootName=模板管理';
        zTreeObj.setting.async.url="/cms/resourcetype/json/ztree?parentid="+tmp_parentid;
        zTreeObj.reAsyncChildNodes(null, "refresh");
    }
    var queryStr="";
    //查询功能的标签说明
    var table_list_query_form = {

    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        initZtree();
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'orgid',align:"center",sortable:true,order:"asc",visible:false,title: '组织id'},
            {field: 'name',align:"left",sortable:true,order:"asc",visible:true,title: '名称'},
            {field: 'resourcetypeid',align:"center",sortable:true,order:"asc",visible:false,title: '资源类别id'},
            {field: 'filePath',align:"left",sortable:true,order:"asc",visible:false,title: '模版路径'},
            {field: 'fileName',align:"left",sortable:true,order:"asc",visible:true,title: '模版文件名'},
            {field: 'content',align:"left",sortable:true,order:"asc",visible:false,title: '模版内容'},
            {field: 'remarks',align:"left",sortable:true,order:"asc",visible:true,title: '备注信息'},
            {field: 'operate', align:"center", title: '内容',width:'240px',
                formatter: function(value, row, index){
                    var  btns="<div class='btn-group'>";
                    btns+="<button class=\"btn btn-xs btn-info \" onclick=\"editTemplate('"+row.id+"')\" type=\"button\"><i class=\"fa fa-paste\"></i> 编辑</button>";
                    btns+="<button class=\"btn btn-xs btn-success \" onclick=\"viewContent('"+row.id+"')\" type=\"button\"><i class=\"fa fa-eye\"></i> 查看</button>";
                    if(row.filePath!=''){
                        btns+="<button class=\"btn btn-xs btn-warning \" onclick=\"preView('"+row.id+"')\" type=\"button\"><i class=\"fa fa-search\"></i> 预览</button>";
                    }else{
                        btns+="<button class=\"btn btn-xs btn-default \"  type=\"button\"><i class=\"fa fa-paste\"></i> 预览</button>";
                    }
                    btns+="</div>";
                    return btns;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/cms/template/json/find?resourcetypeid='+tmp_parentid+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    function preView(id){
        layer_open("预览","/cms/template/preView?id="+id,window.top);

    }
    var viewContent=function(id){
        $.layer.open_page_full("编辑模板内容",sys_ctx+"/cms/template/editContent?id="+id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    var editTemplate=function(id){
        var params="&parentid="+tmp_parentid+"&parentname="+tmp_parentname;
        $.layer.open_page("编辑模板管理",sys_ctx+"/cms/template/edit?id="+id+params,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        var params="?parentid="+tmp_parentid+"&parentname="+tmp_parentname;
        $.layer.open_page("新增模板管理",sys_ctx+"/cms/template/add"+params,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            layer.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/template/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_error("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
