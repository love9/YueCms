<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>模板管理管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <#import "/base/util/macro.ftl" as macro>
</head>

<body class="body-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> cms <span class="c-gray en">&gt;</span>模板管理管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
            <input type="hidden" id="resourcetypeid" readonly name="resourcetypeid" placeholder="模板类别"   class="form-control"   />
            <@macro.dicSelect id="template_type"  value="" changeCallBack=""  idAttr="resourcetypeid" />
            <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>

                <a onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                <a onclick="search_form_reset('form_query')" class="btn btn-outline btn-warning" ><i class="fa fa-trash"></i>&nbsp;重置</a>

            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>


</div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="";
    //查询功能的标签说明
    var table_list_query_form = {
        resourcetypeid:''
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'orgid',align:"center",sortable:true,order:"asc",visible:false,title: '组织id'},
            {field: 'resourcetypeid',align:"center",sortable:true,order:"asc",visible:true,title: '模板分类',width:"130px",
                formatter: function(v, row, index){
                    if(v=='gen'){
                        return "<span class=\"label label-primary radius\">代码生成</span>";
                    }else if(v=='spider'){
                        return "<span class=\"label label-info radius\">爬虫模板</span>";
                    }else if(v=='static'){
                        return "<span class=\"label label-success radius\">静态化模板</span>";
                    }else if(v=='mail'){
                        return "<span class=\"label label-danger radius\">邮件模板</span>";
                    }else{
                        return "<span class=\"label label-default radius\">未分类</span>";
                    }
                }
            },
            {field: 'name',align:"left",sortable:true,order:"asc",visible:true,title: '名称'},

            {field: 'filePath',align:"left",sortable:true,order:"asc",visible:false,title: '模版路径'},
            {field: 'fileName',align:"left",sortable:true,order:"asc",visible:true,title: '模版文件名'},
            {field: 'content',align:"left",sortable:true,order:"asc",visible:false,title: '模版内容'},
            {field: 'remarks',align:"left",sortable:true,order:"asc",visible:true,title: '备注信息'},
            {field: 'operate', align:"center", title: '内容',width:'240px',
                formatter: function(value, row, index){
                    var  btns="<div class='btn-group'>";
                    btns+="<button class=\"btn btn-xs btn-info \" onclick=\"editTemplate('"+row.id+"')\" type=\"button\"><i class=\"fa fa-paste\"></i> 编辑</button>";
                    btns+="<button class=\"btn btn-xs btn-success \" onclick=\"viewContent('"+row.id+"')\" type=\"button\"><i class=\"fa fa-eye\"></i> 查看</button>";
                    if(row.filePath!=''){
                        btns+="<button class=\"btn btn-xs btn-warning \" onclick=\"preView('"+row.id+"')\" type=\"button\"><i class=\"fa fa-search\"></i> 预览</button>";
                    }else{
                        btns+="<button class=\"btn btn-xs btn-default \"  type=\"button\"><i class=\"fa fa-paste\"></i> 预览</button>";
                    }
                    btns+="</div>";
                    return btns;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/cms/template/json/find?'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    function preView(id){
        layer_open("预览","/cms/template/preView?id="+id,window.top);
    }
    var viewContent=function(id){
        $.layer.open_page_full("编辑模板内容",sys_ctx+"/cms/template/editContent?id="+id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    var editTemplate=function(id){
        $.layer.open_page("编辑模板管理",sys_ctx+"/cms/template/edit?id="+id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增模板管理",sys_ctx+"/cms/template/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/template/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_error("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset();
        $("#resourcetypeid").val("");
    }
</script>
</body></html>
