
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>模板管理管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>


    <link rel="stylesheet" href="/resources/lib/jquery-treeview/jquery.treeview.css" />
    <link rel="stylesheet" href="/resources/lib/jquery-treeview/screen.css" />
    <script src="/resources/lib/jquery-treeview/lib/jquery.cookie.js"></script>
    <script src="/resources/lib/jquery-treeview/jquery.treeview.js" type="text/javascript"></script>

    <!--下面两个是使用Code Mirror必须引入的-->
    <link rel="stylesheet" href="/resources/lib/codemirror/codemirror.css">
    <script src="/resources/lib/codemirror/codemirror.js"></script>
    <script src="/resources/lib/codemirror/addon/selection/selection-pointer.js"></script>
    <!--支持那种语言-->

    <script src="/resources/lib/codemirror/mode/javascript/javascript.js"></script>
    <script src="/resources/lib/codemirror/mode/clike/clike.js"></script>
    <script src="/resources/lib/codemirror/mode/css/css.js"></script>
    <script src="/resources/lib/codemirror/mode/xml/xml.js"></script>
    <script src="/resources/lib/codemirror/mode/vbscript/vbscript.js"></script>
    <script src="/resources/lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <!--引入css文件，用以支持主题-->
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/eclipse.css">
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/seti.css">
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/dracula.css">

    <!--支持代码折叠-->
    <link rel="stylesheet" href="/resources/lib/codemirror/addon/fold/foldgutter.css"/>
    <script src="/resources/lib/codemirror/addon/fold/foldcode.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/foldgutter.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/brace-fold.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/comment-fold.js"></script>

    <!--括号匹配-->
    <script src="/resources/lib/codemirror/addon/edit/matchbrackets.js"></script>

    <!--自动补全-->
    <link rel="stylesheet" href="/resources/lib/codemirror/addon/hint/show-hint.css">
    <script src="/resources/lib/codemirror/addon/hint/show-hint.js"></script>
    <script src="/resources/lib/codemirror/addon/hint/anyword-hint.js"></script>

    <style>
        #tree  li>span.file{cursor:pointer;}
        #tree  li>span.file:hover,#tree  li>span.file.hover{cursor:pointer;color:red;}
        .CodeMirror{height:calc(100% - 45px);font-family: Menlo,Monaco,Consolas,"Andale Mono","lucida console","Courier New",monospace;}
        #textareaCode{min-height:300px}
    </style>
</head>

<body class="body-bg" style="">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> cms <span class="c-gray en">&gt;</span>模板管理管理</nav>
<div class="wrapper animated fadeInRight" style="height: calc(100% - 49px);">
<div style="position:relative;width:100%;height:auto;overflow: hidden">
    <div class="clearfix" style="width:225px;height:100%;float:left;overflow-y: auto;" id="west">

        <div  style="vertical-align: top;margin:0 0 0 0px;width:220px;">

           <#--
                后台的fileTree:格式如下：
                rootFiles：根下直接存在的文件，List<String>
                rootFolders：根下还可能存在一层文件夹。它的每个元素是一个Map<String,Object>，包含2个元素：folderName：文件夹名；folderFiles：文件夹下的文件。
            -->
            <ul id="tree" class="filetree">
                <#if fileTree??>
                    <#--根目录文件夹-->
                     <#if (fileTree.rootFolders??) && (fileTree.rootFolders?size>0) >
                         <#list fileTree.rootFolders as item>

                            <li  class="closed" class="folder">
                                <span class="folder">${item.name!}</span>
                                <ul>
                                <#if (item.folders??) && (item.folders?size>0) >
                                    <#list item.folders as item2>
                                    <li class="folder">
                                        <span class="folder">${item2.name!}</span>
                                        <ul>
                                        <#if (item2.folders??) && (item2.folders?size>0) >
                                            <#list item2.folders as item3>
                                                <li class="folder">
                                                    <span class="folder">${item3.name!}</span>
                                                <ul>
                                                    <#if (item3.folders??) && (item3.folders?size>0) >
                                                        <#list item3.folders as item4><#--到了这一级只加载文件-->
                                                            <#if (item4.files??) && (item4.files?size>0) >
                                                                <#list item4.files as file>
                                                                <ul>
                                                                    <li><span data-folder="${item.name!}#${item2.name!}#${item3.name!}#${item4.name!}"  class="file">${file}</span></li>
                                                                </ul>
                                                                </#list>
                                                            </#if>
                                                        </#list>

                                                    </#if>
                                                    <#if (item3.files??) && (item3.files?size>0) >
                                                        <#list item3.files as file>
                                                            <li><span data-folder="${item.name!}#${item2.name!}#${item3.name!}" class="file">${file}</span></li>
                                                         </#list>
                                                    </#if>
                                                </ul>
                                                </li>

                                            </#list>
                                        </#if>
                                        <#if (item2.files??) && (item2.files?size>0) >
                                            <#list item2.files as file>
                                                <li><span data-folder="${item.name!}#${item2.name!}" class="file">${file}</span></li>
                                            </#list>
                                        </#if>
                                        </ul>
                                    </li>

                                    </#list>
                                </#if>
                                <#if (item.files??) && (item.files?size>0) >
                                    <#list item.files as file>
                                        <li><span data-folder="${item.name!}" class="file">${file}</span></li>
                                    </#list>
                                </#if>
                                </ul>
                            </li>

                         </#list>
                     </#if>
                    <#--根目录文件-->
                    <#if (fileTree.rootFiles??) && (fileTree.rootFiles?size>0) >
                        <#list fileTree.rootFiles as rf>
                         <li><span class="file">${rf}</span></li>
                        </#list>
                    </#if>
                </#if>
            </ul>
        </div>
    </div>

    <div id="mainPanle" class=" main-bg" width="100%;float:left;" style="overflow-y: auto;height: 100%;">
    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="" class="form-inline">
            <div class="btn-group">
                <a onclick="saveFile()" class="btn btn-sm btn-primary"><i class="fa fa-save"></i>&nbsp;保存</a>

                <a onclick="preView()" class="btn btn-sm btn-success" ><i class="fa fa-eye"></i>&nbsp;预览</a>
            </div>
        </form>
    </div>

        <textarea id="textareaCode"></textarea>

        <script type="text/javascript">
            var codeEdit;
            var mixedMode = {
                name: "htmlmixed",
                scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,
                    mode: null},
                    {matches: /(text|application)\/(x-)?vb(a|script)/i,
                        mode: "vbscript"}]
            };
            function createEditor(){
                codeEdit=CodeMirror.fromTextArea(document.getElementById("textareaCode"),{

                    // mode:"text/javascript",
                    mode:mixedMode,
                    selectionPointer: true,
                    //显示行号
                    lineNumbers:true,
                    // styleActiveLine: true, //line选择是是否加亮
                    selectionPointer: true,
                    //设置主题
                    theme:"default",
                    //绑定Vim
                    // keyMap:"vim",
                    //代码折叠
                    lineWrapping:true,
                    foldGutter: true,
                    gutters:["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
                    //全屏模式
                    fullScreen:true,
                    //括号匹配
                    matchBrackets:true
                    //extraKeys:{"Ctrl-Space":"autocomplete"}//ctrl-space唤起智能提示
                });
            }

            $(function(){

                createEditor();

            });
        </script>

    </div>
    </div>
</div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" >
    var sys_ctx="";
    var now_folderName='';
    var now_fileName='';//当前文件.请求文件内容成功后赋值
    function initTree(){
        $("#tree").treeview();
    }
    function clearHoverClass(){
        $("#tree").find("span.file").each(function(i,o){
            $(o).removeClass("hover");
        });
    }
    function saveFile(){

        if(isNotEmpty(now_fileName)){
            var content=codeEdit.getValue();
            //保存模板内容
            sys_ajaxPost("/cms/template/json/saveFileContent",{folderName:now_folderName,fileName:now_fileName,content:content},function(json){
                ajaxReturnMsg(json);
            });
        }
    }
    function showFileContent(obj){
        clearHoverClass();
        $(obj).addClass("hover");


        //var folderName=$(obj).parents("li.folder").find("span.folder").text();

        var folderName="";
        folderName=$(obj).data("folder");
        if(isEmpty(folderName)){
            folderName="";
        }
        var fileName=$(obj).text()
        //alert(folderName+"========="+fileName);
        //请求模板文件内容
        sys_ajaxPost("/cms/template/json/getFileContent",{folderName:folderName,fileName:fileName},function(data){

            checkAjaxError(data);
           // $("#textareaCode").val(data);
            now_folderName=folderName;
            now_fileName=fileName;
            codeEdit.setValue(data);
        });
    }
    $(function(){

        initTree();
        $("#tree li>span.file").click(function(){
            //点击文件树，显示内容
            showFileContent(this);
        });
    });

    function preView(id){
        layer_open("预览","/cms/template/preView?id="+id,window.top);
    }
    var viewContent=function(id){
        layer_open_full("编辑模板内容",sys_ctx+"/cms/template/editContent?id="+id,null,function(){$('#tableList').bootstrapTable('refresh');});
    }
    var editTemplate=function(id){
        var params="&parentid="+tmp_parentid+"&parentname="+tmp_parentname;
        layer_open("编辑模板管理",sys_ctx+"/cms/template/edit?id="+id+params,null,function(){$('#tableList').bootstrapTable('refresh');});
    }
    function addNew(){
        var params="?parentid="+tmp_parentid+"&parentname="+tmp_parentname;
        layer_open("新增模板管理",sys_ctx+"/cms/template/add"+params,null,function(){$('#tableList').bootstrapTable('refresh');});
    }

</script>
</body></html>
