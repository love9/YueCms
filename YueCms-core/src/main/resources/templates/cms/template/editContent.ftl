<!DOCTYPE html>
<html>
<head>
  <title>编辑模板</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
	<link href="/resources/css/channelSite.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
	<script type="text/javascript" src="/resources/js/common/channelSite.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

	<#import "/base/util/macro.ftl" as macro>

	<style type="text/css">
		#preViewContent {
			/*display: none;*/
			position: fixed;
			padding:8px 25px;
			-moz-box-shadow:2px 2px 5px #333333; -webkit-box-shadow:2px 2px 5px #333333; box-shadow:2px 2px 5px #333333;
			left: 10%;
			top:10%;
			width: 80%;
			height: 80%;
			z-index: 1200;
			overflow: hidden;
			background: url(/resources/images/bgTree.png) left bottom repeat-x rgb(246, 246, 246);
		}
	</style>
    <!--下面两个是使用Code Mirror必须引入的-->
    <link rel="stylesheet" href="/resources/lib/codemirror/codemirror.css">
    <script src="/resources/lib/codemirror/codemirror.js"></script>
    <script src="/resources/lib/codemirror/addon/selection/selection-pointer.js"></script>
    <!--支持那种语言-->

    <script src="/resources/lib/codemirror/mode/javascript/javascript.js"></script>
    <script src="/resources/lib/codemirror/mode/clike/clike.js"></script>
    <script src="/resources/lib/codemirror/mode/css/css.js"></script>
    <script src="/resources/lib/codemirror/mode/xml/xml.js"></script>
    <script src="/resources/lib/codemirror/mode/vbscript/vbscript.js"></script>
    <script src="/resources/lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <!--引入css文件，用以支持主题-->
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/eclipse.css">
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/seti.css">
    <link rel="stylesheet" href="/resources/lib/codemirror/theme/dracula.css">

    <!--支持代码折叠-->
    <link rel="stylesheet" href="/resources/lib/codemirror/addon/fold/foldgutter.css"/>
    <script src="/resources/lib/codemirror/addon/fold/foldcode.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/foldgutter.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/brace-fold.js"></script>
    <script src="/resources/lib/codemirror/addon/fold/comment-fold.js"></script>

    <!--括号匹配-->
    <script src="/resources/lib/codemirror/addon/edit/matchbrackets.js"></script>

    <!--自动补全-->
    <link rel="stylesheet" href="/resources/lib/codemirror/addon/hint/show-hint.css">
    <script src="/resources/lib/codemirror/addon/hint/show-hint.js"></script>
    <script src="/resources/lib/codemirror/addon/hint/anyword-hint.js"></script>
	<style>
        .CodeMirror{height:calc(100% - 45px);font-family: Menlo,Monaco,Consolas,"Andale Mono","lucida console","Courier New",monospace;}
        #textareaCode{min-height:300px}
	</style>
</head>
<body  class="body-bg-code">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
        <input type="hidden" value="${template.id!}" id="id" name="id"/>
		<div class="form-group">
            <div class=" col-sm-12" style="text-align: left;">
              <button type="button" onclick="save()" class="btn btn-outline btn-primary " ><i class="fa fa-save"></i>&nbsp;&nbsp;保存</button>

				<#if template.filePath?? &&template.filePath!=''>
					<button type="button" onclick="preView()" class="btn btn-outline btn-warning " ><i class="fa fa-search"></i>&nbsp;&nbsp;预览</button>
				<#else>
					<button type="button"  class="btn btn-outline btn-default " ><i class="fa fa-search"></i>预览</button>
				</#if>
				<span class="label label-warning">改动在保存之后预览才能生效!</span>
			</div>

        </div>
      <div class="form-group">
          <div class=" col-sm-12" style="margin:8px auto; ">
              <textarea id="content"></textarea>
          </div>
      </div>

    </div>
</div>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript">
    var codeEdit;
    var mixedMode = {
        name: "htmlmixed",
        scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,
            mode: null},
            {matches: /(text|application)\/(x-)?vb(a|script)/i,
                mode: "vbscript"}]
    };
    function createEditor(){
        codeEdit=CodeMirror.fromTextArea(document.getElementById("content"),{

            // mode:"text/javascript",
            mode:mixedMode,
            selectionPointer: true,
            //显示行号
            lineNumbers:true,
            // styleActiveLine: true, //line选择是是否加亮
            selectionPointer: true,
            //设置主题
            theme:"default",
            //绑定Vim
            // keyMap:"vim",
            //代码折叠
            lineWrapping:true,
            foldGutter: true,
            gutters:["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
            //全屏模式
            fullScreen:true,
            //括号匹配
            matchBrackets:true
            //extraKeys:{"Ctrl-Space":"autocomplete"}//ctrl-space唤起智能提示
        });
    }
    function setValue(v){
        codeEdit.setValue(v);
    }
    function getValue(){
        var v= codeEdit.getValue();
        return v;
    }
    $(function(){
        createEditor();
    });
</script>
<script type="text/javascript">
	var sys_ctx="";
	function preView(){
		layer_open("预览","/cms/template/preView?id=${template.id!}",window.top);
	}
    function showFileContent(){
       var id="${template.id!}";
        sys_ajaxPost("/cms/template/json/editContent",{id:id},function(data){
            checkAjaxError(data);
            codeEdit.setValue(data);
        });
    }
	function save(){
	    var content=getValue();
        var  params="id="+$("#id").val();
		$.ajax({
			type:"post",
			url:'/cms/template/json/save?'+params,
			data:{content:content},
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
        showFileContent();
	})
</script>

</body>
</html>
