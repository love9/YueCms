<!DOCTYPE html>
<html>
<head>
    <title>编辑模板管理</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <#import "/base/util/macro.ftl" as macro>

</head>
<body  class="body-bg-edit">
<div class="content-wrapper  animated fadeInRight">
    <div class="container-fluid">
        <form action="" id="form_show" method="post" class="form-horizontal" role="form">
            <input type="hidden" value="${template.id!}" id="id" name="id"/>
            <input type="hidden" value="${template.filePath!}" id="filePathOld" name="filePathOld"/>
            <h2 class="text-center">编辑模板管理</h2>
            <div class="form-group">
                <label class="col-sm-2 control-label">资源类别：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden" id="resourcetypeid" readonly name="resourcetypeid" placeholder="模板类别" value="${template.resourcetypeid!}" datatype="*" nullmsg="请选择模板类型" class="form-control"   />
                <@macro.dicSelect id="template_type" value="${template.resourcetypeid!}" changeCallBack="changeTemplateType()" idAttr="resourcetypeid" />
                    <script>
                        function changeTemplateType(){
                            var v=$("#resourcetypeid").val();
                            if(isNotEmpty(v)){
                                var path= $("#filePath").val();
                                if(isEmpty(path)){
                                    path="/"+v;
                                }else{
                                    var temp="(/gen|/spider|/static|/mail)";
                                    var reg=new RegExp(temp,"g");
                                    path=path.replace(reg,"");
                                    if(path.substr(0,1)=='/'){}else{
                                        path="/"+path;
                                    }
                                    path="/"+v+path;
                                }
                                $("#filePath").val(path);
                            }
                        }
                    </script>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">模版：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="name" name="name" onkeyup="nameChange()" placeholder="模版名称" value="${template.name!}" class="form-control" datatype="*" nullmsg="请输入名称" />
                    <script>
                        function nameChange(){
                            var str=$('#name').val();
                            if (str.indexOf('.') != -1) {
                                str=str.replace(/\./g,"");
                                $('#name').val(str);
                            }
                            $('#fileName').val(str);
                            var type = $("#resourcetypeid").val();
                            if(isNotEmpty(type)){
                                str="/"+type+"/"+str;
                            }else{
                                str="/"+str;
                            }
                            $("#filePath").val(str);
                        }
                    </script>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">文件名：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="fileName" name="fileName" onkeyup="fileNameChange()"  placeholder="文件名" style="width: calc(100% - 85px);display: inline-block;" value="${template.fileName!}" class="form-control" datatype="*" nullmsg="请输入文件名" />
                    <input type="text" id="fileName_suffix"  style="width: 80px;display: inline-block;" readonly value="${properties.template_path_suffix!}" class="form-control"  />
                    <script>
                        function fileNameChange(){

                            var str=$('#fileName').val();

                            if (str.indexOf('.') != -1) {
                                str=str.replace(/\./g,"");
                                $('#fileName').val(str);
                            }
                            var type = $("#resourcetypeid").val();
                            if(isNotEmpty(type)){
                                str="/"+type+"/"+str;
                            }else{
                                str="/"+str;
                            }
                            $("#filePath").val(str);
                        }
                    </script>
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">文件路径：</label>
                <div class="col-sm-6 formControls">

                    <input type="text" id="filePath_prefix" style="width: 160px;display: inline-block;" readonly value="${properties.template_path_prefix!}" class="form-control" />
                    <input type="text" id="filePath" name="filePath" readonly style="width: calc(100% - 250px);display: inline-block;" placeholder="文件路径" value="${template.filePath!}" class="form-control" datatype="*" nullmsg="请输入文件路径" />
                    <input type="text" id="filePath_suffix" style="width: 80px;display: inline-block;" readonly value="${properties.template_path_suffix!}" class="form-control"  />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">备注信息：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="remarks" name="remarks" placeholder="备注信息" value="${template.remarks!}" class="form-control" />
                </div>
                <div class="col-sm-4">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

        <#--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-2">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
        </form>

    </div>
</div>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
    var validform;
    function save(){
        var b=validform.check(false);
        if(!b)
        {
            return;
        }
        var params=$("#form_show").serialize();
        var filePath=$("#filePath").val();
        var filePath_prefix=$("#filePath_prefix").val();
        var filePath_suffix=$("#filePath_suffix").val();
        filePath=filePath_prefix+filePath+filePath_suffix;
        params=Fast.changeUrlArg(params,"filePath",filePath);
        var fileName=$("#fileName").val();
        fileName=fileName+filePath_suffix;
        params=Fast.changeUrlArg(params,"fileName",fileName);
        $.ajax({
            type:"post",
            url:'/cms/template/json/save?'+params,
            data:null,
            success:function(json,textStatus){
                ajaxReturnMsg(json,function () {
                    setTimeout(function(){
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                    },1000);
                });

            }
        });
    }
    $(function(){
        validform=$("#form_show").Validform({
            btnReset:"#reset",
            tiptype:2,
            postonce:true,//至提交一次
            ajaxPost:false,//ajax方式提交
            showAllError:true //默认 即逐条验证,true验证全部
        });
    })
</script>

</body>
</html>
