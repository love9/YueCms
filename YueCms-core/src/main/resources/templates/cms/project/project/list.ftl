<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>项目管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <#--<link href="/resources/css/skin/${user_skin}.css" rel="stylesheet" />-->
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 项目管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl toolbar">
        <form id="form_query" style="margin: 5px 0px" class="form-inline">
            <div class="btn-group">

                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a  onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
           </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>

<script type="text/javascript" >

    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'name',align:"left", sortable:true, order:"asc", visible:true, title: '项目名称'},
            {field: 'code', align:"center", sortable:true, order:"asc", width:'100px', visible:true, title: '项目代码'},
            {field: 'teamname', align:"center", sortable:true, order:"asc", visible:false, title: '团队名称'},
            {field: 'starttime', align:"center", sortable:false, order:"asc", visible:false, title: '开始时间'},
            {field: 'endtime', align:"center", sortable:true, order:"asc", visible:false, title: '截止时间'},
            {field: 'description', align:"center", sortable:true, order:"asc", visible:false, title: '项目描述'},
            {field: 'accesscontrol', align:"center", sortable:true, order:"asc", visible:false, title: '访问控制类型'},
            {field: 'status', align:"center", sortable:true, width:'100px', order:"asc", visible:true, title: '项目状态'},
            {field: 'operate', align:"center", title: '操作',
                //width: '150px',
               // events: operateEvents1,
                formatter: function(value, row, index){
                    var btns="<div class=\"btn-group\">";
                    //这里通过后台传递过来的当前登录用户信息（角色等信息）判断该显示何种权限按钮。这种前台根据角色写死的方法不如该用户是否有操作该按钮权限判断来的灵活和精准
                    //因为如果遇到显示隐藏需求变更后者只需要对角色进行授权或取消授权即可，而前者需要改页面代码比如新增了项目经理角色
                    if('${isAdmin}'=='true'||'${role}'=='admin'){
                        btns+="<button class=\"btn btn-outline btn-info \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-paste\"></i> 编辑</button>";
                    }
                    btns+="<button class=\"btn btn-outline  btn-warning \" onclick=\"jumpModule('"+row.id+"','"+row.name+"')\" type=\"button\"><i class=\"fa fa-warning\"></i><span class=\"bold\">模块维护</span></button>";
                    btns+="<button class=\"btn btn-outline  btn-success \" onclick=\"jumpMission('"+row.id+"')\" type=\"button\"><i class=\"fa fa-cart-plus\"></i><span class=\"bold\">任务维护</span></button>";
                    btns+="<button class=\"btn btn-outline  btn-danger \" onclick=\"jumpBug('"+row.id+"')\" type=\"button\"><i class=\"fa fa-bug\"></i><span class=\"bold\">提Ｂｕｇ</span></button>";
                    btns+="</div>";
                    return btns;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/cms/project/project/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    //跳转到模块维护
    var jumpModule=function(projectId,projectName){
        projectName=encodeURIComponent(projectName);
        window.location.href="/cms/project/module?projectId="+projectId+"&projectName="+projectName;
    }
    //跳转到任务维护
    var jumpMission=function(projectId){
        window.location.href="/cms/project/mission?projectId="+projectId;
    }
    //跳转到Bug维护
    var jumpBug=function(projectId){
        window.location.href="/cms/project/bug?projectId="+projectId;
    }
    var editRow=function(id){
        $.layer.open_page("编辑项目","/cms/project/project/edit?id="+id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增项目","/cms/project/project/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/project/project/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        refreshData();
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
