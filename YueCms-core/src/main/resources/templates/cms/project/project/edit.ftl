<!DOCTYPE html>
<html>
<head>
  <title>编辑项目</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body class="body-bg-edit">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
    <input type="hidden" value="${project.id}" id="id" name="id"/>
		<h2 class="text-center">编辑项目</h2>

        <div class="form-group">
              <label class="col-sm-2 control-label">项目名称：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="name" name="name" placeholder="项目名称" value="${project.name}" class="form-control" datatype="*" nullmsg="请输入项目名称" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">项目代码：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="code" name="code" placeholder="团队内部简称" value="${project.code}" class="form-control" datatype="*" nullmsg="请输入项目代码。团队内部简称" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">团队名称：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="teamname" name="teamname" placeholder="团队名称" value="${project.teamname}" class="form-control" datatype="*" nullmsg="请输入团队名称" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">开始时间：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="starttime" name="starttime" placeholder="" value="${project.starttime}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">截止时间：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="endtime" name="endtime" placeholder="" value="${project.endtime}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">项目描述：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="description" name="description" placeholder="项目描述" value="${project.description}" class="form-control" datatype="*" nullmsg="请输入项目描述" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

        <div class="form-group">
              <label class="col-sm-2 control-label">项目状态：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="status" name="status" placeholder="项目状态" value="${project.status}" class="form-control" datatype="*" nullmsg="请输入项目状态" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

    </form></div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	   var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/cms/project/project/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		  btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
           });


	})
</script>

</body>
</html>
