<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>项目任务管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>


<style>
    /*解决表头拥挤的时候出现省略号而不能全部显示标题的情况*/
    .fixed-table-container tbody td .th-inner, .fixed-table-container thead th .th-inner {
        padding: 8px;
        line-height: 20px;
        vertical-align: top;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: normal;
    }
</style>
</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i><a style="font-size: 14px;" onclick="window.location.href='/cms/project/project'">项目管理</a>(${projectName})<span class="c-gray en">&gt;</span>项目任务管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl toolbar">
        <form id="form_query" style="margin: 5px 0px" class="form-inline">
          <div class="btn-group">

              <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>

              <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>

               <a  onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var now_projectId='${projectId!}';
    var queryStr="?projectId="+now_projectId+"&";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'projectId',align:"center", sortable:true, order:"asc", visible:false, title: '项目ID'},
            {field: 'moduleId', align:"center", sortable:true, order:"asc", visible:false, title: '模块ID'},
            {field: 'name', align:"left", sortable:true, order:"asc", visible:true, title: '任务标题'},
            {field: 'content', align:"center", sortable:true, order:"asc", visible:false, title: '任务内容'},
            {field: 'missiontype', align:"center", width:'80px', sortable:true, order:"asc", visible:true, title: '类型',
                formatter: function(value, row, index){
                    if(value=="1"){
                        value='设计';
                    }else if(value=="2"){
                        value='开发';
                    }else if(value=="3"){
                        value='测试';
                    }else if(value=="4"){
                        value='界面';
                    }else{}
                    return value;
                }},
            {field: 'priority', align:"center", width:'80px', sortable:true, order:"asc", visible:true, title: '优先级',
            formatter: function(value, row, index){
                if(value=="1"){
                    value='<span class="badge badge-default">1</span>';
                }else if(value=="2"){
                    value='<span class="badge badge-primary">2</span>';
                }else if(value=="3"){
                    value='<span class="badge badge-success">3</span>';
                }else if(value=="4"){
                    value='<span class="badge badge-info">4</span>';
                }else if(value=="5"){
                    value='<span class="badge badge-warning">5</span>';
                }else{}
                   return value;
            }},
            {field: 'starttime', align:"center", sortable:true, width:'100px', order:"asc", visible:false, title: '开始时间'},
            {field: 'endtime', align:"center", width:'100px', sortable:true, order:"asc", visible:false, title: '结束时间'},
            {field: 'copyto', align:"center", sortable:true, order:"asc", visible:false, title: '抄送给'},
            {field: 'status', align:"center", sortable:true, width:'100px', order:"asc", visible:true, title: '任务状态',
                formatter: function(value, row, index){
                    if(value=="0"){
                        value='<span class="label label-default">待抢</span>';
                    }else if(value=="1"){
                        value='<span class="label label-primary">开发中</span>';
                    }else if(value=="2"){
                        value='<span class="label label-info">已经提交</span>';
                    }else if(value=="3"){
                        value='<span class="label label-success">已完成审核通过</span>';
                    }else if(value=="4"){
                        value='<span class="label label-danger">审核不通过</span>';
                    }else{}
                    return value;
                }},
            {field: 'attachfiles', align:"center", sortable:true, order:"asc", visible:false, title: '附件ids'},
            {field: 'assignTo', align:"center", sortable:true, width:'100px', order:"asc", visible:false, title: '指派给'},
            {field: 'assignToNames', align:"center", sortable:true, width:'100px', order:"asc", visible:true, title: '指派给'},
            {field: 'finishBy', align:"center", sortable:true, order:"asc", visible:false, title: '完成者'},
            {field: 'estimateWorkday', align:"center", width:"100px", sortable:true, order:"asc", visible:true, title: '预估(天)'},
            {field: 'operate', align:"center", title: '操作',
                formatter: function(value, row, index){
                    var status=row.status;
                    var btns="<div class=\"btn-group\">";
                    //这里通过后台传递过来信息判断该显示何种权限按钮
                    if(assertTrue('${projectMissionEdit}')){
                        btns+="<button class=\"btn btn-xs btn-outline btn-info \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-plus\"></i> 编辑</button>";
                    }
                    if(status==0||status=='0'){//任务是待抢状态
                        if(assertTrue('${projectMissionAssignTo}')){
                            btns+="<button class=\"btn btn-xs btn-outline btn-primary \" onclick=\"assignTo('"+row.id+"')\" type=\"button\"><i class=\"fa fa-hand-pointer-o\"></i>指派</button>";
                        }else{
                            btns+="<button class=\"btn btn-xs btn-outline btn-default \"  type=\"button\"><i class=\"fa fa-hand-pointer-o\"></i>指派</button>";
                        }
                    }else{
                            btns+="<button class=\"btn btn-xs btn-outline btn-default \"  type=\"button\"><i class=\"fa fa-hand-pointer-o\"></i>指派</button>";
                    }
                    if(assertTrue('${projectMissionAskForMission}'))  {
                       if(status==0||status=='0'){
                            btns+="<button class=\"btn btn-xs btn-outline  btn-warning \" onclick=\"askForMission('"+row.id+"')\" type=\"button\"><i class=\"fa fa-warning\"></i><span class=\"bold\">抢任务</span></button>";
                        }else{
                            btns+="<button class=\"btn btn-xs btn-outline  btn-default \"   type=\"button\"><i class=\"fa fa-warning\"></i><span class=\"bold\">抢任务</span></button>";
                        }
                    }

                    if(assertTrue('${projectMissionSubmitMission}')) {
                        if (status == 1 || status == '1') {
                            btns += "<button class=\"btn btn-xs btn-outline  btn-success \" onclick=\"finishMission('" + row.id + "')\" type=\"button\"><i class=\"fa fa-cart-plus\"></i><span class=\"bold\">提交任务</span></button>";
                        } else {
                            btns += "<button class=\"btn btn-xs btn-outline  btn-default \"  type=\"button\"><i class=\"fa fa-cart-plus\"></i><span class=\"bold\">提交任务</span></button>";
                        }
                    }
                    btns+="</div>";
                    return btns;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/cms/project/mission/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    function assertTrue(per){
        if(per==true||per=='true'){
            return true;
        }
        return false;
    }
    //抢任务
    var askForMission=function(id){
        if(isEmpty(id)){
            return;
        }
        layer.confirm("确定要拿下该任务？", function(){
            $.ajax({
                type:"post",
                url:'/cms/project/mission/json/askForMission/'+id,
                data:null,
                success:function(data,textStatus){
                    ajaxReturnMsg(data);
                    refreshData();
                }
            });
        });
    }
    //完成任务
    var finishMission=function(id){

    }
    var editRow=function(id){
        $.layer.open_page("编辑项目任务","/cms/project/mission/edit?id="+id+"&projectId="+now_projectId,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增项目任务","/cms/project/mission/add?projectId="+now_projectId,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/project/mission/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        refreshData();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?projectId="+now_projectId+"&";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
