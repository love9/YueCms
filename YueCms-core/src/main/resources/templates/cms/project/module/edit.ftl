<!DOCTYPE html>
<html>
<head>
  <title>编辑项目模块</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body class="body-bg-edit">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
    <input type="hidden" value="${module.id}" id="id" name="id"/>
		<h2 class="text-center">编辑项目模块</h2>

       <#-- <div class="form-group">
              <label class="col-sm-2 control-label">parentid：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="parentid" name="parentid" placeholder="" value="${module.parentid}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">parentids：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="parentids" name="parentids" placeholder="" value="${module.parentids}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">项目ID：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="projectId" name="projectId" placeholder="项目ID" value="${module.projectId}" class="form-control" datatype="*" nullmsg="请输入项目ID" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>-->
        <div class="form-group">
              <label class="col-sm-2 control-label">模块名称：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="name" name="name" placeholder="模块名称" value="${module.name!}" class="form-control" datatype="*" nullmsg="请输入模块名称" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">显示文本：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="text" name="text" placeholder="显示文本" value="${module.text!}" class="form-control" datatype="*" nullmsg="请输入显示文本" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group" style="display: none;">
              <label class="col-sm-2 control-label">项目描述：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="description" name="description" placeholder="项目描述" value="${module.description!}" class="form-control"   />

               </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">简称：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="shortname" name="shortname" placeholder="简称" value="${module.shortname!}" class="form-control"  />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>


    </form></div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/cms/project/module/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		  btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
           });


	})
</script>

</body>
</html>
