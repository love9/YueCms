<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>项目模块管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <link href="/resources/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css"/>
    <script src="/resources/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>

</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i><a style="font-size: 14px;" onclick="window.location.href='/cms/project/project'">项目管理</a>(${projectName})<span class="c-gray en">&gt;</span>项目模块管理<span class="c-gray en">&gt;</span><span id="breadPath">${projectName}</span></nav>

<div class="wrapper animated fadeInRight">
    <div style="position:relative;width:100%;height:auto;overflow: hidden">

    <div class="clearfix" style="width:225px;border-right:1px solid #ddd;height:auto;float:left;overflow-y: auto;" id="west">

<script>

</script>
        <div style="height:5px;margin:5px 0 0 0;width:220px;height:30px;">
          <a href="#" class="easyui-linkbutton" onClick="refreshTree()" plain="true">刷新</a>
       <#--<a href="#" class="easyui-linkbutton" onClick="expandAll();" plain="true">展开</a>
          <a href="#" class="easyui-linkbutton" onClick="collapseAll();" plain="true">折叠</a>-->
        </div>
        <div  style="vertical-align: top;margin:0 0 0 0;width:220px;">
          <div>
            <ul class="ztree" id="trees">

            </ul>
          </div>
        </div>
    </div>
    <div id="mainPanle"width="100%;float:left;"  style="overflow-y: hidden;">
    <!--工具条-->
    <div class="cl toolbar">
        <form id="form_query" style="margin: 5px 0px" class="form-inline">
            <div class="btn-group">

                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>

                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>

               <input type="hidden" id="parentid" name="parentid" value="">
                <a  onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var now_projectId='${projectId}';
    var projectName='${projectName}';
    var tmp_parentid=0;
    var list_parentid='${parentid}';
    var tmp_parentname;

    var zTreeObj;
    var treeSetting = {
        data: {
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: 'parentid'
            }
        },
        async:{
            enable:true,
            dataType: "text",
            url: "/cms/project/module/json/ztree?parentid="+tmp_parentid+"&projectId="+now_projectId
            //autoParam: ["id=parentid"]
        },
        view: {
            showLine: true,
            showIcon: true
        },
        callback: {
            beforeAsync: zTreeBeforeAsync,
            onAsyncSuccess: zTreeAjaxSuccess,
            //onAsyncError: zTreeAjaxError,
            onClick: zTreeOnClick
        }
    };
    function initZtree(){
        $.fn.zTree.init($("#trees"), treeSetting, null);
        zTreeObj = $.fn.zTree.getZTreeObj("trees");
    }
    //加载完成的回调
    function zTreeAjaxSuccess(event, treeId, treeNode, msg){
        var nodes=zTreeObj.getNodes();
        zTreeObj.expandNode(nodes[0], true, false, false);
        tmp_parentid=nodes[0].id;
        list_parentid=treeNode.id ;
        tmp_parentname=nodes[0].name;
        sys_table_list();
    }
    function zTreeBeforeAsync(treeId, treeNode){
        if (treeNode) {
            tmp_parentid=	treeNode.id ;
            zTreeObj.setting.async.url="/cms/project/module/json/ztree?parentid="+tmp_parentid+"&projectId="+now_projectId;
        }
    }
    function zTreeOnClick(event, treeId, treeNode,clickFlag) {
        getParentsNodesPaths();
        tmp_parentid=treeNode.id ;
        tmp_parentname=treeNode.name;
        $("#parentid").val(tmp_parentid);
        query('form_query');
    }
    function refreshTree(){
        tmp_parentid=0;
        zTreeObj.setting.async.url="/cms/project/module/json/ztree?parentid="+tmp_parentid+"&projectId="+now_projectId;
        zTreeObj.reAsyncChildNodes(null, "refresh");
        query('form_query');
    }
    var queryStr="?projectId="+now_projectId+"&";
    //查询功能的标签说明
    var table_list_query_form = {
        parentid:tmp_parentid
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    /**
     * 刷新当前节点
     */
    function refreshNode() {
        /*根据 treeId 获取 zTree 对象*/
        var zTree = $.fn.zTree.getZTreeObj("trees"),
                type = "refresh",
                silent = false,
        /*获取 zTree 当前被选中的节点数据集合*/
                nodes = zTree.getSelectedNodes();
        /*强行异步加载父节点的子节点。[setting.async.enable = true 时有效]*/
       if(nodes!=undefined&&nodes!=null&&nodes.length>0){
           zTree.reAsyncChildNodes(nodes[0], type, silent);
           zTreeOnClick(null, zTree.setting.treeId, nodes[0]);
       }else{
           refreshTree();
       }


    }
    function  getParentsNodesPaths(){
        var paths="";
        var zTree = $.fn.zTree.getZTreeObj("trees"),
                type = "refresh",
                silent = false,
                nodes = zTree.getSelectedNodes();

        /*根据 zTree 的唯一标识 tId 快速获取节点 JSON 数据对象*/
       // var parentNode = zTree.getNodeByTId(nodes[0].parentTId);
        parentNode=nodes[0];
        //alert(JSON.stringify(parentNode.name));
        while(parentNode){
           // alert(JSON.stringify(parentNode));
            paths=parentNode.name+">"+paths;
            parentNode=parentNode.getParentNode();
        }
        $("#breadPath").html(paths);
        //alert(paths);
    }
    function refreshParentNode() {
        var zTree = $.fn.zTree.getZTreeObj("trees"),
                type = "refresh",
                silent = false,
                nodes = zTree.getSelectedNodes();
        /*根据 zTree 的唯一标识 tId 快速获取节点 JSON 数据对象*/
        var parentNode = zTree.getNodeByTId(nodes[0].parentTId);
        /*选中指定节点*/
        zTree.selectNode(parentNode);
        zTree.reAsyncChildNodes(parentNode, type, silent);
    }
    $(function(){
        initZtree();
        $("#parentid").val(list_parentid);
        query('form_query');
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'parentid', align:"center", sortable:true, order:"asc", visible:false, title: ''},
            {field: 'parentids', align:"center", sortable:true, order:"asc", visible:false, title: ''},
            {field: 'projectId', align:"center", sortable:true, order:"asc", visible:false, title: '项目ID'},
            {field: 'name', align:"center", sortable:true, order:"asc", visible:true, title: '模块名称'},
            {field: 'text', align:"center", sortable:true, order:"asc", visible:true, title: '显示文本'},
            {field: 'description', align:"center", sortable:true, order:"asc", visible:false, title: '项目描述'},
            {field: 'shortname', align:"center", sortable:true, order:"asc", visible:true, title: '简称'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.url='/cms/project/module/json/find'+queryStr;
        //alert(table_list_Params.url);
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
        $.layer.open_page("编辑项目模块","/cms/project/module/edit?id="+row.id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增项目模块","/cms/project/module/add?parentid="+tmp_parentid+"&parentname="+tmp_parentname+"&projectId="+now_projectId,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/project/module/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        refreshNode();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?projectId="+now_projectId+"&";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        //alert(queryStr);
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
