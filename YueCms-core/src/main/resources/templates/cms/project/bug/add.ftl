<!DOCTYPE html>
<html>
<head>
  <title>新增项目Bug</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
	<meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

	<link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet" />


    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <link rel="stylesheet" href="/resources/lib/kindeditor-4.1.10/themes/default/default.css" />
    <script type="text/javascript" src="/resources/lib/kindeditor-4.1.10/kindeditor-all.js"></script>
    <script type="text/javascript" src="/resources/lib/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script type="text/javascript">
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#editor_content', {

                resizeType : 1,
                allowPreviewEmoticons : false,
                allowImageUpload : false,
                items : [
                    'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                    'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
                    'insertunorderedlist', '|', 'emoticons', 'image', 'link']

            });
        });
        //给editor设置内容
        function setValue(v){
            editor.html(v);
        }
        //取得html内容
        function getValue(){
            editor.sync();
            var v=editor.html();
            return v ;
        }
    </script>
     <#import "/base/util/macro.ftl" as macro>
</head>
<body class="body-bg-add">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
        <h2 class="text-center">新增项目Bug</h2>
        <div class="form-group" style="display: none">
              <label class="col-sm-2 control-label">项目：</label>
               <div class="col-sm-6 formControls">
                    <input type="hidden" id="projectId" name="projectId" placeholder="项目ID" value="${projectId}" class="form-control" datatype="*" nullmsg="请输入项目ID" />
                   <input type="text" id="projectName" name="projectName" readonly placeholder="项目" value="${projectName}" class="form-control" datatype="*" nullmsg="请输入项目ID" />

               </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">模块：</label>
               <div class="col-sm-6 formControls">
                   <input type="hidden" id="moduleId" name="moduleId" placeholder="模块ID" value="" class="form-control" datatype="*" nullmsg="请输入模块ID" />
                   <#--<my:mySelect path="moduleSelect" idAttr="moduleId"   url="/project/module/json/select?projectId=${projectId}" ></my:mySelect>-->
                    <@macro.mySelect id="moduleSelect"  idAttr="moduleId" changeCallBack="" url="/cms/project/module/json/select?projectId=${projectId}"></@macro.mySelect>
               </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group"  >
            <label class="col-sm-2 control-label">指派给：</label>
            <div class="col-sm-4 formControls">
                <input type="hidden" id="assignTo" readonly  name="assignTo" placeholder="指派给" value="" class="form-control"  />
                <input type="text" id="assignToNames" disabled  name="assignToNames" placeholder="指派给" value="" class="form-control"  />
            </div>
            <div class="col-sm-2 formControls">
                <button type="button" onclick="base_openYhxzPage('assignTo','assignToNames','limit=5')" style="display: inline-block;" class="btn btn-block btn-outline btn-primary">选择用户</button>
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">Bug标题：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="title" name="title" placeholder="Bug标题" value="" class="form-control" datatype="*" nullmsg="请输入Bug标题" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">Bug描述：</label>
               <div class="col-sm-6 formControls">
                    <textarea id="editor_content" name="editor_content"  style="width:100%;height:200px;"></textarea>
               </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

        <div class="form-group">
              <label class="col-sm-2 control-label">完成优先级：</label>
               <div class="col-sm-6 formControls">
                   <input type="hidden" id="priority" name="priority" placeholder="完成优先级" value="" class="form-control" datatype="*" nullmsg="请输入完成优先级" />


                    <@macro.dicSelect id="priority"  value="" idAttr="priority" changeCallBack=""></@macro.dicSelect>
               </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group" style="display: none;">
              <label class="col-sm-2 control-label">任务抄送给：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="copyto" name="copyto" placeholder="任务抄送给" value="" class="form-control"   />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group" style="display: none;">
              <label class="col-sm-2 control-label">Bug状态：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="status" name="status" placeholder="Bug状态" value="" class="form-control"  />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group" style="display: none;">
              <label class="col-sm-2 control-label">附件：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="attachfiles" name="attachfiles" placeholder="附件ids" value="" class="form-control"  />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

        <div class="form-group" style="display: none;">
              <label class="col-sm-2 control-label">完成者：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="finishBy" name="finishBy" placeholder="完成者" value="" class="form-control"  />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
    </form></div>
</div>

<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
        var value=getValue();
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/cms/project/bug/json/save?'+params,
			data:{content:value},
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
