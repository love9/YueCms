<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>项目Bug管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>



</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i><a style="font-size: 14px;" onclick="window.location.href='/cms/project/project'">项目管理</a><span class="c-gray en">&gt;</span>项目Bug管理<span class="c-gray en">&gt;</span><span id="">${projectName}</span> </nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl toolbar">
        <input type="hidden" id="projectId" name="projectId" value="${projectId!}">
        <form id="form_query" style="margin: 5px 0px" class="form-inline">
            <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a  onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
           </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var now_projectId='${projectId!}';
    var queryStr="?projectId="+$("#projectId").val()+"&";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'projectId', align:"center", sortable:true, order:"asc", visible:false, title: '项目ID'},
            {field: 'moduleId', align:"center", sortable:true, order:"asc", visible:false, title: '模块ID'},
            {field: 'title', align:"center", sortable:true, order:"asc", visible:true, title: 'Bug标题'},
            {field: 'content', align:"center", sortable:true, order:"asc", visible:false, title: 'Bug描述'},
            {field: 'bugtype', align:"center", sortable:true, order:"asc", visible:false, title: 'Bug类型'},
            {field: 'priority', align:"center", sortable:true, order:"asc", visible:true, title: '优先级',
                formatter: function(value, row, index){
                    if(value=="1"){
                        value='<span class="badge badge-default">1</span>';
                    }else if(value=="2"){
                        value='<span class="badge badge-primary">2</span>';
                    }else if(value=="3"){
                        value='<span class="badge badge-success">3</span>';
                    }else if(value=="4"){
                        value='<span class="badge badge-info">4</span>';
                    }else if(value=="5"){
                        value='<span class="badge badge-warning">5</span>';
                    }else{}
                    return value;
                }
            },
            {field: 'copyto', align:"center", sortable:true, order:"asc", visible:false, title: '任务抄送给'},
            {field: 'status', align:"center", sortable:true, order:"asc", visible:true, title: 'Bug状态'},
            {field: 'attachfiles', align:"center", sortable:true, order:"asc", visible:false, title: '附件'},
            {field: 'assignTo', align:"center", sortable:true, order:"asc", visible:false, title: '指派给'},
            {field: 'assignToNames', align:"center", sortable:true, order:"asc", visible:true, title: '指派给'},
            {field: 'finishBy', align:"center", sortable:true, order:"asc", visible:false, title: '完成者'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=editRow;
        table_list_Params.url='/cms/project/bug/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var editRow=function(row,tr){
        $.layer.open_page("编辑项目Bug","/cms/project/bug/edit?id="+row.id+"&projectId="+now_projectId,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增项目Bug","/cms/project/bug/add?projectId="+now_projectId,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/cms/project/bug/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        refreshData();
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?projectId="+$("#projectId").val()+"&";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
