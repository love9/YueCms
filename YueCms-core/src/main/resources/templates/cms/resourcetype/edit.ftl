<!DOCTYPE html>
<html>
<head>
  <title>编辑资源分类</title>
  <link href="/resources/css/admui.css" rel="stylesheet">
  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>
<body class="body-bg-edit">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
    <input type="hidden" id="id" name="id" value="${resourcetype.id}">
      <h2 class="text-center">编辑栏目</h2>
      <div class="form-group">
        <label class="control-label col-sm-2"><span style='color:red;'>*</span>上级分类:</label>
        <div class="formControls col-sm-6">
				<input type="hidden" class="form-control" name="parentid" datatype="*" value="${resourcetype.parentid}" />
				<input type="text" class="form-control disabled" readonly name="parentname"  value="${resourcetype.parentname}" />
        </div>
        <div class="col-sm-4">
          <div class="Validform_checktip"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2"><span style='color:red;'>*</span>分类名称:</label>
        <div class="formControls col-sm-6">
          <input type="text" class="form-control" name="name" datatype="*" value="${resourcetype.name}" />
        </div>
        <div class="col-sm-4">
          <div class="Validform_checktip"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2">分类代码:</label>
        <div class="formControls col-sm-6">
          <input type="text" class="form-control" name="code"  value="${resourcetype.code}" />
        </div>
        <div class="col-sm-4">
          <div class="Validform_checktip"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2">分类描述:</label>
        <div class="formControls col-sm-6">
          <textarea rows="3" style="height: 70px;" class="form-control" value="" placeholder="" id="description" name="description">${resourcetype.description}</textarea>
        </div>
        <div class="col-sm-4">
          <div class="Validform_checktip"></div>
        </div>
      </div>
      <#--<div class="form-group">
        <div class="formControls col-sm-3 col-sm-offset-2">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form></div>
</div>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>


<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript">
  var validform;
  function save(){
    var b=validform.check(false);
    if(!b){
      return;
    }
    var params=$("#form_show").serialize();
    $.ajax({
      type:"post",
      url:'/cms/resourcetype/json/save?'+params,
      data:null,
      success:function(json,textStatus){
        ajaxReturnMsg(json);
        setTimeout(function(){
          var index = parent.layer.getFrameIndex(window.name);
          parent.layer.close(index);
        },1000);
      }
    });
  }
  $(function(){
    validform=$("#form_show").Validform({
      tiptype:2,
      postonce:true,//至提交一次
      ajaxPost:false,//ajax方式提交
      showAllError:true //默认 即逐条验证,true验证全部
    });
  })
</script>
</body>
</html>
