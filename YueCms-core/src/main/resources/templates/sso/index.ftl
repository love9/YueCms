<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
</head>
<body>
<h1>欢迎登录!${ssoUser.username},这里是登录后首页!您可以指定redirect_url参数作为登录后首页，比如登录页面url如下：/sso/login?redirect_url=/admin</h1>
<a href="/sso/logout">
    <span class="hidden-xs">注销</span>
</a>
</body>
</html>