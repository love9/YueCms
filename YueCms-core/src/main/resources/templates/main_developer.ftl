<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>开发人员登录首页</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/web-icons/web-icons.css" rel="stylesheet">
    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/css/admui/team.css" rel="stylesheet">
    <link href="/resources/css/skin/default.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="http://cdn.admui.com/demo/pjax/1.2.0/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <style>
    </style>
</head>
<body class="site-page body-bg-main" style="">
    <div class="page-content padding-30 container-fluid">
        <div class="row"  >
            <div class="col-lg-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-warning">
                            <i class="icon wb-shopping-cart"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">订单</span>
                        <div class="content-text text-center margin-bottom-0">
                            <i class="text-danger icon wb-triangle-up font-size-20"> </i>
                            <span class="font-size-40 font-weight-100">399</span>
                            <p class="blue-grey-400 font-weight-100 margin-0">+45% 同比增长</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-danger">
                            <i class="fa fa-yen"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">收入</span>
                        <div class="content-text text-center margin-bottom-0">
                            <i class="text-success icon wb-triangle-down font-size-20"> </i>
                            <span class="font-size-40 font-weight-100">¥18,628</span>
                            <p class="blue-grey-400 font-weight-100 margin-0">+45% 同比增长</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-success">
                            <i class="icon wb-eye"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">访客</span>
                        <div class="content-text text-center margin-bottom-0">
                            <i class="text-danger icon wb-triangle-up font-size-20"> </i>
                            <span class="font-size-40 font-weight-100">23,456</span>
                            <p class="blue-grey-400 font-weight-100 margin-0">+25% 同比增长</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-primary">
                            <i class="icon wb-user"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">买家</span>
                        <div class="content-text text-center margin-bottom-0">
                            <i class="text-danger icon wb-triangle-up font-size-20"> </i>
                            <span class="font-size-40 font-weight-100">4,367</span>
                            <p class="blue-grey-400 font-weight-100 margin-0">+25% 同比增长</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xlg-6  col-lg-6 col-md-12"  >
                <div id="recentActivityWidget" class="widget widget-shadow padding-bottom-20">
                    <div class="widget-header">
                        <span class="label label-default label-round pull-right">查看所有</span>
                        <h5 class="widget-title">
                            近期动态 </h5>
                    </div>
                    <ul class="timeline timeline-icon">
                        <li class="timeline-reverse">
                            <div class="timeline-content-wrap">
                                <div class="timeline-dot bg-green-600">
                                    <i class="icon wb-chat" aria-hidden="true"></i>
                                </div>
                                <div class="timeline-content">
                                    <div class="title">
                                        <span class="authors">南学斌</span> 指派了新任务
                                    </div>
                                    <div class="metas">
                                        14 分钟前
                                    </div>
                                    <ul class="members">
                                        <li>
                                            <img class="avatar avatar-sm" src="/resources/images/portraits/7.jpg">
                                        </li>
                                        <li>
                                            <img class="avatar avatar-sm" src="/resources/images/portraits/6.jpg">
                                        </li>
                                        <li>
                                            <img class="avatar avatar-sm" src="/resources/images/portraits/8.jpg">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-reverse">
                            <div class="timeline-content-wrap">
                                <div class="timeline-dot bg-blue-600">
                                    <i class="icon wb-image" aria-hidden="true"></i>
                                </div>
                                <div class="timeline-content">
                                    <div class="title">
                                        <span class="authors">吕佳</span> 上传了 3 张图片
                                    </div>
                                    <div class="metas">
                                        2 小时前
                                    </div>
                                    <ul class="photos">
                                        <li class="cover">
                                            <img class="cover-image" src="/resources/images/photos/animal-4.jpg">
                                        </li>
                                        <li class="cover">
                                            <img class="cover-image" src="/resources/images/photos/animal-2.jpg">
                                        </li>
                                        <li class="cover">
                                            <img class="cover-image" src="/resources/images/photos/animal-3.jpg">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-reverse">
                            <div class="timeline-content-wrap">
                                <div class="timeline-dot bg-cyan-600">
                                    <i class="icon wb-file" aria-hidden="true"></i>
                                </div>
                                <div class="timeline-content">
                                    <div class="title">
                                        <span class="authors">赵烁利</span> 上传了工作日报
                                    </div>
                                    <div class="metas">
                                        4 小时前
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-reverse">
                            <div class="timeline-content-wrap">
                                <div class="timeline-dot bg-orange-600">
                                    <i class="icon wb-map" aria-hidden="true"></i>
                                </div>
                                <div class="timeline-content">
                                    <div class="title">
                                        <span class="authors">付于倩</span> 提交了武汉出差申请
                                    </div>
                                    <div class="metas">
                                        3 小时前
                                    </div>
                                    <ul class="operates">
                                        <li>
                                            <button class="btn btn-outline btn-success btn-round">同意</button>
                                        </li>
                                        <li>
                                            <button class="btn btn-outline btn-danger btn-round">拒绝</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
          <!--<div class="col-xlg-4 col-lg-6 col-md-6" style="height: 696px;">
                <div id="personalCompletedWidget" class="widget widget-shadow padding-bottom-20">
                    <div class="widget-header cover overlay">
                        <img class="cover-image" src="/resources/public/images/placeholder.png">
                        <div class="overlay-panel overlay-background vertical-align">
                            <div class="vertical-align-middle">
                                <a class="avatar" href="javascript:;">
                                    <img alt="" src="/resources/images/portraits/4.jpg">
                                </a>
                                <div class="font-size-20 margin-top-10">吕佳</div>
                                <div class="font-size-14">lv.jia@163.com</div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="row text-center margin-bottom-20">
                            <div class="col-xs-6">
                                <div class="counter">
                                    <div class="counter-label total-completed">总完成</div>
                                    <div class="counter-number red-600">8</div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="counter">
                                    <div class="counter-label">总用时</div>
                                    <div class="counter-number blue-600">17</div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <caption>日报</caption>
                                <tbody>
                                <tr>
                                    <td>
                                        任务一
                                    </td>
                                    <td>
                                        <div class="progress progress-xs margin-bottom-0">
                                            <div class="progress-bar progress-bar-info bg-blue-600" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
                                        </div>
                                    </td>
                                    <td>
                                        90%
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        任务二
                                    </td>
                                    <td>
                                        <div class="progress progress-xs margin-bottom-0">
                                            <div class="progress-bar progress-bar-info bg-green-600" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100" style="width: 86%"></div>
                                        </div>
                                    </td>
                                    <td>
                                        86%
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        任务三
                                    </td>
                                    <td>
                                        <div class="progress progress-xs margin-bottom-0">
                                            <div class="progress-bar progress-bar-info bg-red-600" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%"></div>
                                        </div>
                                    </td>
                                    <td>
                                        68%
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>-->

            <div class="col-xlg-6 col-lg-6 col-md-12" style="height: 696px;">
                <div id="toDoListWidget" class="widget widget-shadow padding-bottom-20">
                    <div class="widget-header">
                        <a id="addNewItemBtn" href="javascript:;" class="add-item-toggle pull-right">
                            <i class="icon wb-plus" aria-hidden="true"></i></a>
                        <h5 class="widget-title">待办事项</h5>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="checkbox-custom checkbox-success checkbox-lg">
                                <input type="checkbox" name="checkbox" checked="checked">
                                <label class="item-title">会议室开总结大会</label>
                            </div>
                            <div class="item-due-date">
                                <span>9.12</span>
                            </div>
                            <ul class="item-members">
                                <li>
                                    <img class="avatar avatar-sm" src="http://cdn.admui.com/demo/pjax/1.2.0/images/portraits/3.jpg">
                                    <button class="btn btn-sm btn-icon btn-default btn-outline btn-round">
                                        <i class="icon wb-pencil" aria-hidden="true"></i>
                                    </button>
                                </li>
                            </ul>
                        </li>
                        <li class="list-group-item">
                            <div class="checkbox-custom checkbox-success checkbox-lg">
                                <input type="checkbox" name="checkbox" checked="checked">
                                <label class="item-title">确定 X 项目的原型图</label>
                            </div>
                            <div class="item-due-date">
                                <span>9.12</span>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="checkbox-custom checkbox-success checkbox-lg">
                                <input type="checkbox" name="checkbox">
                                <label class="item-title">修复不支持IE6的问题</label>
                            </div>
                            <div class="item-due-date">
                                <span>未确定日期</span>
                            </div>
                            <ul class="item-members">
                                <li>
                                    <img class="avatar avatar-sm" src="http://cdn.admui.com/demo/pjax/1.2.0/images/portraits/1.jpg">
                                    <button class="btn btn-sm btn-icon btn-default btn-outline btn-round">
                                        <i class="icon wb-pencil" aria-hidden="true"></i>
                                    </button>
                                </li>
                                <li>
                                    <img class="avatar avatar-sm" src="http://cdn.admui.com/demo/pjax/1.2.0/images/portraits/5.jpg">
                                    <button class="btn btn-sm btn-icon btn-default btn-outline btn-round">
                                        <i class="icon wb-pencil" aria-hidden="true"></i>
                                    </button>
                                </li>
                            </ul>
                        </li>
                        <li class="list-group-item">
                            <div class="checkbox-custom checkbox-success checkbox-lg">
                                <input type="checkbox" name="checkbox">
                                <label class="item-title">修复Bug</label>
                            </div>
                            <div class="item-due-date">
                                <span>9.15</span>
                            </div>
                            <ul class="item-members">
                                <li>
                                    <button class="btn btn-sm btn-icon btn-default btn-outline btn-round">
                                        <i class="icon wb-pencil" aria-hidden="true"></i>
                                    </button>
                                </li>
                            </ul>
                        </li>
                        <li class="list-group-item">
                            <div class="checkbox-custom checkbox-success checkbox-lg">
                                <input type="checkbox" name="checkbox">
                                <label class="item-title">汇报 X 项目的进展</label>
                            </div>
                            <div class="item-due-date">
                                <span>9.15</span>
                            </div>
                            <ul class="item-members">
                                <li>
                                    <img class="avatar avatar-sm" src="http://cdn.admui.com/demo/pjax/1.2.0/images/portraits/4.jpg">
                                    <button class="btn btn-sm btn-icon btn-default btn-outline btn-round">
                                        <i class="icon wb-pencil" aria-hidden="true"></i>
                                    </button>
                                </li>
                                <li>
                                    <img class="avatar avatar-sm" src="http://cdn.admui.com/demo/pjax/1.2.0/images/portraits/6.jpg">
                                    <button class="btn btn-sm btn-icon btn-default btn-outline btn-round">
                                        <i class="icon wb-pencil" aria-hidden="true"></i>
                                    </button>
                                </li>
                                <li>
                                    <img class="avatar avatar-sm" src="http://cdn.admui.com/demo/pjax/1.2.0/images/portraits/7.jpg">
                                    <button class="btn btn-sm btn-icon btn-default btn-outline btn-round">
                                        <i class="icon wb-pencil" aria-hidden="true"></i>
                                    </button>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
</html>