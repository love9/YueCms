<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>首页</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="/favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet">

    <link rel="stylesheet" href="/resources/lib/owlCarousel/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/lib/owlCarousel/css/owl.theme.css">

    <link rel="stylesheet" href="/resources/website/index.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script src="/resources/lib/owlCarousel/owl.carousel.min.js"></script>
    <style>
        .owl-item img {
            width: 100%;
        }
    </style>

    <style>
        .page-index .row .panel {
            height: -webkit-calc(100% - 24px);
            height: calc(100% - 24px)
        }

        .page-index .account-info {
            overflow: visible;
            border-bottom: none
        }

        .page-index .account-info .media-right {
            vertical-align: middle
        }

        @media (max-width: 768px) {
            .page-index .account-info .media-right {
                display: none
            }
        }

        .page-index .introduce-info .media {
            margin: 0;
            border-bottom: none
        }

        .page-index .introduce-info .media .media-object {
            height: 120px
        }

        .page-index .introduce-info .media .media-body {
            padding-left: 20px
        }

        .page-index .introduce-info .media .media-body .btn {
            display: inline-block;
            margin: 3px 5px 3px 0
        }

        .page-index .changelog-info .time-line {
            padding-left: 0;
            list-style: none
        }

        .page-index .changelog-info .time-line:before {
            position: absolute;
            top: 25px;
            bottom: 40px;
            left: 120px;
            width: 1px;
            content: " ";
            background: #e4eaec
        }

        .page-index .changelog-info .time-line > li {
            position: relative;
            width: 100%;
            margin: 15px 0;
            overflow: hidden;
            line-height: 24px
        }

        .page-index .changelog-info .time-line > li:before {
            position: absolute;
            top: 6px;
            left: 93px;
            display: block;
            width: 14px;
            height: 14px;
            content: " ";
            background: #fff;
            border: solid 2px #e4eaec;
            border-radius: 50%
        }

        .page-index .changelog-info .time-line > li:first-child:before {
            background: #5cd29d;
            border-color: #bfedd8
        }

        .page-index .changelog-info .time-line > li:last-child:before {
            background: #fa7a7a;
            border-color: #fad3d3
        }

        .page-index .changelog-info .time-line > li time {
            float: left
        }

        .page-index .changelog-info .time-line > li h5 {
            float: left;
            margin: 0 0 0 55px;
            line-height: 24px
        }

        .page-index .part-info .panel-body .icon {
            font-size: 3.2em;
            line-height: 1.6
        }

        .page-index .part-info .panel-body .label-content {
            margin: 20px 0 10px
        }

        .page-index .part-info .panel-body .label-content .label {
            display: inline-block;
            margin: 3px 0
        }

        @media (max-width: 768px) {
            .introduce-info .media-left {
                display: none
            }

            .introduce-info .media-body {
                padding-left: 0 !important
            }
        }

        .owl-pagination {
            top: -41px;
            position: relative;
        }

        .news-container {
            margin-top: 4px;
            height: 66px;
            background: #f6f6f6;
        }

        .news-container {
            width: 1300px;
            margin: 0 auto;
        }

        .news-list-item {
            display: inline-block;
            /*float: left;*/
            font-size: 14px;
            color: #666;
            /*   width: 296px;*/
            width: 22%;
            margin-right: 18px;
            padding: 20px 0;
        }

        .news-list-item a {
            padding-left: 20px;
            color: #666 !important;
            background-image: radial-gradient(#ccc 30%, transparent 0), radial-gradient(#ccc 30%, transparent 0);
            background-repeat: no-repeat;
            background-size: 15px 15px;
            background-position: 0 3px;
        }

        .news-list-item a:hover {
            color: #fd703e !important;
            background-image: radial-gradient(#fd703e 30%, transparent 0), radial-gradient(#fd703e, transparent 0);
            text-decoration: underline;
        }

        .section .section_header {
            text-align: center;
            margin: 10px auto;
        }

        .section .section_header h3 {
            font-size: 32px;
            margin-bottom: 10px;
        }

        .section .section_body {
            margin: 0 auto;
            font-family: "Helvetica Neue", Helvetica, Tahoma, Arial, "Microsoft Yahei", "Hiragino Sans GB", "WenQuanYi Micro Hei", sans-serif;
        }

        .section .features {
            margin: 0 -25px;
        }

        .features_item .features_desc ul li {
            list-style: circle inside;
            line-height: 1.8;
            color: rgba(90, 105, 120, 0.7);
            white-space: nowrap;
        }

        .features_item .features_img {
            position: absolute;
            top: 5px;
            opacity: 0.8;
            width: 120px;
            height: 140px;
        }

        .features_item .features_desc {
            padding: 0 0 10px 160px;
        }

        .section .features_item {
            box-sizing: border-box;
            max-width: 440px;
            min-height: 140px;
            overflow: hidden;
            margin: 0 auto;
        }

        .summary_item {
            display: inline-block;
            padding: 25px;
            text-align: center;
            box-sizing: border-box;
            margin: 0 auto;
        }

        .summary_item > i {
            display: inline-block;
        }

        .home-bg_efficient {
            background-image: url(/resources/images/admui_index_bg.png);
            background-position: -645px -338px;
            width: 100px;
            height: 80px;
        }

        .home-bg_save {
            background-image: url(/resources/images/admui_index_bg.png);
            background-position: -845px -338px;
            width: 80px;
            height: 80px;
        }

        .home-bg_controlled {
            background-image: url(/resources/images/admui_index_bg.png);
            background-position: -755px -338px;
            width: 80px;
            height: 80px;
        }

        .screenshot_body {
            width: 635px;
            margin: 0 auto;
            position: relative;
            z-index: 20;
        }

        .screenshot_body .screenshot_slider {
            width: 100%;
            height: 502px;
            position: relative;
        }

        .screenshot_body .screenshot_slider .home-bg_computer {
            position: absolute;
            z-index: 0;
            height: 100%;
        }

        .home-bg_computer {
            background-image: url(/resources/images/admui_index_bg.png);
            background-position: 0 0;
            width: 635px;
            height: 502px;
        }

        .showPic .owl-pagination {
            top: -9px;
            position: relative;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
                items: 2,
                autoPlay: true,
                navigation: false,
                navigationText: ["上一个", "下一个"],
                autoHeight: true
            });
            $('.showPic').owlCarousel({
                items: 1,
                autoPlay: true,
                navigation: false,
                navigationText: ["上一个", "下一个"],
                autoHeight: true
            });
        });
    </script>
</head>
<body class="layout-full" style="">

<#include "/website/nav.ftl">
<div class="page animation-fade page-index" style="margin-top:61px;">
    <section>
        <div class="row">
            <div class="owl-carousel-centered owl-carousel owl-theme owl-center" data-ride="carousel">

                <div class="item">
                    <img src="/resources/images/app/back_1.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="/resources/images/app/back_2.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="/resources/images/app/back_3.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="/resources/images/app/back_4.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="/resources/images/app/back_5.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="/resources/images/app/front_1.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="/resources/images/app/front_2.jpg"" alt="...">
                </div>
                <div class="item">
                    <img src="/resources/images/app/front_3.jpg"" alt="...">
                </div>

            </div>
        </div>
        <div class="row" style="margin-top:-38px;position:relative;    background: #f6f6f6;">
            <div class="news-container">
                <ul class="news-list">
                    <li class="news-list-item"><a href="#" target="_blank1"> [ 大事 ] 数据报告</a></li>
                    <li class="news-list-item"><a href="#" target="_blank1"> [ 活动 ] 大赛获奖名单</a></li>
                    <li class="news-list-item"><a href="#" target="_blank1"> [ 流程 ] 如何成为开发者</a></li>
                    <li class="news-list-item"><a href="#" target="_blank1"> [ 福利 ] 送ofo月卡、移动电源</a></li>
                    <div style="clear: both;"></div>
                </ul>
            </div>
        </div>
    </section>

    <div class="page-content" style="margin:0 auto;padding:0 !important;width:80%">
        <div class="row" style="display: none;">
            <div class="col-sm-12">
                <div class="panel" style="height: initial;">
                    <div class="panel-heading">
                        <ul class="panel-actions">
                            <li>
                                <a href="/reg/developer" target="_blank">立刻参与</a>
                            </li>
                        </ul>
                        <h3 class="panel-title">申请参与开发</h3>
                    </div>
                    <div class="panel-body">
                        <div class="steps row steps-xs">
                            <div class="step col-md-3 current">
                                <span class="step-number">1</span>

                                <div class="step-desc">
                                    <span class="step-title">参与申请</span>

                                    <p>简单填写一些您的资历信息。 &nbsp;&nbsp;&nbsp;
                                        <a href="/reg/developer" style="line-height: 1.0;"
                                           class="btn btn-warning btn-sm  btn-round" target="_blank">
                                            去申请
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="step col-md-3 ">
                                <span class="step-number">2</span>

                                <div class="step-desc">
                                    <span class="step-title">审核</span>

                                    <p>通知管理员审核。
                                        <a href="#" style="line-height: 1.0;" class="btn btn-warning btn-sm btn-round"
                                           target="_blank">
                                            去通知
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="step col-md-3 ">
                                <span class="step-number">3</span>

                                <div class="step-desc">
                                    <span class="step-title">审核通过</span>

                                    <p>感谢您的参与!

                                    </p>
                                </div>
                            </div>
                            <div class="step col-md-3 ">
                                <span class="step-number">4</span>

                                <div class="step-desc">
                                    <span class="step-title">参与开发</span>

                                    <p>下载项目、群组讨论、定下需求、立即开发</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="section" style="max-width: 1100px;margin: 0 auto;">
                <div class="section_header">
                    <h3><strong>让</strong>&nbsp;优秀，发自内心</h3>

                    <p>覆盖全端场景，紧贴业务需求，助力企业发展</p>
                </div>
                <div class="section_body">
                    <div class="row summary clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="features_item">
                                <div class="features_img">
                                    <img src="http://cdn.admui.com/site/v2/img/home/sources.svg" width="126" alt="">
                                </div>
                                <div class="features_desc">
                                    <h4>源码可控</h4>
                                    <ul>
                                        <li>始终把安全放在第一位</li>
                                        <li>无加密、混淆的代码，无后门</li>
                                        <li>安全全程可控，可放心使用</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="features_item">
                                <div class="features_img"><img src="http://cdn.admui.com/site/v2/img/home/response.svg"
                                                               width="120" alt="">
                                </div>
                                <div class="features_desc">
                                    <h4>多端支持</h4>
                                    <ul>
                                        <li>支持IE10+，支持现代浏览器</li>
                                        <li>支持智能手机、微信浏览器</li>
                                        <li>HTML5+CSS3响应式设计</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="features_item">
                                <div class="features_img"><img src="http://cdn.admui.com/site/v2/img/home/function.svg"
                                                               width="120" alt=""></div>
                                <div class="features_desc"><h4>功能完善</h4>
                                    <ul>
                                        <li>包含管理系统常用的基础功能</li>
                                        <li>涵盖丰富的基础组件，50+综合演示</li>
                                        <li>易于扩展</li>
                                    </ul>
                                    <p><a href="#" target="_blank"><i class="icon home-icon_demo"></i>
                                        在线演示</a>
                                    </p>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="features_item">
                                <div class="features_img">
                                    <img src="http://cdn.admui.com/site/v2/img/home/easily.svg" width="120" alt="">
                                </div>
                                <div class="features_desc">
                                    <h4>上手容易</h4>
                                    <ul>
                                        <li>在线开发文档</li>
                                        <li>大量的示例演示，组件式开发</li>
                                        <li>5x8小时服务</li>
                                    </ul>
                                    <p>
                                        <a href="#" target="_blank"><i class="icon home-icon_docs"></i>开发文档</a>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="section" style="max-width: 1100px;margin: 40px auto;">
                <div class="section_header"><h3><strong>让</strong>&nbsp;品质，源于执着</h3>

                    <p>她不同于您以往见过的任何产品，如果有一个词能形容她，那就是简洁，高效和极致</p></div>
                <div class="section_body">
                    <div class="row summary clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="summary_item">
                                <i class="home-bg_efficient"></i>

                                <div class="summary_desc">
                                    <h4>高效</h4>

                                    <p>提供的完整方案，让原有产品开发时间缩短200%以上，工作效率大大提升</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="summary_item">
                                <i class="home-bg_save"></i>

                                <div class="summary_desc"><h4>低成本</h4>

                                    <p>帮您减轻或省去了产品、设计、开发、测试等人员的工作，极大降低成本</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="summary_item">
                                <i class="home-bg_controlled"></i>

                                <div class="summary_desc"><h4>可控</h4>

                                    <p>从根本上降低项目工期、成本、安全等不确定因素，让风险更可控</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="section" style="max-width: 1100px;margin: 40px auto;">
                <div class="section_header">
                    <h3>坚持，卓尔不群</h3>

                    <p>借助于它，可以快速开发各种MIS系统，如CMS、OA、CRM、ERP、POS等</p>
                </div>
                <div class="section_body">
                    <div class="screenshot clearfix">
                        <div class="screenshot_body">
                            <div class="screenshot_slider">
                                <div class="home-bg_computer"></div>
                                <div class="owl-carousel-centered showPic owl-theme owl-center" style="padding:20px;"
                                     data-ride="carousel">

                                    <div class="item">
                                        <img src="/resources/website/images/show/myblog_login.jpg" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/website/images/show/index-blog.png" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/website/images/show/bloglist.jpg" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/website/images/show/article1.jpg" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/website/images/show/article2.jpg" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/website/images/show/article3.jpg" alt="...">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>


<#include "/website/footer.ftl">

</body>
</html>
