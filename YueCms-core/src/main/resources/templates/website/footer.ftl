<section>
<footer class="footer container-fluid foot-wrap">
    <!--采用container，使得页尾内容居中 -->
    <div class="container">
        <div class="row">
            <div class="row-content col-lg-3 col-sm-6 col-xs-6">
                <h3>关于我们</h3>
                <ul>
                    <li><a href="/about/agreement" class="">用户协议</a></li>
                    <li><a href="/about/duty" class="">免责声明</a></li>
                    <li><a href="/about/privacy" class="">隐私保护</a></li>
                    <li><a href="/about/intellectual" class="">知识产权声明</a></li>
                    <li><a href="/about/license" class="">软件授权协议</a></li>
                </ul>
            </div>

            <div class="row-content col-lg-3 col-sm-6 col-xs-6">
                <h3>开发文档</h3>
                <ul>
                    <li><a href="http://docs.admui.com/iframe/guide/" target="_blank">基本介绍</a></li>
                    <li><a href="http://docs.admui.com/iframe/ui/" target="_blank">前端开发</a></li>
                    <li><a href="http://docs.admui.com/iframe/java/" target="_blank">Java开发</a></li>
                    <li><a href="http://docs.admui.com/iframe/logs/" target="_blank">更新日志</a></li>
                </ul>
            </div>
            <div class="row-content col-lg-3 col-sm-6 col-xs-6">
                <h3>帮助中心</h3>
                <ul>
                    <li><a href="/help/support" class="">IT支持</a></li>

                </ul>
            </div>
            <div class="row-content col-lg-3 col-sm-6 col-xs-6">
                <h3>联系我们</h3>
                <ul>
                    <li><a href="mailto:747506908@qq.com">747506908@qq.com</a></li>
                    <li>&nbsp;</li>
                    <li><img src="/resources/images/front_wx.png" style="width: 100px;height: 100px;"/></li>
                </ul>
            </div>

        </div>
    </div>
    <p align="center" style="margin-top: 20px;color:#878B91;">
        Copyright &copy;2018 YueCms
    </p>
</footer>
</section>
