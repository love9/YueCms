<div style="padding:0px 0; border-bottom: 1px dashed #ccc">
    <h1>封装的常用js方法</h1>
</div>
<h1>消息提示</h1>
<p>额外引入自己封装的<code style="background-color: transparent;">/js/common/jBoxNitice.js</code>&nbsp;&nbsp;&nbsp;<a class="btn red_btn" target="_blank" href="/myblog/demo/201807/jBox-notice">Demo</a></p>

<p>1.方法jBox_simpleMsg()简单的消息提示</p>
<pre>
    <code style="background-color: transparent;">
    /**
     * 简单消息提示
     * @param msg  消息内容
     * @param position 提示框方位 有：lb:左下方 lt:左上方 rt:右上方 rb:右下方（默认）
     */
    function jBox_simpleMsg(msg,position);
    </code>
</pre>
<p>2.方法jBox_Msg()消息提示(有标题、进度条、关闭按钮)</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * 复杂的消息提示框
        * @param title  消息标题
        * @param msg    消息内容
        * @param position  消息框方位（可选）
        */
        function jBox_Msg(msg,title,position);
    </code>
</pre>

<h1>layer msg提示</h1>
<p>额外引入进一步封装的layer弹出层<code style="background-color: transparent;">/js/common/myutil.js</code></p>
<p>1.方法Fast.msg(msg)最简单常用的提示，在中心弹出一个类似手机端toast的半透明消息提示框，几秒后自动消失。</p>

<p>2.方法$.layer.msg_top(msg,type)对第一个方法额外增加了位置参数，让它在顶部出现。(还可以额外扩展自己常用的方法)</p>

<p>3.类似地,方法$.layer.msg_right(msg,oparent)对第一个方法额外增加了图标参数(icon:1)</p>

<p>4.类似地,方法$.layer.msg_wrong(msg,oparent) 图标参数(icon:2)</p>

<p>5.类似地,方法$.layer.msg_ask(msg,oparent) 图标参数(icon:3)</p>

<p>6.类似地,方法$.layer.msg_locked(msg,oparent) 图标参数(icon:4)</p>

<p>7.类似地,方法$.layer.msg_cry(msg,oparent) 图标参数(icon:5)</p>

<p>8.类似地,方法$.layer.msg_smile(msg,oparent) 图标参数(icon:6)</p>

<p>9.类似地,方法$.layer.msg_warning(msg,oparent) 图标参数(icon:7)</p>

<h1>layer alert提示</h1>
<p>额外引入进一步封装的layer弹出层<code style="background-color: transparent;">/js/common/myutil.js</code></p>
<p>1.方法$.layer.alert(msg,options) 封装了layer alert隐藏了参数细节，方便调用</p>
<p>2.方法$.layer.alert_right(msg,options) 对第一个方法额外增加了图标参数(icon:1)</p>
<p>3.方法$.layer.alert_wrong(msg,options) 对第一个方法额外增加了图标参数(icon:2)</p>
<p>4.方法$.layer.alert_warning(msg,options) 对第一个方法额外增加了图标参数(icon:7)</p>

<h1>layer confirm询问框</h1>
<p>方法$.layer.confirm(msg,callbackOK,callbackCancel)封装了layer confirm参数细节，方便调用。</p>

<h1>layer Iframe弹出层</h1>
<p>1.方法layer_open() 封装了layer 弹出Iframe层的参数细节，方便调用。</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * 新增和编辑的时候弹出iframe层最常用到
        * @param title 标题
        * @param url 请求的url
        * @param oparent 一般为null
        * @param endCallBack 关闭该弹框后的回调函数
        * @param options layer的其他参数
        */
        function layer_open(title,url,oparent,endCallBack,options);
        或
        /**
        * 新增和编辑的时候弹出iframe层最常用到
        * @param title 标题
        * @param url 请求的url
        * @param options layer的其他参数,常用参数如：w 宽度，默认75%，可以为像素如500px；h 高度，默认75%，可以为像素如500px；end 关闭窗体回调函数，可用于刷新父页面数据；
        */
        $.layer.open_page(title, url,options);
    </code>
</pre>
<p>2.方法layer_open_full()  或  $.layer.open_page_full(title, url,options)与上面方法唯一区别是，该方法弹出后即全屏显示。</p>
<p>下面几个弹出的选择页面就是基于上面两个方法：</p>
<p>1).方法base_openYhxzPage()弹出用户选择</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * 弹出用户选择框
        * @param idInput 选择的用户ids返回给父页面元素的id
        * @param mcInput 选择的用户名称返回给父页面元素的id
        * @param params  可选参数，如limit=5表示限选5个
        * @param oparent 一般为null,表明调用该方法页面的层级
        */
        var base_openYhxzPage = function(idInput, mcInput, params,oparent){};
    </code>
</pre>
<p>a).方法base_openBmxzPage()弹出部门选择</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * 弹出用户选择框
        * @param idInput 选择的部门ids返回给父页面元素的id
        * @param mcInput 选择的部门名称返回给父页面元素的id
        * @param params  可选参数，如limit=5表示限选5个
        * @param oparent 一般为null,表明调用该方法页面的层级
        */
        var base_openBmxzPage = function(idInput, mcInput, params,oparent){};
    </code>
</pre>
<p>b).方法base_openBmxzPage()弹出部门选择</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * 弹出用户选择框
        * @param idInput 选择的部门ids返回给父页面元素的id
        * @param mcInput 选择的部门名称返回给父页面元素的id
        * @param params  可选参数，如limit=5表示限选5个
        * @param oparent 一般为null,表明调用该方法页面的层级
        */
        var base_openBmxzPage = function(idInput, mcInput, params,oparent){};
    </code>
</pre>
<p>c).方法base_openIconPage()弹出图标选择</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * 弹出图标选择框
        * @param idInput 选择图标class返回给调用页面元素的id
        * @param value 初始值
        * @param oparent 一般为null,表明调用该方法页面的层级
        */
        var base_openIconPage = function(idInput, value,oparent){};
    </code>
</pre>

<p>d).方法base_openZyflxzPage()弹出资源分类选择</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * 弹出资源分类选择
        * @param idInput 选中的资源分类id返回给调用页面元素的id
        * @param mcInput 选中的资源分类名称返回给调用页面元素的id
        * @param params 可选参数
        * @param oparent 一般为null,表明调用该方法页面的层级
        */
        var base_openZyflxzPage = function(idInput, mcInput, params,oparent){};
    </code>
</pre>

<h1>ajax请求</h1>
<p>封装了jquery ajax请求的参数细节，方便调用。</p>
<p>1.方法 <code style="background-color: transparent;">sys_ajaxPost</code> 最常用的post请求。</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * ajax post
        * @param _url 请求地址 如：/sys/user/save
        * @param _data 请求参数
        * @param callback 成功的回调函数
        */
        function sys_ajaxPost(_url,_data,callback);
    </code>
</pre>
<p>2.方法 <code style="background-color: transparent;">sys_ajaxGet</code> ajax get请求,两参数时会自动调用本js中的方法<code style="background-color: transparent;">bind()</code>实现后台数据显示到页面，三参数需要自己调用。</p>
<pre>
    <code style="background-color: transparent;">
        /**
        * ajax get
        * @param _url 请求地址
        * @param _data 请求参数
        * @param callback 可选参数,成功的回调函数
        */
        function sys_ajaxGet(_url,_data,callback);
    </code>
</pre>


<h1>其它方法</h1>
<ul>
    <li>方法 <code style="background-color: transparent;">isEmail(val)</code> 判断是否email</li>
    <li>方法 <code style="background-color: transparent;">isQQ(val)</code>判断是否QQ </li>
    <li>方法 <code style="background-color: transparent;">isEmpty(val)</code>判断是否为空 </li>
    <li>方法 <code style="background-color: transparent;">isNotEmpty(val)</code>判断是否不为空 </li>
    <li>方法 <code style="background-color: transparent;">Fast.changeUrlArg(url, arg, val)</code>修改url中某个参数的值</li>
</ul>

<p>上面只提及了部分封装的常用的方法，myutil.js中未提及的方法并不是不重要。有兴趣想知道细节的可以仔细阅读。比如后台数据如何通过bing()方法在前台显示、ajax全局设置捕获请求的非正常状态情况的处理等等。</p>