<div style="padding:0px 0; border-bottom: 1px dashed #ccc">
    <h1>权限管理</h1>
</div>
<h1 id="-">引言</h1>
<p>权限管理是一个系统非常重要必不可少的一项内容,它现对用户访问系统的控制，按照一定规则控制用户可以访问而且只能访问自己被授权的资源，比如游客访问不到需用户登录才能看到的内容、普通用户看不到管理员才能有权访问的内容、用户只能操作被授权操作的内容等等。很软件都使用Apache Shiro作为权限管理框架，实现用户认证、用户授权等。</p>
<p>几年前，我去一家公司去面试，谈及Shiro的时候，面试官问我：能否不用Shiro实现用户授权。当时我非常菜，我回答，使用springMVC的拦截器可以做到，至于怎么做，我一时也想不明白。</p>
<p>不可否认，Shiro作为权限管理的框架是非常优秀，但是考虑到对新人又是一种学习负担，于是目前没有引入它。而是使用自己设计的拦截器实现这一目的。（但以后不排除会引入Shiro）</p>
<h1 id="-">权限管理模型</h1>
<p>1.用户：登录系统的用户。(表sys_user)</p>
<p>2.角色：用户拥有的角色。(表org_role)</p>
<p>3.权限：权限是该系统一个访问页面、一个功能菜单、或是一组功能。它是存放在sys_permission表中的一条数据。它有2中体现形式：访问该功能的url或访问该功能的权限代码code。</p>
<p>4.权限-角色：这是权限与角色的对应关系。(表sys_permission_role)</p>
<p>5.权限-用户：除了通过用户角色给用户赋予权限外，还可以直接给用户赋予权限。用户与权限对应关系。(表sys_permission_user)</p>
<h1 id="-">整体思路</h1>
<p>登录到系统的用户，在登后，在缓存中存放了该用户所拥有的权限url和用户拥有的权限代码code,当然还有该用户其它身份信息。该登录用户在访问系统任何内容（当然排除掉静态资源和一些无需进行权限访问控制的功能比如登录和退出等等）都会经过请求过滤拦截器（RequestFilterInterceptor.java）的处理。&nbsp;<a style="text-decoration: underline;color:#3bb4f2;" target="_blank" href="/resources/myblog/images/ljqlct.png">逻辑流程图</a></p>
<p>通过它的处理逻辑可以实现:</p>
<p>1.当前访问的是静态资源或被配置允许通过该拦截器的内容允许通过该拦截器</p>
<p>2.登录用户是否有权访问当前访问的内容</p>
<p>3.检查当前访问的内容是否符合注解@RequireGuest(被该注解标记的方法允许不登录访问)、@RequirePermission（登录用户必须拥有它要求的权限）、@RequireRoles（登录用户必须拥有它要求的角色）、@RequiresAuthentication（登录即可访问）等注解的要求</p>
<p>4.未登录用户抛出未登录异常(NoLoginException)。</p>
<p>5.被检查为没有授权该访问内容抛出未授权访问异常(ForbiddenException)。</p>

<h1 id="-">开发中权限控制的集中应用</h1>
<h2 id="">编程式</h2>
<p>
    工具类<code style="background-color: transparent;">MyBatisRequestUtil.java</code>中封装了2个方法：
</p>
<pre>
    <code class="hljs hljs php" style="background-color: transparent;">
        /**
        * 登录用户是否有某个url操作权限
        * @param permission 权限url或权限代码code
        * @param request
        * @return
        */
        public static boolean hasPermission(String permission,HttpServletRequest request){
        }

        /**
        * 登陆用户是否有某个角色
        * @param role 角色name
        * @param request
        * @return
        */
        public static boolean hasRole(String role,HttpServletRequest request){
        }
    </code>
</pre>
<p>在Controller中这样使用:</p>
<pre>
    <code class="hljs hljs php" style="background-color: transparent;">
        if(MyBatisRequestUtil.hasPermission("/project/mission/edit",request)){
            System.out.pirntln("当前用户有任务编辑权限");
        }
        if(MyBatisRequestUtil.hasPermission("project.mission.submitMission", request)){
            System.out.pirntln("当前用户有提交任务权限");
        }
        if(MyBatisRequestUtil.hasRole("admin", request)){
            System.out.pirntln("当前用户有admin权限");
        }
    </code>
</pre>

<h2 id="">注解式（推荐）</h2>
<p>
    在Controller的方法上指定如下注解：
</p>
<ul>
    <li>@RequiresPermissions(value = {"/org/department","orgTreeManage"},logical= Logical.OR)：表示当前用户需要权限 /org/department 或 orgTreeManage。</li>
    <li>@RequiresAuthentication：表示当前用户已经通过login进行了身份验证</li>
    <li>@RequiresGuest：表示当前用户未登录，是游客身份。</li>
    <li>@RequiresRoles：表示当前用户需要指定的角色。</li>
    <li>@RequiresAnonymous：表示无论是否登录都可访问。</li>
</ul>
<p>注意：</p>
<ul>
    <li>建议权限字符串命名规则：模块:功能:操作，例如：sys:user:edit</li>
    <li>权限字符串Permission：指定权限串必须和菜单中的权限url或权限代码code匹配才行</li>
</ul>
<p>例子CacheMonitorController.java:</p>
<pre>
    <code class="hljs hljs php" style="background-color: transparent;">
        @Controller
        @RequestMapping("/sys/cache")
        @RequiresRoles(value = {"system","admin"},logical = Logical.OR )
        public class CacheMonitorController extends BaseController{
        ...
        }
    </code>
    该类的所有方法都要求system角色或者admin角色或者拥有"/sys/cache"权限才能访问。
</pre>

<h2 id="">视图页(SpringBoot版)</h2>
<p>用到freemark自定义的权限认证标签。</p>
<pre>
<xmp style="background-color: #eee;
    padding-bottom: 15px;
   /* color: #f96868;*/
    border-radius: 5px;
    box-shadow: 3px 0px 7px #333333;
    margin-bottom: 8px;">
    <@authTag method="hasPermission" permission="/project/mission/add">
        <!--拥有新增任务权限-->
        <a class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
    </@authTag>

    <@authTag method="hasRoles" role="admin">
        <!--拥有admin角色-->
    </@authTag>
</xmp>
</pre>