
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>技术文档</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/resources/myblog/assets/i/favicon.png">
    <link href="/resources/css/admui.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/myblog/amazeui/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/resources/website/index.css">
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>

<body class="doc">
${header!}
#parse("/website/nav.html")
<!-- content srart -->
<div class=" content-fixed " style="margin: 0 auto;width: 100%;z-index: 666;position: relative; margin-top:15px;">
    <!-- 侧边导航栏 -->
    <div class="left-sidebar"
         style="padding-top: 8px;top:inherit !important;left:inherit !important;width: 220px !important;background: rgba(255, 255, 255, 0.95) !important;">
        <style>
            html {
                width: 100%;
            }

            body {
                width: 100%;
            }

            body {
                font: 14px "微软雅黑", Arial, Helvetica, sans-serif;
                color: #555;
                /*background: url(/resources/js/login_examstart/images/login-bg-small.jpg) no-repeat;*/
                position: absolute;
                left: 0;
                top: 0;
                background-size: 100% 100%;
            }

            @media only screen and (min-width: 1200px) {
                .content-fixed {
                    max-width: 1200px;
                }
            }

            .left-sidebar {
                transition: all 0.4s ease-in-out;
                width: 240px;
                /* min-height: 100%; */
                min-height: 822px;
                padding-top: 57px;
                position: absolute;
                z-index: 1104;
                top: 0;
                left: 0px;
            }

            .tpl-content-wrapper {
                transition: all 0.4s ease-in-out;
                position: relative;
                margin-left: 240px;
                z-index: 1101;
                min-height: 822px;
                border-bottom-left-radius: 3px;
            }

            .sidebar-nav-link a:hover {
                text-decoration: none;
                background: #f2f6f9;
                color: #868E8E !important;
                border-left: #3bb4f2 3px solid !important;
            }

            .sidebar-nav-link a {
                text-decoration: none;
                border-left: #fff 3px solid;
            }

            .sidebar-nav-sub .sidebar-nav-link-logo {
                margin-left: 10px;
            }

            .sidebar-nav-link a {
                color: #000;
                border-left: #fff 3px solid !important;
            }

            .sidebar-nav-link a {
                display: block;
                color: #868E8E;
                padding: 10px 17px;
                border-left: #282d2f 3px solid;
                font-size: 14px;
                cursor: pointer;
            }

            .sidebar-nav-link a.active, .sidebar-nav-link a:hover {
                color: #3bb4f2 !important;
                text-decoration: none;
                background: #f2f6f9;
                color: #868E8E !important;
                border-left: #3bb4f2 3px solid !important;
            }

            .sidebar-nav-sub .sidebar-nav-link {
                font-size: 12px;
                padding-left: 15px;
            }

            .am-icon-chevron-down:before {
                content: "\f078";
            }

            [class*=am-icon-]:before {
                display: inline-block;
                font: normal normal normal 1.6rem/1 FontAwesome, sans-serif;
                font-size: inherit;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
                -webkit-transform: translate(0, 0);
                -ms-transform: translate(0, 0);
                transform: translate(0, 0);
            }

            .sidebar-nav-sub-ico-rotate {
                -webkit-transform: rotate(180deg);
                transform: rotate(180deg);
                -webkit-transition: all 300ms;
                transition: all 300ms;
            }

            pre {
                padding: 10px 15px;
                line-height: 22px;
                -webkit-border-radius: 4px;
                border-radius: 4px;
                border: 1px solid #ddd;
            }

            pre, code {
                font-family: "Source Code Pro", Monaco, Menlo, Consolas, monospace;
                color: #4d4d4c;
                background: #eee;
                font-size: 13px;
            }
            .hljs {
                display: block;
                padding: 0.5em;
                color: #000;
                background: #f8f8ff;
            }
            .hljs-keyword, .assignment, .hljs-literal, .css .rule .hljs-keyword, .hljs-winutils, .javascript .hljs-title, .lisp .hljs-title, .hljs-subst {
                color: #954121;
            }
            .hljs-comment, .hljs-template_comment, .diff .hljs-header, .hljs-javadoc {
                color: #408080;
                font-style: italic;
            }
            .hljs-string, .hljs-tag .hljs-value, .hljs-phpdoc, .tex .hljs-formula {
                color: #219161;
            }
        </style>
        <!--菜单-->
        <ul class="sidebar-nav">

            <li class="sidebar-nav-link data">
                <a id="item_1">
                    简介
                </a>
            </li>
            <li class="sidebar-nav-link data">
                <a id="item_2">
                    技术选型
                </a>
            </li>
            <li class="sidebar-nav-link data">
                <a id="item_3">
                    功能菜单
                </a>
            </li>
            <li class="sidebar-nav-link data">
                <a id="item_4">
                    更新日志
                </a>
            </li>
            <li class="sidebar-nav-link">
                <a href="javascript:;" class="sidebar-nav-sub-title ">
                    开发手册
                    <span style="float:right;margin-right: 1rem;"
                          class="am-icon-chevron-down  sidebar-nav-sub-ico sidebar-nav-sub-ico-rotate"></span>
                </a>
                <ul class="sidebar-nav sidebar-nav-sub " style="display: block;">
                    <li class="sidebar-nav-link data">
                        <a class="active"  id="item_5">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 部署运行
                        </a>
                    </li>

                    <li class="sidebar-nav-link data">
                        <a href="#" id="item_6">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 快速开发
                        </a>
                    </li>
                    <li class="sidebar-nav-link data">
                        <a href="#" id="item_7">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 权限管理
                        </a>
                    </li>
                    <li class="sidebar-nav-link data">
                        <a href="#" id="item_8">
                            <span class="am-icon-angle-right sidebar-nav-link-logo"></span> 封装js常用类
                        </a>
                    </li>
                </ul>
            </li>
            <li class="sidebar-nav-link data">
                <a href="#" id="item_9">
                    常见问题
                </a>
            </li>
        </ul>
    </div>

    <!-- 内容区域 -->
    <div class="tpl-content-wrapper"
         style="margin-top: 0px;background-color: rgba(255, 255, 255, 0.95) !important; z-index: 1000 !important;padding:0px;height: auto;">
        <div id="update_content">
            <style>
                #update_content ul li {
                    list-style: disc;
                    margin-left: 20px;
                    line-height: 30px;
                }

                #update_content ul li a {
                    text-decoration: underline;
                }

                #update_content ul li a:hover {
                    color: #337ab7 !important;
                    text-decoration: underline;
                }

                #update_content ul li span {
                    color: #939393;
                    margin-left: 10px;
                }
               .doc img{margin-bottom: 20px;max-width:100%;max-height:100%}
            </style>
            <div id="show_1">
                #parse("/website/doc/jianjie.html")
            </div>
            <div id="show_2" style="display: none;">
                #parse("/website/doc/technology.html")
            </div>
            <div id="show_3" style="display: none;">
                #parse("/website/doc/menu.html")
            </div>
                <div id="show_4" style="display: none;">
                #parse("/website/doc/log.html")
            </div>

            <div id="show_5" style="display: none;">
                #parse("/website/doc/deploy_run.html")
            </div>
            <div id="show_6" style="display: none;">

                #parse("/website/doc/dev.html")
            </div>
            <div id="show_7" style="display: none;">
                #parse("/website/doc/permission.html")
            </div>
            <div id="show_8" style="display: none;">
                #parse("/website/doc/myjs.html")
            </div>
            <div id="show_9" style="display: none;">
                #parse("/website/doc/question.html")
            </div>

        </div>
    </div>
    <!-- content end -->
</div>
#parse("/website/footer.html")

<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>

<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="/resources/myblog/assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->


<script type="text/javascript">
    function showthis() {
        var id;
        if (uri.indexOf("#") > 0) {
            id = uri.substring(uri.indexOf("#") + 1, uri.length);
        }
        if (isNotEmpty(id)) {
            $obj = $("#" + id);
            $obj.trigger("click");
        }

    }
    $(function () {

        $('.sidebar-nav-sub-title').on('click', function () {
            $(this).siblings('.sidebar-nav-sub').slideToggle(80)
                    .end()
                    .find('.sidebar-nav-sub-ico').toggleClass('sidebar-nav-sub-ico-rotate');
        })
        $('.data a').on('click', function () {
            $(this).parents(".sidebar-nav").find(".sidebar-nav-link a").removeClass("active");
            $(this).addClass("active");

            var id = $(this).attr("id");
            id = id.replace("item_", "");
            //alert(id);
            $("#update_content").find("#show_" + id).show().siblings().hide();
        })

        showthis();//定位模拟点击某个菜单

    })

</script>
</body>
</html>