<div style="padding:0px 0; border-bottom: 1px dashed #ccc">
    <h1>部署运行123</h1>
</div>
<h1 id="-">环境要求</h1>
<style type="text/css">
    .hideContent{
        display: none;border:1px dashed red;position: relative;padding:25px;margin:15px auto;
    }
</style>
<p>1、Java SDK 1.8 <a target="_blank" href="http://www.oracle.com/technetwork/cn/java/javase/downloads/jdk8-downloads-2133151-zhs.html">下载</a>
</p>

<p>2、IntelliJ IDEA 14.0.2云盘分享 <a target="_blank" href="https://pan.baidu.com/s/19Bmf6oZ1bp08zoLsY0iQTA">下载</a></p>

<p>3、Eclipse IDE for Java EE Mars 2 (4.5.2) <a target="_blank" href="https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/mars2">下载</a>（依
    Eclipse 举例，IDEA 雷同。）</p>

<p>4、Apache Maven 3.3+ <a target="_blank" href="https://maven.apache.org/download.cgi">下载</a></p>

<p>5、MySql 5.7+ <a target="_blank" href="https://dev.mysql.com/downloads/windows/installer/5.7.html">下载</a></p>
<p>6、MySql 5.6.25云盘分享 <a target="_blank" href="https://pan.baidu.com/s/1MZ8XXnBIx6hkO5M3GwnoIg">下载</a></p>
<p>7、Navicat11.2.7及破解工具云盘分享 <a target="_blank" href="https://pan.baidu.com/s/1jHrGF9HcFE5SRu_IL0UuqQ">下载</a></p>
<p>8、SQLyog-12.0.9-x64(Mysql视图界面管理工具)云盘分享 <a target="_blank" href="https://pan.baidu.com/s/1XBarULWkyBxg-DcX7ayy4A">下载</a></p>
<p>9、Tomcat8.0.22云盘分享 <a target="_blank" href="https://pan.baidu.com/s/18hQ8ZU_fWZYIDdgqalwXuw">下载</a></p>

<p>10、ideaIU-2018.2.4.exe云盘分享 <a target="_blank" href="https://pan.baidu.com/s/1H0GolRCuPLmt-1xaW6eokQ">下载</a></p>
<p>11、ideaIU-2018.2.4.exe激活码(可以使用至2019年5月7号)分享 <a target="_blank" href="/resources/website/idea注册码(20190507).txt">下载</a></p>
<h1 id="-idea">导入到Idea</h1>
<!--<p>两种思路：先打开idea界面，使用里面的功能检出源码或者先检出源码，然后导入到iead。</p>-->
<p>1、使用git检出YueCms源代码：</p>
<pre><code class="hljs hljs php" style="background-color: transparent;">git <span class="hljs-keyword"><span
        class="hljs-keyword">clone</span></span> https:<span class="hljs-comment"><span class="hljs-comment">//gitee.com/markbro/YueCms.git</span></span>
</code></pre>
<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;也可使用idea内置功能检出源码&nbsp;&nbsp;<a style="cursor: pointer;color:green;" onclick="showCloneDetail(this)">点击展开如何操作</a></strong></p>

<script type="text/javascript">
    function showCloneDetail(obj){
       var cl = $("#cloneDetail").attr("class");
       if(cl=='close'){
           $("#cloneDetail").removeClass("close").addClass("open").slideDown();
           $(obj).html("点击折叠").css("color","red");
       }else{
           $("#cloneDetail").removeClass("open").addClass("close").slideUp();
           $(obj).html("点击展开如何操作").css("color","green");
       }

    }
    function showImportDetail(obj){
        var cl = $("#importDetail").attr("class");
        if(cl=='close'){
            $("#importDetail").removeClass("close").addClass("open").slideDown();
            $(obj).html("点击折叠").css("color","red");
        }else{
            $("#importDetail").removeClass("open").addClass("close").slideUp();
            $(obj).html("点击展开").css("color","green");
        }
    }
    function showHide(obj){
        var $obj=$(obj).parent().next();
        if($obj.hasClass("close")){
            $obj.removeClass("close").addClass("open").show();
            $(obj).html("点击折叠").css("color","red");
        }else{
            $obj.removeClass("open").addClass("close").hide();
            $(obj).html("点击展开").css("color","green");
        }
    }
    function showImportJar(obj){
        var cl = $("#importJar").attr("class");
        if(cl=='close'){
            $("#importJar").removeClass("close").addClass("open").slideDown();
            $(obj).html("点击折叠").css("color","red");
        }else{
            $("#importJar").removeClass("open").addClass("close").slideUp();
            $(obj).html("点击展开").css("color","green");
        }
    }
</script>
    <div id="cloneDetail" class="close" style="display: none;border:1px dashed red;position: relative;padding:25px;margin:15px auto;">
    <p>1、打开idea界面，<span style="font-weight: 600;">VCS->Checkout from Version Control->git</span>  ,如下图：</p>
    <img class="demoImg" src="/resources/website/images/gitclone1.jpg">
    <br>
    <p>2.点击<strong>git</strong>菜单，弹出如下图，依次输入源码网址（<strong>https://gitee.com/markbro/YueCms.git</strong>）、存放目录、文件夹名称：</p>
    <img class="demoImg"  src="/resources/website/images/gitclone2.jpg">
    <p>3.点击<strong>clone</strong>按钮,iead界面下方出现下载进度条：</p>
    <img class="demoImg" src="/resources/website/images/gitclone3.jpg">
    <p>4.下载完成：</p>
    <img class="demoImg" src="/resources/website/images/gitclone4.jpg">
</div>

<p>2、准备导入。
    您可以选择是否重命名下载的源码文件夹，比如重命名为<code style="background-color: transparent;">YueCms-dmo</code>。默认为<code style="background-color: transparent;">YueCms</code>
</p>

<p>3、准备导入。您可以选择是否重命名项目名称，比如<code style="background-color: transparent;">YueCms-dmo</code>。默认为<code style="background-color: transparent;">YueCms</code>。
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打开<code style="background-color: transparent;">pom.xml</code>文件，修改第13行，artifactId为你的工程名，如：<code
            style="background-color: transparent;">&lt;artifactId&gt;YueCms-demo&lt;/artifactId&gt;</code>
    </p>
</p>
<p>4、导入到Idea.菜单 File -&gt; Import Project，然后选源码根目录<code style="background-color: transparent;">YueCms-dmo</code>，按照默认设置一直点击&nbsp;<img style="margin-bottom: 7px;" src="/resources/website/images/btn_next.jpg" alt="next">&nbsp; 按钮，直至点击 &nbsp;<img style="margin-bottom: 7px;" src="/resources/website/images/btn_finish.jpg" alt="next">&nbsp; 按钮，即可成功导入。
<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;详细步骤图解&nbsp;&nbsp;<a style="cursor: pointer;color:green;" onclick="showImportDetail(this)">点击展开</a></strong>
</p>
<div id="importDetail" class="close" style="display: none;border:1px dashed red;position: relative;padding:25px;margin:15px auto;">
    <p>1).打开idea界面，<span style="font-weight: 600;">菜单File-&gt;Import Project</span>  ,如下图：</p>
    <img  src="/resources/website/images/import_idea_1.jpg">
    <br>
    <p>2).选择源码根目录：</p>
    <img  style="margin-bottom: 7px;"  src="/resources/website/images/import_idea_2.jpg">
    <p>3).选择<code style="background-color: transparent;">Import project from external model-&gt;Maven</code>：</p>
    <img  src="/resources/website/images/import_idea_3.jpg">
    <p>4).点击&nbsp;<img style="margin-bottom: 7px;" src="/resources/website/images/btn_next.jpg" alt="next">&nbsp; 按钮：</p>
    <img src="/resources/website/images/import_idea_4.jpg">
    <p>5).点击&nbsp;<img style="margin-bottom: 7px;" src="/resources/website/images/btn_next.jpg" alt="next">&nbsp; 按钮：</p>
    <img  src="/resources/website/images/import_idea_5.jpg">
    <p>6).点击&nbsp;<img style="margin-bottom: 7px;" src="/resources/website/images/btn_next.jpg" alt="next">&nbsp; 按钮：</p>
    <img   src="/resources/website/images/import_idea_6.jpg">
    <p>7).点击&nbsp;<img style="margin-bottom: 7px;" src="/resources/website/images/btn_finish.jpg" alt="next">&nbsp; 按钮：</p>
    <img  src="/resources/website/images/import_idea_7.jpg">
    <p>8).导入Idea完成：</p>
    <img  src="/resources/website/images/import_idea_8.jpg">
</div>
<p>5、引入jar包依赖。这时，Idea会自动加载Maven依赖包，初次加载会比较慢（根据自身网络情况而定），有几个jar并没有添加maven,而是在<code style="background-color: transparent;">webapp-&gt;WEB-INF-&gt;lib</code>下(<code style="background-color: transparent;">asoiaf-core.jar</code>、<code style="background-color: transparent;">asoiaf-mybatis-cache.jar</code>、<code style="background-color: transparent;">bearinfested-gen-velocity.jar</code>、<code style="background-color: transparent;">xsqlbuilder.jar</code>)。
    <p>简单的可以直接让项目引用本地jar<strong><a style="cursor: pointer;color:green;" onclick="showImportJar(this)">&nbsp;点击展开&nbsp;</a></strong>,也可以将本地jar添加到本地maven仓库<a target="_blank" style="color: #008000;" href="https://blog.csdn.net/litter_strong/article/details/61411971">&nbsp;<strong>如何手动添加jar包到本地仓库?</strong></a>。</p>
    <div id="importJar" class="close" style="display: none;border:1px dashed red;position: relative;padding:25px;margin:15px auto;">
        <p>1).点击选中项目根目录，按快捷键<strong>F4</strong>：</p>
        <img  style="margin-bottom: 7px;"  src="/resources/website/images/import_jar_1.jpg">
        <p>2).点击选中<code style="background-color: transparent;">Project Settings-&gt;Libraries</code>：</p>
        <img  style="margin-bottom: 7px;"  src="/resources/website/images/import_jar_2.jpg">
        <p>3).选中要导入的jar点击<code style="background-color: transparent;">OK</code>按钮：</p>
        <img  style="margin-bottom: 7px;"  src="/resources/website/images/import_jar_3.jpg">
    </div>
</p>

    <p>6、（<strong>若以上过程有问题，可以留言提问</strong>）下载jar包过程中，可以先去准备数据库环境。加入所有jar包依赖解决完毕，如果要项目运行起来，还需要经过<strong><a href="#prepareDb">准备数据库环境</a></strong>、<strong><a href="#prepareFacets">配置项目Facets</a></strong>、<strong><a href="#prepareTomcat">配置本地tomcat环境</a></strong></p>


<h1 id="prepareDb">初始化数据库(以MySql为例)</h1>
<p>1、打开文件 <code style="background-color: transparent;">/src/main/resources/my.properties</code>
    配置产品和项目名称及JDBC连接</p>
<pre><code class="lang-yml hljs hljs bash" style="background-color: transparent;">
    <span class="hljs-comment">#============================#</span>
    <span class="hljs-comment">#===== Database sttings =====#</span>
    <span class="hljs-comment">#============================#</span>
    jdbc.type=mysql
    jdbc.driver=com.mysql.jdbc.Driver
    jdbc.url=jdbc:mysql://127.0.0.1:3306/bearinfested?useUnicode=true&characterEncoding=utf8
    jdbc.username=root
    <span class="hljs-comment">#加密的密码，下面的密码对应的明文是123456</span>
    jdbc.password=5Zj5ZS8E9Dm=
</code></pre>
<p>2、导入项目根目录sql文件夹中的init-mysql.sql数据库文件</p>

<h1 id="prepareFacets">IDEA配置项目Facets</h1>
<!--<p>初次使用idea这个开发工具，可能感觉有很多地方不适应，比如项目配置和添加tomcat就和(my)eclipse差异很大。一旦您掌握了技巧，就会发现它比(my)eclipse好用多了。不喜勿喷。</p>-->
<p>初次导入的WEB项目,需要先配置下项目Facets.(spring和web)</p>
<p>1、选中项目根目录，按快捷键F4</p>
<img  src="/resources/website/images/facets_1.jpg">

<p>2、点击选中<code style="background-color: transparent;">Project Settings-&gt;Libraries</code>:</p>
<img  src="/resources/website/images/facets_2.jpg">
<p>3、点击加号，选中<strong style="color: #008000;">spring</strong>:</p>
<img  src="/resources/website/images/facets_3.jpg">
<p>4、点击<strong>OK</strong>按钮:</p>
<img  src="/resources/website/images/facets_4.jpg">
<p>5、点击<strong>+</strong>按钮:</p>
<img  src="/resources/website/images/facets_5.jpg">
<p>6、如下图所示,选中本项目的xml文件,点击<strong>OK</strong>按钮:</p>
<img  src="/resources/website/images/facets_6.jpg">
<p>7、点击<strong>applay</strong>按钮,Facets（spring）添加完毕:</p>
<img  src="/resources/website/images/facets_7.jpg">
<p>8、类似地添加Facets(web):</p>
<img  src="/resources/website/images/facets_8.jpg">
<p>9、需要修改2个路径，第一个是web.xml的路径，默认路径是<code style="background-color: transparent;">项目根目录-&gt;web-&gt;web.xml</code>,第二个路径是项目webapp根路径，默认也是<code style="background-color: transparent;">项目根目录-&gt;web</code>:</p>
<img  src="/resources/website/images/facets_9.jpg">
<p>10、修改完毕后，点击&nbsp;<img style="margin-bottom: 7px;" src="/resources/website/images/btn_applay.jpg" alt="next">&nbsp; 按钮:</p>
<img  src="/resources/website/images/facets_10.jpg">
<p>11、成功配置项目Facets(Web)后,webapp文件夹变成如下图所示。</p>
<img  src="/resources/website/images/facets_11.jpg">
<p>至此，启动该项目就差最后一步↓^_^↓，截图比较多，看起来很繁琐。</p>
<h1 id="prepareTomcat">IDEA配置本地tomcat环境，启动Tomcat服务</h1>
<p>准备工作。下载tomcat8+,并修改文件<code style="background-color: transparent;">tomcat/conf/server.xml</code>，在节点<code style="background-color: transparent;">&lt;/Host&gt;</code>之上加一行以指定本系统上传文件相对路径和绝对路径的对应关系:</p>
<xmp  style="    background-color: #eee;
    padding-bottom: 15px;
    font-weight: 600;
    color: #f96868;
    border-radius: 5px;
    box-shadow: 3px 0px 7px #333333;
    margin-bottom: 8px;">
    <Context  path="/uploadresources"  docBase="d:\YueCms\uploadresources" reloadable="true"  crossContext="true"/>
</xmp>
<p><strong>若下载使用上面云盘分享的，这行代码已经添加了，根据实际情况修改。</strong></p>
<p>1、如下图，点击工具栏右侧：</p>
<img  src="/resources/website/images/tomcat_1.jpg">
<p>2、选中Tomcat Server-&gt;Local：</p>
<img  src="/resources/website/images/tomcat_2.jpg">
<p>3、点击Configure按钮：</p>
<img  src="/resources/website/images/tomcat_3.jpg">
<p>4、选中准备好的Tomcat8.0+根路径：</p>
<img  src="/resources/website/images/tomcat_4.jpg">
<p>5、</p>
<img  src="/resources/website/images/tomcat_5.jpg">
<p>6、</p>
<img  src="/resources/website/images/tomcat_6.jpg">
<p>7、</p>
<img  src="/resources/website/images/tomcat_7.jpg">
<p>8、至此，本地已经成功配置tomcat服务，如下图，运行本项目即可。</p>
<img  src="/resources/website/images/tomcat_8.jpg">

<h1 id="-">浏览器访问</h1>
<p>若以上步骤都没什么问题，项目成功启动，Idea会在运行成功后使用配置的浏览器自动访问<code style="background-color: transparent;">http://localhost:8080/</code>。&nbsp;&nbsp;如何设置？<a style="cursor: pointer;color:green;font-weight: 500;" onclick="showHide(this)">点击展开</a></p>
<div class="close hideContent">
    <img  src="/resources/website/images/web_1.jpg">
</div>
<p>1、地址：<a href="http://localhost:8080/">http://localhost:8080/</a></p>

<p>2、默认最高管理员账号：system 密码：1</p>

<p>5、这时已经配置完成，开启你的开发之旅吧</p>

<h1 id="-">部署到正式服务器</h1>

<p>1、确保正式服务器<code style="background-color: transparent;">jdk1.8</code>及<code
        style="background-color: transparent;">数据库</code>环境已经具备</p>

<p>2、配置系统环境变量<code style="background-color: transparent;">JAVA_HOME</code>指定<code
        style="background-color: transparent;">jdk1.8</code>路径，注意不要包含空格。
    <a target="_blank"
       href="https://jingyan.baidu.com/article/6dad5075d1dc40a123e36ea3.html">百度经验如何配置Java环境变量</a>
</p>

<p>3、使用Idea或其他工具将项目打成war包，复制war包到服务器。<a target="_blank"
                                        href="https://blog.csdn.net/ns__l/article/details/78961759">IntelliJ idea中如何将javaweb项目打包为war</a></p>

<p>4、进行解压<code style="background-color: transparent;">war</code>包，配置<code
        style="background-color: transparent;">my.properties</code>相关参数，解压到准备好的tomcat路径<code
        style="background-color: transparent;">/webapps/ROOT</code>下，并适当配置tomcat相关参数，运行<code
        style="background-color: transparent;">/bin/startup.bat</code></p>

<p>5、若需优化<code style="background-color: transparent;">jvm</code>参数，编辑<code
        style="background-color: transparent;">startup.bat</code>修改<code
        style="background-color: transparent;">JAVA_OPTS</code>即可</p>
