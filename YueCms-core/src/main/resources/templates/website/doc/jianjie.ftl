<div style="padding:0px 0; border-bottom: 1px dashed #ccc">
    <h1>简介</h1>
</div>
<ul>
    <li>YueCms 官网地址：<a href="http://www.cmsyue.com/">http://www.cmsyue.com/</a></li>
    <li>YueCms 在线文档：<a href="http://www.cmsyue.com/doc">http://www.cmsyue.com/doc</a></li>
    <li>YueCms 源码下载：<a target="_blank" href="https://gitee.com/markbro/YueCms">https://gitee.com/markbro/YueCms</a></li>
    <li>YueCms 在线提问：<a target="_blank" href="https://gitee.com/markbro/YueCms/issues">https://gitee.com/markbro/YueCms/issues</a>
    </li>
    <!--<li>YueCms 演示地址：<a target="_blank" href="#">http://www.cmsyue.com/doc</a></li>-->
    <!--<li>个人博客：<a target="_blank" href="http://my.oschina.net/markbro">http://my.oschina.net/markbro</a></li>-->
    <li>QQ 群号： <!--<code style="background-color: transparent;">769537407(已满)</code>--> <code
            style="background-color: transparent;">769537407</code></li>
</ul>
<p>YueCms 是一个 Java EE 企业级快速开发平台，基于经典技术组合（Spring、Spring
    MVC、MyBatis、Bootstrap、Layer），本地代码生成功能，包括核心模块如：组织机构、角色用户、菜单及按钮授权、数据权限、系统参数、内容管理、工作流等。采用松耦合设计； 采用Cookie-Token方式实现了自己的会话管理，支持同一用户跨浏览器访问；支持集群；支持多数据源。</p>

<p>本系统主要目的是能够让初级的研发人员快速的开发出复杂的业务功能（经典架构会的人多），让开发者注重专注业务，其余有平台来封装技术细节，降低技术难度，从而节省人力成本，缩短项目周期，提高软件安全质量。</p>

<p>本系统是个人结合多年经验总结所成，最大的特点就是入门容易，学习成本很低。可以成为刚毕业的大学生作为入门教材，快速的去学习实践。
</p>
<p><strong>它的优势：</strong>架构清晰、技术先进、入门简单、易于维护、易于扩展。我觉得它最大优势是入门简单。</p>
