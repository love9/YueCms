<div style="padding:0px 0; border-bottom: 1px dashed #ccc">
    <h1>技术选型</h1>
</div>
<h2 id="1-">1、环境</h2>
<ul>
    <li>Java 8</li>
    <li>Servlet 3.0</li>
    <li>Apache Maven 3</li>
</ul>
<h2 id="2-">2、主框架</h2>
<ul>
    <li>Spring Framework 4.2.5</li>
</ul>
<h2 id="3-">3、持久层</h2>
<ul>
    <li>Apache MyBatis 3.2.7</li>
    <li>Alibaba Druid 1.1</li>
</ul>
<h2 id="4-">4、视图层</h2>
<ul>
    <li>Spring MVC 4.1.4</li>
    <li>JSP、HTML</li>
    <li><strong>CSS框架：</strong>
        <ul>
            <li>Bootstrap 3.3</li>
            <li>AmazeUI</li>
            <li>LayerUI</li>
        </ul>
    </li>
    <li><strong>JS框架及组件：</strong>
        <ul>
            <li>jQuery 1.9.1</li>
            <li>layer 3.0</li>
            <li>zTree 3.5</li>
            <li>bootstrap-table</li>
            <li>Validform_v5.3.2</li>
            <li>My97DatePicker</li>
            <li>plupload</li>
            <li>select2</li>
            <li>ueditor</li>
            <li>jBox-notice</li>
            <li>...</li>
        </ul>
    </li>
</ul>
<h2 id="5-">5、工具组件</h2>
<ul>
    <li>Logback 1.1.2</li>
    <li>Apache Commons
        <ul>
            <li>lang3</li>
            <li>io</li>
            <li>logging</li>
            <li>fileupload</li>
            <li>codec</li>
            <li>collections</li>
            <li>email</li>
        </ul>
    </li>
    <li>JSON序列化
        <ul>
            <li>Jackson 2.8</li>
            <li>gson</li>
            <li>fastjson</li>
        </ul>
    </li>
    <li>Office工具：POI 3.14</li>
    <li>MyBatis分页：mybatis-paginator 1.2.17</li>
    <li>简单任务调度：Spring Task</li>
    <li>动态任务调度：quartz 2.2.2</li>
    <li>JSON Web Token：jjwt 0.7.0</li>
    <!--  <li>乐云短信：<a href="http://www.lehuo520.cn">http://www.lehuo520.cn</a></li>-->
    <li>其它
        <ul>
            <li>velocity 1.7</li>
            <li>guava 15.0</li>
            <li>zxing 3.1.0</li>
            <li>itextpdf 5.5.1</li>
            <li>ansj_seg 5.1.1</li>
        </ul>
    </li>
</ul>
<h2 id="6-">6、数据高速缓存</h2>
<ul>
    <li>EhCache 2.10</li>
    <li>Redis/Jedis 2.9</li>
</ul>
<h2 id="7-">7、数据库支持</h2>
<ul>
    <li>MySQL</li>
</ul>


