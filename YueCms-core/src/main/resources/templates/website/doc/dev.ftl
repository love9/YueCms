<div style="padding:0px 0; border-bottom: 1px dashed #ccc">
    <h1>快速开发</h1>
</div>
<h1>使用代码生成器</h1>
<p style="text-indent: 2rem;">
    借助于代码生成器，能通过事先配置的相关参数快速生成一个功能的实体类bean、dao、mapper、service、controller、jsp页面，系统需重启之后，可以访问该功能使用它简单的增删查改。</p>
<p style="text-indent: 2rem;">
    目前生成jsp页面表单元素全部是text类型，实际开发中可能会遇到其它多种表单元素，文本域textarea、日期选择器、开关、select、radio、checkebox等等，所以代码生成之后，<strong>还需根据实际要求修改表单元素的类型，这是目前的代码生成器以后完善的目标</strong>。
</p>
<p style="text-indent: 2rem;">
    代码生成器使用方式是，先在<code style="background-color: transparent">generator.properties</code>修改相关参数，然后右键运行 <code
        style="background-color: transparent">src/main.java.codegen/CodeGen.java</code>即可。
</p>
<p style="text-indent: 2rem;">
    <code style="background-color: transparent">generator.properties</code>部分参数如下：
</p>
<pre>
    <code style="background-color: transparent;">
        <span class="hljs-comment">#最常用的配置</span>
        <span class="hljs-comment">#主（父）表</span>
        <strong>gen.ptable_name=<span class="hljs-keyword">sys_user</span></strong>
        <span class="hljs-comment">#模块名</span>
        <strong>gen.model_name=<span class="hljs-keyword">sys</span></strong>
        <span class="hljs-comment">#子模块名</span>
        <strong>gen.sub_model_name=<span class="hljs-keyword">user</span></strong>
        <span class="hljs-comment">#bean中文名称，比如“用户”</span>
        <strong>gen.beanChineseName=<span class="hljs-keyword">用户</span></strong>

        <span class="hljs-comment">#各个文件生成开关</span>
        gen.GenBean=<span class="hljs-keyword">true</span>
        gen.GenDao=<span class="hljs-keyword">true</span>
        gen.GenMbtsXml=<span class="hljs-keyword">true</span>
        gen.GenService=<span class="hljs-keyword">true</span>
        gen.GenWebController=<span class="hljs-keyword">true</span>
        gen.GenJsp=<span class="hljs-keyword">true</span>
        <span class="hljs-comment"> #新增下面三个文件开关可能时因为三个文件分开重新生成</span>
        gen.GenJsp_list=<span class="hljs-keyword">true</span>
        gen.GenJsp_add=<span class="hljs-keyword">true</span>
        gen.GenJsp_edit=<span class="hljs-keyword">true</span>
        <span class="hljs-comment">#是否在数据库表生成增加、删除、编辑的权限 默认是</span>
        <span class="hljs-comment">#页面权限的父ID(260是cms模块权限ID,也就是sys_permission表中的id)</span>
        gen.GenPermission_list_parentid=<span class="hljs-keyword">260</span>
        gen.GenPermission_add=<span class="hljs-keyword">true</span>
        gen.GenPermission_delete=<span class="hljs-keyword">true</span>
        gen.GenPermission_edit=<span class="hljs-keyword">true</span>

        <span class="hljs-comment">#数据库相关</span>
        <span class="hljs-comment">#mysql</span>
        gen.jdbc.driver=<span class="hljs-keyword">com.mysql.jdbc.Driver</span>
        gen.jdbc.url=<span class="hljs-keyword">jdbc:mysql://localhost:3306/bearinfested?useUnicode=true&characterEncoding=utf8</span>
        gen.jdbc.username=<span class="hljs-keyword">root</span>
        gen.jdbc.password=<span class="hljs-keyword">123456</span>
        gen.jdbc.testsql=<span class="hljs-keyword">select 1</span>
        gen.database_name=<span class="hljs-keyword">bearinfested</span>
        gen.database_type=<span class="hljs-keyword">mysql</span>

    </code>
</pre>
<p>以上只列出了一小部分常用的配置参数。注意：</p>
<p>1.灵活使用开关的设置，可能生成文件之后，已经修改了摸个文件，如果需要再次运行代码生成，默认是所有文件重新生成一遍，这就覆盖了你已经修改的文件。这时候可以用各文件生成开关灵活控制再次生成是否覆盖该文件。</p>
<p>2.自动给该功能生成4个菜单，其中1个页面菜单，它从属与那个模块下面是<code style="background-color: transparent">gen.GenPermission_list_parentid=<span class="hljs-keyword">260</span></code>控制的，请根据实际情况去sys_permission查找。而在这个页面菜单下有增、删、改3个按钮菜单。（目前这个菜单生成功能没有排重，也就是统一功能多次运行代码生成器，每次都会生成4个菜单。）</p>

