<header>
    <div class="container">
        <!-- 导航 -->
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#example-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">切换导航</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <span class="navbar-brand">
                          <img width="60" height="60" style="display: inline-block;top: -19px;position: relative;" src="/resources/images/logo/logo.png">  <span style="display: inline-block;position: relative;top: -18px;">YueCms后台管理系统</span>
                        </span>
                </div>
                <div class="navbar-collapse collapse" id="example-navbar-collapse" aria-expanded="false" style="height: 1px;">
                    <ul class="nav navbar-nav" id="header_nav">
                        <li id="nav_front" class=""><a target="" href="/index">首页</a></li>
                        <li class=""><a target="_blank" href="https://gitee.com/markbro/YueCms">源码下载</a></li>
                        <li class=""><a target="_blank" href="https://gitee.com/markbro/YueCms/issues">在线提问</a></li>
                        <li class=""><a target="" href="/doc">文档</a></li>
                        <li class=""><a target="_blank" href="/myblog">前台演示</a></li>
                        <li class=""><a target="_blank" href="/admin">后台演示</a></li>
                        <!--<li class=""><a target="_blank" href="/contacUs">联系我们</a></li>-->
                    </ul>
                </div>
            </div>
        </nav>
        <script type="text/javascript">
            var url=  window.location.href;
            var host=window.location.host;
            var protocol=window.location.protocol;
            var uri=url.replace(protocol+"//"+host,"");
            function getCookie(name){
                //获取cookie字符串
                var strCookie=document.cookie;
                //将多cookie切割为多个名/值对
                var arrCookie=strCookie.split("; ");
                var value="";
                //遍历cookie数组，处理每个cookie对
                for(var i=0;i<arrCookie.length;i++){
                    var arr=arrCookie[i].split("=");
                    if(name==arr[0]){
                        value=arr[1];
                        break;
                    }
                }
                return value;
            }
            function removeNavClass(){
                var lis = document.getElementById("header_nav").getElementsByTagName("li");
                for (var i = 0; i < lis.length; i++) {
                    removeClass(lis[i],"active");
                }
            }

            function addNavClass(uri){
                uri=uri.replace("/myblog","");
                if(uri.indexOf("?")>0){
                    uri=uri.substring(0,uri.indexOf("?"));
                }
                if(uri.indexOf("#")>0){
                    uri=uri.substring(0,uri.indexOf("#"));
                }
                var lis = document.getElementById("header_nav").getElementsByTagName("li");
                for (var i = 0; i < lis.length; i++) {
                    if(lis[i].getElementsByTagName("a")[0].href.indexOf(uri)>=0){
                        lis[i].className="active";
                        break;
                    }
                }
            }
            function removeClass(elem,classname){
                if(elem.className != ""){
                    var allClassName = elem.className.split(" ");
                    //console.log("第一次赋值后class数量为：" + allClassName);
                    var result;//完成操作后保存类名（在以后调用）
                    var listClass;//保存修改过后的数组
                    for (var i = 0; i < allClassName.length; i++) {
                        if(allClassName[i] == classname){
                            allClassName.splice(i,1);
                        }
                    }
                    listClass = allClassName;
                    for (var j = 0; j < listClass.length; j++) {
                        //之后加上空格因为类名的存在形式就是用空格分隔的
                        if (j == 0) {
                            result = listClass[j];
                            result += " ";
                        }else{
                            result += listClass[j];
                            result += " ";
                        }
                    }
                    // console.log("处理后的listClass数量" + listClass);
                    elem.className = result;//将目标元素的类名重新被 result 覆盖掉
                }else{
                    //console.log("目标元素没有存在的类名")
                }
            }
            setTimeout(function(){
                removeNavClass();
                console.log(uri);
                addNavClass(uri)
                if(uri=="/"||uri=='/index'){
                    document.getElementById("nav_front").className="active";
                }
            },50);
        </script>

    </div>
</header>