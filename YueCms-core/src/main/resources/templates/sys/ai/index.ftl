
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>人工智能</title>
    <link rel="shortcut icon" href="favicon.ico">


    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</head>

<body class="body-bg main-bg">
<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页  <span class="c-gray en">&gt;</span>人工智能</nav>
<div class="wrapper animated fadeInRight">
    <!--工具条-->
    <div class="text-center">
        <a onclick="faceDetect()" class="btn btn-sm btn-primary"><i class="icon wb-check" aria-hidden="true"></i>&nbsp;人脸检测</a>
        <a onclick="" class="btn btn-sm btn-danger" >&nbsp;删除</a>
        <a onclick="" class="btn btn-sm btn-warning" >&nbsp;批量禁用</a>
        <a onclick="" class="btn btn-sm btn-info" >&nbsp;批量启用</a>
    </div>
</div>

<script type="text/javascript" >
    function faceDetect(){
        window.location.href="/ai/faceDetect";
    }
</script>
</body></html>
