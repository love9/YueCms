<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>身份证识别</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script type="text/javascript" src="/resources/lib/plupload/plupload.full.min.js"></script>
</head>

<body class="body-bg main-bg">
<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i><a style="font-size: 14px;" onclick="window.location.href='/ai'">人工智能</a>  <span class="c-gray en">&gt;</span>身份证识别</nav>
<div class="page animation-fade">
<div class=" panel" >

    <div class="row" style="padding:0px 35px;margin-top:0px;margin-bottom: 20px;">
           <div class="col-xlg-6  col-lg-6 col-md-12">
               <span class="label label-primary" style="position:absolute;top:0px;left:0px;padding:8px;z-index: 999;">识别图片</span>
               <div class="timeline-content">
                   <ul class="photos">
                       <li class="cover">
                           <img class="cover-image" src="/resources/images/nofile.png" id="icon">
                       </li>
                   </ul>
               </div>
           </div>
        <div class="col-xlg-6  col-lg-6 col-md-12" style="border: 1px solid #ababab;height:410px;">
            <span class="label label-success" style="position:absolute;top:0px;left:0px;padding:8px;z-index:999;">识别结果</span>
            <style>
                table {width: 100%;height:auto;border:1px solid gray;margin-top:10px;position:relative;z-index:0;}
                table tr{height:55px;border-bottom: 1px #ababab dashed;}
                table tr th{text-align: center;width: 40%;}
                table tr td{text-align: left;width: 60%;white-space: normal;}
            </style>
            <table>
                <tr>
                    <th>结果</th><td id="result"></td>
                </tr>
                <tr  >
                    <th>姓名</th><td id="name"></td>
                </tr>
                <tr >
                    <th>民族</th><td id="minzhu"></td>
                </tr>
                <tr >
                    <th>性别</th><td id="gender"></td>
                </tr>
                <tr  >
                    <th>ID</th><td id="ID"></td>
                </tr>

                <tr >
                    <th>生日</th><td id="birthday"></td>
                </tr>
                <tr  >
                    <th>地址</th><td id="address"></td>
                </tr>

            </table>
        </div>
    </div>
    <div class="row text-center">
        <a id="browse" class="btn btn-sm btn-primary">&nbsp;身份证识别</a>
    </div>
</div>
</div>
<script type="text/javascript" >

    var uploader = new plupload.Uploader({ //实例化一个plupload上传对象
        runtimes: 'html5,silverlight,html4,flash',
        browse_button : 'browse',
        url : '/ai/uploadSingle?type=idCard',
        flash_swf_url : '/resourcesydxs/plupload/Moxie.swf',
        silverlight_xap_url : '/resourcesydxs/plupload/Moxie.xap',
        filters: {
            max_file_size: '5mb', //最大上传文件大小（格式100b, 10kb, 10mb, 1gb）
            mime_types : [{ title : "图片文件", extensions:"jpg,jpeg,gif,png,bmp" }]}//只允许上传图片文件
    });
    //绑定文件添加进队列事件
    uploader.bind('FilesAdded',function(uploader,files){
        uploader.start(); //开始上传
        clearResult();
    });
    uploader.bind('Error',function(up, err){
        alert(err.message);//上传出错的时候触发
    });
    uploader.bind("FileUploaded", function(up, file, res){
        var json = JSON.parse(res.response);

        // alert(JSON.stringify(json));
        $("#icon").attr("src",json.path);

        $("#result").html(json.result);
        if(json.result=='成功'){
            $("#name").html(json.name);
            $("#gender").html(json.gender);
            $("#address").html(json.address);
            $("#ID").html(json.ID);
            $("#birthday").html(json.birthday);
            $("#minzhu").html(json.minzhu);
        }else{

        }
    })

    function clearResult(){
        $("#result").html("");
        $("#name").html("");
        $("#gender").html("");
        $("#address").html("");
        $("#ID").html("");
        $("#birthday").html("");
        $("#minzhu").html("");

    }
    window.onload = function() {
        uploader.init(); //初始化
    };

</script>
</body></html>
