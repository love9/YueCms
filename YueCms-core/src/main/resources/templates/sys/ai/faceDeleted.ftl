<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>人工检测</title>
    <link rel="shortcut icon" href="favicon.ico">


    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script type="text/javascript" src="/resources/lib/plupload/plupload.full.min.js"></script>
</head>

<body class="body-bg main-bg">
<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i><a style="font-size: 14px;" onclick="window.location.href='/ai'">人工智能</a>  <span class="c-gray en">&gt;</span>人脸检测</nav>
<div class="page animation-fade">
<div class=" panel" >
    <!--工具条-->
    <div class="row" style="padding:0px 35px;margin-top:0px;margin-bottom: 20px;">
        <div class="col-xlg-6  col-lg-6 col-md-12">
            <span class="label label-primary" style="position:absolute;top:0px;left:0px;padding:8px;z-index: 999;">识别图片</span>
            <div class="timeline-content">
                <ul class="photos">
                    <li class="cover">
                        <img class="cover-image" src="/resources/images/nofile.png" id="icon">
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-6 col-md-6" style="border: 1px solid #ababab;height: 350px;">
            <span class="label label-success" style="position:absolute;top:0px;left:0px;padding:8px;z-index:999;">识别结果</span>

            <style>
                table {width: 100%;height:auto;border:1px solid gray;margin-top:10px;position:relative;z-index:0;}
                table tr{height:55px;border-bottom: 1px #ababab dashed;}
                table tr th{text-align: center;width: 40%;}
                table tr td{text-align: left;width: 60%;white-space: normal;}
            </style>
            <table>
                <tr>
                    <th>结果</th><td id="result">成功</td>
                </tr>
                <tr>
                    <th>性别</th><td id="gender">男</td>
                </tr>
                <tr>
                    <th>颜值</th><td id="beauty">66</td>
                </tr>
                <tr>
                    <th>年龄</th><td id="age">22</td>
                </tr>
                <tr>
                    <th>表情</th><td id="expression">微笑</td>
                </tr>
                <tr>
                    <th>眼镜</th><td id="hasGlasses">无</td>
                </tr>

            </table>
        </div>
    </div>
    <div class="row text-center">
        <a download class="btn btn-sm btn-default" href="/resources/images/test_girl.png">&nbsp;下载测试图片</a>
        <a id="browse" class="btn btn-sm btn-primary">&nbsp;人脸检测</a>
    </div>
</div>
</div>
<script type="text/javascript" >
    function faceDetect(){
        window.location.href="/ai/faceDetect";
    }


    var uploader = new plupload.Uploader({ //实例化一个plupload上传对象
        runtimes: 'html5,silverlight,html4,flash',
        browse_button : 'browse',
        url : '/ai/uploadSingle?type=faceDetect',
        flash_swf_url : '/resourcesydxs/plupload/Moxie.swf',
        silverlight_xap_url : '/resourcesydxs/plupload/Moxie.xap',
        filters: {
            max_file_size: '10mb', //最大上传文件大小（格式100b, 10kb, 10mb, 1gb）
            mime_types : [{ title : "图片文件", extensions:"jpg,jpeg,gif,png,bmp" }]}//只允许上传图片文件
    });
    //绑定文件添加进队列事件
    uploader.bind('FilesAdded',function(uploader,files){
        uploader.start(); //开始上传
        clearFaceResult();
    });
    uploader.bind('Error',function(up, err){
        alert(err.message);//上传出错的时候触发
    });
    uploader.bind("FileUploaded", function(up, file, res){
        var json = JSON.parse(res.response);
      //alert(JSON.stringify(json));
        //$("#name").val(json[0].name);
        //$("#size").val(json[0].size);
        //$("#url").val(json[0].path);
        $("#icon").attr("src",json.path);
       // $("#suffixes").val(json[0].suffixes);
        $("#gender").html(json.gender);
        $("#beauty").html(json.beauty);
        $("#age").html(json.age);
        $("#expression").html(json.expression);
        $("#hasGlasses").html(json.hasGlasses);
        $("#result").html(json.result);
    })

    function clearFaceResult(){
        $("#gender").html("");
        $("#beauty").html("");
        $("#age").html("");
        $("#expression").html("");
        $("#hasGlasses").html("");
        $("#result").html("");
    }
    window.onload = function() {
        uploader.init(); //初始化
    };

</script>
</body></html>
