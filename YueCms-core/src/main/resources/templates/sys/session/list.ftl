<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>在线用户</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">

</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span>在线用户</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
              <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;踢出用户</a>
              <a  onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>


<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" >

    var sys_ctx="";

    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'orgid',align:"center",sortable:true,order:"asc",visible:false,title: '机构ID'},
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:true,title: '用户ID',width:"100px"},
            {field: 'token',align:"center",sortable:true,order:"asc",visible:true,title: 'Token'},
            {field: 'ip',align:"center",sortable:true,order:"asc",visible:true,title: 'IP',width:"120px"},
            {field: 'location',align:"center",sortable:true,order:"asc",visible:true,title: '登录地点'},
            {field: 'deviceType',align:"center",sortable:true,order:"asc",visible:true,title: '设备类型',width:"120px"},
            {field: 'explore',align:"center",sortable:true,order:"asc",visible:true,title: '浏览器',width:"120px"},
            {field: 'os',align:"center",sortable:true,order:"asc",visible:true,title: '操作系统',width:"120px"},
            {field: 'login_time',align:"center",sortable:true,order:"asc",visible:true,title: '登录时间',width:"140px"},
            {field: 'active_time',align:"center",sortable:true,order:"asc",visible:true,title: '最后访问时间',width:"140px"},
            {field: 'expires_time',align:"center",sortable:true,order:"asc",visible:false,title: '过期时间',width:"140px"},
            {field: '',align:"center",sortable:true,order:"asc",visible:true,title: '操作',width:"140px",formatter:function(v,row){
                var btn='<div class="btn-group"><button class="btn btn-xs  btn-danger " onclick="delRow(\''+row.token+'\')" type="button"><i class="fa fa-sign-out"></i><span class="bold">踢出用户</span></button></div>';
                return btn;
            }}
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.url='/sys/session/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
    }
    function delRow(token){
        if(isNotEmpty(token)){
            $.layer.confirm("确定这样做吗？", function(){
                $.ajax({
                    type:"post",
                    url:'/sys/session/json/delete?token='+token,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length == 1){
            $.layer.confirm("确定这样做吗？", function(){
                var token = selecRow[0]["token"]
                $.ajax({
                    type:"post",
                    url:'/sys/session/json/delete?token='+token,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择一行要删除的会话!")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
