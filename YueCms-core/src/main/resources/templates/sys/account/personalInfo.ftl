<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>个人资料</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico">
   <#-- <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">-->
    <link href="/resources/css/admui.css" rel="stylesheet">

    <link href="/resources/css/web-icons/web-icons.css" rel="stylesheet">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.cookie.js"></script>
    <#--<script src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>-->
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
    <script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
    <script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <#--<script src="/resources/SemanticUI/semantic.min.js"></script>-->
    <style type="text/css">
       /* <link href="http://cdn.admui.com/demo/basic/1.1.0/css/system/account.css" rel="stylesheet">*/
        .page-account .widget-header{padding:40px 15px;background-color:#fff}.page-account .widget-footer{padding:10px;background-color:#f6f9fd}.page-account .widget .avatar{width:130px;margin-bottom:10px}.page-account .account-user{margin-bottom:10px;color:#263238}.page-account .account-stat-count{display:block;margin-bottom:3px;font-size:20px;font-weight:300;color:#526069}.page-account .account-stat-count+span{color:#a3afb7}.page-account .tab-content .tab-message .media{padding-top:0}.page-account .tab-content .tab-message .media-heading{margin-top:5px}.page-account .tab-content .tab-message .media-left{padding:5px 10px 0 0}.page-account .tab-content .tab-message .media-left .icon{margin-left:.6em}.page-account .tab-content .tab-message .media-left .icon.system{background-color:#f96868}.page-account .tab-content .tab-message .media-left .icon.task{background-color:#46be8a}.page-account .tab-content .tab-message .media-left .icon.setting{background-color:#62a8ea}.page-account .tab-content .tab-message .media-left .icon.event{background-color:#8d6658}.page-account .tab-content .tab-message .media-left .icon.other{background-color:#f2a654}.page-account .tab-content .tab-message .media-right .btn{margin:5px 0 0;visibility:hidden}.page-account .tab-content .tab-message .media time{opacity:.5}.page-account .tab-content .tab-message .media:hover .media-right .btn{visibility:visible}
        .list-group-item {
            padding: 5px 10px;
        }
       .icon {
           line-height: 1;
       }

    </style>
</head>
<body class="gray-bg body-bg body-bg-personal" style="padding-top: 0px;overflow-x: hidden;">
<div class="page animation-fade page-account">
    <div class="page-content">
        <div class="row">
            <div class="col-md-3">
                <div class="widget widget-shadow text-center">
                    <div class="widget-header">
                        <div class="widget-header-content">
                            <a class="avatar avatar-lg" href="javascript:;" onclick="jumptHeadImage()">

                                    <#if headerPath??>
                                        <img id="headerImage" src="${headerPath!}" >
                                    <#else>
                                        <img id="headerImage" src="/resources/images/headicons/girl-1.png" >
                                    </#if>
                            </a>
                            <h4 class="account-user">${yhmc}</h4>
                            <p>上次登录：${lastLoginTime}</p>
                        </div>
                    </div>
                    <div class="widget-footer">
                        <div class="row no-space">
                            <div class="col-xs-4">
                                <strong class="account-stat-count">${loginCount!}</strong> <span>登录</span>
                            </div>
                            <div class="col-xs-4">
                                <strong id="messageCount" class="account-stat-count msg-number">0</strong> <span>消息</span>
                            </div>
                            <div class="col-xs-4">
                                <strong class="account-stat-count log-number">${loginLogCount!'-1'}</strong> <span>登录日志</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel">
                    <div class="panel-body nav-tabs-animate">
                        <ul class="nav nav-tabs nav-tabs-line tabList" role="tablist" >
                            <li class="news   active" role="presentation" id="accountMsg">
                                <a href="#"  >
                                    <i class="fa fa-comment-o" aria-hidden="true"></i> 消息
                                    <span id="messageCount2" class="badge badge-danger"></span></a>
                            </li>
                            <li class="news " role="presentation" id="accountLog" style="display: block;">
                                <a href="#"  >
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i> 登录日志
                                </a>
                            </li>


                            <@authTag method="hasPermission" permission="/account/changePwd">
                                   <li class="news" role="presentation">
                                       <a href="#"  >
                                           <i class="fa fa-key" aria-hidden="true"></i> 密码
                                       </a>
                                   </li>
                            </@authTag>


                            <li class="news " role="presentation">
                                <a href="#"  >
                                    <i class="fa fa-magic" aria-hidden="true"></i> 头像设置
                                </a>
                            </li>

                            <li class="news ">
                                <a href="#"  >
                                    <i class="fa fa-lock" aria-hidden="true"></i> 第三方帐号绑定
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content margin-top-15">

                            <!-- 消息-->

                            <div class="animation-slide-left tab-message mainContent" style=""  id="accountContent">
                                <div id="messageLists">
                                    <div class="list-group list-group-full">

                                    </div>
                                    <div style="float: right" id="pagebarMessage"  class="pagebar"  ></div>
                                </div>
                                <script type="text/javascript">

                                    var pageNo_msg=1;
                                    var pageSize_msg=5;
                                    var totalPages_msg;
                                    function pager_msg(){
                                        var $pagebar=$("#pagebarMessage");
                                        laypage({
                                            cont:$pagebar, //容器。值支持id名、原生dom对象，jquery对象,
                                            pages: totalPages_msg, //总页数
                                            curr:pageNo_msg,
                                            jump:function(e,first){
                                                if(!first){ //一定要加此判断，否则初始时会无限刷新
                                                    pageNo_msg=e.curr;
                                                    listMessage();
                                                }
                                            },
                                            skin: 'molv', //皮肤
                                            first: '首页', //若不显示，设置false即可
                                            last: '尾页', //若不显示，设置false即可
                                            prev: '上一页', //若不显示，设置false即可
                                            next: '下一页' //若不显示，设置false即可
                                        });
                                    }

                                    function listMessage(){

                                        var url='/base/recentNews/json/findRecentNews?page='+pageNo_msg+"&rows="+pageSize_msg;
                                        var html="";
                                        $.getJSON(url, function(json){
                                            //alert(json.unread);
                                            if(!(Number(json.total)>0)){
                                                html="<p style='height:253px;text-align: center;'>没有查询到任何记录！</p>";
                                                $("#messageCount2").html("");
                                                $("#messageCount").html("0");
                                                $("#pagebarMessage").html("");
                                                Fast.msg_error("没有查询到任何记录！");
                                                $("#messageLists .list-group").html(html);
                                                return;
                                            }

                                            if(Number(json.unread)>0){
                                                $("#messageCount2").html(json.unread+"+");
                                            }else{
                                                $("#messageCount2").html("");
                                            }
                                            $("#messageCount").html(json.total);
                                            totalPages_msg=Number((Number(json.total)+Number(Number(pageSize_msg)-1)))/pageSize_msg;

                                            $.each(json.rows.rows,function(index, item){
                                                html+='<div class="list-group-item" role="menuitem" data-message-id="'+item.id+'" data-title="'+item.title+'" data-content="'+item.content+'">';
                                                html+='<div class="media">';
                                                html+='<div class="media-left">';
                                                html+=' <i style="line-height: 1;" class="icon fa fa-tasks task white icon-circle" aria-hidden="true"></i>';
                                                html+='</div>';
                                                html+='<div class="media-body">';
                                                html+='<h6 class="media-heading">';
                                                html+='<a style="cursor:pointer;" class="news-list" onclick="showMsgContent('+item.id+')">';
                                                if(item.readflag=='0'){
                                                    html+='<i class="icon wb-medium-point red-600" aria-hidden="true"></i>  ';
                                                }
                                                html+=item.title;
                                                html+='</a>';
                                                html+='</h6>';
                                                html+='<time class="media-meta" datetime="'+item.createTime+'">'+item.elapse+'</time>';
                                                html+='</div>';
                                                html+='<div class="media-right">';
                                                html+='<a onclick="delMsg('+item.id+')" title="删除消息" class="btn btn-pure btn-default icon wb-close"></a>';
                                                html+='</div>';
                                                html+='</div>';
                                                html+='</div>';
                                            });
                                            $("#messageLists .list-group").html(html);
                                            pager_msg();
                                        });
                                    }
                                    function delMsg(id){
                                        sys_ajaxPost("/base/recentNews/json/delete/"+id,null,function(data) {
                                            var json=typeof data=='string'?JSON.parse(data):data;
                                            if (json.type == 'success') {
                                                Fast.msg_success("删除成功！");

                                            }else{
                                                $.layer.alert("删除消息异常，请联系管理员！");
                                                return;
                                            }
                                            listMessage();
                                        });
                                    }
                                    function showMsgContent(id){
                                        sys_ajaxPost("/base/recentNews/json/readMsg?id="+id,null,function(data){
                                            var json=typeof data=='string'?JSON.parse(data):data;
                                            if(json.type=='success'){
                                                var msg=json.data.recentNews;
                                                layer_openHtml("消息","<p style='padding:5px;line-height:2;font-size:16px;'>"+msg.content+"</p>",'',{
                                                    skin: 'layui-layer-rim', //加上边框
                                                    area: ['420px', '240px'] //宽高,
                                                    ,end:function(){
                                                        listMessage();
                                                    }
                                                });
                                            }else{
                                               $.layer.alert("系统异常，请联系管理员！");
                                            }

                                        })
                                    }

                                    $(function(){
                                        listMessage();
                                    })
                                </script>
                            </div>

                            <!-- 登录日志 -->
                            <div class="animation-slide-right tab-message mainContent" style="display:none;"  >
                                <div id="messageLists2">
                                    <#--<p class="text-center height-150 ver    tical-align no-message">
                                        <small class="vertical-align-middle opacity-four">没有日志</small>
                                    </p>-->

                                    <table id="tableList" class="table table-striped"></table>
                                </div>
                                <script type="text/javascript">
                                    //查询功能的标签说明
                                    var table_list_query_form = {
                                    };
                                    function refreshData() {
                                        $('#tableList').bootstrapTable('refresh');
                                    }
                                    $(function(){
                                        sys_table_list();
                                    });
                                    function sys_table_list(){
                                        $('#tableList').bootstrapTable('destroy');
                                        var columns=[{checkbox:true},
                                            {field: 'ip',align:"center",sortable:true,order:"asc",visible:true,title: 'IP'},
                                            {field: 'location',align:"center",sortable:true,order:"asc",visible:true,title: '登录地点'},
                                            {field: 'deviceType',align:"center",sortable:true,order:"asc",visible:true,title: '设备类型',formatter:function(v,row){
                                                if(v=="Computer"){
                                                    return "<span class=\"label label-primary radius\">电脑端</span>";
                                                }else if(v=="Tablet"){
                                                    return "<span class=\"label label-info radius\">平板</span>";
                                                }else if(v=="Mobile"){
                                                    return "<span class=\"label label-success radius\">手机</span>";
                                                }else{
                                                    return "<span class=\"label label-default radius\">未知</span>";
                                                }
                                            }},
                                            {field: 'explore',align:"center",sortable:true,order:"asc",visible:true,title: '浏览器'},
                                            {field: 'os',align:"center",sortable:true,order:"asc",visible:true,title: '操作系统'},
                                            {field: 'result',align:"center",sortable:true,order:"asc",visible:false,title: '登录结果'},
                                            {field: 'msg',align:"center",sortable:true,order:"asc",visible:true,title: '登录结果'},
                                            {field: 'createTime',align:"center",sortable:true,order:"asc",visible:true,title: '登录时间'},
                                        ];
                                        table_list_Params.columns=columns;
                                        table_list_Params.onClickRow=function(){};
                                        table_list_Params.url='/base/loginLog/json/find';
                                        $('#tableList').bootstrapTable(table_list_Params);
                                    }
                                </script>
                            </div>

                        <!-- 修改密码-->

                        <@authTag method="hasPermission" permission="account.personalInfo">
                            <div class="animation-slide-top tab-message mainContent" style="display:none;"  >
                                    <div class="col-sm-6 col-sm-offset-3 margin-vertical-35">
                                        <form id="form_changepwd"  class="form-horizontal" >
                                            <div class="form-group col-sm-offset-2">
                                                <h5 class="margin-left-10">修改密码</h5>
                                            </div>
                                            <div class="form-group">
                                                <div class="formControls col-sm-8">
                                                    <input type="password" name="oldPwd" class="form-control" placeholder="请输入原来的密码" datatype="*" nullmsg="请输入旧密码" autocomplete="off">
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="Validform_checktip"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class=" formControls col-sm-8">
                                                     <input type="password" name="newPwd" datatype="*" nullmsg="请输入新密码"  class="form-control strength-input" placeholder="请输入新密码" autocomplete="off" >
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="Validform_checktip"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="formControls col-sm-8 ">
                                                    <input type="password" name="confirm" class="form-control"  placeholder="请再次输入新密码" datatype="*" nullmsg="请再次输入新密码"   autocomplete="off">
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="Validform_checktip"></div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                 <button type="button" class="btn btn-outline btn-primary margin-left-10" onclick="savePwd()">确认修改</button>
                                            </div>
                                        </form>
                                </div>
                            </div>
                        </@authTag>
                         <!--头像设置-->
                            <div class="animation-slide-bottom tab-message mainContent" style="display:none;"  >
                                        <style type="text/css">
                                            .settingImg_item span {
                                                display: inline-block;
                                                vertical-align: bottom;
                                                margin-right: 7px;
                                            }
                                            .largeImg {
                                                width: 130px;
                                                height: 130px;
                                            }
                                            .userHead-size {
                                                text-align: center;
                                                font-size:12px;
                                                height: 22px;
                                                line-height: 22px;
                                                color: #484848;
                                            }
                                            .mediumImg {
                                                width: 70px;
                                                height: 70px;
                                            }
                                            .smallImg {
                                                width: 50px;
                                                height: 50px;
                                                /* margin-right: 20px;*/
                                            }
                                            .userHeadList-title {
                                                font-size: 12px;;
                                                color: #333;
                                                height: 30px;
                                                line-height: 30px;
                                                margin-top: 2px;
                                                padding-left: 22px;
                                                text-align: left;
                                            }
                                            .headerList{
                                                text-align: left;
                                                height: 130px;
                                                width :calc(100% - 145px);
                                            }
                                        </style>
                                        <div id="messageLists4">

                                            <div class="text-center height-150 vertical-align " >
                                                <div class="pull-left">
                                                    <p class="largeImg" style="position: relative;">
                                                        <img id="headerImage2" src="${headerPath!}" alt=""  style="width:130px;height:130px;border:2px solid #ccc;">
                                                    </p>
                                                </div>
                                                <div class="pull-right margin-left-15 headerList  settingImg_item" id="preview" >
                                                    <span class="largeImgWrap">
                                                         <span class="largeImg" style="position: relative; overflow: block;">
                                                             <img  src="${headerPath!}" alt="" style="width:130px;height:130px;" >
                                                         </span>
                                                         <p class="userHead-size">130*130</p>
                                                    </span>
                                                    <span class="mediumImgWrap">
                                                        <span class="mediumImg" style="position: relative; overflow: block;">
                                                            <img  src="${headerPath!}" alt="" style="width:70px;height:70px;" >
                                                        </span>
                                                        <p class="userHead-size">70*70</p>
                                                    </span>

                                                    <span class="smallImgWrap">
                                                          <span class="smallImg" style="position: relative; overflow: block;">
                                                               <img  src="${headerPath!}" alt="" style="width:50px;height:50px;" >
                                                          </span>
                                                          <p class="userHead-size">50*50</p>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="btn-group"  >
                                                    <!--保存用户设置的头像路径-->
                                                    <input type="hidden" id="wjlj"/>
                                                    <button type="button" class="btn btn-outline btn-success"  id="browse">选择图片</button>
                                                    <button type="button" class="btn btn-outline btn-primary" onclick="saveHeaderImage()">保存头像</button>
                                            </div>



                                            <div class="container" style="height: auto;margin-top: 12px;">
                                                <div class="row" style="height: 35px;">
                                                    <div class="col-md-6 col-xs-6 ">您可以使用：</div>
                                                    <div class="col-md-6 col-xs-6 text-right">
                                                      <div id="pagebarIndex"class="pull-right margin-left-10"></div><div id="pagebar" class="pull-right" style="display: inline-block;"></div>
                                                    </div>
                                                </div>
                                                <style type="text/css">
                                                   .container .row .imgBox{
                                                       /*border: 1px solid green;*/
                                                       height: 100%;min-width: 85px;
                                                      /* background-color: #dadada;*/
                                                       padding: 0 8px;;
                                                       cursor: pointer;
                                                   }
                                                   .container .row .imgBox img{
                                                       position: relative; width: 100%;height:100%;
                                                   }
                                                   .container .row .imgBox.selected{
                                                       border:2px solid green;
                                                   }
                                                </style>
                                                 <div class="row" id="headerList" style="height: 130px;">
                                                     <div class="col-md-2 col-xs-4 imgBox">
                                                         <img data-avatarform-elem="largeImg" src="/resources/images/portraits/1.jpg" alt="" >
                                                     </div>
                                                     <div class="col-md-2 col-xs-4 imgBox" >
                                                         <img data-avatarform-elem="largeImg" src="/resources/images/portraits/2.jpg" alt="" >
                                                     </div>
                                                     <div class="col-md-2 col-xs-4 imgBox">
                                                         <img data-avatarform-elem="largeImg" src="/resources/images/portraits/3.jpg" alt="" >
                                                     </div>
                                                     <div class="col-md-2 col-xs-4 imgBox" >
                                                         <img data-avatarform-elem="largeImg" src="/resources/images/portraits/4.jpg" alt="" >
                                                     </div>
                                                     <div class="col-md-2 col-xs-4 imgBox">
                                                         <img data-avatarform-elem="largeImg" src="/resources/images/portraits/5.jpg" alt="" >
                                                     </div>
                                                     <div class="col-md-2 col-xs-4 imgBox" >
                                                         <img data-avatarform-elem="largeImg" src="/resources/images/portraits/6.jpg" alt="" >
                                                     </div>
                                                 </div>
                                            </div>
                                            <script type="text/javascript" src="/resources/lib/plupload/plupload.full.min.js"></script>
                                            <script type="text/javascript">
                                                    var uploader = new plupload.Uploader({ //实例化一个plupload上传对象
                                                        runtimes: 'html5,silverlight,html4,flash',
                                                        browse_button : 'browse',
                                                        url : '/base/attachment/uploadSingle',
                                                        flash_swf_url : '/resources/lib/plupload/Moxie.swf',
                                                        silverlight_xap_url : '/resources/lib/plupload/Moxie.xap',
                                                        filters: {
                                                            max_file_size: '10mb', //最大上传文件大小（格式100b, 10kb, 10mb, 1gb）
                                                            mime_types : [{ title : "图片文件", extensions:"jpg,jpeg,gif,png,bmp" }]}//只允许上传图片文件
                                                    });
                                                    //绑定文件添加进队列事件
                                                    uploader.bind('FilesAdded',function(uploader,files){
                                                        uploader.start(); //开始上传
                                                    });
                                                    uploader.bind('Error',function(up, err){
                                                        alert(err.message);//上传出错的时候触发
                                                    });
                                                    uploader.bind("FileUploaded", function(up, file, res){
                                                        var json = JSON.parse(res.response);
                                                        uploadCallback(json[0]);

                                                    })
                                                    window.onload = function() {
                                                        uploader.init(); //初始化
                                                    };
                                                function uploadCallback(json){
                                                    alert(JSON.stringify(json));
                                                    $(".settingImg_item span img").attr("src",json.path);
                                                    $("#wjlj").val(json.path);
                                                }
                                            </script>
                                        </div>
                            </div>

                            <!-- 第三方帐号绑定 -->
                            <div class="animation-slide-right tab-message mainContent" style="display:none;"  >
                                <style>
                                    #messageLists5 .pull-left.pagination-detail{display: none !important;}
                                </style>
                                <div id="messageLists5" style="min-height: 100%;">
                                    <table id="tableList_bind" class="table table-striped"></table>
                                    <div class="row margin-top-15 margin-right-10">

                                      <#--<a class="btn btn-success  " style="float: right;"><i class="fa fa-plus"></i>绑定第三方账户</a> -->
                                          <div class="ui teal buttons" style="float: right;">
                                              <div class="ui button"><i class="fa fa-plus"></i>&nbsp;绑定第三方账户</div>
                                              <div class="ui floating dropdown icon button" tabindex="0">
                                                  <i class="dropdown icon"></i>
                                                  <style>
                                                      .menu .item a{text-decoration: none;color: rgba(0,0,0,.87);}
                                                  </style>
                                                  <div class="menu transition hidden" tabindex="-1">
                                                      <div class="item"><a href="/sns?t=wx"><i class="fa fa-wechat"></i>&nbsp; 绑定微信</a></div>
                                                      <div class="item"><a href="/sns?t=qq"><i class="fa fa-qq"></i> &nbsp;绑定QQ</a></div>
                                                      <div class="item"><a href="/sns?t=sina"><i class="fa fa-weibo"></i>&nbsp; 绑定微博</a></div>
                                                      <div class="item"><a href="/sns?t=baidu"><i class="fa fa-bold"></i>&nbsp; 绑定百度</a></div>
                                                      <div class="item"><a href="/sns?t=csdn"><i class="fa fa-copyright"></i>&nbsp; 绑定CSDN</a></div>

                                                      <div class="item"><a href="/sns?t=oschina"><i class="fa fa-circle-o"></i>&nbsp; 绑定OsChina</a></div>
                                                  </div>
                                              </div>
                                          </div>
                                    </div>

                                </div>
                                <script type="text/javascript">
                                    var sys_ctx="";
                                    //查询功能的标签说明
                                    var table_list_query_form_bind = {
                                    };
                                    function refreshData_bind() {
                                        $('#tableList_bind').bootstrapTable('refresh');
                                    }
                                    $(function(){
                                        sys_table_list_bind();
                                    });
                                    function sys_table_list_bind(){
                                        table_list_Params_bind=table_list_Params;
                                        $('#tableList_bind').bootstrapTable('destroy');
                                        var columns=[
                                            {field: 'login_type',align:"center",sortable:true,order:"asc",visible:true,title: '登录方式',width:"110px",formatter:function(v,row){
                                                var res="<span class=\"third-part-icon-32 qq\"></span>";
                                                if(v=='QQ'){
                                                    res="<span class=\"third-part-icon-32 qq\"></span>";
                                                }else if(v=='BAIDU'){
                                                    res="<span class=\"third-part-icon-32 baidu\"></span>";
                                                }else if(v=='OSCHINA'){
                                                    res="<span class=\"third-part-icon-32 cc\"></span>";
                                                }else if(v=='WX'){
                                                    res="<span class=\"third-part-icon-32 wx\"></span>";
                                                }else if(v=='CSDN'){
                                                    res="<span class=\"icon_csdn_32\"></span>";
                                                }else if(v=='SINA'){
                                                    res="<span class=\"third-part-icon-32 sina\"></span>";
                                                }else{
                                                    return v;
                                                }
                                                return res;
                                            }},
                                            {field: 'third_openid',align:"center",sortable:true,order:"asc",visible:true,title: 'Openid/Uid'},
                                            {field: 'bind_time',align:"center",sortable:true,order:"asc",visible:true,title: '绑定时间',width:"220px"},
                                            {field: '操作',align:"center",sortable:true,order:"asc",visible:true,title: '操作',width:"220px",formatter:function(v,row){
                                                var btn="";
                                                btn+="<button class=\"btn btn-xs  btn-danger \" onclick=\"unBind('"+row.login_type+"')\" type=\"button\"><i class=\"fa fa-unlock\"></i><span class=\"bold\">&nbsp;解绑</span></button>";
                                                return btn;
                                                }
                                            }
                                        ];
                                        table_list_Params_bind.columns=columns;
                                        table_list_Params_bind.onClickRow=function(){};
                                        table_list_Params_bind.url='/account/findThirdOauthByYhid';
                                        $('#tableList_bind').bootstrapTable(table_list_Params_bind);
                                    }
                                    function unBind(type){
                                            $.layer.confirm("您确定需要解绑该账户？",function (index) {
                                                //unBindThirdLogin
                                                sys_ajaxPost("/account/unBindThirdLogin",{type:type},function(json){
                                                    ajaxReturnMsg(json,function(){
                                                        sys_table_list_bind(); //解绑成功，刷新列表
                                                   });
                                                });
                                            })
                                    }
                                </script>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var validform;
    $(function(){
        $('.ui.dropdown').dropdown();
        validform=$("#form_changepwd").Validform({
            btnReset:"#reset",
            tiptype:2,
            postonce:true,//至提交一次
            ajaxPost:false,//ajax方式提交
            showAllError:true //默认 即逐条验证,true验证全部
        });

        //消息、日志、密码、设置之间的切换
        $("ul.tabList>li.news").click(function(){
            var i=$(this).index();
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
            $(".tab-content .mainContent").eq(i).show().siblings(".mainContent").hide();
        });

        pager();
    })
    //跳转到修改图像tab页
    function jumptHeadImage(){
        var from=3;
        $("ul.tabList>li.news").eq(from).click();

    }
    //点击选择头像
    function bindClickHeadImage(){
        $(".container .row .imgBox").click(function(){
            $(this).addClass("selected").siblings().removeClass("selected");
            var url=$(this).find("img").attr("src");
            $("#wjlj").val(url);
            //不同尺寸预览该选中图片
            $(".settingImg_item span img").attr("src",url);
        });
    }
    //保存用户头像
    function saveHeaderImage(){
        var headerPath=$("#wjlj").val();
        if(isEmpty(headerPath)){
            Fast.msg_error("请选选择用户头像！");
            return;
        }
        $.ajax({
            type:"post",
            url:'/account/saveHeader?wjlj='+headerPath,
            data:null,
            success:function(data,textStatus){
                ajaxReturnMsg(data);
                var json=typeof data=='string'?JSON.parse(data):data;
                var type=json.type;
                if(type=="success"){
                  //刷新页面现在的头像
                    $("#headerImage").attr("src",headerPath);
                    $("#headerImage2").attr("src",headerPath);
                    window.top.$("#headerImage").attr("src",headerPath);
                    $("#wjlj").val("");
                }
            }
        });
    }
    function savePwd(){
        var b=validform.check(false);
        if(!b)
        {
            return;
        }

        var params=$("#form_changepwd").serialize();
        var token= $.cookie("markbro_token");
        //alert(token);return;
       // alert(params);return;
        $.ajax({
            type:"post",
            url:'/account/changePwd?'+params,
            data:{markbro_token:token},
            success:function(data,textStatus){
                ajaxReturnMsg(data);
                var json=typeof data=='string'?JSON.parse(data):data;
                var type=json.type;
               // alert(type);
                if(type=="success"){
                    listMessage();
                    layer.alert("密码修改成功！请退出重新登录！");
                    //window.top.location.href="/login";
                }
            }
        });
    }
    //初始化分页条
    var pageNo=1;
    var pageSize=6;
    var imgDatas=[
        "/resources/images/portraits/1.jpg",
        "/resources/images/portraits/2.jpg",
        "/resources/images/portraits/3.jpg",
        "/resources/images/portraits/4.jpg",
        "/resources/images/portraits/5.jpg",
        "/resources/images/portraits/1.jpg",
        "/resources/images/headicons/boy-1.png",
        "/resources/images/headicons/boy-2.png",
        "/resources/images/headicons/boy-3.png",
        "/resources/images/headicons/girl-2.png",
        "/resources/images/headicons/girl-1.png",
        "/resources/images/headicons/girl-3.png",
        "/resources/images/headicons/cartoon/1.jpg",
        "/resources/images/headicons/cartoon/2.jpg",
        "/resources/images/headicons/cartoon/3.jpg",
        "/resources/images/headicons/cartoon/4.jpg",
        "/resources/images/headicons/cartoon/5.jpg",
        "/resources/images/headicons/cartoon/6.jpg",
        "/resources/images/headicons/cartoon/7.jpg",
        "/resources/images/headicons/cartoon/8.jpg",
        "/resources/images/headicons/cartoon/9.jpg",
        "/resources/images/headicons/cartoon/10.jpg",
        "/resources/images/headicons/cartoon/11.jpg",
        "/resources/images/headicons/cartoon/12.jpg",
        "/resources/images/headicons/cartoon/13.jpg",
        "/resources/images/headicons/cartoon/14.jpg",
        "/resources/images/headicons/cartoon/15.jpg",
        "/resources/images/headicons/cartoon/16.jpg"
    ];
    var totalPages=Math.floor((imgDatas.length+pageSize-1)/pageSize);

    function pager(){
        var $pagebar=$("#pagebar");
        laypage({
            cont:$pagebar, //容器。值支持id名、原生dom对象，jquery对象,
            pages: totalPages, //总页数
            curr:pageNo,
            jump:function(e,first){
                if(!first){ //一定要加此判断，否则初始时会无限刷新
                    pageNo=e.curr;
                    $("#headerList").html(headerList(pageNo));
                    bindClickHeadImage();
                    $("#pagebarIndex").html(pageNo+"/"+totalPages);
                }
            },
            groups: 0,
            skin: 'molv'  , //皮肤
            first: false, //若不显示，设置false即可
            last: false, //若不显示，设置false即可
            prev: '<em style="font-weight:bold;color:#369b6f;"><</em>', //若不显示，设置false即可
            next: '<em style="font-weight:bold;color:#369b6f;">></em>' //若不显示，设置false即可
        });
        bindClickHeadImage();
        $("#pagebarIndex").html(pageNo+"/"+totalPages);
    }

    //模拟列表
    var headerList = function( pageNo){
        var arr = [],thisData = imgDatas.concat().splice(pageNo*pageSize-pageSize, pageSize);
        $.each(thisData, function(index, item){
            arr.push('<div class="col-md-2 col-xs-4 imgBox"><img   src="'+ item +'" alt="" ></div>');
        });
        return arr.join('');
    };
</script>

</body>

</html>
