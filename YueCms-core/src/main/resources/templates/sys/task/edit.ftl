
<!DOCTYPE html>
<html>
<head>
    <title>更新调度任务</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link rel="stylesheet" href="/resources/lib/simple-switch/jquery.simple-switch.css">

</head>
<body  class="body-bg-edit">
<div class="content-wrapper  animated fadeInRight">
    <div class="container-fluid">
        <form action="" id="form_show" method="post" class="form-horizontal" role="form">
            <h2 class="text-center"></h2>
            <input type="hidden" value="${task.id!}" id="id" name="id"/>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="job_group">任务分组:</label>
                <div class="col-sm-6 formControls">
                    <span class="select-box form-control">
                        <select  value="${task.job_group!}" datatype="*" nullmsg="请输入任务分组"   class="select"  id="job_group" name="job_group">
                            <option value="" >-请选择-</option>
                            <option value="sys">系统任务</option>
                            <option value="user">用户自定义</option>
                            <option value="spider">爬虫任务</option>
                            <option value="blog">博客任务</option>
                            <option value="static">静态化任务</option>
                        </select>
                    </span>
                </div>
                <div class="col-sm-3">
                    <div class="Validform_checktip"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">任务名：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="job_name" name="job_name" placeholder="任务名" value="${task.job_name!}" class="form-control" datatype="*" nullmsg="请输入任务名" />
                </div>
                <div class="col-sm-3">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">cron表达式：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="cron_expression" name="cron_expression" placeholder="cron表达式" value="${task.cron_expression!}" class="form-control" datatype="*" nullmsg="请输入cron表达式" />
                </div>
                <div class="col-sm-3">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">任务执行类：</label>
                <div class="col-sm-6 formControls">
                    <input type="text" id="bean_class" name="bean_class" placeholder="任务执行类,包名+类名" value="${task.bean_class!}" class="form-control" datatype="*" nullmsg="请输入任务执行时调用哪个类的方法 包名+类名" />
                </div>
                <div class="col-sm-3">
                    <div class="Validform_checktip"></div>
                </div>
            </div>



            <div class="form-group">
                <label class="col-sm-3 control-label">启用状态：</label>
                <div class="col-sm-6 formControls">
                    <input type="hidden"  id="available" name="available" value="${task.available!}" class="form-control"  />

                    <#if task.available?? && task.available==1>
                        <div id="switch" class="simple-switch active">
                            <input type="checkbox" checked/>
                            <span class="switch-handler"></span>
                        </div>
                    <#else>
                        <div id="switch" class="simple-switch">
                            <input type="checkbox"/>
                            <span class="switch-handler"></span>
                        </div>
                    </#if>
                </div>
                <div class="col-sm-3">
                    <div class="Validform_checktip"></div>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-3 control-label">任务描述：</label>
                <div class="col-sm-6 formControls">
                <#--<input type="text" id="description" name="description" placeholder="任务描述" value="${task.description!}" class="form-control" datatype="*" nullmsg="请输入任务描述" />-->
                    <textarea rows="4" class="form-control" name="description"  placeholder="描述" id="description" datatype="*"  nullmsg="请输入任务描述" >${task.description!}</textarea>
                </div>
                <div class="col-sm-3">
                    <div class="Validform_checktip"></div>
                </div>
            </div>

            <!--<div class="form-group">
                <div class=" col-sm-10 col-sm-offset-2">
                    <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
                </div>
            </div>-->
        </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>


<script type="text/javascript" src="/resources/lib/simple-switch/jquery.simple-switch.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript">
    var sys_ctx="";
    var validform;
    function save(){
        var b=validform.check(false);
        if(!b)
        {
            return;
        }
        var params=$("#form_show").serialize();
        $.ajax({
            type:"post",
            url:'/sys/task/json/save?'+params,
            data:null,
            success:function(json,textStatus){
                ajaxReturnMsg(json);
                setTimeout(function(){
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                },1000);
            }
        });
    }
    $(function(){
        var job_group='${task.job_group}';
        $("#job_group").val(job_group);

        $('#switch').on('switch-change', function(e){
                    var v=$('#switch').simpleSwitch('state');
                    if(v==true||v=='true'){
                        $("#available").val(1);
                    }else{
                        $("#available").val(0);
                    }
                }
        );
        validform=$("#form_show").Validform({
            btnReset:"#reset",
            tiptype:2,
            postonce:true,//至提交一次
            ajaxPost:false,//ajax方式提交
            showAllError:true //默认 即逐条验证,true验证全部
        });
    })
</script>

</body>
</html>
