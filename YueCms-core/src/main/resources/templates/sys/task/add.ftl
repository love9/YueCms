<!DOCTYPE html>
<html>
<head>
  <title>新增调度任务</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
	<meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link rel="stylesheet" href="/resources/lib/simple-switch/jquery.simple-switch.css">


</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center"></h2>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="job_group">任务分组:</label>
                <div class="col-sm-6 formControls">
                    <span class="select-box form-control">
                        <select  value="" datatype="*" nullmsg="请输入任务分组"   class="select"  id="job_group" name="job_group">
                            <option value="">-请选择-</option>
                            <option value="sys">系统任务</option>
                            <option value="user">用户自定义</option>
                            <option value="spider">爬虫任务</option>
                            <option value="blog">博客任务</option>
                            <option value="static">静态化任务</option>
                        </select>
                    </span>
                </div>
                <div class="col-sm-3">
                    <div class="Validform_checktip"></div>
                </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">任务名：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="job_name" name="job_name" placeholder="任务名"  class="form-control" datatype="*" nullmsg="请输入任务名" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>

         <div class="form-group">
            <label class="col-sm-3 control-label">cron表达式：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="cron_expression" name="cron_expression" placeholder="cron表达式"  class="form-control" datatype="*" nullmsg="请输入cron表达式" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>

         <div class="form-group">
            <label class="col-sm-3 control-label">任务执行类：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="bean_class" name="bean_class" placeholder="任务执行类,包名+类名"  class="form-control" datatype="*" nullmsg="请输入任务执行时调用哪个类的方法 包名+类名" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>


         <div class="form-group">
            <label class="col-sm-3 control-label">启用状态：</label>
            <div class="col-sm-6 formControls">
                <input type="hidden"  id="available" name="available" value="0" class="form-control"  />
                    <div id="switch" class="simple-switch">
                        <input type="checkbox"/>
                        <span class="switch-handler"></span>
                    </div>
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">任务描述：</label>
            <div class="col-sm-6 formControls">
                <#--<input type="text" id="description" name="description" placeholder="任务描述"  class="form-control" datatype="*" nullmsg="请输入任务描述" />-->
                <textarea rows="4" class="form-control" name="description"  placeholder="任务描述" id="description" datatype="*"  nullmsg="请输入任务描述"></textarea>
            </div>
            <div class="col-sm-3">
                <div class="Validform_checktip"></div>
            </div>
        </div>

		<!--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-2">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>

<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/simple-switch/jquery.simple-switch.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript">
    var sys_ctx="";
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/sys/task/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}

	$(function(){

        $('#switch').on('switch-change', function(e){
           var v=$('#switch').simpleSwitch('state');
           if(v==true||v=='true'){
               $("#available").val(1);
           }else{
               $("#available").val(0);
           }
         }
        );
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
