<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>调度任务管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">


</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span>调度任务管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">

                <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <!--<a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>-->
                <a onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                <a onclick="queryAllJobs();" class="btn btn-outline btn-primary" ><i class="fa fa-search"></i>&nbsp;查看所有任务</a>
                <a onclick="queryAllExcuteJobs();" class="btn btn-outline btn-primary" ><i class="fa fa-search"></i>&nbsp;查看执行中的任务</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>

<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<link rel="stylesheet" href="/resources/lib/simple-switch/jquery.simple-switch.css">
<script type="text/javascript" src="/resources/lib/simple-switch/jquery.simple-switch.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'job_group',align:"center",sortable:true,order:"asc",visible:true,title: '任务分组',width:"120px",formatter:function(v,row){
                    var html="";
                    if(v=='sys'){
                        html="<font color='red'>系统任务</font>";
                    }else if(v=='user'){
                        html="<font color='#556b2f'>用户自定义</font>";
                    }else if(v=='spider'){
                        html="<font color='blue'>爬虫任务</font>";
                    }else if(v=='static'){
                        html="<font color='green'>静态化任务</font>";
                    }else if(v=='blog'){
                        html="<font color='orange'>博客任务</font>";
                    }else{
                        html="<font color='green'>未知</font>";
                    }
                    return html;
                }
             },
            {field: 'job_name',align:"center",sortable:true,order:"asc",visible:true,title: '任务名',width:"120px"},
            {field: 'description',align:"center",sortable:true,order:"asc",visible:false,title: '任务描述'},
            {field: 'cron_expression',align:"center",sortable:true,order:"asc",visible:true,title: 'cron表达式',width:"220px"},
            {field: 'available',align:"center",sortable:true,order:"asc",visible:true,title: '启用状态',width:"120px",formatter:function(v,row){
                var html="<div class=\"btn-group\">";
                if(v==1|v=='1'){
                    html+=' <div id="switch_'+row.id+'" data-id='+row.id+' onclick="switchChange(this)" style="display: block;" class="simple-switch small active">';
                }else{
                    html+=' <div id="switch_'+row.id+'" data-id='+row.id+'  onclick="switchChange(this)" style="display: block;" class="simple-switch small ">';
                }
                    html+='<input type="checkbox" />';
                    html+=' <span class="switch-handler"></span>';
                    html+=' </div>';
                    html+="</div>";
                    return html;

                }
            }, {field: 'job_status',align:"center",sortable:true,order:"asc",visible:true,title: '任务状态',width:"120px",formatter:function(v,row){
                    var html="";
                    if(v==0|v=='0'){
                        html="<font color='gray'>未运行</font>";
                    }else if(v==1|v=='1'){
                        html="<font color='green'>运行中</font>";
                    }else if(v==2|v=='2'){
                        html="<font color='blue'>暂停</font>";
                    }else if(v==3|v=='3'){
                        html="<font color='green'>结束</font>";
                    }else if(v==3|v=='3'){
                        html="<font color='green'>结束</font>";
                    }else if(v==4|v=='4'){
                        html="<font color='red'>错误</font>";
                    }else if(v==5|v=='5'){
                        html="<font color='red'>阻塞</font>";
                    }
                    else{
                        html="<font color='green'>未知</font>";
                    }
                    return html;
                    }
            },
            {field: 'bean_class',align:"center",sortable:true,order:"asc",visible:true,title: '执行方法'},
            {field: 'operate', align:"center", title: '操作', width: '260px',
                formatter: function(value, row, index){
                    var btns="<div class=\"btn-group\">";
                    if(row.job_status==1){
                        btns+="<button class=\"btn btn-xs btn-outline btn-second \" onclick=\"runOrPause('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pause\"></i><span class=\"bold\">&nbsp;&nbsp;暂停</span></button>";
                        btns+="<button class=\"btn btn-xs btn-outline btn-success \" onclick=\"runOne('"+row.id+"')\" type=\"button\"><i class=\"fa fa-pause\"></i><span class=\"bold\">&nbsp;&nbsp;执行一次</span></button>";
                    }else if(row.job_status==2){
                        btns+="<button class=\"btn btn-xs btn-outline btn-success \" onclick=\"runOrPause('"+row.id+"')\" type=\"button\"><i class=\"fa fa-play\"></i><span class=\"bold\">&nbsp;&nbsp;运行</span></button>";
                    }else{

                    }
                    btns+="<button class=\"btn btn-xs btn-outline btn-primary \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-edit\"></i><span class=\"bold\">&nbsp;&nbsp;编辑</span></button>";
                    btns+="<button class=\"btn btn-xs btn-outline btn-danger \" onclick=\"deleteRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-remove\"></i><span class=\"bold\">&nbsp;&nbsp;删除</span></button>";
                    btns+="</div>";
                    return btns;
                }
            },

            {field: 'create_user',align:"center",sortable:true,order:"asc",visible:false,title: '创建者'},
            {field: 'create_time',align:"center",sortable:true,order:"asc",visible:false,title: '创建时间'},
            {field: 'update_user',align:"center",sortable:true,order:"asc",visible:false,title: '更新者'},
            {field: 'update_time',align:"center",sortable:true,order:"asc",visible:false,title: '更新时间'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.pageSize=15;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/sys/task/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
        $('#switch').simpleSwitch('toggle');
    }

    function runOne(id){

        $.ajax({
            type:"post",
            url:'/sys/task/json/runOne',
            data:{id:id},
            success:function(data,textStatus){
                var json=typeof data=='string'?JSON.parse(data):data;
                Fast.msg(json.content);
                ajaxReturnMsg(data,function(){
                    setTimeout(function(){
                        if(id==1){
                            sys_table_list();
                        }
                    },500);

                });
            }
        });
    }

    function runOrPause(id){

        $.ajax({
            type:"post",
            url:'/sys/task/json/runOrPause',
            data:{id:id},
            success:function(data,textStatus){
                var json=typeof data=='string'?JSON.parse(data):data;
                Fast.msg(json.content);
                ajaxReturnMsg(data,function(){
                    setTimeout(function(){
                        $('#tableList').bootstrapTable('refresh');
                    },500);

                });
            }
        });
    }

    function switchChange(obj){
        var $this=$(obj);
        var id=$this.data("id");

            $.ajax({
                type:"post",
                url:'/sys/task/json/updateAvailable',
                data:{id:id},
                success:function(data,textStatus){
                    ajaxReturnMsg(data,function(){
                        sys_table_list();
                        return;
                        if($this.hasClass("active")){//关闭
                            $("#switch_"+id).simpleSwitch('toggle', false);
                        }else{//打开
                            $("#switch_"+id).simpleSwitch('toggle', true);
                        }
                    });
                }
            });
    }
    var editRow=function(id){

        $.layer.open_page("编辑调度任务",sys_ctx+"/sys/task/edit?id="+id,{
            w:"800px",
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增调度任务",sys_ctx+"/sys/task/add",{
            w:"800px",
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function deleteRow(id){
        if(isNotEmpty(id)){
            $.layer.confirm("确定删除该任务吗？", function(){
                $.ajax({
                    type:"post",
                    url:'/sys/task/json/delete/'+id,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data,function(){
                            $('#tableList').bootstrapTable('refresh');
                        });
                    }

                });
            });
        }
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            $.layer.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/sys/task/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data,function(){
                                                  //sys_table_list();
                         $('#tableList').bootstrapTable('refresh');
                                                 });
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    var queryAllJobs=function(){
        $.ajax({
            type:"post",
            url:'/sys/task/json/queryAllJob',
            data:null,
            success:function(data,textStatus){
                alert(data);
            }
        });
    }
    var queryAllExcuteJobs = function(){
        $.ajax({
            type:"post",
            url:'/sys/task/json/queryRunJon',
            data:null,
            success:function(data,textStatus){
                alert(data);
            }
        });
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
