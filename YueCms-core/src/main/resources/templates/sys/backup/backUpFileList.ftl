<!DOCTYPE html>
<html class="no-js" lang="zh_CN">

<head>
    <title>查看数据库备份文件</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">

    <style>
        .backup-table {
            padding-left: 50px !important;
        }
    </style>
</head>
<body class="body-bg main-bg">

<div class="wrapper animated fadeInRight">
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript">
    var sys_ctx="";
    var table='${table!}';
    $(function(){
        var data=[];
        //初始化将测试集包含的用例存在数组里面
        <#if fileList??>
            <#list fileList as item>
                var t={name:"${item.name}"};
                data.push(t);
            </#list>
        </#if>

        $('#tableList').bootstrapTable('destroy');
        var columns=[
            {field: 'name', align:"left", visible:true, title: '名称'},
            {field: '操作', align:"left", title: '操作',formatter:function(v,row){
                var btns="";
                btns+="<button class=\"btn btn-xs btn-danger  \" onclick=\"deleteFile('"+row.name+"',this)\" type=\"button\"><i class=\"fa fa-remove\"></i><span class=\"bold\">&nbsp;&nbsp;删除备份</span></button>";
                return btns;
             }
            },
        ];
        table_list_Params.columns=columns;
        table_list_Params.data=data;
        table_list_Params.onClickRow=function(){};
        table_list_Params.pagination=false;
        $('#tableList').bootstrapTable(table_list_Params);

    })


    function deleteFile(name,obj) {
        $.layer.confirm("确定要删除备份文件吗？", function () {
            $.layer.loading();
            sys_ajaxPost("/sys/backup/delete", {table: table,name:name}, function (data) {
                $.layer.closeLoading();
                ajaxReturnMsg(data,function(){
                    $(obj).parents("tr").remove();
                });
            });
        });
    }


</script>

</body>
</html>