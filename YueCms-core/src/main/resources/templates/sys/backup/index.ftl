<!DOCTYPE html>
<html class="no-js" lang="zh_CN">

<head>
    <title>数据库备份</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">

    <#import "/base/util/macro.ftl" as macro>
    <style>
        .backup-table {
            padding-left: 50px !important;
        }
    </style>
</head>



<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span>系统缓存</nav>
<@macro.myMsg msgObj=msgObj ui=""/>
<div class="wrapper animated fadeInRight">
    <div class="row">
        <h4>&nbsp;&nbsp;&nbsp;数据库名：<strong>${catalog!}</strong>， 共<strong>${count!}</strong>张表
            <a class="btn btn-success btn-sm" style="display: inline-block;float:right;margin-right: 10px;margin-bottom: 3px;"><i class="fa fa-gear"></i>设置备份程序路径</a>
        </h4>
    </div>
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>提示：</strong> 数据库用户需要配置 `File` 和 `Lock Tables` 权限
    </div>

    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript">
    var sys_ctx="";
    $(function(){
        var data=[];
        //初始化将测试集包含的用例存在数组里面
        <#if tableList??>
            <#list tableList as item>
                var t={name:"${item.name}"};
                data.push(t);
            </#list>
        </#if>

        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'name', align:"left", visible:true, title: '名称'},
            {field: '操作', align:"left", title: '操作',formatter:function(v,row){
                var btns="";
                    if(row.name.indexOf("数据库") != -1){
                        btns+="<button class=\"btn btn-xs  btn-warning \" onclick=\"backupDb()\" type=\"button\"><i class=\"fa fa-edit\"></i><span class=\"bold\">&nbsp;&nbsp;整库备份</span></button>";
                        btns+="&nbsp;&nbsp;";
                        btns+="<button class=\"btn btn-xs  btn-warning \" onclick=\"viewDbFile()\" type=\"button\"><i class=\"fa fa-eye\"></i><span class=\"bold\">&nbsp;&nbsp;查看备份文件</span></button>";
                    }else{
                        btns+="<button class=\"btn btn-xs   \" onclick=\"backupTb('"+row.name+"')\" type=\"button\"><i class=\"fa fa-edit\"></i><span class=\"bold\">&nbsp;&nbsp;立即备份</span></button>";
                        btns+="&nbsp;&nbsp;";
                        btns+="<button class=\"btn btn-xs  \" onclick=\"viewTbFile('"+row.name+"')\" type=\"button\"><i class=\"fa fa-eye\"></i><span class=\"bold\">&nbsp;&nbsp;查看备份文件</span></button>";
                    }

                return btns;
             }
            },
        ];
        table_list_Params.columns=columns;
        table_list_Params.data=data;
        table_list_Params.onClickRow=function(){};
        table_list_Params.pagination=false;

        $('#tableList').bootstrapTable(table_list_Params);

    })
    function backupDb(){
        doBack("")
    }
    function backupTb(name){
        doBack(name);
    }
    function viewDbFile(){
        $.layer.open_page("查看备份文件",sys_ctx+"/sys/backup/backUpFileList?table=",{
            w:"800px"
        });
    }
    function viewTbFile(table){
        $.layer.open_page("查看备份文件",sys_ctx+"/sys/backup/backUpFileList?table="+table,{
            w:"800px"
        });
    }


    function doBack(table) {
        $.layer.confirm("确定要进行备份操作吗？", function () {
            $.layer.loading();
            sys_ajaxPost("/sys/backup/backup", {table: table}, function (data) {
                $.layer.closeLoading();
                ajaxReturnMsg(data);
            });
        });
    }

</script>

</body>
</html>