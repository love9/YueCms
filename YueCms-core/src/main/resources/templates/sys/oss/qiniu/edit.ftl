<!DOCTYPE html>
<html>
<head>
  <title>编辑七牛文件资源</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/css/web-icons/web-icons.css" rel="stylesheet">


</head>
<body  class="body-bg-edit">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
    <input type="hidden" value="${qiniuFile.id!}" id="id" name="id"/>
		<h2 class="text-center">编辑七牛文件资源</h2>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件名称：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="name" name="name" placeholder="文件名称" value="${qiniuFile.name!}" class="form-control" datatype="*" nullmsg="请输入文件名称" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">七牛key：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="key" name="key" placeholder="上传到七牛的key" value="${qiniuFile.qiniu_key!}" class="form-control" datatype="*" nullmsg="七牛key" />
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">选择本地文件：</label>
              <div class="col-sm-6 formControls">

                     <div class="input-group input-group-file">
                         <input type="text" id="file_show" class="form-control" value="${qiniuFile.localpath!}"  readonly="" placeholder="请选择本地文件" ignore="ignore" datatype="*" nullmsg="请选择本地文件" >
                         <span class="input-group-btn">
                             <span class="btn btn-success btn-file">
                                 <i class="icon wb-upload" aria-hidden="true"></i>
                                 <input type="file" name="file" id="file"  onchange="show()">
                             </span>
                         </span>
                     </div>
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件网络路径：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="url" name="url" placeholder="文件网络路径" value="${qiniuFile.url!}" class="form-control" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">七牛外链：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="qiniu_url" name="qiniu_url"  value="${qiniuFile.qiniu_url!}" class="form-control" readonly />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group" style="display: none;">
              <label class="col-sm-2 control-label">文件后缀：</label>
              <div class="col-sm-6 formControls">
              <input type="text" id="suffix" name="suffix" placeholder="文件后缀" value="${qiniuFile.suffix!}" class="form-control"  />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件类型：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="mimeType" name="mimeType" placeholder="文件类型" value="${qiniuFile.mimeType!}" class="form-control" readonly />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件大小：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="fsize" name="fsize" placeholder="文件大小" value="${qiniuFile.fsize!}" class="form-control" readonly />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">上传时间：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="putTime" name="putTime" placeholder="上传时间" value="${qiniuFile.putTime!}" class="form-control" readonly />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

        <div class="form-group">
              <label class="col-sm-2 control-label">七牛hash：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="hash" name="hash" placeholder="七牛返回的hash" value="${qiniuFile.qiniu_hash!}" class="form-control" readonly />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件状态：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="status" name="status" placeholder="文件状态" value="${qiniuFile.status!}" class="form-control" readonly />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group" style="display: none">
              <label class="col-sm-2 control-label">资源类型引用cms_resourcetype的ID：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="resourcetype" name="resourcetype" placeholder="资源类型引用cms_resourcetype的ID" value="${qiniuFile.resourcetype!}" class="form-control"   nullmsg="请输入资源类型引用cms_resourcetype的ID" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

		<#--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-2">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript">
    var sys_ctx="";
    function show(){
        document.getElementById("file_show").value = document.getElementById("file").value;
        $("#file_show").blur();
        checkFile();
    }
    //本地文件和网络路径文件两者选一
    function checkFile(){
        if(isEmpty($("#file_show").val())){
            $("#url").removeAttr("ignore");
        }else{
            $("#url").attr("ignore","ignore");
        }
    }
	var validform;
	function save(){
        checkFile();
        var b=validform.check(false);
        if(!b)
        {
            return;
        }
        //var params=$("#form_show").serialize();
        var formData = new FormData($("#form_show")[0]);
        //ajax上传含有文件的表单
        $.ajax({
            async : false,
            cache : false,
            contentType: false, //必须
            processData: false, //必须
            dataType : 'json',
            type:"post",
            url : '/sys/oss/qiniu/json/save',
            data:formData,
            success:function(json,textStatus){
                ajaxReturnMsg(json);
                setTimeout(function(){
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                },1000);
            }
        });
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		  btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
