<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>七牛文件资源管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">



</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> oss <span class="c-gray en">&gt;</span>七牛文件资源管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>

                <a onclick="downloadFromQiniu();" class="btn btn-outline btn-warning" ><i class="fa fa-download"></i>&nbsp;从七牛同步数据</a>
                    <input type="text" class="form-control" id="cx_name" name="cx_name" placeholder="搜索的文件名称">
                    <a onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
<script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var downloadFromQiniu=function(){
        var prefix=$("#cx_name").val();
        sys_ajaxPost("/sys/oss/qiniu/json/downloadFromQiniu",{prefix:prefix},function(json){
            ajaxReturnMsg(json,null,function(){
                query('form_query');
            });
        })
    }
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
        cx_name:''
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'name',align:"left",sortable:true,order:"asc",visible:true,title: '文件名称',width:"160px"},
            {field: 'localpath',align:"center",sortable:true,order:"asc",visible:false,title: '本地路径'},
            {field: 'url',align:"center",sortable:true,order:"asc",visible:false,title: '文件网络路径'},
            {field: 'qiniu_url',align:"left",sortable:true,order:"asc",visible:true,title: '外链路径'},
            {field: 'suffix',align:"center",sortable:true,order:"asc",visible:false,title: '后缀',width:"80px"},
            {field: 'mimeTypeName',align:"center",sortable:true,order:"asc",visible:true,title: '类型',width:"80px"
            },
            {field: 'fsize',align:"center",sortable:true,order:"asc",visible:true,title: '大小',width:"100px",formatter:function(v){
               return renderSize(v);
            }},
            {field: 'qiniu_key',align:"center",sortable:true,order:"asc",visible:true,title: '七牛Key',width:"120px"},
            {field: 'qiniu_hash',align:"center",sortable:true,order:"asc",visible:false,title: '七牛Hash',width:"120px"},
            {field: 'putTime',align:"center",sortable:true,order:"asc",visible:true,title: '同步时间',width:"175px"},
            {field: 'status',align:"center",sortable:true,order:"asc",visible:true,title: '状态',width:"100px",formatter: function(value, row, index){
                    if(value=='1'||value==1){
                        return "<font color='green'>已同步</font>";
                    }else if(value=='0'||value==0){
                        return "<font color='gray'>未同步</font>";
                    }else if(value=='2'||value==2){
                        return "<font color='red'>同步失败</font>";
                    }else if(value=='-1'||value==-1){
                        return "<font color='orange'>文件不存在</font>";
                    }else{
                        return value;
                    }
                }
            },
            {field: 'resourcetype',align:"center",sortable:true,order:"asc",visible:false,title: '资源类型'},
            {field: 'operate', align:"center", title: '操作',width:"255px",formatter: function(value, row, index){
                var btns="<div class=\"btn-group\">";
                btns+="<button class=\"btn btn-sm btn-outline btn-primary \" onclick=\"editRow('"+row.id+"')\" type=\"button\"><i class=\"fa fa-edit\"></i><span class=\"bold\">编辑</span></button>";
                if(!isEmpty(row.mimeTypeName)&&row.mimeTypeName.indexOf('图片')>=0){
                    btns+="<button class=\"btn btn-sm btn-outline btn-success \" onclick=\"preview('"+row.qiniu_url+"')\" type=\"button\"><i class=\"fa fa-eye\"></i><span class=\"bold\">预览</span></button>";
                }else{
                    btns+="<button class=\"btn btn-sm btn-outline btn-default \" onclick=\"canNotPreview()\" type=\"button\"><i class=\"fa fa-eye\"></i><span class=\"bold\">预览</span></button>";
                }
                btns+="<button class=\"btn btn-sm btn-outline btn-danger \" onclick=\"reUpload('"+row.id+"')\" type=\"button\"><i class=\"fa fa-upload\"></i><span class=\"bold\">覆盖上传</span></button>";
                btns+="</div>";
                return btns;
            }}
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/sys/oss/qiniu/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }
    var reUpload=function(id){
        sys_ajaxPost("/sys/oss/qiniu/json/qiniuUpload",{ids:id},function(data){
            var json=typeof data=='string'?JSON.parse(data):data;
            var type=json.type;
            if(type=="success"){
                Fast.msg_success(json.content);
                $('#tableList').bootstrapTable('refresh');
            }else{
                Fast.msg_error(json.content);
            }
        })
    }
    var editRow=function(id){
        $.layer.open_page("编辑七牛文件资源",sys_ctx+"/sys/oss/qiniu/edit?id="+id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    var canNotPreview=function(){
        Fast.msg_warning("目前只支持预览图片!");
    }
    var preview=function(v){
        var b=false;
        if(isNotEmpty(v)){
            b=Fast.isExist(v);
        }
        if(b){
            imgshow("#outerdiv", "#innerdiv", "#bigimg", v);
        }else{
            Fast.msg_error("图片不存在!");
        }
    }
    function addNew(){
        $.layer.open_page("新增七牛文件资源",sys_ctx+"/sys/oss/qiniu/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/sys/oss/qiniu/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据!");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>


<div id="outerdiv" style="position: fixed; top: 0px; left: 0px; background: rgba(0, 0, 0, 0.7); z-index: 2; width: 100%; height: 100%; display: none;">
    <div id="innerdiv" style="position: absolute; top: 80.8px; left: 446.079px;">
        <img id="bigimg" style="border: 5px solid rgb(255, 255, 255); width: 969.842px;" src="">
    </div>
</div>
<script type="text/javascript">
    $(function(){
       /* $("article img,.articleDetail img").click(function(){
            var _this = $(this);
            imgshow("#outerdiv", "#innerdiv", "#bigimg", url);
        });*/
    });

    function imgshow(outerdiv, innerdiv, bigimg, url){
       // var src = _this.attr("src");
        $(bigimg).attr("src", url);

        $("<img/>").attr("src", url).load(function(){
            var windowW = $(window).width();
            var windowH = $(window).height();
            var realWidth = this.width;
            var realHeight = this.height;
            var imgWidth, imgHeight;
            var scale = 0.8;

            if(realHeight>windowH*scale) {
                imgHeight = windowH*scale;
                imgWidth = imgHeight/realHeight*realWidth;
                if(imgWidth>windowW*scale) {
                    imgWidth = windowW*scale;
                }
            } else if(realWidth>windowW*scale) {
                imgWidth = windowW*scale;
                imgHeight = imgWidth/realWidth*realHeight;
            } else {
                imgWidth = realWidth;
                imgHeight = realHeight;
            }
            $(bigimg).css("width",imgWidth);

            var w = (windowW-imgWidth)/2;
            var h = (windowH-imgHeight)/2;
            $(innerdiv).css({"top":h, "left":w});
            $(outerdiv).fadeIn("fast");
        });

        $(outerdiv).click(function(){
            $(this).fadeOut("fast");
        });
    }
</script>

</body></html>
