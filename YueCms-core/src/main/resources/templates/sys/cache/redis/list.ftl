<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>redis缓存列表</title>
    <link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">


</head>
<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span>redis缓存</nav>
<div class="wrapper animated fadeInRight">
    <!--工具条-->
    <div class="cl toolbar">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">

            <input type="text" id="keyContent" name="keyContent" placeholder="key" class="form-control" />
            <div class="btn-group">
                <a  onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>
</div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" >
    var sys_ctx="";
    var validform;
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
        keyContent:''
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'key', align:"left", sortable:true, order:"asc",width:"350px", visible:true, title: '键'},
            {field: 'type', align:"left", sortable:true, order:"asc",width:"", visible:true, title: '类型'},
            {field: 'ttl', align:"center", sortable:true, order:"asc",width:"190px", visible:true, title: 'TTL'},

            {field: 'operate', align:"center", title: '操作', width: '150px',
                formatter: function(value, row, index){
                    var btn="<div class='btn btn-group'>";
                    btn+='<button class="btn btn-xs btn-outline btn-primary " onclick="viewInfo(\''+row.key+'\')" type="button"><i class="fa fa-search"></i><span class="bold">&nbsp;&nbsp;查看</span></button>';
                    btn+="<button class=\"btn btn-xs btn-outline btn-danger \" onclick=\"deleteCache('"+row.key+"')\" type=\"button\"><i class=\"fa fa-remove\"></i><span class=\"bold\">&nbsp;&nbsp;删除</span></button>";
                    btn+="</div>";
                    return btn;
                }
            }
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/sys/cache/redis/json/getCacheInfo'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var deleteCache=function(key){
        key=key.replace("#","$");

        var url="/sys/cache/redis/json/removeCacheByKey?key="+key
        sys_ajaxPost(url,null,function(json){
            ajaxReturnMsg(json,function(){
                sys_table_list();
            })
        })
    }
    function viewInfo(key){
        key=key.replace("#","$");
        var url="/sys/cache/redis/viewCacheInfoByKey?key="+key;
        $.layer.open_page("查看详细信息",url,{
            btn: ['<i class="fa fa-close"></i> 关闭'],
            yes:function(index){
                layer.close(index);
            },
            cancel: function(index) {
                return true;
            }
        });
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }

</script>
</body></html>
