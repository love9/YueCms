<!DOCTYPE html>
<html>
<head>
  <title>查看缓存详细信息</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center"></h2>

        <div class="form-group">
            <label class="col-sm-3 control-label">key：</label>
            <div class="col-sm-6 formControls">
                <input type="text"   value="${info.rows[0].key}" class="form-control"   />
            </div>

         </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">value：</label>
            <div class="col-sm-6 formControls">
                <textarea type="text" rows="10" class="form-control"   >${info.rows[0].value}</textarea>
            </div>
        </div>




    </form></div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

</body>
</html>
