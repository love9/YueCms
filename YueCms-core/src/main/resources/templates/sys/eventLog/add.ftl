<!DOCTYPE html>
<html>
<head>
  <title>新增系统事件日志</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
	<meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center"></h2>

         <div class="form-group">
            <label class="col-sm-3 control-label">系统事件：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="event" name="event" placeholder="系统事件" value="" class="form-control" datatype="*" nullmsg="请输入系统事件" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">事件名称：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="eventName" name="eventName" placeholder="事件名称" value="" class="form-control" datatype="*" nullmsg="请输入事件名称" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">事件来源：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="source" name="source" placeholder="事件来源" value="" class="form-control" datatype="*" nullmsg="请输入事件来源" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">参数：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="params" name="params" placeholder="参数" value="" class="form-control" datatype="*" nullmsg="请输入参数" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">发生时间：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="execTime" name="execTime" placeholder="发生时间" value="" class="form-control" datatype="*" nullmsg="请输入发生时间" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
      </div>
    </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript">
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/sys/eventLog/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
