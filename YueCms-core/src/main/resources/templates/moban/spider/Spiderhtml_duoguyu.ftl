<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<title>${title}</title>
	<link href="/resources/css/channelSite.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/js/common/channelSite.js"></script>
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
  
 <style>
    body{font:14px/1.5 "PingFang SC","Lantinghei SC","Microsoft YaHei","HanHei SC","Helvetica Neue","Open Sans",Arial,"Hiragino Sans GB","å¾®è½¯é›…é»‘",STHeiti,"WenQuanYi Micro Hei",SimSun,sans-serif;-webkit-font-smoothing:antialiased;}@media screen and (max-width:768px){.page{width:100%;margin:0 auto;padding:0px;background-color:#fff;box-shadow:#5079d9 2px 1px 5px 3px;}}@media screen and (min-width:768px){.page{width:70%;margin:0 auto;padding:10px 18px;background-color:#fff;}}.articleImg{height:360px;overflow:hidden;position:relative;}.articleImg .textBox{position:absolute;left:0;bottom:40px;z-index:10;padding:0 40px;width:100%;font-size:14px;color:#fff;line-height:28px;vertical-align:bottom;overflow:hidden;}.articleImg .textBox h1{font-size:28px;color:#fff;line-height:32px;vertical-align:bottom;}.articleImg .moreInfo{font-size:14px;color:#fff;line-height:28px;}.overlay{position:absolute;top:0;bottom:0;width:100%;-webkit-transition:all 0.2s ease;-moz-transition:all 0.2s ease;transition:all 0.2s ease;background-image:-webkit-linear-gradient(180deg,rgba(0,0,0,0.05) 5%,rgba(0,0,0,0.85) 100%);background-image:-moz-linear-gradient(180deg,rgba(0,0,0,0.05) 5%,rgba(0,0,0,0.85) 100%);background-image:linear-gradient(180deg,rgba(0,0,0,0.05) 5%,rgba(0,0,0,0.85) 100%);background-size:100%;}.articleImg .img{width:100%;height:360px;overflow:hidden;}.articleDetail{margin-top:20px;padding:20px;border-top:1px dotted #ddd;text-align:justify;}.articleDetail p{color:#4a4a4a;line-height:26px;font-size:15px !important;margin-bottom:15px;text-align:justify;}.articleDetail img{margin:0 0 8px 0;max-width:100%;vertical-align:middle;}.channelSiteInfo{margin:30px 0;text-align:right;}.iconChannelSite{display:inline-block;width:50px;height:50px;background:url(/resources/images/logo/logo_water.png) no-repeat center;background-size:50px;-moz-border-radius:50%;border-radius:50%;overflow:hidden;}.footerInfo.top{text-align:right;border-top:0;margin-top:0;}.footerInfo{margin-top:20px;padding-top:20px;border-top:1px dotted #ddd;}.footerInfo .iconSpan{cursor:pointer;font-size:12px;padding:4px 10px;line-height:12px;border-radius:4px;background:#f4f4f4;color:#999;margin-left:5px;text-decoration:none;}.footerInfo .iconSpan:hover{background:#51e0c1;color:#fff;}
 
 </style>
</head>
<body style="background: #e9ecf3;">
<!-- nav start -->

<script>
    var sys_ctx="";
</script>
<nav id="nav">
    <div class="ui  inverted menu" style="height: 58px;margin: 0;">

        <div class="header item">
            <div style="display: block;float: left;margin-right: 20px;">
                <img style="width: 50px;height:50px;display: inline-block;" src="/resources/images/logo/logo.png">
                <span style="display: inline-block;color:#fff;font-size: 1.3rem;position: relative;bottom: 18px;">我的博客</span>
            </div>
        </div>
        <a id="nav_index" class="item" href="/myblog/index">首页</a>
        <a class="item" href="/myblog/aboutMe">关于我</a>
        <a class="item" href="/myblog/bloglist">博客</a>


        <a class="item" href="/myblog/img" style="display: none;">相册</a>

    <a id="nav_book" class="item" href="/myblog/booklist">读书</a>
    <a class="item" href="/myblog/updateLog">更新日志</a>
    <a class="item" href="/myblog/demo/201807/liuyan">留言</a>
    <div class="right menu">
        <div class="item" id="div_search">
            <div class="ui transparent inverted icon input">
                <input type="text" class="searchInput"  placeholder="Search">
                <div id="searchbtn"> <i class="search icon" style="cursor:pointer;color:#666;"></i></div>
            </div>
        </div>
        <div id="newArticle"  class="item">
            <a class="ui primary button" onclick="writeBlog()">
                写博客
            </a>
        </div>

    </div>

    <script>
        $(document).ready(function() {
            // 鼠标放到 dropdown 时显示下拉菜单，默认只有点击后才显示
            $('.dropdown.item').dropdown({
                on: 'hover'
            });
            var n=($("#mobileNavSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
            $(".toggle-mobile-nav-sidebar").on("click",function(e){n.sidebar("toggle"),e.preventDefault()});
            var a=($("#mobileNavRightSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
            $(".current-user-avatar").on("click",function(e){a.sidebar("toggle"),e.preventDefault()});
        });

        var url=  window.location.href;
        var host=window.location.host;
        var protocol=window.location.protocol;
        var uri=url.replace(protocol+"//"+host,"");
        function headerLogin(){
            window.location.href='/myblog/login?redirectUrl='+uri;
        }
        function writeBlog(){
            window.location.href='/myblog/newblog';
        }
        function getCookie(name){
            //获取cookie字符串
            var strCookie=document.cookie;
            //将多cookie切割为多个名/值对
            var arrCookie=strCookie.split("; ");
            var value="";
            //遍历cookie数组，处理每个cookie对
            for(var i=0;i<arrCookie.length;i++){
                var arr=arrCookie[i].split("=");
                if(name==arr[0]){
                    value=arr[1];
                    break;
                }
            }
            return value;
        }
        function removeNavClass(){
            var lis = document.getElementById("nav").getElementsByTagName("a");
            for (var i = 0; i < lis.length; i++) {
                removeClass(lis[i],"active");
            }
        }
        function showHide(uri){
            uri=uri.replace("/myblog","");
            if(uri.indexOf("?")>0){
                uri=uri.substring(0,uri.indexOf("?"));
            }
            if(uri.indexOf("/admin")>=0){
                $("#div_search").hide();
            }else{
                $("#div_search").show();
            }
            if(uri.indexOf("blog")>=0||uri.indexOf("personalBlog")>=0||uri.indexOf("/admin")>=0){//写博客按钮 显示
                document.getElementById("newArticle").style.display="block";
            }else{
                document.getElementById("newArticle").style.display="none";
            }
        }
        function addNavClass(uri){
            uri=uri.replace("/myblog","");
            if(uri.indexOf("?")>0){
                uri=uri.substring(0,uri.indexOf("?"));
            }
            var lis = document.getElementById("nav").getElementsByTagName("a");
            for (var i = 0; i < lis.length; i++) {

                if(lis[i].href.indexOf(uri)>=0){
                    lis[i].className="item active";
                    break;
                }
            }
        }
        function removeClass(elem,classname){
            if(elem.className != ""){
                var allClassName = elem.className.split(" ");
                //console.log("第一次赋值后class数量为：" + allClassName);
                var result;//完成操作后保存类名（在以后调用）
                var listClass;//保存修改过后的数组
                for (var i = 0; i < allClassName.length; i++) {
                    if(allClassName[i] == classname){
                        allClassName.splice(i,1);
                    }
                }
                listClass = allClassName;
                for (var j = 0; j < listClass.length; j++) {
                    //之后加上空格因为类名的存在形式就是用空格分隔的
                    if (j == 0) {
                        result = listClass[j];
                        result += " ";
                    }else{
                        result += listClass[j];
                        result += " ";
                    }
                }
                // console.log("处理后的listClass数量" + listClass);
                elem.className = result;//将目标元素的类名重新被 result 覆盖掉
            }else{
                //console.log("目标元素没有存在的类名")
            }
        }
        setTimeout(function(){
            showHide(uri);
        },5);
        setTimeout(function(){
            removeNavClass();
            addNavClass(uri)
            if(uri=="/login"||uri=='/myblog/index'||uri=='/myblog'){
                document.getElementById("nav_index").className="active item";
            }
            if(uri.indexOf("book/detail")>0){
                document.getElementById("nav_book").className="active item";
            }
        },50);



    </script>

    </div>
</nav>

<!-- nav end -->
<!-- fullTabs 置顶热门标签 -->
<div class="fullTabs" id="headFullTabs">
<#if fullTabs??>
    <div class="menuBox">
    <#list fullTabs as item>
        <span><a  href="${item.url!}">${item.name!}</a></span>
    </#list>
    </div>
<#else>
    <div class="menuBox">
        <span><a  href="/front/static/search/tag/id/32/1.html" >情怀</a></span>
        <span><a  href="/front/static/search/tag/id/30/1.html" >漫谈</a></span>
        <span><a  href="/front/static/search/tag/id/36/1.html" >娱乐</a></span>
        <span><a  href="/front/static/search/tag/id/34/1.html" >科技</a></span>
        <span><a  href="/front/static/search/tag/id/37/1.html" >设计</a></span>
        <span><a  href="/front/static/search/tag/id/33/1.html" >插画</a></span>
        <span><a  href="/front/static/search/tag/id/31/1.html" >故事</a></span>
        <span><a  href="/front/static/search/tag/id/35/1.html" >创意</a></span>
    </div>
</#if>
</div>
  
<div class="page">
    <div class="articleImg">
         <div class="textBox">
             <h1>${title!}</h1>
             <div class="moreInfo">${author!}</div>
              </div>
              <div class="overlay"></div>
         <div class="img" style="background:url(${cover_image!}) no-repeat center; background-size:cover;"></div>
    </div>
    
    <!--标签-->
    <div class="footerInfo top">
    <#if tagsList??>
      <#list tagsList as tag>
         <a class="iconSpan" data-id="${tag.id!}" target="_blank">${tag.name!}</a>
      </#list>
    </#if>
    </div>
    <!--跳转站点-->
    <div class="channelSiteInfo">
         <#if  extra1=="一个">
             <#assign siteClass="one">
         <#elseif extra1=="豆瓣">
             <#assign siteClass="douban">
         <#elseif extra1=="知乎">
             <#assign siteClass="zhihu">
         <#elseif extra1=="果壳">
             <#assign siteClass="guoke">
         <#elseif extra1=="36 氪">
             <#assign siteClass="kr36">
         <#elseif extra1=="PingWest 品玩">
             <#assign siteClass="pinwan">
         <#elseif extra1=="好奇心日报">
             <#assign siteClass="haoqixin">
         <#elseif extra1=="差评">
             <#assign siteClass="chaping">
         <#elseif extra1=="虎嗅">
             <#assign siteClass="huxiu">
         <#elseif extra1=="人间">
             <#assign siteClass="renjian">
         <#elseif extra1=="真实故事">
             <#assign siteClass="zhenshigushi">
         <#elseif extra1=="NGA">
             <#assign siteClass="NGA">
         <#elseif extra1=="网易">
             <#assign siteClass="wangyi">
         <#elseif extra1=="今日头条">
             <#assign siteClass="toutiao">
         <#elseif extra1=="新浪">
             <#assign siteClass="sina">
         <#elseif extra1=="简书">
             <#assign siteClass="jianshu">
         <#elseif extra1=="少数派">
             <#assign siteClass="sspai">
         <#elseif extra1=="数字尾巴">
             <#assign siteClass="dgtle">
         <#elseif extra1=="十五言">
             <#assign siteClass="yan15">
         <#elseif extra1=="大象公会">
             <#assign siteClass="dxgh">
         <#elseif extra1=="界面">
             <#assign siteClass="jiemian">
         <#elseif extra1=="新世相">
             <#assign siteClass="xinshixiang">
         <#elseif extra1=="极客公园">
             <#assign siteClass="geekpark">
         <#elseif extra1=="站酷">
             <#assign siteClass="zcool">
         <#elseif extra1=="悦cms">
            <#assign siteClass="yuecms">
         </#if>
        <a href="javascript:;" data-href="${extra2!}" data-site-name="${extra1!}" title="查看原文" id="channelSite" class="iconChannelSite jumpHref  ${siteClass!}"></a>
    </div>
    
    <!--文章正文-->
    <div class="articleDetail">
    ${content!}
    </div>
    
    <!--文章来源版权信息-->
    <div class="copyInfo">
         <p class="theEnd"><span>-</span><b>THE END</b><span>-</span></p> 
          <div class="mt10">
           <p>版权声明：本文来自<a href="javascript:;" data-href="${extra2!}" title="查看原文" data-site-name="${extra1!}" class="channelSite jumpHref">${extra1!}</a>，相关版权归原作者及来源网站所有。</p>
           <p><a href="javascript:;" data-href="${extra2!}" data-site-name="${extra1!}" title="查看原文" class="channelSite jumpHref">[ 查看原文 ]</a></p> 
          </div>
    </div>
               
    <a class="to-top" title=""  style="position: fixed; right: 25px; bottom: 50px; cursor: pointer; display: block;" data-original-title="点击返回顶部"  ></a>
               
    <div id="SOHUCS"  sid="${id!}"></div>
    <script charset="utf-8" type="text/javascript" src="https://changyan.sohu.com/upload/changyan.js" ></script>
    <script type="text/javascript">
          window.changyan.api.config({
               appid: 'cyuiRxL9X',
               conf: '55e9ed6e5af85101aa2df4267ed012cd'
         });
    </script>        
               
</div>
<!--百度自动推送-->
<script>
    (function(){
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        }
        else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
</script>
<!--百度统计-->
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?065d63785bc5969ba048e6178619b603";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
<script type="text/javascript" src="/resources/js/common/viewArticle.js"></script>
</body>
</html>