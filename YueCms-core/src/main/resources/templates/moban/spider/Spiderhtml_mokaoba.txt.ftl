<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<title>${title}</title>
 <link href="style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<h2 style="text-align:center;">${title}</h2>
<div id="content_all">
#foreach($item in $questions)
  <div>
  <p>第${item.num}题：（<span style="color:red;">${item.type}</span>）${item.question}</p>
  <p style="color:green;">${item.answer}</p>
  </div>
#end
</div>
<p style="text-align:right;float:right;">来源：${url}</p>
</body>
</html>