<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/pages/include/taglib.jsp"%>
#parse('input.txt')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${beanChineseName}管理</title>
    <link rel="shortcut icon" href="favicon.ico">
#if($GenJsp_style_type=='hplus')
    <link href="/resources/css/hplus.css" rel="stylesheet" />
#else
    <link href="/resources/css/admui.css" rel="stylesheet" />
#end
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
#if($formValidate=='bootstrapValidator')
#if($addPageType=='1')
#else
    <link href="/resources/lib/bootstrap-validator/css/bootstrapValidator.min.css" rel="stylesheet" />
#end
#else
#end
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/common/broutil.js"></script>

#if($treeType=='easyuiTree')
    <link rel="stylesheet" type="text/css" href="${ctx}/resources/lib/easyui/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/resources/lib/easyui/themes/icon.css">
    <script type="text/javascript" src="${ctx}/resources/lib/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/lib/jquery-ui/jquery-ui.min.js"></script>
#else
     <link href="${ctx}/resources/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css"/>
     <script src="${ctx}/resources/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>
#end


</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> ${module} <span class="c-gray en">&gt;</span>${beanChineseName}管理</nav>
<div class="wrapper animated fadeInRight">

#if($needTree==true)
<div style="position:relative;width:100%;height:auto;overflow: hidden">

    <div class="clearfix" style="width:225px;border-right:1px solid #ddd;height:auto;float:left;overflow-y: auto;" id="west">
        <div style="height:5px;margin:5px 0 0 0;width:220px;height:30px;">
          <a href="#" class="easyui-linkbutton" onClick="refreshTree()" plain="true">刷新</a>
          <a href="#" class="easyui-linkbutton" onClick="expandAll();" plain="true">展开</a>
          <a href="#" class="easyui-linkbutton" onClick="collapseAll();" plain="true">折叠</a>
        </div>
        <div  style="vertical-align: top;margin:0 0 0 0;width:220px;">
          <div>
            <ul id="trees">

            </ul>
          </div>
        </div>
    </div>
    <div id="mainPanle"width="100%;float:left;"  style="overflow-y: hidden;">
#end
    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
#if($addButton=='1')
                <a onclick="addNew()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
#end
                <a onclick="list_del('tableList')" class="btn btn-sm btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
#if($needSort==true)
                <a href="javascript:void(0)"  onClick="sortMenu();" class="btn btn-sm btn-info" ><i class="fa fa-sort"></i>&nbsp;排序</a>
#end
#if($search_hidden==true)
              <a id="open_search"  onclick="javascript:$('#search_hidden').show();$(this).hide();" class="btn btn-sm btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
              </div>
              <div id="search_hidden"  style="display: none;margin-top: 8px"  >
                    //在此处添加查询条件
                   <div class="btn-group">
                        <a onclick="query('form_query');" class="btn btn-sm btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                        <a onclick="search_form_reset('form_query')" class="btn btn-sm btn-warning" ><i class="fa fa-trash"></i>&nbsp;清空</a>
                         <a  onclick="javascript:$('#search_hidden').hide();$('#open_search').show();" class="btn btn-default btn-sm"><i class="fa fa-close"></i>&nbsp;关闭</a>
                   </div>
              </div>
#else
              <a  onclick="query('form_query');" class="btn btn-sm btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
#end

        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

#if($addPageType=='1')
#else
  <!--add Modal -->
      <div id="modal-form" class="modal fade" aria-hidden="true">
          <div class="modal-dialog">
              <form id="form_show" class="form-horizontal" role="form">
                  <div class="modal-content">

                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h3>对话框标题</h3>
                      </div>
                      <div class="modal-body">
                          <input type="hidden" id="id" name="id">
                          #foreach($input in $jspShowColumns)
                          <div class="form-group">
                              <label class="col-sm-3 control-label">${input.comment}：</label>
                              <div class="col-sm-9">
                                  #if($input.input.type=='text')
                                  #input_text($input.comment $input.name)
                                  #elseif($input.input.type=='textarea')
                                  #input_area($input.comment $input.name)
                                  #elseif($input.input.type=='password')
                                  #input_password($input.comment $input.name)
                                  #elseif($input.input.type=='select')
                                  #select($select.title $select.id $select.items)
                                  #else
                                  #input_text($input.comment $input.name)
                                  #end
                                  </div>
                          </div>
#end

                      </div>
                      <div class="modal-footer" style="text-align: center">
                          <button type="submit"  id="saveBtn"  class="btn btn-primary" >保存</button>
                          <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
                      </div>
                  </div>
              </form>
          </div>
#if($needTree==true)
        </div>
#end
      </div>
#end
    </div>
</div>
#if($needSort==true)
    <div id="div_sort" title="菜单排序" style="width:350px;height:400px;display: none;">
        <div class="c_div_tool_bar">
            <a href="javascript:void(0)" class="btn btn-primary size-S" style="margin:4px;" onClick="saveSort()">保存</a>
        </div>
        <div class="c_div_show_content">
            <form id="form_sort">
                <div id="dd" style="width:100%;">
                    <ul id="sortable">

                    </ul>
                </div>
            </form>
        </div>
    </div>
#end
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<%-- <script src="/resources/js/content.min.js"></script> --%>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
#if($formValidate=='bootstrapValidator')
<script src="/resources/lib/bootstrap-validator/js/bootstrapValidator.min.js"></script>
#else
<script type="text/javascript" src="${ctx}/resources/lib/Validform/Validform_v5.3.2.js"></script>
#end


<script type="text/javascript" >

#if($needTree==true)
    var tmp_parentid=0;
    var tmp_parentname;
#if($treeType=='easyuiTree')
    function loadTree(){
        $('#trees').tree( {
            url :'${ctx}/$module/$lowerModelClass/json/tree?parentid=0',
            checkbox :false,
            lines:true,
            onBeforeExpand:function(node,param){
                //当节点展开之后触发.
                var id=node.id;
                if(id!='0'&&id!=0){
                    $('#trees').tree('options').url = "${ctx}/$module/$lowerModelClass/json/tree?parentid="+id;
                }else{
                    return;
                }
            },
            onLoadSuccess:function(){
                tmp_parentname=$('#trees').tree('getRoot').text;
            },
            onClick:function(node){
                var id = node.id;
                tmp_parentname=node.text;
                tmp_parentid=id;
                sys_table_list();
            }
        });

    }
    function refreshTree(){
        $('#trees').tree('options').url = "${ctx}/$module/$lowerModelClass/json/tree?parentid=0";
        $('#trees').tree('reload');
    }
    function collapseAll() {
        var node = $('#trees').tree('getSelected');
        if (node) {
            $('#trees').tree('collapseAll', node.target);
        } else {
            $('#trees').tree('collapseAll');
        }
    }
    //全部收缩
    function expandAll() {
        var node = $('#trees').tree('getSelected');
        if (node) {
            $('#trees').tree('expandAll', node.target);
        } else {
            $('#trees').tree('expandAll');
        }
    }
#else
    var zTreeObj;
	var treeSetting = {
		data: {
			simpleData: {
				enable: true,
				idKey: "id",
				pIdKey:"parentid"
			}
		},
		async:{
			enable:true,
			url: "${ctx}/$module/$lowerModelClass/json/ztree?parentid="+tmp_parentid
		},
		view: {
			showLine: true,
			showIcon: true
		},
		callback: {
			beforeAsync: zTreeBeforeAsync,
			onAsyncSuccess: zTreeAjaxSuccess,
			onClick: zTreeOnClick
		}
	}
	function initZtree(){
		$.fn.zTree.init($("#trees"), treeSetting, null);
		zTreeObj = $.fn.zTree.getZTreeObj("trees");
	}
	function zTreeAjaxSuccess(event, treeId, treeNode, msg){
		var nodes=zTreeObj.getNodes();
		zTreeObj.expandNode(nodes[0], true, false, false);
		tmp_parentid=nodes[0].id;
		tmp_parentname=nodes[0].name;
		table_list_query_form.parentid=tmp_parentid;
		sys_table_list();
	}
	function zTreeBeforeAsync(treeId, treeNode){
		if (treeNode) {
			tmp_parentid=treeNode.id ;
			 zTreeObj.setting.async.url="${ctx}/$module/$lowerModelClass/json/ztree?parentid="+tmp_parentid;
		}
	}
	function zTreeOnClick(event, treeId, treeNode,clickFlag) {
		tmp_parentid=treeNode.id ;
		tmp_parentname=treeNode.name ;
		table_list_query_form.parentid=tmp_parentid;
		sys_table_list();
	}
	function refreshTree(){
		tmp_parentid=0;
		zTreeObj.setting.async.url="${ctx}/$module/$lowerModelClass/json/ztree?parentid="+tmp_parentid;
		zTreeObj.reAsyncChildNodes(null, "refresh");
	}
	  /**
         * 刷新当前节点
         */
        function refreshNode() {
            /*根据 treeId 获取 zTree 对象*/
            var zTree = $.fn.zTree.getZTreeObj("trees"),
                    type = "refresh",
                    silent = false,
            /*获取 zTree 当前被选中的节点数据集合*/
                    nodes = zTree.getSelectedNodes();
            /*强行异步加载父节点的子节点。[setting.async.enable = true 时有效]*/
            //  alert(JSON.stringify(nodes[0]));
         if(nodes!=undefined&&nodes!=null&&nodes.length>0){
            zTree.reAsyncChildNodes(nodes[0], type, silent);
            zTreeOnClick(null, zTree.setting.treeId, nodes[0]);
         }else{
            refreshTree();
         }
   }
        function refreshParentNode() {
            var zTree = $.fn.zTree.getZTreeObj("trees"),
                    type = "refresh",
                    silent = false,
                    nodes = zTree.getSelectedNodes();
            /*根据 zTree 的唯一标识 tId 快速获取节点 JSON 数据对象*/
            var parentNode = zTree.getNodeByTId(nodes[0].parentTId);
            /*选中指定节点*/
            zTree.selectNode(parentNode);
            zTree.reAsyncChildNodes(parentNode, type, silent);
        }
	function collapseAll() {
            var node = $('#trees').tree('getSelected');
            if (node) {
                $('#trees').tree('collapseAll', node.target);
            } else {
                $('#trees').tree('collapseAll');
            }
        }
        //全部收缩
        function expandAll() {
            var node = $('#trees').tree('getSelected');
            if (node) {
                $('#trees').tree('expandAll', node.target);
            } else {
                $('#trees').tree('expandAll');
            }
        }
#end
#end
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
#if($needTree==true)
        parentid:tmp_parentid
#end
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
#if($addButton=='0')
#if($formValidate=='bootstrapValidator')
      $('form').bootstrapValidator({
                          message: 'This value is not valid',
                          excluded: [':disabled'],
                          feedbackIcons: {
                              valid: 'glyphicon glyphicon-ok',
                              invalid: 'glyphicon glyphicon-remove',
                              validating: 'glyphicon glyphicon-refresh'
                          },

                          fields: {
#foreach($input in $jspShowColumns)
      $input.name: {
                                  message: '$input.comment验证失败',
                                  validators: {
                                      notEmpty: {
                                          message: '$input.comment不能为空'
                                      }
                                  }
                              },
#end
                          },
                          submitHandler: function (validator, form, submitButton) {
                              alert("submit");
                          }
                      }).on('success.form.bv', function (e) {
                          e.preventDefault();
                      });
#else
 	 validform=$("#form_show").Validform({
      		  btnReset:"#reset",
              tiptype:2,
              postonce:true,//至提交一次
              ajaxPost:false,//ajax方式提交
              showAllError:true //默认 即逐条验证,true验证全部
            });
#end
#end
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
#foreach($input in $jspShowColumns)
            {field: '$input.name',align:"center",sortable:true,order:"asc",visible:true,title: '$input.comment'},
#end
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.url='${ctx}/${module}/${lowerModelClass}/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
#if($addPageType=='1')
    #if($needTree==true)
       layer_show("编辑$beanChineseName",sys_ctx+"/$module/$lowerModelClass/edit?id="+row.id,'','','',function(){sys_table_list();refreshNode();});
    #else
       layer_show("编辑$beanChineseName",sys_ctx+"/$module/$lowerModelClass/edit?id="+row.id,'','','',function(){sys_table_list();});
    #end

#else
        $('#modal-form').on('show.bs.modal', function() {
            $('#modal-form .modal-header h3').html("编辑${beanChineseName}");
            $('#form_show').bootstrapValidator('resetForm', true);
            bind(JSON.stringify(row));
        });
        $('#modal-form').modal('show');
#end
    }
    function addNew(){
#if($addPageType=='1')
    #if($needTree==true)
       layer_show("新增$beanChineseName",sys_ctx+"/$module/$lowerModelClass/add$toAddParams,'','','',function(){sys_table_list();refreshNode();});
    #else
       layer_show("新增$beanChineseName",sys_ctx+"/$module/$lowerModelClass/add$toAddParams,'','','',function(){sys_table_list();});
    #end
#else
            $('#modal-form').on('show.bs.modal', function() {
                $('#modal-form .modal-header h3').html("新增${beanChineseName}");
                $('#form_show').bootstrapValidator('resetForm', true);
                $('#form_show')[0].reset();
                $('#id').val("");
            });
            $('#modal-form').modal('show');
#end
    }
#if($addPageType=='1')
#else
    function save(){
        var b=$("#form_show").data('bootstrapValidator').isValid();
        if(!b)
        {
            return;
        }
        var params=$("#form_show").serialize();
        $.ajax({
            type:"post",
            url:'${ctx}/${module}/${lowerModelClass}/json/save?'+params,
            data:null,
            success:function(json,textStatus){
                $('#modal-form').modal('hide')
                ajaxReturnMsg(json);
                query("form_query");
            }
        });
    }
#end

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            layer.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'${ctx}/${module}/${lowerModelClass}/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        #if($needTree==true)
                         refreshNode();
                        #else
                         sys_table_list();
                        #end
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
#if($needSort==true)
    var sortMenu = function(){
        $.ajax({
            type:"post",
            url:'${ctx}/${module}/${lowerModelClass}/json/findByParentid/'+tmp_parentid,
            data:null,
            success:function(data,textStatus){
                var rows = JSON.parse(data);
                var str = "";
                for(var i=0;i<rows.length;i++){
                    str += '<li class="ui-state-default" id="xh_'+rows[i]['id']+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+rows[i]['name']+'</li>';
                };
                $("#sortable").html('').append(str);
                $("#sortable").sortable();
                $("#sortable").disableSelection();
            }
        });
        layer_openHtml("排序",$("#div_sort"),null,{width:'350px',height:'400px'})
        $("#div_sort").show();

    }
    var saveSort = function(){
        var arr = $("#sortable").sortable('toArray');
        //Dumper.alert(arr);
        for(var i=0;i<arr.length;i++){
            arr[i] = arr[i] + "_"+(i+1)
        }
        var sort = arr.join(',');
        $.ajax({
            type:"post",
            url:'${ctx}/${module}/${lowerModelClass}/json/saveSort',
            data:{sort:sort},
            success:function(data,textStatus){
                ajaxReturnMsg(data);
                query("form_query");
                refreshTree();
                setTimeout(function(){
                    layer.closeAll();
                },1000);
            }
        });
    }
#end
</script>
</body></html>
