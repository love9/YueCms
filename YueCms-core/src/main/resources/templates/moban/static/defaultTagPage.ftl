<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title></title>
    <meta name="renderer" content="webkit">

    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>


</head>
<body id="myblog-index" class="pushable ">
<script>
    var sys_ctx = "";
</script>
<!--手机试图导航 start-->
<div class="ui left inverted sidebar vertical menu" id="mobileNavSidebar" style="display: none;">
    <a class="item" href="/myblog/index">首页</a>
    <a class="item" href="/myblog/aboutMe">关于我</a>
    <a class="item" href="/myblog/bloglist">博客</a>
    <#if front_yhmc??>
    <a class="item" href="/myblog/img">相册</a>
    </#if>
    <a id="nav_book" class="item" href="/myblog/booklist">读书</a>
    <a class="item" href="/myblog/updateLog">更新日志</a>
</div>
<div class="ui right inverted sidebar vertical menu" id="mobileNavRightSidebar" style="display: none;">
    <a class="item" href="/myblog/personalBlog">个人博客</a>
    <a class="item" href="/myblog/admin/postlist">管理博客</a>
    <a class="item" href="/admin/login">登录后台</a>
    <a class="item" href="/myblog/logout">退出</a>
</div>
<!--手机试图导航 end-->
<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->

    <script>
        var sys_ctx="";
    </script>
    <nav id="nav">
        <div class="ui  inverted menu" style="height: 58px;margin: 0;">

            <div class="header item">
                <div style="display: block;float: left;margin-right: 20px;">
                    <img style="width: 50px;height:50px;display: inline-block;" src="/resources/images/logo/logo.png">
                    <span style="display: inline-block;color:#fff;font-size: 1.3rem;position: relative;bottom: 18px;">我的博客</span>
                </div>
            </div>
            <a id="nav_index" class="item" href="/myblog/index">首页</a>
            <a class="item" href="/myblog/aboutMe">关于我</a>
            <a class="item" href="/myblog/bloglist">博客</a>


            <a class="item" href="/myblog/img" style="display: none;">相册</a>

            <a id="nav_book" class="item" href="/myblog/booklist">读书</a>
            <a class="item" href="/myblog/updateLog">更新日志</a>
            <a class="item" href="/myblog/demo/201807/liuyan">留言</a>
            <div class="right menu">
                <div class="item" id="div_search">
                    <div class="ui transparent inverted icon input">
                        <input type="text" class="searchInput"  placeholder="Search">
                        <div id="searchbtn"> <i class="search icon" style="cursor:pointer;color:#666;"></i></div>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function() {
                    // 鼠标放到 dropdown 时显示下拉菜单，默认只有点击后才显示
                    $('.dropdown.item').dropdown({
                        on: 'hover'
                    });
                    var n=($("#mobileNavSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
                    $(".toggle-mobile-nav-sidebar").on("click",function(e){n.sidebar("toggle"),e.preventDefault()});
                    var a=($("#mobileNavRightSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
                    $(".current-user-avatar").on("click",function(e){a.sidebar("toggle"),e.preventDefault()});
                });

                var url=  window.location.href;
                var host=window.location.host;
                var protocol=window.location.protocol;
                var uri=url.replace(protocol+"//"+host,"");
                function headerLogin(){
                    window.location.href='/myblog/login?redirectUrl='+uri;
                }
                function writeBlog(){
                    window.location.href='/myblog/newblog';
                }
                function getCookie(name){
                    //获取cookie字符串
                    var strCookie=document.cookie;
                    //将多cookie切割为多个名/值对
                    var arrCookie=strCookie.split("; ");
                    var value="";
                    //遍历cookie数组，处理每个cookie对
                    for(var i=0;i<arrCookie.length;i++){
                        var arr=arrCookie[i].split("=");
                        if(name==arr[0]){
                            value=arr[1];
                            break;
                        }
                    }
                    return value;
                }
                function removeNavClass(){
                    var lis = document.getElementById("nav").getElementsByTagName("a");
                    for (var i = 0; i < lis.length; i++) {
                        removeClass(lis[i],"active");
                    }
                }
                function showHide(uri){
                    uri=uri.replace("/myblog","");
                    if(uri.indexOf("?")>0){
                        uri=uri.substring(0,uri.indexOf("?"));
                    }
                    if(uri.indexOf("/admin")>=0){
                        $("#div_search").hide();
                    }else{
                        $("#div_search").show();
                    }
                   
                }
                function addNavClass(uri){
                    uri=uri.replace("/myblog","");
                    if(uri.indexOf("?")>0){
                        uri=uri.substring(0,uri.indexOf("?"));
                    }
                    var lis = document.getElementById("nav").getElementsByTagName("a");
                    for (var i = 0; i < lis.length; i++) {

                        if(lis[i].href.indexOf(uri)>=0){
                            lis[i].className="item active";
                            break;
                        }
                    }
                }
                function removeClass(elem,classname){
                    if(elem.className != ""){
                        var allClassName = elem.className.split(" ");
                        //console.log("第一次赋值后class数量为：" + allClassName);
                        var result;//完成操作后保存类名（在以后调用）
                        var listClass;//保存修改过后的数组
                        for (var i = 0; i < allClassName.length; i++) {
                            if(allClassName[i] == classname){
                                allClassName.splice(i,1);
                            }
                        }
                        listClass = allClassName;
                        for (var j = 0; j < listClass.length; j++) {
                            //之后加上空格因为类名的存在形式就是用空格分隔的
                            if (j == 0) {
                                result = listClass[j];
                                result += " ";
                            }else{
                                result += listClass[j];
                                result += " ";
                            }
                        }
                        // console.log("处理后的listClass数量" + listClass);
                        elem.className = result;//将目标元素的类名重新被 result 覆盖掉
                    }else{
                        //console.log("目标元素没有存在的类名")
                    }
                }
                setTimeout(function(){
                    showHide(uri);
                },5);
                setTimeout(function(){
                    removeNavClass();
                    addNavClass(uri)
                    if(uri=="/login"||uri=='/myblog/index'||uri=='/myblog'){
                        document.getElementById("nav_index").className="active item";
                    }
                    if(uri.indexOf("book/detail")>0){
                        document.getElementById("nav_book").className="active item";
                    }
                },50);



            </script>

        </div>
    </nav>

    <!-- nav end -->
    <!-- fullTabs 置顶热门标签 -->
    <div class="fullTabs" id="headFullTabs">
<#if fullTabs??>
    <div class="menuBox">
    <#list fullTabs as item>
        <#if tag.name==item.name>
                <span><a class="hover" href="${item.url!}" >${item.name!}</a></span>
        <#else>
           <span><a  href="${item.url!}" >${item.name!}</a></span>
        </#if>
        
    </#list>
    </div>
<#else>
     <div class="menuBox">
            <#if tag.name=="情怀">
                <span><a class="hover" href="/front/static/search/tag/id/32/1.html" >情怀</a></span>
            <#else>
            <span><a  href="/front/static/search/tag/id/32/1.html" >情怀</a></span>
            </#if>
            
            <#if tag.name=="漫谈">
                <span><a class="hover" href="/front/static/search/tag/id/30/1.html" >漫谈</a></span>
            <#else>
                <span><a  href="/front/static/search/tag/id/30/1.html" >漫谈</a></span>
            </#if>
            
            <#if tag.name=="娱乐">
                <span><a class="hover" href="/front/static/search/tag/id/36/1.html" >娱乐</a></span>
            <#else>
                <span><a  href="/front/static/search/tag/id/36/1.html">娱乐</a></span>
            </#if>
            
            <#if tag.name=="科技">
                <span><a class="hover" href="/front/static/search/tag/id/34/1.html" >科技</a></span>
            <#else>
                <span><a  href="/front/static/search/tag/id/34/1.html" >科技</a></span>
            </#if>
            
            <#if tag.name=="设计">
                <span><a class="hover" href="/front/static/search/tag/id/37/1.html" >设计</a></span>
            <#else>
                <span><a  href="/front/static/search/tag/id/37/1.html">设计</a></span>
            </#if>
            
            <#if tag.name=="插画">
                <span><a class="hover" href="/front/static/search/tag/id/33/1.html" >插画</a></span>
            <#else>
                <span><a  href="/front/static/search/tag/id/33/1.html">插画</a></span>
            </#if>
            
            <#if tag.name=="故事">
                <span><a class="hover" href="/front/static/search/tag/id/31/1.html">故事</a></span>
            <#else>
                <span><a  href="/front/static/search/tag/id/31/1.html">故事</a></span>
            </#if>
            
            <#if tag.name=="创意">
                <span><a class="hover" href="/front/static/search/tag/id/35/1.html" >创意</a></span>
            <#else>
                <span><a  href="/front/static/search/tag/id/35/1.html" >创意</a></span>
            </#if>

     </div>
</#if>
    </div>


    <!-- content srart -->
    <div class="ui basic segment" id="mainScreen">
        <div class="ui container grid" style="background: #fff;">
           
                <!--左侧内容 start-->
                <div class="sixteen wide tablet eleven wide computer column">
                    <!--搜索结果区域 start-->

                    <div class="blog-sidebar-widget" style="margin:0 -1.2rem;">
                        <h3 class="blog-sidebar-widget-title"><b>"${searchText!}"找到${totalCount!}个结果</b></h3>
                        <div class="blog-sidebar-widget-content" id="">
                            <div class="itemGrid">
                                <ul>
<#if articles??>
                <#list articles as item >
                    <li>
                        <div class="article_box">

    <#if item.static_url?? && item.static_url!="">
                            <a href="${item.static_url!}" title="${item.title!}" target="_blank">
    <#else>
                            <a href="/myblog/article/detail/${item.id!}" title="${item.title!}" target="_blank">
    </#if>
                            <div class="tophead">
                                
                                <img class="lazy" width="294" height="160" data-original="${item.cover_image!}"
                                     src="${item.cover_image!}" style="display: block;">
                            </div>
                            <article class="h135">
                                <h3>
                                    ${item.title!}
                                </h3>
                                <p class="h40 cf999 mt10">${item.description!}</p>
                                <p class="mt15" style="text-align: right;"><span class="cf_green f12">阅读全文 &gt;</span>
                                </p>
                            </article>
                        </a>
                        </div>
                    </li>
                </#list>
<#else>
</#if>
                                </ul>
                                <div class="line"></div>
                            </div>
                        </div>
                    </div>

                    <!--搜索结果区域 end-->
                    <#if totalCount?? && (totalCount>0)>
                      <div id="pageGroup">
                          <a href="${firstPage!}" class="homepage">首页</a>
                          <a href="${prePage!}" class="nopage">上页</a>
                        <#if pageGroup?? && (pageGroup?size>0)>
                            <#list pageGroup as item>
                                <#if pageNo==item.num>
                                    <a href="${item.url!}" class="listpage curpage">${item.num!}</a>
                                <#else>
                                    <a href="${item.url!}" class="listpage">${item.num!}</a>
                                </#if>
                            </#list>
                        </#if>
                          <a href="${nextPage!}" class="nextpage">下页</a>
                          <a href="${endPage!}" class="endpage">尾页</a>
                          <span class="pageinfo">共<strong>${totalPages!}</strong>页<strong>${totalCount!}</strong>条记录</span>
                      </div>
                    </#if>
                </div>
                <!--左侧内容 end-->
                <!--右侧内容 start-->
                <div class="sixteen wide tablet five wide computer column">
                    <!--最新专区-->
                    <div class="blog-sidebar-widget">
                        <h3 class="blog-sidebar-widget-title"><b>热门</b></h3>
                        <div class="blog-sidebar-widget-content blog-side-bar" id="">
                        <#if hots??>
                        
                            <#list hots as item>
                                <div class="item">
                                    <#if item.static_url?? && item.static_url!="">
                                        <a href="${item.static_url!}" title="${item.title!}" target="_blank">
                                    <#else>
                                        <a href="/myblog/detail?id=${item.id!}" title="${item.title!}" target="_blank">
                                    </#if>
                                    <h3>${item.title!}</h3>
                                    <div class="desc mb15">${item.description!}</div>
                                </a>
                                </div>
                            </#list>
                        <#else>
                        </#if>
                        </div>
                        <div class="adGrid"><a href="http://www.wildaidchina.org" target="_blank" title="野生救援"><img  src="/resources/myblog/images/gy.jpg"></a></div>
                    </div>
                </div>
                <!--右侧内容 end-->
         
        </div>
    </div>
    <!-- content end -->
    <script>

    </script>

    <footer>
        <div class="ui black inverted vertical footer segment">
            <div class="ui center aligned container">
                <div class="ui stackable inverted grid">
                    <div class="column">
                        <p><span style="color:#666;">站点声明：本站转载作品版权归原作者及来源网站所有，原创内容作品版权归作者所有，任何内容转载、商业用途等均须联系原作者并注明来源。</span></p>
                        <p> <span style="color:#666;">   相关侵权、举报、投诉及建议等，请联系747506908@qq.com，我们定将及时处理。</span> </p>
                        <p style="color:#888;">友情链接：

                            <a style="padding-right:20px; color:#888;" href=" http://www.kuaidi.com/all/yunda.html" target="_blank">韵达快递查询</a>
                            <a style="padding-right:20px; color:#888;" href="//www.aikuaidi.cn/" target="_blank" >快递查询</a>
                            <a style="padding-right:20px; color:#888;" href="http://www.yangqq.com/" target="_blank">杨青博客</a>
                            <a style="padding-right:20px; color:#888;" href="http://nec.netease.com/" target="_blank">NEC</a>
                            <a style="padding-right:20px; color:#888;" href="http://amazeui.org/" target="_blank">amazeUi</a>
                        </p>
                    </div>
                </div>

                <div class="ui inverted section divider" style="margin:16px auto;"></div>
                <img src="/resources/images/logo/logo.png" class="ui centered mini image">
                <div class="ui horizontal inverted small divided link list">
                    <a style="color:#FFF" class="item" >@2016-2018 我的博客 All Rights Reserved</a>
                </div>
            </div>
        </div>
    </footer>
    
</div>
 
    <a class="to-top" title=""  style="position: fixed; right: 25px; bottom: 50px; cursor: pointer; display: block;" data-original-title="点击返回顶部"  ></a>
</body>
</html>