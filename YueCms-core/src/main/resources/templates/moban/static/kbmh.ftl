<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/><title>恐怖漫画-${title}</title><p>
	</p><link href="/resources/css/channelSite.css" rel="stylesheet"/><script type="text/javascript" src="/resources/js/jquery.min.js"></script><script type="text/javascript" src="/resources/js/common/channelSite.js"></script><style>body{
     font: 14px/1.5 "PingFang SC","Lantinghei SC","Microsoft YaHei","HanHei SC","Helvetica Neue","Open Sans",Arial,"Hiragino Sans GB","å¾®è½¯é›…é»‘",STHeiti,"WenQuanYi Micro Hei",SimSun,sans-serif;
    -webkit-font-smoothing: antialiased;
 }
 @media screen and (max-width:768px){
 	.page{
 	width: 100%;
    margin: 0 auto;
    padding: 0px;
    background-color: #fff;
    box-shadow: #5079d9 2px 1px 7px 6px;
 	}
}
 @media screen and (min-width:768px){
 	.page{
 	width: 70%;
    height:auto;
    min-height:100%;
    overflow-y:auto;
    margin: 0 auto;
    padding: 10px 18px;
    background-color: #fff;
    box-shadow: #5079d9 2px 1px 7px 6px;
 	}
}

 h1 {
    font-size: 28px;
    color: #333;
    line-height: 32px;
    vertical-align: bottom;
}
.articleImg .moreInfo {
    font-size: 14px;
    color: #fff;
    line-height: 28px;
}

.articleDetail {
    margin-top: 20px;
    padding: 20px;
    border-top: 1px dotted #ddd;
    text-align: justify;
}
.articleDetail p{
    color: #4a4a4a;
    line-height: 26px;
    font-size: 15px !important;
    margin-bottom: 15px;
    text-align: justify;
}
.articleDetail img {
    margin: 0 0 8px 0;
    max-width: 100%;
    vertical-align: middle;
}

/*标签*/
 .footerInfo.top {
    text-align: right;
    border-top: 0;
    margin-top: 0;
}

 .footerInfo {
    margin-top: 20px;
    padding-top: 20px;
    border-top: 1px dotted #ddd;
}
 .footerInfo .iconSpan {
    cursor:pointer;
    font-size: 12px;
    padding: 4px 10px;
    line-height: 12px;
    border-radius: 4px;
    background: #f4f4f4;
    color: #999;
    margin-left: 5px;
  text-decoration: none;
}
.footerInfo .iconSpan:hover {
    background: #51e0c1;
    color: #fff;
}</style><h1>${title}</h1><p><!--标签--></p><p><span style="display:inline-block;float:left;">作者：${author}</span>
    #foreach($tag in ${tagsList})       <a class="iconSpan" data-id="${tag.id}" target="_blank">${tag.name}</a>
    #end</p><p><!--文章正文--></p><p>${content}</p><p><!--一些常量--></p><p>${aaa}------${bb}</p><p><!--文章来源版权信息--></p><p class="theEnd">-<strong>THE END</strong>-</p><p> 
		</p><p> 
 			</p><p>版权声明：本文相关版权归原作者及来源网站所有。</p><p> 
 			</p><p><a href="javascript:;" data-href="${link}" title="查看原文" class="channelSite jumpHref">[ 查看原文 ]</a></p><p> 
		</p><!--百度自动推送--><script>(function(){
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        }
        else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();</script><script type="text/javascript" src="/resources/js/common/viewArticle.js"></script>