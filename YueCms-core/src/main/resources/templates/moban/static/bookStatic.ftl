<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/resources/SemanticUI/semantic.min.css">
    <link rel="stylesheet" href="/resources/myblog/assets/css/myblog.css">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script src="/resources/SemanticUI/semantic.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
</head>
<body  style="background: #e9ecf3;">

<!-- content srart -->
<div class="pusher" style="background: #e9ecf3;">
    <!-- nav start -->
    <script>
        var sys_ctx="";
    </script>
    <nav id="nav">
        <div class="ui  inverted menu" style="height: 50px;margin: 0;">

            <div class="header item">
                <div style="display: block;float: left;margin-right: 20px;">
                    <img style="width: 50px;height:50px;display: inline-block;" src="/resources/images/logo/logo.png">
                    <span style="display: inline-block;color:#fff;font-size: 1.3rem;position: relative;bottom: 18px;">我的博客</span>
                </div>
            </div>
            <a id="nav_index" class="item" href="/myblog/index">首页</a>
            <a class="item" href="/myblog/aboutMe">关于我</a>
            <a class="item" href="/myblog/bloglist">博客</a>


            <a id="myblog_img" class="item" href="/myblog/img" style="display: none;">相册</a>

            <a id="nav_book" class="item" href="/myblog/booklist">读书</a>
            <a class="item" href="/myblog/updateLog">更新日志</a>
            <a class="item" href="/myblog/demo/201807/liuyan">留言</a>

            <div class="right menu">
                <div class="item" id="div_search">
                    <div class="ui transparent inverted icon input">
                        <input type="text" class="searchInput"  placeholder="Search">
                        <div id="searchbtn"> <i class="search icon" style="cursor:pointer;color:#666;"></i></div>
                    </div>
                </div>
                <div id="newArticle"  class="item">
                    <a class="ui primary button" onclick="writeBlog()">
                        写博客
                    </a>
                </div>


                <div class="ui dropdown item" tabindex="0"  id="login_user_info" style="display: none;">
                    <span id="login_username">${r'${front_yhmc!}'}</span><i class="dropdown icon"></i>
                    <div class="menu" tabindex="-1">
                        <a class="item" href="/myblog/personalBlog">个人博客</a>
                        <a class="item" href="/myblog/admin/postlist">管理博客</a>
                        <a class="item" href="/admin/login">登录后台</a>
                        <a class="item" href="/myblog/logout">退出</a>
                    </div>
                </div>

                <div class="item" id="login_form">
                    <div class="ui tiny buttons">
                        <button class="ui positive basic button" onclick="headerLogin()">登录</button>
                        <button class="ui orange  basic button" onclick="javascript:alert('功能在开发中...');">注册</button>
                    </div>
                </div>


            </div>

            <script>
                var loginState='0';
                var username='';
                function checkLoginState(){
                    sys_ajaxPost("/api/loginState",null,function(data){

                        if(data==1||data=='1'){//已经登录
                            loginState='1';
                            $("#login_user_info").show();
                            $("#login_form").hide();
                            username=getCookie('markbro_username');
                            $("#comment_from").show();
                            $("#comment_from_login").hide();

                            $("#myblog_img").show();
                            $("#login_username").html(username);
                        }else{//未登录
                            $("#login_user_info").hide();
                            $("#login_form").show();

                            $("#comment_from").hide();
                            $("#comment_from_login").show();
                        }
                    })
                }
                $(document).ready(function() {
                    checkLoginState();
                    // 鼠标放到 dropdown 时显示下拉菜单，默认只有点击后才显示
                    $('.dropdown.item').dropdown({
                        on: 'hover'
                    });
                    var n=($("#mobileNavSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
                    $(".toggle-mobile-nav-sidebar").on("click",function(e){n.sidebar("toggle"),e.preventDefault()});
                    var a=($("#mobileNavRightSidebar")).sidebar({dimPage:!0,transition:"overlay",mobileTransition:"overlay",useLegacy:"auto",duration:50,exclusive:!0})
                    $(".current-user-avatar").on("click",function(e){a.sidebar("toggle"),e.preventDefault()});
                });

                var url=  window.location.href;
                var host=window.location.host;
                var protocol=window.location.protocol;
                var uri=url.replace(protocol+"//"+host,"");
                function headerLogin(){
                    window.location.href='/myblog/login?redirectUrl='+uri;
                }
                function writeBlog(){
                    window.location.href='/myblog/newblog';
                }
                function getCookie(name){
                    //获取cookie字符串
                    var strCookie=document.cookie;
                    //将多cookie切割为多个名/值对
                    var arrCookie=strCookie.split("; ");
                    var value="";
                    //遍历cookie数组，处理每个cookie对
                    for(var i=0;i<arrCookie.length;i++){
                        var arr=arrCookie[i].split("=");
                        if(name==arr[0]){
                            value=arr[1];
                            break;
                        }
                    }
                    return value;
                }
                function removeNavClass(){
                    var lis = document.getElementById("nav").getElementsByTagName("a");
                    for (var i = 0; i < lis.length; i++) {
                        removeClass(lis[i],"active");
                    }
                }
                function showHide(uri){
                    uri=uri.replace("/myblog","");
                    if(uri.indexOf("?")>0){
                        uri=uri.substring(0,uri.indexOf("?"));
                    }
                    if(uri.indexOf("/admin")>=0){
                        $("#div_search").hide();
                    }else{
                        $("#div_search").show();
                    }
                    if(uri.indexOf("blog")>=0||uri.indexOf("personalBlog")>=0||uri.indexOf("/admin")>=0){//写博客按钮 显示
                        document.getElementById("newArticle").style.display="block";
                    }else{
                        document.getElementById("newArticle").style.display="none";
                    }
                }
                function addNavClass(uri){
                    uri=uri.replace("/myblog","");
                    if(uri.indexOf("?")>0){
                        uri=uri.substring(0,uri.indexOf("?"));
                    }
                    var lis = document.getElementById("nav").getElementsByTagName("a");
                    for (var i = 0; i < lis.length; i++) {

                        if(lis[i].href.indexOf(uri)>=0){
                            lis[i].className="item active";
                            break;
                        }
                    }
                }
                function removeClass(elem,classname){
                    if(elem.className != ""){
                        var allClassName = elem.className.split(" ");
                        //console.log("第一次赋值后class数量为：" + allClassName);
                        var result;//完成操作后保存类名（在以后调用）
                        var listClass;//保存修改过后的数组
                        for (var i = 0; i < allClassName.length; i++) {
                            if(allClassName[i] == classname){
                                allClassName.splice(i,1);
                            }
                        }
                        listClass = allClassName;
                        for (var j = 0; j < listClass.length; j++) {
                            //之后加上空格因为类名的存在形式就是用空格分隔的
                            if (j == 0) {
                                result = listClass[j];
                                result += " ";
                            }else{
                                result += listClass[j];
                                result += " ";
                            }
                        }
                        // console.log("处理后的listClass数量" + listClass);
                        elem.className = result;//将目标元素的类名重新被 result 覆盖掉
                    }else{
                        //console.log("目标元素没有存在的类名")
                    }
                }
                setTimeout(function(){
                    showHide(uri);
                },5);
                setTimeout(function(){
                    removeNavClass();
                    addNavClass(uri)
                    if(uri=="/login"||uri=='/myblog/index'||uri=='/myblog'){
                        document.getElementById("nav_index").className="active item";
                    }
                    if(uri.indexOf("book/detail")>0){
                        document.getElementById("nav_book").className="active item";
                    }
                },50);



            </script>

        </div>
    </nav>
    <div class="ui secondary inverted  menu" id="mobileHeaderNavMenu" style="display: none;">
        <a class="icon item toggle-mobile-nav-sidebar">
            <i class="large content icon"></i>
        </a>
        <!--<div class="logo back-to-top-toggle"><img src="https://www.oschina.net/new-osc/img/logo.svg" alt="开源中国"></div>-->
        <div class="logo back-to-top-toggle"> <div style="display: block;float: left;margin-right: 20px;">
            <img style="width: 40px;height:40px;display: inline-block;" src="/resources/images/logo/logo.png">
            <span style="display: inline-block;color:#fff;font-size: 1.3rem;position: relative;bottom: 12px;">我的博客</span>
        </div>
        </div>
        <a class="icon item toggle-mobile-user-sidebar">

            <div class="osc-avatar small-portrait _28x28 ui avatar image current-user-avatar" style="display: none">


                <img src="/resources/images/headicons/girl-1.png">

            </div>

        </a>
    </div>


    <!-- nav end -->
    <div class="ui basic segment" id="mainScreen" >
        <div class="ui container bg-white">
            <div class="ui grid" >
                <div class="row" >
                    <div class="sixteen wide tablet eleven wide computer column">
                        <div class="ui items">
                            <div class="item">
                                <div class="big image">
                                    <img src="${book.sheet_url!}">
                                </div>
                                <div class="middle aligned content">
                                    <h4 class="header">${book.bookname!}</h4>
                                    <div class="meta" style="margin: 1.2rem auto;">
                                        <span>作者：&nbsp;${book.author!}</span>
                                        <span class="right floated time">添加时间:&nbsp;&nbsp;${book.createTime!}</span>
                                    </div>
                                    <div class="description">
                                        <p>${book.description!}</p>
                                    </div>
                                    <div class="extra">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="ui divider"></div>
                   
                      <#if book.chapterDetails?? &&  (book.chapterDetails?size>0)>
                        <div class="ui two columns grid chapterDetails">
                         
                          <#list book.chapterDetails as item>
                            <div class="column">
                                <a style="text-align: left;" class="fluid ui button primary"  href="/front/static/book/${book.id!}/detail/${item.sort!}.html">${item.sort!} . ${item.title!}</a>
                            </div>
                            </#list>
                        </div>
                        <#else>
                        <p style="text-align: center">暂无内容!</p>
                        </#if>
                        <div class="ui divider"></div>

                        <!--评论区域 start-->
                        <div  id="comment_area"  class="ui container " style="background-color: #fff;margin-top:15px;padding-bottom:15px;margin: 0 auto;">
                            <h4 class="ui header">评论book</h4>
                          
 							<#if book.comment_flag== 1 >
                            <form class="ui reply form" style="display: none" id="comment_from">
                                <div class="field">
                                    <textarea rows="5" placeholder="说点什么吧..." id="comment_content"></textarea>
                                </div>
                                <div  onclick="comment_book()" class="ui blue labeled submit icon button">
                                    <i class="icon edit"></i> 发表评论
                                </div>
                            </form>


                            <form class="ui reply form" id="comment_from_login">
                                <div class="field">
                                    <textarea readonly ></textarea>
                                    <div style="margin:0 auto;position: absolute;top:50%;left:50%;transform: translate(-50%,-100%); ">
                                        <small> 你需要登录后才能进行评论</small>
                                        <div class="ui tiny buttons" style="line-height: 0.8;">
                                            <button class="ui positive basic button" onclick="commentLogin()">登录</button>
                                            <button class="ui orange  basic button" onclick="javascript:alert('功能在开发中...');">注册</button>
                                        </div>
                                    </div>
                                </div>
                                <div    class="ui  labeled submit icon button">
                                    <i class="icon edit"></i> 发表评论
                                </div>
                            </form>

                            <!--评论列表 start-->
                            <script type="text/javascript" src="/resources/js/common/jubao.js"></script>
                            <div class="ui comments"  id="comment_list">
                                <h4 class="ui dividing header">评论列表</h4>
                                <script type="text/javascript">
                                    function up_vote(obj,id){
                                        if(loginState=='0'){
                                            $.layer.msg_cry("请先登录!");
                                            return;
                                        }
                                        $this=$(obj);
                                        $this.removeAttr("onclick");
                                        $this.css("color","green");
                                        // $this.siblings().removeAttr("style");
                                        upDownVote(id,"up");
                                        $this.find(".my-add-num").html("<em class='add-animation'>+1</em>").show();
                                    }
                                    function show_jubao(targetId){
                                        if(loginState=='0'){
                                            $.layer.msg_cry("请先登录!");
                                            return;
                                        }
                                        jubao(targetId);
                                    }
                                    //用户需要重写覆盖jubao.js的提交方法
                                    function jubaoSubmit(target_id){
                                        var report_type=getJuBaoValue();
                                        sys_ajaxPost("/cms/report/json/doReport",{report_type:report_type,target_id:target_id},function(data){
                                            broAjaxReturnMsg(data,null,function(){
                                                layer.closeAll();
                                                jubaoSuccess();
                                            });
                                        })
                                    }
                                    function down_vote(obj,id){
                                        if(loginState=='0'){
                                            $.layer.msg_cry("请先登录!");
                                            return;
                                        }
                                        $this=$(obj);
                                        $this.removeAttr("onclick");
                                        $this.css("color","red");
                                        upDownVote(id,"down");

                                    }
                                    function upDownVote(id,type,obj,a){

                                        var params="?id="+id+"&type="+type;
                                        $.ajax({
                                            type: 'POST',
                                            url: '/cms/comment/json/votes'+params,
                                            data: null,
                                            contentType: 'application/json',
                                            success:function(data){
                                                broAjaxReturnMsg(data,null,function(){//成功的回调
                                                    $this.find(".my-add-num").html("<em class='add-animation'>+1</em>").show();//+1动画效果
                                                    if(type=="down"){
                                                        var num=Number($("#down_num_"+id).text());
                                                        num+=1;
                                                        $("#down_num_"+id).text(num);
                                                        //禁用按钮几秒钟
                                                    }
                                                    if(type=="up"){
                                                        var num=Number($("#up_num_"+id+"").html());
                                                        num+=1;
                                                        $("#up_num_"+id).text(num);
                                                    }
                                                });
                                            }
                                        })
                                    }
                                    function listComments(rows){
                                        if(rows.length>0){
                                            //$('#comment_list').html("");
                                            $.each(rows,function(i,json){
                                                var num=(pageNo-1)*limit+i+1;
                                                var html="<div class=\"comment\" data-id=\""+json.id+"\">";
                                                html+=" <a class=\"avatar\">";
                                                html+="<img src=\""+json.usericon+"\">";
                                                html+="</a>";
                                                html+="<div class=\"content\">";
                                                html+="<a class=\"author\">"+json.username+"</a>";
                                                html+="<div class=\"metadata\">";
                                                html+="<span class=\"date\">"+json.createTime+"</span>";
                                                html+="<span class=\"floor\">#"+num+"</span>";
                                                html+="</div>";
                                                html+="<div class=\"text\">"+json.comment+"</div>";
                                                html+="<div class=\"actions\">";
                                                html+="<div class=\"ui horizontal list\">";

                                                html+="<div class=\"item\">";
                                                html+="<a class='comment_tool_btn'  onclick='up_vote(this,\""+json.id+"\")'><i class=\"icon_comment icon_ding\"></i><span class='up_down_num' id=\"up_num_"+json.id+"\">"+json.up_vote+"</span><span  class=\"my-add-num\"></span></a>";
                                                html+="</div>";
                                                html+="<div class=\"item\">";
                                                html+="<a class='comment_tool_btn'  onclick='down_vote(this,\""+json.id+"\")'><i  class=\"icon_comment icon_cai\"></i><span class='up_down_num' id=\"down_num_"+json.id+"\">"+json.down_vote+"</span><span  class=\"my-add-num\"></span></a>";
                                                html+="</div>";
                                                html+="<div class=\"item\">";
                                                html+="<a class='comment_tool_btn'><i class=\"icon_comment icon_yinzhang\"></i></a>";
                                                html+="</div>";
                                                html+="<div class=\"item\">";
                                                html+="<a class='comment_tool_btn comment_tool_btn_flag'   onclick='show_jubao("+json.id+")'><i class=\"icon_comment icon_flag\"></i><span>举报</span></a>";
                                                html+="</div>";
                                                html+="</div>";
                                                html+="</div>";
                                                html+="</div>";
                                                html+="</div>";

                                                $(html).hide().appendTo($('#comment_list')).show();
                                            })
                                        }else{
                                            var html="";
                                            html+='<div>';
                                            html+='<p style="text-align: center;">暂无评论!</p>';
                                            html+='</div>';
                                            $('#comment_list').html(html);
                                        }
                                    }
                                    var pageNo=1;
                                    var limit=10;
                                    var totalPages=0;
                                    var book_id = "${book.id!}";
                                    function queryComments(successCallBack){
                                        var params="?page="+pageNo+"&rows="+limit+"&book_id="+book_id+"&myblog=1";
                                        $.ajax({
                                            type: 'POST',
                                            url: '/myblog/blog/comment/list'+params,
                                            data: null,
                                            contentType: 'application/json',
                                            success:function(data){
                                                var json=typeof data=='string'?JSON.parse(data):data;
                                                totalPages=json.totalPages;
                                                var rows=json.rows;
                                                listComments(rows);
                                                //alert(JSON.stringify(json.items));
                                                if(typeof successCallBack=='function'){
                                                    successCallBack();
                                                }
                                            }
                                        })
                                    }
                                    function successCallBack(){
                                        if(totalPages==0){
                                            $("#btn_more_comments").hide();
                                            return;
                                        }
                                        if(pageNo>=totalPages){
                                            $("#btn_more_comments").attr("onclick","").removeClass("primary").html("已展开所有评论");
                                        }else{
                                            $("#btn_more_comments").attr("onclick","moreComments()").html("<i class=\"angle double down icon\"></i>&nbsp&nbsp更多");
                                        }
                                    }

                                    function moreComments(){
                                        pageNo++;
                                        $("#btn_more_comments").attr("onclick","").html("<i class=\"spinner icon\"></i>&nbsp&nbsp加载中");
                                        queryComments(successCallBack);
                                    }
                                    function pager(){
                                        //alert(totalPages+"==="+pageNo);
                                        $("#pager").page({ pages: totalPages,theme:"success" ,
                                            curr: pageNo , //当前页
                                            groups: 0, //连续显示分页数
                                            prev: false, //若不显示，设置false即可，默认为上一页
                                            next: '更多',
                                            render: function(context, $el, index) { //渲染[context：对this的引用，$el：当前元素，index：当前索引]
                                                //逻辑处理
                                                if (index == 'last') { //虽然上面设置了last的文字为尾页，但是经过render处理，结果变为最后一页
                                                    $el.find('a').html('没有更多了');
                                                    return $el; //如果有返回值则使用返回值渲染
                                                }
                                                return false; //没有返回值则按默认处理
                                            },
                                            jump:function(context,first) {
                                                pageNo = context.option.curr;
                                                if (!first) {
                                                    queryComments(successCallBack);
                                                }
                                            }
                                        });
                                    }
                                    $(function(){
                                        queryComments(successCallBack);
                                    })
                                </script>
                            </div>
                            <div id="btn_more_comments"  class="ui bottom primary attached button" onclick="moreComments()">
                                <i class="spinner icon"></i>&nbsp&nbsp加载中 <i class="angle double down icon"></i>&nbsp&nbsp更多
                            </div>
                            <!--评论列表 end-->
                            <script type="text/javascript">
                                function commentLogin(){
                                    var url=  window.location.href;
                                    var host=window.location.host;
                                    var protocol=window.location.protocol;
                                    var uri=url.replace(protocol+"//"+host,"");
                                    var href=protocol+"//"+host+'/myblog/login?redirectUrl='+uri+"%23comment_area";
                                    //alert(href);
                                    window.location.href=href;
                                }
                                function comment_book(){

                                    var comment_content = document.getElementById("comment_content").value;

                                    var book_id = "${book.id!}";

                                    if(comment_content==''){
                                        $.layer.msg("请输入评论内容!");
                                        return;
                                    }
                                    if(comment_content.length<5){
                                        //$.layer.alert("不如再多写点!");
                                        $.layer.msg("不如再多写点!");
                                        return;
                                    }
                                    var params="?book_id="+book_id+"&content="+comment_content;
                                    $.ajax({
                                        type: 'POST',
                                        url: '/myblog/blog/comment'+params,
                                        data: null,
                                        contentType: 'application/json',
                                        success:function(data){
                                            var json=JSON.parse(data.replace("\r\n",""));
                                            if(json.type == "success"){
                                                appendComment(json);
                                                $("#comment_content").val("");
                                                $("#btn_comment").addClass("am-disabled").removeClass("am-btn-primary");
                                                setTimeout(function(){
                                                    $("#btn_comment").removeClass("am-disabled").addClass("am-btn-primary");
                                                },5000);
                                            }else{
                                                $.layer.alert(json.msg);
                                            }
                                        }
                                    })
                                }

                                function appendComment(json){
                                    //console.log(json);
                                    var html="<div class=\"comment\" data-id=\""+json.comment_id+"\">";
                                    html+=" <a class=\"avatar\">";
                                    html+="<img src=\""+json.headPath+"\">";
                                    html+="</a>";
                                    html+="<div class=\"content\">";
                                    html+="<a class=\"author\">"+json.author+"</a>";
                                    html+="<div class=\"metadata\">";
                                    html+="<span class=\"date\">"+json.time+"</span>";
                                    html+="</div>";
                                    html+="<div class=\"text\">"+json.comment+"</div>";
                                    html+="<div class=\"actions\">";

                                    html+="<a class='comment_tool_btn'  onclick='up_vote(this,"+json.comment_id+")'><i class=\"icon_comment icon_ding\"></i><span class='up_down_num' id=\"up_num_"+json.comment_id+"\">0</span><span  class=\"my-add-num\"></span></a>";
                                    html+="<a class='comment_tool_btn'  onclick='down_vote(this,"+json.comment_id+")'><i  class=\"icon_comment icon_cai\"></i><span class='up_down_num' id=\"down_num_"+json.comment_id+"\">0</span><span  class=\"my-add-num\"></span></a>";
                                    html+="<a class='comment_tool_btn'><i class=\"icon_comment icon_yinzhang\"></i></a>";
                                    html+="<a class='comment_tool_btn comment_tool_btn_flag' onclick='show_jubao("+json.comment_id+")'><i class=\"icon_comment icon_flag\"></i><span>举报</span></a>";
                                    html+="</div>";
                                    html+="</div>";
                                    html+="</div>";

                                    $(html).hide().prependTo($('#comment_list')).show();
                                }
                            </script>

                            <#else>
                            <div class="ui container">
                                <p style="text-align: center;">评论已经关闭！</p>
                            </div>
                            </#if>
                        </div>
                        <!--评论区域 end-->


                    </div>

                    <!--右侧-->
                    <div class="sixteen wide tablet five wide computer column"  style="background:#fff;border-left: 1px dashed rgba(34,36,38,.1);">
                        <div class="ui divided items">
                            <div class="item">
                                <div class="ui tiny image">
                                    <img src="https://semantic-ui.qyears.com/images/wireframe/image.png">
                                </div>
                                <div class="middle aligned content">
                                    内容 A
                                </div>
                            </div>
                            <div class="item">
                                <div class="ui tiny image">
                                    <img src="https://semantic-ui.qyears.com/images/wireframe/image.png">
                                </div>
                                <div class="middle aligned content">
                                    内容 B
                                </div>
                            </div>
                            <div class="item">
                                <div class="ui tiny image">
                                    <img src="https://semantic-ui.qyears.com/images/wireframe/image.png">
                                </div>
                                <div class="middle aligned content">
                                    <div class="description">
                                        11111111
                                    </div>
                                    <div class="extra">
                                        <div class="ui tiny right floated orange basic button">
                                            Action
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>



    <!-- content end -->

    <footer>
        <div class="ui black inverted vertical footer segment">
            <div class="ui center aligned container">
                <div class="ui stackable inverted grid">
                    <div class="column">
                        Semantic UI 最终版权归原网站所有：<a style="color:#FFF" href="http://www.semantic-ui.com">www.semantic-ui.com</a>。
                        本网站在翻译过程中若有任何侵权行为，请联系16853197@qq.com，我们定将及时处理。<p></p>
                    </div>
                </div>
                <div class="ui inverted section divider"></div>
                <img src="/resources/images/logo/logo.png" class="ui centered mini image">
                <div class="ui horizontal inverted small divided link list">
                    <a style="color:#FFF" class="item" >@2016-2018 我的博客 All Rights Reserved</a>
                </div>
            </div>
        </div>
    </footer>

    <div id="fixed">
        <a rel="nofollow" target="_blank"  class="contact-kf"><img src="/resources/myblog/images/rdhl3.png" alt="">联系<br>客服</a>
        <a rel="nofollow" href="javascript:void(0);" class="fankui"><img src="/resources/myblog/images/rdhl4.png" alt="">意见<br>反馈</a>
        <div rel="nofollow" class="back-top" id="gotop" style="display: block;"><i></i><span>返回顶部</span></div>
    </div>

</div>
</body>
</html>