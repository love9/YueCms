<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<title>${bookname}</title>
	<link href="/resources/css/channelSite.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/js/common/channelSite.js"></script>
 <style>
 @media screen and (max-width: 768px){
     .page{
      width: 100%;
      height:calc(100% - 30px);
      overflow-y:auto;
      margin: 0 auto;
      margin-top:15px;
      padding: 10px 18px;
      background-color: #fff;
      box-shadow: #5079d9 2px 1px 7px 6px;

 	}
}
 .page{

 	width: 70%;
    height:calc(100% - 30px);
    overflow-y:auto;
    margin: 0 auto;
    margin-top:15px;
    padding: 10px 18px;
    background-color: #fff;
    box-shadow: #5079d9 2px 1px 7px 6px;
   
 }

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    transition: all 0.2s ease;
    background-image: -webkit-linear-gradient(180deg, rgba(0,0,0,0.05) 5%, rgba(0,0,0,0.85) 100%);
    background-image: -moz-linear-gradient(180deg, rgba(0,0,0,0.05) 5%, rgba(0,0,0,0.85) 100%);
    background-image: linear-gradient(180deg, rgba(0,0,0,0.05) 5%,rgba(0,0,0,0.85) 100%);
    background-size: 100%;
}

.iconChannelSite {
    display: inline-block;
    width: 50px;
    height: 50px;
    background: url(/resources/images/logo/logo_water.png) no-repeat center;
    background-size: 50px;
    -moz-border-radius: 50%;
    border-radius: 50%;
    overflow: hidden;
}
/*标签*/
 .footerInfo.top {
    text-align: right;
    border-top: 0;
    margin-top: 0;
}

 .footerInfo {
    margin-top: 20px;
    padding-top: 20px;
    border-top: 1px dotted #ddd;
}
 .footerInfo .iconSpan {
    cursor:pointer;
    font-size: 12px;
    padding: 4px 10px;
    line-height: 12px;
    border-radius: 4px;
    background: #f4f4f4;
    color: #999;
    margin-left: 5px;
  text-decoration: none;
}
.footerInfo .iconSpan:hover {
    background: #51e0c1;
    color: #fff;
}

#at{   
margin-top:14px;
border-collapse:collapse;
	overflow: hidden;
    position: relative;
    height: 135px;
    line-height: 30px;
     color: #666;
    background: transparent;
    font: 12px/120% 微软雅黑,宋体,Verdana,Arial,sans-serif;
}

#at tr th{background-color:#E4E4E4;padding:3px 12px;}
#at tr td{    background-color: #f4f4f4;min-width:100px;padding:3px 5px 3px 5px}
 </style>
</head>
<body style="background: #e9ecf3;">
<div class="page">
    
  	<h1 style="text-align:left;">${bookname}</h1>
    <hr>
    <div style="float: left;display: inline;">
        <a class="hst">
            <img style="padding:7px; border:1px solid #E4E4E4; width:120px; height:150px; margin:0 25px 0 15px;" src="${sheet_url}">
        </a>
    </div>
    <div  style="width:550px;float: left;display: inline;">
      <table cellspacing="1" cellpadding="0" bgcolor="#E4E4E4" id="at">
        <tbody><tr><th>文章类别</th><td><a  target="_blank">${tags}</a></td><th>文章作者</th><td> ${author}</td><th>文章状态</th><td> ${status}</td></tr>
        <tr><th>收 藏 数</th><td> ${favourite}</td><th>全文长度</th><td> ${totalWords}字</td><th>最后更新</th><td> ${last_update}</td></tr>
        <tr><th>总点击数</th><td> ${hit}</td><th>本月点击</th><td> ${month_hit}</td><th>本周点击</th><td> ${week_hit}</td></tr>
     
        </tbody>
      </table>
    </div>
	<div style="clear:both;"></div>
    <hr>    
  
    <!--文章简介-->
    <div class="articleDetail" style=" color: #666;background: transparent;font: 12px/120% 微软雅黑,宋体,Verdana,Arial,sans-serif;">
   		<p style="background: #F2F2F2; padding-left: 10px;padding: 8px 10px;font-size: 14px;margin:0 0 8px 0"><b>${bookname}内容简介：</b></p>
        <p style="text-indent:2em;">
         ${description}
        </p>
   		<hr>  
        <p style="background: #F2F2F2; padding-left: 10px;padding: 8px 10px;font-size: 14px;margin:0 0 8px 0"><b>${bookname}章节列表：</b></p>
          
    </div>


</div>
<!--百度自动推送-->
<script>
    (function(){
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        }
        else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
</script>   
    
</body>
</html>