<style>
  body{font-family:"Helvetica Neue",Helvetica,Tahoma,Arial,"Microsoft Yahei","Hiragino Sans GB","WenQuanYi Micro Hei",sans-serif;font-size:14px;line-height:1.6;color:#76838f;background-color:#fff}.page-email .page-content .panel{max-width:660px;margin:0 auto}.page-email .page-content .widget{padding:20px 0;margin:0}.page-email .page-content .widget-header{margin-bottom:5px}.page-email .page-content .widget-body h3{margin-bottom:20px}.page-email .page-content .widget-body-footer{margin:25px 0}.page-email .email-title{padding:15px 0 5px;text-align:center}.page-email .email-more{padding:25px 0 10px;margin-top:0;text-align:center;border-top:1px solid #e4eaec}.page-email .email-more-title{font-size:20px}.page-email .email-more-content{padding:47px 0 54px}.page-email .email-more-social{padding:20px 0}.page-email .email-more-social a{padding:5px;margin:0 7px;text-decoration:none}.page-email .email-more-social .icon{font-size:16px;color:rgba(55,71,79,0.4)}.page-email .email-more-social .icon:focus,.page-email .email-more-social .icon:hover{color:rgba(55,71,79,0.6)}.page-email .email-more-social .icon.active,.page-email .email-more-social .icon:active{color:#37474f}.page-email .email-unsubscribe{text-decoration:underline}.panel{margin-bottom:22px;background-color:#fff;border:1px solid transparent;border-radius:0;-webkit-box-shadow:0 1px 1px rgba(0,0,0,.05);box-shadow:0 1px 1px rgba(0,0,0,.05)}.panel-body{position:relative}.panel-body{padding:20px 20px}.container-fluid{padding-right:12px;padding-left:12px;margin-right:auto;margin-left:auto}.widget{border-radius:3px}.widget{position:relative;margin-bottom:24px;background-color:#fff}.widget .widget-header{border-radius:3px 3px 0 0}.widget-body{position:relative;padding:30px 25px}.widget .cover{width:100%}.cover{overflow:hidden}.cover-image{width:100%}img{vertical-align:middle}img{border:0}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{font-weight:500}.h4,h4{font-size:16px}.h4,.h5,.h6,h4,h5,h6{margin-top:11px;margin-bottom:11px}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{font-family:"Helvetica Neue",Helvetica,Tahoma,Arial,"Microsoft Yahei","Hiragino Sans GB","WenQuanYi Micro Hei",sans-serif;font-weight:300;line-height:1.2;color:#37474f}.btn{padding:4px 12px;font-size:14px;line-height:1.6;border-radius:3px;-webkit-transition:border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;-o-transition:border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;transition:border .2s linear,color .2s linear,width .2s linear,background-color .2s linear}.btn-primary{color:#fff;background-color:#62a8ea;border-color:#62a8ea}.btn{display:inline-block;padding:4px 12px;margin-bottom:0;font-size:14px;font-weight:400;line-height:1.6;text-align:center;white-space:nowrap;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-image:none;border:1px solid transparent;border-radius:3px}a{color:#62a8ea;text-decoration:none}
</style>
<div class="page animation-fade page-email" style="margin:10px auto;width: 50%;">
    <div class="page-content" >

        <div class="panel"  style="background-color: #2a96d8 !important;">
            <div class="panel-body container-fluid">
                <div class="widget widget-article">
                    <div class="widget-header cover">
                        <img class="cover-image" src="http://cdn.admui.com/demo/iframe/1.1.0/images/photos/city-1.jpg" alt="...">
                    </div>
                    <div class="widget-body">
                        <h4>您好${username},您刚刚使用了找回密码功能:</h4>
                        <p>
                            请在<font color="red">60分钟</font>内点击下面链接设置您的新密码！
                        </p>
                        <div class="widget-body-footer">
                            <a target="_blank" class="btn btn-primary widget-readMore" href="${link}" style="color:#fff !important;text-decoration: none;">重置密码</a>
                        </div>
                        <p style="font-family:'微软雅黑','黑体',arial;font-size:12px;">
                            如果上面的链接点击无效，请复制以下链接至浏览器的地址栏直接打开。
                        </p>
                        <p style="font-family:'微软雅黑','黑体',arial;font-size:12px;">
                            <a href="${link}" target="_blank" >${link}</a>
                        </p>
                    </div>
                </div>

                <div class="email-more">

                    <p  style="color: #fff !important;">如果您不知道为什么收到了这封邮件，可能是他人不小心输错邮箱意外发给了您，请忽略此邮件。
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
