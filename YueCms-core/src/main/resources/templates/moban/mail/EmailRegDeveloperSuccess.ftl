<style type="text/css">
    *,html,body{margin:0;padding:0}html{color:#000;background:#fff}body{overflow-x:hidden}body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td,hr,button,article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{margin:0;padding:0}body,button,input,select,textarea{font:12px/1.0 "Hiragino Sans GB","Microsoft YaHei","WenQuanYi Micro Hei",sans-serif;word-wrap:break-word;color:#000}input,select,textarea{font-size:12px;padding:5px;outline:0 none}input[type="checkbox"],input[type="radio"]{border:0}table{border-collapse:collapse;border-spacing:0}th{text-align:inherit}fieldset,img{border:0}iframe{display:block}abbr,acronym{border:0;font-variant:normal}del{text-decoration:line-through}address,caption,cite,code,dfn,em,th,var{font-style:normal;font-weight:500}ol,ul,li{list-style:none}caption,th{text-align:left}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:500}q:before,q:after{content:''}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-0.5em}sub{bottom:-0.25em}a:hover{text-decoration:none}a:visited{color:#333}ins,a{text-decoration:none}button,a{cursor:pointer}hr{height:1px;border:0;border-top:1px solid #ccc}.Zebra_DatePicker *,.Zebra_DatePicker *:after,.Zebra_DatePicker *:before{-moz-box-sizing:content-box!important;-webkit-box-sizing:content-box!important;box-sizing:content-box!important}.Zebra_DatePicker *{margin:0;padding:0;color:#373737;background:transparent;border:1px solid #dfdedf}.Zebra_DatePicker table{border-collapse:collapse;border-spacing:0}.Zebra_DatePicker td,.Zebra_DatePicker th{text-align:center;padding:5px 0}.Zebra_DatePicker td{cursor:pointer}.Zebra_DatePicker .dp_daypicker td,.Zebra_DatePicker .dp_daypicker th,.Zebra_DatePicker .dp_monthpicker td,.Zebra_DatePicker .dp_yearpicker td{width:30px;height:24px}.Zebra_DatePicker .dp_daypicker th{font-weight:bold;background-color:#efefef}.Zebra_DatePicker .dp_monthpicker td{width:33%}.Zebra_DatePicker .dp_yearpicker td{width:33%}input:disabled{background-color:#ebebe4}.f_wrapper{position:relative;width:100%;height:auto;overflow-y:auto;z-index:30}.f_main{position:relative;width:640px;margin:0 auto;margin-top:20px;background-color:#FFF;box-shadow:0 1px 6px rgba(124,124,124,0.42);-moz-box-shadow:0 1px 6px rgba(124,124,124,0.42);-webkit-box-shadow:0 1px 6px rgba(124,124,124,0.42);z-index:3}.f_header{padding:0 20px 10px 20px;text-align:center;box-sizing:border-box}.f_title{font-size:24px;font-weight:bold;line-height:30px;text-shadow:0 2px 2px rgba(0,0,0,0.2)}.f_describe{font-size:12px;line-height:18px}.f_text_no_logo{padding-top:10px}.f_text_no_logo .f_describe{padding-top:10px}.f_text_no_logo .f_describe_none{padding-top:0}.f_body{width:100%;padding-bottom:20px;margin-top:20px}.f_component{padding:12px 30px 12px 30px;margin-top:10px;margin-bottom:5px;transition-property:background-color;transition-duration:200ms;transition-delay:0;transition-timing-function:ease-in-out;-webkit-transition-property:background-color;-webkit-transition-duration:200ms;-webkit-transition-delay:0;-webkit-transition-timing-function:ease-in-out;-moz-transition-property:background-color;-moz-transition-duration:200ms;-moz-transition-delay:0;-moz-transition-timing-function:ease-in-out;-o-transition-property:background-color;-o-transition-duration:200ms;-o-transition-delay:0;-o-transition-timing-function:ease-in-out}.shopItem_img img{width:100%}.spictureItem_img img{width:100%;position:absolute;top:0;left:0;z-index:1}.f_pictureImg{display:inline-block;width:100%}.f_imgDescribe{line-height:20px;color:#444}.f_submit{margin-top:20px;padding:0 30px;text-align:center}.f_submitBtn{display:inline-block;line-height:28px;padding:2px 24px;font-size:13px;text-align:center;color:#FFF;background:#fb2e5d;border-radius:2px}.f_submitBtn:hover{background:#ff003a}.f_copyright{height:20px;margin-top:20px;margin-bottom:20px;text-align:center}.f_page .f_submitBtn{margin:0 8px}
</style>
<div class="f_wrapper fs_wrapper">
    <div class="f_main fs_main">
        <div class="f_header fs_header">
            <div class="f_text_no_logo">
                <h2 class="f_title fs_title">恭喜,您申请参与审核通过</h2>

                <p class="f_describe fs_describe f_describe_none"></p>
            </div>
        </div>
        <div class="f_body fs_body">
            <div class="f_component" com-name="id_picture" name="com1" serialnum="0">
                <a class="f_pictureImg" style="cursor:default;text-align:center;">
                    <img src="http://jieerkangxidi.com/bg_developer" border="none" style="width:100%"></a>

                <p class="f_imgDescribe fs_imgDescribe" style="text-align:left"></p>
            </div>
            <div class="f_component" com-name="id_picture" name="com1" serialnum="0">
                <h4 class="f_title" style="font-size: 15px;margin-bottom: 6px;">您好!您申请参与系统开发已经审核通过:</h4>
                <p style="line-height: 1.5;font-size:14px; ">
                    您可以点击下面链接登录我们的系统后台！<h4>您的账户名：${account} 密码：${password}</h4>
                </p>
                <div class="f_submit" style="margin-bottom: 18px;">
                    <a  target="_blank"  href="${link}" class="f_submitBtn fs_submitBtn validate_submit">去登录</a></div>
                <p style="font-family:'微软雅黑','黑体',arial;font-size:12px;line-height: 1.8;">
                    如果上面的按钮点击无效，请复制以下链接至浏览器的地址栏直接打开。
                </p>
                <p style="font-family:'微软雅黑','黑体',arial;font-size:12px;line-height: 1.8;">
                    <a href="${link}" target="_blank" >${link}</a>
                </p>
            </div>

        </div>
    </div>
    <div class="f_copyright" id="fCopyright">

    </div>
</div>