<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>首页</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/css/admui/team.css" rel="stylesheet">
    <link href="/resources/css/web-icons/web-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="/resources/lib/owlCarousel/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/lib/owlCarousel/css/owl.theme.css">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <script src="/resources/lib/owlCarousel/owl.carousel.min.js"></script>
    <style>
        .owl-item img{width: 100%;}
    </style>
    <script type="text/javascript">
        var sys_ctx="";
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({items: 3,autoPlay:true,navigation: false, navigationText: ["上一个","下一个"],  autoHeight: true});

            //请求系统配置信息
            sys_ajaxPost("/sys/getSysConfig",null,function(data){
                var json=typeof data=='string'?JSON.parse(data):data;
                $.each(json,function(k,v){
                    $("#"+k).html(v);
                });
            });
        });
    </script>
    <style>
        .page-index .row .panel{height:-webkit-calc(100% - 24px);height:calc(100% - 24px)}.page-index .account-info{overflow:visible;border-bottom:none}.page-index .account-info .media-right{vertical-align:middle}@media (max-width:768px){.page-index .account-info .media-right{display:none}}.page-index .introduce-info .media{margin:0;border-bottom:none}.page-index .introduce-info .media .media-object{height:120px}.page-index .introduce-info .media .media-body{padding-left:20px}.page-index .introduce-info .media .media-body .btn{display:inline-block;margin:3px 5px 3px 0}.page-index .changelog-info .time-line{padding-left:0;list-style:none}.page-index .changelog-info .time-line:before{position:absolute;top:25px;bottom:40px;left:120px;width:1px;content:" ";background:#e4eaec}.page-index .changelog-info .time-line>li{position:relative;width:100%;margin:15px 0;overflow:hidden;line-height:24px}.page-index .changelog-info .time-line>li:before{position:absolute;top:6px;left:93px;display:block;width:14px;height:14px;content:" ";background:#fff;border:solid 2px #e4eaec;border-radius:50%}.page-index .changelog-info .time-line>li:first-child:before{background:#5cd29d;border-color:#bfedd8}.page-index .changelog-info .time-line>li:last-child:before{background:#fa7a7a;border-color:#fad3d3}.page-index .changelog-info .time-line>li time{float:left}.page-index .changelog-info .time-line>li h5{float:left;margin:0 0 0 55px;line-height:24px}.page-index .part-info .panel-body .icon{font-size:3.2em;line-height:1.6}.page-index .part-info .panel-body .label-content{margin:20px 0 10px}.page-index .part-info .panel-body .label-content .label{display:inline-block;margin:3px 0}@media (max-width:768px){.introduce-info .media-left{display:none}.introduce-info .media-body{padding-left:0!important}}
    </style>
</head>
<body class="site-page layout-full" style="">
<div class="page animation-fade page-index">
    <div class="page-content">

        <div class="row"  >
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-warning">
                            <i class="fa fa-envira"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">文章</span>
                        <div class="content-text text-center margin-bottom-0">
                            <#--<i class="text-danger icon wb-triangle-up font-size-20"> </i>-->
                            <span class="font-size-40 font-weight-100" id="count_article">399</span>
                            <#--<p class="blue-grey-400 font-weight-100 margin-0">+45% 同比增长</p>-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-danger">
                            <i class="fa fa-tags"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">标签</span>
                        <div class="content-text text-center margin-bottom-0">
                            <#--<i class="text-success icon wb-triangle-down font-size-20"> </i>-->
                            <span class="font-size-40 font-weight-100" id="count_tags">¥18,628</span>
                           <#-- <p class="blue-grey-400 font-weight-100 margin-0">+45% 同比增长</p>-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-success">
                            <i class="fa fa-book"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">书籍</span>
                        <div class="content-text text-center margin-bottom-0">
                           <#-- <i class="text-danger icon wb-triangle-up font-size-20"> </i>-->
                            <span class="font-size-40 font-weight-100" id="count_book">23,456</span>
                            <#--<p class="blue-grey-400 font-weight-100 margin-0">+25% 同比增长</p>-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 info-panel" style="height: 190px;">
                <div class="widget widget-shadow">
                    <div class="widget-content bg-white padding-20">
                        <button type="button" class="btn btn-floating btn-sm btn-primary">
                            <i class="fa fa-comments-o"></i>
                        </button>
                        <span class="margin-left-15 font-weight-400">留言数</span>
                        <div class="content-text text-center margin-bottom-0">
                            <#--<i class="text-danger icon wb-triangle-up font-size-20"> </i>-->
                            <span class="font-size-40 font-weight-100" id="count_comments">4,367</span>
                            <#--<p class="blue-grey-400 font-weight-100 margin-0">+25% 同比增长</p>-->
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $.ajax({
                    type:"get",
                    url:"/website/siteInfo",
                    success:function(json){
                        //console.log(json);
                        $("#count_article").html(json.articleCount);
                        $("#count_tags").html(json.tagCount);
                        $("#count_book").html(json.bookCount);
                        $("#count_comments").html(json.commentCount);
                    }
                });
            </script>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="well well-sm well-success" style="background-color: #1E9FFF!important;">

                     非常感谢您的使用，本系统正在不断完善中，可以向我们提出宝贵的建议或意见，它的成长需要大家的支持。
               <#-- <@authTag method="hasPermission" permission="/sys/test">123</@authTag>
                <@authTag method="hasRoles" role="system">hasRoles system</@authTag>

                ${sys.cookieNameKey!}==== ${sys.db_type}==${dict.process_valuetype!}-->
                </div>
            </div>
        </div>


        <div class="row margin-top-10" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-7" style="height: 423px;">
                <div class="panel introduce-info">
                    <div class="panel-heading">
                        <ul class="panel-actions">
                            <li>
                                <a href="http://docs.admui.com/iframe/" target="_blank">更多</a>
                            </li>
                        </ul>
                        <h1 class="panel-title">基本介绍</h1>
                    </div>
                    <div class="panel-body">
                        <div class="media">
                            <div class="media-left">
                                <a  style="display: block;background-color: #fff !important;">
                                    <img class="media-object" src="/resources/images/logo/logo.png" alt="...">
                                </a>

                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">YueCms通用管理系统快速开发框架</h3>
                                <ul class="list-group list-group-full">
                                    <li class="list-group-item">这 是一个基于最新 Web
                                        技术的企业级通用管理系统快速开发框架，可以帮助企业极大的提高工作效率，节省开发成本，提升品牌形象。
                                    </li>
                                    <li class="list-group-item">您可以 它 为基础，快速开发各种MIS系统，如CMS、OA、CRM、ERP、POS等。</li>
                                    <li class="list-group-item">紧贴业务特性，涵盖了大量的常用组件和基础功能，最大程度上帮助企业节省时间成本和费用开支。</li>
                                </ul>

                                <div>
                                    <a href="${properties.website_website!}" class="btn btn-success btn-sm btn-outline btn-round" target="_blank">
                                        返回官网
                                    </a>
                                   <!-- <a href="#" class="btn btn-warning btn-sm btn-outline btn-round" data-toggle="modal"  ">
                                        手机体验
                                    </a>
                                    <a href="#" class="btn btn-primary btn-sm btn-outline btn-round open-kf" data-toggle="modal">
                                        咨询客服
                                    </a>-->
                                    <a href="${properties.website_doc!}" class="btn btn-danger btn-sm btn-outline btn-round" target="_blank">
                                        开发文档
                                    </a>
                                   <!--<a href="#" class="btn btn-dark btn-sm btn-outline btn-round" target="_blank">
                                        服务协议
                                    </a>-->
                                </div>

                                <div style="margin-top: 38px;">
                                    <a class="btn btn-primary" href="http://wpa.qq.com/msgrd?v=3&amp;uin=747506908&amp;site=qq&amp;menu=yes" target="_blank">
                                        <i class="fa fa-qq"></i>&nbsp;&nbsp;联系我</a>
                                    <a class="btn btn-warning" href="${properties.website_website!}" target="_blank">
                                        <i class="fa fa-home"></i>&nbsp;&nbsp;访问官网</a>
                                    <a class="btn btn-warning" href="https://gitee.com/markbro/YueCms" target="_blank">
                                        <img src="/resources/images/gitee.ico" width="18" alt="">&nbsp;&nbsp; 码&nbsp;云&nbsp;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5" style="height: 423px;">
                <div class="panel changelog-info">
                    <div class="panel-heading">
                        <!--<ul class="panel-actions">
                            <li>
                                <a href="#" target="_blank">更多</a>
                            </li>
                        </ul>-->
                        <h1 class="panel-title">版本信息</h1>
                    </div>
                    <div class="panel-body">
                        <ul class="time-line">
                            <li>
                                <time datetime="2018-08-10">2018-08-10</time>
                                <h5>
                                     v1.1.0
                                        正式发布
                                </h5>
                            </li>
                            <li>
                                <time datetime="2018-05-23">2018-05-23</time>
                                <h5>
                                    v1.0.0
                                        正式发布
                                </h5>
                            </li>

                            <li>
                                <time datetime="2018-03-21">2018-03-21</time>
                                <h5> v1.0.0 Beta1 发布</h5>
                            </li>
                            <li>
                                <time datetime="2017-11-27">2017-11-27</time>
                                <h5> v0.1.1 - v0.9.5 开发</h5>
                            </li>
                            <li>
                                <time datetime="2017-03-21">2017-03-21</time>
                                <h5> v0.1.0 Beta 开发完成</h5>
                            </li>

                            <li>
                                <time datetime="2016-12-14">2016-12-14</time>
                                <h5> 正式立项</h5>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="panel" style="height: initial;">
                    <div class="panel-heading">
                        <ul class="panel-actions">
                            <li>
                                <a href="/reg/developer" target="_blank">立刻参与</a>
                            </li>
                        </ul>
                        <h3 class="panel-title">申请参与开发</h3>
                    </div>
                    <div class="panel-body">
                        <div class="steps row steps-xs">
                            <div class="step col-md-3 current">
                                <span class="step-number">1</span>
                                <div class="step-desc">
                                    <span class="step-title">参与申请</span>
                                    <p>简单填写一些您的资历信息。 &nbsp;&nbsp;&nbsp;
                                    <a href="/reg/developer" style="line-height: 1.0;" class="btn btn-warning btn-sm  btn-round" target="_blank">
                                        去申请
                                    </a>
                                    </p>
                                </div>
                            </div>
                            <div class="step col-md-3 ">
                                <span class="step-number">2</span>
                                <div class="step-desc">
                                    <span class="step-title">审核</span>
                                    <p>通知管理员审核。
                                      <!--<a href="#" style="line-height: 1.0;" class="btn btn-warning btn-sm btn-round" target="_blank">
                                            去通知
                                        </a>-->
                                    </p>
                                </div>
                            </div>
                            <div class="step col-md-3 ">
                                <span class="step-number">3</span>
                                <div class="step-desc">
                                    <span class="step-title">审核通过</span>
                                    <p>感谢您的参与!

                                    </p>
                                </div>
                            </div>
                            <div class="step col-md-3 ">
                                <span class="step-number">4</span>
                                <div class="step-desc">
                                    <span class="step-title">参与开发</span>
                                    <p>下载项目、群组讨论、定下需求、立即开发</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-12">
                <div class="panel" style="height: initial;">
                    <div class="panel-heading">
                        <h3 class="panel-title">截图展示</h3>
                    </div>
                    <div class="panel-body"　id="showImg">
                        <div class="row">
                            <div class="owl-carousel-centered owl-carousel owl-theme owl-center" data-ride="carousel" >

                                <div class="item">
                                    <img src="/resources/website/images/show/myblog_login.jpg" alt="...">
                                </div>
                                <div class="item">
                                    <img src="/resources/website/images/show/index-blog.png" alt="...">
                                </div>
                                <div class="item">
                                    <img src="/resources/website/images/show/bloglist.jpg" alt="...">
                                </div>
                                <div class="item">
                                    <img src="/resources/website/images/show/article1.jpg" alt="...">
                                </div>
                                <div class="item">
                                    <img src="/resources/website/images/show/article2.jpg" alt="...">
                                </div>
                                <div class="item">
                                    <img src="/resources/website/images/show/article3.jpg" alt="...">
                                </div>

                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
                <div class="panel" style="height: 339px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">环境信息</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="35%" style="text-align: left;">JAVA版本:</td>
                                <td style="text-align: left;" id="javaVersion">1.8</td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: left;">MYSQL版本</td>
                                <td style="text-align: left;" id="mysqlVersion">2017/01/09</td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: left;">WEB服务器</td>
                                <td style="text-align: left;" id="webVersion">2018/03/23</td>
                            </tr>
                            <tr>
                                <td  width="35%" style="text-align: left;">CPU个数</td>
                                <td style="text-align: left;" id="cpu">2019/12/01</td>
                            </tr>
                            <tr>
                                <td  width="35%" style="text-align: left;">虚拟机内存总量</td>
                                <td style="text-align: left;" id="totalMemory">2018/09/08</td>
                            </tr>
                            <tr>
                                <td  width="35%" style="text-align: left;">虚拟机空闲内存量</td>
                                <td style="text-align: left;" id="freeMemory">2016/12/10</td>
                            </tr>
                            <tr>
                                <td  width="35%" style="text-align: left;">虚拟机使用的最大内存量</td>
                                <td style="text-align: left;" id="maxMemory">2016/12/10</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel" style="height: 339px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">系统信息</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <colgroup>
                                <col width="25%">
                                <col>
                            </colgroup>
                            <tbody>
                            <tr>
                                <td  style="text-align: left;">客户端IP：</td>
                                <td  style="text-align: left;" id="clientIP">0:0:0:0:0:0:0:1</td>
                            </tr>
                            <tr>
                                <td  style="text-align: left;">服务器IP</td>
                                <td  style="text-align: left;" id="serverIP">0:0:0:0:0:0:0:1</td>
                            </tr>
                            <tr>
                                <td  style="text-align: left;">操作系统</td>
                                <td  style="text-align: left;" id="osName">Windows 76.1</td>
                            </tr>
                            <tr>
                                <td  style="text-align: left;">用户主目录</td>
                                <td  style="text-align: left;"  id="userHome">C:\Users\Administrator</td>
                            </tr>
                            <tr>
                                <td  style="text-align: left;">工作目录</td>
                                <td  style="text-align: left;" id="userDir">D:\ProgramFiles\apache-tomcat-8.0.22\bin</td>
                            </tr>
                            <tr>
                                <td  style="text-align: left;">系统目录</td>
                                <td  style="text-align: left;" id="webRootPath">C:\Users\Administrator\Desktop\wxmp\target\ROOT\</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel" style="height: 260px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">二次开发</h3>
                    </div>
                    <div class="panel-body">
                        <p>提供基于本系统的二次开发、模板定制服务，具体费用请联系我们参与开发人员。</p>
                        <p>同时，我们也提供以下服务：</p>
                        <ol style="padding: 5px 0 5px 15px;">
                            <li>网站定制开发</li>
                            <li>仿站服务</li>
                            <li>APP开发</li>
                            <li>微信开发等</li>
                            <li>......</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel" style="height: 260px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">商业授权说明</h3>
                    </div>
                    <div class="panel-body">
                        <p>商业授权后我可以获得什么？</p>
                        <ol style="padding: 5px 0 5px 15px;">
                            <li>可以用于商业网站</li>
                            <li>可以去除本页面</li>
                            <li>可以去除Powered by xxx，改成贵公司的名称!</li>
                            <li>获得更多功能；</li>
                            <li>意见或建议优先考虑；</li>
                            <li>提供技术服务支持；</li>
                            <li>……</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel" style="height: 260px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">版本信息</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <colgroup>
                                <col width="35%">
                                <col>
                            </colgroup>
                            <tbody>
                            <tr>
                                <td>系统支持</td>
                                <td>YueCms</td>
                            </tr>
                            <tr>
                                <td>当前版本</td>
                                <td>1.1.0</td>
                            </tr>
                            <tr>
                                <td>当前版本更新时间</td>
                                <td>2018-05-29</td>
                            </tr>
                            <tr>
                                <td>最新版本</td>
                                <td>2.0.0</td>
                            </tr>
                            <tr>
                                <td>最新版本更新时间</td>
                                <td>2018-09-01</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</body>
</html>