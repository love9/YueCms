<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">

    <title>开发者 - 主页</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link href="/resources/css/hplus.css" rel="stylesheet">
    <!--皮肤样式-->
    <link href="/resources/css/skin/default_index.css" rel="stylesheet">
    <link href="/resources/lib/toastr/toastr.min.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>

    
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/lib/metisMenu/jquery.metisMenu.js"></script>
    <script src="/resources/lib/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/2.1/layer.js"></script>
    <script src="/resources/js/common/myutil.js"></script>
    <script src="/resources/js/index_h.js"></script>
    <script type="text/javascript" src="/resources/js/contabs.min.js"></script>
    <script src="/resources/lib/pace/pace.min.js"></script>


    <script type="text/javascript">

       /* var path = '';
        var websocket;
        var initWebSocket=function(){
            if ('WebSocket' in window) {
                websocket = new WebSocket("ws://" + path + "/websocket");
            } else if ('MozWebSocket' in window) {
                websocket = new MozWebSocket("ws://" + path + "/websocket");
            } else {
                websocket = new SockJS("http://" + path + "/websocket/sockjs");
            }
            websocket.onopen = function(event) {
                console.log("WebSocket:已连接");
                console.log(event);
            };
            websocket.onmessage = function(event) {
                var data=JSON.parse(event.data);
                console.log("WebSocket:收到一条消息",data);
                jBox_Msg(data.text,"系统");
                // alert("系统通知："+data.text);
            };
            websocket.onerror = function(event) {
                Fast.msg("WebSocket:发生错误");
                console.log("WebSocket:发生错误 ");
                console.log(event);
            };
            websocket.onclose = function(event) {
                Fast.msg("WebSocket:已关闭");
                console.log("WebSocket:已关闭");
                console.log(event);
            }
        }
        //关闭链接---未调用
        var closeConnect=function(){
            websocket.close();
        }
        $(function(){
            initWebSocket();
        });*/

    </script>

</head>

<body class="fixed-sidebar full-height-layout gray-bg body-bg-index" style="overflow:hidden">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav id="sliderbar" class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <!--<span><img alt="image" class="img-circle" src="/resources/images/img/profile_small.jpg" /></span>-->
                               <span><img alt="image" id="headerImage" class="img-circle" src="${headerPath!}"  style="width: 70px;height:70px;" /></span>
                               <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">
                               <span class="block m-t-xs">
                                   <strong class="font-bold">
                                   ${yhmc!}
                                    </strong>
                               </span>
                                <span class="text-muted text-xs block">
<#if (isAdmin) == 'true'>
                                            <span>超级管理员|</span>
<#else>
                                            ${role!}
</#if>
                                    <b class="caret"></b></span>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="J_menuItem" href="/account/personalInfo">个人资料</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="/logout">安全退出</a>
                                </li>
                            </ul>
                        </div>
                        <div class="logo-element">H+
                        </div>
                    </li>
                <#list qxlist as qx>
                <li>
                    <#if qx.children?? >
                        <#assign children2 = qx.children />
                        <a href="#"><i class="${qx.icon!}"></i> <span class="nav-label">${qx.name!}</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <#list children2 as qx2>
                                <#if qx2.children?? >
                                <#assign children3 = qx2.children />
                                        <li>
                                            <a href="#"><i class="${qx2.icon!}"></i> <span class="nav-label">${qx2.name!}</span><span class="fa arrow"></span></a>
                                            <ul class="nav nav-third-level">
                                                <#list children3 as qx3>

                                                <#if qx3.children?? >
                                                <#assign children4 = qx3.children />
                                                            <li>
                                                                <a href="#"><i class="${qx3.icon!}"></i> <span class="nav-label">${qx3.name!}</span><span class="fa arrow"></span></a>
                                                                <ul class="nav nav-third-level">
                                                                        <#list children4 as qx4>
                                                                        <!--5级菜单-->
                                                                        <#if qx4.children??>
                                                                            <#assign children5 = qx4.children />
                                                                                <li>
                                                                                    <a href="#"><i class="${qx4.icon!}"></i> <span class="nav-label">${qx4.name!}</span><span class="fa arrow"></span></a>
                                                                                    <ul class="nav nav-third-level">
                                                                                        <#list children5 as qx5>
                                                                                            <!--6级菜单-->
                                                                                                <#if qx5.children??>
                                                                                                <#assign children6 = qx5.children />
                                                                                                    <li>
                                                                                                        <a href="#"><i class="${qx5.icon!}"></i> <span class="nav-label">${qx5.name!}</span><span class="fa arrow"></span></a>
                                                                                                        <ul class="nav nav-third-level">
                                                                                                            <#list children6 as qx6>
                                                                                                                <li>
                                                                                                                <#if qx6.target?? && qx6.target!='' && qx6.target!='undefined'>
                                                                                                                    <a href="${qx6.url!}" target="${qx6.target!}"><i class="${qx6.icon!}"></i> <span class="nav-label">${qx6.name!}</span></a>
                                                                                                                <#else>
                                                                                                                    <a class="J_menuItem" href="${qx6.url!}" ><i class="${qx6.icon!}"></i> <span class="nav-label">${qx6.name!}</span></a>
                                                                                                                </#if>
                                                                                                                </li>
                                                                                                            </#list>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                <#else>
                                                                                                    <li>
                                                                                                        <#if qx5.target?? && qx5.target!='' && qx5.target!='undefined' >
                                                                                                        <a href="${qx5.url!}" target="${qx5.target!}"><i class="${qx5.icon!}"></i> <span class="nav-label">${qx5.name!}</span></a>
                                                                                                        <#else>
                                                                                                        <a class="J_menuItem" href="${qx5.url!}" ><i class="${qx5.icon!}"></i> <span class="nav-label">${qx5.name!}</span></a>
                                                                                                    </#if>
                                                                                                    </li>
                                                                                                 </#if>
                                                                                            </#list>
                                                                                    </ul>
                                                                                </li>
                                                                            <#else>
                                                                                <li>
                                                                                    <#if qx4.target==''||qx4.target=='undefined'||qx4.target==null>
                                                                                            <a class="J_menuItem" href="${qx4.url!}" ><i class="${qx4.icon!}"></i> <span class="nav-label">${qx4.name!}</span></a>
                                                                                    <#else>
                                                                                            <a href="${qx4.url!}" target="${qx4.target!}"><i class="${qx4.icon!}"></i> <span class="nav-label">${qx4.name!}</span></a>
                                                                                    </#if>
                                                                                </li>
                                                                            </#if>
                                                                        </#list>
                                                                </ul>
                                                            </li>
                                                        <#else>
                                                            <li>
                                                                <#if qx3.target?? && qx3.target!=''&& qx3.target!='undefined'>
                                                                <a href="${qx3.url!}" target="${qx3.target!}"><i class="${qx3.icon!}"></i> <span class="nav-label">${qx3.name!}</span></a>
                                                                <#else>
                                                                <a class="J_menuItem" href="${qx3.url!}" ><i class="${qx3.icon!}"></i> <span class="nav-label">${qx3.name!}</span></a>
                                                            </#if>
                                                            </li>
                                                        </#if>

                                                    </#list>
                                            </ul>
                                        </li>
                                    <#else>
                                        <li>
                                    <#if qx2.target?? && qx2.target!=''&& qx2.target!='undefined'>
                                            <a href="${qx2.url!}" target="${qx2.target!}"><i class="${qx2.icon!}"></i><span class="nav-label">${qx2.name!}</span></a>
                                    <#else>
                                            <a class="J_menuItem" href="${qx2.url!}" ><i class="${qx2.icon!}"></i> <span class="nav-label">${qx2.name!}</span></a>
                                    </#if>
                                        </li>
                                    </#if>
                                </#list>
                            </li>
                        </ul>
                    <#else>
                        <#if qx.target?? && qx.target!=''&&qx.target!='undefined'>
                            <a href="${qx.url}" target="${qx.target!}"><i class="${qx.icon!}"></i> <span class="nav-label">${qx.name!}</span></a>
                        <#else>
                            <a class="J_menuItem" href="${qx.url!}" ><i class="${qx.icon!}"></i> <span class="nav-label">${qx.name!}</span></a>
                        </#if>
                    </#if>
                </li>
            </#list>

                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="page-wrapper gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#" style="display: none"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" style="margin-bottom: 0;" method="post" action="http://www.zi-han.net/theme/hplus/search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li class="m-t-xs">
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="/resources/images/img/a7.jpg">
                                        </a>
                                        <div class="media-body">
                                            <small class="pull-right">46小时前</small>
                                            <strong>小四</strong> 这个在日本投降书上签字的军官，建国后一定是个不小的干部吧？
                                            <br>
                                            <small class="text-muted">3天前 2014.11.8</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="/resources/images/img/a4.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right text-navy">25小时前</small>
                                            <strong>国民岳父</strong> 如何看待“男子不满自己爱犬被称为狗，刺伤路人”？——这人比犬还凶
                                            <br>
                                            <small class="text-muted">昨天</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a class="J_menuItem" href="/cms/siteEmail">
                                            <i class="fa fa-envelope"></i> <strong> 查看所有消息</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="mailbox.html">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> 您有16条未读消息
                                            <span class="pull-right text-muted small">4分钟前</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            <i class="fa fa-qq fa-fw"></i> 3条新回复
                                            <span class="pull-right text-muted small">12分钟钱</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a class="J_menuItem" href="notifications.html">
                                            <strong>查看所有 </strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!--<li class="hidden-xs">
                            <a href="index_v1.html" class="J_menuItem" data-index="0"><i class="fa fa-cart-arrow-down"></i> 购买</a>
                        </li>-->
                        <li id="div_showhideFullScreen">
                            <!--<a><i class="fa fa-remove"></i> 退出全屏</a>-->
                            <a  ><i class="fa fa-arrows-alt"></i>   全屏  </a>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a class="right-sidebar-toggle" aria-expanded="false">
                                <i class="fa fa-tasks"></i> 主题
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
            <div class="row content-tabs">
                <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
                </button>
                <nav class="page-tabs J_menuTabs">
                    <div class="page-tabs-content">
                        <a href="javascript:;" class="active J_menuTab" data-id="/sys/front">首页</a>
                    </div>
                </nav>
                <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
                </button>
                <div class="btn-group roll-nav roll-right">
                    <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span>

                    </button>
                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
                        <li class="J_tabShowActive"><a>定位当前选项卡</a>
                        </li>
                        <li class="divider"></li>
                        <li class="J_tabCloseAll"><a>关闭全部选项卡</a>
                        </li>
                        <li class="J_tabCloseOther"><a>关闭其他选项卡</a>
                        </li>
                    </ul>
                </div>
                <a href="/logout" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
            </div>
            <div class="row J_mainContent" id="content-main">
                <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="/sys/front" frameborder="0" data-id="/sys/front" seamless></iframe>
            </div>

            <div class="footer">

                    <p style="text-align:center;">Copyright ©2016-<script type="text/javascript">
                        document.write(new Date().getFullYear());
                    </script> Powered By WuJiyue.
                    </p>
            </div>
        </div>
        <!--右侧部分结束-->
        <!--右侧边栏开始-->
        <div id="right-sidebar" class="right-sidebar">
            <div class="sidebar-container">

                <ul class="nav nav-tabs navs-3">

                    <li class="active">
                        <a data-toggle="tab" href="#tab-1">
                            <i class="fa fa-gear"></i> 主题
                        </a>
                    </li>
                    <li class=""><a data-toggle="tab" href="#tab-2">
                        通知
                    </a>
                    </li>
                    <li><a data-toggle="tab" href="#tab-3">
                        项目进度
                    </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="sidebar-title">
                            <h3> <i class="fa fa-comments-o"></i> 主题设置</h3>
                            <small><i class="fa fa-tim"></i> 你可以从这里选择和预览主题的布局和样式，这些设置会被保存在本地，下次打开的时候会直接应用这些设置。</small>
                        </div>
                        <div class="skin-setttings">
                            <div class="title">主题设置</div>

                            <div class="setings-item">
                                <span>右侧菜单</span>
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="rightmenu" class="onoffswitch-checkbox" id="rightmenu">
                                        <label class="onoffswitch-label" for="rightmenu">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="setings-item">
                                <span>固定顶部</span>
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="fixednavbar" class="onoffswitch-checkbox" id="fixednavbar">
                                        <label class="onoffswitch-label" for="fixednavbar">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="setings-item">
                                <span>固定宽度 </span>
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="boxedlayout" class="onoffswitch-checkbox" id="boxedlayout">
                                        <label class="onoffswitch-label" for="boxedlayout">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="setings-item">
                                <span>全屏按钮 </span>
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="showhideFullScreen" class="onoffswitch-checkbox" id="showhideFullScreen">
                                        <label class="onoffswitch-label" for="showhideFullScreen">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="title">皮肤选择</div>
                            <div class="setings-item default-skin nb">
                                <span class="skin-name ">
                         <a href="#" class="s-skin-0">
                             默认皮肤
                         </a>
                    </span>
                            </div>
                            <div class="setings-item blue-skin nb">
                                <span class="skin-name ">
                        <a href="#" class="s-skin-1">
                            蓝色主题
                        </a>
                    </span>
                            </div>
                            <div class="setings-item yellow-skin nb">
                                <span class="skin-name ">
                        <a href="#" class="s-skin-3">
                            黄色/紫色主题
                        </a>
                    </span>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">

                        <div class="sidebar-title">
                            <h3> <i class="fa fa-comments-o"></i> 最新通知</h3>
                            <small><i class="fa fa-tim"></i> 您当前有10条未读信息</small>
                        </div>

                        <div>

                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a1.jpg">

                                        <div class="m-t-xs">
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">

                                        据天津日报报道：瑞海公司董事长于学伟，副董事长董社轩等10人在13日上午已被控制。
                                        <br>
                                        <small class="text-muted">今天 4:21</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a2.jpg">
                                    </div>
                                    <div class="media-body">
                                        HCY48之音乐大魔王会员专属皮肤已上线，快来一键换装拥有他，宣告你对华晨宇的爱吧！
                                        <br>
                                        <small class="text-muted">昨天 2:45</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a3.jpg">

                                        <div class="m-t-xs">
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        写的好！与您分享
                                        <br>
                                        <small class="text-muted">昨天 1:10</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a4.jpg">
                                    </div>

                                    <div class="media-body">
                                        国外极限小子的炼成！这还是亲生的吗！！
                                        <br>
                                        <small class="text-muted">昨天 8:37</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a8.jpg">
                                    </div>
                                    <div class="media-body">

                                        一只流浪狗被收留后，为了减轻主人的负担，坚持自己觅食，甚至......有些东西，可能她比我们更懂。
                                        <br>
                                        <small class="text-muted">今天 4:21</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a7.jpg">
                                    </div>
                                    <div class="media-body">
                                        这哥们的新视频又来了，创意杠杠滴，帅炸了！
                                        <br>
                                        <small class="text-muted">昨天 2:45</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a3.jpg">

                                        <div class="m-t-xs">
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        最近在补追此剧，特别喜欢这段表白。
                                        <br>
                                        <small class="text-muted">昨天 1:10</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-left text-center">
                                        <img alt="image" class="img-circle message-avatar" src="/resources/images/img/a4.jpg">
                                    </div>
                                    <div class="media-body">
                                        我发起了一个投票 【你认为下午大盘会翻红吗？】
                                        <br>
                                        <small class="text-muted">星期一 8:37</small>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div id="tab-3" class="tab-pane">

                        <div class="sidebar-title">
                            <h3> <i class="fa fa-cube"></i> 最新任务</h3>
                            <small><i class="fa fa-tim"></i> 您当前有14个任务，10个已完成</small>
                        </div>

                        <ul class="sidebar-list">
                            <li>
                                <a href="#">
                                    <div class="small pull-right m-t-xs">9小时以后</div>
                                    <h4>市场调研</h4> 按要求接收教材；

                                    <div class="small">已完成： 22%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
                                    </div>
                                    <div class="small text-muted m-t-xs">项目截止： 4:00 - 2015.10.01</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-right m-t-xs">9小时以后</div>
                                    <h4>可行性报告研究报上级批准 </h4> 编写目的编写本项目进度报告的目的在于更好的控制软件开发的时间,对团队成员的 开发进度作出一个合理的比对

                                    <div class="small">已完成： 48%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 48%;" class="progress-bar"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-right m-t-xs">9小时以后</div>
                                    <h4>立项阶段</h4> 东风商用车公司 采购综合综合查询分析系统项目进度阶段性报告武汉斯迪克科技有限公司

                                    <div class="small">已完成： 14%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 14%;" class="progress-bar progress-bar-info"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-primary pull-right">NEW</span>
                                    <h4>设计阶段</h4>
                                    <!--<div class="small pull-right m-t-xs">9小时以后</div>-->
                                    项目进度报告(Project Progress Report)
                                    <div class="small">已完成： 22%</div>
                                    <div class="small text-muted m-t-xs">项目截止： 4:00 - 2015.10.01</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-right m-t-xs">9小时以后</div>
                                    <h4>拆迁阶段</h4> 科研项目研究进展报告 项目编号: 项目名称: 项目负责人:

                                    <div class="small">已完成： 22%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
                                    </div>
                                    <div class="small text-muted m-t-xs">项目截止： 4:00 - 2015.10.01</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-right m-t-xs">9小时以后</div>
                                    <h4>建设阶段 </h4> 编写目的编写本项目进度报告的目的在于更好的控制软件开发的时间,对团队成员的 开发进度作出一个合理的比对

                                    <div class="small">已完成： 48%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 48%;" class="progress-bar"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-right m-t-xs">9小时以后</div>
                                    <h4>获证开盘</h4> 编写目的编写本项目进度报告的目的在于更好的控制软件开发的时间,对团队成员的 开发进度作出一个合理的比对

                                    <div class="small">已完成： 14%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 14%;" class="progress-bar progress-bar-info"></div>
                                    </div>
                                </a>
                            </li>

                        </ul>

                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>