<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>最新动态管理</title>
    <link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>

    <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
    <script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>


</head>

<body  class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> cms <span class="c-gray en">&gt;</span>最新动态管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl toolbar">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline  btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline  btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
            <a  onclick="query('form_query');" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'module', align:"center", sortable:true, order:"asc", visible:false, title: '所属模块'},
            {field: 'newstype', align:"center", sortable:true, width:"100px", order:"asc", visible:true,
            title: '消息类型',
                formatter:function(value, row, index){
                    if(value=="1"){
                        value="文本消息";
                    }else if(value=="2"){
                        value="图文消息";
                    }else if(value=="3"){
                        value="附件消息";
                    }else if(value=="4"){
                        value="申请消息";
                    }else if(value=="5"){
                        value="工作动态";
                    }else if(value=="6"){
                        value="通知";
                    }else if(value=="7"){
                        value="公告";
                    }else{
                    }
                    return value;
                }
            },
            {field: 'content', align:"left", sortable:true, order:"asc", visible:true, title: '消息内容'},
            {field: 'refid', align:"center", sortable:true, order:"asc", visible:false, title: '引用id'},
            {field: 'attachefiles', align:"center", sortable:true, order:"asc", visible:false, title: '存储附件'},
            {field: 'pushscope', align:"center", sortable:true, order:"asc", width:"110px", visible:true, title: '推送范围',
                formatter:function(value, row, index){
                    if(value=="1"){
                        value="<span class=\"label label-primary\">全部推送</span>";
                    }else if(value=="2"){
                        value="<span class=\"label label-info\">仅推送本部门</span>";
                    }else if(value=="3"){
                        value="<span class=\"label label-info\">仅本人</span>";
                    }else{
                        value="<span class=\"label label-info\">未知</span>";
                    }
                    return value;
                }
            },
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.url='/base/recentNews/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
        $.layer.open_page("编辑最新动态",sys_ctx+"/base/recentNews/edit?id="+row.id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        $.layer.open_page("新增最新动态",sys_ctx+"/base/recentNews/add",{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            $.layer.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/base/recentNews/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        query('form_query');
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
