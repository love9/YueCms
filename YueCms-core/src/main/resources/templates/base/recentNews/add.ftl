<!DOCTYPE html>
<html>
<head>
  <title>新增最新动态</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
	<meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

	<link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet">

    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <#import "/base/util/macro.ftl" as macro>
</head>
<body  class="body-bg-add">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center"> </h2>

        <#--<div class="form-group">
              <label class="col-sm-2 control-label">所属模块：</label>
               <div class="col-sm-6 formControls">
  <input type="text" id="module" name="module" placeholder="所属模块" value="" class="form-control" datatype="*" nullmsg="请输入消息所属模块" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>-->
        <div class="form-group">
              <label class="col-sm-2 control-label">消息类型：</label>
               <div class="col-sm-6 formControls">
                   <input type="hidden" id="newstype" name="newstype" placeholder="消息类型" value="" class="form-control" datatype="*" nullmsg="请输入消息类型 " />

                    <@macro.dicSelect id="newstype"  value="" idAttr="newstype" changeCallBack=""></@macro.dicSelect>
               </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>


        <div class="form-group">
            <label class="col-sm-2 control-label">标题：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="title" name="title" placeholder="消息标题" value="" class="form-control" datatype="*" nullmsg="请输入记录类型" />
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">通知范围：</label>
            <div class="col-sm-6 formControls">
                <input type="hidden" id="pushscope" name="pushscope" placeholder="该消息推送范围" value="" class="form-control" datatype="*" nullmsg="请输入该消息推送范围" />

                <@macro.dicSelect id="pushscope"  value="" idAttr="pushscope" changeCallBack=""></@macro.dicSelect>
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">内容：</label>
            <div class="col-sm-6 formControls">
                <textarea rows="6" style="width:100%;"  id="content" name="content"   placeholder="记录内容" class="form-control" datatype="*" nullmsg="请输入内容"> </textarea>
            </div>
            <div class="col-sm-4">
                <div class="Validform_checktip"></div>
            </div>
        </div>





    </form></div>
</div>


<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/base/recentNews/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
