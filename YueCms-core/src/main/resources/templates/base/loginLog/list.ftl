<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录日志管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">

</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span>登录日志管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                <!--<a onclick="addNew()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>-->
                <a onclick="list_del('tableList')" class="btn btn-outline  btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a onclick="query('form_query');" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:true,title: '登录用户ID'},
            {field: 'account',align:"center",sortable:true,order:"asc",visible:true,title: '登录账户'},
            {field: 'ip',align:"center",sortable:true,order:"asc",visible:true,title: 'IP'},
            {field: 'location',align:"center",sortable:true,order:"asc",visible:true,title: '登录地点'},
            {field: 'deviceType',align:"center",sortable:true,order:"asc",visible:true,title: '设备类型',formatter:function(v,row){
                if(v=="Computer"){
                    return "<span class=\"label label-primary radius\">电脑端</span>";
                }else if(v=="Tablet"){
                    return "<span class=\"label label-info radius\">平板</span>";
                }else if(v=="Mobile"){
                    return "<span class=\"label label-success radius\">手机</span>";
                }else{
                    return "<span class=\"label label-default radius\">未知</span>";
                }
            }},
            {field: 'explore',align:"center",sortable:true,order:"asc",visible:true,title: '浏览器'},
            {field: 'os',align:"center",sortable:true,order:"asc",visible:true,title: '操作系统'},
            {field: 'result',align:"center",sortable:true,order:"asc",visible:false,title: '登录结果'},
            {field: 'msg',align:"center",sortable:true,order:"asc",visible:true,title: '登录结果'},
            {field: 'params',align:"center",sortable:true,order:"asc",visible:false,title: '参数'},
            {field: 'createTime',align:"center",sortable:true,order:"asc",visible:true,title: '登录时间'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/base/loginLog/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
       $.layer.open_page("编辑登录日志",sys_ctx+"/base/loginLog/edit?id="+row.id,{
           end:function(){
             $('#tableList').bootstrapTable('refresh');
           }
        });
    }
    function addNew(){
        $.layer.open_page("新增登录日志",sys_ctx+"/base/loginLog/add",{
            end:function(){
             $('#tableList').bootstrapTable('refresh');
            }
        });
   }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            $.layer.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/base/loginLog/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data,function(){
                            $('#tableList').bootstrapTable('refresh');
                        });
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
