

<!DOCTYPE html>
<html>
<head>
  <title>新增登录日志</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body  class="body-bg-add">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center"></h2>

         <div class="form-group">
            <label class="col-sm-3 control-label">登录用户ID：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="yhid" name="yhid" placeholder="登录用户ID" value="${loginLog.yhid!}" class="form-control" datatype="*" nullmsg="请输入登录用户ID" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">登录账户：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="account" name="account" placeholder="登录账户" value="${loginLog.account!}" class="form-control" datatype="*" nullmsg="请输入登录账户" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">IP：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="ip" name="ip" placeholder="" value="${loginLog.ip!}" class="form-control" datatype="*" nullmsg="请输入IP" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">登录地点：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="location" name="location" placeholder="登录地点" value="${loginLog.location!}" class="form-control" datatype="*" nullmsg="请输入登录地点" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">浏览器：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="explore" name="explore" placeholder="浏览器" value="${loginLog.explore!}" class="form-control" datatype="*" nullmsg="请输入浏览器" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">操作系统：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="os" name="os" placeholder="操作系统" value="${loginLog.os!}" class="form-control" datatype="*" nullmsg="请输入操作系统" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">登录结果：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="result" name="result" placeholder="登录结果" value="${loginLog.result!}" class="form-control" datatype="*" nullmsg="请输入登录结果" />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">信息：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="msg" name="msg" placeholder="" value="${loginLog.msg!}" class="form-control"  />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">参数：</label>
            <div class="col-sm-6 formControls">
                <input type="text" id="params" name="params" placeholder="参数" value="${loginLog.params!}" class="form-control"  />
            </div>
            <div class="col-sm-3">
              	<div class="Validform_checktip"></div>
            </div>
         </div>


    </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/base/loginLog/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
