<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>角色管理</title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <link rel="shortcut icon" href="favicon.ico">
  <link href="/resources/css/admui.css" rel="stylesheet">
  <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
</head>
<body class="body-bg main-bg">
<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span> 基本设置</nav>
<div class="wrapper wrapper-content animated fadeInRight">
      <!--工具条-->
      <div class="cl toolbar">
          <form id="form_query" style="margin-bottom: 15px">
              <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline  btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline  btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                  <a id="open_search"  onclick="javascript:$('#search_hidden').show();$(this).hide();" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                 </div>
              <div id="search_hidden"  style="display: none;margin-top: 8px" >
                <input type="text" class="form-control" style="display:inline-block;width:175px;" id="cx_name" name="cx_name"    placeholder="角色名称"/>
                <div class="btn-group">
                  <a  onclick="query('form_query');" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                  <a href="javascript:void(0)"  id="btn_reset" onClick="search_form_reset('form_query')" class="btn btn-outline  btn-info"><i class="fa fa-reply"></i>&nbsp;重置</a>
                  <a  onclick="javascript:$('#search_hidden').hide();$('#open_search').show();" class="btn btn-default btn-outline "><i class="fa fa-close"></i>&nbsp;关闭</a>
                </div>
              </div>
          </form>
      </div>
      <!--数据列表-->
  <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" >
    var sys_ctx="";
    var validform;
    var queryStr="?";
  //查询功能的标签说明
  var table_list_query_form = {
    cx_name:''
  };

  function refreshData() {
    $('#tableList').bootstrapTable('refresh');
  }
  $(function(){
    sys_table_list();
  });
function sys_table_list(){
  $('#tableList').bootstrapTable('destroy');
    var columns=[{checkbox:true},
      {field: 'id', align:"center", sortable:true, order:"asc", visible:false,title: '序号'},
      {field: 'orgid', align:"center", visible:false, title: '机构Id'},
      {field: 'name', align:"center", sortable:true, width:'25%', visible:true, title: '角色名称'},
      {field: 'loginpage', align:"center", sortable:true, width:'25%', visible:true, title: '登录后跳转的页面'},
      {field: 'mainpage', align:"center", sortable:true, width:'25%', visible:true, title: '首页'},
      {field: 'description', title: '描述',
      formatter:function(value, row, index){
        value = '<font color="gray">'+value+'</font>';
        return value;
      }}];
  table_list_Params.columns=columns;
  table_list_Params.onClickRow=onClickRow;
  table_list_Params.url='/org/role/json/find'+queryStr;
  $('#tableList').bootstrapTable(table_list_Params);
}
  var onClickRow=function(row,tr){
    $.layer.open_page("编辑角色",sys_ctx+"/org/role/edit?id="+row.id,{end:function(){
      sys_table_list();
    }});
  }
  function addNew(){
    $.layer.open_page("新增角色",sys_ctx+"/org/role/add",{end:function(){
      refreshData();
    }});
  }

  function list_del(tableid){
    var selecRow = $("#"+tableid).bootstrapTable('getSelections');
    if(selecRow.length > 0){
      $.layer.confirm("确定这样做吗？", function(){
        var ids = new Array();
        for(var i=0;i<selecRow.length;i++){
          ids[ids.length] = selecRow[i]["id"]
        }
        $.ajax({
          type:"post",
          url:'/org/role/json/removes/'+ids,
          data:null,
          success:function(data,textStatus){
            ajaxReturnMsg(data);
            refreshData();
          }
          // ,error:ajaxError()
        });
      });
    }else{
      Fast.msg_warning("请选择要删除的数据")
    }
  }

  //根据表单查询
  var query = function(formid){
    queryStr="?";
    var qArr = $("#"+formid)[0];//查询表单区域序列化重写
    var queryStrTem="";
    for(var i=0;i<qArr.length;i++){
      var id = qArr[i].id;
      if(typeof table_list_query_form[id] != 'undefined')
      {
        table_list_query_form[id] = $("#"+id).val();
        queryStrTem+="&"+id+"="+$("#"+id).val();
      }

    }
    queryStrTem=queryStrTem.substring(1);
    queryStr+=queryStrTem;
   // alert(queryStr);
   sys_table_list();
    //refreshData();
  }
  function search_form_reset(tableid){
    $('#'+tableid)[0].reset()
  }

</script>
</body>
</html>
