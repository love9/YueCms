<!DOCTYPE html>
<html>
<head>
  <title>编辑角色</title>
  <link href="/resources/css/admui.css" rel="stylesheet">
  <link href="/resources/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css"/>

</head>
<body class="body-bg-edit">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-5">
          <h2 class="text-center"></h2>
          <input type="hidden" value="${role.id!}" id="id" name="id"/>
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-sm-2"><span class="text-danger">*</span>角色名称：</label>
        <div class="col-sm-6 formControls">
          <input type="text" class="form-control"  id="name"  datatype="*" nullmsg="请输入角色名称"  name="name" value="${role.name!}"      />
        </div>
        <div class="col-sm-4">
            <div class="Validform_checktip"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2"><span class="text-danger">*</span>描述：</label>
        <div class="col-sm-6 formControls">
          <textarea rows="3" style="height: 70px;" class="form-control"  placeholder=""  datatype="*" nullmsg="请输入角色描述" id="description" name="description"> ${role.description!} </textarea>
        </div>
        <div class="col-sm-4">
            <div class="Validform_checktip"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2">角色授权：</label>
        <div class="col-sm-6  ">
          <div id="menuTrees" class="ztree"></div>
        </div>
      </div>


      <#--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-2">
          <button type="submit" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->
    </form>
  </div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/lib/bootstrap-validator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script src="/resources/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>
<script type="text/javascript">
  var validform;
  function add(){
    $('#form_show')[0].reset();
  }


  function save(){
    var ids="";
      var b=validform.check(false);
      if(!b)
      {
          return;
      }
    ids = $.tree.getCheckedNodes();
    var params=$("#form_show").serialize();
    $.ajax({
      type:"post",
      url:'/org/role/json/saveRoleAndRolePermissions?'+params,
      data:{ids:ids},
      success:function(json,textStatus){
        ajaxReturnMsg(json);
        setTimeout(function(){
          var index = parent.layer.getFrameIndex(window.name);
          parent.layer.close(index);
        },1200);
      }
    });
  }

  $(function(){
    var jsid='${role.id}';
    var url = "/org/role/json/zTreeStatic?jsid="+jsid;
    var options = {
      id: "menuTrees",
      url: url,
      check: { enable: true, nocheckInherit: true, chkboxType: { "Y": "ps", "N": "ps" } },
      expandLevel: 1
    };
    $.tree.init(options);

    validform=$("#form_show").Validform({
          btnReset:"#reset",
          tiptype:2,
          postonce:true,//至提交一次
          ajaxPost:false,//ajax方式提交
          showAllError:true //默认 即逐条验证,true验证全部
    });


  })
</script>


</body>
</html>
