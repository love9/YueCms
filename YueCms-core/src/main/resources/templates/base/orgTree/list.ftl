<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>组织目录</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/css/web-icons/web-icons.css" rel="stylesheet">
    <link href="/resources/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/resources/lib/jquery-layout/jquery.layout-latest.css">

<style>
    table thead {font-weight: 600;}
</style>
</head>
<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="top breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span>
    组织目录管理
</nav>

<div  class="left"  style=" padding:0 8px;" id="west">
    <div style="height:5px;margin:5px 0 0 0px; height:30px;">
        <a href="#" class="easyui-linkbutton" onClick="initZtree()" plain="true">刷新</a>
        <a href="#" class="easyui-linkbutton" onClick="expandAll();" plain="true">展开</a>
        <a href="#" class="easyui-linkbutton" onClick="collapseAll();" plain="true">折叠</a>
    </div>
    <div style="vertical-align: top;margin:0 0 0 0px; ">
        <div>
            <ul class="ztree" id="trees">

            </ul>
        </div>
    </div>
</div>

<div id="mainPanle"  class="table-responsive" style="padding:0 18px;">

    <div id="div_bm">
        <!--工具条-->
        <div class="cl toolbar">
            <form id="form_query_bm" style="margin-bottom: 15px">
                <div class="btn-group">
                    <a onclick="add('bm')" class="btn btn-sm btn-primary"><i
                            class="fa fa-plus"></i>&nbsp;新增部门</a>
                    <a onclick="list_del('bm')" class="btn btn-sm btn-danger"><i class="fa fa-minus"></i>&nbsp;删除部门</a>
                    <a href="javascript:void(0)" onClick="sortMenu('bm');" class="btn btn-sm btn-info"><i
                            class="fa fa-sort"></i>&nbsp;排序</a>
                    <a onclick="importOrg()" class="btn btn-sm btn-warning"><i class="fa fa-file-excel-o"></i>&nbsp;导入组织目录</a>
                </div>
            </form>
        </div>
        <!--数据列表-->

        <table id="table_bm" class="table table-border table-bordered table-hover"
               style="border-collapse:collapse;border:1px solid #D4EFF7">
            <thead class="text-c">
            <tr>
                <td width="30px"><input type='checkbox' id="ck_all_bm" onclick="checkall(this)"></td>
                <td width="50px">序号</td>
                <td>ID</td>
                <td>部门名称</td>
                <td>排序</td>
                <td>电话</td>
                <td>传真</td>
                <td>邮箱</td>
                <td>备注</td>
                <td width="160px">操作</td>
            </tr>

            </thead>
            <tbody class="text-c">

            </tbody>

        </table>
        <!--分页条-->
        <div style="float: right" id="pagebar_bm" class="pagebar"></div>
    </div>
    <div id="div_gw">
        <div class="cl toolbar">
            <form id="form_query_gw" style="margin-bottom: 15px">
                <div class="btn-group">
                    <a onclick="add('gw')" class="btn btn-sm btn-primary"><i
                            class="fa fa-plus"></i>&nbsp;新增岗位</a>
                    <a onclick="list_del('gw')" class="btn btn-sm btn-danger"><i class="fa fa-minus"></i>&nbsp;删除岗位</a>
                    <a href="javascript:void(0)" onClick="sortMenu('gw');" class="btn btn-sm btn-info"><i
                            class="fa fa-sort"></i>&nbsp;排序</a>
                </div>
            </form>
        </div>
        <table id="table_gw" class="table table-border table-bordered table-hover"
               style="border-collapse:collapse;border:1px solid #D4EFF7">
            <thead class="text-c">
            <tr>
                <td width="30px"><input type='checkbox' id="ck_all_gw" onclick="checkall(this)"></td>
                <td width="50px">序号</td>
                <td>ID</td>
                <td>岗位名称</td>
                <td>排序</td>
                <td>描述</td>
                <td width="160px">操作</td>
            </tr>

            </thead>
            <tbody class="text-c">

            </tbody>
        </table>
        <!--分页条-->
        <div style="float: right" id="pagebar_gw" class="pagebar"></div>
    </div>
    <div id="div_ry" style="display: none;">
        <div class="cl toolbar">
            <form id="form_query_ry" style="margin-bottom: 15px">
                <div class="btn-group">
                    <a onclick="add('ry')" class="btn btn-sm btn-primary"><i
                            class="fa fa-plus"></i>&nbsp;新增用户</a>
                    <a onclick="list_del('ry')" class="btn btn-sm btn-danger"><i class="fa fa-minus"></i>&nbsp;删除用户</a>
                    <a href="javascript:void(0)" onClick="sortMenu('ry');" class="btn btn-sm btn-info"><i
                            class="fa fa-sort"></i>&nbsp;排序</a>
                    <a onclick="importOrg()" class="btn btn-sm btn-warning"><i class="fa fa-file-excel-o"></i>&nbsp;导入组织目录</a>
                </div>
            </form>
        </div>
        <table id="table_ry" class="table table-border table-bordered table-hover"
               style="border-collapse:collapse;border:1px solid #D4EFF7">
            <thead class="text-c">
            <tr>
                <td width="30px"><input type='checkbox' id="ck_all_ry" onclick="checkall(this)"></td>
                <td width="50px">序号</td>
                <td>登录账户</td>
                <td>真实姓名</td>
                <td>性别</td>
                <td>邮箱</td>
                <td>电话</td>
                <td>状态</td>
                <td width="160px">操作</td>
            </tr>

            </thead>
            <tbody class="text-c">

            </tbody>
        </table>
        <!--分页条-->
        <div style="float: right" id="pagebar_ry" class="pagebar"></div>
    </div>
</div>

<div id="div_sort" title="菜单排序" style="width:350px;height:400px;display: none;">
    <div class="c_div_tool_bar">
        <a href="javascript:void(0)" class="btn btn-primary size-S" style="margin:4px;" onClick="saveSort()">保存</a>
    </div>
    <div class="c_div_show_content">
        <form id="form_sort">
            <div id="dd" style="width:100%;">
                <ul id="sortable">

                </ul>
            </div>
        </form>
    </div>
</div>
<div id="div_import" title="导入组织目录" style="width:350px;height:280px;padding:8px;display: none;">

    <div class="c_div_show_content">

        <form action="" id="form_import" method="post" class="form-horizontal" role="form">
            <div class="form-group1">
                <div class="input-group input-group-file">
                    <input type="text" id="file_show" class="form-control" readonly="" placeholder="请选择excel文件">
                    <span class="input-group-btn">
                        <span class="btn btn-success btn-file">
                            <i class="icon wb-upload" aria-hidden="true"></i>
                            <input type="file" name="file" id="file" accept=".xls" onchange="show()">
                        </span>
                    </span>

                </div>
            </div>
        </form>
        <div class="alert alert-danger alert-dismissible" style="margin-top: 8px;">

            请根据模板要求认真填写导入组织目录表格!
        </div>
        <div class="c_div_tool_bar text-center margin-bottom-10">
            <a href="javascript:void(0)" class="btn btn-primary size-S" style="margin:4px;"
               onClick="doImportOrg()">导入</a>
            <a href="javascript:void(0)" class="btn btn-success size-S" style="margin:4px;" onClick="downloadExcel()">下载模板</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<!--<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">-->
<script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/resources/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script src="/resources/lib/jquery-layout/jquery.layout-latest.js"></script>
<script type="text/javascript">

    function show() {
        var v = document.getElementById("file").value;
        if (!v.endsWith(".xls")) {
            $.layer.alert_wrong("请选择xls文件");
            return;
        }
        document.getElementById("file_show").value = v;
    }
</script>

<script type="text/javascript">
    var sys_ctx="";
    var tmp_parentid = 0;
    var tmp_parentname;
    var zTreeObj;
    var tree_id;
    var rows = 5;//每页的记录数
    var page_bm = 1;
    var totalPages_bm = 0;
    var page_gw = 1;
    var totalPages_gw = 0;
    var page_ry = 1;
    var totalPages_ry = 0;
    function pager_bm() {

        var $pagebar = $("#pagebar_bm");
        laypage({
            cont: $pagebar, //容器。值支持id名、原生dom对象，jquery对象,
            pages: totalPages_bm, //总页数
            curr: page_bm,
            jump: function (e, first) {
                if (!first) { //一定要加此判断，否则初始时会无限刷新
                    page_bm = e.curr;
                    table_list("bm");
                }
            },
            skin: 'molv', //皮肤
            first: '首页', //若不显示，设置false即可
            last: '尾页', //若不显示，设置false即可
            prev: '上一页', //若不显示，设置false即可
            next: '下一页' //若不显示，设置false即可
        });
    }
    function pager_gw() {

        var $pagebar = $("#pagebar_gw");
        laypage({
            cont: $pagebar, //容器。值支持id名、原生dom对象，jquery对象,
            pages: totalPages_gw, //总页数
            curr: page_gw,
            jump: function (e, first) {
                if (!first) { //一定要加此判断，否则初始时会无限刷新
                    page_gw = e.curr;
                    table_list("gw");
                }
            },
            skin: 'molv', //皮肤
            first: '首页', //若不显示，设置false即可
            last: '尾页', //若不显示，设置false即可
            prev: '上一页', //若不显示，设置false即可
            next: '下一页' //若不显示，设置false即可
        });
    }
    function pager_ry() {

        var $pagebar = $("#pagebar_ry");
        laypage({
            cont: $pagebar, //容器。值支持id名、原生dom对象，jquery对象,
            pages: totalPages_ry, //总页数
            curr: page_ry,
            jump: function (e, first) {
                if (!first) { //一定要加此判断，否则初始时会无限刷新
                    page_ry = e.curr;
                    table_list("ry");
                }
            },
            skin: 'molv', //皮肤
            first: '首页', //若不显示，设置false即可
            last: '尾页', //若不显示，设置false即可
            prev: '上一页', //若不显示，设置false即可
            next: '下一页' //若不显示，设置false即可
        });
    }


    function checkall(obj) {
        $this = $(obj);
        var b = $this.is(":checked");
        $this.parents("table").find("tbody tr td").find("input[type='checkbox']").prop("checked", b);
    }
    function table_list(type) {
        var url = "";
        var totalPages;
        if (type == "bm") {

            url = '/org/tree/json/findBmListByBmid?t=' + new Date().getTime() + '&sjbmid=' + tmp_parentid + '&page=' + page_bm + "&rows=" + rows;
        } else if (type == "gw") {

            url = '/org/tree/json/findGwListByBmid?t=' + new Date().getTime() + '&sjbmid=' + tmp_parentid + '&page=' + page_gw + "&rows=" + rows;
        } else if (type == "ry") {

            url = '/org/tree/json/findRyListByGwid?t=' + new Date().getTime() + '&sjgwid=' + tmp_parentid + '&page=' + page_ry + "&rows=" + rows;
        } else {
        }


        $.getJSON(url, function (json) {
            //alert(JSON.stringify(json));
            //alert((Number(json.total)));

            if (type == "bm") {
                totalPages_bm = Number((Number(json.total) + Number(Number(rows) - 1))) / rows;
            } else if (type == "gw") {
                totalPages_gw = Number((Number(json.total) + Number(Number(rows) - 1))) / rows;
            } else if (type == "ry") {
                totalPages_ry = Number((Number(json.total) + Number(Number(rows) - 1))) / rows;
            } else {
            }
            var html = "";
            $.each(json.rows, function (index, item) {
                if (type == "bm") {
                    html += "<tr  data-id='" + item.id + "' >";
                    html += "<td><input type='checkbox' > </td>";
                    html += "<td>" + Number(Number(Number(index) + 1) + (page_bm - 1) * rows) + "</td>";
                    html += "<td>" + item.id + "</td>";
                    html += "<td>" + item.name + "</a></td>";

                    html += "<td>" + item.sort + "</td>";
                    html += "<td>" + item.phone + "</td>";
                    html += "<td>" + item.fax + "</td>";
                    html += "<td>" + item.email + "</td>";
                    html += "<td>" + item.bz + "</td>";
                    html+="<td><div class='btn btn-group'><button class=\"btn btn-xs  btn-primary \" onclick=\"editRow('"+item.id+"','bm')\" type=\"button\"><i class=\"fa fa-edit\"></i><span class=\"bold\">&nbsp;&nbsp;编辑</span></button>";
                    html+="<button class=\"btn btn-xs  btn-danger \" onclick=\"deleteRow('"+item.id+"','bm',this)\" type=\"button\"><i class=\"fa fa-minus\"></i><span class=\"bold\">&nbsp;&nbsp;删除</span></button>";
                    html+="</div></td>";
                    html += "</tr>";
                } else if (type == "gw") {
                    html += "<tr  data-id='" + item.id + "'  >";
                    html += "<td><input type='checkbox' > </td>";
                    html += "<td>" + Number(Number(Number(index) + 1) + (page_gw - 1) * rows) + "</td>";
                    html += "<td>" + item.id + "</td>";
                    html += "<td>" + item.name + "</a></td>";
                    html += "<td>" + item.sort + "</td>";
                    html += "<td>" + item.description + "</td>";
                    html+="<td><div class='btn btn-group'><button class=\"btn btn-xs  btn-primary \" onclick=\"editRow('"+item.id+"','gw')\" type=\"button\"><i class=\"fa fa-edit\"></i><span class=\"bold\">&nbsp;&nbsp;编辑</span></button>";
                    html+="<button class=\"btn btn-xs  btn-danger \" onclick=\"deleteRow('"+item.id+"','gw',this)\" type=\"button\"><i class=\"fa fa-minus\"></i><span class=\"bold\">&nbsp;&nbsp;删除</span></button>";
                    html+="</div></td>";
                    html += "</tr>";
                } else if (type == "ry") {
                    var zt = "";
                    if (item.deleted == "1") {
                        zt = "<span class=\"label label-danger radius\">已注销</span>";
                    } else {
                        if (item.available == "1") {
                            zt = "<span class=\"label label-success radius\">正常</span>";
                        } else {
                            zt = "<span class=\"label label-default radius\">未激活</span>";
                        }
                    }
                    var sex = "";
                    if (item.gender == "1") {
                        sex = "男";
                    } else if (item.gender == "0") {
                        sex = "女";
                    } else {
                        sex = "未知";
                    }
                    html += "<tr   data-id='" + item.id + "'  >";
                    html += "<td><input type='checkbox' > </td>";
                    html += "<td>" + Number(Number(Number(index) + 1) + (page_ry - 1) * rows) + "</td>";
                    html += "<td>" + item.account + "</td>";
                    html += "<td>" + item.realname + "</a></td>";
                    html += "<td>" + sex + "</td>";
                    html += "<td>" + item.email + "</td>";
                    html += "<td>" + item.phone + "</td>";
                    html += "<td>" + zt + "</td>";
                    html+="<td><div class='btn btn-group'><button class=\"btn btn-xs  btn-primary \" onclick=\"editRow('"+item.id+"','ry')\" type=\"button\"><i class=\"fa fa-edit\"></i><span class=\"bold\">&nbsp;&nbsp;编辑</span></button>";
                    html+="<button class=\"btn btn-xs  btn-danger \" onclick=\"deleteRow('"+item.id+"','ry',this)\" type=\"button\"><i class=\"fa fa-minus\"></i><span class=\"bold\">&nbsp;&nbsp;删除</span></button>";
                    html+="</div></td>";
                    html += "</tr>";
                } else {
                }
            });
            $("#table_" + type).find("tbody").html(html);
            if (type == "bm") {
                pager_bm();
                $("#ck_all_bm").prop("checked", false);
            } else if (type == "gw") {
                pager_gw();
                $("#ck_all_gw").prop("checked", false);
            } else if (type == "ry") {
                pager_ry();
                $("#ck_all_ry").prop("checked", false);
            } else {
            }
        });
    }
    function deleteRow(id,type,obj){
        $this=$(obj);
        var url="";
        if (type == 'bm') {
            url = '/org/department/json/removes/' + id;
        } else if (type == 'gw') {
            url = '/org/position/json/removes/' + id;
        } else if (type == 'ry') {
            url = '/org/user/json/removes/' + id;
        } else {
        }

        $.layer.confirm("确定这样做吗？", function () {
                $.ajax({
                    type: "post",
                    url: url,
                    data: null,
                    success: function (data, textStatus) {
                        ajaxReturnMsg(data);
                        $this.parents("tr").remove();
                        removeTreeNodeByLxId(id);
                    }
                });
        });

    }
    //根据ID删除树节点
    function removeTreeNodeByLxId(id){
        console.log("removeTreeNodeByLxId:"+id);
       // $.tree.removeNodeById(id);//这个方法要求的id为树记录的id
        $.tree.removeNodeByParam("lxid",id);
    }
    function getCheckedNodes(){
        var a= $.tree.getCheckedNodes();
        console.log(a);
        return a;
    }
    function expandAll(){


        $.tree.expand();
    }
    function collapseAll(){
        $.tree.collapse();
    }
    function initZtree() {
        var url = "/org/tree/json/zTreeStatic";
        var options = {
            check:{enable:true},
            id:"trees",
            url: url,
            expandLevel: 2,
            onClick : zTreeOnClick
        };
        $.tree.init(options);

        //模拟点击根节点加载列表
        var url = "/org/tree/json/getZtreeRoot";
        $.getJSON(url, function (json) {
            //alert(JSON.stringify(json));
            tmp_parentid = json.lxid;
            tmp_parentname = json.name;
            page_bm = 1;
            totalPages_bm = 0;
            page_gw = 1;
            totalPages_gw = 0;
            $("#div_bm").show();
            $("#div_gw").show();
            $("#div_ry").hide();
            table_list("bm");
            table_list("gw");
        });
    }
    //加载完成的回调
    function zTreeAjaxSuccess(event, treeId, treeNode, msg) {

    }
    function zTreeBeforeAsync(treeId, treeNode) {
        if (treeNode) {
            tmp_parentid = treeNode.id;
            zTreeObj.setting.async.url = "/org/tree/json/ztree?parentid=" + tmp_parentid;
        }
    }

    /**
     * 刷新当前节点
     */
    function refreshNode() {
        /*根据 treeId 获取 zTree 对象*/
        var zTree = $.fn.zTree.getZTreeObj("trees"), type = "refresh", silent = false, nodes = zTree.getSelectedNodes();
        //alert(JSON.stringify(nodes));
        /*强行异步加载父节点的子节点。[setting.async.enable = true 时有效]*/
        zTree.reAsyncChildNodes(nodes[0], type, silent);
        zTreeOnClick(null, zTree.setting.treeId, nodes[0]);
    }
    function refreshParentNode() {
        var zTree = $.fn.zTree.getZTreeObj("trees"), type = "refresh", silent = false, nodes = zTree.getSelectedNodes();
        /*根据 zTree 的唯一标识 tId 快速获取节点 JSON 数据对象*/
        var parentNode = zTree.getNodeByTId(nodes[0].parentTId);
        /*选中指定节点*/
        zTree.selectNode(parentNode);
        zTree.reAsyncChildNodes(parentNode, type, silent);
    }
    function zTreeOnClick(event, treeId, treeNode, clickFlag) {
        //tree_id=treeNode.id;
        console.log(treeNode.id);
        tmp_parentid = treeNode.lxid;
        tmp_parentname = treeNode.name;

        switch (treeNode.type) {
            case "zz":
                page_bm = 1;
                totalPages_bm = 0;
                page_gw = 1;
                totalPages_gw = 0;
                $("#div_bm").show();
                $("#div_gw").show();
                $("#div_ry").hide();
                table_list("bm");
                table_list("gw");
                break;
            case "bm":
                page_bm = 1;
                totalPages_bm = 0;
                page_gw = 1;
                totalPages_gw = 0;
                $("#div_bm").show();
                $("#div_gw").show();
                $("#div_ry").hide();
                table_list("bm");
                table_list("gw");
                break;
            case "gw":
                // alert(JSON.stringify(treeNode));
                //重置页码
                page_ry = 1;//当前页
                totalPages_ry = 0;
                $("#div_bm").hide();
                $("#div_gw").hide();
                $("#div_ry").show();
                table_list("ry");
                break;
            case "ry":
                //alert(JSON.stringify(treeNode));
                var id = treeNode.id;
                var name = treeNode.name;
                break;
        }

    }


    var queryStr = "?";
    //查询功能的标签说明
    var table_list_query_form = {
        parentid: tmp_parentid
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function () {
        $('body').layout({
            //applyDemoStyles:true,//是否采用默认样式
            west__paneSelector: ".left",
            north__paneSelector: ".top",
            center__paneSelector: "#mainPanle"
        });

        initZtree();
        // sys_table_list_bm();
        table_list("bm");
    });

    var onClickRow = function (row, tr) {
        layer_open("编辑部门", sys_ctx + "/org/department/edit?id=" + row.id, null, function () {
            sys_table_list_bm();
        });
    }
    function add(type) {
        if (type == 'bm') {
            var params = "?parentid=" + tmp_parentid + "&parentname=" + tmp_parentname;
            layer_open("新增部门", sys_ctx + "/org/department/add" + params,null, function () {
                initZtree();
            });

        } else if (type == 'gw') {
            var params = "?parentid=" + tmp_parentid + "&parentname=" + tmp_parentname;
            layer_open("新增岗位", sys_ctx + "/org/position/add" + params,null, function () {
                initZtree();
            });

        } else if (type == 'ry') {
            var params = "?sjgwid=" + tmp_parentid + "&sjgwname=" + tmp_parentname;
            layer_open("新增用户", sys_ctx + "/org/user/add" + params,null, function () {
                initZtree();
            });

        } else {
        }
    }

    function getSelectedRows(table_id) {
        var ids = [];
        $("#" + table_id).find(" tbody").find("input[type='checkbox']:checked").each(function () {
            var id = $(this).parent().parent().attr("data-id");
            /*获得选中行的数据id在该节点上缓存的值*/
            ids.push(id);
        });
        return ids;
    }
    function list_del(type) {
        var idArr = getSelectedRows('table_' + type);

        if (isEmpty(idArr)) {
            layer.msg("请您选择至少1条数据");
            return;
        }
        var ids = idArr.join(",");
        var url;
        if (type == 'bm') {
            url = '/org/department/json/removes/' + ids;
        } else if (type == 'gw') {
            url = '/org/position/json/removes/' + ids;
        } else if (type == 'ry') {
            url = '/org/user/json/removes/' + ids;
        } else {
        }
        if (idArr.length > 0) {
            $.layer.confirm("确定这样做吗？", function () {
                var ids = new Array();
                for (var i = 0; i < idArr.length; i++) {
                    ids[ids.length] = idArr[i]["id"]
                }
                $.ajax({
                    type: "post",
                    url: url,
                    data: null,
                    success: function (data, textStatus) {
                        //layer.closeAll();
                        ajaxReturnMsg(data);
                        // refreshTree();
                        refreshNode()
                        // refreshParentNode();
                    }
                    // ,error:ajaxError()
                });
            });
        } else {
            Fast.msg_warning("请选择要删除的数据")
        }
    }

    var sort_type;
    var sortMenu = function (type) {
        sort_type = type;
        var url;

        if (type == "bm") {
            url = '/org/tree/json/findBmListByBmid?sjbmid=' + tmp_parentid + '&page=1&rows=99999';
        } else if (type == "gw") {

            url = '/org/tree/json/findGwListByBmid?sjbmid=' + tmp_parentid + '&page=1&rows=99999';
        } else if (type == "ry") {
            url = '/org/tree/json/findRyListByGwid?sjgwid=' + tmp_parentid + '&page=1&rows=99999';
        } else {
        }
        $.ajax({
            type: "post",
            url: url,
            data: null,
            success: function (data, textStatus) {
                //console.log(data);
                //var json = JSON.parse(data);
                //alert(JSON.stringify(json));
                //var rows = json.rows.items;
                var rows=data.rows;
                var str = "";
                if (type == "ry") {
                    for (var i = 0; i < rows.length; i++) {
                        str += '<li class="ui-state-default" id="xh_' + rows[i]['id'] + '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' + rows[i]['account'] + '</li>';
                    }
                    ;
                } else {
                    for (var i = 0; i < rows.length; i++) {
                        str += '<li class="ui-state-default" id="xh_' + rows[i]['id'] + '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' + rows[i]['name'] + '</li>';
                    }
                    ;
                }
                $("#sortable").html('').append(str);
                $("#sortable").sortable();
                $("#sortable").disableSelection();
            }

        });
        layer_openHtml("排序", $("#div_sort"), null, {width: '350px', height: '400px'})
        $("#div_sort").show();


    }
    var saveSort = function () {
        if (sort_type == "bm") {
            url = '/org/department/json/saveSort';
        } else if (sort_type == "gw") {
            url = '/org/position/json/saveSort';
        } else if (sort_type == "ry") {
            url = "";
        } else {
        }
        var arr = $("#sortable").sortable('toArray');
        //Dumper.alert(arr);
        for (var i = 0; i < arr.length; i++) {
            arr[i] = arr[i] + "_" + (i + 1)
        }
        var sort = arr.join(',');
        $.ajax({
            type: "post",
            url: url,
            data: {sort: sort},
            success: function (data, textStatus) {
                ajaxReturnMsg(data);
                //  query("form_query");
                refreshNode();
                setTimeout(function () {
                    layer.closeAll();
                }, 1000);
            }
        });
    }
    //导入组织目录
    function importOrg() {
        $("#file_show").val("");
        layer_openHtml("导入组织目录", $("#div_import"), null, {width: '350px', height: '400px'})
    }
    function doImportOrg() {
        var filename = $("#file_show").val();
        if (isEmpty(filename)) {
            Fast.msg_error("请选择xls文件!");
            return;
        }

        var formData = new FormData($("#form_import")[0]);
        $.ajax({
            async : false,
            cache : false,
            contentType: false, //必须
            processData: false, //必须
            dataType : 'json',
            type:"post",
            url : '/org/organization/json/importOrg',
            data:formData,
            success:function(json,textStatus){
                ajaxReturnMsg(json,function(){
                    setTimeout(function(){
                        layer.closeAll();
                    },2500);
                });
            }
        });

    }
    function downloadExcel() {
        window.top.location.href = "/resources/excel/importOrg.xls";
    }
</script>
</body>
</html>
