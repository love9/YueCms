<!DOCTYPE html>
<html>
<head>
  <title>表单构建</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="/resources/lib/fontawesome/css/font-awesome.min.css">
    <script type="text/javascript" src="/resources/js/beautifyhtml.js"></script>
    <link href="/resources/css/web-icons/web-icons.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

    <script src="/resources/lib/My97DatePicker/WdatePicker.js"></script>

    <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
    <style type="text/css">
        .gray-bg {
            background-color: #f3f3f4;
        }
        .tools a{cursor:pointer;}
        .draggable{    cursor: move;}
        .hr-line-dashed {
            border-top: 1px dashed #e7eaec;
            color: #fff;
            background-color: #fff;
            height: 1px;
            margin: 20px 0;
        }
    </style>
<script type="text/javascript">

</script>
</head>
<body class="padding-top-35 gray-bg body-bg-formBuilder">
<div class="page-content container-fluid row">
    <div class="col-md-6 col-xs-12">
        <div class="panel panel-bordered">
            <div class="panel-heading">
                <h3 class="panel-title">元素选项 <span style="display: inline-block;" class="label label-info">拖拽左侧的表单元素到右侧区域，即可生成相应的HTML代码，表单代码，轻松搞定！</span></h3>
            </div>
            <div class="panel-body">
                <form role="form" class="form-horizontal m-t">
                <div class="form-group draggable "   >
                        <label class="col-sm-2 control-label">文本框：</label>
                        <div class="col-sm-7">
                            <input type="text" name="" class="form-control" placeholder="请输入文本" >
                        </div>
                        <div class="col-sm-3">
                            <span class="help-block m-b-none">说明文字</span>
                        </div>
                </div>

                    <div class="form-group draggable "   >
                        <label class="col-sm-2 control-label">开始日期：</label>
                        <div class="col-sm-7">
                            <input type="text" style="height:32px;"  id="starttime" name="starttime" placeholder="开始时间" value="" size="12"  class="Wdate form-control" datatype="*" nullmsg="请输入开始时间"  onFocus="var rpz=$dp.$('endtime');WdatePicker({onpicked:function(){rpz.focus();},maxDate:'#F{$dp.$D(\'endtime\')}'})" />
                        </div>
                        <div class="col-sm-3">
                            <span class="help-block m-b-none">说明文字</span>
                        </div>
                    </div>

                    <div class="form-group draggable "   >
                        <label class="col-sm-2 control-label">结束日期：</label>
                        <div class="col-sm-7">
                            <input type="text" style="height:32px;" id="endtime" name="endtime" value="" size="12" class="Wdate form-control" datatype="*" nullmsg="请输入结束时间"  onFocus="WdatePicker({minDate:'#F{$dp.$D(\'starttime\')}'})" />
                        </div>
                        <div class="col-sm-3">
                            <span class="help-block m-b-none">说明文字</span>
                        </div>
                    </div>

                    <div class="form-group draggable ui-draggable sortable ui-sortable"   >
                    <label class="col-sm-2 control-label">用户选择：</label>

                    <div class="col-sm-5">
                        <input type="text" id="assignToNames" disabled  name="assignToNames" placeholder="指派给" value="" class="form-control"  />
                    </div>
                    <div class="col-sm-2">
                        <button type="button" onclick="base_openYhxzPage('assignTo','assignToNames','limit=5')" style="display: inline-block;" class="btn btn-block btn-outline btn-primary">选择</button>
                        <input type="hidden" id="assignTo" readonly  name="assignTo" placeholder="指派给" value="" class="form-control" datatype="*" nullmsg="请选择完成人员！"/>
                    </div>
                    <div class="col-sm-3">
                        <span class="help-block m-b-none">说明文字</span>
                    </div>
                </div>

                <div class="form-group draggable ui-draggable sortable ui-sortable"   >
                        <label class="col-sm-2 control-label">地区选择：</label>

                        <div class="col-sm-5">
                            <input type="text" id="dqmc" disabled  name="dqmc" placeholder="地区" value="" class="form-control"  />
                        </div>
                        <div class="col-sm-2">
                            <button type="button" onclick="base_openAreaPage('dqid','dqmc')" style="display: inline-block;" class="btn btn-block btn-outline btn-primary">选择</button>
                            <input type="hidden" id="dqid" readonly  name="dqid" placeholder="地区ID" value="" class="form-control" datatype="*" nullmsg="请选择地区！"/>
                        </div>
                        <div class="col-sm-3">
                            <span class="help-block m-b-none">说明文字</span>
                        </div>
                </div>
                <div class="form-group draggable ui-draggable sortable ui-sortable"   >
                    <label class="col-sm-2 control-label">密码框：</label>
                    <div class="col-sm-7">
                        <input type="password" name="" class="form-control" placeholder="请输入密码" >
                    </div>
                    <div class="col-sm-3">
                        <span class="help-block m-b-none">说明文字</span>
                    </div>
                </div>
                <div class="form-group draggable ui-draggable sortable ui-sortable"   >
                    <label class="col-sm-2 control-label">单选框：</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input type="radio" checked="" value="option1" id="optionsRadios1" name="optionsRadios">选项1</label>
                        <label class="radio-inline">
                            <input type="radio" value="option2" id="optionsRadios2" name="optionsRadios">选项2</label>
                    </div>
                    <div class="col-sm-3">
                        <span class="help-block m-b-none">说明文字</span>
                    </div>
                </div>

                <div class="form-group draggable ui-draggable sortable ui-sortable"   >
                    <label class="col-sm-2 control-label">多选框：</label>
                    <div class="col-sm-7">
                        <label class="checkbox-inline">
                            <input type="checkbox" value="option1" id="inlineCheckbox1">选项1</label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="option2" id="inlineCheckbox2">选项2</label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="option3" id="inlineCheckbox3">选项3</label>
                    </div>
                    <div class="col-sm-3">
                        <span class="help-block m-b-none">说明文字</span>
                    </div>
                </div>

                <div class="form-group draggable ui-draggable sortable ui-sortable"   >
                    <label class="col-sm-2 control-label">下拉框选择：</label>
                    <div class="col-sm-7">
                         <span class="select-box" style="display: inline-block;">
                           <select id="stopusing"  name="stopusing" class="select " datatype="*" nullmsg="请输入是否停用" >
                               <option value="0" selected>启用</option>
                               <option value="1">停用</option>
                           </select>
                        </span>
                    </div>
                    <div class="col-sm-3">
                        <span class="help-block m-b-none">说明文字</span>
                    </div>
                </div>

                <div class="form-group draggable ui-draggable ui-sortable"   >
                    <label class="col-sm-2 control-label">文本域：</label>
                    <div class="col-sm-7  inline-block">
                        <textarea id="content" name=""  class="form-control"  style="width:100%;height:80px;">在这里输入文本</textarea>
                    </div>
                    <div class="col-sm-3">
                        <span class="help-block m-b-none">说明文字</span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group draggable ui-draggable ui-sortable"   >

                    <div class="col-sm-12 col-sm-offset-2">
                        <button class="btn btn-primary" >保存内容</button>
                        <button class="btn  btn-outline btn-default" >取消</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="panel panel-bordered">
            <div class="panel-heading">
                <h3 class="panel-title">拖拽左侧表单元素到此区域</h3>
            </div>
            <div id="htmlBox" class="panel-body droppable sortable form-body " style="min-height: 290px;">

            </div>
            <div class="panel-footer">
                <div class="btn-group">
                    <button type="button" id="copy-to-clipboard" class="btn btn-warning">复制代码</button>
                    <button type="button" onclick="clearAll()" class="btn btn-outline btn-warning">清空</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sus_ctx="";
    function clearAll(){
        $("#htmlBox").html("");
    }
    var setup_draggable = function () {
        $(".draggable").draggable({appendTo: "body", helper: "clone"});
        $(".droppable").droppable({
            accept: ".draggable",
            helper: "clone",
            hoverClass: "droppable-active",
            drop: function (event, ui) {
                $(".empty-form").remove();
                var $orig = $(ui.draggable);
                if (!$(ui.draggable).hasClass("dropped")) {
                    var $el = $orig.clone().addClass("dropped").css({
                        "position": "static",
                        "left": null,
                        "right": null
                    }).appendTo(this);
                    var id = $orig.find(":input").attr("id");
                    if (id) {
                        id = id.split("-").slice(0, -1).join("-") + "-" + (parseInt(id.split("-").slice(-1)[0]) + 1);
                        $orig.find(":input").attr("id", id);
                        $orig.find("label").attr("for", id)
                    }
                    $('<p class="tools col-sm-12 col-sm-offset-2"><a class="edit-link">编辑HTML<a> <a class="remove-link">移除</a></p>').appendTo($el)
                } else {
                    if ($(this)[0] != $orig.parent()[0]) {
                        var $el = $orig.clone().css({"position": "static", "left": null, "right": null}).appendTo(this);
                        $orig.remove()
                    }
                }
            }
        }).sortable()
    };
    var get_modal = function (content) {
        var modal = $('<div class="modal" style="overflow: auto;" tabindex="-1">	<div class="modal-dialog"><div class="modal-content"><div class="modal-header"><a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a><h4 class="modal-title">编辑HTML</h4></div><div class="modal-body ui-front">	<textarea class="form-control" 	style="min-height: 200px; margin-bottom: 10px;font-family: Monaco, Fixed">' + content + '</textarea><button class="btn btn-success">更新HTML</button></div>				</div></div></div>').appendTo(document.body);
        return modal
    };
	$(function(){
        setup_draggable();
        $(document).on("click", ".edit-link", function (ev) {
            var $el = $(this).parent().parent();
            var $el_copy = $el.clone();
            var $edit_btn = $el_copy.find(".edit-link").parent().remove();
            var $modal = get_modal(html_beautify($el_copy.html())).modal("show");
            $modal.find(":input:first").focus();
            $modal.find(".btn-success").click(function (ev2) {
                var html = $modal.find("textarea").val();
                if (!html) {
                    $el.remove()
                } else {
                    $el.html(html);
                    $edit_btn.appendTo($el)
                }
                $modal.modal("hide");
                return false
            })
        });
        $(document).on("click", ".remove-link", function (ev) {
            $(this).parent().parent().remove()
        });
        $("#copy-to-clipboard").on("click", function () {
            var $copy = $(".form-body ").clone().appendTo(document.body);
            $copy.find(".tools, :hidden").remove();
            $.each(["draggable", "droppable", "sortable", "dropped", "ui-sortable", "ui-draggable", "ui-droppable", "form-body","ui-draggable-handle"], function (i, c) {
                $copy.find("." + c).removeClass(c).removeAttr("style")
            });
            var html = html_beautify($copy.html());
            $copy.remove();
            $modal = get_modal(html).modal("show");
            $modal.find(".btn").remove();
            $modal.find(".modal-title").html("复制HTML代码");
            $modal.find(":input:first").select().focus();
            return false
        })

	})
</script>

</body>
</html>
