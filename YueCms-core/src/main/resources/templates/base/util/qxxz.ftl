<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <title>权限选择</title>
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <link  href="/resources/css/admui.css" rel="stylesheet" type="text/css"/>
  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
  <script  type="text/javascript" src="/resources/js/common/myutil.js"></script>
  <!-- EasyUI -->
  <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
  <link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
  <script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
  <script type="text/javascript" src="/resources/lib/easyui/easyloader.js"></script>
  <script type="text/javascript" src="/resources/lib/easyui/locale/easyui-lang-zh_CN.js"></script>
  <script type="text/javascript">
    var selectId="";var selectName="";
    var idInput='${idInput}';
    var mcInput='${mcInput}';
    var winindex = parent.layer.getFrameIndex(window.name);
    function clos(){
      parent.layer.close(winindex);
    }
    $(function() {
      loadTree();
    });
    function loadTree(){
      $('#trees').tree( {
        url :'/base/permission/json/tree?parentid=0',
        onBeforeExpand:function(node,param){
          //当节点展开之后触发.
          var id=node.id;
          if(id!='0'&&id!=0){
            $('#trees').tree('options').url = "/base/permission/json/tree?parentid="+id;
          }else{
           return;
          }
        },
        checkbox :false,
        lines:true,
        onClick:function(node){
        //alert(JSON.stringify(node));
          var id = node.id;
          var name=node.text;

          if(node.id=='0'||node.id==0||node.id==null){
            //layerMsg_cryIcon("您不能选择根节点！")
           // return;
            name="权限系统"
          }
          selectId=id;
          selectName=name;

        }
      });

    }
    function refreshTree(){
      $('#trees').tree('options').url = "/base/permission/json/tree?parentid=0";
      $('#trees').tree('reload');
    }
    function collapseAll() {
      var node = $('#trees').tree('getSelected');
      if (node) {
        $('#trees').tree('collapseAll', node.target);
      } else {
        $('#trees').tree('collapseAll');
      }
    }
    //全部收缩
    function expandAll() {
      var node = $('#trees').tree('getSelected');
      if (node) {
        $('#trees').tree('expandAll', node.target);
      } else {
        $('#trees').tree('expandAll');
      }
    }
    function resetFrameHeight(){}
    var save=function(){
      if(isEmpty(selectId)||isEmpty(selectName)){
        return;
      }
      parent.$('#'+idInput).val(selectId);
      parent.$('#'+mcInput).val(selectName);
      parent.layer.close(winindex);
    }

  </script>

</head>
<body class="body-bg-dqxz">

<div style="width:100%;height:calc(100% - 43px);overflow: auto;">
    <div style="height:5px;margin:5px 0 0 5px;width:220px;height:30px;">
      <a href="#" class="easyui-linkbutton" onClick="refreshTree()" plain="true">刷新</a>
      <a href="#" class="easyui-linkbutton" onClick="expandAll();" plain="true">展开</a>
      <a href="#" class="easyui-linkbutton" onClick="collapseAll();" plain="true">折叠</a>
    </div>
    <div  style="vertical-align: top;margin:0 0 0 5px;width:220px;">
      <div>
        <ul id="trees">

        </ul>
      </div>
    </div>
  <div class="row cl" style="display: inline-block;position: absolute;bottom: 10px;right:20px;margin-top:15px;">
    <div style="text-align: center;">
      <button type="button" onclick="clos()" class="btn btn-default size-S  fr" id="" name=""><i class="icon-ok"></i> 关闭</button>
      &nbsp;&nbsp;
      <button type="button" onclick="save()" class="btn btn-primary size-S  fr" id="" name=""><i class="icon-ok"></i> 确定</button>
    </div>
  </div>
</div>
</body></html>
