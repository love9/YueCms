<#--消息提示标签-->
<#macro myMsg msgObj  ui >
    <#if msgObj??>
        <#if ui =="semantic">
            <#if msgObj.type??>
                <#assign ctype='${msgObj.type}'>
            </#if>
            <#if msgObj.type=='error'>
                <#assign ctype="red">
            </#if>
            <#if msgObj.type=='success'>
                <#assign ctype="green">
            </#if>
            <div class="ui ${ctype} message">
                <i class="close icon"></i>
                <div class="header"></div>
                <p> ${msgObj.content}</p>
            </div>

        <#else>
            <#if msgObj.type??>
                <#assign ctype='${msgObj.type}'>
            </#if>
            <#if msgObj.type=='error'>
                <#assign ctype="danger">
            </#if>
            <div  id="msgBox" class="alert  dark alert-alt alert-icon alert-${ctype} alert-dismissible" >
                <button type="button" class="close" data-dismiss="alert" aria-label="关闭">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="icon wb-info" aria-hidden="true"></i>${msgObj.content}
            </div>

        </#if>
      <script type="text/javascript">

          $('#msgBox').show();

          setTimeout(function () {
              $('#msgBox').hide();
          }, 2500);
      </script>
    </#if>
</#macro>
<#--数字选择标签-->
<#macro codeEdit id  codetype  value  >
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<!--下面两个是使用Code Mirror必须引入的-->
<link rel="stylesheet" href="/resources/lib/codemirror/codemirror.css">
<script src="/resources/lib/codemirror/codemirror.js"></script>
<script src="/resources/lib/codemirror/addon/selection/selection-pointer.js"></script>
<!--支持那种语言-->

<script src="/resources/lib/codemirror/mode/javascript/javascript.js"></script>

<script src="/resources/lib/codemirror/mode/css/css.js"></script>

<script src="/resources/lib/codemirror/mode/vbscript/vbscript.js"></script>
<script src="/resources/lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<!--支持代码折叠-->
<link rel="stylesheet" href="/resources/lib/codemirror/addon/fold/foldgutter.css"/>
<script src="/resources/lib/codemirror/addon/fold/foldcode.js"></script>
<script src="/resources/lib/codemirror/addon/fold/foldgutter.js"></script>
<script src="/resources/lib/codemirror/addon/fold/brace-fold.js"></script>
<script src="/resources/lib/codemirror/addon/fold/comment-fold.js"></script>

<!--括号匹配-->
<script src="/resources/lib/codemirror/addon/edit/matchbrackets.js"></script>

<!--自动补全-->
<link rel="stylesheet" href="/resources/lib/codemirror/addon/hint/show-hint.css">
<script src="/resources/lib/codemirror/addon/hint/show-hint.js"></script>
<script src="/resources/lib/codemirror/addon/hint/anyword-hint.js"></script>
<style type="text/css">
    .CodeMirror{position:relative;border:1px solid #dadada;font-size:15px;width:100%;height:100%;}
</style>
<textarea id="${id!}">${value!}</textarea>
<script type="text/javascript">

    var mixedMode = {
        name: "htmlmixed",
        scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,
            mode: null},
            {matches: /(text|application)\/(x-)?vb(a|script)/i,
                mode: "vbscript"}]
    };
    var codeEdit;
    function createEditor(){
        if("${codetype!}"=="java"){
            codeEdit= CodeMirror.fromTextArea(document.getElementById("${id!}"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "text/x-java"
            });
        }else if("${codetype!}"=="javascript"){

            codeEdit= CodeMirror.fromTextArea(document.getElementById("${id!}"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "text/javascript"
            });
        }else if("${codetype!}"=="css"){

            codeEdit= CodeMirror.fromTextArea(document.getElementById("${id!}"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "text/css"
            });
        }else if("${codetype!}"=="xml"){

            codeEdit= CodeMirror.fromTextArea(document.getElementById("${id!}"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "application/xml"
            });
        }else if("${codetype!}"=="html"){
            codeEdit=CodeMirror.fromTextArea(document.getElementById("${id}"),{
                //Java高亮显示
                // mode:"text/javascript",
                mode:mixedMode,
                selectionPointer: true,
                //显示行号
                lineNumbers:true,
                // styleActiveLine: true, //line选择是是否加亮
                selectionPointer: true,
                //设置主题
                theme:"default",
                //绑定Vim
                // keyMap:"vim",
                //代码折叠
                lineWrapping:true,
                foldGutter: true,
                gutters:["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
                //全屏模式
                fullScreen:true,
                //括号匹配
                matchBrackets:true
                //extraKeys:{"Ctrl-Space":"autocomplete"}//ctrl-space唤起智能提示
            });
        }else{
            codeEdit=CodeMirror.fromTextArea(document.getElementById("${id!}"),{
                //Java高亮显示
                // mode:"text/javascript",
                mode:mixedMode,
                selectionPointer: true,
                //显示行号
                lineNumbers:true,
                // styleActiveLine: true, //line选择是是否加亮
                selectionPointer: true,
                //设置主题
                theme:"default",
                //绑定Vim
                // keyMap:"vim",
                //代码折叠
                lineWrapping:true,
                foldGutter: true,
                gutters:["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
                //全屏模式
                fullScreen:true,
                //括号匹配
                matchBrackets:true
                //extraKeys:{"Ctrl-Space":"autocomplete"}//ctrl-space唤起智能提示
            });
        }
    }

    function setValue(v){
        codeEdit.setValue(v);
    }
    function getValue(){
        var v=  codeEdit.getValue();
        return v;
    }
    $(function(){
        setTimeout(function(){ createEditor();},200)
    });
</script>


</#macro>
<#--字典选择标签-->
<#macro dicSelect id  value idAttr changeCallBack>
<span class="select-box form-control">
<#if idAttr?? && idAttr!="">

<#else>
    <#if hiddenPrefix?? && hiddenPrefix!="">
        <input type="hidden" id="${hiddenPrefix}${id}" name="${hiddenPrefix}${id}" datatype="${datatype}" nullmsg="${nullmsg}" value="${value!}"  >
    <#else>
        <input type="hidden" id="${id}" name="${id}" datatype="${datatype}" nullmsg="${nullmsg}" value="${value!}"  >
    </#if>
</#if>

	<select id="dicSelect_${id}"  name="dicSelect_${id}" onchange="${id}_SelectChange(this)" style="${cssStyle!}" class="select ${cssClass!}" >
	</select>
</span>

<script type="text/javascript">

    //请求url获取数据，把dm作为value，mc作为text显示到select中
    $.ajax({
        type:"post",
        url:"${request.contextPath}/base/dictionary/json/select?type=${id}",
        data:null,
        success:function(json,textStatus){
            json=(typeof json=='string')?JSON.parse(json):json;
            bind(json,true);
            if(notEmpty("${idAttr}"))
            {
                var v=$("#${idAttr}").val() ;
                $("#dicSelect_${id}").val(v);
            }else{
                var v=$("#${id}").val() ;
                $("#dicSelect_${id}").val(v);
            }
                ${changeCallBack};
        }
    });

    function ${id}_SelectChange(obj){
        if(notEmpty("${idAttr}"))
        {
            $("#${idAttr}").val($(obj).val()) ;
            $("#${idAttr}").blur();
        }else{

            $("#${id}").val($(obj).val()) ;
            $("#${id}").blur();

        }
            ${changeCallBack};

    }

    setTimeout(function(){

    },1000);
    function isEmpty(val) {
        val = $.trim(val);
        if (val == null)
            return true;
        if (val == undefined || val == 'undefined')
            return true;
        if (val == "")
            return true;
        if (val.length == 0)
            return true;
        if (!/[^(^\s*)|(\s*$)]/.test(val))
            return true;
        return false;
    }

    function notEmpty(val) {
        return !isEmpty(val);
    }
</script>
</#macro>

<#--mySelect标签-->
<#macro mySelect id  idAttr changeCallBack url>
<span class="select-box form-control">

	<select id="${id}"  name="${id}" onchange="${id}_SelectChange(this)" class="select" >
	</select>
</span>

<script type="text/javascript">

    //请求url获取数据，把dm作为value，mc作为text显示到select中
    var  url='${url}';
    $.ajax({
        type:"post",
        url:url,
        data:null,
        success:function(json,textStatus){
            json=(typeof json=='string')?JSON.parse(json):json;
            bind(json,true);
            if(notEmpty("${idAttr}"))
            {
                var v=$("#${idAttr}").val() ;
                $("#${id}").val(v);
            }else{
                var v=$("#${id}").val() ;
                $("#${id}").val(v);
            }
                ${changeCallBack};
        }
    });

    function ${id}_SelectChange(obj){
        if(notEmpty("${idAttr}"))
        {
            $("#${idAttr}").val($(obj).val()) ;
            $("#${idAttr}").blur();
        }else{

            $("#${id}").val($(obj).val()) ;
            $("#${id}").blur();

        }
            ${changeCallBack};

    }

    setTimeout(function(){

    },1000);
    function isEmpty(val) {
        val = $.trim(val);
        if (val == null)
            return true;
        if (val == undefined || val == 'undefined')
            return true;
        if (val == "")
            return true;
        if (val.length == 0)
            return true;
        if (!/[^(^\s*)|(\s*$)]/.test(val))
            return true;
        return false;
    }

    function notEmpty(val) {
        return !isEmpty(val);
    }
</script>
</#macro>

<#macro commonScript>
<script type="text/javascript">
    var sys_ctx='${request.contextPath}';
</script>
</#macro>

<#macro ueditorScript>
<script type="text/javascript" src="${request.contextPath}/resources/lib/ueditor/1.4.3/ueditor.config.js"></script>
<script type="text/javascript" src="${request.contextPath}/resources/lib/ueditor/1.4.3/ueditor.all.min.js"></script>
<script type="text/javascript" charset="utf-8" src="${request.contextPath}/resources/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
</#macro>


