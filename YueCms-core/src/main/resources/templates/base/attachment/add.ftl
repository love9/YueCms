<!DOCTYPE html>
<html>
<head>
  <title>新增附件</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">

    <link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body class="body-bg-add">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
		<h2 class="text-center">新增附件</h2>

        <div class="form-group">
              <label class="col-sm-2 control-label">附件组唯一ID：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="zid" name="zid" placeholder="附件组唯一ID" value="" class="form-control" datatype="*" nullmsg="请输入附件组唯一ID" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="yhid" name="yhid" placeholder="" value="" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">所属模块：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="module" name="module" placeholder="所属模块" value="" class="form-control" datatype="*" nullmsg="请输入所属模块" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件名称：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="filename" name="filename" placeholder="文件名称" value="" class="form-control" datatype="*" nullmsg="请输入文件名称" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件路径：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="filepath" name="filepath" placeholder="文件路径" value="" class="form-control" datatype="*" nullmsg="请输入文件路径" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">文件空间：</label>
               <div class="col-sm-6 formControls">
                    <input type="text" id="size" name="size" placeholder="文件空间" value="" class="form-control" datatype="*" nullmsg="请输入文件空间" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">后缀：</label>
               <div class="col-sm-6 formControls">
                     <input type="text" id="suffixes" name="suffixes" placeholder="后缀" value="" class="form-control" datatype="*" nullmsg="请输入后缀" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

    </form></div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/base/attachment/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		 btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
