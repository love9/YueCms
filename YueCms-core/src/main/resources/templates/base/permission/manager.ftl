<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>菜单权限管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">

</head>
<body class="body-bg">
<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span> 菜单权限管理</nav>
<div class="margin-left-5 animated fadeInRight" style=" ">
<div style="position:relative;width:100%;overflow: hidden">

    <div class="clearfix" style="width:185px; height: auto ; float:left;overflow-y: auto;" id="west">
        <div style="height:5px;margin:5px 0 0 0px;width:180px;height:30px;">
            <a href="#" class="easyui-linkbutton" onClick="refreshTree()" plain="true">刷新</a>
        </div>
        <div  style="vertical-align: top;margin:0 0 0 0;width:180px;height:100%;overflow-y: auto;">
            <div>
                <ul id="trees">

                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix main-bg " style="float: left;width: calc(100% - 185px);height: 125%">
        <div id="mainPanle"   width="100%;" style="margin:0 0 0 0;overflow-y: auto;">
         <!--工具条-->
          <div class="cl toolbar" style="margin-top:0px;">
              <form id="form_query" style="margin-bottom: 0px">
                  <div class="btn btn-group" style="padding-left:0px">
                          <a onclick="addNew()" class="btn btn-outline  btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                          <a onclick="list_del('tableList')" class="btn btn-outline  btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                          <a href="javascript:void(0)"  onClick="sortMenu();" class="btn btn-outline  btn-info" ><i class="fa fa-sort"></i>&nbsp;排序</a>
                          <a class="btn btn-outline btn-warning " onclick="modifySjid()">
                              <i class="fa fa-upload"></i>&nbsp;&nbsp;<span class="bold">调整上级</span>
                          </a>
                      <a  onclick="query('form_query');" class="btn btn-outline btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                  </div>

                  <input  type="hidden"  id="sjid" name="sjid" value="" />
                  <input type="hidden" id="sjname" name="sjname" value="" />
              </form>
          </div>

         <!--数据列表-->
          <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>
          </div>
    </div>

</div>


</div>

<div id="div_sort" title="菜单排序" style="width:350px;height:400px;display: none;">
    <div class="c_div_tool_bar">
        <a href="javascript:void(0)" class="btn btn-primary btn-outline  size-S" style="margin:4px;" onClick="saveSort()">保存</a>
    </div>
    <div class="c_div_show_content">
        <form id="form_sort">
            <div id="dd" style="width:100%;">
                <ul id="sortable">

                </ul>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>

<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="/resources/lib/easyui/themes/icon.css">
<script type="text/javascript" src="/resources/lib/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" >
    var sys_ctx="";
    var tmp_parentid=0;
    var tmp_parentname;
    function loadTree(){
        $('#trees').tree( {
            url :'/base/permission/json/tree?parentid=0',
            checkbox :false,
            lines:true,
            onBeforeExpand:function(node,param){
                //当节点展开之后触发.
                var id=node.id;
                if(id!='0'&&id!=0){
                    $('#trees').tree('options').url = "/base/permission/json/tree?parentid="+id;
                }else{
                    return;
                }
            },
            onLoadSuccess:function(){
                tmp_parentname=$('#trees').tree('getRoot').text;
            },
            onClick:function(node){
                var id = node.id;
                tmp_parentname=node.text;
                tmp_parentid=id;
                sys_table_list();
            }
        });
    }
    /**
    *   重新刷新整个数
     */
    function refreshTree(){
        $('#trees').tree('options').url = "/base/permission/json/tree?parentid=0";
        $('#trees').tree('reload');
    }
    function collapseAll() {
        var node = $('#trees').tree('getSelected');
        if (node) {
            $('#trees').tree('collapseAll', node.target);
        } else {
            $('#trees').tree('collapseAll');
        }
    }
    //全部收缩
    function expandAll() {
        var node = $('#trees').tree('getSelected');
        if (node) {
            $('#trees').tree('expandAll', node.target);
        } else {
            $('#trees').tree('expandAll');
        }
    }
    //刷新某个节点
    function  refreshNode(){
        var node = $('#trees').tree('getSelected');
        if (node){
            $('#trees').tree('reload', node.target);
        }else{
            refreshTree();
        }
    }
    function resetFrameHeight(){}
    var queryStr="?time="+ new Date().getTime();
    //查询功能的标签说明
    var table_list_query_form = {
        parentid:tmp_parentid
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }


    $(function(){
        loadTree();
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        $('#tableList').bootstrapTable({
            columns: [{checkbox:true},
                {field: 'id',align:"center", sortable:true,order:"asc",visible:false,title: '序号'},
                {field: 'parentid',align:"center",visible:false,title: '权限父ID'},
                {field: 'parentids',align:"center",visible:false,title: '所有父级编号'},
                {field: 'qxlx',align:"center",visible:true,title: '权限类型',width:"100px",
                formatter:function(value, row, index){
                    //alert(JSON.stringify(value)+"====="+value);
                    if(value=="1"){
                        value="<i class=\"fa fa-file-code-o\" ></i>&nbsp;&nbsp;页面";
                    }else if(value=="2"){
                        value="<i class=\"fa fa-bold\" ></i>&nbsp;&nbsp;按钮";
                    }else if(value=="0"){
                        value="<i class=\"fa fa-folder\" ></i>&nbsp;&nbsp;文件夹";
                    }else{}

                    return value;
                }},
                {field: 'name',align:"left",visible:true,title: '权限名称',width:"120px"},
                {field: 'url',align:"left",visible:true,title: '权限(url:黑色 | 权限代码：蓝色)',formatter:function(value, row, index){
                    var v="";
                    if(isNotEmpty(row.url)){
                        v = row.url;
                        if(isNotEmpty(row.code)){
                            v += "<font color='gray'> | </font>";
                        }
                    }
                    if(isNotEmpty(row.code)){
                        v += "<font color='blue'>" +row.code+"</font>";
                    }
                    return v;
                }},
                /*{field: 'code',align:"left",visible:true,title: '权限代码' },*/
                {field: 'icon',align:"center",visible:true,title: '图标',width:"60px",
                formatter:function(value, row, index){
                   value="<i class='"+value+"'></i>";
                    return value;
                }},
                {field: 'sort',align:"center",visible:true,title: '排序',width:"60px"},
                {field: 'available',title: '是否有效',align:"center",visible:true,width:"90px",
                    formatter:function(value, row, index){
                        if(value==1){
                            value = '<font color="green">有效</font>';
                        }else{
                            value = '<font color="gray">无效</font>';
                        }
                        return value;
                    }},
                {field: 'ismenu',title: '是否菜单',align:"center",visible:true,width:"90px",
                    formatter:function(value, row, index){
                        if(value==1){
                            value = '<font color="green">是</font>';
                        }else{
                            value = '<font color="red">否</font>';
                        }
                        return value;
                    }},
                {field: 'description',title: '描述',visible:false}],
            pagination: true,  //开启分页
            sidePagination: 'server',//服务器端分页
            pageNumber: 1,//默认加载页
            pageSize: 10,//每页数据
            pageList: [10,20, 50, 100, 500],//可选的每页数据
            queryParams:function (params) {
                return {
                    parentid:tmp_parentid,
                    rows: params.limit,
                    page: (params.offset / params.limit + 1),
                    offset: params.offset
                }
            }
            ,//请求服务器数据时的参数
            onClickRow:onClickRow,
            url:'/base/permission/json/find'+queryStr //服务器数据的加载地址
        });
    }
    var onClickRow=function(row,tr){
        $.layer.open_page("编辑菜单权限",sys_ctx+"/base/permission/edit?id="+row.id,{
            w:"800px",
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }
    function addNew(){
        var params="?parentid="+tmp_parentid+"&parentname="+tmp_parentname;
        $.layer.open_page("新增菜单权限",sys_ctx+"/base/permission/add"+params,{
            w:"800px",
            end:function(){
                sys_table_list();
                refreshNode();
            }
        });
    }
    function save(){
        var params=$("#form_show").serialize();
        $.ajax({
            type:"post",
            url:'/base/permission/json/save?'+params,
            data:null,
            success:function(json,textStatus){
                ajaxReturnMsg(json);
                query("form_query");
            }
        });
    }
    function modifySjid(){
        var ids="";
        $("#sjid").val("");
        $("#sjname").val("");
        var selecRow = $("#tableList").bootstrapTable('getSelections');
        if(selecRow.length > 0){
            for(var i=0;i<selecRow.length;i++){
                ids+=selecRow[i]["id"]+"~";
            }
            alert(ids);
            base_openMenuxzPage("sjid","sjname", function(){
                        var id=$("#sjid").val();
                        var name=$("#sjname").val();
                        alert(id+"===="+name);
                        if(isNotEmpty(id)){
                            var title="确认要将一下菜单移动到【"+name+"】下吗？";
                            Fast.confirm(title,function(index){
                                $.ajax({
                                    type:"post",
                                    url:"/base/permission/json/modifySjid",
                                    data:{sjid:id,ids:ids},
                                    success:function(json){
                                        ajaxReturnMsg(json);
                                        refreshNode();
                                    }
                                });
                            });
                        }
              }
            );
        }else
        {
           // Fast.msg_warning("请勾选要调整的菜单");
            Fast.msg_warning("请勾选要调整的菜单")
        }
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/base/permission/json/removes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data);
                        refreshData();
                        refreshNode();
                    }
                });
            });
        }else{
            //Fast.msg_warning("请选择要删除的数据")
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?time="+ new Date().getTime();
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
    var sortMenu = function(){
        $.ajax({
            type:"post",
            url:'/base/permission/json/findByParentid/'+tmp_parentid,
            data:null,
            success:function(data,textStatus){
                var data=typeof data=='string'?JSON.parse(data):data;
                var rows=data.rows;
                var str = "";
                for(var i=0;i<rows.length;i++){
                    str += '<li class="ui-state-default" id="xh_'+rows[i]['id']+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+rows[i]['name']+'</li>';
                };
                $("#sortable").html('').append(str);
                $("#sortable").sortable();
                $("#sortable").disableSelection();
            }
        });
        layer_openHtml("排序",$("#div_sort"),null,{width:'350px',height:'400px'})
        $("#div_sort").show();
    }
    var saveSort = function(){
        var arr = $("#sortable").sortable('toArray');
        for(var i=0;i<arr.length;i++){
            arr[i] = arr[i] + "_"+(i+1)
        }
        var sort = arr.join(',');
        $.ajax({
            type:"post",
            url:'/base/permission/json/saveSort',
            data:{sort:sort},
            success:function(data,textStatus){
                ajaxReturnMsg(data);
                query("form_query");
                refreshNode();

                setTimeout(function(){
                    layer.closeAll();
                },1000);

            }
        });
    }
</script>
</body></html>
