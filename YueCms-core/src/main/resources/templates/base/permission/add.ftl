<!DOCTYPE html>
<html>
<head>
  <title>新增权限菜单</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
  <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

  <link rel="shortcut icon" href="favicon.ico">
  <link href="/resources/css/admui.css" rel="stylesheet">
  <script type="text/javascript" src="/resources/js/jquery.min.js"></script>
  <script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
    <script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="/resources/js/common/myutil.js"></script>
  <script>

  </script>
  <style>
    .mui-switch {
      width: 52px;
      height:25px;
      position: relative;
      border: 1px solid #dfdfdf;
      background-color: #fdfdfd;
      box-shadow: #dfdfdf 0 0 0 0 inset;
      border-radius: 20px;
      border-top-left-radius: 20px;
      border-top-right-radius: 20px;
      border-bottom-left-radius: 20px;
      border-bottom-right-radius: 20px;
      background-clip: content-box;
      display: inline-block;
      -webkit-appearance: none;
      user-select: none;
      outline: none; }
    .mui-switch:before {
      content: '';
      width: 24px;
      height: 24px;
      position: absolute;
      top: 0px;
      left: 0;
      border-radius: 20px;
      border-top-left-radius: 20px;
      border-top-right-radius: 20px;
      border-bottom-left-radius: 20px;
      border-bottom-right-radius: 20px;
      background-color: #fff;
      box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4); }
    .mui-switch:checked {
      border-color: #64bd63;
      box-shadow: #64bd63 0 0 0 16px inset;
      background-color: #64bd63; }
    .mui-switch:checked:before {
      left: 26px; }
    .mui-switch.mui-switch-animbg {
      transition: background-color ease 0.4s; }
    .mui-switch.mui-switch-animbg:before {
      transition: left 0.3s; }
    .mui-switch.mui-switch-animbg:checked {
      box-shadow: #dfdfdf 0 0 0 0 inset;
      background-color: #64bd63;
      transition: border-color 0.4s, background-color ease 0.4s; }
    .mui-switch.mui-switch-animbg:checked:before {
      transition: left 0.3s; }
    .mui-switch.mui-switch-anim {
      transition: border cubic-bezier(0, 0, 0, 1) 0.4s, box-shadow cubic-bezier(0, 0, 0, 1) 0.4s; }
    .mui-switch.mui-switch-anim:before {
      transition: left 0.3s; }
    .mui-switch.mui-switch-anim:checked {
      box-shadow: #64bd63 0 0 0 16px inset;
      background-color: #64bd63;
      transition: border ease 0.4s, box-shadow ease 0.4s, background-color ease 1.2s; }
    .mui-switch.mui-switch-anim:checked:before {
      transition: left 0.3s; }
  </style>
</head>
<body class="body-bg-add">
<div class="wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal " role="form">

      <h2 class="text-center"></h2>
      <div class="form-group">
        <label class="control-label col-sm-3"><span class="text-danger">*</span>父权限：</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" readonly  id="parentname"  name="parentname"   value="${parentname}" />
          <input type="hidden"  id="parentid" name="parentid" value="${parentid}"   />
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-3"><span class="text-danger">*</span>权限名称：</label>
        <div class="col-sm-6 formControls">
          <input type="text" class="form-control"  id="name"  name="name" placeholder="权限名称"  datatype="*" nullmsg="请输入权限名称" />
        </div>
        <div class="col-sm-3">
          <div class="Validform_checktip"></div>
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-sm-3"><span class="text-danger">*</span>权限类型:</label>
        <input type="hidden" id="qxlx" name="qxlx" value="1">
        <div class="col-sm-6 formControls">


          <label><input type="radio" value="1"  name="option1" checked>页面</label><!--有url-->
          <label><input type="radio"  value="0"    name="option1" >目录</label><!--没有url-->
          <label><input type="radio"  value="2"  name="option1">按钮</label><!--保留暂未实现-->
            <script>
                $("input[name='option1']").click(function(){
                  //alert($(this).val());
                  $("#qxlx").val($(this).val());
                  if($(this).val()=="0"){
                    $("#div_url").hide();
                    $("#url").removeAttr("datatype");
                  }else{
                    $("#div_url").show();
                    $("#url").attr("datatype","*");
                  }
                });
            </script>
        </div>
        <div class="col-sm-3">
          <div class="Validform_checktip"></div>
        </div>
      </div>

      <div class="form-group" id="div_url">
        <label class="control-label col-sm-3">权限url:</label>
        <div class="col-sm-6 formControls">
          <input type="text" class="form-control"  id="url"  name="url"  value="" datatype="restfulURL" ignore="ignore" nullmsg="请输入权限url,格式如/sys/user或/sys/user/add" />
        </div>
        <div class="col-sm-3">
          <div class="Validform_checktip"></div>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3">权限代码:</label>
        <div class="col-sm-6 formControls">
          <input type="text" class="form-control"  id="code"  name="code"  datatype="permissionCode" ignore="ignore" nullmsg="请输入权限代码，格式 sys.user或sys.user.add" />
        </div>
        <div class="col-sm-3">
          <div class="Validform_checktip"></div>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3">图标:</label>
        <div class="col-sm-6 formControls">
          <input type="text" class="form-control"  id="icon"  name="icon" style="float:left;width: 50%;" onclick="base_openIconPage('icon',$(this).val())"/>
          <i style="line-height:35px;margin-left:10px;width: 20px;height:20px;display: inline-block;float:left;font-size: 20px;" class=""></i>
        </div>
        <div class="col-sm-3">
          <div class="Validform_checktip"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-3"><span class="text-danger">*</span>是否有效:</label>
        <div class="col-sm-6 formControls">
          <input type="hidden" id="available" name="available" value="1" />
          <label><input class="mui-switch mui-switch-anim switch-available" type="checkbox" checked></label>
        </div>
        <div class="col-sm-3">
          <div class="Validform_checktip"></div>
        </div>
      </div>


      <div class="form-group">
      <label class="control-label col-sm-3"><span class="text-danger">*</span>是否菜单:</label>
      <div class="col-sm-6 formControls">
        <input type="hidden" id="ismenu" name="ismenu" value="1">
        <label><input class="mui-switch mui-switch-anim switch-ismenu" type="checkbox" checked></label>
      </div>
      <div class="col-sm-3">
        <div class="Validform_checktip"></div>
      </div>
    </div>

      <div class="form-group">
        <label class="control-label col-sm-3">描述:</label>
        <div class="col-sm-6 formControls">
          <textarea rows="2" style="height: 50px;" class="form-control"  placeholder="" id="description" name="description"> </textarea>
        </div>
        <div class="col-sm-3">
          <div class="Validform_checktip"></div>
        </div>
      </div>

     <!--<div class="form-group">
        <div class=" col-sm-10 col-sm-offset-3">
          <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
        </div>
      </div>-->


    </form>

  </div>
</div>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>


<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript">
  var sys_ctx="";
  var validform;
  function save(){
    var b=validform.check(false);
    if(!b)
    {
      return;
    }
    var params=$("#form_show").serialize();
    $.ajax({
      type:"post",
      url:'/base/permission/json/save?'+params,
      data:null,
      success:function(json,textStatus){
        ajaxReturnMsg(json);
        setTimeout(function(){
          var index = parent.layer.getFrameIndex(window.name);
          parent.layer.close(index);
        },1000);
      }
    });
  }
  $(function(){
   $(".switch-available").on("click",function(){
     var v=$("#available").val();
     if(v==1||v=='1'){
       $("#available").val("0");
     }else{
       $("#available").val("1");
     }

     $(".switch-ismenu").on("click",function(){
       var v=$("#ismenu").val();
       if(v==1||v=='1'){
         $("#ismenu").val("0");
       }else{
         $("#ismenu").val("1");
       } });
   });
    validform=$("#form_show").Validform({
      btnReset:"#reset",
      tiptype:2,
      postonce:true,//至提交一次
      ajaxPost:false,//ajax方式提交
      showAllError:true //默认 即逐条验证,true验证全部
    });
  })
</script>

</body>
</html>
