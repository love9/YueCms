<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>系统参数管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/resources/lib/bootstrap-validator/css/bootstrapValidator.min.css" rel="stylesheet" />

</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span>系统参数管理</nav>
<div class="wrapper wrapper-content animated fadeInRight">
    <!--工具条-->
    <div class="cl toolbar">
        <form id="form_query" style="margin-bottom: 15px">
            <div class="btn-group">
                <a onclick="addNew()" class="btn btn-outline  btn-primary"><i class="fa fa-plus"></i>&nbsp;新增</a>
                <a onclick="list_del('tableList')" class="btn btn-outline  btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a id="open_search"  onclick="javascript:$('#search_hidden').show();$(this).hide();" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
            <div id="search_hidden"  style="display: none;margin-top: 8px" >
                <div class="btn-group">
                <a  onclick="query('form_query');" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
                <a  href="javascript:void(0)" id="btn_reset" onClick="search_form_reset('form_query')" class="btn btn-outline  btn-info"><i class="fa fa-reply"></i>&nbsp;重置</a>
                <a  onclick="javascript:$('#search_hidden').hide();$('#open_search').show();" class="btn btn-default btn-outline "><i class="fa fa-close"></i>&nbsp;关闭</a>
                </div>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>




</div>
<!--add Modal -->
<div id="modal-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <form id="form_show" class="form-horizontal" role="form">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>对话框标题</h3>
                </div>


                <div class="modal-body">

                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">模块代码：</label>
                        <div class="col-sm-9">
                            <input type="text" id="mk_dm" name="mk_dm"  placeholder="请输入模块代码" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label">参数代码：</label>
                        <div class="col-sm-9">
                            <input type="text" id="csdm" name="csdm"  placeholder="请输入参数代码" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label   class="col-sm-3 control-label">参数值：</label>

                        <div class="col-sm-9">
                            <textarea id="csz" name="csz" rows="3" style="width: 100%" class="form-control"  placeholder="参数值"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="submit"  id="saveBtn"  class="btn btn-primary" >保存</button>
                    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="/resources/lib/bootstrap-validator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" >

    var bootstrapValidator = $('#form_show').data('bootstrapValidator');
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {

    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();


        $('form').bootstrapValidator({
             message: 'This value is not valid',
            excluded: [':disabled'],
            feedbackIcons: {
             valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
            },
        fields: {
            mk_dm: {
                message: '模块代码验证失败',
                        validators: {
                    notEmpty: {
                        message: '模块代码不能为空'
                    }
                }
            },
            csdm: {
                message: '参数代码验证失败',
                validators: {
                    notEmpty: {
                        message: '参数代码不能为空'
                    }
                }
            },
            csz: {
                message: '参数值验证失败',
                validators: {
                    notEmpty: {
                        message: '参数值不能为空'
                    }
                }
            }
        },
            submitHandler: function (validator, form, submitButton) {
                alert("submit");
            }
    }).on('success.form.bv', function (e) {
            e.preventDefault();
           save();

        });

    });

    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},{field: 'id', align:"center", sortable:true, order:"asc", visible:false,title: '序号'},
            {field: 'mk_dm', align:"center", visible:true, width:'10%', title: '模块代码'},
            {field: 'csdm', visible:true, width:'15%', title: '参数代码'},
            {field: 'csz', visible:true, title: '参数值'},
            {field: 'cssm', title: '参数说明',
                formatter:function(value, row, index){
                    value = '<font color="red">'+value+'</font>';
                    return value;
                }
            }];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=onClickRow;
        table_list_Params.url='/base/para/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }


    var onClickRow=function(row,tr){

        $('#modal-form').on('show.bs.modal', function() {
            $('#form_show').bootstrapValidator('resetForm', true);
            bind(JSON.stringify(row));
        });
        $('#modal-form').modal('show');
    }
    function addNew(){
        $('#modal-form').on('show.bs.modal', function() {
            $('#form_show').bootstrapValidator('resetForm', true);
            $('#form_show')[0].reset();
            $('#id').val("");
        });
        $('#modal-form').modal('show');
    }

    function save(){
        var b=$("#form_show").data('bootstrapValidator').isValid();
        if(!b)
        {
            return;
        }


        var params=$("#form_show").serialize();
        $.ajax({
            type:"post",
            url:'/base/para/json/save?'+params,
            data:null,
            success:function(json,textStatus){
                $('#modal-form').modal('hide')
               // $("#modal-form").modal("close");
                ajaxReturnMsg(json);
                query("form_query");
            }
        });
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            $.layer.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/base/para/json/removes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        //layer.closeAll();
                        ajaxReturnMsg(data);
                        // query("form_query");
                        refreshData();
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }

    function form_reset(formid){
        $('#'+formid).bootstrapValidator('resetForm', true);
    }
</script>
</body></html>
