<!DOCTYPE html>
<html>
<head>
	<title>编辑区域</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">

	<link href="/resources/css/admui.css" rel="stylesheet" />

</head>
<body class="body-bg-edit">
<div class="wrapper  animated fadeInRight">
	<div class="container-fluid">
		<form action="" id="form_show" method="post" class="form-horizontal" role="form">
			<h2 class="text-center">编辑区域</h2>
			<input type="hidden" value="${area.id}" id="id" name="id"/>
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="text-danger">*</span>区域类型：</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" readonly  id="areatypename"  name="areatypename"  value="${area.areatypename}" />
					<input type="hidden"  name="areatype"   value="${area.areatype}" />
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2"><span class="text-danger">*</span>上级区域：</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" readonly  id="parentname"  name="parentname"  value="${area.parentname}" />
					<input type="hidden"   name="parentid"   value="${area.parentid}" />
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2"><span class="text-danger">*</span>区域编码:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  id="code"  name="code" placeholder="区域编码"  value="${area.code}" />
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2"><span class="text-danger">*</span>区域名称:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  id="name" name="name" value="${area.name}"   placeholder="区域名称" />
				</div>
			</div>

			<div class="form-group">
				<div class=" col-sm-10 col-sm-offset-2">
					<button type="submit" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
				</div>
			</div>
		</form></div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>

<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>


<script src="/resources/lib/bootstrap-validator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript">

	function save(){

		var b=$("#form_show").data('bootstrapValidator').isValid();
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/base/area/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){

		$('form').bootstrapValidator({
			message: 'This value is not valid',
			excluded: [':disabled'],
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
				name: {
					message: '区域名称验证失败',
					validators: {
						notEmpty: {
							message: '区域名称不能为空'
						}
					}
				},
				code: {
					message: '区域编码验证失败',
					validators: {
						notEmpty: {
							message: '区域编码不能为空'
						}
					}
				}
			},
			submitHandler: function (validator, form, submitButton) {
				alert("submit");
			}
		}).on('success.form.bv', function (e) {
			e.preventDefault();
			save();
		});

	})
</script>

</body>
</html>