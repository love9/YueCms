<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>操作日志管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">


</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> base <span class="c-gray en">&gt;</span>操作日志管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl toolbar">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
                <div class="btn-group">
                <a onclick="list_del('tableList')" class="btn btn-outline btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                <a  onclick="query('form_query');" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" >
    var sys_ctx="";
    var validform;
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
 	    validform=$("#form_show").Validform({
      		  btnReset:"#reset",
              tiptype:2,
              postonce:true,//至提交一次
              ajaxPost:false,//ajax方式提交
              showAllError:true //默认 即逐条验证,true验证全部
            });
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:true,title: '请求人Id'},
            {field: 'yhmc',align:"center",sortable:true,order:"asc",visible:true,title: '请求人'},
            {field: 'uri',align:"center",sortable:true,order:"asc",visible:true,title: '请求的uri'},
            {field: 'params',align:"center",sortable:true,order:"asc",visible:true,title: '请求参数'},
            {field: 'method',align:"center",sortable:true,order:"asc",visible:true,title: '方法'},
            {field: 'description',align:"center",sortable:true,order:"asc",visible:true,title: '描述'},
            {field: 'result',align:"center",sortable:true,order:"asc",visible:true,title: '执行结果'},
            {field: 'actionTime',align:"center",sortable:true,order:"asc",visible:true,title: '消耗的时间'},
            {field: 'type',align:"center",sortable:true,order:"asc",visible:true,title: '类型'},
        ];
        table_list_Params.columns=columns;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/base/actionlog/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
        layer_open("编辑操作日志",sys_ctx+"/base/actionlog/edit?id="+row.id,null,
            function(){$('#tableList').bootstrapTable('refresh');
        });
    
    }
    function addNew(){
        layer_open("新增操作日志",sys_ctx+"/base/actionlog/add",null,
            function(){$('#tableList').bootstrapTable('refresh');
        });
    }
    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            $.layer.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:sys_ctx+'/base/actionlog/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        //alert(JSON.stringify(data));
                        ajaxReturnMsg(data);
                        sys_table_list();
                    }
                    // ,error:ajaxError()
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据")
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>