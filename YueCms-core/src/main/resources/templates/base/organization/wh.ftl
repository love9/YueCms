<!DOCTYPE html>
<html>
<head>
  <title>组织机构信息维护</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="shortcut icon" href="favicon.ico">

  <link type="text/css" rel="stylesheet" href="/resources/lib/fontawesome/css/font-awesome.min.css">
  <link href="/resources/css/admui.css" rel="stylesheet">

</head>
<body class="body-bg main-bg">
<div class="container-fluid">
  <form action="" id="form_show" method="post" class="form-horizontal">
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-6">
        <h2 style="text-align:center;">组织机构信息维护</h2>
      </div>
    </div>
    <input type="hidden" value="${organization.id}" id="id" name="id"/>
    <div class="form-group">
      <label class="control-label col-sm-2"><span class="c-red">*</span>组织名称：</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" value="${organization.name!}" placeholder="" id="name" name="name" datatype="*" nullmsg="请输入组织名称">
      </div>
      <div class="col-sm-4"><div class="Validform_checktip"></div></div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">描述：</label>
      <div class="col-sm-6">
        <textarea rows="3" style="height: 70px;" class="form-control"  placeholder="" id="description" name="description">${organization.description!}</textarea>
      </div>
      <div class="col-sm-4"><div class="Validform_checktip"></div></div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">电话：</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" value="${organization.phone!}" placeholder="" id="phone" name="phone">
      </div>
      <div class="col-sm-4"><div class="Validform_checktip"></div></div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">传真：</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" value="${organization.fax!}" placeholder="" id="fax" name="fax">
      </div>
      <div class="col-sm-4"><div class="Validform_checktip"></div></div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">添加时间：</label>
      <div class="col-sm-6">
        <input type="text" class="form-control " disabled readonly value="${organization.createTime!}" placeholder="" id="createTime" name="createTime">
      </div>
      <div class="col-sm-4"><div class="Validform_checktip"></div></div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">更新时间：</label>
      <div class="col-sm-6">
        <input type="text" class="form-control " disabled readonly value="${organization.updateTime!}" placeholder="" id="updateTime" name="updateTime">
      </div>
      <div class="col-sm-4"><div class="Validform_checktip"></div></div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">状态：</label>
      <div class="col-sm-6">
        <input type="text" class="form-control " disabled readonly value="${organization.available!}" placeholder="" id="available" name="available">
      </div>
      <div class="col-sm-4"><div class="Validform_checktip"></div></div>
    </div>
    <div class="form-group" >
      <div class="col-sm-offset-2  col-sm-6" style="text-align:center;">
        <button type="button" onclick="save()" class="btn btn-primary btn-outline  radius" id="saveBtn" ><i class="icon-ok"></i> 确定</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript">
  var validform;
  var sys_ctx="";
  function save(){

    var b=validform.check(false);
    if(!b){
      return;
    }
    var params=$("#form_show").serialize();
    $.ajax({
      type:"post",
      url:'/org/organization/json/save?'+params,
      data:null,
      success:function(json,textStatus){
        ajaxReturnMsg(json);
      }
    });
  }

  $(function(){
    validform=$("#form_show").Validform({
      tiptype:2,
      postonce:true,//至提交一次
      ajaxPost:false,//ajax方式提交
      showAllError:true //默认 即逐条验证,true验证全部
    });
  })
</script>
</body>
</html>
