<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>客户端信息管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    <link href="/resources/lib/bootstrap/table/bootstrap-table.min.css" rel="stylesheet">
    
</head>

<body class="body-bg main-bg">

<!--面包屑导航条-->
<nav class="breadcrumb"><i class="fa fa-home"></i> 首页 <span class="c-gray en">&gt;</span> cms <span class="c-gray en">&gt;</span>客户端信息管理</nav>
<div class="wrapper animated fadeInRight">

    <!--工具条-->
    <div class="cl">
        <form id="form_query" style="margin: 8px 0px" class="form-inline">
            <div class="btn-group">
                  <a onclick="list_del('tableList')" class="btn btn-outline  btn-danger" ><i class="fa fa-minus"></i>&nbsp;删除</a>
                  <a onclick="query('form_query');" class="btn btn-outline  btn-success" ><i class="fa fa-search"></i>&nbsp;搜索</a>
            </div>
        </form>
    </div>
    <!--数据列表-->
    <div class="table-responsive"><table id="tableList" class="table table-striped"></table> </div>

    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>

<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table.min.js"></script>
<script src="/resources/lib/bootstrap/table/bootstrap-table-mobile.min.js"></script>
<script src="/resources/lib/bootstrap/table/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>


<script type="text/javascript" >
    var sys_ctx="";
    var queryStr="?";
    //查询功能的标签说明
    var table_list_query_form = {
    };
    function refreshData() {
        $('#tableList').bootstrapTable('refresh');
    }
    $(function(){
        sys_table_list();
    });
    function sys_table_list(){
        $('#tableList').bootstrapTable('destroy');
        var columns=[{checkbox:true},
            {field: 'yhid',align:"center",sortable:true,order:"asc",visible:false,title: '用户id'},
            {field: 'screen',align:"center",sortable:true,order:"asc",visible:true,title: '屏幕分辨率',width:"120px"},
            {field: 'browser',align:"center",sortable:true,order:"asc",visible:true,title: '浏览器',width:"80px"},
            {field: 'colorDepth',align:"center",sortable:true,order:"asc",visible:false,title: '位深度'},
            {field: 'lang',align:"center",sortable:true,order:"asc",visible:true,title: '语言',width:"80px"},
            {field: 'charset',align:"center",sortable:true,order:"asc",visible:true,title: '字符集',width:"80px"},
            {field: 'javaAppletEnabled',align:"center",sortable:true,order:"asc",visible:false,title: ''},
            {field: 'cookieEnabled',align:"center",sortable:true,order:"asc",visible:false,title: '是否启用cookie'},
            {field: 'referer',align:"center",sortable:true,order:"asc",visible:false,title: 'referer'},
            {field: 'os',align:"center",sortable:true,order:"asc",visible:true,title: '操作系统',width:"100px"},
            {field: 'deviceType',align:"center",sortable:true,order:"asc",visible:true,title: '设备类型',width:"100px"},
            {field: 'title',align:"center",sortable:true,order:"asc",visible:true,title: 'title'},
            {field: 'domain',align:"center",sortable:true,order:"asc",visible:false,title: 'domain'},
            {field: 'lastModified',align:"center",sortable:true,order:"asc",visible:false,title: ''},
            {field: 'currentUrl',align:"center",sortable:true,order:"asc",visible:false,title: ''},
            {field: 'uri',align:"center",sortable:true,order:"asc",visible:true,title: 'URI'},
            {field: 'host',align:"center",sortable:true,order:"asc",visible:false,title: ''},
            {field: 'timeZone',align:"center",sortable:true,order:"asc",visible:false,title: '时区'},
            {field: 'ip',align:"center",sortable:true,order:"asc",visible:true,title: 'ip地址',width:"120px"},
            {field: 'createTime',align:"center",sortable:true,order:"asc",visible:true,title: '访问时间',width:"180px"},
        ];
        table_list_Params.columns=columns;
        table_list_Params.pageSize=15;
        table_list_Params.onClickRow=function(){};
        table_list_Params.url='/base/clientinfo/json/find'+queryStr;
        $('#tableList').bootstrapTable(table_list_Params);
    }

    var onClickRow=function(row,tr){
        $.layer.open_page("编辑客户端信息",sys_ctx+"/base/clientinfo/edit?id="+row.id,{
            end:function(){
                $('#tableList').bootstrapTable('refresh');
            }
        });
    }

    function list_del(tableid){
        var selecRow = $("#"+tableid).bootstrapTable('getSelections');
        if(selecRow.length > 0){
            Fast.confirm("确定这样做吗？", function(){
                var ids = new Array();
                for(var i=0;i<selecRow.length;i++){
                    ids[ids.length] = selecRow[i]["id"]
                }
                $.ajax({
                    type:"post",
                    url:'/base/clientinfo/json/deletes/'+ids,
                    data:null,
                    success:function(data,textStatus){
                        ajaxReturnMsg(data,function(){
                         $('#tableList').bootstrapTable('refresh');
                     });
                    }
                });
            });
        }else{
            Fast.msg_warning("请选择要删除的数据");
        }
    }
    //根据表单查询
    var query = function(formid){
        queryStr="?";
        var qArr = $("#"+formid)[0];//查询表单区域序列化重写
        var queryStrTem="";
        for(var i=0;i<qArr.length;i++){
            var id = qArr[i].id;
            if(typeof table_list_query_form[id] != 'undefined')
            {
                table_list_query_form[id] = $("#"+id).val();
                queryStrTem+="&"+id+"="+$("#"+id).val();
            }
        }
        queryStrTem=queryStrTem.substring(1);
        queryStr+=queryStrTem;
        sys_table_list();
    }
    function search_form_reset(tableid){
        $('#'+tableid)[0].reset()
    }
</script>
</body></html>
