<!DOCTYPE html>
<html>
<head>
  <title>客户端信息</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico">
    <link href="/resources/css/admui.css" rel="stylesheet" />
    

</head>
<body  class="body-bg-edit">
<div class="content-wrapper  animated fadeInRight">
  <div class="container-fluid">
    <form action="" id="form_show" method="post" class="form-horizontal" role="form">
    <input type="hidden" value="${clientinfo.id}" id="id" name="id"/>
		<h2 class="text-center">编辑客户端信息</h2>
        <div class="form-group">
              <label class="col-sm-2 control-label">用户id：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="yhid" name="yhid" placeholder="用户id" value="${clientinfo.yhid}" class="form-control" datatype="*" nullmsg="请输入用户id" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">屏幕分辨率：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="screen" name="screen" placeholder="屏幕分辨率" value="${clientinfo.screen}" class="form-control" datatype="*" nullmsg="请输入屏幕分辨率" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">浏览器：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="browser" name="browser" placeholder="浏览器" value="${clientinfo.browser}" class="form-control" datatype="*" nullmsg="请输入浏览器" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">位深度：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="colorDepth" name="colorDepth" placeholder="位深度" value="${clientinfo.colorDepth}" class="form-control" datatype="*" nullmsg="请输入位深度" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">语言：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="lang" name="lang" placeholder="语言" value="${clientinfo.lang}" class="form-control" datatype="*" nullmsg="请输入语言" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">字符集：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="charset" name="charset" placeholder="字符集" value="${clientinfo.charset}" class="form-control" datatype="*" nullmsg="请输入字符集" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="javaAppletEnabled" name="javaAppletEnabled" placeholder="" value="${clientinfo.javaAppletEnabled}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">是否启用cookie：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="cookieEnabled" name="cookieEnabled" placeholder="是否启用cookie" value="${clientinfo.cookieEnabled}" class="form-control" datatype="*" nullmsg="请输入是否启用cookie" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">referer：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="referer" name="referer" placeholder="referer" value="${clientinfo.referer}" class="form-control" datatype="*" nullmsg="请输入referer" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">操作系统：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="os" name="os" placeholder="操作系统" value="${clientinfo.os}" class="form-control" datatype="*" nullmsg="请输入操作系统" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">title：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="title" name="title" placeholder="title" value="${clientinfo.title}" class="form-control" datatype="*" nullmsg="请输入title" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">domain：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="domain" name="domain" placeholder="domain" value="${clientinfo.domain}" class="form-control" datatype="*" nullmsg="请输入domain" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="lastModified" name="lastModified" placeholder="" value="${clientinfo.lastModified}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="currentUrl" name="currentUrl" placeholder="" value="${clientinfo.currentUrl}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="uri" name="uri" placeholder="" value="${clientinfo.uri}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="host" name="host" placeholder="" value="${clientinfo.host}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="deviceType" name="deviceType" placeholder="" value="${clientinfo.deviceType}" class="form-control" datatype="*" nullmsg="请输入" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">时区：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="timeZone" name="timeZone" placeholder="时区" value="${clientinfo.timeZone}" class="form-control" datatype="*" nullmsg="请输入时区" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">ip地址：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="ip" name="ip" placeholder="ip地址" value="${clientinfo.ip}" class="form-control" datatype="*" nullmsg="请输入ip地址" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>
        <div class="form-group">
              <label class="col-sm-2 control-label">用户所在地：</label>
              <div class="col-sm-6 formControls">
            <input type="text" id="location" name="location" placeholder="用户所在地" value="${clientinfo.location}" class="form-control" datatype="*" nullmsg="请输入用户所在地" />
              </div>
              <div class="col-sm-4">
              		<div class="Validform_checktip"></div>
              </div>
         </div>

		<#--<div class="form-group">
            <div class=" col-sm-10 col-sm-offset-2">
              <button type="button" onclick="save()" class="btn btn-primary " ><i class="icon-ok"></i>保存</button>
            </div>
        </div>-->
    </form></div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/resources/lib/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common/myutil.js"></script>
<script type="text/javascript" src="/resources/lib/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="/resources/lib/Validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
    var sys_ctx="";
	var validform;
	function save(){
	    var b=validform.check(false);
		if(!b)
		{
			return;
		}
		var params=$("#form_show").serialize();
		$.ajax({
			type:"post",
			url:'/base/clientinfo/json/save?'+params,
			data:null,
			success:function(json,textStatus){
				ajaxReturnMsg(json);
				setTimeout(function(){
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				},1000);
			}
		});
	}
	$(function(){
	 validform=$("#form_show").Validform({
     		  btnReset:"#reset",
             tiptype:2,
             postonce:true,//至提交一次
             ajaxPost:false,//ajax方式提交
             showAllError:true //默认 即逐条验证,true验证全部
     });
	})
</script>

</body>
</html>
