
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>重置密码</title>
    <style>
        .center{margin:0 auto}div,input,textarea,ul{resize:none;outline:0;font-size:inherit}body,html{background:#f0f0f0;color:#000;font:400 13px arial,Microsoft Yahei!important;margin:0;overflow:hidden;height:100%;-webkit-font-smoothing:antialiased}div,input,textarea,ul{resize:none;outline:0;font-size:inherit}div,input,textarea,ul{resize:none;outline:0;font-size:inherit}ol,ul{margin:0;padding:0;list-style:none}.txt{border:1px solid #d9d9d9;border-top:1px solid silver;padding:9px 5px;width:180px;background-color:#fff;border-radius:2px}.button{margin-top:20px;display:inline-block;width:76px;text-align:center;padding:6px 12px;color:#fff;background:#4386f5;border-radius:3px;cursor:pointer}a{font-size:inherit;text-decoration:none;color:#666;cursor:pointer}
   </style>
</head>

<body class="gray-bg" style="padding:0;background: rgb(241, 244, 245);">

<div class="page animation-fade page-email" style="margin:10px auto;width: 50%;">
    <div class="page-content" >

        <div class="center main" style="min-height: 400px;">
            <div class="content-r" style=" margin: 50px auto;padding: 0; width: 500px;border:none;">
                <h1 style="font-size:20px;color:#555;font-weight:normal;text-align:center;">
                    重置您的密码
                </h1>
                <div class="resetpwd-module" style=" background-color: #fff;border-radius: 5px;-moz-border-radius:5px;-webkit-border-radius:5px;margin-top: 8px;padding:20px 28px 30px 29px">
                    <div style="padding:20px 0px; color:#111;">
                        <p style="margin: 0px 0px 10px;">为了验证您的新密码，请认真填写以下每一项：</p>
                        <p style="margin:0px;">密码区分大小写，为6-24个字符，最好由大小写字母、数字和特殊字符构成。</p>
                    </div>
                    <div id="error_tip" style="width: 385px; color: red; text-align: center; display: none;">密码不能为空。</div>
                    <form id="passwordform" method="post">
                        <ul style="line-height:40px;">
                            <li>
                                <input type="hidden" id="token" name="token" value="${token}">
<span class="label" style="display:inline-block; width:120px; text-align: right;">
新密码：
</span>
                                <input id="new_pass"  name="new_pass" type="password" class="txt" style="width: 240px;">
                            </li>
                            <li>
<span class="label" style="display:inline-block; width:120px; text-align: right;">
确认密码：
</span>
                                <input id="confirm_pass" name="confirm_pass" type="password" class="txt" style="width: 240px;">
                            </li>
                            <li style="text-align: right;width:376px;">
                                <span class="label"></span>
                                <a href="javascript:void(0);" style="padding:5px 16px;line-height:25px;" class="button" onclick="doSubmit()">
                                    确定
                                </a>&nbsp;&nbsp;
                                <a href="/" class="button" style="background:#f0f0f0;color:#333;padding:5px 16px;line-height:25px;">
                                    取消
                                </a>
                            </li>
                            <li id="success-tip" style="display:none;"><div style="text-align:center;color:#468847;font-size:13px;">您的密码修改成功，去 <a href="/login">登录</a></div></li>
                        </ul>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/resources/js/jquery.min.js"></script>
<script type="text/javascript">
    var sys_ctx="";
    $(function(){
        $("input[name=password]").focus();
    });
    function doSubmit(){
        var token=$("#token").val();
        var new_pass=$("#new_pass").val();
        var confirm_pass=$("#confirm_pass").val();
        if(new_pass==''){
            tip("密码不能为空!");
            $("#new_pass").focus();
            return;
        }
        if(confirm_pass==''){
            $("#confirm_pass").focus();
            tip("确认密码不能为空!");
            return;
        }
        if(new_pass!=confirm_pass){
            tip("两次密码输入不一致!");
            return;
        }
        $.ajax({
            url: "/account/pwdreset/change_password",
            type: "post",
            data: {
                new_pass: new_pass,
                confirm_pass:confirm_pass,
                token:token
            },
            success: function(json) {
                json=JSON.parse(json);
                //alert(JSON.stringify(json));
                if(json.type=="success"){
                    $("#success-tip").show();
                    setTimeout(function(){
                        window.location.href="/login";
                    }, 5000);
                }else{
                    tip(json.content);
                }

            }
        })
    }
    var tipTimeout;
    function tip(msg){
        $("#error_tip").text(msg).fadeIn();
        if(tipTimeout){
            window.clearTimeout(tipTimeout);
        }
        tipTimeout = setTimeout(function(){
            $("#error_tip").fadeOut();
        },5000);
    }
</script>
</body>
</html>
