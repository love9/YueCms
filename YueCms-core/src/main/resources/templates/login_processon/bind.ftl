<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>绑定帐号</title>
    <meta name="description" content="ProcessOn是一个在线协作绘图平台，为用户提供最强大、易用的作图工具！支持在线创作流程图、思维导图、组织结构图、网络拓扑图、BPMN、UML图、UI界面原型设计、iOS界面原型设计等。同时依托于互联网实现了人与人之间的实时协作和共享。">
    <meta name="keywords" content="免费在线作图工具,在线流程图,在线思维导图,流程图,思维导图,组织结构图,网络拓扑图,UML作图,UI界面原型设计,iOS原型设计,BPMN,多人协作绘图">
   <style>
       body,html{color:#000;margin:0;overflow:hidden;font:normal 13px arial,Microsoft Yahei!important}a{font-size:inherit;text-decoration:none;color:#666;cursor:pointer}.txt{width:90%;padding:4px 20px;border-radius:22px;border:solid 1px #ccc;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;height:3.6em;outline:0}.txt:focus{border-color:#4386f5}.button{background:#4386f5;color:#fff;padding:6px 0;height:25px;line-height:25px;border-radius:5px;width:90%;border-radius:22px;display:inline-block;cursor:pointer}.page-con{text-align:center;width:400px;margin:0 auto;height:362px;position:absolute;top:50%;margin-top:-181px;left:50%;margin-left:-200px}.page-con .sep{border-top:1px solid #ddd;height:20px;text-align:center}.page-con label{color:#666;display:inline-block;padding:0 10px;background:#fff}.page-con .signup{font-size:12px;margin-top:19px;color:#666;text-align:left;padding-left:26px}.page-con .signup a{color:#333}#bg-canvas{position:absolute;z-index:-1}#logo-text{display:inline-block;margin-left:8px;font-size:35px;vertical-align:7px;font-family:arial;height:60px;text-shadow:1px 1px 2px #bbb inset}#logo-text .logo-dot{width:68px;height:6px;display:inline-block;position:absolute;left:50%;top:50%;margin-left:-34px;margin-top:-3px;display:none}#logo-text .logo-dot:after{width:6px;background:#4386f5;height:6px;display:inline-block;position:absolute;right:-4px;border-radius:50%;content:""}#logo-text label{display:inline-block;background:#4386f5;border-radius:50%;color:#fff;padding:11px 8px;margin-left:6px;position:relative;box-shadow:1px 1px 6px #ccc}.login-input{position:relative}.login-input a{position:absolute;right:33px;top:13px;width:auto;height:20px;line-height:20px;text-align:right;color:#888;z-index:1}.weixin-con{display:none;padding:15px;background:#fff;position:absolute;z-index:9;width:362px;height:410px;left:50%;top:50%;margin-left:-181px;margin-top:-205px;text-align:center;border-raidus:5px;box-shadow:1px 1px 4px #ccc}.error-tip{height:30px;line-height:30px;position:absolute;right:32px;color:red;bottom:6px;font-size:12px;z-index:2;background:#fff}.success-tip{position:absolute;width:194px;top:155px;display:none;right:96px;padding:5px 8px;border:1px solid green;border-radius:3px;box-shadow:1px 1px 5px #d6e9c6;background-color:#dff0d8;color:#468847;text-shadow:0 1px 0 rgba(255,255,255,.5)}.icons{background:url(/assets/images/login/login.png) no-repeat;display:inline-block;width:42px;height:42px}.icons.weixin{background-position:0 0;background-size:42px}.icons.qq{background-position:0 -85px;background-size:43px}.icons.weibo{background-position:0 -131px;background-size:44px}.icons.google{background-position:0 -45px;background-size:44px}.icons.somemore{border-radius:100%;border:1px solid #ddd;background-position:-1px -165px;background-size:42px;width:38px;height:38px}.icons.somemore:hover{-webkit-box-shadow:inset 0 0 1px rgba(0,0,0,.2);-moz-box-shadow:inset 0 0 1px rgba(0,0,0,.2);box-shadow:inset 0 0 1px rgba(0,0,0,.2);cursor:pointer}.morelogo-con{background-color:#fff;padding:10px;position:absolute;left:249px;opacity:0;top:30px;z-index:3;border-radius:5px;box-shadow:0 1px 2px 0 rgba(0,0,0,.2);-webkit-transition:all .3s ease-in-out;-moz-transition:all .3s ease-in-out;-ms-transition:all .3s ease-in-out;-o-transition:all .3s ease-in-out;transition:all .3s ease-in-out}.morelogo-con.popover{opacity:1;top:45px}.morelogo-con>span{display:block;font-size:13px;color:#3d474d}.morelogo-con .mingdao{display:inline-block;vertical-align:middle;background-position:0 -257px;background-size:44px;margin-right:11px}.rotate{animation:.8s infinite rotate;-webkit-animation:.8s infinite rotate;-o-animation:.8s infinite rotate;-moz-animation:.8s infinite rotate;-ms-animation:.8s infinite rotate}@-webkit-keyframes rotate{from{-webkit-transform:rotate(0)}to{-webkit-transform:rotate(360deg)}}@-moz-keyframes rotate{from{-moz-transform:rotate(0)}to{-moz-transform:rotate(360deg)}}@-ms-keyframes rotate{from{-ms-transform:rotate(0)}to{-ms-transform:rotate(360deg)}}@-o-keyframes rotate{from{-o-transform:rotate(0)}to{-o-transform:rotate(360deg)}}@keyframes rotate{from{transform:rotate(0)}to{transform:rotate(360deg)}}.bind{width:600px;margin-left:-300px}.bind .button,.bind .txt{width:55%}.bind #logo-text{vertical-align:-7px}
       .bind .icons{background:url(/resources/images/login_processon.png) no-repeat;display:inline-block;width:74px;height:74px;vertical-align:top}
       .bind .icons.weixin{background-position:0 0;background-size:71px;width:70px;height:68px}.bind .icons.qq{background-position:0 -141px;background-size:71px;width:70px;height:68px}.bind .icons.weibo{background-position:0 -211px;background-size:71px;width:70px;height:68px}.bind .icons.google{background-position:0 -73px;background-size:71px;width:70px;height:68px}.bind_item{position:relative}.bind-arrow{margin-left:30px;display:inline-block;position:relative;color:#666;margin-right:30px}.bind-tip{margin-top:40px}.des{margin-top:10px;font-size:14px}@media screen and (max-width:760px){.page-con{width:100%;left:0;margin-left:0}}
       .baidu{display: inline-block;width:70px;height:70px;background: url(/resources/images/logo/logo_bd.png) no-repeat ;background-size: 100%;}
   </style>

    <script src="https://ssl.captcha.qq.com/TCaptcha.js"></script>
    <style type="text/css">@keyframes animate_dots{0%{opacity:1}to{opacity:0}}@-webkit-keyframes animate_dots{0%{opacity:1}to{opacity:0}}.dot0,.dot1{animation:animate_dots .9s infinite;-moz-animation:animate_dots .9s infinite;-webkit-animation:animate_dots .9s infinite;-o-animation:animate_dots .9s infinite}.dot1{animation-delay:.2s;-webkit-animation-delay:.2s}.dot2{animation:animate_dots .9s infinite;-moz-animation:animate_dots .9s infinite;-webkit-animation:animate_dots .9s infinite;-o-animation:animate_dots .9s infinite;animation-delay:.4s;-webkit-animation-delay:.4s}.dots_item{display:inline-block;margin-right:5px;width:10px;height:10px;border-radius:50%;background:#4886ff}.verify-icon{position:absolute;width:100%;margin-top:70px;text-align:center}.t-mask{width:100%;height:100%;position:fixed;_position:absolute;left:0;top:0;background:#000;opacity:.5;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=50);z-index:2000000000}</style>

</head>
<body>

<canvas id="bg-canvas" width="1879" height="598"></canvas>
<div class="page-con bind">
    <div>
        <!--<span id="logo-text"><label>On<span class="logo-dot rotate"></span></label></span>-->
        <img width="70" height="70" style="display: inline-block;top: -19px;" src="/resources/images/logo/logo_72.png">
        <div class="bind-arrow">
            绑定
        </div>
        #if($type == 'baidu')
        <div class="baidu"></div>
        #else
        <div class="icons ${type}"></div>
        #end
    </div>
    <div class="bind-tip">
        <div class="des" style="color:#666;">第一次使用<b>${type}</b>登录，您需要填写帐号和密码，以后您也可以使用此帐号和密码登录</div>
        <div class="des" style="font-size:15px;">如果您已有账号，可以直接进行帐号绑定<b>${type}</b></div>
    </div>
    <div>
        <form action="/sns/bind/save" method="post" id="signin_bind_form">
            <div class="bind_item" style="margin-top:25px;">
                <input id="account" class="txt" value="" name="account" type="text" placeholder="帐号">
                <div class="error-tip"></div>
            </div>
            <div class="bind_item" style="margin-top:20px;">
                <input id="password" class="txt" name="password" type="password" placeholder="密码">
                <input name="tencentCaptcha_ticket" id="tencentCaptcha_ticket" type="hidden">
                <input name="tencentCaptcha_Randstr" id="tencentCaptcha_Randstr" type="hidden">
                <input name="type" id="type" value="${type}" type="hidden">
                <div class="error-tip"></div>
            </div>
            <div id="tencent_btn" style="margin-top:20px;">
                <!--这是腾讯的图形验证码服务-->
                <span id="TencentCaptcha" data-appid="2088990589" data-cbfn="callback_submit" class="button">点击验证</span>
                <div class="error-tip">${msg!}</div>
            </div>
            <div id="bind_submit_div" style="margin-top:20px;display:none;">
                <span id="bind_submit" onclick="login.bind();" class="button">提交</span>
                <div class="error-tip">${msg!}</div>
            </div>

        </form>
    </div>
</div>

<script src="/resources/js/login_processon/jquery.js" charset="UTF-8" type="text/javascript"></script>
<script src="/resources/js/login_processon/login.js" charset="UTF-8" type="text/javascript"></script>
<!--<script src="/assets/js/util-a3323c4a.js" charset="UTF-8" type="text/javascript"></script>-->


<script>

    window.callback_submit = function(res){
        console.log(res);
        if(res.ret === 0){
            var tk = res.ticket;
            $("#tencentCaptcha_ticket").val(tk);
            $("#tencentCaptcha_Randstr").val(res.randstr);
            $("#tencent_btn").hide();
            $("#bind_submit_div").show();
        }
    }
    /**
     * 判断非空
     * @param val
     * @returns {Boolean}
     */
    function isEmpty(val) {
        val = $.trim(val);
        if (val == null)
            return true;
        if (val == undefined || val == 'undefined')
            return true;
        if (val == "")
            return true;
        if (val.length == 0)
            return true;
        if (!/[^(^\s*)|(\s*$)]/.test(val))
            return true;
        return false;
    }
    function isNotEmpty(val) {
        return !isEmpty(val);
    }
</script>
</body></html>