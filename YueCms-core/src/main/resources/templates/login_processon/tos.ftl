<!doctype html>
<html>
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="/resources/js/login_processon/common.css">
  <style type="text/css">
   html,body{overflow:auto;height:auto;}.main{margin-top:15px;background-color:#ffffff;max-width:960px;-moz-box-shadow:0px 1px 4px #DCDCDC;-ms-box-shadow:0px 1px 4px #DCDCDC;-o-box-shadow:0px 1px 4px #DCDCDC;box-shadow:0px 1px 4px #DCDCDC;min-height:600px;}.p-head{clear:both;position:relative;padding:15px 10px 0px;}.section{color:#333;font-size:16px;}.content{line-height:23px;text-indent:2em;font-size:13px;}.ita{font-style:italic;}.items{padding-left:45px;line-height:20px;}.items li{margin:5px 0px;}footer{min-height:260px;padding:10px 10px 20px 10px;color:black;}footer .wx-con{right:0px;top:94px;width:130px;position:absolute;text-align:center;}footer .wx-con img{width:100px;display:inline-block;margin-bottom:7px;}footer .wx-con span{display:block;}footer .footer-con{max-width:700px;margin:0 auto;position:relative;}footer .footer-title{font-size:20px;margin-bottom:20px;text-align:center;color:#555;}footer .footer-item{line-height:30px;color:#777;}footer .footer-item a{font-size:14px;color:#444;}footer .footer-nav{margin-bottom:14px;color:#666;font-size:13px;text-align:center;}footer .footer-nav a{display:inline-block;margin-right:10px;}footer .footer-nav a{font-size:inherit;text-decoration:none;color:#333;cursor:pointer;}h3{margin:0px;padding:0px 6px;font-size:20px;text-align:center;font-weight:normal;}#logo-text{display:inline-block;margin-left:8px;font-size:20px;vertical-align:7px;font-family:arial;height:60px;text-shadow:1px 1px 2px #bbb inset}#logo-text .logo-dot{width:68px;height:6px;display:inline-block;position:absolute;left:50%;top:50%;margin-left:-34px;margin-top:-3px;display:none}#logo-text .logo-dot:after{width:6px;background:#4386f5;height:6px;display:inline-block;position:absolute;right:-4px;border-radius:50%;content:""}#logo-text label{display:inline-block;background:#4386f5;border-radius:50%;color:#fff;padding:11px 8px;margin-left:6px;position:relative;box-shadow:1px 1px 6px #ccc}
  </style>

 </head>
 <body>

 <div class="center main">
  <div class="p-head">
   <h3 class="head-title"><span id="logo-text">Process<label>On<span class="logo-dot rotate"></span></label></span>&nbsp;&nbsp;服务条款</h3>
  </div>
  <div style="padding:10px 20px">
   <p class="content">
    <span>欢迎来到ProcessOn！</span>
   </p>
   <p class="content">ProcessOn是由北京大麦地信息技术有限公司（下称“大麦地”）提供的互联网软件服务。本服务条款（下称“服务条款”）是您与大麦地之间关于您（“您”或“用户”）访问和使用ProcessOn以及大麦地提供的其他服务（下称“服务”）的主要协议。您注册、登录ProcessOn和/或使用大麦地提供的服务，即表示您同意接受服务条款。因此，敬请仔细阅读。</p>
   <p class="content">大麦地有权不时地对服务条款做任何修改和补充，并在ProcessOn网站上公布。通常情况下（例如当需要进行修改或补充以满足适用法律要求时），对服务条款的修改和补充将在公布时立即生效。您继续访问和使用ProcessOn即视为您接受修订后的服务条款。否则，您有权通过停止访问ProcessOn且拒绝使用服务、删除您在ProcessOn上的信息和帐户等方式来终止服务条款。</p>
   <p class="content">如果您代表某个机构而非您个人注册、登录和使用ProcessOn和/或我们其他的服务，则您将被认为获得充分授权代表该机构同意本服务条款以及服务条款不时的修改和补充。</p>
   <h2 class="section">1.服务内容</h2>
   <p class="content">1.1 ProcessOn的具体服务由大麦地根据实际情况提供，例如个人信息、个人分享信息以及评论、在线协作等。为用户提供了专业易用的作图工具和海量图库资源！支持在线创作流程图、思维导图、原型图、网络拓扑图和UML图等。同时依托于互联网实现了人与人之间的实时协作和共享。</p>
   <p class="content">1.2 根据实际需要和大麦地不时提供的其他服务内容，大麦地可能与您另行签订其他协议。同时，即使未另行签订其他协议，您使用ProcessOn的具体服务也将被视为您同意大麦地关于该等具体服务的任何要求。如果其他协议和本条款之间存在冲突，应以其他协议为准，但以该冲突和与该协议特定事宜相关为限。</p>
   <p class="content">1.3 大麦地保留随时变更、中止或终止部分免费服务的权利，并保留根据实际情况随时调整ProcessOn提供的服务种类、形式。大麦地不承担因ProcessOn提供的任何免费服务的调整给您造成的损失。尽管有本条约定，大麦地有权在未来恰当的时机对该等免费服务内容收取相应的服务费用。大麦地保留随时终止向您提供的收费服务的权利，并保留根据实际情况随时调整ProcessOn提供的收费服务种类和形式。如果大麦地终止提供某项收费服务，大麦地的义务仅在于向您返还您尚未使用的服务期对应的部分费用。但无论如何，大麦地将尽合理的努力给您预留合理的时间以便您为该等服务变更、中止或终止做出应对。</p>
   <h2 class="section">2.注册</h2>
   <p class="content">2.1 为了能访问ProcessOn和使用服务，您同意以下事项：依ProcessOn网站注册提示填写准确的、真实的注册邮箱、密码和名称，并确保今后更新的登录邮箱、名称、头像等资料的有效性和合法性。若您提供任何违法、虚假、不道德或大麦地认为不适合在ProcessOn上展示的资料；或者大麦地有理由怀疑您的资料属于病毒程序或恶意操作；或者您违反本服务条款的规定；或者未经大麦地同意，将ProcessOn用于商业目的，大麦地有权暂停或终止您的帐号，并拒绝您于现在和未来使用服务之全部或任何部分。</p>
   <p class="content">2.2 尽管有前述规定，大麦地无义务对任何用户的任何登记资料承担任何责任，包括但不限于鉴别、核实任何登记资料的真实性、准确性、完整性、适用性及/或是否为最新资料的责任。同时，大麦地建议您妥善保管您的注册邮箱、密码和名称，准确输入该等信息将作为您访问登录ProcessOn并享有服务的关键环节。如果您发现有人未经授权使用您的帐户信息或怀疑任何人未经授权可能能够访问您的私有内容，您应立即更改密码，并向我们反馈。如果您提供的信息不正确或您未能确保您的帐户信息的安全而造成任何损失或损害，大麦地不承担任何责任。</p>
   <h2 class="section">3.内容使用权</h2>
   <p class="content">3.1 用户在ProcessOn上发布的内容（包含但不限于ProcessOn目前各产品功能里的内容）仅表明其个人的立场和观点，并不代表大麦地的立场或观点。作为内容的发表者，需自行对所发布的内容负责，因所发布内容引发的一切纠纷，由该内容的发布者承担全部法律责任，大麦地不承担任何法律责任。用户在该平台发布侵犯他人知识产权或其他合法权益的内容，ProcessOn有权利但无义务删除，对于涉嫌违法犯罪的，ProcessOn有权移交司法机关处理。用户在ProcessOn发布的内容被举报侵犯他人知识产权及其他合法权益的，ProcessOn一经查实，有权将侵权内容删除，但由此产生的纠纷，ProcessOn不承担任何责任。</p>
   <p class="content">3.2 用户不得使用ProcessOn服务发送或传播敏感信息和违反国家法律制度的信息，包括但不限于下列信息：</p>
   <ul class="items">
    <li>(1) 反对宪法所确定的基本原则的；</li>
    <li>(2) 危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；</li>
    <li>(3) 损害国家荣誉和利益的；</li>
    <li>(4) 煽动民族仇恨、民族歧视，破坏民族团结的；</li>
    <li>(5) 破坏国家宗教政策，宣扬邪教和封建迷信的；</li>
    <li>(6) 散布谣言，扰乱社会秩序，破坏社会稳定的；</li>
    <li>(7) 散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；</li>
    <li>(8) 侮辱或者诽谤他人，侵害他人合法权益的；</li>
    <li>(9) 含有法律、行政法规禁止的其他内容的。</li>
    <p>用户将文件发布并公开到ProcessOn后，即视为授权ProcessOn使用，进行推广展示。若用户上传的文件涉及到个人隐私及商业秘密应在文件中予以注明，对于未注明的，视为未涉及个人隐私及商业秘密，ProcessOn可以不做任何标记直接使用，若因此发生纠纷，ProcessOn不承担任何法律责任。</p>
   </ul>
   <p class="content">3.3 用户承诺发表言论要：爱国、守法、自律、真实、文明。不传播任何非法的、骚扰性的、中伤他人的、辱骂性的、恐吓性的、伤害性的、庸俗的，淫秽的、危害国家安全的、泄露国家机密的、破坏国家宗教政策和民族团结的以及其它违反法律法规及政策的内容。若用户的行为不符合以上提到的服务条款，ProcessOn将作出独立判断立即暂停或终止用户的服务帐号。用户需对自己在网上的行为承担法律责任，ProcessOn不承担任何法律责任及连带责任。</p>
   <h2 class="section">4.版权</h2>
   <p class="content">4.1 ProcessOn的外观设计、计算机代码与专利等均归大麦地所有。未经大麦地事先书面许可，您不能复制、拷贝、或者使用任何部分的代码和外观设计。</p>
   <h2 class="section">5.隐私政策</h2>
   <p class="content">5.1 使用ProcessOn和大麦地提供的服务，即表示您同意大麦地合法收集和使用有关您及您所使用服务的技术性或诊断性信息。收集到的这些信息将用于改进ProcessOn产品的内容和技术，提升ProcessOn的服务品质。 </p>
   <p class="content">5.2 大麦地不会将您的信息和内容提供或出售给其他的组织或个人，但以下情况除外：</p>
   <ul class="items">
    <li>（1）您的事先同意或授权，或您于ProcessOn上主动与第三方进行分享操作；</li>
    <li>（2）司法机关或有权政府机构或任何法律法规部门规章要求大麦地提供该等信息；</li>
    <li>（3）您违反了本服务条款，且大麦地需要向第三方提供的。</li>
   </ul>
   <h2 class="section">6.数据安全</h2>
   <p class="content">6.1 大麦地将尽合理的努力保护您的信息安全，并为此采取合理的数据传输、存储、转换等预防保护措施。但是，互联网数据传输、存储、转换均可能存在一定未知且不确定的数据安全风险，该等风险将导致包括但不限于数据丢失、泄露、损坏、无法读取或提取等后果。您确认，您已明确知晓并同意接受该等因互联网引发的风险和后果，并已采取恰当的措施（例如数据备份等），以便在该等风险发生时将损失降至最低。</p>
   <p class="content">6.2 因互联网技术本身等非大麦地主观故意或重大过失导致危害您数据安全的，大麦地不承担任何赔偿责任。因大麦地重大过失危害您数据安全的，大麦地的赔偿责任以向您收取的服务费用为上限。</p>
   <h2 class="section">7.免责声明</h2>
   <p class="content">一旦您注册成为用户即表示您与大麦地达成协议，完全接受本服务条款项下的全部条款。对免责声明的解释、修改及更新权均属于大麦地所有。</p>
   <ul class="items">
    <li>（1）由于您将用户密码告知他人或与他人共享注册帐户，由此导致的任何个人信息的泄漏，或其他非因大麦地原因导致的个人信息的泄漏，大麦地不承担任何法律责任；
    </li><li>（2）任何第三方根据大麦地各服务条款及声明中所列明的情况使用您的个人信息，由此所产生的纠纷，大麦地不承担任何法律责任以及连带责任；
   </li><li>（3）任何由于黑客攻击、电脑病毒侵入或政府管制而造成的暂时性网站关闭，大麦地不承担任何法律责任；
   </li><li>（4）我们鼓励用户充分利用ProcessOn自由地发布和共享自己的信息。您可以自由发布文字、图片等内容，但这些内容必须位于公共领域内，或者您拥有这些内容的使用权。同时，用户不应在自己的个人主页或ProcessOn的任何其他地方发布受版权保护的内容；
   </li><li>（5）用户在ProcessOn发布侵犯他人知识产权或其他合法权益的内容，大麦地有权予以删除，并保留移交司法机关处理的权利；
   </li><li>（6）用户对于自己创作并在ProcessOn上发布的合法内容依法享有著作权及其他相关权利；
   </li><li>（7）互联网是一个开放平台，用户将图片等资料上传到互联网上，有可能会被其他组织或个人复制、转载、擅改或做其它非法用途，用户必须充分意识到此类风险的存在。用户明确同意使用大麦地服务所存在的风险将完全由用户自己承担；因用户使用大麦地服务而产生的一切后果也由用户自己承担，大麦地对用户不承担任何责任。
   </li></ul>
   <h2 class="section">8.服务变更/中断/终止</h2>
   <p class="content">8.1 如因系统维护或升级的需要而暂停网络服务、调整服务功能的，大麦地将尽可能事先在网站上进行通告。</p>
   <p class="content">8.2 如发生下列任何一种情形，大麦地有权单方面中断或终止向用户提供服务而无需通知用户：</p>
   <ul class="items">
    <li>（1）用户提供的个人资料不真实；</li>
    <li>（2）用户违反本服务条款中规定的使用规则；</li>
    <li>（3）未经大麦地同意，将ProcessOn用于商业目的。</li>
   </ul>
   <h2 class="section">9.服务条款的完善和修改</h2>
   <p class="content">大麦地有权根据互联网的发展和中华人民共和国有关法律、法规的变化，不时地完善和修改大麦地服务条款。大麦地保留随时修改服务条款的权利，用户在使用大麦地的服务时，有必要对最新的大麦地服务条款进行仔细阅读和重新确认，当发生相关争议时，以最新的服务条款为准。</p>
   <h2 class="section">10.特别约定</h2>
   <p class="content">10.1 本服务条款及其下的服务受中华人民共和国法律管辖，并按之解释。</p>
   <p class="content">10.2 用户使用本服务的行为若有任何违反国家法律法规或侵犯任何第三方合法权益的情形，大麦地有权直接删除该等违反规定的信息，并可以暂停或终止向该用户提供服务。</p>
   <p class="content">10.3 若用户利用本服务从事任何违法或侵权行为，由用户自行承担全部责任，大麦地不承担任何法律责任及连带责任。因此给大麦地或任何第三方造成任何损失的，用户应负责全额赔偿。</p>
   <p class="content">10.4 用户在此特别声明并承诺，用户已充分注意本服务协议内免除或限制大麦地责任的条款，用户完全知晓和理解该等条款的规定并同意接受。</p>
   <h2 class="section">11.联系我们</h2>
   <p class="content">欢迎您对我们的服务条款提出意见。如有任何问题、意见或疑虑，请发邮件至support@processon.com 此电子邮件地址作为本服务条款的组成部分可能会不时进行更新。</p>
   <p class="content">©北京大麦地信息技术有限公司保留一切权利。</p>
  </div>
 </div>
</body>

</html>
