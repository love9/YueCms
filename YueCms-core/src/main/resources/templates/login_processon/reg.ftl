
<!doctype html>
<html>
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <title>Document</title>

  <style>
   body,html {
    color: #000;
    margin: 0;
    overflow: hidden;
    font: 400 13px arial,Microsoft Yahei!important
   }

   a {
    font-size: inherit;
    text-decoration: none;
    color: #666;
    cursor: pointer
   }

   .txt {
    width: 90%;
    padding: 4px 20px;
    border-radius: 22px;
    border: solid 1px #ccc;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    height: 3.6em;
    outline: 0
   }

   .txt:focus {
    border-color: #4386f5
   }

   .button {
    background: #4386f5;
    color: #fff;
    padding: 6px 0;
    height: 25px;
    line-height: 25px;
    border-radius: 5px;
    width: 90%;
    border-radius: 22px;
    display: inline-block;
    cursor: pointer
   }

   .page-con {
    text-align: center;
    width: 400px;
    margin: 0 auto;
    height: 362px;
    position: absolute;
    top: 50%;
    margin-top: -181px;
    left: 50%;
    margin-left: -200px
   }

   .page-con .sep {
    border-top: 1px solid #ddd;
    height: 20px;
    text-align: center
   }

   .page-con label {
    color: #666;
    display: inline-block;
    padding: 0 10px;
    /*background: #fff*/
   }

   .page-con .signup {
    font-size: 12px;
    margin-top: 19px;
    color: #666;
    text-align: left;
    padding-left: 26px
   }

   .page-con .signup a {
    color: #333
   }

   #bg-canvas {
    position: absolute;
    z-index: -1
   }

   #logo-text {
    display: inline-block;
    margin-left: 8px;
    font-size: 35px;
    vertical-align: 7px;
    font-family: arial;
    height: 60px;
    text-shadow: 1px 1px 2px #bbb inset
   }

   #logo-text .logo-dot {
    width: 68px;
    height: 6px;
    display: inline-block;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -34px;
    margin-top: -3px;
    display: none
   }

   #logo-text .logo-dot:after {
    width: 6px;
    background: #4386f5;
    height: 6px;
    display: inline-block;
    position: absolute;
    right: -4px;
    border-radius: 50%;
    content: ""
   }

   #logo-text label {
    display: inline-block;
    background: #4386f5;
    border-radius: 50%;
    color: #fff;
    padding: 11px 8px;
    margin-left: 6px;
    position: relative;
    box-shadow: 1px 1px 6px #ccc
   }

   .login-input {
    position: relative
   }

   .login-input a {
    position: absolute;
    right: 33px;
    top: 13px;
    width: auto;
    height: 20px;
    line-height: 20px;
    text-align: right;
    color: #888;
    z-index: 1
   }

   .weixin-con {
    display: none;
    padding: 15px;
    background: #fff;
    position: absolute;
    z-index: 9;
    width: 362px;
    height: 410px;
    left: 50%;
    top: 50%;
    margin-left: -181px;
    margin-top: -205px;
    text-align: center;
    border-raidus: 5px;
    box-shadow: 1px 1px 4px #ccc
   }

   .error-tip {
    height: 30px;
    line-height: 30px;
    position: absolute;
    right: 32px;
    color: red;
    bottom: 6px;
    font-size: 12px;
    z-index: 2;
    background: #fff
   }

   .success-tip {
    position: absolute;
    width: 194px;
    top: 155px;
    display: none;
    right: 96px;
    padding: 5px 8px;
    border: 1px solid green;
    border-radius: 3px;
    box-shadow: 1px 1px 5px #d6e9c6;
    background-color: #dff0d8;
    color: #468847;
    text-shadow: 0 1px 0 rgba(255,255,255,.5)
   }

   .icons {
    background: url(/resources/images/login_processon.png) no-repeat;
    display: inline-block;
    width: 42px;
    height: 42px
   }

   .icons.weixin {
    background-position: 0 0;
    background-size: 42px
   }

   .icons.qq {
    background-position: 0 -85px;
    background-size: 43px
   }

   .icons.weibo {
    background-position: 0 -131px;
    background-size: 44px
   }

   .icons.google {
    background-position: 0 -45px;
    background-size: 44px
   }

   .icons.somemore {
    border-radius: 100%;
    border: 1px solid #ddd;
    background-position: -1px -165px;
    background-size: 42px;
    width: 38px;
    height: 38px
   }

   .icons.somemore:hover {
    -webkit-box-shadow: inset 0 0 1px rgba(0,0,0,.2);
    -moz-box-shadow: inset 0 0 1px rgba(0,0,0,.2);
    box-shadow: inset 0 0 1px rgba(0,0,0,.2);
    cursor: pointer
   }

   .morelogo-con {
    background-color: #fff;
    padding: 10px;
    position: absolute;
    left: 249px;
    opacity: 0;
    top: 30px;
    z-index: 3;
    border-radius: 5px;
    box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out
   }

   .morelogo-con.popover {
    opacity: 1;
    top: 45px
   }

   .morelogo-con>span {
    display: block;
    font-size: 13px;
    color: #3d474d
   }

   .morelogo-con .mingdao {
    display: inline-block;
    vertical-align: middle;
    background-position: 0 -257px;
    background-size: 44px;
    margin-right: 11px
   }

   .rotate {
    animation: .8s infinite rotate;
    -webkit-animation: .8s infinite rotate;
    -o-animation: .8s infinite rotate;
    -moz-animation: .8s infinite rotate;
    -ms-animation: .8s infinite rotate
   }

   @-webkit-keyframes rotate {
    from {
     -webkit-transform: rotate(0)
    }

    to {
     -webkit-transform: rotate(360deg)
    }
   }

   @-moz-keyframes rotate {
    from {
     -moz-transform: rotate(0)
    }

    to {
     -moz-transform: rotate(360deg)
    }
   }

   @-ms-keyframes rotate {
    from {
     -ms-transform: rotate(0)
    }

    to {
     -ms-transform: rotate(360deg)
    }
   }

   @-o-keyframes rotate {
    from {
     -o-transform: rotate(0)
    }

    to {
     -o-transform: rotate(360deg)
    }
   }

   @keyframes rotate {
    from {
     transform: rotate(0)
    }

    to {
     transform: rotate(360deg)
    }
   }

   .bind {
    width: 600px;
    margin-left: -300px
   }

   .bind .button,.bind .txt {
    width: 55%
   }

   .bind #logo-text {
    vertical-align: -7px
   }

   .bind .icons {
    background: url(/resources/images/login_processon.png) no-repeat;
    display: inline-block;
    width: 74px;
    height: 74px;
    vertical-align: top
   }

   .bind .icons.weixin {
    background-position: 0 0;
    background-size: 70px;
    width: 70px;
    height: 68px
   }

   .bind .icons.qq {
    background-position: 0 -141px;
    background-size: 70px;
    width: 70px;
    height: 68px
   }

   .bind .icons.weibo {
    background-position: 0 -211px;
    background-size: 70px;
    width: 70px;
    height: 68px
   }

   .bind .icons.google {
    background-position: 0 -73px;
    background-size: 70px;
    width: 70px;
    height: 68px
   }

   .bind_item {
    position: relative
   }

   .bind-arrow {
    margin-left: 30px;
    display: inline-block;
    position: relative;
    color: #666;
    margin-right: 30px
   }
   .bind-tip {
    margin-top: 40px
   }
   .des {
    margin-top: 10px;
    font-size: 14px
   }

   @media screen and (max-width:760px) {
    .page-con {
     width: 100%;
     left: 0;
     margin-left: 0
    }
   }

  </style>

 </head>
 <body>
 <canvas id="bg-canvas" width="1280" height="590"></canvas>
 <div class="page-con" style="margin-top:-215px;">
  <div><span id="logo-text">Process<label>On<span class="logo-dot rotate"></span></label></span></div>
  <div>
   <form id="signup_form" method="post" action="/signup/submit" onsubmit="return signup.doSignup()">
    <div style="margin-top:25px;position:relative;">
     <input name="email" id="login_email" value="" class="txt" type="text" placeholder="邮箱地址" data-form-un="1513312579314.4077">
     <div class="error-tip"></div>
    </div>
    <div class="login-input" style="margin-top:20px;position:relative;">
     <input name="pass" id="login_password" class="txt" type="password" placeholder="密码" data-form-pw="1513312579314.4077">
     <div class="error-tip"></div>
    </div>
    <div class="login-input" style="margin-top:20px;position:relative;">
     <input name="fullname" id="login_fullname" class="txt" type="text" value="" placeholder="请输入不超过15位字符的昵称">
     <div class="error-tip"></div>
    </div>
    <div style="margin-top:20px;">
     <span id="signin_btn" onclick="login.signup();" class="button">立即注册</span>
    </div>
   </form>
  </div>
 <div class="signup"><a href="/login">登录</a><a style="float:right;margin-right: 35px;" href="/forget">忘记密码</a></div>
  <div style="margin:20px 24px 0px 24px">
   <div class="sep"><div style="margin-top:-9px;"><label>第三方账号注册</label></div></div>
   <div style="position: relative;">
    <span id="weixin_login" onclick="login.paltform.weixin()" class="icons weixin"></span>&nbsp;&nbsp;
    <span onclick="login.paltform.login('qq')" class="icons qq"></span>&nbsp;&nbsp;
    <span onclick="login.paltform.login('google')" class="icons google"></span>&nbsp;&nbsp;
    <span onclick="login.paltform.login('sina')" class="icons weibo"></span>&nbsp;&nbsp;
    <span class="icons somemore" style="display: none;"></span>
    <div class="morelogo-con">
     <span><span class="icons mingdao"></span>明道</span>
    </div>
   </div>
  </div>
  <div style="font-size:12px;margin-top:16px;"><span>注册表示您已阅读和同意 <a href="tos.html">服务协议</a></span></div>
 </div>
 <div class="weixin-con" id="weixin_dlg"><div style="margin-top:200px;">加载中...</div></div>
 <script async="" src="https://www.google-analytics.com/analytics.js"></script>
 <script src="/resources/js/login_processon/jquery.js" charset="UTF-8" type="text/javascript"></script>
 <script src="/resources/js/login_processon/common.js" charset="UTF-8" type="text/javascript"></script>
 <script src="/resources/js/login_processon/login.js" charset="UTF-8" type="text/javascript"></script>
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-28802488-1', 'auto');
  ga('send', 'pageview');
 </script>

 <div id="qb-sougou-search" style="display: none; opacity: 0;"><p>搜索</p><p class="last-btn">复制</p><iframe src=""></iframe></div></body>
</html>
