$(function(){
    var channelClass;
    var channelSite = $('#channelSite').attr('data-site');
    if(isEmpty(channelSite)){
        channelSite = $('#channelSite').attr('data-site-name');
    }
    switch (channelSite)
    {
        case '一个':
            channelClass = 'one';
            break;
        case '少数派':
            channelClass = 'sspai';
            break;
        case '十五言':
            channelClass = 'yan15';
            break;
        case '站酷':
            channelClass = 'zcool';
            break;
        case '真实故事':
            channelClass = 'zhenshigushi';
            break;
        case '虎嗅':
            channelClass = 'huxiu';
            break;
        case '差评':
            channelClass = 'chaping';
            break;
        case 'NGA':
            channelClass = 'NGA';
            break;
        case '好奇心日报':
            channelClass = 'haoqixin';
            break;
        case '知乎':
            channelClass = 'zhihu';
            break;
        case '果壳':
            channelClass = 'guoke';
            break;
        case '今日头条':
            channelClass = 'toutiao';
            break;
        case '36 氪':
            channelClass = 'kr36';
            break;
        case '大象公会':
            channelClass = 'dxgh';
            break;
        case '界面':
            channelClass = 'jiemian';
            break;
        case '新世相':
            channelClass = 'xinshixiang';
            break;
        case '豆瓣':
            channelClass = 'douban';
            break;
        case '数字尾巴':
            channelClass = 'dgtle';
            break;
        case '网易':
            channelClass = 'wangyi';
            break;
        case '新浪':
            channelClass = 'sina';
            break;
        case '简书':
            channelClass = 'jianshu';
            break;
        case '极客公园':
            channelClass = 'geekpark';
            break;
        case 'PingWest 品玩':
            channelClass = 'pinwan';
            break;
        case '悦cms':
            channelClass = 'yuecms';
            break;
    }
    $('#channelSite').addClass(channelClass);
});

//跳转
$(function() {
    function jumpHref(count,_href) {
        window.setTimeout(function(){
            count--;
            if(count > 0) {
                $('#jumpCount').html(count);
                jumpHref(count,_href);
            } else {
                location.href = _href;
            }
        }, 1000);
    }

    $('.jumpHref').on('click',function(){
        var _href = $(this).attr('data-href');
        var _website = $(this).attr('data-site-name');
        if(isNotEmpty(_website)&&_website!="悦cms"){
            var _hrefHtml = '<div class="jumpHrefGrid"><div class="hrefGroup"><h4>即将离开本站，跳转至'+ _website +'</h4><p><span id="jumpCount">5</span>秒后自动跳转</p><p><a class="btn" href="'+ _href +'">立即前往</a></p></div></div>';
            $('body').append(_hrefHtml);
            jumpHref(5,_href);
        }else{
            var _hrefHtml = '<div class="jumpHrefGrid"><div class="hrefGroup"><h4>即将离开本站，跳转至原文</h4><p><span id="jumpCount">5</span>秒后自动跳转</p><p><a class="btn" href="'+ _href +'">立即前往</a></p></div></div>';
            $('body').append(_hrefHtml);
            jumpHref(5,_href);
            return false;
        }

    });


});
/**
 * 判断非空
 * @param val
 * @returns {Boolean}
 */
function isEmpty(val) {
    val = $.trim(val);
    if (val == null)
        return true;
    if (val == undefined || val == 'undefined')
        return true;
    if (val == "")
        return true;
    if (val.length == 0)
        return true;
    if (!/[^(^\s*)|(\s*$)]/.test(val))
        return true;
    return false;
}
function isNotEmpty(val) {
    return !isEmpty(val);
}