var GlobalConfig={
    msg_type:"toastr",//消息提示样式，可选的有toastr和layer
    msg_type_toastr:"toastr",
    msg_type_layer:"layer",
}

/**
 * table list 2015年11月5日 20:42:59
 */
/* 检测当前页面是否我的博客,跳回登录页面 */
function toLogin(){
    var url=  window.location.href;
    var host=window.location.host;
    var protocol=window.location.protocol;
    var uri=url.replace(protocol+"//"+host,"");
    if(uri.indexOf("/myblog")>=0){
        top.window.location.href='/myblog/login?redirectUrl='+uri;
    }else{
        top.window.location="/admin/login";
    }
}

function commentLogin(){
    var url=  window.location.href;
    var host=window.location.host;
    var protocol=window.location.protocol;
    var uri=url.replace(protocol+"//"+host,"");
    var href='/myblog/login?redirectUrl='+uri+"%23comment_area";
    //alert(href);
    window.location.href=href;
}
/**
 * 检查ajax请求异常情况
 */
function checkAjaxError(data){
    try{
        var json=typeof data=='string'?JSON.parse(data):data;
        //先检查异常错误情况
        //{"error":{"code":"SessionTimeout","error":"缓存信息失效！","uri":""}}
        var errorCode=json.error.code;
        if(errorCode!=''){
            //console.log(data);
            switch (errorCode){
                case "SessionTimeout":
                    $.layer.alert("登录信息失效，请重新登录系统。",function(){
                        toLogin();
                    });
                    return;
                case "NoLogin":
                    $.layer.alert("请先登录系统");
                    toLogin();
                    return;
                case(500):
                    var data= jqXHR.responseText;
                    var json=typeof data=='string'?JSON.parse(data):data;
                    $.layer.alert(json.error.error);
                    break;
                case(401):
                    // alert("未登录");
                    $.layer.alert("未登录！");
                    break;
                case(403):
                    $.layer.alert("您无权限执行此操作！");
                    //alert("无权限执行此操作");
                    break;
                case(404):
                    $.layer.alert("404不存在的请求路径!");
                    break;
                case(408):
                    $.layer.alert("请求超时！");
                    break;
                default:
                    $.layer.alert("未知错误！");
            }
        }

        //{"code":501, "msg":"sso not login."}  兼容sso
        errorCode=json.code;
        var msg=json.msg;
        if(isNotEmpty(errorCode)){
            switch (errorCode){
                case "501":
                case 501:
                    $.layer.alert(msg,function(){
                        toLogin();
                    });
                    return;
                default:
                    console.info(errorCode+""+msg);
            }
        }
    }catch (Ex){}
}
/*根据ajax返回结果给出对应的提示*/

//他与上面的方法相比多了成功时的回调方法
function ajaxReturnMsg(data,successCallBack,oparent){
    var json=typeof data=='string'?JSON.parse(data):data;
    try{
        //先检查异常错误情况
        //{"error":{"code":"SessionTimeout","error":"缓存信息失效！","uri":""}}
        var errorCode=json.error.code;
        if(errorCode!=''){
            switch (errorCode){
                case "SessionTimeout":
                    $.layer.alert("登录信息失效，请重新登录系统。",function(){
                        toLogin();
                    });
                    return;
                case "NoLogin":
                    $.layer.alert("请先登录系统");
                    toLogin();
                    return;
                case(500):
                    var data= jqXHR.responseText;
                    var json=typeof data=='string'?JSON.parse(data):data;
                    $.layer.alert(json.error.error);
                    break;
                case(401):
                    $.layer.alert("未登录！");
                    break;
                case(403):
                    $.layer.alert("您无权限执行此操作！");
                    break;
                case(404):
                    $.layer.alert("404不存在的请求路径!");
                    break;
                case(408):
                    $.layer.alert("请求超时！");
                    break;
                default:
                    $.layer.alert("未知错误！");
            }
        }
    }catch (Ex){}
    var type=json.type;
    var msg=json.content;
    switch (type){
        case 'success':
            $.layer.msg_right(msg);
            break;
        case 'error':
            $.layer.msg_wrong(msg);
            break;
        case 'info':
            $.layer.msg_info(msg);
            break;
        case 'block':
        case 'warning':
        case 'danger':
            $.layer.msg_warning(msg);
            break;
        default :
            Fast.msg("操作成功!");
    }
    if(!isEmpty(successCallBack)&&typeof successCallBack=='function'&&type=='success'){
        successCallBack();
    }
}

/*封装了layer弹出层组件*/


var layer_skin='layui-layer-molv';



/**
 * 判断非空
 * @param val
 * @returns {Boolean}
 */
function isEmpty(val) {
    val = $.trim(val);
    if (val == null)
        return true;
    if (val == undefined || val == 'undefined')
        return true;
    if (val == "")
        return true;
    if (val.length == 0)
        return true;
    if (!/[^(^\s*)|(\s*$)]/.test(val))
        return true;
    return false;
}
function isNotEmpty(val) {
    return !isEmpty(val);
}
/*
* 两参数时会自动调用bind，三参数需要自己调用
*/
function sys_ajaxGet(url,data,callback){
    if (callback===undefined){
        if (data===undefined){
            $.getJSON(sys_ctx+url, function(json){bind(json);});
        }else{
            $.getJSON(sys_ctx+url, data,function(json){bind(json);});
        }
    }else{
        $.getJSON(sys_ctx+url, data, callback);
    }
}
/**
 * ajax post
 * @param _url 请求地址如：/sys/user/save
 * @param _data 请求参数
 * @param callback 成功的回调函数
 */
function sys_ajaxPost(_url,_data,callback){
    if (typeof callback!="function"){
        $.ajax({type:"POST", url:sys_ctx+_url, data:_data, dataType:"json",
            beforeSend:function(){
                layer.load(1);
            },
            success:function (msg) {
                layer.closeAll('loading');
                Fast.msg_success(msg);
            }
        });
    }else{

        $.ajax({type:"POST", url:sys_ctx+_url, data:_data,
            success:callback
        });
    }
}
/*****************************************Bind数据*********************************************/
var sys_bind_formData;//抽取出json中的formData供select组件使用
//绑定函数
function bind(bindValue,flag){//json map结构
    if(flag!=null&&flag!=undefined&&flag==true){
        sys_bind_formData=bindValue;
    }else{
        for(var key in bindValue.formData){
            sys_bind_formData = bindValue.formData;
            break;
        }
    }
    if (bindValue.result===false){
        window.alert(bindValue.msg);
        return;
    }
    //alert(JSON.stringify(sys_bind_formData));
    if(typeof bindValue=='string'){
        bindValue=eval("("+bindValue+")");
    }

    $.each(bindValue,function(k,v){
        if(v=='null'||v==null){
            v="";
        }
        //k=k.toLowerCase();
        if (/selectData.*/ig.test(k))//先显示下拉列表数据
        {bind(v);return;}
        if ((/formData.*/ig.test(k))||(/gridData.*/ig.test(k)))
        {bind(v);return;}
        var elem = document.getElementById(k);
        if(elem==undefined||elem==null){
            elem = $('[name='+k+']');
            if(elem!=undefined&&elem!=null){
                elem = elem[0];
            }
        }
        if (elem!==null&&elem!=undefined){
            switch(elem.tagName) {
                case "INPUT":
                    bindInput(k,v);
                    break;
                case "TEXTAREA":
                    bindText(k,v);
                    break;
                case "SELECT":
                    bindSelect(k,v);
                    break;
                case "DIV":
                case "SPAN":
                    elem.innerHTML = v;
            }
        }
    });
}
function bindWithPrefix(bindValue,prefix){//json map结构
    sys_bind_formData=bindValue;
    //alert(JSON.stringify(sys_bind_formData));
    if(typeof bindValue=='string'){
        bindValue=eval("("+bindValue+")");
    }
    $.each(bindValue,function(k,v){
        if(v=='null'||v==null){
            v="";
        }
        //k=k.toLowerCase();
        if(prefix!=null && prefix!='' && prefix!= 'null' && prefix!='undefined' ){
            k=prefix+k;
        }
        if (/selectData.*/ig.test(k))//先显示下拉列表数据
        {bind(v);return;}
        if ((/formData.*/ig.test(k))||(/gridData.*/ig.test(k)))
        {bind(v);return;}
        var elem = document.getElementById(k);
        if(elem==undefined||elem==null){
            elem = $('[name='+k+']');
            if(elem!=undefined&&elem!=null){
                elem = elem[0];
            }
        }
        if (elem!==null&&elem!=undefined){
            switch(elem.tagName) {
                case "INPUT":
                    bindInput(k,v);
                    break;
                case "TEXTAREA":
                    bindText(k,v);
                    break;
                case "SELECT":
                    bindSelect(k,v);
                    break;
                case "DIV":
                case "SPAN":
                    elem.innerHTML = v;
            }
        }
    });
}
//input绑定:只根据对象ID进行
function bindInput(e,v) {
    v=filterTimestamp(v);
    var obj = $("#"+e).attr("type");
    switch (obj) {
        case "text":
        case "hidden":
        case "password":$("#"+e).val(v); break;
        case "radio" :
        case "checkbox" :
            $("[name="+e+"]").each(function(){
                if (String(v).indexOf(this.value)>-1) {this.checked=true;}
                else{this.checked=false;}
            });
            break;
    }
}
//下拉列表绑定
function bindSelect(e, v) {
    //alert(JSON.stringify(v));
    if (v instanceof Array){//初始化select列表[{dm='',mc=''},{}]
       // if($("select[name='"+e+"']"))
       // {
            $("select[name='"+e+"']").empty();
            var aObjsel=document.getElementsByName(e);
      //  }else{
         //   var $tar=$("select[data-target='"+e+"']")
         //   $tar.empty();
         //   var aObjsel=document.getElementsByName(e);
      //  }
        var objsel;
        if(aObjsel.length>0){
            objsel = aObjsel[0];
        }else{
            return;
        }
        var mselected = 0;
        var optioninnerHtml = "";
        for(var i=0;i<v.length;i=i+1){

            if((i==0||i==1)&&(objsel.type!="select-multiple")){
                if ((i==0)&&(v[i].dm!="-")&&(v[i].dm!="+")){//如果有删除标志则不显示请选择
                    optioninnerHtml = optioninnerHtml + "<option value=\"\">请选择</option>";
                }
                if (v[i].dm=="-"){
                    continue;
                }
            }
            var oOption=document.createElement("OPTION");
            if (v[i].dm=="+") {v[i].dm="";}
            oOption.value=v[i].dm;

            oOption.text=v[i].mc;
            var isel = v[i]['selected'];
            if(!!isel){
                $(oOption).attr("selected",true);
                mselected = 1
                optioninnerHtml = optioninnerHtml + "<option value=\""+v[i].dm+"\" selected=\"true\">"+v[i].mc+"</option>";
            }else{
                optioninnerHtml = optioninnerHtml + "<option value=\""+v[i].dm+"\">"+v[i].mc+"</option>";
            }
           // alert(optioninnerHtml);
        }
        for(var z=0;z<aObjsel.length;z++){
            $(aObjsel[z]).append(optioninnerHtml);
            if (aObjsel[z].type!="select-multiple" && mselected == 0){aObjsel[z].selectedIndex=0;}
            if(mselected == 1){
                $(aObjsel[z]).trigger("change");
            }
        }
    }else{
        var element =document.getElementById(e);
        for (var j = 0; j < element.options.length; j=j+1) {
            var temp = element.options[j];
            //可以绑定多值的情况
            temp.selected= (eval("/\\b"+temp.value+"\\b/.test(\""+v+"\")"));
        }
    }
}
function htmlEscape(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}
//文本绑定
function bindText(e, v) {
    v=filterTimestamp(v);
    $("#"+e).val(v);
}
//过滤掉时间串的毫秒
function filterTimestamp(str){
    var v=new String(str);
    if (v.match(/(\d\d:\d\d:\d\d)\.\d{1,3}/)) {
        var v2=v.replace(/(\d\d:\d\d:\d\d)\.\d{1,3}/g, "$1");
        return v2;
    }
    return v;
}


/*ajax请求出错*/
var ajaxError=function(){
    layer.closeAll('loading');/*移除加载动画*/
    layer.msg("ajax请求出错！", {icon: 5,time:2500});
}

/***********************常用的树形选择弹出框****************************/
//用户选择  params: limit=5
/**
 * 弹出用户选择框
 * @param idInput 选择的用户ids返回给父页面元素的id
 * @param mcInput 选择的用户名称返回给父页面元素的id
 * @param params  可选参数，如limit=5表示限选5个
 * @param oparent 一般为null,表明调用该方法页面的层级
 */
var base_openYhxzPage = function(idInput, mcInput, params,oparent){
    var url = '/base/util/yhxz';
    var show='';
    url += '?show='+show + (typeof params === 'string' ? "&"+params : "");
    url+="&idInput="+idInput+"&mcInput="+mcInput;
    $.layer.open_page("用户选择",url,{w:"500px",oparent:oparent,shadeClose:true,maxmin:false})
}
//部门选择  params: limit=5
var base_openBmxzPage = function(idInput, mcInput, params,oparent){
    var url = '/base/util/yhxz';
    var show='';
    url += '?show='+show + (typeof params === 'string' ? "&"+params : "");
    url+="&idInput="+idInput+"&mcInput="+mcInput+"&treeType=bm";
    layer_open("部门选择",url,oparent,null,{shadeClose:true,maxmin:false})
}
//岗位选择  params: limit=5
var base_openGwxzPage = function(idInput, mcInput, params,oparent){
    var url = '/base/util/yhxz';
    var show='';
    url += '?show='+show + (typeof params === 'string' ? "&"+params : "");
    url+="&idInput="+idInput+"&mcInput="+mcInput+"&treeType=gw";
    layer_open("岗位选择",url,oparent,null,{shadeClose:true,maxmin:false})
}

/**
 * 弹出图标选择框
 * @param idInput 选择图标class返回给调用页面元素的id
 * @param value 初始值
 * @param oparent 一般为null,表明调用该方法页面的层级
 */
var base_openIconPage = function(idInput, value,oparent){
    var url = '/base/util/iconxz';
    url += '?idInput='+idInput+'&mcInput='+value;
    layer_open_full("图标选择",url,oparent,null,{shadeClose:false,maxmin:false})
}
/**
 * 弹出地区选择
 * @param idInput
 * @param mcInput
 * @param oparent
 */
var base_openAreaPage = function(idInput, mcInput,oparent){
    var url = '/base/util/dqxz';
    url += '?mcInput='+mcInput;
    url+="&idInput="+idInput;
    layer_open("地区选择",url,oparent,null,{width:"250px",shadeClose:false,maxmin:false})
}
/**
 * 弹出资源分类选择
 * @param idInput 选中的资源分类id返回给调用页面元素的id
 * @param mcInput 选中的资源分类名称返回给调用页面元素的id
 * @param params 可选参数
 * @param oparent 一般为null,表明调用该方法页面的层级
 */
var base_openZyflxzPage = function(idInput, mcInput, params,oparent){
    var url = '/cms/util/zyflxz';
    var show='';
    url += '?show='+show + (typeof params === 'string' ? "&"+params : "");
    url+="&idInput="+idInput+"&mcInput="+mcInput;
    layer_open("资源分类选择",url,oparent,null,{width:"350px",shadeClose:true,maxmin:false})
}

//菜单权限选择
/*var base_openMenuxzPage = function(idInput, mcInput, params,oparent,endFun){
   // var url = '/cms/util/selectMenu';
    var url = '/base/util/qxxz';
    var show='';
    url += '?show='+show + (typeof params === 'string' ? "&"+params : "");
    url+="&idInput="+idInput+"&mcInput="+mcInput;
    layer_open("菜单选择",url,'',oparent,endFun,{width:"300px",shadeClose:true,maxmin:false});
}*/
var base_openMenuxzPage = function(idInput, mcInput,endFun){
    var url = '/base/util/qxxz';
    var show='';
    url += '?show='+show ;
    url+="&idInput="+idInput+"&mcInput="+mcInput;
    layer_open("菜单选择",url,null,endFun,{width:"300px",shadeClose:true,maxmin:false});
    $.layer
}
/*弹出层*/

/*弹出页面层*/
function layer_openHtml(title,htmlContent,oparent,options){
    var opts={
        title:title,
        skin: 'layui-layer-rim', //加上边框
        type: 1,/*type为1的时候，content接收html文本或dom*/
        area:'auto',
        scrollbar: false,/*屏蔽浏览器滚动条*/
        shadeClose: true, //开启遮罩关闭
        content:htmlContent
    };
    opts= $.extend(opts,options);
    if(oparent!=undefined&&oparent!=null&&oparent!=''){
        var index=oparent.layer.open(opts);
    }else{
        var index=layer.open(opts);
    }
    return index;
}


/**
 * 新增和编辑的时候弹出iframe层最常用到
 * @param title 标题
 * @param url 请求的url
 * @param oparent 一般为null,，可以为window.top
 * @param endCallBack 关闭该弹框后的回调函数
 * @param options layer的其他参数(默认btn:false)
 */
function layer_open(title,url,oparent,endCallBack,options){
    var w,h;
    if (isEmpty(options)||isEmpty(options.width)) {
        w=$(window).width() > 800 ? '800px' : '85%';
    };
    if (isEmpty(options)||isEmpty(options.height)) {
        h=$(window).height() > 600 ? '600px' : '100%';
    };
    var  opts={
        //skin: 'layui-layer-lan',
        type: 2,
        area: [w, h],
        fix: false, //不固定
        maxmin: true,
        shadeClose:true,
        shade:0.4,
        btn: ['确定', '关闭'],
        title: title,
        yes: function(index,layero){
            var body = layer.getChildFrame('body', index);
            var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：
            iframeWin.save();
        },
        content: url
    };
    if(typeof endCallBack=='function'){
        $.extend(opts,{end:endCallBack});
    }
    opts= $.extend(opts,options);
    opts.btn=false;//为了兼容老程序，(默认btn:false)
    if (isEmpty(title)) {
        opts.title=false;
    };
    var index;
    if(oparent!=undefined&&oparent!=null&&oparent!=''){
        index=oparent.layer.open(opts);
    }else{
        index=layer.open(opts);
    }
}
//弹出即全屏
function layer_open_full(title,url,oparent,endCallBack,options){
    var opts;
    var w="90%";
    var h="90%";
    opts={
            //skin: 'layui-layer-lan',
            type: 2,
            area: [w,h],
            fix: false, //不固定
            maxmin: true,
            shadeClose:true,
            shade:0.4,
            title: title,
            content: url
    };

    if(typeof endCallBack=='function'){
        $.extend(opts,{end:endCallBack});
    }
    opts= $.extend(opts,options);
    if (title == null || title == '') {
        opts.title=false;
    };
    var index;
    if(oparent!=undefined&&oparent!=null&&oparent!=''){
        index=oparent.layer.open(opts);
    }else{
        index=layer.open(opts);
    }
    layer.full(index);
}

//统一的ajax请求异常处理
$(function(){
    // 设置jQuery Ajax全局的参数
    $.ajaxSetup({
        type: "POST",
        error: function(jqXHR, textStatus, errorThrown){
            layer.closeAll('loading');
            //console.log(jqXHR.responseText);
            switch (jqXHR.status){
                case "SessionTimeout":
                case "NoLogin":
                case "SsoNoLogin":
                    alert(jqXHR.responseText);
                    toLogin();
                    return;
                case(500):
                    var data= jqXHR.responseText;
                    var json=typeof data=='string'?JSON.parse(data):data;
                    $.layer.alert(json.error.error);
                    break;
                case(401):
                    $.layer.alert("未登录！");
                    break;
                case(403):
                    var msg="";
                    try{
                        var data= jqXHR.responseText;
                        var json=typeof data=='string'?JSON.parse(data):data;
                        msg=json.error.error;
                    }catch (e){}
                    $.layer.alert(isNotEmpty(msg)?msg:"您无权限执行此操作！");
                    break;
                case(404):
                    $.layer.alert("404不存在的请求路径!");
                    break;
                case(408):
                    $.layer.alert("请求超时！");
                    break;
                default:
                    $.layer.alert("未知错误！");
            }
        },
        complete: function (XMLHttpRequest, textStatus) {
            //console.log("textStatus==="+textStatus);
        }
    });
});
/*****************************************Bootstrap处理*******************************************/
//列表显示的基础配置
var table_list_Params = {
    escape:false,
    pagination: true,  //开启分页
    sidePagination: 'server',//服务器端分页
    pageNumber: 1,//默认加载页
    pageSize: 10,//每页数据
    pageList: [10,15, 20, 50],//可选的每页数据
    showColumns:false,
    showRefresh:false,
    queryParams:function (params) {
        return {
            rows: params.limit,
            page: (params.offset / params.limit + 1),
            offset: params.offset
        }
    },
    columns:[],
    onLoadSuccess:function(data){
        checkAjaxError(data);
        return false;
    },
    onLoadError:function(status){
        console.log("请求出错啦！错误代码:"+status);
        Fast.msg_error("请求出错啦！错误代码:"+status);
        return false;
    },
    url:"",
    onClickRow:function(){}
}



//某字符串是否包含指定字符串
function containsString(str1,target,separte){
    var flag=false;
    if(separte==undefined||separte==null||separte==""){
        separte="|";
    }
    var arr=str1.split(separte);

    $.each(arr,function(i,data){
        if(data==target){
            flag=true;
        }
    });
    return flag;
}
function isEmail(v) {
    var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    if (!myreg.test(v))//对输入的值进行判断
    {
        return false;
    }
    return true;
}
function isQQ(v){
    if(/^[0-9]{5,10}$/.test(v)){
        return true;
    }else{
        return false;
    }
}
function  addFav(title,url) {//  加入收藏夹
    if(document.all)
    {
        window.external.addFavorite(url,title);
    }
    else if(window.sidebar)
    {
        window.sidebar.addPanel(title,url,  "" );
    }
}


String.prototype.endsWith = function (str){
    return this.slice(-str.length) == str;
};

$(function(){
    baseMotheds.init();
});
var baseMotheds = function(){

    var semantic_checkbox=function(){
        try{
            $checkbox= $('form').find('.ui.checkbox') ;
            $checkbox.checkbox();
        }catch(ex){}
    }
    var scrollHead = function(){
        var headGroup = $('#headGroup').height();
        var headFullTabs = $('#headFullTabs').height();
        var sroll = headGroup - headFullTabs;
        $(window).bind("scroll resize", function() {
            if ($(window).scrollTop() > sroll) {
                $('#headFullTabs').addClass('index');
                $('#indexGroup').css('padding-top','50px');
            }else{
                $('#headFullTabs').removeClass('index');
                $('#indexGroup').css('padding-top','0');
            }
        });
    };
    var scrollHeadNew = function(){
        var nav = $('#nav').height();
        var fullPitch = $('.fullPitch').height();
        var headFullTabs = $('#headFullTabs').height();
        var sroll = nav+fullPitch+headFullTabs+headFullTabs-1;
        $(window).bind("scroll resize", function() {
            if ($(window).scrollTop() > sroll) {
                $('#headFullTabs').addClass('index');

            }else{
                $('#headFullTabs').removeClass('index');

            }
        });
    };
    // headSearch
    var headSearch = function(){
        $('.searchWrap .searchbtn').click(function(){
         var searchText=$('.searchWrap input.searchInput').val();
         if(isEmpty(searchText)){
         return;
         }
         $.layer.alert("搜索功能正在开发中...");
         });
         $('.searchWrap input.searchInput').focus(function(){
         $(this).parents('.searchWrap').addClass('show');
         }).blur(function(){
         if(isEmpty($(this).val())){
         $(this).parents('.searchWrap').removeClass('show');
         }
         });

    };
    var headSearchNew = function(){
        $('#searchbtn').click(function(){
            var searchText=$('#div_search input.searchInput').val();
            if(isEmpty(searchText)){
                return;
            }else{
                //$.layer.alert("搜索功能正在开发中...");
                window.location.href="/myblog/search?words="+searchText+"&rows=6";
            }

        });
        document.onkeydown = function(e){
            var ev = document.all ? window.event : e;
            if(ev.keyCode==13) {
                $('#searchbtn').click();
            }
        }
    };
    // lazyLoad
    var lazyLoad = function(){
        try{
            $("img.lazy").lazyload({threshold: 100,effect: "fadeIn"});
        }catch (ex){}
    };
    var imgError = function(){
        document.addEventListener("error", function (e) {
            var elem = e.target;
            if (elem.tagName.toLowerCase() == "img") {
                elem.src = "/resources/myblog/assets/i/f10.jpg";
            }
        }, true);
    };
    var imgNull = function(){
        $('img.lazy').each(function(){
            var _newImg = '/resources/myblog/assets/i/f10.jpg';
            var _thisSrc = $(this).attr('src');
            if(_thisSrc == '' || _thisSrc == undefined){
                $(this).attr('src',_newImg);
            }
        });
        document.addEventListener("error", function (e) {
            var elem = e.target;
            if (elem.tagName.toLowerCase() == "img") {
                elem.src = "/resources/myblog/assets/i/f10.jpg";
            }
        }, true);
    };
    var goTop=function(){// 返回顶部
        $('.to-top').toTop({
            autohide: true,//返回顶部按钮是否自动隐藏。可以设置true或false。默认为true
            offset: 100,//页面滚动到距离顶部多少距离时隐藏返回顶部按钮。默认值为420
            speed: 500,//滚动和渐隐的持续时间，默认值为500
            right: 25,//返回顶部按钮距离屏幕右边的距离，默认值为15
            bottom: 50//返回顶部按钮距离屏幕顶部的距离，默认值为30
        });

        /*$("body").on("click","#gotop",function(e) {
            $('body,html').animate({scrollTop:0},300);
        });
        $(window).scroll(function(e) {
            if($(window).scrollTop()>200)
                $("#gotop").show();
            else
                $("#gotop").hide();
        });*/
    }
    var bubble=function () {
        if ($.bubble) {
            $.bubble.init();
        }
    }
    var websocket=function(){
        if ($.websocket) {
            var host=window.location.host;
            host="ws://" + host + "/websocket";
            //alert(host);
            //if (host) {
                // 申请显示通知的权限

                $.websocket.open({
                    host: host,
                    reconnect: true,
                    callback: function (result) {
                        var resultJson = JSON.parse(result);
                        //console.log(result);
                        wesocketMsgResolver[resultJson["fun"]](resultJson["msg"]);
                    }
                });
            /*} else {
                console.warn("网站host获取失败，将不启动webscoket。");
            }*/
        }
    }
    return{
        init: function(){
            scrollHead();
            scrollHeadNew();
            headSearch();
            headSearchNew();
            lazyLoad();
            semantic_checkbox();
            //imgError();
            //imgNull();
            goTop();
            bubble();
            websocket();
        }
    }
}();
/**
 * websocket消息解析器
 *
 * @type {{online: wesocketMsgResolver.online}}
 */
var wesocketMsgResolver = {
    online: function (value) {
        value && $(".onlineNum").html(value);
    },
    notification: function (value) {
        value && Fast.msg_info(value);
    }
};
var showSendMessage=function(){
var html='<div class="ui  small modal transition hidden"  id="modal_modal4">';
    html+='<i class="close icon"></i>';
    html+='<div class="header">';
    html+='发送私信';
    html+='</div>';
    html+='<div class="content">';
    html+='<div class="ui form">';
    html+='<div class="field">';
    html+='<textarea placeholder="请输入内容"></textarea>';
    html+='</div>';

    html+='</div>';
    html+='</div>';
    html+='<div class="actions">';
    html+='<div class="ui deny  button">取消</div>';
    html+='<div class="ui positive  button">发送</div>';
    html+='</div>';
    html+=' </div>';

    $(html).modal('show');

}

//  格式化文件大小
function renderSize(value){
    if(null==value||value==''){
        return "0 Bytes";
    }
    var unitArr = new Array("Bytes","KB","MB","GB","TB","PB","EB","ZB","YB");
    var index=0,
        srcsize = parseFloat(value);
    index=Math.floor(Math.log(srcsize)/Math.log(1024));
    var size =srcsize/Math.pow(1024,index);
    //  保留的小数位数
    size=size.toFixed(2);
    return size+unitArr[index];
}
function getCookie(name){
    //获取cookie字符串
    var strCookie=document.cookie;
    //将多cookie切割为多个名/值对
    var arrCookie=strCookie.split("; ");
    var value="";
    //遍历cookie数组，处理每个cookie对
    for(var i=0;i<arrCookie.length;i++){
        var arr=arrCookie[i].split("=");
        if(name==arr[0]){
            value=arr[1];
            break;
        }
    }
    return value;
}
function randomInt(min, max){
    return Math.floor((Math.random() * max) + min);
}

(function ($) {
    $.extend({
        layer:{
            msg: function(content, type,options) {
                if(isNotEmpty(options)&&isNotEmpty(options.oparent)){
                    var opts={ icon: type, time: 2000, shift: randomInt(1, 5) };
                    if(isNotEmpty(options)){
                        opts= $.extend(opts,options);
                    }
                    if (isNotEmpty(type)) {
                        opts.icon=type;
                    }
                    options.oparent.layer.msg(content,opts);
                }else{
                    var opts={ icon: type, time: 2000, shift: randomInt(1, 5) };
                    if(isNotEmpty(options)){
                        opts= $.extend(opts,options);
                    }
                    if (isNotEmpty(type)) {
                        opts.icon=type;
                    }
                    layer.msg(content,opts);
                }

            },
            msg_top:function(content, type,options){
                options=$.extend({offset:1},options);
                $.layer.msg(content, type,options);
            },
            msg_right:function(content,oparent){
                $.layer.msg(content, 1,{oparent:oparent});
            },
            msg_wrong:function(content,oparent){
                $.layer.msg(content, 2,{oparent:oparent});
            },
            msg_ask:function(content,oparent){
                $.layer.msg(content, 3,{oparent:oparent});
            },
            msg_locked:function(content,oparent){
                $.layer.msg(content, 4,{oparent:oparent});
            },
            msg_cry:function(content,oparent){
                $.layer.msg(content, 5,{oparent:oparent});
            },
            msg_smile:function(content,oparent){
                $.layer.msg(content, 6,{oparent:oparent});
            },
            msg_warning:function(content,oparent){
                $.layer.msg(content, 7,{oparent:oparent});
            },
            alert:function(content,options){
                var callback;
                if(isNotEmpty(options)&&typeof (options.callback)=='function'){
                    callback=options.callback;
                }
                if(typeof options=='function'){
                    callback=options;
                }
                  var opts = $.extend({
                        skin:layer_skin,
                        shade: [0.1,'#000'],
                        shadeClose: true
                        // shift:2/*动画类型2为从下向上出现*/
                    },options);

                layer.alert(content, opts,callback );
            },
            alert_right:function(content,options){
                options=$.extend({icon:1},options);
                $.layer.alert(content,options);
            },
            alert_wrong:function(content,options){
                options=$.extend({icon:2},options);
                $.layer.alert(content,options);
            },
            alert_ask:function(content,options){
                options=$.extend({icon:3},options);
                $.layer.alert(content,options);
            },
            alert_locked:function(content,options){
                options=$.extend({icon:4},options);
                $.layer.alert(content,options);
            },
            alert_cry:function(content,options){
                options=$.extend({icon:5},options);
                $.layer.alert(content,options);
            },
            alert_smile:function(content,options){
                options=$.extend({icon:6},options);
                $.layer.alert(content,options);
            },
            alert_warning:function(content,options){
                options=$.extend({icon:7},options);
                $.layer.alert(content,options);
            },
            //询问框
            confirm:function(content,callbackOK,callbackCancel){
               var options={
                    btn: ['确定','取消'],
                    icon: 3,
                    title:'询问',
                    shade: [0.3,'#000']
                };
                layer.confirm(content,options , function (index) {
                    layer.close(index);
                        if(typeof(callbackOK)=='function' ){
                            callbackOK();
                        }
                }, callbackCancel
                );
            },
            //打开一个Iframe页面
            open_page: function (title, url,options) {
                var w,h;
                if(isEmpty(options)||isEmpty(options.w)){
                    w="70%";
                }else{
                    w=options.w;
                }
                if(isEmpty(options)||isEmpty(options.h)){
                    h="70%";
                }else{
                    h=options.h;
                };
                if (navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)) {
                    w = 'auto';
                    h = 'auto';
                }
                var  opts={
                    type: 2,
                    area: [w, h],
                    fix: false, //不固定
                    maxmin: true,
                    shadeClose:true,
                    shade:0.4,
                    btn: ['确定', '关闭'],
                    title: title,
                    yes: function(index,layero){
                        var body = layer.getChildFrame('body', index);
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        iframeWin.save();
                    },
                    content: url
                };
                if(isNotEmpty(options)&&isNotEmpty(options.endCallBack)&& typeof (options.endCallBack)=='function'){
                        $.extend(opts,{end:endCallBack});
                }
                opts= $.extend(opts,options);
                if (isEmpty(title)) {
                    opts.title=false;
                };
                if(isNotEmpty(options)&&isNotEmpty(options.oparent)){
                    options.oparent.layer.open(opts);
                }else{
                   layer.open(opts);
                }
            },
            //打开即全屏
            open_page_full: function (title, url,options) {
                var w,h;
                if(isEmpty(options)||isEmpty(options.w)){
                    w="70%";
                };
                if(isEmpty(options)||isEmpty(options.h)){
                    h="70%";
                };
                if (navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)) {
                    w = 'auto';
                    h = 'auto';
                }
                var  opts={
                    type: 2,
                    area: [w, h],
                    fix: false, //不固定
                    maxmin: true,
                    shadeClose:true,
                    shade:0.4,
                    btn: ['确定', '关闭'],
                    title: title,
                    yes: function(index,layero){
                        var body = layer.getChildFrame('body', index);
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        iframeWin.save();
                    },
                    content: url
                };
                if(isNotEmpty(options)&&isNotEmpty(options.endCallBack)&& typeof (options.endCallBack)=='function'){
                    $.extend(opts,{end:endCallBack});
                }
                opts= $.extend(opts,options);
                if (isEmpty(title)) {
                    opts.title=false;
                };
                var index;
                if(isNotEmpty(options)&&isNotEmpty(options.oparent)){
                    index=options.oparent.layer.open(opts);
                }else{
                    index=layer.open(opts);
                }
                layer.full(index);
            },
            // 打开遮罩层
            loading: function () {
                var loadType=1;
                var opts= {
                    area:['37px','37px'],
                    shade: [0.1,'#000']
                };
                layer.load(loadType,opts);
            },
            // 关闭遮罩层
            closeLoading: function () {
                setTimeout(function(){
                    //layer.closeAll(); //疯狂模式，关闭所有层
                    //layer.closeAll('dialog'); //关闭信息框
                    //layer.closeAll('page'); //关闭所有页面层
                    //layer.closeAll('iframe'); //关闭所有的iframe层
                    layer.closeAll('loading'); //关闭加载层
                    //layer.closeAll('tips'); //关闭所有的tips层
                }, 50);
            },
            tips:function(msg,attachment,position){
                layer.tips(msg, attachment, {
                    tips: [position, '#3595CC'],
                    time:5000
                });
            }

}
        ,
        tree:{
            _option: {},
            // 初始化树结构
            init: function(options) {
                $.tree._option = options;
                // 属性ID
                var _id = isEmpty(options.id) ? "tree" : options.id;
                // 展开等级节点
                var _expandLevel = isEmpty(options.expandLevel) ? 0 : options.expandLevel;
                // 树结构初始化加载
                var setting = {
                    check: options.check,
                    view: { selectedMulti: false, nameIsHTML: true,showLine: true,
                        showIcon: true
                    },
                    data: { key: { title: "title"}, simpleData: { enable: true,
                        idKey: isEmpty(options.idKey)? "id":options.idKey,
                        pIdKey: isEmpty(options.pIdKey)? 'parentid':options.pIdKey
                    }
                    },
                    callback: {
                        beforeAsync: options.beforeAsync,
                        onAsyncSuccess: options.onAsyncSuccess,
                        onClick: options.onClick
                    }
                };
                $.getJSON(options.url, function(data) {

                    var  tree = $.fn.zTree.init($("#" + _id), setting, data);
                    $._tree = tree;
                    // 展开第一级节点
                    var nodes = tree.getNodesByParam("level", 0);
                    for (var i = 0; i < nodes.length; i++) {
                        if(_expandLevel > 0) {
                            tree.expandNode(nodes[i], true, false, false);
                        }
                        //$.tree.selectByIdName(treeId, treeName, nodes[i]);
                    }
                    // 展开第二级节点
                    nodes = tree.getNodesByParam("level", 1);
                    for (var i = 0; i < nodes.length; i++) {
                        if(_expandLevel > 1) {
                            tree.expandNode(nodes[i], true, false, false);
                        }
                       // $.tree.selectByIdName(treeId, treeName, nodes[i]);
                    }
                    // 展开第三级节点
                    nodes = tree.getNodesByParam("level", 2);
                    for (var i = 0; i < nodes.length; i++) {
                        if(_expandLevel > 2) {
                            tree.expandNode(nodes[i], true, false, false);
                        }
                        //$.tree.selectByIdName(treeId, treeName, nodes[i]);
                    }
                });
            },
            //根据数据ID获得该节点

            getNodeById:function(id){
                var node=  $._tree.getNodeByParam(isEmpty($.tree._option.idKey)? "id":$.tree._option.idKey, id, null);
                return node;
            },
            //根据数据ID移除某个节点
            removeNodeById:function(id){
                var node=  $.tree.getNodeById(id);
                $._tree.removeNode(node);
            },
            removeNodeByParam:function(name,value){
                var node=  $._tree.getNodeByParam(name, value, null);
                $._tree.removeNode(node);
            },
            // 根据Id和Name选中指定节点
            selectByIdName: function(treeId, treeName, node) {
                if (isNotEmpty(treeName) && isNotEmpty(treeId)) {
                    if (treeId == node.id && treeName == node.name) {
                        $._tree.selectNode(node, true);
                    }
                }
            },
            // 获取当前被勾选集合
            getCheckedNodes: function(column) {
                var _column =isEmpty(column) ? "id" : column;
                var nodes = $._tree.getCheckedNodes(true);
                return $.map(nodes, function (row) {
                    return row[_column];
                }).join();
            },
            // 折叠
            collapse: function() {
                $._tree.expandAll(false);
            },
            // 展开
            expand: function() {
                $._tree.expandAll(true);
            }
        }
        ,
        toastr:{
            config:{
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            },
            info:function(msg,title){
                toastr.options=$.toastr.config;
                toastr.info(msg,title);
            },
            success:function(msg,title){
                toastr.options=$.toastr.config;
                toastr.success(msg,title);
            },
            warning:function(msg,title){
                toastr.options=$.toastr.config;
                toastr.warning(msg,title);
            },
            error:function(msg,title){
                toastr.options=$.toastr.config;
                toastr.error(msg,title);
            }
        }
    });

    var Fast={
        confirm:function(title,okFunction,cancelFunction){
            var index = layer.confirm(
                title,
                {icon: 3, title:"询问", shadeClose: false, btn: ["确定","取消"]},
                function () {
                    if(typeof okFunction=='function'){
                        okFunction();
                    }
                    layer.close(index);
                },function(){
                    if(typeof cancelFunction=='function'){
                        cancelFunction();
                    }
                    layer.close(index);
                }
            );
            return index;
        },
        msg:function(msg){
            if(GlobalConfig.msg_type==GlobalConfig.msg_type_toastr){
                $.toastr.info(msg);
            }else{
                layer.msg(msg);
            }
        },
        msg_success:function(msg){
            if(GlobalConfig.msg_type==GlobalConfig.msg_type_toastr){
                $.toastr.success(msg);
            }else{
                $.layer.msg_right(msg);
            }
        },
        msg_error:function(msg){
            if(GlobalConfig.msg_type==GlobalConfig.msg_type_toastr){
                $.toastr.error(msg);
            }else{
                $.layer.msg_wrong(msg);
            }
        },
        msg_warning:function(msg){
            if(GlobalConfig.msg_type==GlobalConfig.msg_type_toastr){
                $.toastr.warning(msg);
            }else{
                $.layer.msg_warning(msg);
            }
        },
        msg_info:function(msg){
            if(GlobalConfig.msg_type==GlobalConfig.msg_type_toastr){
                $.toastr.info(msg);
            }else{
                $.layer.msg_warning(msg);
            }
        },
        isExist:function (url){
            var flag=false;
            if(url.indexOf("http")==-1){
                var host=window.location.host;
                var protocol=window.location.protocol;
                url=protocol+"//"+host+url;
            }
            $.ajax({async:false,type:"GET",url:url,error:function(data,status){
                //这里需要覆盖ajaxSetup中的方法
            },complete:function(XMLHttpRequest, textStatus) {
                 if(textStatus=="success"){
                     flag= true;
                 }
            }
            });
            return flag;
        },
        changeUrlArg:function(url, arg, val){
            var pattern = arg+'=([^&]*)';
            var replaceText = arg+'='+val;
            return url.match(pattern) ? url.replace(eval('/('+ arg+'=)([^&]*)/gi'), replaceText) : (url.match('[\?]') ? url+'&'+replaceText : url+'?'+replaceText);
        },
        getUri:function(){
            var url=  window.location.href;
            var host=window.location.host;
            var protocol=window.location.protocol;
            var uri=url.replace(protocol+"//"+host,"");
            if(uri.indexOf("?")>0){
                uri=uri.substring(0,uri.indexOf("?"));
            }
            return uri;
        }
};
    window.Fast=Fast;
})(jQuery);

(function ($) {
    $.extend({
        websocket: {
            _this: null,
            _initialized: false,
            init: function (options) {
                if (!this.isSupported()) {
                    // console.error('Not support websocket');
                    return;
                }
                var op = $.extend({
                    callback: function () {
                    },
                    host: null,
                    reconnect: false
                }, options);
                if (!op.host) {
                    // console.error("初始化WebSocket失败，无效的请求地址");
                    return;
                }
                try {
                    this._this = new WebSocket(op.host);
                } catch (error) {
                    return;
                }
                this._initialized = true;
                //连接发生错误的回调方法
                this._this.onerror = function () {
                    // console.log("与服务器连接失败...");
                };

                //连接成功建立的回调方法
                this._this.onopen = function (event) {
                    // console.log("与服务器连接成功...");
                };

                //接收到消息的回调方法
                this._this.onmessage = function (event) {
                    // dwz.notification.show({notification: event.data});
                    op.callback(event.data);
                    // console.log("接收到服务器端推送的消息：" + event.data);
                };

                //连接关闭的回调方法
                this._this.onclose = function () {
                    $.websocket._initialized = false;
                    // console.log("已关闭当前链接");
                    if (op.reconnect) {
                        // 自动重连
                        setTimeout(function () {
                            $.websocket.open(op);
                        }, 5000);
                    }
                }
            },
            open: function (options) {
                var op = $.extend({
                    callback: function () {
                    },
                    host: null,
                    reconnect: false
                }, options);

                if (this._initialized) {
                    this.close();
                }
                this.init(options);
                //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
                window.onbeforeunload = function () {
                    // console.log("窗口关闭了");
                    $.websocket.close();
                }
            },
            isSupported: function () {
                return 'WebSocket' in window;
            },
            send: function (message) {
                if (!this._this) {
                    return;
                }
                this._this.send(message);
            },
            close: function () {
                if (!this._this) {
                    return;
                }
                this._this.close();
            }
        }
    });
})(jQuery);
/* 鼠标点击向上冒泡弹出提示动画 */
$.extend({
    bubble: {
        _tip: ['法制', '爱国', '敬业', '诚信', '友善', '富强', '民主', '文明', '和谐', '自由', '平等', '公正'],
        init: function () {
            var bubbleIndex = 0;
            $('body').click(function (e) {
                bubbleIndex = bubbleIndex >= $.bubble._tip.length ? 0 : bubbleIndex;
                if (!e.originalEvent) {
                    return;
                }
                var x = e.originalEvent.x || e.originalEvent.layerX || 0;
                var y = e.originalEvent.y || e.originalEvent.layerY || 0;
                var html = '<span style="position: fixed;z-index:9999;left: ' + x + 'px;top: ' + y + 'px;"></span>';
                var $box = $(html).appendTo($(this));
                $box.effectBubble({
                    y: -100,
                    className: 'thumb-bubble',
                    fontSize: 0.5,
                    content: '<i class="fa fa-smile-o"></i>' + $.bubble._tip[bubbleIndex]
                });
                setTimeout(function () {
                    $box.remove();
                }, 1002);
                bubbleIndex++;
            });
        },
        unbind: function (duration) {
            $("body").unbind('click');
            if (duration && !isNaN(duration = parseInt(duration))) {
                setTimeout(function () {
                    $.bubble.init();
                }, duration);
            }
        }
    }
});

/* 鼠标点击弹出提示动画 */

$.fn.extend({
    // 文字向上冒泡
    effectBubble: function (options) {
        var op = $.extend({
            content: '+1',
            y: -100,
            duration: 1000,
            effectType: 'ease',
            className: '',
            fontSize: 2
        }, options);

        return this.each(function () {
            var $box = $(this), flyId = 'effect-fly-' + (new Date().getTime());

            var tpl = '<span id="#flyId#" class="effect-bubble-fly #class#" style="opacity: 1;top:#top#px;left:#left#px;font-size: #fontSize#rem;">#content#</span>';
            var html = tpl.replaceAll('#left#', 12).replaceAll('#top#', -8)
                .replaceAll('#flyId#', flyId).replaceAll('#content#', op.content)
                .replaceAll('#class#', op.className).replaceAll('#fontSize#', op.fontSize);

            var $fly = $(html).appendTo($box);
            $fly.fadeIn(100, "swing", function () {
                $fly.animate({top: op.y, opacity: 0}, 100, function () {
                    $fly.fadeOut(op.duration, function () {
                        $fly.remove();
                    });
                });
            });
        });
    }
});
/**
 * 扩展String方法
 */
$.extend(String.prototype, {
    trim: function () {
        return this.replace(/(^\s*)|(\s*$)|\r|\n/g, "");
    },
    startsWith: function (str) {
        return new RegExp("^" + str).test(this);
    },
    endsWith: function (str) {
        return new RegExp(str + "$").test(this);
    },
    replaceAll: function (os, ns) {
        return this.replace(new RegExp(os, "gm"), ns);
    }
});

/* 返回顶部插件 */
(function ($) {
    $.fn.toTop = function (opt) {
        //variables
        var elem = this;
        var win = $(window);
        var doc = $('html, body');
        var options = opt;
        //如果没有配置自定义的参数，则使用默认
        if (!options) {
            options = $.extend({
                autohide: true,
                offset: 100,
                speed: 500,
                right: 15,
                bottom: 50
            }, opt);
        }
        elem.css({
            'position': 'fixed',
            'right': options.right,
            'bottom': options.bottom,
            'cursor': 'pointer'
        });
        if (options.autohide) {
            elem.css('display', 'none');
        }
        elem.click(function () {
            doc.animate({scrollTop: 0}, options.speed);
        });
        win.scroll(function () {
            var scrolling = win.scrollTop();
            if (options.autohide) {
                if (scrolling > options.offset) {
                    elem.fadeIn(options.speed);
                }
                else elem.fadeOut(options.speed);
            }
        });
    };
})(jQuery);
/* 返回顶部插件 end */