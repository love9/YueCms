$(function(){
    var data={
        screen:window.screen.width+"*"+window.screen.height,
        colorDepth:window.screen.colorDepth,
        javaAppletEnabled:navigator.javaEnabled(),
        lang:navigator.language,
        charset:document.charset,

        cookieEnabled:navigator.cookieEnabled,
        os:navigator.platform,
        title:document.title,
        domain:document.domain,
        lastModified:document.lastModified,
        timeZone:new Date().getTimezoneOffset()/60
    };
    $.ajax({
        type: 'POST',
        url: "/myblog/page/view",
        data: data,
        success: function(json){
            console.log(json);
        }
     }
    );
})
