/*分装了常用到的jBox-Notice的一些方法以达到简单易用*/

var colors = ['red', 'green', 'blue', 'yellow'], index = 0;
var getColor = function () {
    (index >= colors.length) && (index = 0);
    return colors[index++];
};

var strings = ['Short', 'You just switched the internet off', 'Please do not click too hard - next time we\'ll notify google.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'];
var getString = function () {
    return strings[Math.floor(Math.random()*strings.length)];
};
/**
 * 简单消息提示
 * @param msg  消息内容
 * @param position 提示框方位 有：lb:左下方 lt:左上方 rt:右上方 rb:右下方（默认）
 */
function jBox_simpleMsg(msg,position){
    var x,y;
    if(position==null||position==undefined||position==''){
        x='right';
        y='bottom';
    }else{
        if(position=='lb'){
            x='left';
            y='bottom';
        }else if(position=='lt'){
            x='left';
            y='top';
        }else if(position=='rt'){
            x='right';
            y='top';
        }else{
            x='right';
            y='bottom';
        }
    }
    new jBox('Notice', {
        theme: 'NoticeFancy',
        attributes: {
            x:x ,
            y:y
        },
        color: getColor(),
        content: msg,
        audio: '/resources/audio/msg/msg_msn',
        volume: 60,
        animation: {open: 'slide:bottom', close: 'slide:left'}
    });
}


/**
 * 复杂的消息提示框
 * @param title  消息标题
 * @param msg    消息内容
 * @param position  消息框方位（可选）
 */
function jBox_Msg(msg,title,position){
    var x, y,tl;
    if(title==null||title==''||title==undefined){
        tl=false;
    }else{
        tl=title;
    }
    if(position==null||position==undefined||position==''){
        x='right';
        y='bottom';
    }else{
        if(position=='lb'){
            x='left';
            y='bottom';
        }else if(position=='lt'){
            x='left';
            y='top';
        }else if(position=='rt'){
            x='right';
            y='top';
        }else{
            x='right';
            y='bottom';
        }
    }
    new jBox('Notice', {
        theme: 'NoticeFancy',
        attributes: {
            x: x,
            y: y
        },
        color: getColor(),
        content: msg,
        title: tl,
        maxWidth: 600,
        audio: '/resources/audio/msg/msg',
        volume: 60,
        autoClose: 5000,
        animation: {open: 'slide:bottom', close: 'slide:left'},
        delayOnHover: true,
        showCountdown: true,
        closeButton: true
    });
}