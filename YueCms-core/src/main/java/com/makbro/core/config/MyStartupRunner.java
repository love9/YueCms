package com.makbro.core.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.framework.listener.InitCacheListener;
import com.makbro.core.framework.quartz.bean.Task;
import com.makbro.core.framework.quartz.dao.TaskMapper;
import com.makbro.core.framework.quartz.quartz.QuartzManager;
import com.markbro.base.utils.DbUtil;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.PropertiesListenerConfig;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * springboot 允许实现CommandLineRunner接口的类程序启动后run方法中做一些事情，比如加载缓存.
 */
@Component
@Order(value = 1)
public class MyStartupRunner implements CommandLineRunner {
    protected   final Logger log= LoggerFactory.getLogger(getClass());
    @Autowired
    private List<InitCacheListener> serviceList;

    @Override
    public void run(String... args) throws Exception {
        //spring 启动的时候缓存信息
        this.initParamsFromDb();
        this.getMysqlVersion();

        if(CollectionUtils.isNotEmpty(serviceList)){
            for(InitCacheListener service:serviceList){
                //System.out.println(service.getClass().getName());
                if(!service.getClass().getName().contains("com.sun.proxy")){//不知何故spring注入的实现InitCacheListener接口的多一个名为com.sun.proxy代理类，所以要排除它
                    service.onInit();
                }
            }
        }
        //加载配置文件
        this.loadMyProperties();
        //加载字典数据
        this.loadDictionary();
        this.startQuartzJobs();
    }

    //查询mysql版本号放入到缓存
    private void getMysqlVersion(){
        try{
            String   mysqlVersion= DbUtil.getJtN().queryForObject("select version()",String.class);
            EhCacheUtils.putSysInfo("mysqlVersion",mysqlVersion);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void initParamsFromDb(){ //加载sys_para表的值到缓存中
        try {
            //从数据库查询需要放到系统缓存中的键、值
            List<Map<String,Object>> list = DbUtil.getJtN().queryForList("select csdm,csz from sys_para where deleted=0");
            if (list.size() == 0) {
                throw new Exception("系统参数信息没有配置");
            }
            for (Iterator it = list.iterator(); it.hasNext();) {
                Map map = (Map) it.next();
                EhCacheUtils.putSysInfo((String) map.get("csdm"), map.get("csz"));//放入到缓存
                GlobalConfig.putSysPara((String) map.get("csdm"),(String) map.get("csz"));//放入到静态map
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加载配置文件内容。springboot中可以在实现CommandLineRunner接口的类中做这些事情。非springboot可以使用MyPropertiesListener.java监听器
     */
    private void loadMyProperties(){
        String[] propertyFileName={"config/my.properties","config/thirdParty.properties"};
        for(String s:propertyFileName){
            PropertiesListenerConfig.loadAllProperties(s);
        }
    }

    /**
     * 加载字典数据
     */
    private void loadDictionary(){

        List<Map<String,Object>> list = DbUtil.getJtN().queryForList("select type,value,label,sort from base_dictionary where available=1 and deleted=0");
        if(CollectionUtils.isNotEmpty(list)){
            String type=null;
            List<Map> typeList=null;
            for(Map<String,Object> t:list){
                type=(String)t.get("type");
                String str=GlobalConfig.getDict(type);
                if(StringUtil.notEmpty(str)){
                    typeList= JSON.parseArray(str,Map.class);
                }
                if(CollectionUtils.isEmpty(typeList)){
                    typeList = new ArrayList<>();
                }
                typeList.add(t);
                GlobalConfig.putDict(type, JSON.toJSONString(typeList));
                typeList=null;
            }
        }
    }
    @Autowired
    QuartzManager quartzManager;
    @Autowired
    TaskMapper taskMapper;

    /**
     * 从数据库加载并启动ob
     */
    private void startQuartzJobs(){
        Map qmap=new HashMap<String,Object>();
        qmap.put("available",1);
        List<Task> tasks = taskMapper.find(new PageBounds(), qmap);
        if(CollectionUtils.isNotEmpty(tasks)){
            for(Task task:tasks){
                try {
                    Map jobDataMap=new HashMap<String,Object>();
                    if(StringUtil.notEmpty(task.getJob_data())){
                        jobDataMap=(Map) JSONObject.parse(task.getJob_data());
                    }
                    quartzManager.addJob(quartzManager.getClass(task.getBean_class()),task.getJob_name(),task.getJob_group(),task.getCron_expression(),task.getDescription(),jobDataMap);

                    //System.out.println(" task:"+task.toString()+"启动");
                    //更新状态为1
                    taskMapper.updateJobStatus(task.getId().toString(),"1");
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(" task:"+task.toString()+"启动异常!");
                    //更新状态为0
                    taskMapper.updateJobStatus(task.getId().toString(),"0");
                }
            }
        }
    }
}
