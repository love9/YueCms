package com.makbro.core.config;


import com.makbro.core.base.login.service.SsoUserServiceImpl;
import com.makbro.core.framework.sso.server.SsoUserService;
import com.makbro.core.framework.sso.server.service.DemoUserServiceImpl;
import com.markbro.sso.core.config.SsoBaseConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 单点登录bean的配置。
 *
 */
@Configuration
@PropertySource(value = "config/sso.properties")
public class SsoConfig extends SsoBaseConfig {

    /*******配置 SsoUserService********/
    @ConditionalOnProperty(name = "sso.server.bean.SsoUserService", havingValue = "SsoUserServiceImpl")
    @Bean("ssoUserService")
    public SsoUserService getSsoUserService(){
        log.info(">>>>>>>>>>> create SsoUserService,return new SsoUserServiceImpl();");
        return new SsoUserServiceImpl();
    }
    @ConditionalOnMissingBean(SsoUserService .class)
    @Bean("ssoUserService")
    public SsoUserService getDemoSsoUserService(){
        log.info(">>>>>>>>>>> create SsoUserService,return new DemoUserServiceImpl();");
        return new DemoUserServiceImpl();
    }
}
