package com.makbro.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 程序启动后通过ApplicationRunner处理一些事务
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @website https://www.zhyd.me
 * @date 2018/6/6 16:07
 * @since 1.0
 */

@Component
public class MyApplicationRunner implements ApplicationRunner {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    MyIpConfiguration myIpConfiguration;
    @Override
    public void run(ApplicationArguments applicationArguments) {
        InetAddress address = null;
        try {
            address = InetAddress.getLocalHost();
            logger.info("项目部署完成，项目访问地址：http://"+address.getHostAddress()+":" + myIpConfiguration.getPort());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
