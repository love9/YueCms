package com.makbro.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

/**
 * 数据源配置
 */
@Configuration
@PropertySource(value = "config/multiDataSource.properties")
public class DataSourceConfig {
    protected   final Logger log= LoggerFactory.getLogger(getClass());
    @Value("${spring.datasource.type}")
    private Class<? extends DataSource> dataSourceType;

    @Bean(name = "writeDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource getWriteDataSource() {
        log.info("-------------------- writeDataSource init ---------------------");
        DataSource dataSource= DataSourceBuilder.create().type(dataSourceType).build();
        return dataSource;
    }
    @Bean(name = "readDataSource")
    @ConfigurationProperties(prefix = "datasource.slave")

    public DataSource getReadDataSource() {
        log.info("-------------------- readDataSourceOne init --------------------");
        return DataSourceBuilder.create().type(dataSourceType).build();
    }


   /* @Bean(name = "dataSource")
    public DataSource dynamicDataSource() {
        ChooseDataSource dynamicDataSource = new ChooseDataSource();
        // 默认数据源
        dynamicDataSource.setDefaultTargetDataSource(getWriteDataSource());
        // 配置多数据源
        Map<Object, Object> dsMap = new HashMap();
        dsMap.put("write", getWriteDataSource());
        dsMap.put("read", getReadDataSource());
        dynamicDataSource.setTargetDataSources(dsMap);
        Map<String, String> methodTypeMap = new HashMap();
        methodTypeMap.put("write", ",add*,update*,updateBy*,delete*,remove*,insert*,create,update,delete,remove,save*,");
        methodTypeMap.put("read", ",get,getMap,find,findByMap,findBy*,query*,tree,select,");
        dynamicDataSource.setMethodType(methodTypeMap);
        return dynamicDataSource;
    }
*/




}
