package com.makbro.core.config;


import com.makbro.core.framework.quartz.quartz.JobFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
@EnableScheduling//启用支持springTask
public class QuartzConfig {

    @Bean("jobFactory")
    public JobFactory getJobFactory(){
        return new JobFactory();
    }

    @Bean("schedulerFactoryBean")
    public SchedulerFactoryBean getSchedulerFactoryBean(){
        SchedulerFactoryBean schedulerFactoryBean=new SchedulerFactoryBean();
        schedulerFactoryBean.setJobFactory(getJobFactory());
        schedulerFactoryBean.setOverwriteExistingJobs(true);
        schedulerFactoryBean.setStartupDelay(20);
        return schedulerFactoryBean;
    }

    /*@Bean("scheduler")
    public Scheduler scheduler() {
        return getSchedulerFactoryBean().getScheduler();
    }*/

    /*@Bean("quartzManager")
    public QuartzManager getQuartzManager(){
        QuartzManager quartzManager=new QuartzManager();
        quartzManager.setScheduler(scheduler());
        return quartzManager;
    }*/


}
