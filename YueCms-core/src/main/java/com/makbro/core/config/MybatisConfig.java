package com.makbro.core.config;

import com.github.miemiedev.mybatis.paginator.OffsetLimitInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class MybatisConfig  implements TransactionManagementConfigurer {
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;
    @Bean
    public OffsetLimitInterceptor getOffsetLimitInterceptor(){
        OffsetLimitInterceptor interceptor =new OffsetLimitInterceptor();
        interceptor.setDialectClass("com.github.miemiedev.mybatis.paginator.dialect.MySQLDialect");
        interceptor.setAsyncTotalCount(false);
        return interceptor;
    }

   /* @Bean
    public ConfigurationCustomizer getConfigurationCustomizer(){//这种写法，使用自己配置的数据源不管用
        return new ConfigurationCustomizer() {
            @Override
            public void customize(org.apache.ibatis.session.Configuration configuration) {
                configuration.setCacheEnabled(false);
                configuration.setJdbcTypeForNull(null);
                configuration.setLazyLoadingEnabled(true);
                configuration.setAggressiveLazyLoading(false);
                configuration.addInterceptor(getOffsetLimitInterceptor());
            }
        };
    }*/
    @Bean(name = "sqlSessionFactory")
    @ConfigurationProperties(prefix = "mybatis")
    public SqlSessionFactory sqlSessionFactoryBean() {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        // 配置数据源，此处配置为关键配置，如果没有将 dynamicDataSource 作为数据源则不能实现切换
        sqlSessionFactoryBean.setDataSource(dataSource);
        //添加插件
        sqlSessionFactoryBean.setPlugins(new Interceptor[]{getOffsetLimitInterceptor()});
        sqlSessionFactoryBean.setTypeAliasesPackage("com.markbro.core");
        //添加XML目录
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:com/markbro/**/*Mapper.xml"));
            return sqlSessionFactoryBean.getObject();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Bean
    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return new DataSourceTransactionManager(dataSource);
    }
}
