package com.makbro.core.config;


import com.makbro.core.framework.CustomJsonMapper;
import com.makbro.core.framework.DefaultExceptionHandler;
import com.makbro.core.framework.interceptor.*;
import com.markbro.base.utils.GlobalConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Configuration
class MyWebMvcConfigurer extends WebMvcConfigurationSupport {
    protected   final Logger log= LoggerFactory.getLogger(getClass());

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/resources/");
        registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");
        registry.addResourceHandler("/uploadresources/**").addResourceLocations("file:D:/dzd/uploadresources/");//虚拟路径与相对应的本地路径映射
        super.addResourceHandlers(registry);
    }


    @Bean
    public RequestFilterInterceptor getRequestFilterInterceptor(){
        String[] allowUrls=new String[]{"/","/index","/reg","/servlet/veryCode","/sys/cache","/sys/developer","/front/**","/logout","/login","/doc","/admin/login","/admin/logout","/test/**"};
        return new RequestFilterInterceptor(allowUrls);
    }
    @ConditionalOnProperty(name = "sso.enable", havingValue = "true")
    @Bean
    public SsoRequestFilterInterceptor getSsoRequestFilterInterceptor(){
        String[] allowUrls=new String[]{"/","/index","/reg","/servlet/veryCode","/sys/cache","/sys/developer","/front/**","/logout","/login","/doc","/admin/login","/admin/logout","/test/**"};
        return new SsoRequestFilterInterceptor(allowUrls);
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //分页拦截器
        String[] commonExcludePath=new String[]{"/sso/**","/*.ico","/resources/**","/uploadresources/**","/front/**","/error","/static"};
        //新建MyPageListAttrHandlerInterceptor替代原来的PageListAttrHandlerInterceptor，是因为原来的在返回ModelAndView获取Slider分页条时异常
        registry.addInterceptor(new MyPageListAttrHandlerInterceptor()).excludePathPatterns(commonExcludePath).addPathPatterns("/**");
        registry.addInterceptor(new MonitorInterceptor()).excludePathPatterns(commonExcludePath).addPathPatterns("/**");
        registry.addInterceptor(new MalevolenceInterceptor(new String[]{"/front/static"})).addPathPatterns("/myblog/page/view");

        //防止用户重复提交表单
        //1.在需要生成token（通常是要点击提交得那个页面）的Controller中的方法上面使用@FormToken注解.@FormToken注解(get=true)
        // 2.只要通过这个方法跳向得页面都是随机生成一个token,这个token放置到表单隐藏域中，然后表单提交的时候把该token一同提交后台，处理保存表单的后台方法上加入@FormToken注解(remove=true)
        registry.addInterceptor(new FormTokenInterceptor()).excludePathPatterns(commonExcludePath).addPathPatterns("/**");

        if(GlobalConfig.SSO_ENABLE){
            registry.addInterceptor(getSsoRequestFilterInterceptor()).excludePathPatterns(commonExcludePath).addPathPatterns("/**");
        }else{
            registry.addInterceptor(getRequestFilterInterceptor()).excludePathPatterns(commonExcludePath).addPathPatterns("/**");
        }
        super.addInterceptors(registry);

    }
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        StringHttpMessageConverter converter1= new StringHttpMessageConverter(Charset.forName("UTF-8"));
        converters.add(converter1);

        MappingJackson2HttpMessageConverter converter=new MappingJackson2HttpMessageConverter();
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        converter.setSupportedMediaTypes(fastMediaTypes);
        converter.setObjectMapper(new CustomJsonMapper());//mybatis分页返回JSON结果处理 PageListJsonMapper
        converter.setPrettyPrint(true);
        converters.add(converter);
    }

    //注册统一异常处理
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        exceptionResolvers.add(new DefaultExceptionHandler());
    }

    //嵌入式Servlet容器的配置
   /* @Bean
    public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer(){
        return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
            @Override
            public void customize(ConfigurableWebServerFactory factory) {
                ErrorPage errorPage400 = new ErrorPage(HttpStatus.BAD_REQUEST,"/error/400.html");
                ErrorPage errorPage404 = new ErrorPage(HttpStatus.NOT_FOUND,"/error/404.html");
                ErrorPage errorPage500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500.html");
                factory.addErrorPages();
            }
        };
    }*/

}
