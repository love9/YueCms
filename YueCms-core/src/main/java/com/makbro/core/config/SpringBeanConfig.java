package com.makbro.core.config;


import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.applicationEvent.ApplicationEventAspect;
import com.makbro.core.framework.applicationEvent.ApplicationEventManager;
import com.makbro.core.framework.applicationEvent.IApplicationEvent;
import com.makbro.core.framework.applicationEvent.eventLog.service.EventLogService;
import com.markbro.base.utils.SpringContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class SpringBeanConfig {

    @Bean("springContextHolder")
    public SpringContextHolder getSpringContextHolder(){
        SpringContextHolder springContextHolder=new SpringContextHolder();
        return springContextHolder;
    }

    @Bean("keyService")
    public TableKeyService getTableKeyService(){
        TableKeyService service= new TableKeyService();
        service.setKey_name("common");
        return service;
    }
    @Bean("bmKeyService")
    public TableKeyService getTableKeyServiceBm(){
        TableKeyService service= new TableKeyService();
        service.setKey_name("org_department");
        return service;
    }
    @Bean("gwKeyService")
    public TableKeyService getTableKeyServiceGW(){
        TableKeyService service= new TableKeyService();
        service.setKey_name("org_position");
        return service;
    }
    @Bean("yhKeyService")
    public TableKeyService getTableKeyServiceYH(){
        TableKeyService service= new TableKeyService();
        service.setKey_name("sys_user");
        return service;
    }

    @Autowired
    List<IApplicationEvent> applicationEventList;//所有实现系统事件类
    @Autowired
    EventLogService eventLogService;//系统事件日志记录

    /**
     * 系统事件管理器
     * @return
     */
    @Bean("applicationEventManager")
    public ApplicationEventManager getApplicationEventManager(){
        ApplicationEventManager applicationEventManager =new ApplicationEventManager();
        applicationEventManager.setApplicationEventList(applicationEventList);
        applicationEventManager.setEventLogService(eventLogService);
        return applicationEventManager;
    }

    /**
     * 系统事件触发切面类
     */
    @Bean("applicationEventAspect")
    public ApplicationEventAspect getApplicationEventAspect(){
        ApplicationEventAspect applicationEventAspect=new ApplicationEventAspect();
        applicationEventAspect.setApplicationEventManager(getApplicationEventManager());
        return applicationEventAspect;
    }


}
