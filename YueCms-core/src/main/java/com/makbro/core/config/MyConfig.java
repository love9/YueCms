package com.makbro.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = {"config/my.properties","config/thirdParty.properties"})
public class MyConfig {
    protected   final Logger log= LoggerFactory.getLogger(getClass());
    public static String TENCENT_CAPTCHA_APP_ID;
    public static String TENCENT_CAPTCHA_APP_SECRET_KEY;
    public static String CREATE_USER_ROLE_ID;
    public static String CREATE_USER_POSITION_ID;
    public static String TOKEN_TIMEOUT;

    /*由于Spring Boot不支持静态变量的自动注入，所以需要使用一个非静态的setter方法将通过@Value注解获取到的属性信息赋值给对应静态变量*/
    @Value("${tencentCaptcha.appId}")
    public  void setTencentCaptcha_appId(String tencentCaptcha_appId) {
        TENCENT_CAPTCHA_APP_ID = tencentCaptcha_appId;
    }
    @Value("${tencentCaptcha.appSecretKey}")
    public  void setTencentCaptcha_appSecretKey(String tencentCaptcha_appSecretKey) {
        TENCENT_CAPTCHA_APP_SECRET_KEY = tencentCaptcha_appSecretKey;
    }
    @Value("${third_login_create_user_roleId}")
    public  void setCreate_user_roleId(String create_user_roleId) {
        CREATE_USER_ROLE_ID = create_user_roleId;
    }
    @Value("${third_login_create_user_positionId}")
    public static void setCreate_user_positionId(String create_user_positionId) {
        CREATE_USER_POSITION_ID = create_user_positionId;
    }
    @Value("${token_timeout}")
    public  void setToken_timeout(String token_timeout) {
        TOKEN_TIMEOUT = token_timeout;
    }

}
