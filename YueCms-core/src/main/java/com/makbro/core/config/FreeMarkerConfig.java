package com.makbro.core.config;


import com.makbro.core.framework.freemark.tag.AuthTag;
import com.makbro.core.framework.freemark.tag.CustomTags;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.PropertiesListenerConfig;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 设置freemark全局变量。注意他的加载顺序要在MyStartupRunner.java之后。
 */
@Component
@Order(value = 2)
public class FreeMarkerConfig implements CommandLineRunner {

    @Autowired
    protected freemarker.template.Configuration configuration;
    @Autowired
    AuthTag authTag;
    @Autowired
    CustomTags customTags;
    /**
     * 添加自定义标签
     */

    @Override
    public void run(String... args) throws Exception {
        try {
            configuration.setSharedVariable("myTag", customTags);
            configuration.setSharedVariable("authTag", authTag);
            //注意：这些配置文件值当且仅当程序启动时才会初始化一次。
            //把加载到的配置文件值放到freemark公共变量池中，那么再前台页面可以用类似EL表达式取值
            configuration.setSharedVariable("properties", PropertiesListenerConfig.getAllProperty());
            //表sys_para的配置
            configuration.setSharedVariable("sys", GlobalConfig.getAllSysPara());
            //字典
            configuration.setSharedVariable("dict", GlobalConfig.getAllDict());
        } catch (TemplateModelException e) {
            e.printStackTrace();
        }
    }
}
