package com.makbro.core.framework.listener;

/**
 * 本系统自己实现的会话的监听器
 */
public interface ITokenListener {
    public void tokenCreate(Object event, Object arg);
    public void tokenDelete(Object event, Object arg);
}
