package com.makbro.core.framework.authz.annotationInterceptorImpl;


import com.makbro.core.framework.authz.annotationHandler.AbstractAuthorizingAnnotationHandler;
import com.makbro.core.framework.authz.common.AnnotationInterceptor;
import com.makbro.core.framework.authz.common.AnnotationResolver;
import com.makbro.core.framework.authz.common.AuthorizingInterceptor;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.markbro.base.exception.ApplicationException;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * 注解拦截
 */
public abstract class AbstractAuthorizingAnnotationInterceptor extends AnnotationInterceptor implements AuthorizingInterceptor {


    public AbstractAuthorizingAnnotationInterceptor(AbstractAuthorizingAnnotationHandler handler) {
        super(handler);
    }

    public AbstractAuthorizingAnnotationInterceptor(AbstractAuthorizingAnnotationHandler handler, AnnotationResolver resolver) {
        super(handler, resolver);
    }

    public void invoke(HttpServletRequest request, HandlerMethod method, AuthorizingSubject subject) throws Throwable {
        this.assertAuthorized(request,method,subject);
    }
    public boolean assertAuthorized(HttpServletRequest request,HandlerMethod mi,AuthorizingSubject subject) throws ApplicationException {
        try {
            return  ((AbstractAuthorizingAnnotationHandler)this.getHandler()).assertAuthorized(request,this.getAnnotation(mi),subject);
        } catch (ApplicationException ex) {
            if(ex.getCause() == null) {
                ex.initCause(new ApplicationException("Not authorized to invoke method: " + mi.getMethod()));
            }
            throw ex;
        }
    }
    public boolean supports(HandlerMethod mi) {
        return this.getAnnotation(mi) != null;
    }

}
