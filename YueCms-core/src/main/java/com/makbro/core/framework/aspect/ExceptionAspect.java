package com.makbro.core.framework.aspect;


import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.exception.ForbiddenException;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 异常切面（用来处理webController和Service层面抛出异常后的动作，如记录日志，打印堆栈信息）
 * 
 * @author wujiyue
 * @version 2017年11月20日14:17:15
 */
@Aspect
@Component
@EnableAspectJAutoProxy(proxyTargetClass = false)
public class ExceptionAspect {
	private  Logger logger = LoggerFactory.getLogger(getClass());

	@Pointcut("execution(* com.markbro..web..*.*(..))")
	public void controllerException() {
	}
	@AfterThrowing(pointcut = "controllerException()",throwing = "ex")
	public void afterThrowsControllerException(Throwable ex){
		logger.info("====afterThrowsControllerException===");
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		Map paramMap= MyBatisRequestUtil.requestToMap(request);
		String exmsg=ex.getMessage();
		logger.error("请求url{}【Controller层面】发生了异常：参数{},异常信息：{}",request.getRequestURI(),paramMap.toString(),exmsg);
		ex.printStackTrace();
		if(exmsg.contains("java.util.ArrayList cannot be cast to com.github.miemiedev.mybatis.paginator.domain.PageList")){
			//这种错误情况，是用户尝试直接浏览器或ajax请求访问分页数据接口。这是不允许的。
			throw new ForbiddenException("非法的请求!");//注意，这种提示非法请求并不是用户真的没有权限，而是防止用户通过分析出来数据接口恶意趴取数据而制造的假象提示。
		}else{
			throw new ApplicationException(exmsg);
		}

	}
}
