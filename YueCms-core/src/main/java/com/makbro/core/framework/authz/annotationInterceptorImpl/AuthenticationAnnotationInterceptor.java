package com.makbro.core.framework.authz.annotationInterceptorImpl;


import com.makbro.core.framework.authz.annotationHandler.AuthenticationAnnotationHandler;
import com.makbro.core.framework.authz.common.AnnotationResolver;

/**
 * Created by Administrator on 2018/12/5.
 */
public class AuthenticationAnnotationInterceptor extends AbstractAuthorizingAnnotationInterceptor {

    public AuthenticationAnnotationInterceptor() {
        super(new AuthenticationAnnotationHandler());
    }

    public AuthenticationAnnotationInterceptor(AnnotationResolver resolver) {
        super(new AuthenticationAnnotationHandler(), resolver);
    }
}
