package com.makbro.core.framework;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.google.common.collect.Maps;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseController {
    /**
     * 日志对象
     */
    protected Logger logger = LoggerFactory.getLogger(getClass());
    protected String basePath="";
    protected HttpServletResponse response;
    protected HttpServletRequest request;
    protected HttpSession session;
    protected Map resultMap= Maps.newConcurrentMap();


    @ModelAttribute
    public void init(HttpServletRequest request,HttpServletResponse response,HttpSession session){
        this.request=request;
        this.response=response;
        this.session=session;
    }

    protected Map getPageMap(List<?> list){

            PageList ls=(PageList)list;
            Paginator paginator= ls.getPaginator();
            Map resultMap=new HashMap();
            resultMap.put(TmConstant.PAGE_TOTAL_KEY,paginator.getTotalCount());
            resultMap.put(TmConstant.PAGE_TOTAL_PAGES_KEY,paginator.getTotalPages());
            resultMap.put(TmConstant.PAGE_PAGE_KEY, paginator.getPage());
            resultMap.put(TmConstant.PAGE_ROWS_KEY,list);
            return resultMap;
    }

    //把登陆用户信息传递到前台
    protected void pushLoginUserInfo(Model model){
        LoginBean lb= MyBatisRequestUtil.getLoginUserInfo(request);
        if(lb!=null){
            List<Map<String,String>> rolelist=lb.getJsList();
            List<Map<String,String>> bmlist=lb.getBmList();
            List<Map<String,String>> gwlist=lb.getGwList();
            if(rolelist!=null&&rolelist.size()>0){
                String jsstr="";
                for (Map<String,String> t:rolelist){
                    jsstr+=t.get(TmConstant.MC_KEY)+"|";
                }
                model.addAttribute(TmConstant.ROLE_KEY,jsstr);
            }else{
                model.addAttribute(TmConstant.ROLE_KEY,"");
            }
            if(bmlist!=null&&bmlist.size()>0){
                String bmstr="";
                for (Map<String,String> t:bmlist){
                    bmstr+=t.get(TmConstant.MC_KEY)+"|";
                }
                model.addAttribute(TmConstant.BM_KEY,bmstr);
            }else{
                model.addAttribute(TmConstant.BM_KEY,"");
            }
            if(gwlist!=null&&gwlist.size()>0){
                String gwstr="";
                for (Map<String,String> t:gwlist){
                    gwstr+=t.get(TmConstant.MC_KEY)+"|";
                }
                model.addAttribute(TmConstant.GW_KEY,gwstr);
            }else{
                model.addAttribute(TmConstant.GW_KEY,"");
            }
            if(lb.isAdmin()){
                model.addAttribute("isAdmin",TmConstant.TRUE_FLAG_STR);
            }
            model.addAttribute(TmConstant.YHID_KEY,lb.getYhid());
            model.addAttribute(TmConstant.YHMC_KEY,lb.getXm());
            String headerPath = (String) EhCacheUtils.getUserInfo("headerPath", lb.getYhid());
            model.addAttribute("headerPath", headerPath);
        }else{
            logger.error("请求URL"+request.getRequestURI()+"时推送用户信息到前台页面错误:LoginBean is Null!");
        }
    }
    protected PageBounds getPageBounds(){

        String page = getParam(TmConstant.PAGE_PAGE_KEY);
        String limit = getParam(TmConstant.PAGE_ROWS_KEY);
        String sort = getParam(TmConstant.PAGE_SORT_KEY);
        String dir = getParam(TmConstant.PAGE_DIR_KEY);
        page= StringUtil.isEmpty(page)?"1":page;
        limit= StringUtil.isEmpty(limit)?"10":limit;

        PageBounds  pageBounds =null;
        //if(StringUtil.notEmpty(page) && StringUtil.notEmpty(limit)){
        pageBounds=new PageBounds(Integer.valueOf(page),Integer.valueOf(limit));//当2个以上参数的构造函数返回的List 才是PageList
       /* }else if(StringUtil.notEmpty(limit)){
            pageBounds=new PageBounds(Integer.valueOf(limit));
        }else{
            pageBounds = new PageBounds();
        }*/

        if(StringUtil.notEmpty(sort) && StringUtil.notEmpty(dir)){
            pageBounds.getOrders().add(Order.create(sort, dir));
        }else if(StringUtil.notEmpty(sort)){
            pageBounds.getOrders().add(Order.create(sort, "asc"));
        }
        pageBounds.setAsyncTotalCount(false);
        return pageBounds;
    }



    protected String getParam(String key){
        return request.getParameter(key);
    }

    protected String getFullUrl(){
        String scheme = request.getScheme();             // http
        String serverName = request.getServerName();     // hostname.com
        int serverPort = request.getServerPort();        // 80
        String contextPath = request.getContextPath();

        StringBuffer url =  new StringBuffer();
        url.append(scheme).append("://").append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url.append(":").append(serverPort);
        }
        url.append("/").append(contextPath);
        return url.toString()+request.getRequestURI();
    }
    protected String getFullUrlAndParams(){
        String scheme = request.getScheme();             // http
        String serverName = request.getServerName();     // hostname.com
        int serverPort = request.getServerPort();        // 80
        String contextPath = request.getContextPath();

        StringBuffer url =  new StringBuffer();
        url.append(scheme).append("://").append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url.append(":").append(serverPort);
        }
        url.append("/").append(contextPath);
        String res=url.toString()+request.getRequestURI();
        String qs = request.getQueryString(); // 参数
        if (qs != null) {
            res = res + "?" + (request.getQueryString());
        }
        return res;
    }
    protected String getServerContextPath(){
        String scheme = request.getScheme();             // http
        String serverName = request.getServerName();     // hostname.com
        int serverPort = request.getServerPort();        // 80
        String contextPath = request.getContextPath();

        StringBuffer url =  new StringBuffer();
        url.append(scheme).append("://").append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url.append(":").append(serverPort);
        }
        url.append("/").append(contextPath);
        return url.toString();
    }

}
