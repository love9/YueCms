package com.makbro.core.framework.quartz.quartz;


import com.markbro.base.utils.test.TestUtil;
import org.quartz.*;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class QuartzManager {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private Scheduler scheduler;

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public static Class getClass(String className){
        try {
           Class cl= Class.forName(className).newInstance().getClass();
            return cl;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 增加一个job
     * @param jobClass 任务实现类
     * @param jobName 任务名称
     * @param jobGroupName 任务组名
     * @param cron 时间表达式 （如：0/5 * * * * ? ）
     * @param description 任务描述
     * @param jobDataMap
     */
    public  void addJob(Class<? extends Job> jobClass, String jobName,String jobGroupName,String cron,String description,Map jobDataMap) throws Exception{
       // try {
            Assert.notNull(jobClass,"任务启动失败!jobClass为空!");
            //创建jobDetail实例，绑定Job实现类
            //指明job的名称，所在组的名称，以及绑定job类
            JobDetail jobDetail = JobBuilder.newJob(jobClass).withDescription(description).setJobData(new JobDataMap(jobDataMap))
                    .withIdentity(jobName, jobGroupName)//任务名称和组构成任务key
                    .build();
            //定义调度触发规则
            //使用cornTrigger规则
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(jobName,jobGroupName)//触发器key
                    .startAt(DateBuilder.futureDate(1, IntervalUnit.SECOND))
                    .withSchedule(CronScheduleBuilder.cronSchedule(cron))
                    .startNow().build();
            //把作业和触发器注册到任务调度中
            scheduler.scheduleJob(jobDetail, trigger);
            // 启动
            if (!scheduler.isShutdown()) {
                scheduler.start();
            }
       /* } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /**
     * 增加一个job
     * @param jobClass  任务实现类
     * @param jobName  任务名称
     * @param jobGroupName 任务组名
     * @param secondsInterval  时间表达式 (这是每隔多少秒为一次任务)
     */
    public void addJob(Class<? extends Job> jobClass, String jobName,String jobGroupName,int secondsInterval){
        addJob(jobClass,jobName,jobGroupName,secondsInterval,-1);
    }

    /**
     * 增加一个job
     * @param jobClass 任务实现类
     * @param jobName  任务名称
     * @param jobGroupName 任务组名
     * @param secondsInterval  时间表达式 (这是每隔多少秒为一次任务)
     * @param jobTimes  运行的次数 （<0:表示不限次数）
     */
    public void addJob(Class<? extends Job> jobClass, String jobName,String jobGroupName,int secondsInterval,int jobTimes){
        try {
            JobDetail jobDetail = JobBuilder.newJob(jobClass)
                    .withIdentity(jobName, jobGroupName)//任务名称和组构成任务key
                    .build();
            //使用simpleTrigger规则
            Trigger trigger=null;
            if(jobTimes<0){
                trigger=TriggerBuilder.newTrigger().withIdentity(jobName, jobGroupName)
                        .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(1).withIntervalInSeconds(secondsInterval))
                        .startNow().build();
            }else{
                trigger=TriggerBuilder.newTrigger().withIdentity(jobName, jobGroupName)
                        .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(1).withIntervalInSeconds(secondsInterval).withRepeatCount(jobTimes))
                        .startNow().build();
            }

            scheduler.scheduleJob(jobDetail, trigger);
            if (!scheduler.isShutdown()) {
                scheduler.start();
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改 一个job的 时间表达式
     * @param jobName
     * @param jobGroupName
     * @param cron
     */
    public boolean updateJob(String jobName,String jobGroupName,String cron){
        try {

            TriggerKey triggerKey = TriggerKey.triggerKey(jobName,jobGroupName);
            if(scheduler.checkExists(triggerKey)){
                logger.error(">>>>>>>>>>>updateJob>>>>>>triggerKey:"+triggerKey.getName()+" not exists!");
                return false;
            }
            CronTrigger oldTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
                //任务存在直接更新一下
            if (oldTrigger != null) {
                // avoid repeat
                String oldCron = oldTrigger.getCronExpression();
                if (oldCron.equals(cron)) {
                    return true;
                }
                CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cron).withMisfireHandlingInstructionDoNothing();
                oldTrigger = oldTrigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(cronScheduleBuilder).build();
                scheduler.rescheduleJob(triggerKey, oldTrigger);
                logger.info(">>updateJob=success=>jobGroupName:{},jobName:{}, cron:{}",jobGroupName,jobName, cron);
                return true;
            }else{
                return false;
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除任务一个job
     * @param jobName 任务名称
     * @param jobGroupName 任务组名
     */
    public  void deleteJob(String jobName,String jobGroupName) {
        try {
            scheduler.deleteJob(new JobKey(jobName, jobGroupName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除任务(这是从触发器层面删除任务。区别于上面的deleteJob)
     * @param jobName
     * @param jobGroupName
     * @return
     */
    public boolean unscheduleJob(String jobName,String jobGroupName){
        try {
            TriggerKey triggerKey =  TriggerKey.triggerKey(jobName, jobGroupName);
            scheduler.unscheduleJob(triggerKey);
            return true;
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 暂停一个任务
     * @param jobName
     * @param jobGroupName
     */
    public void pauseJob(String jobName,String jobGroupName){
        try {
            JobKey jobKey = JobKey.jobKey(jobName, jobGroupName);
            scheduler.pauseJob(jobKey);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
    /**
     * 恢复一个任务
     * @param jobName
     * @param jobGroupName
     */
    public void resumeJob(String jobName,String jobGroupName){
        try {
            JobKey jobKey = JobKey.jobKey(jobName, jobGroupName);
            scheduler.resumeJob(jobKey);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }



    /**
     * 立即执行一个job
     * @param jobName
     * @param jobGroupName
     */
    public void runAJobNow(String jobName,String jobGroupName) throws SchedulerException{
        try {
            JobKey jobKey = JobKey.jobKey(jobName, jobGroupName);
            scheduler.triggerJob(jobKey);
        }catch (SchedulerException e) {
            //e.printStackTrace();
            throw e;
        }
    }
    public static void main(String[] args){
        List<String> list=getRecentTriggerTime("0 0/5 * * * ?",5);
        TestUtil.printList(list);
    }
    /**
     *
     * @param cronExpression    cron表达式
     * @param numTimes    下(几)次运行的时间
     * @return
     */
    public static List<String> getRecentTriggerTime(String cronExpression,Integer numTimes) {
        List<String> list = new ArrayList<>();
        CronTriggerImpl cronTriggerImpl = new CronTriggerImpl();
        try {
            cronTriggerImpl.setCronExpression(cronExpression);
        } catch(ParseException e) {
            e.printStackTrace();
        }
        // 这个是重点，一行代码搞定
        List<Date> dates = TriggerUtils.computeFireTimes(cronTriggerImpl, null, numTimes);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Date date : dates) {
            list.add(dateFormat.format(date));
        }
        return list;
    }

    /**
     * 获得某任务某个触发器当前运行状态
     * @param jobName
     * @param jobGroupName
     * @return
     */
    public Trigger.TriggerState getJob_status(String jobName,String jobGroupName){

        try {
            TriggerKey triggerKey=TriggerKey.triggerKey(jobName,jobGroupName);
            Trigger.TriggerState triggerState=scheduler.getTriggerState(triggerKey);
            return triggerState;
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取所有计划中的任务列表
     * @return
     */
    public List<Map<String,Object>> queryAllJob(){
        List<Map<String,Object>> jobList=null;
        try {
            GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
            Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
            jobList = new ArrayList<Map<String,Object>>();
            for (JobKey jobKey : jobKeys) {
                List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
                for (Trigger trigger : triggers) {
                    Map<String,Object> map=new HashMap<>();
                    map.put("job_name",jobKey.getName());
                    map.put("job_group",jobKey.getGroup());

                    //map.put("description","触发器:" + trigger.getKey());
                    Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
                    map.put("job_status",triggerState);
                    if (trigger instanceof CronTrigger) {
                        CronTrigger cronTrigger = (CronTrigger) trigger;
                        String cronExpression = cronTrigger.getCronExpression();
                        map.put("cron_expression",cronExpression);
                    }
                    jobList.add(map);
                }
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return jobList;
    }

    /**
     * 获取所有正在运行的job(这里是指任务在执行中，如果方法执行耗时很短，可能每次获取都是空的)
     * @return
     */
    public List<Map<String,Object>> queryRunJon(){
        List<Map<String,Object>> jobList=null;
        try {
            List<JobExecutionContext> executingJobs = scheduler.getCurrentlyExecutingJobs();
            jobList = new ArrayList<Map<String,Object>>(executingJobs.size());
            for (JobExecutionContext executingJob : executingJobs) {
                Map<String,Object> map=new HashMap<String, Object>();
                JobDetail jobDetail = executingJob.getJobDetail();
                JobKey jobKey = jobDetail.getKey();
                Trigger trigger = executingJob.getTrigger();
                map.put("job_name",jobKey.getName());
                map.put("job_group",jobKey.getGroup());
                Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
                map.put("job_status",triggerState);
                if (trigger instanceof CronTrigger) {
                    CronTrigger cronTrigger = (CronTrigger) trigger;
                    String cronExpression = cronTrigger.getCronExpression();
                    map.put("cron_expression",cronExpression);
                }
                jobList.add(map);
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return jobList;
    }
}