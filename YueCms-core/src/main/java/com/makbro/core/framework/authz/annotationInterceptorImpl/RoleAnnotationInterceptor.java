package com.makbro.core.framework.authz.annotationInterceptorImpl;


import com.makbro.core.framework.authz.annotationHandler.RoleAnnotationHandler;
import com.makbro.core.framework.authz.common.AnnotationResolver;

/**
 * Created by Administrator on 2018/12/5.
 */
public class RoleAnnotationInterceptor extends AbstractAuthorizingAnnotationInterceptor {
    public RoleAnnotationInterceptor() {
        super(new RoleAnnotationHandler());
    }

    public RoleAnnotationInterceptor(AnnotationResolver resolver) {
        super(new RoleAnnotationHandler(), resolver);
    }
}
