package com.makbro.core.framework;


import com.markbro.base.utils.SpringContextHolder;
import com.markbro.sso.core.SsoConf;
import com.markbro.sso.core.login.ISsoLoginHelper;
import com.markbro.sso.core.store.ISsoLoginStore;
import com.markbro.sso.core.store.ISsoSessionIdHelper;
import com.markbro.sso.core.user.SsoUser;
import com.markbro.sso.core.user.SsoUserInfo;
import com.markbro.sso.core.util.RequestContextHolderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * 该类除了login方法外，基本就是对ssoLoginHelper的再一层封装。在Controller中可以直接使用该类只需要调用一个方法。如果使用ssoLoginHelper则稍微有点麻烦。
 */
public class SsoHelper {

    protected static Logger log = LoggerFactory.getLogger(SsoHelper.class);

    static ISsoLoginStore ssoLoginStore = SpringContextHolder.getBean("ssoLoginStore");

    static ISsoSessionIdHelper ssoSessionIdHelper = SpringContextHolder.getBean("ssoSessionIdHelper");

    static ISsoLoginHelper ssoLoginHelper = SpringContextHolder.getBean("ssoLoginHelper");


    public static String login(SsoUserInfo userInfo, boolean ifRemember){

        HttpServletRequest request= RequestContextHolderUtil.getRequest();
        HttpServletResponse response = RequestContextHolderUtil.getResponse();
        //sso 单点登录
        // 1、make xxl-sso user
        SsoUser ssoUser = new SsoUser();
        ssoUser.setUserid(userInfo.getUserid());
        ssoUser.setUsername(userInfo.getUsername());
        ssoUser.setVersion(UUID.randomUUID().toString().replaceAll("-", ""));
        ssoUser.setExpireMinite(ssoLoginStore.getExpireMinite());
        ssoUser.setExpireFreshTime(System.currentTimeMillis());


        // 2、make session id
        String sessionId = ssoSessionIdHelper.makeSessionId(ssoUser);

        // 3、login, store storeKey + cookie sessionId
        ssoLoginHelper.login(request,response, sessionId, ssoUser, ifRemember);

        request.getSession().setAttribute(SsoConf.SSO_USER,ssoUser);
        return sessionId;
    }
    public static void logout(HttpServletRequest request,HttpServletResponse response){


        ssoLoginHelper.logout(request,response);
    }
    public static SsoUser loginChecked(HttpServletRequest request,HttpServletResponse response){

        return ssoLoginHelper.loginCheck(request,response);
    }

    public static boolean invalidSession(String sessionId){

        boolean flag = ssoLoginHelper.invalidSession(sessionId);
        return flag;
    }
}
