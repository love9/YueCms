package com.makbro.core.framework.mysession.bean;


import com.markbro.base.common.util.DesUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ApplicationException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;


public class Token {
	private static final String token_key="token_ke";
	//通过随机guid串和数据库配置的固定token串经过一次md5加密构造出一个token串
	public static String getToken(String str){
		try {
			return  DesUtil.encrypt(str,token_key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("生成Token出现异常!请联系管理员!");
		}
	}
	public static String genToken(String yhid){
		try {
			String s = yhid + "#" + System.currentTimeMillis();
			return Token.getToken(s);//生成token(用户名和时间戳des加密)
		} catch (Exception e) {
			e.printStackTrace(); //gentoken = Md5.getMd5(Guid.get() + "eqioz;d238*l");
			throw new ApplicationException("生成Token出现异常!请联系管理员!");
		}
	}
	//检验客户端的token是否有效(能否正确解密并且时间有效)
	public static int checkToken(String token){
		try {
			String s=DesUtil.decrypt(token, token_key);
			return TmConstant.TOKEN_OK;//1正常有效
		}catch (Exception ex){
			System.out.println("checkToken异常!"+ex.getMessage());
			return TmConstant.TOKEN_illegal;//-1解密异常，说明token无效
		}

	}

	private static final char[] legalChars2 = "ghijk67stuJKLM89rvwxyzAlmnOUV+/abcdefPQRSTopqCDEFG012345BHINWXYZ".toCharArray();//字典内顺序任意更改，只要内容保证不缺就行
	private static final char[] legalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefjhijklmnopqrstuvwxyz0123456789+/".toCharArray();
	private static HashMap<Character,Integer> hashDecode=new HashMap<Character,Integer>();
	/**
	 * data[]进行编码
	 *
	 * @param data
	 * @return
	 */
	public static String base64Encode(byte[] data) {
		int start = 0;
		int len = data.length;
		StringBuffer buf = new StringBuffer(data.length * 3 / 2);
		int end = len - 3;
		int i = start;
		int n = 0;
		while (i <= end) {
			int d = ((((int) data[i]) & 0x0ff) << 16)
					| ((((int) data[i + 1]) & 0x0ff) << 8)
					| (((int) data[i + 2]) & 0x0ff);
			buf.append(legalChars[(d >> 18) & 63]);
			buf.append(legalChars[(d >> 12) & 63]);
			buf.append(legalChars[(d >> 6) & 63]);
			buf.append(legalChars[d & 63]);
			i += 3;
			if (n++ >= 14) {
				n = 0;
				buf.append(" ");
			}
		}
		if (i == start + len - 2) {
			int d = ((((int) data[i]) & 0x0ff) << 16)
					| ((((int) data[i + 1]) & 255) << 8);
			buf.append(legalChars[(d >> 18) & 63]);
			buf.append(legalChars[(d >> 12) & 63]);
			buf.append(legalChars[(d >> 6) & 63]);
			buf.append("=");
		} else if (i == start + len - 1) {
			int d = (((int) data[i]) & 0x0ff) << 16;
			buf.append(legalChars[(d >> 18) & 63]);
			buf.append(legalChars[(d >> 12) & 63]);
			buf.append("==");
		}
		return buf.toString();
	}
	public static byte[] base64Decode(String s) {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			decode(s, bos);
		} catch (IOException e) {
			throw new RuntimeException();
		}
		byte[] decodedBytes = bos.toByteArray();
		try {
			bos.close();
			bos = null;
		} catch (IOException ex) {
			System.err.println("Error while decoding BASE64: " + ex.toString());
		}
		return decodedBytes;
	}
	private static void decode(String s, OutputStream os) throws IOException {
		int i = 0;

		int len = s.length();

		while (true) {
			while (i < len && s.charAt(i) <= ' ')
				i++;

			if (i == len)
				break;

			int tri = (decode(s.charAt(i)) << 18)
					+ (decode(s.charAt(i + 1)) << 12)
					+ (decode(s.charAt(i + 2)) << 6)
					+ (decode(s.charAt(i + 3)));

			os.write((tri >> 16) & 255);
			if (s.charAt(i + 2) == '=')
				break;
			os.write((tri >> 8) & 255);
			if (s.charAt(i + 3) == '=')
				break;
			os.write(tri & 255);

			i += 4;
		}
	}
	private static int decode(char c) {

		if(hashDecode.size()==0)
		{
			for(int i =0;i<64;i++)
			{ char ch = legalChars[i];
				hashDecode.put(ch, i);
			}
		}
		if(hashDecode.containsKey(c))
		{
			return hashDecode.get(c);
		}
		else if(c =='=')
			return 0;
		else
			throw new RuntimeException("unexpected code: " + c);
	}
	public static void main(String[] args) throws Exception {
		String s="w747506908198703211527926159";
		//String ss=encode(s.getBytes());
		//System.out.println(ss);
		//String sss=new String(decode(ss));
		//System.out.println(sss);
		String ss= DesUtil.encrypt(s,"12345678") ;
		System.out.println(ss);
		String sss= DesUtil.decrypt("13B34D5778277D454BFBD86CD0B71463", token_key) ;
		System.out.println(sss);
	}
}
