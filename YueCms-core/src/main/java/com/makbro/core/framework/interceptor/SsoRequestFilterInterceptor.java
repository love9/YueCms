package com.makbro.core.framework.interceptor;


import com.makbro.core.framework.authz.AuthorizingInterceptors;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.makbro.core.base.login.service.LoginService;
import com.makbro.core.framework.interceptor.path.impl.AntPathMatcher;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ForbiddenException;
import com.markbro.base.exception.NoLoginException;
import com.markbro.base.exception.UnAuthorizedException;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.sso.core.SsoConf;
import com.markbro.sso.core.login.ISsoLoginHelper;
import com.markbro.sso.core.user.SsoUser;
import com.markbro.sso.core.util.CookieUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 *  本系统整合Sso的请求过滤拦截器
 */
public class SsoRequestFilterInterceptor extends HandlerInterceptorAdapter {

    Logger log= LoggerFactory.getLogger(SsoRequestFilterInterceptor.class);
    private static final AntPathMatcher antPathMatcher = new AntPathMatcher();

    public String[] allowUrls;

    public SsoRequestFilterInterceptor(String[] allowUrls) {
        this.allowUrls = allowUrls;
    }

    @Autowired
    LoginService loginService;


    @Autowired
    ISsoLoginHelper ssoLoginHelper;



    private boolean isAjax(HttpServletRequest request, HandlerMethod handlerMethod){
        String header = request.getHeader("content-type");
        boolean isJson=  header!=null && header.contains("json");
        if(isJson||(handlerMethod!=null&&handlerMethod.getMethodAnnotation(ResponseBody.class) != null)||((request.getHeader("accept").contains("application/json")  || (request.getHeader("X-Requested-With")!= null && request
                .getHeader("X-Requested-With").contains("XMLHttpRequest") )))){
            return true;
        }
        return false;
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        request.setCharacterEncoding("UTF-8");
        String uri = request.getServletPath();
        if(!(handler instanceof HandlerMethod) ){
            return true;
        }
        HandlerMethod handlerMethod=(HandlerMethod) handler;
        /*if(uri.equals(loginPath)||uri.equals(logoutPath)){
            return true;
        }*/
        if(isStaticResources(uri) || urlMatch(uri, allowUrls)){
            return true;
        }
        String method  =  handlerMethod.getMethod().getName();


        SsoUser ssoUser = ssoLoginHelper.loginCheck(request, response);

        //构造下面校验注解权限的用户信息权限bean  AuthorizingSubject
        AuthorizingSubject subject = getAuthorizingSubject(ssoUser);
        // valid login fail
        if (subject == null) {

            boolean anonymousFlag= AuthorizingInterceptors.allowAnonymous(subject,request, handlerMethod);//是否允许不登录访问
            if(anonymousFlag){
                return true;
            }

            String sessionId = CookieUtil.getValue(request, SsoConf.SSO_SESSIONID);
            if(ssoLoginHelper.isInvalidSession(sessionId)){
               String  msg = "您登录信息已经失效，请重新登录！";
               throw new NoLoginException(msg);
            }

            if (isAjax(request,handlerMethod)) {
                String msg="{\"code\":"+ SsoConf.SSO_LOGIN_FAIL_RESULT.getCode()+", \"msg\":\""+ SsoConf.SSO_LOGIN_FAIL_RESULT.getMsg() +"\"}";
                responseString(response,msg,"application/json;charset=utf-8");
            } else {
                throw  new NoLoginException("sso no login");
            }
        }
        request.setAttribute(SsoConf.SSO_USER, ssoUser);
        request.setAttribute(TmConstant.YHID_KEY, ssoUser.getUserid());
        try {
           AuthorizingInterceptors.invoke(subject,request, handlerMethod);
        } catch (ForbiddenException e) {
            log.warn("没有权限访问服务：" + uri + " "+e.getMessage());
            throw e;
        } catch (UnAuthorizedException e) {
            log.warn("没有权限访问服务的方法：" + uri + " " + " " + method);
            responseString(response,"{alert:'您没有执行method:" + method + "的权限'}",null);
        }
        return true;
    }
    private AuthorizingSubject getAuthorizingSubject(SsoUser ssoUser){
        if(ssoUser==null)
            return null;
        Object o= EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_USERBEAN, ssoUser.getUserid());
        LoginBean lb=(LoginBean)o;
        if(lb==null){
            lb=loginService.cacheInfo(ssoUser.getUserid());
        }
        AuthorizingSubject subject=new AuthorizingSubject();
        subject.setAccount(ssoUser.getUsername());
        List<String> urls = (List<String>) EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_URL, ssoUser.getUserid());
        subject.setUrls(urls);
        List<String> codes = (List<String>) EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_CODE, ssoUser.getUserid());
        subject.setCodes(codes);

        subject.setData(lb);
        subject.setAdmin(lb.isAdmin());
        subject.setSystemAdmin(lb.isSystemAdmin());
        List<Map<String, String>> jsList = lb.getJsList();
        List<String> roles=new ArrayList<>();
        for (Map t : jsList) {
            String jsmc = String.valueOf(t.get("mc"));
            if(StringUtil.notEmpty(jsmc)){
                roles.add(jsmc);
            }
        }
        subject.setRoles(roles);
        return subject;
    }


    protected String responseString(HttpServletResponse response, String msg,String contentType) {
        try {
            response.reset();
            response.setContentType(contentType!=null?contentType:"text/html;charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(msg);
            writer.close();
            return null;
        } catch (IOException e) {
            return null;
        }
    }
    private boolean urlMatch(String servletPath,String[] paths){
        for (String path:paths) {
            String uriPattern = path.trim();
            // 支持ANT表达式
            if (antPathMatcher.match(uriPattern, servletPath)) {
                return true;
            }
        }
        return false;
    }
    //静态资源
    public static boolean isStaticResources(String uri){
        if(uri.indexOf("/resources/")>=0|| uri.indexOf("/front/")>=0||uri.endsWith(".js")||uri.endsWith(".css")||uri.endsWith(".jpg")||uri.endsWith(".jpeg")||uri.endsWith(".png")||uri.endsWith(".bmp")||uri.endsWith(".gif")||uri.endsWith(".eot")||uri.endsWith(".svg")||uri.endsWith(".ttf")||uri.endsWith(".woff")){
            return true;
        }else{
            return false;
        }
    }

}
