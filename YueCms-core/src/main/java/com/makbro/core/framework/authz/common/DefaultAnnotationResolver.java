package com.makbro.core.framework.authz.common;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ClassUtils;
import org.springframework.web.method.HandlerMethod;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 默认的注解解析器
 */
public class DefaultAnnotationResolver implements AnnotationResolver {

    public DefaultAnnotationResolver() {
    }
    @Override
    public Annotation getAnnotation(HandlerMethod handlerMethod, Class<? extends Annotation> cls) {
        Method m = handlerMethod.getMethod();
        Annotation a = AnnotationUtils.findAnnotation(m, cls);
        if(a != null) {
            return a;
        } else {
            Class targetClass =handlerMethod.getBeanType();
            m = ClassUtils.getMostSpecificMethod(m, targetClass);
            a = AnnotationUtils.findAnnotation(m, cls);
            return a != null?a:AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), cls);
        }
    }
}
