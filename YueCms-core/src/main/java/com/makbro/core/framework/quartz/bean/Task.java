package com.makbro.core.framework.quartz.bean;


import com.markbro.base.model.AliasModel;

/**
 * 调度任务 bean
 * @author wujiyue
 * @date 2018-09-28 18:26:29
 */
public class Task  implements AliasModel {

	private Integer id;//
	private String job_group;//任务分组
	private String job_name;//任务名

	private String description;//任务描述
	private String cron_expression;//cron表达式
	private String bean_class;//任务执行时调用哪个类

	private String job_data;//json格式的字符串。用来构造JobDataMap
	private String job_status;//任务状态
	private String create_user;//创建者
	private String create_time;//创建时间
	private String update_user;//更新者
	private String update_time;//更新时间
	private Integer available;//可用状态

	/**
	 * 扩展字段
	 * @see JobStatusListenerJob
	 */
	private int extend;
	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getJob_name(){ return job_name ;}
	public void  setJob_name(String job_name){this.job_name=job_name; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getCron_expression(){ return cron_expression ;}
	public void  setCron_expression(String cron_expression){this.cron_expression=cron_expression; }
	public String getBean_class(){ return bean_class ;}
	public void  setBean_class(String bean_class){this.bean_class=bean_class; }

	public String getJob_status(){ return job_status ;}
	public void  setJob_status(String job_status){this.job_status=job_status; }
	public String getJob_group(){ return job_group ;}
	public void  setJob_group(String job_group){this.job_group=job_group; }
	public String getCreate_user(){ return create_user ;}
	public void  setCreate_user(String create_user){this.create_user=create_user; }
	public String getCreate_time(){ return create_time ;}
	public void  setCreate_time(String create_time){this.create_time=create_time; }
	public String getUpdate_user(){ return update_user ;}
	public void  setUpdate_user(String update_user){this.update_user=update_user; }
	public String getUpdate_time(){ return update_time ;}
	public void  setUpdate_time(String update_time){this.update_time=update_time; }



	public String getJob_data() {
		return job_data;
	}

	public void setJob_data(String job_data) {
		this.job_data = job_data;
	}

	public int getExtend() {
		return extend;
	}

	public void setExtend(int extend) {
		this.extend = extend;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}


	@Override
	public String toString() {
	return "Task{" +
			"id=" + id+
			", job_group=" + job_group+
			", job_name=" + job_name+
			", description=" + description+
			", cron_expression=" + cron_expression+
			", bean_class=" + bean_class+

			", job_data=" + job_data+
			", job_status=" + job_status+
			", create_user=" + create_user+
			", create_time=" + create_time+
			", update_user=" + update_user+
			", update_time=" + update_time+
			", available=" + available+
			 '}';
	}
}
