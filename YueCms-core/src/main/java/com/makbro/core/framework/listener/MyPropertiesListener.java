package com.makbro.core.framework.listener;

import com.markbro.base.utils.PropertiesListenerConfig;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

/**
 * 个人配置文件监听器
 * 系统启动后加载配置文件到Map中，通过PropertiesListenerConfig.getAllProperty()获得配置文件素有配置信息。（注意，key中的“.”被替换成了“_”.）
 * 可以通过json接口（/sys/myProperties）查看配置文件的配置.
 * 目的是运行freemark在前台页面通过类似EL表达式确定配置信息。如：${properties.key}.  @see FreeMarkerConfig
 *  特别说明：MyConfig.java也是读取配置文件。只不过它读取到的值主要用于后台代码调用。而MyPropertiesListener.java用户freemark在前台展示
 */
public class MyPropertiesListener implements ApplicationListener<ApplicationStartedEvent> {

    private String[] propertyFileName;

    public MyPropertiesListener(String... propertyFileName) {
        this.propertyFileName = propertyFileName;
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        for(String s:propertyFileName){
            PropertiesListenerConfig.loadAllProperties(s);
        }

    }

}
