package com.makbro.core.framework.authz.annotationHandler;

import com.makbro.core.framework.authz.common.AuthorizingHandler;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.markbro.base.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * 注解处理
 */
public abstract class AbstractAuthorizingAnnotationHandler implements AuthorizingHandler {
    protected Logger log= LoggerFactory.getLogger(AbstractAuthorizingAnnotationHandler.class);
    protected Class<? extends Annotation> annotationClass;

    //protected AuthorizingSubject subject;
    public AbstractAuthorizingAnnotationHandler(Class<? extends Annotation> annotationClass) {
       // this.subject=subject;
        this.setAnnotationClass(annotationClass);
    }

   // protected AuthorizingSubject getSubject() {
    //    return subject;
   // }

    protected void setAnnotationClass(Class<? extends Annotation> annotationClass) throws IllegalArgumentException {
        if(annotationClass == null) {
            String msg = "annotationClass argument cannot be null";
            throw new IllegalArgumentException(msg);
        } else {
            this.annotationClass = annotationClass;
        }
    }

    public Class<? extends Annotation> getAnnotationClass() {
        return this.annotationClass;
    }

    public abstract boolean assertAuthorized(HttpServletRequest request, Annotation a, AuthorizingSubject subject) throws ApplicationException;

    protected static String StringArray2String(String split,String... arr){
        if(arr!=null){
            StringBuffer sb=new StringBuffer();
            for(String s:arr){
                sb.append(s).append(split);
            }
            String res=sb.toString();
            res=res.substring(0,res.length()-1);
            return res;
        }
        return "";
    }
}
