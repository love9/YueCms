package com.makbro.core.framework.interceptor;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 新建MyPageListAttrHandlerInterceptor替代原来的PageListAttrHandlerInterceptor，是因为原来的在返回ModelAndView获取Slider分页条时异常
 * 新增了第49行
 */
public class MyPageListAttrHandlerInterceptor extends HandlerInterceptorAdapter {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object o) throws Exception {
        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        Enumeration enumeration = request.getAttributeNames();
        while (enumeration.hasMoreElements()){
            Object element = enumeration.nextElement();
            if(element instanceof String){
                String name = (String)element;
                Object attr = request.getAttribute(name);
                if(attr instanceof PageList){
                    PageList pageList = (PageList)attr;
                    //原属性加后缀
                    request.setAttribute(name+"Paginator", pageList.getPaginator());
                }
            }
        }
        if(modelAndView != null){
            Map<String,Object> model = modelAndView.getModel();
            Map<String,Object> newModel = new HashMap<String, Object>();
            for(Map.Entry<String, Object> item : model.entrySet()){
                Object attr = item.getValue();
                if(attr instanceof PageList){
                    PageList pageList = (PageList)attr;
                    //原属性加后缀
                    Paginator paginator=pageList.getPaginator();

                    newModel.put(item.getKey()+"Paginator", paginator);
                    newModel.put(item.getKey()+"Slider", paginator.getSlider());
                }
            }
            modelAndView.addAllObjects(newModel);
        }
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
