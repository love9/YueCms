package com.makbro.core.framework.applicationEvent.eventLog.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.framework.applicationEvent.eventLog.bean.EventLog;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 系统事件日志 dao
 * @author wujiyue
 * @date 2018-11-20 21:48:33
 */
@Repository
public interface EventLogMapper {

    public EventLog get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(EventLog eventlog);
    public void addByMap(Map<String, Object> map);
    public void update(EventLog eventlog);
    public void updateByMap(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<EventLog> find(PageBounds pageBounds, Map<String, Object> map);
    public List<EventLog> findByMap(PageBounds pageBounds, Map<String, Object> map);




}
