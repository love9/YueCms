package com.makbro.core.framework.aspect;

import com.makbro.core.base.actionlog.bean.Actionlog;
import com.makbro.core.base.actionlog.service.ActionlogService;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.RequestContextHolderUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * AOP切面记录日志
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @website https://www.zhyd.me
 * @date 2018/4/16 16:26
 * @since 1.0
 */
@Slf4j
@Aspect
@Component
public class ActionLogAspect {

    @Autowired
    ActionlogService actionlogService;

    @Pointcut(value = "@annotation(com.markbro.base.annotation.ActionLog)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object writeLog(ProceedingJoinPoint point) throws Throwable {
        Long startTime;
        Long E_time;
        // 获取开始时间
        startTime = System.currentTimeMillis();
        //先执行业务
        Object result = point.proceed();
        // 获取方法执行时间
        E_time = System.currentTimeMillis() - startTime;
        try {
            //记录日志
            Signature sig = point.getSignature();
            MethodSignature msig = (MethodSignature) sig;
            Object target = point.getTarget();
            Method currentMethod =  target.getClass().getMethod(msig.getName(), msig.getParameterTypes());
            ActionLog annotation = currentMethod.getAnnotation(ActionLog.class);
            HttpServletRequest request=RequestContextHolderUtil.getRequest();
            JSONObject json= JSONObject.fromObject(MyBatisRequestUtil.getMap(request));
            String desc=annotation.description();
            String  uri=request.getRequestURI();
            Actionlog actionlog=new  Actionlog();
            String ip= MyBatisRequestUtil.getIpByHttpRequest(request);
            LoginBean loginBean= MyBatisRequestUtil.getLoginUserInfo(request);
            actionlog.setIp(ip);
            actionlog.setDescription(desc);
            actionlog.setUri(uri);
            actionlog.setYhid(loginBean.getYhid());
            actionlog.setYhmc(loginBean.getXm());
            actionlog.setOrgid(loginBean.getOrgid());
            actionlog.setMethod(currentMethod.getName());
            if(json!=null){
                actionlog.setParams(json.toString());
            }
            actionlog.setActionTime(E_time.intValue());
            actionlogService.add(actionlog);
        } catch (Exception e) {
            log.error("日志记录出错!", e);
        }

        return result;
    }



}
