package com.makbro.core.framework.applicationEvent.eventLog.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.framework.applicationEvent.eventLog.bean.EventLog;
import com.makbro.core.framework.applicationEvent.eventLog.dao.EventLogMapper;
import com.markbro.base.model.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 系统事件日志 Service
 * @author wujiyue
 * @date 2018-11-20 21:48:33
 */
@Service
public class EventLogService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private EventLogMapper eventlogMapper;

     /*基础公共方法*/
    public EventLog get(Integer id){
        return eventlogMapper.get(id);
    }
    public List<EventLog> find(PageBounds pageBounds,Map<String,Object> map){
        return eventlogMapper.find(pageBounds,map);
    }
    public List<EventLog> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return eventlogMapper.findByMap(pageBounds,map);
    }
    public void add(EventLog evenlog){
        eventlogMapper.add(evenlog);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          EventLog evenlog=JSON.parseObject(JSON.toJSONString(map),EventLog.class);
          if(evenlog.getId()==null||"".equals(evenlog.getId().toString())){
              eventlogMapper.add(evenlog);
          }else{
              eventlogMapper.update(evenlog);
          }
          msg.setType(Msg.MsgType.success);
          msg.setContent("保存信息成功");
          return msg;
    }
    public void update(EventLog evenlog){
        eventlogMapper.update(evenlog);
    }

    public void updateByMap(Map<String,Object> map){
        eventlogMapper.updateByMap(map);
    }

    public void delete(Integer id){
        eventlogMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        eventlogMapper.deleteBatch(ids);
    }



     /*自定义方法*/

}
