package com.makbro.core.framework.authz.annotationInterceptorImpl;


import com.makbro.core.framework.authz.annotationHandler.PermissionAnnotationHandler;
import com.makbro.core.framework.authz.common.AnnotationResolver;

/**
 * Created by Administrator on 2018/12/5.
 */
public class PermissionAnnotationInterceptor extends AbstractAuthorizingAnnotationInterceptor {

    public PermissionAnnotationInterceptor() {
        super(new PermissionAnnotationHandler());
    }

    public PermissionAnnotationInterceptor(AnnotationResolver resolver) {
        super(new PermissionAnnotationHandler(), resolver);
    }
}
