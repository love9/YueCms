package com.makbro.core.framework;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.markbro.base.utils.MyPageListJsonSerializer;


public class CustomJsonMapper extends ObjectMapper {

    public CustomJsonMapper() {

        SimpleModule pageListJSONModule = new SimpleModule("PageListJSONModule", new Version(1, 0, 0, null, null, null));
        pageListJSONModule.addSerializer(PageList.class, new MyPageListJsonSerializer());
        registerModule(pageListJSONModule);

       /* else{//这是默认的
            SimpleModule pageListJSONModule = new SimpleModule("PageListJSONModule", new Version(1, 0, 0, null, null, null));
            pageListJSONModule.addSerializer(PageList.class, new PageListJsonSerializer());
            registerModule(pageListJSONModule);
        }*/

        /*SimpleModule onTheEarthJSONModule = new SimpleModule("OnTheEarthJSONModule", new Version(1, 0, 0, null, null, null));
        onTheEarthJSONModule.addSerializer(OnTheEarth.class, new OnTheEarthJsonSerializer());
        registerModule(onTheEarthJSONModule);

        SimpleModule onTheEarthMapJSONModule = new SimpleModule("OnTheEarthMapJSONModule", new Version(1, 0, 0, null, null, null));
        onTheEarthJSONModule.addSerializer(Map.class, new OnTheEarthMapJsonSerializer());
        registerModule(onTheEarthMapJSONModule);*/

    }
}