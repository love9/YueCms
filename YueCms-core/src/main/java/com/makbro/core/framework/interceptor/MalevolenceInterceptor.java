package com.makbro.core.framework.interceptor;


import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 防止恶意请求
 * 点击文章会发送一个请求记录该页面被请求，然后做出相应处理，比如该文章查看次数+1等等
 * 但是由于该请求时允许匿名访问，为了防止用户分析出该请求链接而用程序恶意刷新请求。故有此拦截器来拦截恶意的请求。
 * 基于IP和请求时间拦截。同一个IP在一小短时间内比如3秒内不允许请求多次。
 * 把想要防止恶意请求的uri配置到springMVC配置文件中
 */
public class MalevolenceInterceptor implements HandlerInterceptor {

    private String[] allowReferers;


    public MalevolenceInterceptor(String[] allowReferers) {
        this.allowReferers = allowReferers;
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int timeSpan=5;//请求最小间隔
        String ip= MyBatisRequestUtil.getIpByHttpRequest(request);
        String time= DateUtil.getDatetime();
        String referer=request.getHeader("Referer");

        boolean allowFlag=false;
        for(String t:allowReferers){
            if(referer.contains(t)){
                allowFlag=true;
                break;
            }
        }
        if(!allowFlag){
            //该referer不允许
            outError(response);
            return false;
        }
        String cacheTime=(String) EhCacheUtils.getSysInfo("MalevolenceInterceptor_",ip);//缓存中去该ip上次请求时间
        if(StringUtil.notEmpty(cacheTime)){
            if(DateUtil.parseDatetime(time).before(DateUtil.addSecond(DateUtil.parseDatetime(cacheTime), timeSpan))){
                //如果当前时间在缓存中存放的时间+请求最小间隔之前，不允许通过
                outError(response);
                return false;
            }
        }
        EhCacheUtils.putSysInfo("MalevolenceInterceptor_", ip, time);//刷新刷出存放时间
        return true;
    }

    private void outError(HttpServletResponse response){
        response.setContentType("text/html;charset=UTF-8");// 解决中文乱码
        try {
            PrintWriter writer = response.getWriter();
            writer.write("error");
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
