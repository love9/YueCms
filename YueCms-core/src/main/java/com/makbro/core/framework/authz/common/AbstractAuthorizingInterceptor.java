package com.makbro.core.framework.authz.common;


import com.markbro.base.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * 注解拦截
 */
public abstract class AbstractAuthorizingInterceptor  implements AuthorizingInterceptor {
    protected Logger log= LoggerFactory.getLogger(AbstractAuthorizingInterceptor.class);
    private AbstractAuthorizingHandler handler;


    public AbstractAuthorizingInterceptor(AbstractAuthorizingHandler handler) {
        if(handler == null) {
            throw new IllegalArgumentException("AbstractAuthorizingInterceptor argument cannot be null.");
        }
        this.handler=handler;
    }



    public AbstractAuthorizingHandler getHandler() {
        return handler;
    }

    public void setHandler(AbstractAuthorizingHandler handler) {
        this.handler = handler;
    }



    public void invoke(HttpServletRequest request,AuthorizingSubject subject) throws Throwable {
        this.assertAuthorized(request,subject);
    }
    public boolean assertAuthorized(HttpServletRequest request,AuthorizingSubject subject) throws ApplicationException {
        try {
           return ((AbstractAuthorizingHandler)this.getHandler()).assertAuthorized(request,subject);
        } catch (ApplicationException ex) {
            /*if(ex.getCause() == null) {
                ex.initCause(new ApplicationException(ex.getMessage()));
            }
            throw ex;*/
            log.error(ex.getMessage());
            return false;
        }
    }


}
