package com.makbro.core.framework.quartz.jobs;

import com.alibaba.fastjson.JSONObject;
import com.makbro.core.framework.quartz.bean.Task;
import com.makbro.core.framework.quartz.dao.TaskMapper;
import com.makbro.core.framework.quartz.quartz.QuartzManager;
import com.makbro.core.framework.quartz.service.TaskService;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 监控job运行状态确保和数据库值保持一致
 * 每天校验一次
 * 重要：如果新增或更新任务分组或名称，最好<strong>立即</strong>触发一次该任务，以保证任务和数据库表中数据一致
 */
public class JobStatusListenerJob implements Job {
    @Autowired
    QuartzManager quartzManager;
    @Autowired
    TaskMapper taskMapper;
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        //获得所有任务，并根据运行状态初始化运行状态的值
        List<Map<String,Object>> list=quartzManager.queryAllJob();
        //从缓存中取得所有task
        List<Task> taskList=(List<Task>) EhCacheUtils.getSysInfo(TaskService.KEY_TASKS_LIST);

        if(CollectionUtils.isNotEmpty(list)){
            for(Map m:list){
                String jobName=(String)m.get("job_name");
                String job_group=(String)m.get("job_group");
                if(!checkExists(taskList,jobName,job_group)){
                    //该任务不在表中存在，那么认为它是不受管控的任务，需要删除它
                    quartzManager.deleteJob(jobName,job_group);
                }
                String job_status="";
                Trigger.TriggerState triggerState=(Trigger.TriggerState)m.get("job_status");
                if(triggerState.equals(Trigger.TriggerState.NORMAL)){
                    job_status="1";
                }else if(triggerState.equals(Trigger.TriggerState.PAUSED)){
                    job_status="2";
                }else if(triggerState.equals(Trigger.TriggerState.NONE)){
                    job_status="0";
                }else if(triggerState.equals(Trigger.TriggerState.COMPLETE)){
                    job_status="3";
                }else if(triggerState.equals(Trigger.TriggerState.ERROR)){
                    job_status="4";
                }else if(triggerState.equals(Trigger.TriggerState.BLOCKED)){
                    job_status="5";
                }else{}

                taskMapper.updateJobStatusByJobNameAndJobGroup(jobName,job_group, job_status);
            }
            //add by wjy on 2018年10月2日22:51:58
            for(Task task:taskList){
                if(task.getExtend()==0&&task.getAvailable()==1){
                    //这认为是新更新或新增加的，需要加入scheduler
                    try {
                        Map jobDataMap=new HashMap<String,Object>();
                        if(StringUtil.notEmpty(task.getJob_data())){
                            jobDataMap=(Map) JSONObject.parse(task.getJob_data());
                        }
                        quartzManager.addJob((Class<? extends Job>)(Class.forName((String)task.getBean_class()).newInstance().getClass()),task.getJob_name(), task.getJob_group(),task.getCron_expression(),task.getDescription(),jobDataMap);
                        taskMapper.updateJobStatus(task.getId().toString(),"1");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 检查该任务是否需要删除，返回false需要删除
     * @param taskList
     * @param jobName
     * @param jobGroup
     * @return
     */
    private boolean checkExists(List<Task> taskList, String jobName, String jobGroup){
      return   taskList.stream().anyMatch(t->{
          if(t.getJob_name().equals(jobName) && t.getJob_group().equals(jobGroup)){
              t.setExtend(1);//存在于scheduler中的标记为1，而不存在，需要新加入的为0
              return true;
          }else{
              t.setExtend(0);
              return false;
          }
        });
    }
}
