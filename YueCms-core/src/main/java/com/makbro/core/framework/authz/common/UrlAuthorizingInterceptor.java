package com.makbro.core.framework.authz.common;

/**
 * Created by Administrator on 2018/12/6.
 */
public class UrlAuthorizingInterceptor extends  AbstractAuthorizingInterceptor {

    public UrlAuthorizingInterceptor() {
        super(new UrlAuthorizingHandler());
    }

}
