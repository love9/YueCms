package com.makbro.core.framework;


import com.markbro.base.exception.Error;
import com.markbro.base.exception.*;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerMethodExceptionResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

/**
 *凡是方法标识@ResponseBody的遇到异常都返回为JSON格式
 */
public class DefaultExceptionHandler extends AbstractHandlerMethodExceptionResolver {

    private boolean isAjax(HttpServletRequest request, HandlerMethod handlerMethod){
        if((handlerMethod!=null&&handlerMethod.getMethodAnnotation(ResponseBody.class) != null)||((request.getHeader("accept").contains("application/json")  || (request.getHeader("X-Requested-With")!= null && request
                .getHeader("X-Requested-With").contains("XMLHttpRequest") )))){
            return true;
        }
        return false;
    }
    @Override
    protected ModelAndView doResolveHandlerMethodException(HttpServletRequest request, HttpServletResponse response, HandlerMethod handlerMethod, Exception ex) {
        int errorCode = 200;
        int responseCode = 200;
        String uri=request.getRequestURI();
        if(ex.getClass().getName().equals(UnAuthorizedException.class.getName())){
            responseCode = HttpServletResponse.SC_FORBIDDEN;
            errorCode = responseCode;

        }
        else if(ex.getClass().getName().equals(BindException.class.getName())){
            responseCode = HttpServletResponse.SC_BAD_REQUEST;
            errorCode = responseCode;

        }
        else if(ex.getClass().getName().equals(IllegalArgumentException.class.getName())){
            responseCode = HttpServletResponse.SC_BAD_REQUEST;
            errorCode = responseCode;
        }
        else if(ex.getClass().getName().equals(ConstraintViolationException.class.getName())){
            responseCode = HttpServletResponse.SC_BAD_REQUEST;

            errorCode = responseCode;
        }else if(ex.getClass().getName().equals(ForbiddenException.class.getName())){
            responseCode = HttpServletResponse.SC_FORBIDDEN;
            errorCode = responseCode;

        }
        else if(ex.getClass().getName().equals(NoLoginException.class.getName())){
            //返回JSON
            if(isAjax(request,handlerMethod)){

                Error error = new Error();
                error.setCode("NoLogin");
                error.setError(ex.getMessage());

                ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
                mav.addObject(error);
                return mav;
            }
            //返回VIEW
            ModelAndView mv = new ModelAndView();
            mv.addObject("error", ex);
            mv.addObject("info", ex.getMessage());
            mv.addObject("redirectUrl", uri);
            mv.addObject("noRedirect", "");
            //mv.setViewName("error/NoLogin");
            mv.setViewName("/error/NoLogin");
            return mv;
        }
        else if(ex.getClass().getName().equals(SessionTimeoutException.class.getName())){
            //返回JSON
            if(isAjax(request,handlerMethod)){

                Error error = new Error();
                error.setCode("SessionTimeout");
                error.setError(ex.getMessage());

                ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
                mav.addObject(error);
                return mav;
            }
            //返回VIEW
            ModelAndView mv = new ModelAndView();
            mv.addObject("error", ex);
            mv.addObject("info", ex.getMessage());
            mv.addObject("redirectUrl", uri);
            mv.addObject("noRedirect", "");
            mv.setViewName("error/timeout");
            return mv;
        } else if(ex.getClass().getName().equals(SessionTimeoutExceptionNoRedirect.class.getName())){
            //返回JSON
            if(isAjax(request,handlerMethod)){
                Error error = new Error();
                error.setCode("SessionTimeout");
                error.setError(ex.getMessage());
                ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
                mav.addObject(error);
                return mav;
            }
            //返回VIEW
            ModelAndView mv = new ModelAndView();
            mv.addObject("error", ex);
            mv.addObject("info", ex.getMessage());
            mv.addObject("redirectUrl", uri);
            mv.addObject("noRedirect", "1");
            mv.setViewName("error/timeout");
            return mv;
        }else if(ex.getClass().getName().equals(FormRepeatSubmitException.class.getName())){
            //返回JSON
            if(isAjax(request,handlerMethod)){
                Error error= new Error();
                error.setCode("FormRepeatSubmit");
                error.setError("请勿重复提交表单！");
                ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
                mav.addObject(error);
                return mav;
            }
            //返回VIEW
           /* ModelAndView mv = new ModelAndView();
            mv.addObject("error", ex);
            mv.addObject("info", ex.getMessage());
            mv.addObject("redirectUrl", uri);
            mv.setViewName("error/timeout");
            return mv;*/
        }else if(ex instanceof Throwable){
            responseCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            errorCode = responseCode;
        }
        response.setStatus(responseCode);
        //返回JSON
        if(isAjax(request,handlerMethod)){
                Error error = new Error(ex.getMessage());
                error.setCode(String.valueOf(errorCode));
                error.setError(ex.getMessage());
                ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
                mav.addObject(error);
                return mav;
        }
        //返回VIEW
        ModelAndView mv = new ModelAndView();
        mv.addObject("error", ex);
        mv.addObject("info", ex.getMessage());
        if(responseCode == HttpServletResponse.SC_FORBIDDEN){
            mv.setViewName("error/403");
        }else if(responseCode == HttpServletResponse.SC_BAD_REQUEST){
            mv.setViewName("error/400");
        }else if(responseCode == HttpServletResponse.SC_NOT_FOUND){
            mv.setViewName("error/404");
        }else{
            mv.setViewName("error/500");
        }
        return mv;
    }

}
