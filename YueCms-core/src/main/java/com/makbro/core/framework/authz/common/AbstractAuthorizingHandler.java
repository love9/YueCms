package com.makbro.core.framework.authz.common;


import com.markbro.base.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * 注解处理
 */
public abstract class AbstractAuthorizingHandler implements AuthorizingHandler {
    protected Logger log= LoggerFactory.getLogger(AbstractAuthorizingHandler.class);



    public abstract boolean assertAuthorized(HttpServletRequest request,AuthorizingSubject subject) throws ApplicationException;


}
