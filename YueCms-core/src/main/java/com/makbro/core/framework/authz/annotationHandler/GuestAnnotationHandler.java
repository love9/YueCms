package com.makbro.core.framework.authz.annotationHandler;


import com.makbro.core.framework.authz.annotation.RequiresGuest;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.exception.ForbiddenException;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * Created by Administrator on 2018/12/5.
 */
public class GuestAnnotationHandler extends AbstractAuthorizingAnnotationHandler {


    public GuestAnnotationHandler() {
        super(RequiresGuest.class);
    }

    @Override
    public boolean assertAuthorized(HttpServletRequest request, Annotation a,AuthorizingSubject subject) throws ApplicationException {
        if(a instanceof RequiresGuest) {
            String uri = request.getServletPath();
            RequiresGuest rrAnnotation = (RequiresGuest) a;
            if(subject!=null){
                //如果已经登录则抛出异常
                String msg="用户["+subject.getAccount()+"]没有权限访问["+request.getRequestURI()+"],需要匿名访问!";
                log.warn(msg);
                throw new ForbiddenException(msg);
            }else{
                return true;
            }
        }
        return false;
    }
}
