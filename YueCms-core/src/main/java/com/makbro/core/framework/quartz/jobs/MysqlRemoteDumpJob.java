package com.makbro.core.framework.quartz.jobs;


import com.markbro.base.common.util.SecurityUtil;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.kit.RegexKit;
import com.markbro.base.utils.string.StringUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * MySql数据库自动备份任务（远程备份）与上面的本机备份区别在于cmd命令多了-h参数
 * 注：1.必须在Windows环境下
 * 2. 运行环境必须要安装mysql，并将mysql的bin目录配置到系统变量Path中
 */
public class MysqlRemoteDumpJob extends BaseJob {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);




        String localhostMysqlHome= GlobalConfig.getConfig("system.backup.exec");//本机mysql执行程序目录
        if(!localhostMysqlHome.endsWith("/")){
            localhostMysqlHome+="/";
        }

        String host="";
        int port=3306;
        String dbName="";//备份数据库名称
        String path= GlobalConfig.getConfig("system.backup.savePath");//数据库备份基本路径
        if(StringUtil.isEmpty(path)){
            path="c:/db_backup/";
        }
        if(!path.endsWith("/")){
            path+="/";
        }
        String url = GlobalConfig.getConfig("system.backup.url");
        String username = GlobalConfig.getConfig("system.backup.username");
        String password = GlobalConfig.getConfig("system.backup.password");
        //因为这个密码加密，所以要尝试解密,如果是明文则忽略解密过程
        try {
            final byte[] key = { 9, -1, 0, 5, 39, 8, 6, 19 };
            String pwd= SecurityUtil.decryptDes(password, key);
            password=pwd;
        }catch (Exception ex){  }

        //通过URL来解析
        String patten = "^jdbc:mysql://(.+):(\\d+)/(.+)";
        if (RegexKit.isMatch(url, patten)) {
            List<String> list = RegexKit.getMatchArray(url, patten);
            host = list.get(1);
            port = Integer.parseInt(list.get(2));
            String db = list.get(3);
            if (db.contains("?")) {
                db = db.substring(0, db.indexOf("?"));
            }
            dbName = db;
        } else {
            logger.error("请确保参数中有：url, username, password，并且符合对应数据的规范");
            throw new RuntimeException("参数错误");
        }


            try {
                logger.info("/* 数据库远程备份开始... */");
                //CMD 命令
                String cmd = String.format(localhostMysqlHome+"/mysqldump -u%s -p%s -h %s -P %s %s", username, password, host,port, dbName);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm");
                String filePath = path + dbName +"_" + sdf.format(new Date()) + ".sql";
                Runtime run = Runtime.getRuntime();
                Process p = run.exec(cmd);
                InputStream is = p.getInputStream();// 控制台的输出信息作为输入流
                InputStreamReader isr = new InputStreamReader(is, "UTF-8");//设置输入流编码格式
                BufferedReader br = new BufferedReader(isr);
                File file = new File(filePath);
                if (!file.exists()) {
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                }
                //将控制台输入信息写入到文件输出流中
                FileOutputStream fos = new FileOutputStream(filePath);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"));
                String temp = null;
                while ((temp = br.readLine()) != null) {
                    bw.write(temp);
                    bw.newLine();
                }
                bw.flush();
                bw.close();
                br.close();
                logger.info("/* 数据库远程已备份，  存放于 " + filePath + "。*/");
            } catch (IOException e) {
                e.printStackTrace();
                logger.info("/* 数据库远程备份失败，" + e.getLocalizedMessage() + "*/");
            }
    }
}
