package com.makbro.core.framework.authz.annotationHandler;


import com.makbro.core.framework.authz.annotation.RequiresAnonymous;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.markbro.base.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * Created by Administrator on 2018/12/5.
 */
public class AnonymousAnnotationHandler extends AbstractAuthorizingAnnotationHandler {


    public AnonymousAnnotationHandler() {
        super(RequiresAnonymous.class);
    }

    @Override
    public boolean assertAuthorized(HttpServletRequest request, Annotation a, AuthorizingSubject subject) throws ApplicationException {
        if(a instanceof RequiresAnonymous) {
            String uri = request.getServletPath();
           //被该注解标记，都能访问，单仍然向request设置一些信息

            return true;
        }
        return false;
    }
}
