package com.makbro.core.framework.applicationEvent.eventLog.web;


import com.makbro.core.framework.applicationEvent.eventLog.bean.EventLog;
import com.makbro.core.framework.applicationEvent.eventLog.service.EventLogService;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 系统事件日志管理
 * @author wujiyue
 * @date 2018-11-20 21:48:33
 */

@Controller
@RequestMapping("/sys/eventLog")
public class EventLogController extends BaseController {
    @Autowired
    protected EventLogService eventLogService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/sys/eventLog/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(EventLog eventLog, Model model){
                return "/sys/eventLog/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(EventLog eventLog,Model model){
        if(eventLog!=null&&eventLog.getId()!=null){
            eventLog=eventLogService.get(eventLog.getId());
        }
         model.addAttribute("eventLog",eventLog);
         return "/sys/eventLog/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return eventLogService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return eventLogService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }



    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return eventLogService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除系统事件日志")
    public Object delete(@PathVariable Integer id) {
    	Msg msg=new Msg();
    	try{
            eventLogService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
        		msg.setType(Msg.MsgType.error);
        		msg.setContent("删除失败！");
        }
        return msg;
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除系统事件日志")
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
    	try{
             eventLogService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }
}