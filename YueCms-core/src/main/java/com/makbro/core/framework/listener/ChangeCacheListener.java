package com.makbro.core.framework.listener;

/**
 * 通知改变缓存内容监听器
 */
public interface ChangeCacheListener {

    public void onAdd(Object id);

    public void onUpdate(Object id);

    public void onDelete(Object id);
}
