package com.makbro.core.framework.interceptor;


import com.markbro.base.annotation.FormToken;
import com.markbro.base.exception.FormRepeatSubmitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * 防止表单重复提交拦截器
 * @author wjy
 */
public class FormTokenInterceptor extends HandlerInterceptorAdapter {

    private  Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            FormToken annotation = method.getAnnotation(FormToken.class);
            if (annotation != null) {
                boolean needSaveSession = annotation.get();//需要生成token
                if (needSaveSession) {
                    request.getSession(true).setAttribute("FormToken", UUID.randomUUID().toString());
                }
                boolean needRemoveSession = annotation.remove();//需要移除token
                if (needRemoveSession) {
                    if (isRepeatSubmit(request)) {
                        logger.warn("please don't repeat submit,url:"+ request.getServletPath());
                        throw new FormRepeatSubmitException("请勿重复提交表单!");
                       // return false;
                    }
                    request.getSession(true).removeAttribute("FormToken");
                }
            }
        }
        return true;
    }
    private boolean isRepeatSubmit(HttpServletRequest request) {
        String serverToken = (String) request.getSession(true).getAttribute("FormToken");
        if (serverToken == null) {
            return true;
        }
        String clinetToken = request.getParameter("FormToken");

        if (clinetToken == null) {
            return true;
        }
        if (!serverToken.equals(clinetToken)) {
            return true;
        }
        return false;
    }
}
