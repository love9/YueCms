package com.makbro.core.framework.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * 切换数据源(不同方法调用不同数据源)
 * 
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:17:52
 */
@Aspect
@Component
@EnableAspectJAutoProxy(proxyTargetClass = false)
public class DataSourceAspect {
	private  Logger logger = LoggerFactory.getLogger(getClass());
	@Pointcut("execution(* com.markbro..*Mapper.*(..))")
	public void aspect() {
	}

	/**
	 * 配置前置通知,使用在方法aspect()上注册的切入点
	 */
	@Before("aspect()")
	public void before(JoinPoint point) {
		//System.out.println("=========开始选择读、写数据源================");
		String className = point.getTarget().getClass().getName();
		String method = point.getSignature().getName();
		//logger.debug("==============>"+className + "." + method + "(" + StringUtils.join(point.getArgs(), ",") + ")");
		try {
			L: for (String key : ChooseDataSource.METHOD_TYPE.keySet()) {
				for (String type : ChooseDataSource.METHOD_TYPE.get(key)) {
					if(type.endsWith("*")){
						type=type.substring(0,type.length()-1);
						if(type.startsWith(method)){
							logger.debug("======>选定"+key+"数据源");
							HandleDataSource.putDataSource(key);
							break L;
						}
					}else{
						if (method.startsWith(type)) {
							logger.debug("======>选定"+key+"数据源");
							HandleDataSource.putDataSource(key);
							break L;
						}
					}

				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			HandleDataSource.putDataSource("write");
		}
	}

	@After("aspect()")
	public void after(JoinPoint point) {
		HandleDataSource.clear();
	}
}
