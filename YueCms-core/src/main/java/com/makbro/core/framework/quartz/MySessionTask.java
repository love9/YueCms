package com.makbro.core.framework.quartz;


import com.makbro.core.framework.listener.InitCacheListener;
import com.makbro.core.framework.listener.MySessionListener;
import com.makbro.core.framework.mysession.bean.MySession;
import com.makbro.core.framework.mysession.dao.MySessionMapper;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.utils.EhCacheUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Administrator on 2018/8/1.
 */
@Component
public class MySessionTask implements InitCacheListener {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    MySessionMapper mySessionMapper;

    /**
     * 该方法定时检查sys_session中存在的超过过期时间的会话，删掉
     */
    //@Scheduled(cron="0/60 * * * * ? ")
    public void dealWithExpires_time(){
        List<MySession> sessions = mySessionMapper.findExpires();
        if(CollectionUtils.isNotEmpty(sessions)){
            MySessionListener listener=new MySessionListener();
            for(MySession mySession:sessions){
                listener.tokenDelete("sessionExpires", mySession);
                mySessionMapper.deleteUserSessionToken(mySession.getYhid(),mySession.getToken());
            }
        }
    }

    /**
     * 该方法是在系统启动后，初始化缓存的当前在线人数的值
     */
    @Override
    public void onInit() {
        Integer num=mySessionMapper.queryOnlineNum();
        logger.info("======MySessionTask======onInit========num:"+num);
        EhCacheUtils.putSysInfo(TmConstant.NUM_ONLINE,num);
    }
}
