package com.makbro.core.framework.quartz.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.framework.quartz.bean.Task;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 调度任务 dao
 * @author wujiyue
 * @date 2018-09-28 18:26:30
 */
@Repository
public interface TaskMapper{

    public Task get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Task task);
    public void addByMap(Map<String, Object> map);
    public void update(Task task);
    public void updateByMap(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Task> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Task> findByMap(PageBounds pageBounds, Map<String, Object> map);

    public void updateJobStatus(@Param("id") String id, @Param("job_status") String job_status);

    public void updateJobStatusByJobNameAndJobGroup(@Param("job_name") String job_name, @Param("job_group") String job_group, @Param("job_status") String job_status);

}
