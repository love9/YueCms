package com.makbro.core.framework.freemark.tag;


import com.makbro.core.base.login.service.LoginService;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuthTag extends BaseAuthTag{
    public AuthTag() {
        super(AuthTag.class.getName());
    }
    @Autowired
    private LoginService loginService;

    public boolean hasPermission(Map params){

        String permission=String.valueOf(params.get("permission"));
        if(StringUtil.notEmpty(permission)){
            return MyBatisRequestUtil.hasPermission(permission);
        }else{
            return false;
        }

    }
    public boolean hasRoles(Map params){

        String role=String.valueOf(params.get("role"));
        if(StringUtil.notEmpty(role)){
            return MyBatisRequestUtil.hasRole(role);
        }else{
            return false;
        }

    }
}
