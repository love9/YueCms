package com.makbro.core.framework.aspect;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 动态切换获取数据源
 * 
 */
@Component("dataSource")
public class ChooseDataSource extends AbstractRoutingDataSource {

	@Autowired
	@Qualifier("writeDataSource")
	private DataSource writeDataSource;
	@Autowired
	@Qualifier("readDataSource")
	private DataSource readDataSource;
	public static Map<String, List<String>> METHOD_TYPE = new HashMap<String, List<String>>();


	@Override
	public void afterPropertiesSet() {
		this.setDefaultTargetDataSource(writeDataSource);
		// 配置多数据源
		Map<Object, Object> dsMap = new HashMap();
		dsMap.put("write", writeDataSource);
		dsMap.put("read", readDataSource);
		this.setTargetDataSources(dsMap);
		String writeMethod=",add*,update*,updateBy*,delete*,remove*,insert*,create,update,delete,remove,save*,";
		String[] arr1=writeMethod.split(",");
		List<String> writeList=new ArrayList<>();
		for(String s:arr1){
			if (StringUtils.isNotBlank(s)) {
				writeList.add(s);
			}
		}
		METHOD_TYPE.put("write",writeList);
		String readMethod=",get,getMap,find,findByMap,findBy*,query*,tree,select,";
		String[] arr2=readMethod.split(",");
		List<String> readList=new ArrayList<>();
		for(String s:arr2){
			if (StringUtils.isNotBlank(s)) {
				readList.add(s);
			}
		}
		METHOD_TYPE.put("read",readList);

		super.afterPropertiesSet();
	}

	// 获取数据源名称
	@Override
	protected Object determineCurrentLookupKey() {
		return HandleDataSource.getDataSource();
	}

}
