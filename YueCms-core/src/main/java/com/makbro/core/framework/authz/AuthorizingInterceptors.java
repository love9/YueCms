package com.makbro.core.framework.authz;


import com.makbro.core.framework.authz.annotationInterceptorImpl.*;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.makbro.core.framework.authz.common.DefaultAnnotationResolver;
import com.makbro.core.framework.authz.common.UrlAuthorizingInterceptor;
import com.markbro.base.exception.ApplicationException;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Administrator on 2018/12/5.
 */
public  class AuthorizingInterceptors {

    protected static Collection<AbstractAuthorizingAnnotationInterceptor> methodInterceptors = new ArrayList(5);
    static DefaultAnnotationResolver resolver=new DefaultAnnotationResolver();

    //允许匿名访问拦截器比较特殊，不好放入到拦截器链里面。它的优先级最高，方法被他标注，不在走其他注解拦截器判断，无论是否登录都可以访问。
    static AbstractAuthorizingAnnotationInterceptor anonymousAnnotationInterceptor=new AnonymousAnnotationInterceptor(resolver);

    static UrlAuthorizingInterceptor urlAuthorizingInterceptor=new UrlAuthorizingInterceptor();

    static {
        methodInterceptors.add(new RoleAnnotationInterceptor(resolver));//需要角色
        methodInterceptors.add(new PermissionAnnotationInterceptor(resolver));//需要权限
        methodInterceptors.add(new AuthenticationAnnotationInterceptor(resolver));//登录即可
        methodInterceptors.add(new GuestAnnotationInterceptor(resolver));//必须游客才能访问
    }
  /*  public AuthorizingAnnotationsInterceptor() {
    }
    public Collection<AuthorizingAnnotationInterceptor> getMethodInterceptors() {
        return this.methodInterceptors;
    }
    public void setMethodInterceptors(Collection<AuthorizingAnnotationInterceptor> methodInterceptors) {
        this.methodInterceptors = methodInterceptors;
    }*/

    protected static boolean assertAuthorized(AuthorizingSubject subject, HttpServletRequest request, HandlerMethod handlerMethod) throws ApplicationException {

        boolean flag=false;
        if(methodInterceptors != null && !methodInterceptors.isEmpty()) {
            Iterator it = methodInterceptors.iterator();
            while(it.hasNext()) {
                AbstractAuthorizingAnnotationInterceptor aami = (AbstractAuthorizingAnnotationInterceptor)it.next();
                if(aami.supports(handlerMethod)) {
                    flag = aami.assertAuthorized(request,handlerMethod,subject);
                }
            }
        }
        if(!flag){
            flag = urlAuthorizingInterceptor.assertAuthorized(request,subject);
        }
        return flag;
    }
    public static boolean allowAnonymous(AuthorizingSubject subject,HttpServletRequest request,HandlerMethod method)  {
        if(anonymousAnnotationInterceptor.supports(method)){
            anonymousAnnotationInterceptor.assertAuthorized(request,method,subject);
            return true;
        }
        return false;
    }
    public static boolean invoke(AuthorizingSubject subject,HttpServletRequest request,HandlerMethod method)  {
        return assertAuthorized(subject, request,method);
    }
}
