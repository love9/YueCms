package com.makbro.core.framework.authz.common;

import java.util.List;

/**
 * 登陆后用户信息实体类继承该接口，用来存储登录用户的权限信息。比如有什么角色，有什么权限等
 */
public class AuthorizingSubject<T> {
   public String account;//登录账户
    public T data;//所有信息
    public List<String> roles;//角色
    public List<String> codes;//权限代码
    public List<String> urls;//权限代码
    private boolean isSystemAdmin;//系统管理员
    private boolean isAdmin;//以admin角色定义的管理员

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public boolean isSystemAdmin() {
        return isSystemAdmin;
    }

    public void setSystemAdmin(boolean isSystemAdmin) {
        this.isSystemAdmin = isSystemAdmin;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }
}
