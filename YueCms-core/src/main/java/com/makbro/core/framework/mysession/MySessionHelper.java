package com.makbro.core.framework.mysession;


import com.makbro.core.config.MyConfig;
import com.makbro.core.framework.listener.MySessionListener;
import com.makbro.core.framework.mysession.bean.MySession;
import com.makbro.core.framework.mysession.bean.Token;
import com.makbro.core.framework.mysession.dao.MySessionMapper;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.RequestContextHolderUtil;
import com.markbro.base.utils.SpringContextHolder;
import com.markbro.base.utils.SysPara;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.base.utils.useragent.UserAgentUtils;
import com.markbro.thirdapi.tencent.TencentApiUtil;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.DeviceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by Administrator on 2018/12/3.
 */

public class MySessionHelper {

    protected static Logger log = LoggerFactory.getLogger(MySessionHelper.class);
    static MySessionMapper mySessionMapper= SpringContextHolder.getBean(MySessionMapper.class);

    public static void login(HttpServletRequest request, HttpServletResponse response, LoginBean lbean, String ip){
        String gentoken = "";
        String session_IP = "";
        String expires_time="";

        // 先去sys_session中查询该用户，如果有就取得表中的token而不用新生成了，这样可以解决不同浏览器共享会话（token）
        Map<String, Object> sessionMap = mySessionMapper.queryUserSessionByYhid(lbean.getYhid());
        if (sessionMap != null) {
            gentoken = String.valueOf(sessionMap.get("token"));
            session_IP = String.valueOf(sessionMap.get("ip"));
            expires_time=String.valueOf(sessionMap.get("expires_time"));
        }
        int ckToken= Token.checkToken(gentoken);
        if (StringUtil.notEmpty(gentoken) && ckToken== TmConstant.TOKEN_OK && session_IP.equals(ip)) {
            //该用户已经登录、token还有效  、同一个电脑（IP）
        } else {//未登录过的用户需要新生成token
            gentoken=Token.genToken(lbean.getYhid());
            expires_time= DateUtil.formatDatetime(DateUtil.addMinute(Integer.valueOf(MyConfig.TOKEN_TIMEOUT)));
        }
        //log.info("gentoken ======" + gentoken);
        String[] arrayOfString = MySessionHelper.insertSessionInfo(lbean.getOrgid(),lbean.getYhid(), ip, gentoken,expires_time);//把用户信息、token保存到数据库
        //log.info("arrayOfString[0]====" + arrayOfString[0] + "\n arrayOfString[1]=====" + arrayOfString[1]);
        if (!"".equals(arrayOfString[0])) {
            if (ip.equals(arrayOfString[1])) {
                //session存储方式：1-放在sys_session中；2-放在cache中 3.存放到redis中
                String session_storage = SysPara.getValue("session_storage","1");
                if (session_storage.equals(TmConstant.NUM_ONE)) {
                    mySessionMapper.deleteUserSession(lbean.getYhid());
                } else {
                    EhCacheUtils.removeSysInfo(TmConstant.YHID_KEY, arrayOfString[0]);
                    EhCacheUtils.removeSysInfo(TmConstant.CACHE_LOGIN_TIME, arrayOfString[0]);
                    EhCacheUtils.removeSysInfo(TmConstant.CACHE_ACTIVE_TIME, arrayOfString[0]);
                    EhCacheUtils.removeSysInfo(TmConstant.IP_KEY, arrayOfString[0]);
                }
            } else {
                String str1 = "在" + arrayOfString[1] + "上登录的用户" + lbean.getDlmc() + "将自动退出";
                request.setAttribute(TmConstant.IP_KEY, arrayOfString[1]);
                request.setAttribute("prompt", str1);
                request.setAttribute("msg", str1);}
        }
        setCookie(response, TmConstant.TokenKey, gentoken, false);
        request.setAttribute(TmConstant.TokenKey, gentoken);

    }
    /**
     * 保存
     *
     * @param response
     * @param key
     * @param value
     * @param ifRemember
     */
    public static void setCookie(HttpServletResponse response, String key, String value, boolean ifRemember) {
        int age = ifRemember?Integer.MAX_VALUE:-1;
        set(response, key, value, null, "/", age, true);
    }
    /**
     * 保存
     *
     * @param response
     * @param key
     * @param value
     * @param maxAge
     */
    private static void set(HttpServletResponse response, String key, String value, String domain, String path, int maxAge, boolean isHttpOnly) {
        Cookie cookie = new Cookie(key, value);
        if (domain != null) {
            cookie.setDomain(domain);
        }
        cookie.setPath(path);
        cookie.setMaxAge(maxAge);
        cookie.setHttpOnly(isHttpOnly);
        response.addCookie(cookie);
    }
    /**
     * 保存Session信息
     *
     * @param yhid
     * @param ip
     * @param token
     * @return
     */
    public static final String KEY_YHID_TOKEN="yhid_token";//根据yhid获得token
    public static final String KEY_TOKEN_YHID="token_yhid";//根据token获得yhid
    public static String[] insertSessionInfo(String orgid,String yhid, String ip, String token,String expires_time) {
        String[] arrayOfString = {"", ""};
        String str1 =  SysPara.getValue("sys_login", "1_3");  //登录控制；1:后登录用户踢出之前登录用户；2：不允许重复登录；3：无限制
        try {
            String tokenOld = "";
            if (str1.indexOf("1") != -1) {
                tokenOld = (String) EhCacheUtils.getSysInfo(TmConstant.CACHE_TOKEN, yhid);//判断该用户是否有重新登录，使用了新token，而导致旧token失效
                if (StringUtil.notEmpty(tokenOld)) {
                    arrayOfString[0] = yhid;
                    arrayOfString[1] = (String) EhCacheUtils.getSysInfo(TmConstant.IP_KEY, tokenOld);
                    EhCacheUtils.putSysInfo(TmConstant.CACHE_GLOBAL_ITOKEN, tokenOld, yhid);
                }
                EhCacheUtils.removeSysInfo(TmConstant.YHID_KEY, token);
                EhCacheUtils.removeSysInfo(TmConstant.CACHE_LOGIN_TIME, tokenOld);
                EhCacheUtils.removeSysInfo(TmConstant.CACHE_ACTIVE_TIME, tokenOld);
                EhCacheUtils.removeSysInfo(TmConstant.IP_KEY, tokenOld);
            }
            EhCacheUtils.putSysInfo(TmConstant.YHID_KEY, token, yhid);//设置token与yhid对应关系,根据yhid获取token
            EhCacheUtils.putSysInfo(TmConstant.CACHE_LOGIN_TIME, token, yhid);
            EhCacheUtils.putSysInfo(TmConstant.CACHE_ACTIVE_TIME, token, System.currentTimeMillis());
            EhCacheUtils.putSysInfo(TmConstant.IP_KEY, token, ip);
            EhCacheUtils.putSysInfo(KEY_YHID_TOKEN, yhid, token);//设置yhidt与对oken应关系,根据yhid获取token
            EhCacheUtils.putSysInfo(KEY_TOKEN_YHID, token, yhid);//设置token与yhid对应关系,根据token获取yhid

            mySessionMapper.updateUserSession(yhid);//sys_session先删除后插入，防止一个用户有多条记录
            mySessionMapper.deleteByToken(token);

            MySessionListener tokenListener=new MySessionListener();


            HttpServletRequest request= RequestContextHolderUtil.getRequest();
            String location=(String)EhCacheUtils.getSysInfo("IP_LOCATION",ip);//IP对应的地址(先从缓存中取，取不到再调用接口查询)
            if("127.0.0.1".equals(ip)||"0:0:0:0:0:0:0:1".equals(ip)){
                location="本地";
            }
            if(StringUtil.isEmpty(location)){
                //调用接口查询ip对应的地址
                if(StringUtil.notEmpty(ip)){
                    location= TencentApiUtil.queryIpLocation(ip);
                    if(StringUtil.notEmpty(location)){
                        EhCacheUtils.putSysInfo("IP_LOCATION",ip,location);
                    }
                }
            }
            Browser browserObj = UserAgentUtils.getBrowser(request);
            String browser=browserObj.getName();//浏览器类型
            String deviceType="Unknown";//设备类型
            DeviceType deviceType1= UserAgentUtils.getDeviceType(request);//是否pc
            if(deviceType1!=null){
                deviceType=deviceType1.getName();
            }
            String os=UserAgentUtils.getUserAgent(request).getOperatingSystem().getName();
            MySession mySession=MySession.builder().yhid(yhid).ip(ip).token(token).expires_time(expires_time)
                    .os(os).deviceType(deviceType).explore(browser).location(location).orgid(orgid).build();

            mySessionMapper.insertUserSession(mySession);
            tokenListener.tokenCreate("insertUserSession",mySession);

        } catch (Exception localException2) {
            String str3 = "缓存用户登录信息时出现异常:" + localException2.toString();
            log.error(str3);
            throw new ApplicationException("缓存用户登录信息时出现异常");
        }
        return arrayOfString;
    }

    /**
     * 退出系统时，清除与token有关的缓存
     * @param token
     */
    public static void removeTokenCache(String token){
        EhCacheUtils.removeSysInfo(TmConstant.CACHE_LOGIN_TIME, token);
        EhCacheUtils.removeSysInfo(TmConstant.CACHE_ACTIVE_TIME, token);
        EhCacheUtils.removeSysInfo(TmConstant.IP_KEY, token);
        EhCacheUtils.removeSysInfo(KEY_TOKEN_YHID, token);
        EhCacheUtils.removeSysInfo(TmConstant.CACHE_GLOBAL_ITOKEN, token);//invalid_token
    }
}
