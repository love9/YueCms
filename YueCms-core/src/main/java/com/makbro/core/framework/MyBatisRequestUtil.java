package com.makbro.core.framework;


import com.makbro.core.base.login.service.LoginService;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.NoLoginException;
import com.markbro.base.model.ILoginService;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.RequestContextHolderUtil;
import com.markbro.base.utils.SpringContextHolder;
import com.markbro.base.utils.cookie.CookieUtils;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.sso.core.SsoConf;
import com.markbro.sso.core.user.SsoUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class MyBatisRequestUtil{

	protected static Logger logger = LoggerFactory.getLogger(MyBatisRequestUtil.class);
	private static final String CACHE_YH_URL = "url";
	private static final String CACHE_YH_CODE = "auth_code";
	 private  static LoginService loginService = SpringContextHolder.getBean("loginService");
	/**
	 * 登录用户是否有某个url操作权限
	 * @param permission 权限url或权限代码code
	 * @return
	 */
	public static boolean hasPermission(String permission){
		LoginBean lb= MyBatisRequestUtil.getLoginUserInfo();
		if(lb.isAdmin()){//操作及管理员有所有权限
			return  true;
		}
		String yhid=lb.getYhid();
		List<String> urls=(List<String>) EhCacheUtils.getUserInfo(CACHE_YH_URL,yhid);
		if(StringUtil.isContains(permission,urls)){//先根据url权限判断
			return  true;
		}else{
			//再根据权限代码判断
			if(permission.startsWith("/")){
				permission=permission.substring(1);
				permission=permission.replaceAll("/",".");
			}
			List<String> codes=(List<String>)EhCacheUtils.getUserInfo(CACHE_YH_CODE,yhid);
			if(codes!=null&&!codes.isEmpty()&& StringUtil.isContains(permission, codes)){
				return  true;
			}
		}
		return  false;
	}

	/**
	 * 登陆用户是否有某个角色
	 * @param role 角色name
	 * @return
	 */
	public static boolean hasRole(String role){
		LoginBean lb= MyBatisRequestUtil.getLoginUserInfo();
		List<Map<String,String>> rolelist=lb.getJsList();

		if(rolelist!=null&&rolelist.size()>0){
			for (Map<String,String> t:rolelist){
				if(role.equals(t.get("mc"))){
					return  true;
				}
			}
		}
		return  false;
	}
	/**
	 * 获取登录用户的LoginBean
	 * @param request
	 * @return
	 */
	public static LoginBean getLoginUserInfo(HttpServletRequest request){
		try{
			Map m=getMap(request);
			String yhid=String.valueOf(m.get(TmConstant.YHID_KEY));
			Object o=EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_USERBEAN,yhid);
			LoginBean lb=(LoginBean)o;
			return lb;
		}catch (Exception ex){
			ex.printStackTrace();
			return null;
		}
	}


	public static LoginBean getLoginUserInfo(){
		try{
			HttpServletRequest request = RequestContextHolderUtil.getRequest();
			Map m=getMap(request);
			String yhid=String.valueOf(m.get(TmConstant.YHID_KEY));
			Object o=EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_USERBEAN,yhid);
			LoginBean lb=(LoginBean)o;
			return lb;
		}catch (Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	public static String getYhid(){
		try{
			HttpServletRequest request = RequestContextHolderUtil.getRequest();
			Map m=getMap(request);
			String yhid=String.valueOf(m.get(TmConstant.YHID_KEY));
			return yhid;
		}catch (Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	/**
	 * request转map（未登录用户不要调用该方法，保证调用该方法的都是授权登录用户。否则会跑出未登录异常）
	 * @param request
	 * @return
	 */
	public  static   Map<String, Object> getMap(HttpServletRequest request) {
		return getMapBase(request,false);
	}

	/**
	 *  request转map
	 * @param request
	 * @return
	 */
	public  static   Map<String, Object> getMapGuest(HttpServletRequest request) {
		return getMapBase(request,true);
	}
	/**
	 * 将HttpServletRequest对象转换成Map
	 * @param request
	 * @return
	 */
	private static   Map<String, Object> getMapBase(HttpServletRequest request,boolean guestFlag) {
		Map mapReq = new HashMap();
		Enumeration enu = request.getParameterNames();
		while (enu.hasMoreElements()) {
			String paramName = (String) enu.nextElement();
			String[] values = request.getParameterValues(paramName);
			String value = "";
			for (int i = 0; i < values.length; i++) {
				value = value + (i == 0 ? "" : "~") + values[i];
			}
			if(paramName.startsWith("cx_")){
				paramName=paramName.replace("cx_","");
			}
			mapReq.put(paramName, value);
		}

		String yhid ="";
		if(!GlobalConfig.SSO_ENABLE) {//原来的非sso模式
			yhid = (String) request.getSession().getAttribute(TmConstant.KEY_LOGIN_USER);
			if (StringUtil.isEmpty(yhid)) {
				//String sysToken = TmConstant.NUM_ZERO;//0采用原有cookie方式（默认方式）
				String token = CookieUtils.getCookie(request, TmConstant.TokenKey);
				yhid = loginService.getYhidByToken(token);
				/*if (StringUtil.isEmpty(yhid)) {
					yhid = (String) request.getAttribute(TmConstant.YHID_KEY);
				}*/
			}
		}else{
			//sso 模式
			yhid = (String) request.getAttribute(TmConstant.YHID_KEY);

			if (StringUtil.isEmpty(yhid)) {
				 SsoUser ssoUser =(SsoUser)request.getSession().getAttribute(SsoConf.SSO_USER);
				if(ssoUser!=null){
					yhid = ssoUser.getUserid();
				}
			}
		}

		if (StringUtil.notEmpty(yhid)) {
			mapReq.put("yhid", yhid);
			Object o = EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_USERBEAN, yhid);
			if (o == null) {
				o = loginService.cacheInfo(yhid);
			}
			if (o != null) {
				LoginBean loginBean = (LoginBean) o;
				if (loginBean != null) {
					mapReq.put(TmConstant.BMID_KEY, loginBean.getBmid());
					mapReq.put(TmConstant.GWID_KEY, loginBean.getGwid());
					mapReq.put(TmConstant.ORGID_KEY, loginBean.getOrgid());
				}
			}
		} else {
			if (!guestFlag) {
				throw new NoLoginException();
			}
		}

		String names= (String)request.getAttribute(TmConstant.Request_Referer_Name);
		if(StringUtil.notEmpty(names)){
			String[] arr=null;
			if(names.contains("#")){
				arr=names.split("#");
			}else{
				arr=new String[1];
				arr[0]=names;
			}
			if(arr!=null&&arr.length>0){
				for(String name:arr){
					if(StringUtil.notEmpty(name)){
						String v=(String)request.getAttribute(name);
						mapReq.put(name,v);
					}
				}
			}
		}
		return mapReq;
	}

	/**
	 * 通过http请求获得客户端真实ip地址
	 * @param request
	 * @return String
	 */
	public static String getIpByHttpRequest(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (ip.contains(",")) {//如果有多个IP,只取第一个
			String[] arrIp = ip.split(",");
			ip = arrIp[0];
		}
		return ip;
	}

	public static Map<String, Object> requestToMap(HttpServletRequest req) {
		Map mapReq = new HashMap();
		Enumeration enu = req.getParameterNames();
		while (enu.hasMoreElements()) {
			String paramName = (String) enu.nextElement();
			String[] values = req.getParameterValues(paramName);
			String value = "";
			for (int i = 0; i < values.length; i++) {
				value = value + (i == 0 ? "" : "~") + values[i];
			}
			if(paramName.startsWith("cx_")){
				paramName=paramName.replace("cx_","");
			}
			mapReq.put(paramName, value);
		}
		return mapReq;
	}

}
