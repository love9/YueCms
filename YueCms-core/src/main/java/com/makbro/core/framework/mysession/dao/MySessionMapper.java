package com.makbro.core.framework.mysession.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.framework.mysession.bean.MySession;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 本系统自己的会话管理
 */
@Repository
public interface MySessionMapper {

    public String queryUserSession(@Param("token") String token);
    public Map<String,Object> queryUserSessionByYhid(@Param("yhid") String yhid);
    public int updateUserSessionATime(@Param("token") String token);
    public int updateToken(@Param("oldToken") String oldToken, @Param("newToken") String newToken, @Param("expires_time") String expires_time);
    public String queryLoginToken(@Param("yhid") String yhid);

    public int updateUserSession(@Param("yhid") String yhid);

    public int deleteUserSession(@Param("yhid") String yhid);
    public int deleteUserSessionToken(@Param("yhid") String yhid, @Param("token") String token);
    public int insertUserSession(MySession mySession);

    public List<MySession> find(PageBounds pageBounds, Map<String, Object> queryMap);
    public void deleteByToken(@Param("token") String token);

    public List<MySession> findExpires();//查询已经过期的记录，定时任务定时清理这些过期的会话

    //查询sys_session表中有效记录的行数，是当前系统在线人数
    public int queryOnlineNum();
}
