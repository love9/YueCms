package com.makbro.core.framework.listener;

import com.alibaba.fastjson.JSON;
import com.makbro.core.framework.mysession.MySessionHelper;
import com.makbro.core.framework.mysession.bean.MySession;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.utils.EhCacheUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by Administrator on 2018/8/1.
 */
public class MySessionListener implements ITokenListener {
    protected Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public void tokenCreate(Object event, Object arg) {
        logger.info("tokenCreate====>event:{0},arg:{1}",String.valueOf(event), JSON.toJSON(arg));

        Integer num =(Integer) EhCacheUtils.getSysInfo(TmConstant.NUM_ONLINE);
        if(num==null){
            num=0;
        }
        num++;
        EhCacheUtils.putSysInfo(TmConstant.NUM_ONLINE,num);
    }

    @Override
    public void tokenDelete(Object event, Object arg) {
        logger.info("tokenDelete====>event:{0},arg:{1}",String.valueOf(event), JSON.toJSON(arg));
        MySession mySession=(MySession) arg;
        MySessionHelper.removeTokenCache(mySession.getToken());
        Integer num =(Integer) EhCacheUtils.getSysInfo(TmConstant.NUM_ONLINE);
        if(num!=null&&num>0){
            num--;
        }else{
            num=0;
        }
        EhCacheUtils.putSysInfo(TmConstant.NUM_ONLINE,num);
    }
}
