package com.makbro.core.framework.authz.annotationHandler;


import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresRoles;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.exception.ForbiddenException;
import com.markbro.base.exception.UnAuthorizedException;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Stream;

/**
 * 角色注解校验
 */
public class RoleAnnotationHandler extends AbstractAuthorizingAnnotationHandler {
    private boolean checkRole(AuthorizingSubject subject, Logical logical, String... roles){

        boolean returnBool = false;
        if(subject==null){
            throw new UnAuthorizedException("RoleAnnotationHandler error! subject argument is null!");
        }
        try{
           /* List<Map<String,String>> jsList=lb.getJsList();
            List<String> jsmcs=new ArrayList<String>();
            for(Map<String,String> t:jsList){
                String jsMc=t.get("mc");
                jsmcs.add(jsMc);
            }*/
            List<String> jsmcs=subject.getRoles();

                if(CollectionUtils.isNotEmpty(jsmcs)){
                    if(logical==Logical.OR){
                        for(String  mc:jsmcs){
                            if(Stream.of(roles).anyMatch(s->{
                                return s.equals(mc);
                            })){
                                returnBool = true;
                                break;
                            }
                        }
                    }else{
                        boolean thisflag=true;
                        for(String s:roles){//指定的每个角色都需要符合
                            boolean b=  StringUtil.isContains(s, jsmcs);
                            if(!b){
                                thisflag=false;
                                break;
                            }
                        }
                        returnBool = thisflag;
                    }
                }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return returnBool;
    }
    public RoleAnnotationHandler() {
        super(RequiresRoles.class);
    }
    @Override
    public boolean assertAuthorized(HttpServletRequest request,Annotation a,AuthorizingSubject subject) throws ApplicationException {
        if(a instanceof RequiresRoles) {
            RequiresRoles rrAnnotation = (RequiresRoles)a;
            String[] roles = rrAnnotation.value();
            boolean b=this.checkRole(subject,rrAnnotation.logical(),roles);
            if(!b){
                String code="";
                if(rrAnnotation.logical()==Logical.AND){
                    code=StringArray2String("、",roles);
                }else{
                    code=StringArray2String("|",roles);
                }
                String msg="用户["+subject.getAccount()+"]没有权限访问["+request.getRequestURI()+"],需要角色："+code;
                log.warn(msg);
                throw new ForbiddenException("您需要["+code+"]角色权限!");
            }else{
                return true;
            }
        }
        return false;
    }

}
