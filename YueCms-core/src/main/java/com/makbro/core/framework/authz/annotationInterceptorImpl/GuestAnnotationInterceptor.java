package com.makbro.core.framework.authz.annotationInterceptorImpl;


import com.makbro.core.framework.authz.annotationHandler.GuestAnnotationHandler;
import com.makbro.core.framework.authz.common.AnnotationResolver;

/**
 * Created by Administrator on 2018/12/5.
 */
public class GuestAnnotationInterceptor extends AbstractAuthorizingAnnotationInterceptor {

    public GuestAnnotationInterceptor() {
        super(new GuestAnnotationHandler());
    }

    public GuestAnnotationInterceptor(AnnotationResolver resolver) {
        super(new GuestAnnotationHandler(), resolver);
    }
}
