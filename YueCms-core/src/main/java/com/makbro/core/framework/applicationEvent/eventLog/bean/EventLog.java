package com.makbro.core.framework.applicationEvent.eventLog.bean;


import com.markbro.base.model.AliasModel;

/**
 * 系统事件日志 bean
 * @author wujiyue
 * @date 2018-11-20 21:48:32
 */
public class EventLog implements AliasModel {

	private Integer id;//
	private String orgid;//组织机构ID
	private String event;//系统事件
	private String eventName;//事件名称
	private String source;//事件来源
	private String params;//参数
	private String execTime;//发生时间

	public static EventLog build() {
		return new EventLog();
	}

	public Integer getId(){ return id ;}
	public EventLog setId(Integer id){this.id=id; return this;}
	public String getOrgid(){ return orgid ;}
	public EventLog setOrgid(String orgid){this.orgid=orgid; return this;}
	public String getEvent(){ return event ;}
	public EventLog setEvent(String event){this.event=event; return this;}
	public String getEventName(){ return eventName ;}
	public EventLog setEventName(String eventName){this.eventName=eventName; return this;}
	public String getSource(){ return source ;}
	public EventLog setSource(String source){this.source=source; return this;}
	public String getParams(){ return params ;}
	public EventLog setParams(String params){this.params=params;return this;}
	public String getExecTime(){ return execTime ;}
	public EventLog setExecTime(String execTime){this.execTime=execTime; return this;}


	@Override
	public String toString() {
	return "EventLog{" +
			"id=" + id+
			", orgid=" + orgid+
			", event=" + event+
			", eventName=" + eventName+
			", source=" + source+
			", params=" + params+
			", execTime=" + execTime+
			 '}';
	}
}
