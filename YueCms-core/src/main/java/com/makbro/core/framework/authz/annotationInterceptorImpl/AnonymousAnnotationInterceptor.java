package com.makbro.core.framework.authz.annotationInterceptorImpl;


import com.makbro.core.framework.authz.annotationHandler.AnonymousAnnotationHandler;
import com.makbro.core.framework.authz.common.AnnotationResolver;

/**
 * Created by Administrator on 2018/12/5.
 */
public class AnonymousAnnotationInterceptor extends AbstractAuthorizingAnnotationInterceptor {

    public AnonymousAnnotationInterceptor() {
        super(new AnonymousAnnotationHandler());
    }

    public AnonymousAnnotationInterceptor(AnnotationResolver resolver) {
        super(new AnonymousAnnotationHandler(), resolver);
    }
}

