package com.makbro.core.framework.freemark.tag;

import com.makbro.core.base.system.service.WebsiteService;
import com.makbro.core.framework.freemark.BaseTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
@Component
public class CustomTags extends BaseTag {

    @Autowired
    WebsiteService websiteService;

    public CustomTags() {
        super(CustomTags.class.getName());
    }

    public Object siteInfo(Map params) {
        Map map=websiteService.getSiteInfo();
        return map;
    }
}
