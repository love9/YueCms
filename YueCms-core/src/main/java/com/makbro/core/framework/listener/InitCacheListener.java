package com.makbro.core.framework.listener;

/**
 * 初始化缓存信息
 */
public interface InitCacheListener {
    //bean被创建时
    public void onInit();
}
