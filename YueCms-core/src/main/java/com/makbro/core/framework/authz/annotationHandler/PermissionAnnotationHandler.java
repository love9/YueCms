package com.makbro.core.framework.authz.annotationHandler;


import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.markbro.base.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.util.List;

/**
 * 权限注解校验
 */
public class PermissionAnnotationHandler extends AbstractAuthorizingAnnotationHandler {
    public PermissionAnnotationHandler() {
        super(RequiresPermissions.class);
    }

    @Override
    public boolean assertAuthorized(HttpServletRequest request, Annotation a, AuthorizingSubject subject) throws ApplicationException {
        if(a instanceof RequiresPermissions) {
            String uri = request.getServletPath();
            RequiresPermissions rrAnnotation = (RequiresPermissions) a;
            String[] pers = rrAnnotation.value();
            boolean b=this.checkAuthByAnno(subject, rrAnnotation.logical(), pers);
            if(!b){
                //这里不符合RequiresPermissions 先不抛出异常，因为后面还要校验是否管理员和url权限，所以返回false.

                /*String code="";
                if(rrAnnotation.logical()==Logical.AND){
                    code=StringArray2String("、",pers);
                }else{
                    code=StringArray2String("|",pers);
                }
                String msg="用户["+this.getSubject().getDlmc()+"]没有权限访问["+uri+"],需要权限："+code;
                log.warn(msg);
                throw new ForbiddenException("请求拒绝访问!您需要权限代码：" + code);*/
            }
            return b;
        }
        return false;
    }
    private boolean checkAuthByAnno(AuthorizingSubject subject, Logical logical, String... pers){
       // List<String> list = (List<String>) EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_CODE,loginBean.getYhid());
        List<String> list =subject.getCodes();
        if(logical==Logical.OR){
            // OR 逻辑
            boolean returnBool = false;
            try {
                //用户所拥有的权限代码
                if(list!=null&&list.size()>0){
                    for (String per : pers) {
                        //如果有一个权限代码用户拥有，返回true
                        if(list.stream().anyMatch(s->{return  s.equals(per);})) {
                            returnBool = true;
                            break;
                        }
                    }
                }
            }catch (Exception ex){
                return  false;
            }
            return returnBool;
        }else{
            // AND 逻辑
            boolean returnBool = true;
            try {
                //用户所拥有的权限代码
                if(list!=null&&list.size()>0){
                    for (String per : pers) {
                        //如果有一个权限代码用户不具备，返回false
                        if(!(list.stream().anyMatch(s->{return  s.equals(per);}))) {
                            returnBool = false;
                            break;
                        }
                    }
                }else{
                    return  false;
                }
            }catch (Exception ex){
                return  false;
            }
            return returnBool;
        }

    }
}
