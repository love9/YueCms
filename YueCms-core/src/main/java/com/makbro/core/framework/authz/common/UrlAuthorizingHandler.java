package com.makbro.core.framework.authz.common;


import com.markbro.base.exception.ApplicationException;
import com.markbro.base.exception.ForbiddenException;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2018/12/6.
 */
public class UrlAuthorizingHandler extends AbstractAuthorizingHandler {

    @Override
    public boolean assertAuthorized(HttpServletRequest request,AuthorizingSubject subject) throws ApplicationException {
        Assert.notNull(subject);
        String uri=request.getRequestURI();
        List<String> urls=subject.getUrls();//登录用户拥有的url权限
        if(subject.isAdmin()||subject.isSystemAdmin()){
                return true;//管理员能访问所有url
        }else{
            //不是管理员

            if(!(uri.indexOf("/json/")>0)){
                if(checkAuthByUrl(urls,uri)){
                    return true;
                }else{
                    throw new ForbiddenException();
                }
            }else{
                //1.基础的数据接口使系统用户就可以调用。而非基础信息接口只有在有相应主权限才能调用。比如具体的增加（/project/mission/add）删除(/project/mission/delete)修改(/project/mission/edit)
                //需要具体授权，而其它json数据接口比如/project/mission/json/find接口或/project/mission/json/select或/project/mission/json/tree只需要需要/project/mission主接口权限既可以了。
                //2.基础数据接口是要是系统用户就可以调用，而不要具体授权。比如/base/dictionary/json/select.而哪些接口是基础数据接口。可以在数据库中配置指定。
                String mainuri = uri.substring(0,uri.indexOf("/json/"));
                if((uri.indexOf("/json/save")>0)){
                    //增删改需要具体授权  /json/save需要检测/add权限或/edit权限 /json/remove或/json/delete接口需要检测/delete权限
                    if(checkAuthByUrl(urls,mainuri + "/add")||checkAuthByUrl(urls,mainuri + "/edit")){
                    }else{
                        throw new ForbiddenException();
                    }
                }else if((uri.indexOf("/json/remove")>0)||(uri.indexOf("/json/delete")>0)){
                    if(checkAuthByUrl(urls,mainuri + "/delete")){
                    }else{
                        throw new ForbiddenException();
                    }
                }else{
                    //其它的json接口只需要主授权
                    if(checkAuthByUrl(urls,mainuri)){
                    }else{
                        throw new ForbiddenException();
                    }
                }
                return true;
            }

        }
    }

    //根据url检测权限
    private boolean checkAuthByUrl(List<String> urls,String uri){
        boolean returnBool = false;
        //List<String> list = (List<String>) EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_URL, this.getSubject().getYhid());
        for (String s : urls) {
            if (s.indexOf(uri) >= 0) {
                returnBool = true;
                break;
            }
        }
        return returnBool;
    }
}
