package com.makbro.core.framework.authz.annotationHandler;


import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.exception.ForbiddenException;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * Created by Administrator on 2018/12/5.
 */
public class AuthenticationAnnotationHandler extends AbstractAuthorizingAnnotationHandler {


    public AuthenticationAnnotationHandler() {
        super(RequiresAuthentication.class);
    }

    @Override
    public boolean assertAuthorized(HttpServletRequest request, Annotation a, AuthorizingSubject subject) throws ApplicationException {
        if(a instanceof RequiresAuthentication) {
            String uri = request.getServletPath();
            RequiresAuthentication rrAnnotation = (RequiresAuthentication) a;
            if(subject==null){
                //当前用户没有登录则抛出异常
                String msg="您没有权限访问["+request.getRequestURI()+"],请登录后访问!";
                log.warn(msg);
                throw new ForbiddenException(msg);
            }else{
                return true;
            }
        }
        return false;
    }
}
