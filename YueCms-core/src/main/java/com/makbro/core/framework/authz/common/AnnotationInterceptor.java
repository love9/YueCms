package com.makbro.core.framework.authz.common;


import com.makbro.core.framework.authz.annotationHandler.AbstractAuthorizingAnnotationHandler;
import org.springframework.web.method.HandlerMethod;

import java.lang.annotation.Annotation;

/**
 * Created by Administrator on 2018/12/5.
 */
public abstract class AnnotationInterceptor {

    private AbstractAuthorizingAnnotationHandler handler;
    private AnnotationResolver resolver;

    public AnnotationInterceptor(AbstractAuthorizingAnnotationHandler handler) {
        this(handler, new DefaultAnnotationResolver());
    }

    public AnnotationInterceptor(AbstractAuthorizingAnnotationHandler handler, AnnotationResolver resolver) {
        if(handler == null) {
            throw new IllegalArgumentException("AuthorizingAnnotationHandler argument cannot be null.");
        } else {
            this.setHandler(handler);
            this.setResolver((AnnotationResolver)(resolver != null?resolver:new DefaultAnnotationResolver()));
        }
    }

    protected Annotation getAnnotation(HandlerMethod method) {

        return this.getResolver().getAnnotation(method, this.getHandler().getAnnotationClass());
    }
    public AbstractAuthorizingAnnotationHandler getHandler() {
        return handler;
    }

    public void setHandler(AbstractAuthorizingAnnotationHandler handler) {
        this.handler = handler;
    }

    public AnnotationResolver getResolver() {
        return resolver;
    }

    public void setResolver(AnnotationResolver resolver) {
        this.resolver = resolver;
    }
}
