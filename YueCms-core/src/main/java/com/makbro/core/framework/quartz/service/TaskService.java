package com.makbro.core.framework.quartz.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.listener.InitCacheListener;
import com.makbro.core.framework.quartz.bean.Task;
import com.makbro.core.framework.quartz.dao.TaskMapper;
import com.makbro.core.framework.quartz.quartz.QuartzManager;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 调度任务 Service
 * @author wujiyue
 * @date 2018-09-28 18:26:30
 */
@Service
public class TaskService implements InitCacheListener {

    private Logger logger = LoggerFactory.getLogger(getClass());



    public static final String JOB_STATUS_RUNNING="1";//运行中
    public static final String JOB_STATUS_STOP="0";//关闭中
    public static final String JOB_STATUS_PAUSE="2";//暂停中
    @Autowired
    private TableKeyService keyService;
    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    QuartzManager quartzManager;

     /*基础公共方法*/
    public Task get(Integer id){
        return taskMapper.get(id);
    }
    public List<Task> find(PageBounds pageBounds,Map<String,Object> map){
        return taskMapper.find(pageBounds,map);
    }
    public List<Task> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return taskMapper.findByMap(pageBounds,map);
    }
    public void add(Task task){
        taskMapper.add(task);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();

          Task task= JSON.parseObject(JSON.toJSONString(map),Task.class);
          if(task.getId()==null||"".equals(task.getId().toString())){
              Integer id= keyService.getIntegerId();
              task.setId(id);
              taskMapper.add(task);
          }else{
              if(task.getAvailable()==1){
                  quartzManager.updateJob(task.getJob_name(),task.getJob_group(),task.getCron_expression());
              }
              taskMapper.update(task);
          }
            msg.setType(Msg.MsgType.success);
            msg.setContent("保存信息成功");
            loadCache();
            return msg;
    }


    public void updateByMap(Map<String,Object> map){
        taskMapper.updateByMap(map);
    }

    public void delete(Integer id){
        taskMapper.delete(id);
        loadCache();
    }


     /*自定义方法*/

    /**
     * 开启或关闭任务
     * @param map
     *  map内参数：id(调度任务id)
     * @return
     */
    public Object openOrClose(Map<String,Object> map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.success);
        String id=String.valueOf(map.get(TmConstant.ID_KEY));
        Task task=taskMapper.get(Integer.valueOf(id));

        if(task!=null){
            String state=task.getJob_status();//当前的状态,这个状态不能根据数据库值为依据，应该以实际值
            if(JOB_STATUS_RUNNING.equals(state)||JOB_STATUS_PAUSE.equals(state)){//当前是运行中或当前是暂停
                quartzManager.deleteJob(task.getJob_name(),task.getJob_group());
                msg.setContent("关闭任务成功!");
                taskMapper.updateJobStatus(task.getId().toString(),JOB_STATUS_STOP);
                return msg;
            }
            else{//当前是关闭状态

                Map jobDataMap=new HashMap<String,Object>();
                if(StringUtil.notEmpty(task.getJob_data())){
                    jobDataMap=(Map) JSONObject.parse(task.getJob_data());
                }

                try {
                    quartzManager.addJob(QuartzManager.getClass(task.getBean_class()),task.getJob_name(),task.getJob_group(),task.getCron_expression(),task.getDescription(),jobDataMap);
                } catch (Exception e) {
                    e.printStackTrace();
                    return Msg.error("任务启动失败!"+e.getMessage());
                }


                taskMapper.updateJobStatus(task.getId().toString(),JOB_STATUS_RUNNING);
                msg.setContent("启动任务成功!");
                return msg;
            }

        }else{
            msg.setType(Msg.MsgType.error);
            msg.setContent("启动或停止失败!参数异常!");
            return msg;
        }
    }

    /**
     * 运行或暂停任务
     * @param map
     *  map内参数：id(调度任务id)
     * @return
     */
    public Object runOrPause(Map<String,Object> map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.success);
        String id=String.valueOf(map.get(TmConstant.ID_KEY));
        Task task=taskMapper.get(Integer.valueOf(id));
        if(task!=null){

            //这里不根据数据库值判断，而根据该任务实际运行状态
            Trigger.TriggerState triggerState = quartzManager.getJob_status(task.getJob_name(), task.getJob_group());
            if(triggerState.equals(Trigger.TriggerState.NORMAL)){//运行中
                quartzManager.pauseJob(task.getJob_name(), task.getJob_group());
                taskMapper.updateJobStatus(task.getId().toString(),JOB_STATUS_PAUSE);
                msg.setContent("暂停任务成功!");
                return msg;
            }else if(triggerState.equals(Trigger.TriggerState.PAUSED)){//暂停
                quartzManager.resumeJob(task.getJob_name(), task.getJob_group());
                taskMapper.updateJobStatus(task.getId().toString(),JOB_STATUS_RUNNING);
                msg.setContent("启动任务成功!");
                return msg;
            }else{
                quartzManager.resumeJob(task.getJob_name(), task.getJob_group());
                taskMapper.updateJobStatus(task.getId().toString(),JOB_STATUS_RUNNING);
                msg.setContent("启动任务成功!");
                return msg;
            }


        }else{
            msg.setType(Msg.MsgType.error);
            msg.setContent("运行或暂停失败!没有该任务!");
            return msg;
        }

    }

    public Object updateAvailable(Map<String,Object> map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.success);
        String id=String.valueOf(map.get(TmConstant.ID_KEY));
        Task task=taskMapper.get(Integer.valueOf(id));
        if(task!=null){
            int v=task.getAvailable();
            if(v==1){
                msg.setContent("禁用成功!");
                task.setAvailable(0);
                quartzManager.deleteJob(task.getJob_name(),task.getJob_group());
                taskMapper.updateJobStatus(task.getId().toString(),JOB_STATUS_STOP);
                task.setJob_status(JOB_STATUS_STOP);
            }else{
                msg.setContent("启用成功!");
                task.setAvailable(1);
                quartzManager.deleteJob(task.getJob_name(),task.getJob_group());

                Map jobDataMap=new HashMap<String,Object>();
                if(StringUtil.notEmpty(task.getJob_data())){
                    jobDataMap=(Map) JSONObject.parse(task.getJob_data());
                }


                try {
                    quartzManager.addJob(QuartzManager.getClass(task.getBean_class()),task.getJob_name(),task.getJob_group(),task.getCron_expression(),task.getDescription(),jobDataMap);
                } catch (Exception e) {
                    e.printStackTrace();
                    return Msg.error("任务启动失败!"+e.getMessage());
                }
                task.setJob_status(JOB_STATUS_RUNNING);
            }
            taskMapper.update(task);
            this.loadCache();
            return msg;
        }
        msg.setType(Msg.MsgType.error);
        msg.setContent("更新可用状态失败!");
        return msg;
    }

    /**
     * 获得所有任务
     * @param map
     * @return
     */
    public Object queryAllJob(Map<String,Object> map){
      return   quartzManager.queryAllJob();
    }

    /**
     * 获得正在执行没运行完的任务（而不是在打开轮训中的任务）
     * @param map
     * @return
     */
    public Object queryRunJon(Map<String,Object> map){
        return   quartzManager.queryRunJon();
    }
    //测试用获得任务状态
    public Object getJobState(Map<String,Object> map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.success);
        String id=String.valueOf(map.get(TmConstant.ID_KEY));
        Task task=taskMapper.get(Integer.valueOf(id));
        if(task!=null){
            Trigger.TriggerState triggerState = quartzManager.getJob_status(task.getJob_name(), task.getJob_group());
            if(triggerState!=null){
                msg.setType(Msg.MsgType.success);
                msg.setContent("getJobState成功!");
                msg.setExtend("state",triggerState.name());
                return msg;
            }
        }
            msg.setType(Msg.MsgType.error);
            msg.setContent("getJobState失败!");
            return msg;
    }


    /**
     * 立即执行一次
     * @param map
     * @return
     */
    public Object runOne(Map<String,Object> map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.success);
        String id=String.valueOf(map.get(TmConstant.ID_KEY));
        Task task=taskMapper.get(Integer.valueOf(id));
        if(task!=null){
            try{
                quartzManager.runAJobNow(task.getJob_name(),task.getJob_group());
            }catch (SchedulerException e){
                e.printStackTrace();
                String eMessage=e.getMessage();
                if(e.getClass().getName().contains("JobPersistenceException") && eMessage.contains("referenced by the trigger does not exist")){
                    String name=StringUtil.trim_mid_exclu(eMessage,"(",")");

                    return Msg.error("指定任务"+(StringUtil.notEmpty(name)?name:"")+"不存在!");
                }
            }
            //applicationEventManager.trigger(ApplicationEventDefined.ON_SCHEDULER_EXECUTED_BY_HAND,this,map);//触发系统定义事件
            msg.setContent("操作成功!");
            return msg;
        }
        msg.setType(Msg.MsgType.error);
        msg.setContent("操作失败!");
        return msg;
    }

    public static final String KEY_TASKS_LIST="taskList";
    /**
     * 重新加载放入缓存的值
     * 把表中task放入到缓存中
     */
    private void loadCache(){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("available",1);
        List<Task> list = taskMapper.find(new PageBounds(),map);
        if(CollectionUtils.isNotEmpty(list)){
            EhCacheUtils.putSysInfo(KEY_TASKS_LIST,list);
        }
    }

    @Override
    public void onInit() {
        loadCache();
    }
}
