package com.makbro.core.framework.mysession.bean;

import lombok.Builder;
import lombok.Data;

/**
 * 本系统自己的会话
 * @author wujiyue
 */
@Data
@Builder
public class MySession {

    private String orgid;//
    private String yhid;//
    private String ip;//
    private String token;//
    private String location;//登录地点
    private String deviceType;//设备类型
    private String explore;//浏览器类型
    private String os;//操作系统
    private String login_time;//
    private String active_time;//
    private String expires_time;
    private String deleted;//




}
