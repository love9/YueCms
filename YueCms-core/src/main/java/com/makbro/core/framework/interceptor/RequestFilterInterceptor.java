package com.makbro.core.framework.interceptor;

import com.google.common.base.Stopwatch;
import com.makbro.core.framework.authz.AuthorizingInterceptors;
import com.makbro.core.framework.authz.common.AuthorizingSubject;
import com.makbro.core.base.login.service.LoginService;
import com.makbro.core.framework.interceptor.path.impl.AntPathMatcher;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ForbiddenException;
import com.markbro.base.exception.NoLoginException;
import com.markbro.base.exception.UnAuthorizedException;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * 请求过滤拦截器
 * 拦截所有非静态资源请求，判断是否有访问权限。
 * 支持权限注解。
 */
public class RequestFilterInterceptor extends HandlerInterceptorAdapter {
    Logger log= LoggerFactory.getLogger(RequestFilterInterceptor.class);
    public String[] allowUrls;
    private static final AntPathMatcher antPathMatcher = new AntPathMatcher();
    public void setAllowUrls(String... allowUrls) {
        this.allowUrls = allowUrls;
    }
    public RequestFilterInterceptor() {

    }
    public RequestFilterInterceptor(String[] allowUrls) {
        this.allowUrls = allowUrls;
    }

    @Autowired
    LoginService loginService;



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 创建自动start的计时器
        Stopwatch watch = Stopwatch.createStarted();

        request.setCharacterEncoding("UTF-8");
        String uri = request.getServletPath();
        if(!(handler instanceof HandlerMethod) ){
            return true;
        }
        HandlerMethod handlerMethod=(HandlerMethod) handler;

        if(isStaticResources(uri) || urlMatch(uri, allowUrls)){
            return true;
        }
        String method  =  handlerMethod.getMethod().getName();
        String yhid="";
        yhid=String.valueOf(request.getSession().getAttribute(TmConstant.KEY_LOGIN_USER));

        if(StringUtil.notEmpty(yhid)){
            request.setAttribute(TmConstant.YHID_KEY, yhid);
            log.info("从session获取登录用户ID：" + yhid);
        }



        LoginBean loginBean=null;
        loginBean=loginService.loginChecked(request,response);




        //构造下面校验注解权限的用户信息权限bean  AuthorizingSubject
        AuthorizingSubject subject = getAuthorizingSubject(loginBean);

        if (subject == null) {

            boolean anonymousFlag= AuthorizingInterceptors.allowAnonymous(subject,request, handlerMethod);//登录与否都可访问
            if(anonymousFlag){
                return true;
            }
            boolean  invalid = loginService.isInvalidToken(request);
            if(invalid){
               throw new NoLoginException("您登录信息已经失效，请重新登录！");
            }
           throw  new NoLoginException("请先登录系统!");
       }

        try {
           AuthorizingInterceptors.invoke(subject,request, handlerMethod);
        } catch (ForbiddenException e) {
            log.warn("没有权限访问服务：" + uri + " "+e.getMessage());
            throw e;
        } catch (UnAuthorizedException e) {
            log.warn("没有权限访问服务的方法：" + uri + " " + " " + method);
            responseString(response,"{alert:'您没有执行method:" + method + "的权限'}",null);
        }


        long timeT = watch.elapsed(TimeUnit.MILLISECONDS);
        log.info("request uri>>>>" +request.getRequestURI()+">>>>>代码执行总时长："+ timeT);
        return true;
    }
    private AuthorizingSubject getAuthorizingSubject(LoginBean loginBean){
        if(loginBean==null) {
            return null;
        }
        AuthorizingSubject subject=new AuthorizingSubject();
        subject.setAccount(loginBean.getDlmc());
        List<String> urls = (List<String>) EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_URL, loginBean.getYhid());
        subject.setUrls(urls);
        List<String> codes = (List<String>) EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_CODE, loginBean.getYhid());
        subject.setCodes(codes);
        Object o=EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_USERBEAN, loginBean.getYhid());
        LoginBean lb=(LoginBean)o;
        subject.setData(lb);
        subject.setAdmin(lb.isAdmin());
        subject.setSystemAdmin(lb.isSystemAdmin());
        List<Map<String, String>> jsList = loginBean.getJsList();
        List<String> roles=new ArrayList<>();
        for (Map t : jsList) {
            String jsmc = String.valueOf(t.get("mc"));
            if(StringUtil.notEmpty(jsmc)){
                roles.add(jsmc);
            }
        }
        subject.setRoles(roles);
        return subject;
    }
    protected String responseString(HttpServletResponse response, String msg,String contentType) {
        try {
            response.reset();
            response.setContentType(contentType!=null?contentType:"text/html;charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(msg);
            writer.close();
            return null;
        } catch (IOException e) {
            return null;
        }
    }
    private boolean urlMatch(String servletPath,String... paths){
        for (String path:paths) {
            String uriPattern = path.trim();
            // 支持ANT表达式
            if (antPathMatcher.match(uriPattern, servletPath)) {
                return true;
            }
        }
        return false;
    }
    //静态资源
    public static boolean isStaticResources(String uri){
        if(uri.indexOf("/resources/")>=0|| uri.indexOf("/front/")>=0||uri.endsWith(".js")||uri.endsWith(".css")||uri.endsWith(".jpg")||uri.endsWith(".jpeg")||uri.endsWith(".png")||uri.endsWith(".bmp")||uri.endsWith(".gif")||uri.endsWith(".eot")||uri.endsWith(".svg")||uri.endsWith(".ttf")||uri.endsWith(".woff")){
            return true;
        }else{
            return false;
        }
    }

}
