package com.makbro.core.framework.quartz.web;


import com.makbro.core.framework.quartz.bean.Task;
import com.makbro.core.framework.quartz.service.TaskService;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 调度任务管理
 * @author wujiyue
 * @date 2018-09-28 18:26:31
 */

@Controller
@RequestMapping("/sys/task")
public class TaskController extends BaseController {
    @Autowired
    protected TaskService taskService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/sys/task/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Task task, Model model){
                return "/sys/task/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Task task,Model model){
        if(task!=null&&task.getId()!=null){
            task=taskService.get(task.getId());
        }
         model.addAttribute("task",task);
         return "/sys/task/edit";
    }
    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return taskService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return taskService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));

    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return taskService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除调度任务")
    public Object delete(@PathVariable Integer id) {
    	try{
            if(id<=10){
                return Msg.error("您不能删除系统自带的任务！");
            }
            taskService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }



    /**
     * 开启或关闭任务
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/json/openOrClose",method = RequestMethod.POST)
    public Object startOrStop() {
        Map map=MyBatisRequestUtil.getMap(request);
        return taskService.openOrClose(map);
    }

    /**
     * 运行或暂停任务
     * @return
     */
    @ResponseBody
     @RequestMapping(value="/json/runOrPause",method = RequestMethod.POST)
     public Object runOrPause() {
        Map map=MyBatisRequestUtil.getMap(request);
        return taskService.runOrPause(map);
    }

    @ResponseBody
    @RequestMapping(value="/json/queryAllJob",method = RequestMethod.POST)
    public Object queryAllJob() {
        Map map=MyBatisRequestUtil.getMap(request);
        return taskService.queryAllJob(map);
    }


    @ResponseBody
    @RequestMapping(value="/json/queryRunJon",method = RequestMethod.POST)
    public Object queryRunJon() {
        Map map=MyBatisRequestUtil.getMap(request);
        return taskService.queryRunJon(map);
    }

    @ResponseBody
    @RequestMapping(value="/json/runOne",method = RequestMethod.POST)
    public Object getJobState() {
        Map map=MyBatisRequestUtil.getMap(request);
        return taskService.runOne(map);
    }

    @ResponseBody
    @RequestMapping(value="/json/updateAvailable",method = RequestMethod.POST)
    public Object updateAvailable() {
        Map map=MyBatisRequestUtil.getMap(request);
        return taskService.updateAvailable(map);
    }

}