package com.makbro.core.framework.authz.common;

import org.springframework.web.method.HandlerMethod;

import java.lang.annotation.Annotation;

/**
 * 注解解析器
 */
public interface AnnotationResolver {
    Annotation getAnnotation(HandlerMethod handlerMethod, Class<? extends Annotation> var2);
}
