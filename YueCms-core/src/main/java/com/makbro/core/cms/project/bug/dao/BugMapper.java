package com.makbro.core.cms.project.bug.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.project.bug.bean.Bug;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 项目Bug dao
 * @author wujiyue
 */
@Repository
public interface BugMapper{

    public Bug get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Bug bug);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Bug> bugs);
    public void update(Bug bug);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Bug> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Bug> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
