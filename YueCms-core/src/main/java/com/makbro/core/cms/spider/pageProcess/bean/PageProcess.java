package com.makbro.core.cms.spider.pageProcess.bean;


import com.markbro.base.model.AliasModel;

/**
 * 页面提取配置 bean
 * @author  wujiyue on 2017-11-14 13:44:57 .
 */
public class PageProcess  implements AliasModel {

	private Integer id;//
	private String orgid;//组织id
	private String yhid;//
	private String name;//名称
	private String description;//说明
	private Integer available;//
	private Integer deleted;//


	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getOrgid(){ return orgid ;}
	public void  setOrgid(String orgid){this.orgid=orgid; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }


	@Override
	public String toString() {
	return "PageProcess{" +
			"id=" + id+
			", orgid=" + orgid+
			", yhid=" + yhid+
			", name=" + name+
			", description=" + description+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
