package com.makbro.core.cms.project.project.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.project.project.bean.Project;
import com.makbro.core.cms.project.project.dao.ProjectMapper;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目 Service
 * @author wujiyue
 * @date 2019-6-29 14:35:41
 */
@Service
public class ProjectService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private ProjectMapper projectMapper;

     /*基础公共方法*/
    public Project get(Integer id){
        return projectMapper.get(id);
    }

    public List<Project> find(PageBounds pageBounds,Map<String,Object> map){
        return projectMapper.find(pageBounds,map);
    }
    public List<Project> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return projectMapper.findByMap(pageBounds,map);
    }

    public void add(Project project){
        projectMapper.add(project);
    }
    public Object save(Map<String,Object> map){
            Project project= JSON.parseObject(JSON.toJSONString(map),Project.class);
            if(project.getId()==null||"".equals(project.getId().toString())){
               Integer id= keyService.getIntegerId();
               project.setId(id);
               projectMapper.add(project);
            }else{
               projectMapper.update(project);
            }
            return Msg.success("保存信息成功!");
    }
    public void addBatch(List<Project> projects){
        projectMapper.addBatch(projects);
    }

    public void update(Project project){
        projectMapper.update(project);
    }

    public void updateByMap(Map<String,Object> map){
        projectMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        projectMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        projectMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        projectMapper.deleteBatch(ids);
    }




    /*自定义方法*/
     public Object  select(Map<String,Object> map){
         List<Project> list=projectMapper.find(new PageBounds(999),map);
         List<Map<String,Object>> res=new ArrayList<Map<String,Object>>();
         Map<String,Object> tmap=null;
         for(Project p:list){
             tmap=new HashMap<String,Object>();
             tmap.put("dm",p.getId());
             tmap.put("mc",p.getName());
             res.add(tmap);
         }
         return res;
     }
}
