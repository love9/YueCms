package com.makbro.core.cms.jobs;

import com.makbro.core.cms.spider.core.processor.service.PageProcessMissonService;
import com.makbro.core.framework.quartz.jobs.BaseJob;
import com.markbro.base.utils.SpringContextHolder;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 定时趴取互联网文章爬虫任务
 * @author wjy
 * @date 2018年10月03日
 */
public class SpiderMissionJob extends BaseJob {


    /*不知道为什么，把该类实例化配置到spring配置文件并注入missionCode 调试断点为null */
    private String missionCode;

    public void setMissionCode(String missionCode) {
        this.missionCode = missionCode;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        //既然spring属性注入value获取不到missionCode。只好使用JobDataMap方式
        String missionCode2 = jobExecutionContext.getJobDetail().getJobDataMap().getString("missionCode");
        System.out.println("missionCode2====>:"+missionCode2);
        PageProcessMissonService pageProcessMissonService= SpringContextHolder.getBean("pageProcessMissonService");
        super.execute(jobExecutionContext);
        pageProcessMissonService.spiderByMissionCode(missionCode2);

    }
}
