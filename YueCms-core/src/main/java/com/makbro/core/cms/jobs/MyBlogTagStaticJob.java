package com.makbro.core.cms.jobs;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.article.service.StaticService;
import com.makbro.core.cms.tag.bean.Tags;
import com.makbro.core.cms.tag.service.TagsService;
import com.makbro.core.framework.quartz.jobs.BaseJob;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.SpringContextHolder;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 博客首页标签静态化文章列表任务
 * @author wjy
 * @date 2019年1月12日22:29:08
 */
public class MyBlogTagStaticJob extends BaseJob {

    StaticService staticService= SpringContextHolder.getBean("staticService");
    TagsService tagsService= SpringContextHolder.getBean("tagsService");
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);
        Map tagMap=new HashMap();
        tagMap.put("tag_type", Tags.TagType.system.name());
        tagMap.put("tag_module", "fullTabs");
        List<Tags> tags=tagsService.find(new PageBounds(),tagMap);
        if(CollectionUtils.isNotEmpty(tags)){
            Map map=new HashMap();
            for(Tags tag:tags){
                Msg msg=(Msg)staticService.staticArticlesByTag(map,tag.getId().toString(),6);
                if(msg.getType()== Msg.MsgType.success){
                    logger.info(msg.getContent());
                }else{
                    logger.warn(msg.getContent());
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            logger.info("========>fullTabs标签静态化完成!");
        }else{
            logger.warn("没有查询到要静态化的标签!");
        }


    }
}
