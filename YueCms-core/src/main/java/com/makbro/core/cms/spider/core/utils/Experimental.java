package com.makbro.core.cms.spider.core.utils;

/**
 * Stands for features unstable.
 * @author code4crafter@gmail.com <br>
 */
public @interface Experimental {
}
