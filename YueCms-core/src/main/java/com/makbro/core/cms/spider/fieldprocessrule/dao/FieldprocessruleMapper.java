package com.makbro.core.cms.spider.fieldprocessrule.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.spider.fieldprocessrule.bean.Fieldprocessrule;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 字段处理规则 dao
 * @author  on 2017-11-18 15:20:49.
 */
@Repository
public interface FieldprocessruleMapper{

    public Fieldprocessrule get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Fieldprocessrule fieldprocessrule);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Fieldprocessrule> fieldprocessrules);
    public void update(Fieldprocessrule fieldprocessrule);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Fieldprocessrule> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Fieldprocessrule> findByMap(PageBounds pageBounds, Map<String, Object> map);
    
    public List<Fieldprocessrule> findByIds(@Param("ids") String ids);


}
