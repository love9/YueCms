package com.makbro.core.cms.resourcetype.web;


import com.makbro.core.cms.resourcetype.bean.Resourcetype;
import com.makbro.core.cms.resourcetype.service.ResourcetypeService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 资源分类管理
 * Created by wujiyue on 2016-07-21 21:21:06.
 */
@Controller
@RequestMapping("/cms/resourcetype")
public class ResourcetypeController extends BaseController {
    @Autowired
    protected ResourcetypeService resourcetypeService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/cms/resourcetype/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        model.addAttribute("parentid",map.get("parentid"));
        model.addAttribute("parentname",map.get("parentname"));
        return "/cms/resourcetype/add";
    }
    /**
     * 跳转到编辑页面
     */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String id=(String)map.get("id");
        Map<String,Object> resourcetype=resourcetypeService.getMap(id);
        model.addAttribute("resourcetype",resourcetype);
        return "/cms/resourcetype/edit";
    }


    //-----------json数据接口--------------------
    
	@ResponseBody
	@RequestMapping("/json/findByParentid/{parentid}")
	public Object findByParentid(@PathVariable String parentid) {
		return resourcetypeService.findByParentid(getPageBounds(),parentid);
	}

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return resourcetypeService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        Map map=MyBatisRequestUtil.getMap(request);
        return resourcetypeService.find(getPageBounds(),map);

    }
    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增资源分类")
    public void add(Resourcetype m) {
        
        resourcetypeService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Resourcetype m) {
        resourcetypeService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存资源分类")
    public Object save(Resourcetype m) {
        return  resourcetypeService.save(m);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            int count=resourcetypeService.getChildrenCount(id);
            if(count>0){
                return Msg.error("删除的分类下不能有子分类！");
            }else{
                Map<String,Object> map=new HashMap<String,Object>();
                map.put("deleted",1);
                map.put("id",id);
                resourcetypeService.updateByMap(map);
                return Msg.success("删除成功！");
            }
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){

        try{
            String ids_str= StringUtil.arrToString(ids, ",");
            int count=resourcetypeService.getChildrenCount(ids_str);
            if(count>0){
                return Msg.error("删除的分类下不能有子分类！");
            }else{
                Map<String,Object> map=new HashMap<String,Object>();
                map.put("deleted",1);
                map.put("ids",ids);
                resourcetypeService.updateByMapBatch(map);
                return Msg.success("删除成功！");
            }

        }catch (Exception e){
            return Msg.error("删除失败！");
        }

	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除资源分类")
    public void delete(@PathVariable String id) {
        resourcetypeService.delete(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除资源分类")
    public void deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
         resourcetypeService.deleteBatch(ids);
    }
    //zTree简单数据格式的 动态树
    @ResponseBody
    @RequestMapping("/json/ztree")
    @RequiresAuthentication
    public Object tree() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return resourcetypeService.ztree(map);
    }

    @ResponseBody
    @RequestMapping("/json/treeSelect")
    public Object treeSelect() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        resultMap.put("resourcetypeSelect", resourcetypeService.treeSelect(map));
        return resultMap;
    }

    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return resourcetypeService.saveSort(MyBatisRequestUtil.getMap(request));
    }
}
