package com.makbro.core.cms.article.service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.article.bean.Article;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.file.FileUtils;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 清理项目数据库内旧数据以及关联的图片和静态页面
 */
@Service
public class CleanDataService {
    @Autowired
    ArticleService articleService;
    /**
     * 根据趴取任务的id清理cms_article数据以及关联的图片和静态页面
     * @param mission_id
     */
    public void cleanDataByMissionId(String mission_id){
        List<Article> list= articleService.findByMap(new PageBounds(), new HashMap<String, Object>(){{
            put("mission_id",mission_id);
        }});

        String absoluteBasePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.webAppPath");
        if(CollectionUtils.isNotEmpty(list)){
            for(Article article:list){
                String cover_img=article.getCover_image();
                if(StringUtil.notEmpty(cover_img)&&!cover_img.startsWith("http")){
                    FileUtils.deleteFile(absoluteBasePath+cover_img);
                }
                String static_url=article.getStatic_url();
                if(StringUtil.notEmpty(static_url)){
                    FileUtils.deleteFile(absoluteBasePath+static_url);
                }
                articleService.delete(article.getId());
            }
        }
    }
}
