package com.makbro.core.cms.template.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.template.bean.Template;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 模板管理 dao
 * @author  wujiyue on 2017-11-10 10:07:06.
 */
@Repository
public interface TemplateMapper{

    public Template get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Template template);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Template> templates);
    public void update(Template template);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Template> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Template> findByMap(PageBounds pageBounds, Map<String, Object> map);

    public Template getByName(String name);//因为name唯一所以getByName

}
