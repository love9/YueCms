package com.makbro.core.cms.spider.pageProcessMx.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.spider.pageProcessMx.bean.PageProcessMx;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 页面字段配置 dao
 * Created by wujiyue on 2017-11-16 11:07:49.
 */
@Repository
public interface PageProcessMxMapper{

    public PageProcessMx get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(PageProcessMx pageProcessMx);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<PageProcessMx> pageProcessMxs);
    public void update(PageProcessMx pageProcessMx);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<PageProcessMx> find(PageBounds pageBounds, Map<String, Object> map);
    public List<PageProcessMx> findByMap(PageBounds pageBounds, Map<String, Object> map);

    public void deleteByParentid(Integer[] id);

}
