package com.makbro.core.cms.spider.core.pipeline.db;


import com.makbro.core.cms.chapterDetail.dao.ChapterDetailMapper;
import com.makbro.core.cms.chapterDetail.service.ChapterDetailService;
import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Task;
import com.makbro.core.cms.spider.core.pipeline.Pipeline;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by wjy on 2017/12/21.
 * 写入书籍章节数据库表
 */
@Component
public class ChapterDetailPipeline implements Pipeline {

    @Autowired
    ChapterDetailService chapterDetailService;
    @Autowired
    ChapterDetailMapper chapterDetailMapper;
    @Override
    public void process(ResultItems resultItems, Task task) {
        Map map=resultItems.getAll();
       // map.put("id", Guid.get());
        map.put("available",1);
        map.put("deleted",0);
        String sort_str=String.valueOf(map.get("sort"));
        int sort=0;
        if(StringUtil.notEmpty(sort_str)){
            try{
                sort=Integer.valueOf(sort_str);
            }catch (Exception ex){
                sort=chineseNumber2Int(sort_str);
            }
        }
        map.put("sort",sort);
        if(StringUtil.isEmpty(sort_str)){

            //没带序号参数，那么从数据库查询
            String book_id=String.valueOf(map.get("book_id"));
            int nowSort = chapterDetailMapper.getMaxSortOfBook(book_id);
            nowSort=nowSort+1;
            map.put("sort",nowSort);
        }
        chapterDetailService.addByMap(map);
    }
    /**
     * 中文數字转阿拉伯数组【十万九千零六十  --> 109060】
     * @author 雪见烟寒
     * @param chineseNumber
     * @return
     */
    @SuppressWarnings("unused")
    private static int chineseNumber2Int(String chineseNumber){
        int result = 0;
        int temp = 1;//存放一个单位的数字如：十万
        int count = 0;//判断是否有chArr
        char[] cnArr = new char[]{'一','二','三','四','五','六','七','八','九'};
        char[] chArr = new char[]{'十','百','千','万','亿'};
        for (int i = 0; i < chineseNumber.length(); i++) {
            boolean b = true;//判断是否是chArr
            char c = chineseNumber.charAt(i);
            for (int j = 0; j < cnArr.length; j++) {//非单位，即数字
                if (c == cnArr[j]) {
                    if(0 != count){//添加下一个单位之前，先把上一个单位值添加到结果中
                        result += temp;
                        temp = 1;
                        count = 0;
                    }
                    // 下标+1，就是对应的值
                    temp = j + 1;
                    b = false;
                    break;
                }
            }
            if(b){//单位{'十','百','千','万','亿'}
                for (int j = 0; j < chArr.length; j++) {
                    if (c == chArr[j]) {
                        switch (j) {
                            case 0:
                                temp *= 10;
                                break;
                            case 1:
                                temp *= 100;
                                break;
                            case 2:
                                temp *= 1000;
                                break;
                            case 3:
                                temp *= 10000;
                                break;
                            case 4:
                                temp *= 100000000;
                                break;
                            default:
                                break;
                        }
                        count++;
                    }
                }
            }
            if (i == chineseNumber.length() - 1) {//遍历到最后一个字符
                result += temp;
            }
        }
        return result;
    }
}
