package com.makbro.core.cms.album.service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.album.bean.Album;
import com.makbro.core.cms.album.dao.AlbumMapper;
import com.makbro.core.cms.image.bean.Image;
import com.makbro.core.cms.image.dao.ImageMapper;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 相册图集管理 Service
 * @author wujiyue
 * @date 2019-7-4 21:56:54
 */
@Service
public class AlbumService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private AlbumMapper albumMapper;
    @Autowired
    private ImageMapper imageMapper;
     /*基础公共方法*/
    public Album get(Integer id){
        return albumMapper.get(id);
    }

    public List<Album> find(PageBounds pageBounds,Map<String,Object> map){
        return albumMapper.find(pageBounds,map);
    }
    public List<Album> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return albumMapper.findByMap(pageBounds,map);
    }

    public void add(Album album){
        albumMapper.add(album);
    }
    public Object save(Album album){

          Msg msg=new Msg();
          try{
            if(album.getId()==null||"".equals(album.getId().toString())){
               Integer id= keyService.getIntegerId();


               album.setId(id);
               albumMapper.add(album);
            }else{

               albumMapper.update(album);
            }
               msg.setType(Msg.MsgType.success);
               msg.setContent("保存信息成功");
             }catch (Exception ex){
               msg.setType(Msg.MsgType.error);
               msg.setContent("保存信息失败");
            }
            return msg;
    }
    public void addBatch(List<Album> albums){
        albumMapper.addBatch(albums);
    }

    public void update(Album album){
        albumMapper.update(album);
    }

    public void updateByMap(Map<String,Object> map){
        albumMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        albumMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        albumMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        albumMapper.deleteBatch(ids);
    }
     /*自定义方法*/
    public Object findImagesByAlbumId(String  id){
        Map<String,Object> map=new HashMap<>();
        Integer aid=Integer.valueOf(id);
        Album ab=albumMapper.get(aid);
        map.put("title",ab.getName());
        map.put("id",aid);
        map.put("start","0");
        String ids=ab.getImageIds();
        if(ids.endsWith(TmConstant.COMMA))//逗号结尾
        {

            ids=ids.substring(0,ids.length()-1);
        }
        ids=ids.replaceAll(TmConstant.COMMA,"','");
        List<Image> res= imageMapper.findByImageIds(ids);
        List<Map<String,Object>> data=new ArrayList<>();
        Map<String,Object> m=null;
        for(Image i:res)
        {
            m=new HashMap<String,Object>();
            m.put("alt",i.getName());
            m.put("pid",i.getId());
            m.put("src",i.getUrl());
            m.put("thumb",i.getUrl_small());
            data.add(m);
        }
        map.put("data",data);
        return map;
    }

    public Object findPhotos(PageBounds pageBounds,Map<String,Object> map){
        map.put("albumtype","photo");

        int pageSize=pageBounds.getLimit();
        List<Album> list=albumMapper.find(pageBounds,map);
        List<Map<String,Object>> rmaps=new ArrayList<Map<String,Object>>();
        Map<String,Object> temp=null;
        Image i=null;

        PageList ls=(PageList)list;
        Paginator paginator= ls.getPaginator();

        int total=paginator.getTotalCount();
        int totalPages=(total+pageSize-1)/pageSize;
        for(Album a:list){
            temp=new HashMap<String,Object>();
            temp.put("id",a.getId());
            temp.put("name",a.getName());
            temp.put("time",a.getCreateTime());
            i=imageMapper.get(a.getFrontImgId().toString());
            temp.put("frontpath",i.getUrl());
            temp.put("totalPages",totalPages);
            rmaps.add(temp);
        }
        return  rmaps;
    }

}
