package com.makbro.core.cms.chapterDetail.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.chapterDetail.bean.ChapterDetail;
import com.makbro.core.cms.chapterDetail.dao.ChapterDetailMapper;
import com.markbro.base.model.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 书籍章节 Service
 * @author  wujiyue on 2017-12-20 08:47:25.
 */
@Service
public class ChapterDetailService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ChapterDetailMapper chapterDetailMapper;

     /*基础公共方法*/
    public ChapterDetail get(Integer id){
        ChapterDetail chapterDetail= chapterDetailMapper.get(id);
        String content=chapterDetailMapper.getChapterContent(id);
        chapterDetail.setContent(content);
        return chapterDetail;
    }

    public List<ChapterDetail> find(PageBounds pageBounds, Map<String,Object> map){
        return chapterDetailMapper.find(pageBounds,map);
    }
    public List<ChapterDetail> findByMap(PageBounds pageBounds, Map<String,Object> map){
        return chapterDetailMapper.findByMap(pageBounds,map);
    }

    public void addByMap(Map<String,Object> map){
        chapterDetailMapper.addByMap(map);
        chapterDetailMapper.addChapterContent(map);
    }

    public Object save(Map<String,Object> map){
        ChapterDetail chapterDetail= JSON.parseObject(JSON.toJSONString(map),ChapterDetail.class);
        if(chapterDetail.getId()==null||"".equals(chapterDetail.getId().toString())){
            int sort=chapterDetailMapper.getMaxSort();
            chapterDetail.setSort(sort+1);
            chapterDetailMapper.add(chapterDetail);
            map.put("id",chapterDetail.getId());
            chapterDetailMapper.addChapterContent(map);
        }else{
            chapterDetailMapper.update(chapterDetail);
            chapterDetailMapper.updateChapterContent(map);
        }
        return Msg.success("保存信息成功");
    }

    public void update(ChapterDetail chapterDetail){
        chapterDetailMapper.update(chapterDetail);
    }
    public void delete(Integer id){
        chapterDetailMapper.delete(id);
    }
    public void deleteBatch(Integer[] ids){
        chapterDetailMapper.deleteBatch(ids);
    }
	public Object saveSort(Map map){
		try{
			String sort = String.valueOf(map.get("sort"));
			if(!"".equals(sort)){
				String[] sx = sort.split(",");
				for(int i=0;i<sx.length;i++) {
					String[] arr = sx[i].split("_");
					chapterDetailMapper.updateSort(arr[1], arr[2]);
				}
			}
            return Msg.success("排序成功！");
		}catch (Exception e){
            return Msg.error("排序失败！");
		}
	}

     /*自定义方法*/

    //根据书籍id和书籍序号获得章节 用于获得前一章节和后一章节
    public ChapterDetail getByBookidAndSort(String bood_id,int sort){
        return chapterDetailMapper.getByBookidAndSort(bood_id,sort);
    }

}
