package com.makbro.core.cms.jobs;

import com.makbro.core.cms.book.bean.Book;
import com.makbro.core.cms.book.dao.BookMapper;
import com.makbro.core.cms.book.service.BookService;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.service.TemplateService;
import com.makbro.core.framework.quartz.jobs.BaseJob;
import com.markbro.base.common.util.FreeMarkerUtil;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.common.util.VelocityHelper;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.SpringContextHolder;
import com.markbro.base.utils.string.StringUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 静态化书籍任务
 * @author wjy
 * @date 2018年10月6日14:42:06
 */
public class BookStaticJob extends BaseJob {

    BookService bookService= SpringContextHolder.getBean("bookService");
    BookMapper bookMapper= SpringContextHolder.getBean("bookMapper");
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);
        //从数据库查询或者从jobDataMap获得需要静态化的书籍ID

        //String bookId="13";
        String bookId=bookMapper.getBookIdForBookStatic();
        if(StringUtil.isEmpty(bookId)){
            return;
        }
        TemplateService templateService= SpringContextHolder.getBean("templateService");
        String templateName = jobExecutionContext.getJobDetail().getJobDataMap().getString("templateName");
        if(StringUtil.isEmpty(templateName)){
            templateName="bookStatic";
        }
        Template template= templateService.getByName(templateName);
        String templateString="";//模板字符串
        if(template!=null){
            templateString=template.getContent();
        }
        if(StringUtil.isEmpty(templateString)){
            logger.warn("templateString 为空!");
            return;
        }
        bookStaticFreemarker(bookId,templateString);
    }
    public void bookStaticFreemarker(String bookId,String templateString){
        try {

            Book book = bookService.getBookAndChapterList(bookId);
            //String path=ProjectUtil.getProjectPath()+Global.getConfig("static.webAppPath")+Global.getConfig("static.relativeBathPath");;//项目根目录+webapp路径+静态文件输出路径相对webapp目录
            //上面的路径在开发环境下可以生成文件到源程序，但是运行环境下生成的真实文件目录多了src/main/webapp
            //所以使用下面的路径
            String path= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.webAppPath")+ GlobalConfig.getConfig("static.relativeBathPath");
            String relative_path="";
            String relative_base_path= GlobalConfig.getConfig("static.relativeBathPath");
            //String htmlName= DigestUtils.md5Hex(book.getLink());
            String htmlName=bookId;
            if(StringUtil.notEmpty(relative_base_path)){
                if (!path.endsWith("/")) {
                    path += "/";
                }
                relative_path= relative_base_path+ File.separator+"book"+File.separator+bookId+File.separator+htmlName+ ".html";//文件相对路径
                path=path +"book"+File.separator+bookId+File.separator+ htmlName+ ".html";//文件绝对路径
            }else{//没配置相对路径，就不考虑它
                return;
            }
            File f=getFile(path);

            Map dataMap=new HashMap();
            dataMap.put("book",book);
            FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(FreeMarkerUtil.TemplateLoaderType.StringTemplateLoader,new HashMap<String,String>(){{
                put("name",templateString);
            }});
            boolean flag=freeMarkerUtil.ExecuteFile("name",dataMap,path);
            if(flag){
                logger.info(">>>>>>>>static Book file path success!====>"+path);
            }else{
                logger.warn(">>>>>>>>static Book file path fail!====>"+path);
            }

            //把生成的静态文件相对地址写到数据库
            Book updateBook=new Book();
            updateBook.setId(bookId);
            relative_path=relative_path.replaceAll("\\\\","\\/");//把反斜杠换成正斜杠
            updateBook.setStatic_url(relative_path);
            bookService.update(updateBook);
            logger.info("书籍(bookId:{})静态化成功!filepath===>"+f.getAbsolutePath(),bookId);
        } catch (Exception e) {
            logger.warn("write file error", e);
        }

    }
    public void bookStatic(String bookId,String templateString){
        try {

            Book book = bookService.getBookAndChapterList(bookId);
            //String path=ProjectUtil.getProjectPath()+Global.getConfig("static.webAppPath")+Global.getConfig("static.relativeBathPath");;//项目根目录+webapp路径+静态文件输出路径相对webapp目录
            //上面的路径在开发环境下可以生成文件到源程序，但是运行环境下生成的真实文件目录多了src/main/webapp
            //所以使用下面的路径
            String path= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.relativeBathPath");
            String relative_path="";
            String relative_base_path= GlobalConfig.getConfig("static.relativeBathPath");
            //String htmlName= DigestUtils.md5Hex(book.getLink());
            String htmlName=bookId;
            if(StringUtil.notEmpty(relative_base_path)){
                if (!path.endsWith("/")) {
                    path += "/";
                }
                relative_path= relative_base_path+ File.separator+"book"+File.separator+bookId+File.separator+htmlName+ ".html";//文件相对路径
                path=path +"book"+File.separator+bookId+File.separator+ htmlName+ ".html";//文件绝对路径
            }else{//没配置相对路径，就不考虑它
                return;
            }
            File f=getFile(path);
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f),"UTF-8"));
            VelocityHelper helper=new VelocityHelper("");
            helper.AddKeyValue("book", book);
            String all=helper.ExecuteMergeString(templateString);
            printWriter.println(all);
            printWriter.close();

            //把生成的静态文件相对地址写到数据库
            Book updateBook=new Book();
            updateBook.setId(bookId);
            relative_path=relative_path.replaceAll("\\\\","\\/");//把反斜杠换成正斜杠
            updateBook.setStatic_url(relative_path);
            bookService.update(updateBook);
            logger.info("书籍(bookId:{})静态化成功!filepath===>"+f.getAbsolutePath(),bookId);
        } catch (Exception e) {
            logger.warn("write file error", e);
        }

    }
    public File getFile(String fullName) {
        checkAndMakeParentDirecotry(fullName);
        return new File(fullName);
    }

    public void checkAndMakeParentDirecotry(String fullName) {
        int index = fullName.lastIndexOf("/");
        if (index > 0) {
            String path = fullName.substring(0, index);
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        index = fullName.lastIndexOf("\\");
        if (index > 0) {
            String path = fullName.substring(0, index);
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
    }
}
