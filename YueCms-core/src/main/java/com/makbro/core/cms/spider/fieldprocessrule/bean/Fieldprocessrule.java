package com.makbro.core.cms.spider.fieldprocessrule.bean;


import com.markbro.base.model.AliasModel;

/**
 * 字段处理规则 bean
 * @author  wujiyue on 2017-11-18 15:20:48 .
 */
public class Fieldprocessrule  implements AliasModel {

	private Integer id;//
	private String fieldprocessrule;//数据处理规则 参考数据字典dataprocessrule
	private String replaceReg;//要替换的正则表达式
	private String replacement;//替换成的内容
	private String substrtarget;//截取字符串目标
	private Integer substrlength;//截取长度

	private Integer sort;//附加字段。有些字段有多个处理规则，这些规则有一定处理顺序，否则后面的不能正常处理

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getFieldprocessrule(){ return fieldprocessrule ;}
	public void  setFieldprocessrule(String fieldprocessrule){this.fieldprocessrule=fieldprocessrule; }
	public String getReplaceReg(){ return replaceReg ;}
	public void  setReplaceReg(String replaceReg){this.replaceReg=replaceReg; }
	public String getReplacement(){ return replacement ;}
	public void  setReplacement(String replacement){this.replacement=replacement; }
	public String getSubstrtarget(){ return substrtarget ;}
	public void  setSubstrtarget(String substrtarget){this.substrtarget=substrtarget; }
	public Integer getSubstrlength(){ return substrlength ;}
	public void  setSubstrlength(Integer substrlength){this.substrlength=substrlength; }

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	public String toString() {
	return "Fieldprocessrule{" +
			"id=" + id+
			"sort=" + sort+
			", fieldprocessrule=" + fieldprocessrule+
			", replaceReg=" + replaceReg+
			", replacement=" + replacement+
			", substrtarget=" + substrtarget+
			", substrlength=" + substrlength+
			 '}';
	}
}
