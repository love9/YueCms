package com.makbro.core.cms.spider.pageProcessMx.bean;


import com.makbro.core.cms.spider.fieldprocessrule.bean.Fieldprocessrule;
import com.markbro.base.model.AliasModel;

import java.util.List;

/**
 * 页面字段配置 bean
 * Created by wujiyue on 2017-11-16 11:07:48 .
 */
public class PageProcessMx  implements AliasModel {

	private Integer id;//
	private Integer parentid;//上级id
	private Integer zbguid;//父表id
	private String field;//字段
	private String fieldname;//字段名称
	private String datatype;//数据类型如 文本 或者集合
	private String extractbytype;//提取类型 css、xpath
	private String extractby;//提取规则   如div#title之类的css筛选
	private String constant_value;//元素类或ID

	private String extractby_index="0";//提取元素索引，默认为0，提取第一个元素，允许为一个区间比如0-5
	private String extractby_description;//提取描述

	private String fieldprocessrule_id;//数据处理规则 多个用逗号分隔

	private List<Fieldprocessrule> fieldprocessrules;

	public List<Fieldprocessrule> getFieldprocessrules() {
		return fieldprocessrules;
	}

	private String extractby_attr_flag="0";//是否根据元素属性取值
	private String extractby_attr;// 根据哪个元素属性取值
	public void setFieldprocessrules(List<Fieldprocessrule> fieldprocessrules) {
		this.fieldprocessrules = fieldprocessrules;
	}

	public String getExtractby_attr_flag() {
		return extractby_attr_flag;
	}

	public void setExtractby_attr_flag(String extractby_attr_flag) {
		this.extractby_attr_flag = extractby_attr_flag;
	}

	public String getExtractby_attr() {
		return extractby_attr;
	}

	public void setExtractby_attr(String extractby_attr) {
		this.extractby_attr = extractby_attr;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getExtractby_index() {
		return extractby_index;
	}

	public void setExtractby_index(String extractby_index) {
		this.extractby_index = extractby_index;
	}

	public String getExtractby_description() {
		return extractby_description;
	}

	public void setExtractby_description(String extractby_description) {
		this.extractby_description = extractby_description;
	}

	public String getFieldprocessrule_id() {
		return fieldprocessrule_id;
	}

	public void setFieldprocessrule_id(String fieldprocessrule_id) {
		this.fieldprocessrule_id = fieldprocessrule_id;
	}

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public Integer getZbguid(){ return zbguid ;}
	public void  setZbguid(Integer zbguid){this.zbguid=zbguid; }
	public String getField(){ return field ;}
	public void  setField(String field){this.field=field; }
	public String getFieldname(){ return fieldname ;}
	public void  setFieldname(String fieldname){this.fieldname=fieldname; }
	public String getDatatype(){ return datatype ;}
	public void  setDatatype(String datatype){this.datatype=datatype; }
	public String getExtractbytype(){ return extractbytype ;}
	public void  setExtractbytype(String extractbytype){this.extractbytype=extractbytype; }
	public String getExtractby(){ return extractby ;}
	public void  setExtractby(String extractby){this.extractby=extractby; }

	public String getConstant_value() {
		return constant_value;
	}

	public void setConstant_value(String constant_value) {
		this.constant_value = constant_value;
	}

	@Override
	public String toString() {
		return "PageProcessMx{" +
				"id=" + id +
				", parentid=" + parentid +
				", zbguid=" + zbguid +
				", field='" + field + '\'' +
				", fieldname='" + fieldname + '\'' +
				", datatype='" + datatype + '\'' +
				", extractbytype='" + extractbytype + '\'' +
				", extractby='" + extractby + '\'' +
				", constant_value='" + constant_value + '\'' +
				", extractby_index='" + extractby_index + '\'' +
				", extractby_description='" + extractby_description + '\'' +
				", fieldprocessrule_id='" + fieldprocessrule_id + '\'' +
				", fieldprocessrules=" + fieldprocessrules +
				", extractby_attr_flag='" + extractby_attr_flag + '\'' +
				", extractby_attr='" + extractby_attr + '\'' +
				'}';
	}
}
