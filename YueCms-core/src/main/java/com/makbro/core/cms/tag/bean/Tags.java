package com.makbro.core.cms.tag.bean;


import com.markbro.base.model.AliasModel;

/**
 * 标签 bean
 * @author  wujiyue on 2018-07-06 15:42:12 .
 */
public class Tags  implements AliasModel {

	private Integer id;//
	private String tag_module;//备用
	private String tag_type;//类型，如系统标签，个人分类标签
	private String yhid;//谁增加的该标签
	private String name;//分类名称
	private Integer sort;//排序
	private Integer available;//是否显示
	private String url;
	public enum TagType{
		personal,
		system
	}

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getTag_module(){ return tag_module ;}
	public void  setTag_module(String tag_module){this.tag_module=tag_module; }
	public String getTag_type(){ return tag_type ;}
	public void  setTag_type(String tag_type){this.tag_type=tag_type; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
	return "Tags{" +
			"id=" + id+
			", tag_module=" + tag_module+
			", tag_type=" + tag_type+
			", yhid=" + yhid+
			", name=" + name+
			", sort=" + sort+
			", url=" + url+
			", available=" + available+
			 '}';
	}
}
