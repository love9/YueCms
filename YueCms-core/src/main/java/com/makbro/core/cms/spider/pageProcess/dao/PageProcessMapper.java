package com.makbro.core.cms.spider.pageProcess.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.spider.pageProcess.bean.PageProcess;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 页面提取配置 dao
 * Created by wujiyue on 2017-11-14 13:44:58.
 */
@Repository
public interface PageProcessMapper{

    public PageProcess get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(PageProcess pageProcess);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<PageProcess> pageProcesss);
    public void update(PageProcess pageProcess);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<PageProcess> find(PageBounds pageBounds, Map<String, Object> map);
    public List<PageProcess> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
