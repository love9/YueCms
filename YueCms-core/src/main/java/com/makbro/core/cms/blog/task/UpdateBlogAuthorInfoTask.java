package com.makbro.core.cms.blog.task;


import com.makbro.core.cms.blog.dao.BlogManageMapper;
import com.makbro.core.cms.blog.service.BlogManageService;
import com.markbro.base.common.util.thread.MultiThreadHandler;
import com.markbro.base.common.util.thread.exception.ChildThreadException;
import com.markbro.base.common.util.thread.parallel.ParallelTaskWithThreadPool;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.SpringContextHolder;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 更新博主信息任务
 * @Author wjy
 * @date 2018年9月20日12:13:52
 */
public class UpdateBlogAuthorInfoTask  implements Runnable{
    public static final String KEY_BLOG_VISIT_COUNT="blogVisitCount";//一小段时间内博客访问次数缓存key
    private String yhid;
    private String taskName="UpdateBlogAuthorInfoTask[更新博主信息任务]";
    private Map<String, Object> result;

    public UpdateBlogAuthorInfoTask(String yhid, Map<String, Object> result) {
        this.yhid=yhid;
        this.result=result;
    }
    @Override
    public void run() {
        System.out.println(this.taskName+"[更新博主信息任务]yhid:"+yhid+" run！");

        Integer n=0;
        try{
            n=(Integer) EhCacheUtils.getUserInfo(KEY_BLOG_VISIT_COUNT, yhid);
        }catch (Exception ex){}
        if(n!=null&&n>0){

            //次数n累加到 visit_today、visit_week、visit_month、visit_total
            BlogManageMapper blogManageMapper= SpringContextHolder.getBean("blogManageMapper");
            if(blogManageMapper!=null){
                blogManageMapper.updateVisitNum(yhid,n);
            }else{
                throw new ApplicationException("UpdateBlogAuthorInfoTask:blogManageMapper为空!");
            }

            EhCacheUtils.putUserInfo(KEY_BLOG_VISIT_COUNT,yhid,0);//累加入数据库后重置为0
        }

        Map<String, Object> t=(Map<String, Object>)result.get(yhid);
        if(t==null){
            t=new HashMap<String, Object>();
        }
        //更新缓存的博主信息的updateTime为空，下次用户取该信息就能从数据库取最新的
        Map<String,Object> map=null;
        map=(Map<String,Object>)EhCacheUtils.getUserInfo(BlogManageService.KEY_BLOG_AUTHOR_INFO,yhid);
        if(map!=null){
            map.put("update_time",null);
            EhCacheUtils.putUserInfo(BlogManageService.KEY_BLOG_AUTHOR_INFO,yhid,map);
        }
        t.put("result", "success");
        t.put("msg", this.taskName+"yhid:"+yhid+" 结束！");
        result.put(yhid,t);
    }
    public static void main(String[] args) {

        System.out.println("main begin \t=================");
        Map<String, Object> resultMap = new HashMap<String, Object>(8, 1);
        //MultiThreadHandler handler = new MultiParallelThreadHandler();
        ExecutorService service = Executors.newFixedThreadPool(5);
        MultiThreadHandler handler = new ParallelTaskWithThreadPool(service);
        UpdateBlogAuthorInfoTask task = null;
        // 启动5个子线程作为要处理的并行任务，共同完成结果集resultMap
        for(int i=1; i<=5 ; i++){
            task = new UpdateBlogAuthorInfoTask("" + i, resultMap);
            handler.addTask(task);
        }
        try {
            handler.run();
        } catch (ChildThreadException e) {
            System.out.println(e.getAllStackTraceMessage());
        }
        System.out.println(resultMap);
        service.shutdown();
        System.out.println("main end \t=================");
    }
}
