package com.makbro.core.cms.report.bean;


import com.markbro.base.model.AliasModel;

/**
 * 举报 bean
 * @author  wujiyue on 2018-07-27 15:22:31 .
 */
public class Report  implements AliasModel {

	private Integer id;//
	private String module;//被举报内容所属模块，比如评论、文章、图片等
	private String target_id;//被举报目标id,一般是评论或文章主键
	private String report_yhid;//举报人用户id
	private Integer report_type;//举报类型
	private Integer count;//被举报次数，说明还未被处理再次被其它人举报，可反映出处理急切程度
	private Integer deal_flag;//处理标志 0未处理 1已处理


	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getModule(){ return module ;}
	public void  setModule(String module){this.module=module; }
	public String getTarget_id(){ return target_id ;}
	public void  setTarget_id(String target_id){this.target_id=target_id; }
	public String getReport_yhid(){ return report_yhid ;}
	public void  setReport_yhid(String report_yhid){this.report_yhid=report_yhid; }
	public Integer getReport_type(){ return report_type ;}
	public void  setReport_type(Integer report_type){this.report_type=report_type; }
	public Integer getCount(){ return count ;}
	public void  setCount(Integer count){this.count=count; }
	public Integer getDeal_flag(){ return deal_flag ;}
	public void  setDeal_flag(Integer deal_flag){this.deal_flag=deal_flag; }


	@Override
	public String toString() {
	return "Report{" +
			"id=" + id+
			", module=" + module+
			", target_id=" + target_id+
			", report_yhid=" + report_yhid+
			", report_type=" + report_type+
			", count=" + count+
			", deal_flag=" + deal_flag+
			 '}';
	}
}
