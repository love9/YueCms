package com.makbro.core.cms.spider.mission.bean;


import com.markbro.base.model.AliasModel;

/**
 * 趴取任务 bean
 * @author  wujiyue on 2017-11-16 10:52:33 .
 */
public class Spidermission  implements AliasModel {

	private Integer id;//
	private String orgid;//组织id
	private String bmid;//部门id
	private String gwid;//岗位id
	private String yhid;//
	private String missionGroupId;//任务组 0是默认任务
	private String missionCode;//任务代码
	private String missionName;//抓取任务名称
	private String missionType;//任务类型 抓取图片、抓取链接、抓取文章等
	private String ref_id;//引用id 比如抓取书籍章节任务时的book_id
	private String tablename;//数据写入哪个表
	private String status;//任务状态

	private String urls;//需要采集的目标网页多个用分号分割
	private String targetUrlReg;//目标url正则
	private String targetUrlCssSelector;//提取目标url的css选择器。选填，一般只用到targetUrlReg就能得到目标url
	private String targetUrlXpathSelector;//提取目标url的xpath选择器。选填，一般只用到targetUrlReg就能得到目标url
	private String loopFlag;//
	private String loopParam;//
	private Integer loopNum;//循环翻页次数
	private Integer sleep;//每个页面之间的休眠毫秒数
	private Integer thread;//线程数
	private String pipeline;//数据管道
	private String templateName;//模板代码（TemplateMergerStringFilePipeline）或者模板文件名称（TemplateFilePipeline）

	private Integer sort;//排序
	private Integer available;//
	private Integer deleted;//

	private Integer limitCount=0;//限制趴取前几条数据；默认0表示全部。大于零的数生效。
	private String contentImgFolder;//任务下载的正文包含的图片放到这个文件夹路径
	public String getMissionCode() {
		return missionCode;
	}

	public void setMissionCode(String missionCode) {
		this.missionCode = missionCode;
	}

	public String getRef_id() {
		return ref_id;
	}

	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}

	public Integer getLimitCount() {
		return limitCount;
	}

	public void setLimitCount(Integer limitCount) {
		this.limitCount = limitCount;
	}

	public String getContentImgFolder() {
		return contentImgFolder;
	}

	public void setContentImgFolder(String contentImgFolder) {
		this.contentImgFolder = contentImgFolder;
	}

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getOrgid(){ return orgid ;}
	public void  setOrgid(String orgid){this.orgid=orgid; }
	public String getBmid(){ return bmid ;}
	public void  setBmid(String bmid){this.bmid=bmid; }
	public String getGwid(){ return gwid ;}
	public void  setGwid(String gwid){this.gwid=gwid; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getMissionGroupId(){ return missionGroupId ;}
	public void  setMissionGroupId(String missionGroupId){this.missionGroupId=missionGroupId; }
	public String getMissionName(){ return missionName ;}
	public void  setMissionName(String missionName){this.missionName=missionName; }
	public String getMissionType(){ return missionType ;}
	public void  setMissionType(String missionType){this.missionType=missionType; }
	public String getStatus(){ return status ;}
	public void  setStatus(String status){this.status=status; }

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	public String getTargetUrlReg(){ return targetUrlReg ;}
	public void  setTargetUrlReg(String targetUrlReg){this.targetUrlReg=targetUrlReg; }
	public String getUrls(){ return urls ;}
	public void  setUrls(String urls){this.urls=urls; }

	public Integer getSleep(){ return sleep ;}
	public void  setSleep(Integer sleep){this.sleep=sleep; }
	public Integer getThread(){ return thread ;}
	public void  setThread(Integer thread){this.thread=thread; }
	public String getPipeline(){ return pipeline ;}
	public void  setPipeline(String pipeline){this.pipeline=pipeline; }

	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	public String getLoopFlag() {
		return loopFlag;
	}

	public void setLoopFlag(String loopFlag) {
		this.loopFlag = loopFlag;
	}

	public String getLoopParam() {
		return loopParam;
	}

	public void setLoopParam(String loopParam) {
		this.loopParam = loopParam;
	}

	public Integer getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(Integer loopNum) {
		this.loopNum = loopNum;
	}

	public String getTargetUrlCssSelector() {
		return targetUrlCssSelector;
	}

	public void setTargetUrlCssSelector(String targetUrlCssSelector) {
		this.targetUrlCssSelector = targetUrlCssSelector;
	}

	public String getTargetUrlXpathSelector() {
		return targetUrlXpathSelector;
	}

	public void setTargetUrlXpathSelector(String targetUrlXpathSelector) {
		this.targetUrlXpathSelector = targetUrlXpathSelector;
	}

	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	@Override
	public String toString() {
		return "Spidermission{" +
				"id=" + id +
				", orgid='" + orgid + '\'' +
				", bmid='" + bmid + '\'' +
				", gwid='" + gwid + '\'' +
				", yhid='" + yhid + '\'' +
				", missionGroupId='" + missionGroupId + '\'' +
				", missionCode='" + missionCode + '\'' +
				", missionName='" + missionName + '\'' +
				", missionType='" + missionType + '\'' +
				", ref_id='" + ref_id + '\'' +
				", tablename='" + tablename + '\'' +
				", status='" + status + '\'' +
				", targetUrlReg='" + targetUrlReg + '\'' +
				", targetUrlCssSelector='" + targetUrlCssSelector + '\'' +
				", targetUrlXpathSelector='" + targetUrlXpathSelector + '\'' +
				", urls=" + urls +
				", loopFlag='" + loopFlag + '\'' +
				", loopParam='" + loopParam + '\'' +
				", loopNum=" + loopNum +
				", sleep=" + sleep +
				", thread=" + thread +
				", pipeline='" + pipeline + '\'' +

				", sort=" + sort +
				", available=" + available +
				", deleted=" + deleted +
				'}';
	}
}
