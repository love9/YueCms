package com.makbro.core.cms.chapterDetail.web;


import com.makbro.core.cms.chapterDetail.bean.ChapterDetail;
import com.makbro.core.cms.chapterDetail.service.ChapterDetailService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresAnonymous;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 书籍章节管理
 * Created by wujiyue on 2017-12-20 08:47:25.
 */

@Controller
@RequestMapping("/cms/chapterDetail")
public class ChapterDetailController extends BaseController {
    @Autowired
    protected ChapterDetailService chapterDetailService;

    @RequiresAnonymous
    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/chapterDetail/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(ChapterDetail chapterDetail, Model model){
                return "/cms/chapterDetail/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(ChapterDetail chapterDetail, Model model){
        if(chapterDetail!=null&&chapterDetail.getId()!=null){
            chapterDetail=chapterDetailService.get(chapterDetail.getId());
        }
         model.addAttribute("chapterDetail",chapterDetail);
         return "/cms/chapterDetail/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    @RequiresAnonymous
    public Object get(@PathVariable Integer id) {
        return chapterDetailService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    @RequiresAnonymous
    public Object find() {
        return chapterDetailService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return chapterDetailService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	try{
            chapterDetailService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
    	    return Msg.error("删除失败！");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
            chapterDetailService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return chapterDetailService.saveSort(MyBatisRequestUtil.getMap(request));
    }
}