package com.makbro.core.cms.comment.service;


import com.markbro.base.common.util.SensitiveWord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Map;

/**
 * Created by wjy on 2017/12/29.
 * 用来过滤评论中的敏感词语
 */
@Service
public class CommentFilterService{

    private Logger logger = LoggerFactory.getLogger(getClass());
    SensitiveWord sensitiveWord = null;
   public CommentFilterService(){
       logger.info("Init CommentFilterService >>>");
       sensitiveWord = new SensitiveWord();
   }

    /**
     * 敏感词过滤
     * @param content
     * @return
     */
    public Map<String,Object> filter(String content){
        Assert.notNull(sensitiveWord,"CommentFilterService > sensitiveWord为空!");
        Map<String,Object> res= sensitiveWord.filterInfo(content);
        return res;
    }


}
