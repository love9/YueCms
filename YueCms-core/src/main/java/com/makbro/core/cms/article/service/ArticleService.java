package com.makbro.core.cms.article.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.article.bean.Article;
import com.makbro.core.cms.article.bean.ArticleModel;
import com.makbro.core.cms.article.bean.ArticleRegionType;
import com.makbro.core.cms.article.dao.ArticleMapper;
import com.makbro.core.cms.resourcetype.service.ResourcetypeService;
import com.makbro.core.cms.tag.bean.Tags;
import com.makbro.core.cms.tag.service.TagsService;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.service.TemplateService;
import com.markbro.base.common.util.FreeMarkerUtil;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文章 service
 * @author  wujiyue on 2016-08-14 20:02:46.
 */
@Service
public class ArticleService{

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private TableKeyService keyService;
    @Autowired
    ResourcetypeService resourcetypeService;
    @Autowired
    TagsService tagsService;
    public Article get(String id) {
        Article article = articleMapper.get(id);
        if(article==null){
            return null;
        }
        Map<String, Object> m = articleMapper.getArticleContent(id);
        if(m!=null){
            article.setContent(String.valueOf(m.get("content")));
            article.setContent_markdown_source(String.valueOf(m.get("content_markdown_source")));
        }
        return article;
    }

    public Map<String, Object> getMap(String id) {
        Map<String, Object> m = articleMapper.getMap(id);
        Map<String, Object> m2 = articleMapper.getArticleContent(id);
        if(m2!=null){
            m.putAll(m2);
        }
        return m;
    }

    public List<Article> find(PageBounds pageBounds, Map<String, Object> map) {

        List<Article> list = articleMapper.find(pageBounds, map);

        list.stream().forEach(a->{
                String name = null;
                //查询文章模型名称
                name= ArticleModel.getDescByVal(a.getArticle_model());
                a.setArticle_model_name(name);
                a.setTags_name(getTags_name(a.getTags()));
                //截取创建时间只留日期
                String dt=a.getCreateTime();
                if(StringUtil.notEmpty(dt)&&dt.length()>10){
                    dt=dt.substring(0,10);
                    a.setCreateTime(dt);
                }

                if(StringUtil.notEmpty(a.getTags())){//为节省空间，有些地方只要一个标签
                    String[] arr=a.getTags().split(",");
                    a.setTag(arr[0]);
                    a.setTag_name(getTags_name(arr[0]));
                }
        });

        return list;
    }
    public String getTags_name(String tags){
        String res="";
        if(StringUtil.notEmpty(tags)){
            String[] arr=tags.split(",");
            for(String s:arr){
              Tags tg =  tagsService.get(Integer.valueOf(s));
                if(tg!=null){
                    res+=tg.getName()+",";
                }
            }
        }
        if(res.endsWith(",")){
            res=StringUtil.subEndStr(res,",");
        }
        return res;
    }

    public Integer getTotalCountByMap(Map<String, Object> map) {
        return articleMapper.getTotalCountByMap(map);
    }
    public List<Article> findByMap(PageBounds pageBounds, Map<String, Object> map) {
        return articleMapper.findByMap(pageBounds, map);
    }


    public void add(Article article) {
        articleMapper.add(article);
    }

    public void addByMap(Map<String, Object> map) {
        articleMapper.addByMap(map);
        articleMapper.addArticleContent(map);
    }

    @Transactional
    public Object save(Map<String, Object> map) {
        Msg msg = new Msg();
        Map<String, Object> contentMap = new HashMap<String, Object>();
        Article article= JSON.parseObject(JSON.toJSONString(map),Article.class);
        if(StringUtil.isEmpty(article.getCover_image())){
            article.setCover_image(null);
        }
        String strTags=article.getTags();
        String tagsIds="";

        if(StringUtil.notEmpty(strTags)){
            List<Map> tagsList=   JSONArray.parseArray(strTags,Map.class);
            for(Map t:tagsList){
                String id =(String)t.get("id");
                String name=(String)t.get("name");
                String newflag=String.valueOf(t.get("newflag"));
                if("1".equals(newflag)){
                    //先根据name去取，取不到就新增
                    Map<String,Object> tmap=new HashMap<String,Object>();
                    tmap.put("name",name);
                    tmap.put(TmConstant.YHID_KEY,String.valueOf(map.get(TmConstant.YHID_KEY)));
                    List<Tags> list = tagsService.find(new PageBounds(),tmap);
                    if(CollectionUtils.isEmpty(list)){
                        Tags tags=new Tags();
                        tags.setName(name);
                        tags.setAvailable(1);
                        tags.setSort(1);
                        tags.setYhid(String.valueOf(map.get(TmConstant.YHID_KEY)));
                        tagsService.add(tags);
                        tagsIds+=tags.getId()+TmConstant.COMMA;//id+逗号
                    }else{
                        Tags tg=list.get(0);
                        tagsIds+=tg.getId()+TmConstant.COMMA;
                    }
                }else{
                    tagsIds+=id+",";
                }
            }
        }
        tagsIds=StringUtil.subEndStr(tagsIds,TmConstant.COMMA);
        article.setTags(tagsIds);

        String copy_flag=String.valueOf(map.get("copy_flag"));
        article.setCopy_flag(TmConstant.NUM_ONE.equals(copy_flag)?1:0);

        if(StringUtil.notEmpty(article.getArticle_model())){
            //设置文章模型对应的扩展字段值的设置
            if(article.getArticle_model().equals(ArticleModel.DUOGUYU.getVal())){
                //设置 template_name=Spiderhtml_duoguyu
                article.setTemplate_name("Spiderhtml_duoguyu");
            }else{}
        }

        contentMap.put("content", article.getContent());

        if(StringUtil.isEmpty(article.getDescription())){ //设置文章描述
            String desc=article.getContent()!=null&&article.getContent().length()>40?article.getContent().substring(0,40)+"...":article.getContent();
            article.setDescription(desc);
        }

        contentMap.put("content_markdown_source", article.getContent_markdown_source());
        if (article.getId() == null || "".equals(article.getId().toString())) {
            String id = keyService.getStringId();
            article.setId(id);
            articleMapper.add(article);
            contentMap.put("id", id);
            articleMapper.addArticleContent(contentMap);
        } else {
            articleMapper.update(article);
            contentMap.put("id", article.getId());
            int n=articleMapper.checkContentExists(article.getId());
            if(n>0){
                articleMapper.updateArticleContent(contentMap);
            }else{
                articleMapper.addArticleContent(contentMap);
            }
        }
        msg.setExtend("id", article.getId());
        msg.setType(Msg.MsgType.success);
        msg.setContent("保存信息成功");
        return msg;
    }

    public void addBatch(List<Article> articles) {
        articleMapper.addBatch(articles);
    }

    public void update(Article article) {
        articleMapper.update(article);
    }

    public void updateByMap(Map<String, Object> map) {
        articleMapper.updateByMap(map);
    }

    public void delete(String id) {
        articleMapper.delete(id);
    }

    public void deleteBatch(String[] ids) {
        articleMapper.deleteBatch(ids);
    }
    public void batchPublish(String[] ids) {
        articleMapper.batchPublish(ids);
    }

    /*自定义方法*/

    @Deprecated
    public List<Article> findByRegxpLink(String reg) {
        return articleMapper.findByRegxpLink(reg);
    }

    /**
     * 查询博客首页的文章推荐专区
     * @param articleRegionType
     * @return
     */
    public List<Article> findByArticleRegionType(ArticleRegionType articleRegionType) {

        List<Article> list=(List<Article>) EhCacheUtils.getSysInfo("ArticleRegionType_", articleRegionType.getVal());
        if(CollectionUtils.isEmpty(list)){
            list=findByArticleRegionTypeInDb(articleRegionType);
            EhCacheUtils.putSysInfo("ArticleRegionType_", articleRegionType.getVal(), list);
        }
        return list;
    }
    public List<Article> findByArticleRegionTypeInDb(ArticleRegionType articleRegionType) {
        Map<String,Object> queryMap=new HashMap<String,Object>();
        queryMap.put("article_region",articleRegionType.getVal());
        List<Article> list = articleMapper.findByMap(new PageBounds(), queryMap);
        list.stream().forEach(a->{
            if(StringUtil.notEmpty(a.getTags())){
                String[] arr=a.getTags().split(",");
                a.setTags(getTags_name(arr[0]));
            }
        });//为节省空间，首页文章专区要一个标签
        return list;
    }

    @Autowired
    TemplateService templateService;
    public Object staticArticles(String... ids){
        for(String id:ids){
            staticArticle(id);
        }
        return Msg.success("批量静态化成功!");
    }
    /**
     * 静态化文章
     * @param id
     * @return
     */
    public Object staticArticle(String id){
        Article article=get(id);
        if(article==null){
            return Msg.error("不存在的文章!");
        }
        if(StringUtil.isEmpty(article.getTemplate_name())){
            return Msg.error("没有指定静态化模板!");//如果是爬虫趴取下来的文章都会有模板名称，但是手工添加的文章还没有制定静态化模板
        }
        //Map dataMap=MyBatisRequestUtil.convertBeanToMap(article);
        Map dataMap=JSONObject.parseObject(JSON.toJSONString(article),Map.class);
        dataMap=processTag(dataMap);
        if(article.getArticle_model().equals(ArticleModel.DUOGUYU.getVal())){
            List<Tags> fullTabsTags=tagsService.findFullTabs();
            dataMap.put("fullTabs",fullTabsTags);
        }
        Template template=templateService.getByName(article.getTemplate_name());
        String path =""; String relative_path="";String htmlName="";
        if(StringUtil.notEmpty(article.getLink())&&StringUtil.notEmpty(article.getExtra2())){

             htmlName= DigestUtils.md5Hex(StringUtil.notEmpty(article.getLink())?article.getLink():article.getExtra2());
        }else{
             htmlName=DigestUtils.md5Hex(article.getId());
        }
        String relative_base_path = GlobalConfig.getConfig("static.relativeBathPath");
        String outPath= "";
        if(GlobalConfig.isDevMode()){//开发模式
            outPath= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.webAppPath")+ GlobalConfig.getConfig("static.relativeBathPath");
        }else{
            outPath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.relativeBathPath");
        }
        path = outPath + File.separator;

        relative_path=relative_base_path+File.separator+"article"+File.separator+htmlName+ ".html";//文件相对路径
        path=path +"article"+File.separator+ htmlName+ ".html";//文件绝对路径

        if(template!=null){
            FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(FreeMarkerUtil.TemplateLoaderType.StringTemplateLoader,new HashMap<String,String>(){{
                put("name",template.getContent());
            }});
            boolean flag=freeMarkerUtil.ExecuteFile("name",dataMap,path);
            if(flag){
                logger.info(">>>>>>>>static file path success!====>"+path);
                article.setStatic_url(relative_path);
                this.update(article);
            }else{
                logger.warn(">>>>>>>>static file path fail!====>"+path);
            }

        }
        return Msg.success("静态化成功!");
    }


    public Map processTag(Map dataMap){

        String ids=(String)dataMap.get("tags");//文章标签tags,是逗号分隔的标签id
        if(StringUtil.notEmpty(ids)){
            String[] tagArr=null;
            List<Tags> tagsList=new ArrayList<Tags>();
            if(ids.contains(",")){
                tagArr=ids.split(",");
            }else{
                tagArr=new String[1];
                tagArr[0]=ids;
            }
            if(tagArr!=null){
                Tags tmp=null;
                for(String id:tagArr){
                    //检测每个标签再数据库cms_tag表中是否存在（根据名称），如果存在记下id，不存在则新增并记下id
                    tmp=tagsService.get(Integer.valueOf(id));
                    if(tmp!=null){
                        tagsList.add(tmp);
                    }
                }
                dataMap.put("tagsList",tagsList);//这个值用于输出模板文件的标签
            }
        }
        return dataMap;
    }

    public void processTag(Article article, Model model){

        //String ids=(String)dataMap.get("tags");//文章标签tags,是逗号分隔的标签id
        String ids=article.getTags();
        if(StringUtil.notEmpty(ids)){
            String[] tagArr=null;
            List<Tags> tagsList=new ArrayList<Tags>();
            if(ids.contains(",")){
                tagArr=ids.split(",");
            }else{
                tagArr=new String[1];
                tagArr[0]=ids;
            }
            if(tagArr!=null){
                Tags tmp=null;
                for(String id:tagArr){
                    //检测每个标签再数据库cms_tag表中是否存在（根据名称），如果存在记下id，不存在则新增并记下id
                    tmp=tagsService.get(Integer.valueOf(id));
                    if(tmp!=null){
                        tagsList.add(tmp);
                    }
                }
                model.addAttribute("tagsList",tagsList);//这个值用于输出模板文件的标签
            }
        }
    }

}
