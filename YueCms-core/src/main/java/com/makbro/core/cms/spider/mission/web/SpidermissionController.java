package com.makbro.core.cms.spider.mission.web;

import com.makbro.core.cms.resourcetype.service.ResourcetypeService;
import com.makbro.core.cms.spider.core.processor.service.PageProcessMissonService;
import com.makbro.core.cms.spider.mission.bean.Spidermission;
import com.makbro.core.cms.spider.mission.service.SpidermissionService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 趴取任务管理
 * @author  wujiyue on 2017-11-16 10:52:35.
 */

@Controller
@RequestMapping("/spider/spidermission")
@RequiresPermissions("spider")
public class SpidermissionController extends BaseController {
    @Autowired
    protected SpidermissionService spidermissionService;
    @Autowired
    protected ResourcetypeService resourcetypeService;
    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/spider/spidermission/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Spidermission spidermission, Model model){
                return "/cms/spider/spidermission/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Spidermission spidermission,Model model){
        if(spidermission!=null&&spidermission.getId()!=null){
            spidermission=spidermissionService.get(spidermission.getId());
        }
         model.addAttribute("spidermission",spidermission);
         return "/cms/spider/spidermission/edit";
    }
    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return spidermissionService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return spidermissionService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }



    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return spidermissionService.save(map);
    }

    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable Integer id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            spidermissionService.updateByMap(map);
            return Msg.success("删除成功!");
        }catch (Exception e){
            return Msg.error("删除失败!");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable Integer[] ids){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            spidermissionService.updateByMapBatch(map);
            return Msg.success("批量删除成功!");
        }catch (Exception e){
            return Msg.error("批量删除失败!");
        }
	}


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	try{
            spidermissionService.delete(id);
            return Msg.success("删除成功!");
        }catch (Exception e){
            return Msg.error("删除失败!");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
            spidermissionService.deleteBatch(ids);
            return Msg.success("删除成功!");
         }catch (Exception e){
            return Msg.error("删除失败!");
         }
    }
    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return spidermissionService.saveSort(MyBatisRequestUtil.getMap(request));
    }

    /**
     * 检测任务代码是否可用
     * @return
     */
    @ResponseBody
    @RequestMapping("/json/checkMissionCode")
    public Object checkMissionCode() {
        return spidermissionService.checkMissionCode(MyBatisRequestUtil.getMap(request));
    }


    @Autowired
    PageProcessMissonService pageProcessMissonService;
    @ResponseBody
    @RequestMapping("/json/startMission/{id}")
    public Object startMission(@PathVariable Integer id) {
        Map map= MyBatisRequestUtil.getMap(request);
        Spidermission spidermission=spidermissionService.get(id);
        if(spidermission==null|| StringUtil.isEmpty(spidermission.getMissionCode())){
            return Msg.error("不存在该任务!");
        }
        if("1".equals(spidermission.getStatus())){
            return Msg.error("任务在执行中!");
        }
        return pageProcessMissonService.spiderByMissionCode(spidermission.getMissionCode());
    }

}