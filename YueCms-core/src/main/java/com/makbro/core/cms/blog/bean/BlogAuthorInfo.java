package com.makbro.core.cms.blog.bean;


import com.markbro.base.model.AliasModel;

/**
 * 博客访问信息 bean
 * @author wujiyue
 * @date
 */
public class BlogAuthorInfo implements AliasModel {

	private String yhid;//
	private String name;//名称
	private String headerPath;//头像地址
	private Integer visit_today;//今日访问
	private Integer visit_yesterday;//昨日访问
	private Integer visit_week;//周访问
	private Integer visit_month;//月访问
	private Integer visit_total;//总访问
	private Integer num_fans;//粉丝数
	private Integer num_yuanchuang;//原创数
	private Integer num_like;//喜欢数
	private String update_time;//信息更新时间
	private String join_time;//加入时间
	private String last_login_time;//最近登录世家你

	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public Integer getVisit_today(){ return visit_today ;}
	public void  setVisit_today(Integer visit_today){this.visit_today=visit_today; }
	public Integer getVisit_yesterday(){ return visit_yesterday ;}
	public void  setVisit_yesterday(Integer visit_yesterday){this.visit_yesterday=visit_yesterday; }
	public Integer getVisit_week(){ return visit_week ;}
	public void  setVisit_week(Integer visit_week){this.visit_week=visit_week; }
	public Integer getVisit_month(){ return visit_month ;}
	public void  setVisit_month(Integer visit_month){this.visit_month=visit_month; }
	public Integer getVisit_total(){ return visit_total ;}
	public void  setVisit_total(Integer visit_total){this.visit_total=visit_total; }
	public Integer getNum_fans(){ return num_fans ;}
	public void  setNum_fans(Integer num_fans){this.num_fans=num_fans; }
	public Integer getNum_yuanchuang(){ return num_yuanchuang ;}
	public void  setNum_yuanchuang(Integer num_yuanchuang){this.num_yuanchuang=num_yuanchuang; }
	public String getUpdate_time(){ return update_time ;}
	public void  setUpdate_time(String update_time){this.update_time=update_time; }

	public String getJoin_time() {
		return join_time;
	}

	public void setJoin_time(String join_time) {
		this.join_time = join_time;
	}

	public String getLast_login_time() {
		return last_login_time;
	}

	public void setLast_login_time(String last_login_time) {
		this.last_login_time = last_login_time;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeaderPath() {
		return headerPath;
	}

	public void setHeaderPath(String headerPath) {
		this.headerPath = headerPath;
	}

	public Integer getNum_like() {
		return num_like;
	}

	public void setNum_like(Integer num_like) {
		this.num_like = num_like;
	}

	@Override
	public String toString() {
		return "BlogAuthorInfo{" +
				"yhid='" + yhid + '\'' +
				", name='" + name + '\'' +
				", headerPath='" + headerPath + '\'' +
				", visit_today=" + visit_today +
				", visit_yesterday=" + visit_yesterday +
				", visit_week=" + visit_week +
				", visit_month=" + visit_month +
				", visit_total=" + visit_total +
				", num_fans=" + num_fans +
				", num_yuanchuang=" + num_yuanchuang +
				", num_like=" + num_like +
				", update_time='" + update_time + '\'' +
				", join_time='" + join_time + '\'' +
				", last_login_time='" + last_login_time + '\'' +
				'}';
	}
}
