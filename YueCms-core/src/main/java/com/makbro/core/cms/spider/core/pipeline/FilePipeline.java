package com.makbro.core.cms.spider.core.pipeline;


import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Task;
import com.makbro.core.cms.spider.core.utils.FilePersistentBase;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.annotation.ThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;

/**
 * Store results in files.<br>
 *
 * @author code4crafter@gmail.com <br>
 * @since 0.1.0
 */
@ThreadSafe
public class FilePipeline extends FilePersistentBase implements Pipeline {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * create a FilePipeline with default path"/data/webmagic/"
     */
    public FilePipeline() {
        setPath("/data/webmagic/");
    }

    public FilePipeline(String path) {
        setPath(path);
    }

    @Override
    public void process(ResultItems resultItems, Task task) {
        String path = this.path + PATH_SEPERATOR + task.getUUID() + PATH_SEPERATOR;
        try {
            File f=getFile(path + DigestUtils.md5Hex(resultItems.getRequest().getUrl()) + ".html");
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f),"UTF-8"));
            logger.info("filepath===>"+f.getAbsolutePath());
            printWriter.println("<html>");
            printWriter.println("<head>");
            printWriter.println("<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">");
            printWriter.println("</head>");
            printWriter.println("<body>");

            printWriter.println("url:\t" + resultItems.getRequest().getUrl()+"\r\n");
            for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
                if (entry.getValue() instanceof Iterable) {
                    Iterable value = (Iterable) entry.getValue();
                    printWriter.println(entry.getKey() + ":");
                    for (Object o : value) {
                        printWriter.println(o);
                        printWriter.println("\r\n");
                    }
                    printWriter.println("\r\n");
                } else {
                    printWriter.println(entry.getKey() + ":\t" + entry.getValue()+"\r\n");
                }
            }

            printWriter.println("</body>");
            printWriter.println("</html>");
            printWriter.close();
        } catch (IOException e) {
            logger.warn("write file error", e);
        }
    }
}
