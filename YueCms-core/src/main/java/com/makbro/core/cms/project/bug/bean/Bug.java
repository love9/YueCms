package com.makbro.core.cms.project.bug.bean;


import com.markbro.base.model.AliasModel;

/**
 * 项目Bug bean
 * @author wujiyue
 */
public class Bug  implements AliasModel {

	private Integer id;//项目编号
	private Integer projectId;//项目ID
	private String moduleId;//模块ID
	private String title;//Bug标题
	private Object content;//Bug描述
	private Integer bugtype;//Bug类型
	private Integer priority;//完成优先级。优先级比较高的排在上面。
	private String copyto;//任务抄送给
	private String status;//Bug状态
	private String attachfiles;//附件ids
	private String assignTo;//指派给。多人用分号分割
	private String assignToNames;//完成者
	private String createBy;//创建者
	private String finishBy;//完成者

	public String getAssignToNames() {
		return assignToNames;
	}

	public void setAssignToNames(String assignToNames) {
		this.assignToNames = assignToNames;
	}

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public Integer getProjectId(){ return projectId ;}
	public void  setProjectId(Integer projectId){this.projectId=projectId; }
	public String getModuleId(){ return moduleId ;}
	public void  setModuleId(String moduleId){this.moduleId=moduleId; }
	public String getTitle(){ return title ;}
	public void  setTitle(String title){this.title=title; }
	public Object getContent(){ return content ;}
	public void  setContent(Object content){this.content=content; }
	public Integer getBugtype(){ return bugtype ;}
	public void  setBugtype(Integer bugtype){this.bugtype=bugtype; }
	public Integer getPriority(){ return priority ;}
	public void  setPriority(Integer priority){this.priority=priority; }
	public String getCopyto(){ return copyto ;}
	public void  setCopyto(String copyto){this.copyto=copyto; }
	public String getStatus(){ return status ;}
	public void  setStatus(String status){this.status=status; }
	public String getAttachfiles(){ return attachfiles ;}
	public void  setAttachfiles(String attachfiles){this.attachfiles=attachfiles; }
	public String getAssignTo(){ return assignTo ;}
	public void  setAssignTo(String assignTo){this.assignTo=assignTo; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getFinishBy(){ return finishBy ;}
	public void  setFinishBy(String finishBy){this.finishBy=finishBy; }


	@Override
	public String toString() {
	return "Bug{" +
			"id=" + id+
			", projectId=" + projectId+
			", moduleId=" + moduleId+
			", title=" + title+
			", content=" + content+
			", bugtype=" + bugtype+
			", priority=" + priority+
			", copyto=" + copyto+
			", status=" + status+
			", attachfiles=" + attachfiles+
			", assignTo=" + assignTo+
			", createBy=" + createBy+
			", finishBy=" + finishBy+
			 '}';
	}
}
