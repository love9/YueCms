package com.makbro.core.cms.image.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.image.bean.Image;
import com.makbro.core.cms.image.dao.ImageMapper;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.SysPara;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.fileupload.util.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.*;

/**
 * 图片 service
 * @author wujiyue
 * @date 2019-7-4 21:58:58
 */
@Service
public class ImageService{
    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private TableKeyService keyService;
     /*基础公共方法*/
    public Image get(String id){
        return imageMapper.get(id);
    }
    public Map<String,Object> getMap(String id){
        return imageMapper.getMap(id);
    }
    public List<Map<String,Object>> find(PageBounds pageBounds,Map<String,Object> map){
        return imageMapper.find(pageBounds,map);
    }
    public List<Image> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return imageMapper.findByMap(pageBounds,map);
    }
    public void add(Image image){
        imageMapper.add(image);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
                 try{
                     Image image= JSON.parseObject(JSON.toJSONString(map),Image.class);
                     if(image.getId()==null||"".equals(image.getId().toString())){
                         String id= keyService.getStringId();
                         image.setYhid(String.valueOf(map.get(TmConstant.YHID_KEY)));
                         image.setId(id);
                         imageMapper.add(image);
                     }else{

                         imageMapper.update(image);
                     }
                     msg.setType(Msg.MsgType.success);
                     msg.setContent("保存信息成功");
                 }catch (Exception ex){
                     msg.setType(Msg.MsgType.error);
                     msg.setContent("保存信息失败");
                 }
                return msg;
    }
    public void addBatch(List<Image> images){
        imageMapper.addBatch(images);
    }
    public void update(Image image){
        imageMapper.update(image);
    }
    public void updateByMap(Map<String,Object> map){
        imageMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        imageMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        imageMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        imageMapper.deleteBatch(ids);
    }

    public void deleteFile(String id, HttpServletRequest req){
        Image image=imageMapper.get(id);
        String basePathType = SysPara.getValue("uploadpath_type", "0");
        String basepath="";
        if("1".equals(basePathType)){
            basepath= SysPara.getValue("uploadfile_basepath","D:/dzd");
        }else{
            //资源图片路径(相对于webapp下的路径)
            basepath=req.getSession().getServletContext().getRealPath("/");
        }
        if(image!=null){
            String path=basepath+image.getUrl();
            File f=new File(path);
            f.delete();
        }
    }

     /*自定义方法*/

    public Map<String, Object> uploadSingle(Map<String, Object> map, HttpServletRequest req, MultipartFile mFile) {
        Map<String, Object> rmap=new HashMap<String, Object>();
        String basePathType = SysPara.getValue("uploadpath_type","0");
        String basepath = "";
        String relativePath = "";
        if ("1".equals(basePathType)) {//绝对路径类型
            basepath = SysPara.getValue("uploadfile_basepath","D:/dzd");
            relativePath = "/uploadresources";
        } else {
            basepath = req.getSession().getServletContext().getRealPath("/");
            relativePath = SysPara.getValue("uploadfile_basepath", "/resources/static/cms/images");
        }
        relativePath= StringUtil.subEndStr(relativePath,"/");
        File dirFile=new File(basepath+relativePath);
        if(!dirFile.exists()){
            dirFile.mkdirs();
        }
        try {
            long size = 0;
            String path = "";
            String filename = "";
            String name = "";
            String dir = "";
            String suffixes ="";
            String guid= Guid.get();
            if (!mFile.isEmpty()) {
                BufferedInputStream in = new BufferedInputStream(mFile.getInputStream());
                filename = mFile.getOriginalFilename();
                name=filename.substring(0,filename.indexOf("."));
                // 取得文件后缀
                suffixes = filename.substring(filename.lastIndexOf("."), filename.length());
                path= relativePath+"/"+ DateUtil.formatDate(new Date(),"yyyyMMdd")+"/"+guid + suffixes;//相对路径
                dir = basepath+File.separatorChar+path;// 绝对路径
                String tempDir=dir.substring(0,dir.lastIndexOf("/"));
                File tempDirFile=new File(tempDir);
                if(!tempDirFile.exists()){
                    tempDirFile.mkdirs();
                }
                File ffout = new File(dir);
                rmap.put("abpath", ffout.getCanonicalPath());//服务器绝对路径
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(ffout));
                Streams.copy(in, out, true);
                size = ffout.length();
            }

            rmap.put("path", path);
            rmap.put("name", name);
            rmap.put("suffixes", suffixes);
            rmap.put("size", size);
            if("1".equals(String.valueOf(map.get("saveDb")))){
                //数据记录保存到数据库
                rmap.put("url",rmap.get("path"));
                rmap.put("id",Guid.get());
                imageMapper.addByMap(rmap);
            }
        }catch (IOException e) {
                e.printStackTrace();
        }
        return rmap;
    }
    public List<Map<String, Object>> uploadMulti(Map<String, Object> map, HttpServletRequest req, List<MultipartFile> mFiles) {
        List<Map<String, Object>> resultList=new ArrayList<Map<String, Object>>();
        //资源图片路径(相对于webapp下的路径)
        String basepath=req.getSession().getServletContext().getRealPath("/");
        String relativePath=null;
        try {
            req.setCharacterEncoding("utf-8");
            relativePath= SysPara.getValue("uploadfile_basepath");
        } catch (Exception e) {
            relativePath= "/resources/static/cms/images";
        }
        relativePath= StringUtil.subEndStr(relativePath,"/");
        File dirFile=new File(basepath);
        if(!dirFile.exists()){
            dirFile.mkdirs();
        }
        try {
            long size = 0;
            String path = "";
            String filename = "";
            String name = "";
            String dir = "";
            String suffixes ="";
            String guid= null;
            MultipartFile mFile=null;
            int n=mFiles.size();
            Map<String, Object> tmap=null;
            for(int i=0;i<n;i++) {
                if (!mFile.isEmpty()) {
                    tmap=new HashMap<String, Object>();
                    guid= Guid.get();
                    BufferedInputStream in = new BufferedInputStream(mFile.getInputStream());
                    filename = mFile.getOriginalFilename();

                    name = filename.substring(0, filename.indexOf("."));
                    // 取得文件后缀
                    suffixes = filename.substring(filename.lastIndexOf("."), filename.length());
                    path = relativePath + "/" + DateUtil.formatDate(new Date(), "yyyyMMdd") + "/" + guid + suffixes;//相对路径
                    dir = basepath + File.separatorChar + path;// 绝对路径
                    String tempDir = dir.substring(0, dir.lastIndexOf("/"));
                    File tempDirFile = new File(tempDir);
                    if (!tempDirFile.exists()) {
                        tempDirFile.mkdirs();
                    }
                    File ffout = new File(dir);
                    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(ffout));
                    Streams.copy(in, out, true);
                    size = ffout.length();
                }
                tmap.put("path", path);
                tmap.put("name", name);
                tmap.put("suffixes", suffixes);
                tmap.put("size", size);
                resultList.add(tmap);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return resultList;
    }

}
