package com.makbro.core.cms.spider.mission.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.spider.mission.bean.Spidermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 趴取任务 dao
 * @author  wujiyue on 2017-11-16 10:52:35.
 */
@Repository
public interface SpidermissionMapper{
    public Spidermission get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Spidermission spidermission);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Spidermission> spidermissions);
    public void update(Spidermission spidermission);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    public List<Spidermission> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Spidermission> findByMap(PageBounds pageBounds, Map<String, Object> map);
	public int updateSort(@Param("id") String id, @Param("sort") String sort);
	public Integer getMaxSort();
    public Spidermission getByMissonCode(String missionCode);
    public Integer checkMissionCode(String missionCode);
}
