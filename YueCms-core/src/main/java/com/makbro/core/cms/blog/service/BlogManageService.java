package com.makbro.core.cms.blog.service;


import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.orgUser.dao.OrgUserMapper;
import com.makbro.core.cms.blog.bean.BlogAuthorInfo;
import com.makbro.core.cms.blog.dao.BlogManageMapper;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;


@Service
public class BlogManageService {

    @Autowired
    BlogManageMapper blogManageMapper;
    @Autowired
    OrgUserMapper orgUserMapper;
    private static final String KEY_BLOG_VISIT_COUNT="blogVisitCount";//一小段时间内博客访问次数缓存key
    public static final String KEY_BLOG_AUTHOR_INFO="blogAuthorInfo";

    /**
     * 插入一条信息
     * @param yhid
     */
    public void addBlogAuthorInfo(String yhid){
        BlogAuthorInfo blogAuthorInfo=blogManageMapper.getBlogAuthorInfo(yhid);
        if(blogAuthorInfo==null){
            blogAuthorInfo=new BlogAuthorInfo();
            blogAuthorInfo.setYhid(yhid);
            blogAuthorInfo.setVisit_today(0);
            blogAuthorInfo.setVisit_week(0);
            blogAuthorInfo.setVisit_month(0);
            blogAuthorInfo.setVisit_total(0);
            blogAuthorInfo.setVisit_yesterday(0);
            blogAuthorInfo.setUpdate_time(DateUtil.getDatetime());
            OrgUser user=orgUserMapper.get(yhid);
            if(user!=null){
                blogAuthorInfo.setName(user.getNickname());
                blogAuthorInfo.setHeaderPath(user.getHeaderPath());
                blogAuthorInfo.setJoin_time(user.getCreateTime());
                blogAuthorInfo.setLast_login_time(user.getLastLoginTime());
            }
            int fansNum=0;//todo 目前还没有粉丝这以概念
            blogAuthorInfo.setNum_fans(fansNum);
            int yuanChuangNum=0;
            blogAuthorInfo.setNum_yuanchuang(yuanChuangNum);
            int num_like=0;
            blogAuthorInfo.setNum_like(num_like);

            blogManageMapper.addBlogAuthorInfo(blogAuthorInfo);
        }
    }
    /**
     * 获得博客文章博主信息（头像、昵称、原创数、粉丝数、喜欢数、评论数、访问数等）
     * @param yhid
     * @return
     */
    public Map<String,Object> getBlogAuthorInfo(String yhid){
        Map<String,Object> map=null;
        map=(Map<String,Object>) EhCacheUtils.getUserInfo(KEY_BLOG_AUTHOR_INFO,yhid);
        if(map==null){
            /*int visit_today=0;//今日访问
            int visit_yesterday=0;//昨日访问
            int visit_week=0;//周访问
            int visit_month=0;//月访问
            int visit_total=0;//总访问
            String join_time="";
            String last_login_time="";
            int num_fans=0;//粉丝数
            int num_yuanchuang=0;//原创数*/
            map=blogManageMapper.getMapBlogAuthorInfo(yhid);
            if(map!=null){
                EhCacheUtils.putUserInfo(KEY_BLOG_AUTHOR_INFO,yhid,map);
            }
        }else{
            try{
                String updateTime=String.valueOf(map.get("update_time"));
                if(StringUtil.notEmpty(updateTime)){
                    Date tempDate = DateUtil.addMinute(DateUtil.parseDatetime(updateTime), 30);
                    if(new Date().after(tempDate)){
                        //当前时间在数据库更新时间+30分钟之后
                        map=blogManageMapper.getMapBlogAuthorInfo(yhid);
                        EhCacheUtils.putUserInfo(KEY_BLOG_AUTHOR_INFO,yhid,map);
                    }
                }else{
                    map=blogManageMapper.getMapBlogAuthorInfo(yhid);
                    if(map!=null){
                        EhCacheUtils.putUserInfo(KEY_BLOG_AUTHOR_INFO,yhid,map);
                    }
                }

            }catch (Exception ex){
                EhCacheUtils.removeUserInfo(KEY_BLOG_AUTHOR_INFO,yhid);
            }

        }
        return map;
    }

    /**
     * 访问了默认的博客，缓存计数器+1
     * @param blogYhid
     * @return
     */
    public void blogVisit(String blogYhid){
        Integer n=(Integer) EhCacheUtils.getUserInfo(KEY_BLOG_VISIT_COUNT,blogYhid);
        if(n==null){
            n=1;
        }else{
            n++;
        }
        EhCacheUtils.putUserInfo(KEY_BLOG_VISIT_COUNT,blogYhid,n);
    }
}
