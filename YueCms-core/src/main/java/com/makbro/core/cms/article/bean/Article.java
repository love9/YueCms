package com.makbro.core.cms.article.bean;

import com.markbro.base.model.AliasModel;

/**
 * 文章 bean
 * @author  wujiyue on 2016-08-14 20:02:44.
 */
public class Article  implements AliasModel {
	private String id;//主键，文章ID
	private String yhid;//系统用户的ID
	private String article_region;//数据范围
	private String title;//文章标题
	private String keywords;//关键词
	private String description;//摘要
	private String cover_image;//封面图片
	private String content;//文章内容
	private String content_markdown_source;//文章markdown源码

	private String article_model;//文章模型  多骨鱼模型，其它等
	private Integer copy_flag;//转载标志 1转载 0原创
	private String channelids;//频道栏目ID
	private String personal_category;//来源
	private String link;//原始链接
	private String static_url;//静态化后url
	private String tags;//标签
	private Integer hit;//点击数
	private Integer reply_num;//回复数
	private Integer up_vote;//点赞数
	private Integer down_vote;//差评数
	private Integer hot_flag;//热点标志
	private Integer new_flag;//新增标志
	private Integer comment_flag;//是否开启评论
	private Integer top_flag;//置顶标志
	private Integer favourite;//收藏数
	private String mission_id;//趴取任务的ID
	private String template_name;//生成静态化页面的模板ID
	private String createTime;//创建时间
	private String updateTime;//更新时间
	private Integer available;//状态标志
	private Integer deleted;//删除标志
	private String extra1;//附加字段1
	private String extra2;//附加字段2
	private String extra3;//附加字段3
	private String publishTime;//发布时间
	private String author;//原文作者

	private String article_model_name;//扩展字段。

	private String tags_name;//扩展字段。标签名称
	private String tag;//扩展字段，存放一个标签
	private String tag_name;//扩展字段，存放一个标签

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }

	public String getArticle_region() {
		return article_region;
	}

	public void setArticle_region(String article_region) {
		this.article_region = article_region;
	}

	public String getTitle(){ return title ;}
	public void  setTitle(String title){this.title=title; }
	public String getKeywords(){ return keywords ;}
	public void  setKeywords(String keywords){this.keywords=keywords; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getContent(){ return content ;}
	public String getContent_markdown_source() {
		return content_markdown_source;
	}
	public void setContent_markdown_source(String content_markdown_source) {
		this.content_markdown_source = content_markdown_source;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTag_name() {
		return tag_name;
	}

	public void setTag_name(String tag_name) {
		this.tag_name = tag_name;
	}

	public String getTags_name() {
		return tags_name;
	}

	public void setTags_name(String tags_name) {
		this.tags_name = tags_name;
	}

	public String getArticle_model() {
		return article_model;
	}

	public String getArticle_model_name() {
		return article_model_name;
	}

	public void setArticle_model_name(String article_model_name) {
		this.article_model_name = article_model_name;
	}

	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getExtra2() {
		return extra2;
	}

	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getExtra3() {
		return extra3;
	}

	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}

	public void setArticle_model(String article_model) {
		this.article_model = article_model;
	}

	public String getTemplate_name() {
		return template_name;
	}

	public void setTemplate_name(String template_name) {
		this.template_name = template_name;
	}

	public String getMission_id() {
		return mission_id;
	}

	public void setMission_id(String mission_id) {
		this.mission_id = mission_id;
	}

	public String getCover_image() {
		return cover_image;
	}

	public void setCover_image(String cover_image) {

		this.cover_image = cover_image;
	}

	public Integer getCopy_flag() {
		return copy_flag;
	}

	public void setCopy_flag(Integer copy_flag) {
		this.copy_flag = copy_flag;
	}

	public Integer getReply_num() {
		return reply_num;
	}

	public void setReply_num(Integer reply_num) {
		this.reply_num = reply_num;
	}

	public void  setContent(String content){this.content=content; }

	public String getChannelids(){ return channelids ;}
	public void  setChannelids(String channelids){this.channelids=channelids; }
	public String getPersonal_category(){ return personal_category ;}
	public void  setPersonal_category(String personal_category){this.personal_category=personal_category; }
	public String getLink(){ return link ;}
	public void  setLink(String link){this.link=link; }
	public String getStatic_url(){ return static_url ;}
	public void  setStatic_url(String static_url){this.static_url=static_url; }
	public String getTags(){ return tags ;}
	public void  setTags(String tags){this.tags=tags; }
	public Integer getHit(){ return hit ;}
	public void  setHit(Integer hit){this.hit=hit; }
	public Integer getUp_vote(){ return up_vote ;}
	public void  setUp_vote(Integer up_vote){this.up_vote=up_vote; }
	public Integer getDown_vote(){ return down_vote ;}
	public void  setDown_vote(Integer down_vote){this.down_vote=down_vote; }
	public Integer getHot_flag(){ return hot_flag ;}
	public void  setHot_flag(Integer hot_flag){this.hot_flag=hot_flag; }
	public Integer getNew_flag(){ return new_flag ;}
	public void  setNew_flag(Integer new_flag){this.new_flag=new_flag; }
	public Integer getComment_flag(){ return comment_flag ;}
	public void  setComment_flag(Integer comment_flag){this.comment_flag=comment_flag; }
	public Integer getTop_flag(){ return top_flag ;}
	public void  setTop_flag(Integer top_flag){this.top_flag=top_flag; }
	public Integer getFavourite(){ return favourite ;}
	public void  setFavourite(Integer favourite){this.favourite=favourite; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Article{" +
				"id='" + id + '\'' +
				", yhid='" + yhid + '\'' +
				", article_region='" + article_region + '\'' +
				", title='" + title + '\'' +
				", keywords='" + keywords + '\'' +
				", description='" + description + '\'' +
				", cover_image='" + cover_image + '\'' +

				", article_model='" + article_model + '\'' +
				", copy_flag='" + copy_flag + '\'' +
				", channelids='" + channelids + '\'' +
				", personal_category='" + personal_category + '\'' +
				", link='" + link + '\'' +
				", static_url='" + static_url + '\'' +
				", tags='" + tags + '\'' +
				", hit=" + hit +
				", reply_num=" + reply_num +
				", up_vote=" + up_vote +
				", down_vote=" + down_vote +
				", hot_flag=" + hot_flag +
				", new_flag=" + new_flag +
				", comment_flag=" + comment_flag +
				", top_flag=" + top_flag +
				", favourite=" + favourite +
				", mission_id='" + mission_id + '\'' +
				", template_name='" + template_name + '\'' +
				", createTime='" + createTime + '\'' +
				", updateTime='" + updateTime + '\'' +
				", available=" + available +
				", deleted=" + deleted +
				", publishTime=" + publishTime +
				", author=" + author +
				", article_model_name='" + article_model_name + '\'' +
				'}';
	}
}
