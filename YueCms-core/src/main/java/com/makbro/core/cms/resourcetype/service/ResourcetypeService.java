package com.makbro.core.cms.resourcetype.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.resourcetype.bean.Resourcetype;
import com.makbro.core.cms.resourcetype.dao.ResourcetypeMapper;
import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 资源分类 service
 * @author  wujiyue on 2016-07-21 21:21:06.
 */
@Service
public class ResourcetypeService implements InitCacheListener {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private ResourcetypeMapper resourcetypeMapper;
    @Autowired
    private TableKeyService keyService;

    private List<Resourcetype> resourcetypeList;
     /*基础公共方法*/
    public Resourcetype get(String id){
        return resourcetypeMapper.get(id);
    }
    public Map<String,Object> getMap(String id){
        return resourcetypeMapper.getMap(id);
    }
    public List<Resourcetype> find(PageBounds pageBounds,Map<String,Object> map){
        return resourcetypeMapper.find(pageBounds,map);
    }
    public List<Resourcetype> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return resourcetypeMapper.findByMap(pageBounds,map);
    }
    public void add(Resourcetype resourcetype){
        resourcetypeMapper.add(resourcetype);
    }
    public Object save(Resourcetype resourcetype){
          Msg msg=new Msg();
                 try{
                     if(resourcetype.getId()==null||"".equals(resourcetype.getId().toString())){
                         String id= keyService.getStringId();
                         resourcetype.setId(id);
                         String parentid=resourcetype.getParentid();
                         String pids= resourcetypeMapper.getParentidsById(parentid);
                         if("null".equals(pids)||pids==null){
                             pids="0,";
                         }
                         pids+=id+",";
                         resourcetype.setParentids(pids);
                         int sort=resourcetypeMapper.getMaxSortByParentid(parentid);
                         resourcetype.setSort(sort+1);
                         resourcetypeMapper.add(resourcetype);
                     }else{
                         String parentid =resourcetype.getParentid();
                         String pids= resourcetypeMapper.getParentidsById(parentid);
                         if("null".equals(pids)||pids==null){
                             pids="0,";
                         }
                         pids+=resourcetype.getId()+",";
                         resourcetype.setParentids(pids);
                         resourcetypeMapper.update(resourcetype);
                     }
                     msg.setType(Msg.MsgType.success);
                     msg.setContent("保存信息成功");
                 }catch (Exception ex){
                     msg.setType(Msg.MsgType.error);
                     msg.setContent("保存信息失败");
                 }
                return msg;
    }
    public void addBatch(List<Resourcetype> resourcetypes){
        resourcetypeMapper.addBatch(resourcetypes);
    }
    public void update(Resourcetype resourcetype){
        resourcetypeMapper.update(resourcetype);
    }
    public void updateByMap(Map<String,Object> map){
        resourcetypeMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        resourcetypeMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        resourcetypeMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        resourcetypeMapper.deleteBatch(ids);
    }
     /*自定义方法*/

	 public List<Resourcetype> findByParentid(PageBounds pageBounds, String parentid){
		return resourcetypeMapper.findByParentid(pageBounds,parentid);
	}

    public List<Map<String,Object>> ztree(Map<String,Object> map){
        String parentid= (String) map.get("parentid");
        String rootFlag= (String) map.get("rootFlag");//代表用户指定了根节点
        String rootName= (String) map.get("rootName");//用户指定了根节点名称
        if(!StringUtil.isEmpty(rootName)&&!"null".equals(rootName)){
            //根据用户想要的根节点名称查找节点id
            parentid=resourcetypeMapper.getIdByName(rootName);
            if(StringUtil.isEmpty(parentid)||"null".equals(parentid)){
                parentid="0";
            }else{
                rootFlag="1";
            }
        }


        List<Resourcetype> list=new ArrayList<Resourcetype>();
        if("1".equals(rootFlag)){
            Resourcetype root= resourcetypeMapper.get(parentid);
            list.add(root);
        }else{
           list=resourcetypeMapper.findByParentid(new PageBounds(),parentid);
        }

        List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
        Map<String,Object> tmap=null;
        String id=null;
        int n=0;
        for(Resourcetype t:list){
            tmap=new HashMap<String,Object>();
            tmap = JSON.parseObject(JSON.toJSONString(t));
            id=t.getId();
            n=resourcetypeMapper.findByParentidCount(id);
           if(n>0){
                tmap.put("isParent",true);
            }
           /*  String type= (String) tmap.get("type");
            if("bm".equals(type)){
                tmap.put("iconSkin","bm");
            }
            if("gw".equals(type)){
                tmap.put("iconSkin","gw");
            }
            if("ry".equals(type)){
                tmap.put("iconSkin","ry");
            }*/
            result.add(tmap);
        }
        return result;
    }

    public int getChildrenCount(String ids){
        ids=ids.replaceAll(",", "','").replaceAll("~", "','");
        return resourcetypeMapper.getChildrenCount(ids);
    }




    @Transactional
    public Object saveSort(Map map){
        Msg msg=new Msg();
        try{
            String sort = String.valueOf(map.get("sort"));
            if(!"".equals(sort)){
                String[] sx = sort.split(",");
                for(int i=0;i<sx.length;i++) {
                    String[] arr = sx[i].split("_");
                    resourcetypeMapper.updateSort(arr[1], arr[2]);
                }
            }
            msg.setType(Msg.MsgType.success);
            msg.setContent("排序成功！");
        }catch (Exception e){
            msg.setType(Msg.MsgType.error);
            msg.setContent("排序失败！");
        }
        return msg;
    }
    public List<Map<String,Object>> treeSelect(Map<String,Object> map){

        List<Map<String,Object>> list=resourcetypeMapper.treeSelect();
        List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
        Map<String,Object> tmap=null;
        String id=null;
        int n=0;
        for(Map<String,Object> t:list){
            tmap=new HashMap<String,Object>();
            n=String.valueOf(t.get("parentids")).split(",").length-1;
            tmap.put("dm",String.valueOf(t.get("id")));
            tmap.put("mc",getPrependStr(n)+String.valueOf(t.get("name")));
            result.add(tmap);
        }
        return result;
    }
    private  String getPrependStr(int n){
        String s="|---";
        if(n==1){
            return s;
        }else if(n>1){
            for(int i=0;i<n-1;i++){
                s+="|----";
            }
            return s;
        }else{
            return "";
        }

    }
    //获得某个资源类型及下所有孩子
    public  void findChildren(List<Resourcetype> resourcetypes,String id){
        Resourcetype resourcetype=resourcetypeMapper.get(id);
        List<Resourcetype> tempList=resourcetypeMapper.findByParentid(new PageBounds(),resourcetype.getId());
        resourcetypes.addAll(tempList);
        if(tempList!=null&&tempList.size()>0){
            for (Resourcetype r:tempList){
                //判断是否还有孩子
               int n= resourcetypeMapper.getChildrenCount(r.getId());
                if(n>0){
                    findChildren(resourcetypes,r.getId());
                }

            }

        }
    }
    //判断传入的 resource_type_id 是否是某个资源或其子资源
    public boolean checkIsChildren(String resource_type_id,String name){


        List<Resourcetype> list =findByName(name);
        if(CollectionUtils.isEmpty(list)){
            logger.info("根据{}查询出集合为空！",name);
            return  false;
        }
        List<Resourcetype> resourcetypes = new ArrayList<Resourcetype>();
        findChildren(resourcetypes,list.get(0).getId());
        resourcetypes.add(list.get(0));
        return    resourcetypes.stream().anyMatch(resourcetype->{return resource_type_id.equals(resourcetype.getId());});
    }

    @Override
    public void onInit() {
        logger.info("-------ResourcetypeService--onInit-------");
        //将数据缓存到EhCache中取
        Map map=new HashMap<String,Object>();
        //map.put("","");
        List<Resourcetype> list = resourcetypeMapper.find(new PageBounds(), map);
        for(Resourcetype resourcetype:list){
            if(StringUtil.notEmpty(resourcetype.getCode())){
                EhCacheUtils.putSysInfo("resourcetype",resourcetype.getCode(),resourcetype);
            }
        }
        resourcetypeList=list;
    }

    public String getIdByCode(String code) {
        Resourcetype resourcetype =null;
        try{
           resourcetype = (Resourcetype)EhCacheUtils.getSysInfo("resourcetype", code);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        if(resourcetype!=null){
            return resourcetype.getId();
        }else{
            return  resourcetypeMapper.getIdByCode(code);
        }
    }

    public List<Resourcetype> findByName(String name) {
        if(resourcetypeList!=null){
          return   resourcetypeList.stream().filter(t->{return  t.getName().equals(name);}).collect(Collectors.toList());
        }else{
            return resourcetypeMapper.findByName(name);
        }
    }



}
