package com.makbro.core.cms.sendEmail.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.sendEmail.bean.SendEmail;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 邮件 dao
 * Created by wujiyue on 2017-09-07 16:30:44.
 */
@Repository
public interface SendEmailMapper{

    public SendEmail get(String id);
    public Map<String,Object> getMap(String id);
    public void add(SendEmail sendEmail);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<SendEmail> sendEmails);
    public void update(SendEmail sendEmail);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<SendEmail> find(PageBounds pageBounds, Map<String, Object> map);
    public List<SendEmail> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
