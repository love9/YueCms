package com.makbro.core.cms.sendEmail.bean;


import com.markbro.base.model.AliasModel;

/**
 * 邮件 bean
 * Created by wujiyue on 2017-09-07 16:30:37 .
 */
public class SendEmail  implements AliasModel {

	private String id;//
	private String emailtype;//邮件类型
	private String userid;//用户ID
	private String toemail;//
	private String fromemail;//
	private String title;//标题
	private Object content;//内容
	private String sendflag;//是否发送
	private String feedback;//是否反馈
	private String createTime;//创建时间
	private String sendTime;//发送时间
	private String sendtype;//发送类型 0立即 1定时
	private String planTime;//定时时间
	private String fromYhid;//站内邮件发送用户
	private String toYhid;//站内邮件接收用户
	private String copyTo;//抄送用户
	private String attachfiles;//附件
	private String emailPwd;//邮件发送者邮箱密码
	private String status;//状态
	private String fl;//分类
	private String bq;//标签

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getEmailtype(){ return emailtype ;}
	public void  setEmailtype(String emailtype){this.emailtype=emailtype; }
	public String getUserid(){ return userid ;}
	public void  setUserid(String userid){this.userid=userid; }
	public String getToemail(){ return toemail ;}
	public void  setToemail(String toemail){this.toemail=toemail; }
	public String getFromemail(){ return fromemail ;}
	public void  setFromemail(String fromemail){this.fromemail=fromemail; }
	public String getTitle(){ return title ;}
	public void  setTitle(String title){this.title=title; }
	public Object getContent(){ return content ;}
	public void  setContent(Object content){this.content=content; }
	public String getSendflag(){ return sendflag ;}
	public void  setSendflag(String sendflag){this.sendflag=sendflag; }
	public String getFeedback(){ return feedback ;}
	public void  setFeedback(String feedback){this.feedback=feedback; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getSendTime(){ return sendTime ;}
	public void  setSendTime(String sendTime){this.sendTime=sendTime; }
	public String getSendtype(){ return sendtype ;}
	public void  setSendtype(String sendtype){this.sendtype=sendtype; }
	public String getPlanTime(){ return planTime ;}
	public void  setPlanTime(String planTime){this.planTime=planTime; }
	public String getFromYhid(){ return fromYhid ;}
	public void  setFromYhid(String fromYhid){this.fromYhid=fromYhid; }
	public String getToYhid(){ return toYhid ;}
	public void  setToYhid(String toYhid){this.toYhid=toYhid; }
	public String getCopyTo(){ return copyTo ;}
	public void  setCopyTo(String copyTo){this.copyTo=copyTo; }
	public String getAttachfiles(){ return attachfiles ;}
	public void  setAttachfiles(String attachfiles){this.attachfiles=attachfiles; }
	public String getEmailPwd(){ return emailPwd ;}
	public void  setEmailPwd(String emailPwd){this.emailPwd=emailPwd; }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFl() {
		return fl;
	}

	public void setFl(String fl) {
		this.fl = fl;
	}

	public String getBq() {
		return bq;
	}

	public void setBq(String bq) {
		this.bq = bq;
	}

	@Override
	public String toString() {
	return "SendEmail{" +
			"id=" + id+
			", emailtype=" + emailtype+
			", userid=" + userid+
			", toemail=" + toemail+
			", fromemail=" + fromemail+
			", title=" + title+
			", content=" + content+
			", sendflag=" + sendflag+
			", feedback=" + feedback+
			", createTime=" + createTime+
			", sendTime=" + sendTime+
			", sendtype=" + sendtype+
			", planTime=" + planTime+
			", fromYhid=" + fromYhid+
			", toYhid=" + toYhid+
			", copyTo=" + copyTo+
			", attachfiles=" + attachfiles+
			", emailPwd=" + emailPwd+
			 '}';
	}
}
