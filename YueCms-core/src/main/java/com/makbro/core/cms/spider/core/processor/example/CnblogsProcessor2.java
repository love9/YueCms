package com.makbro.core.cms.spider.core.processor.example;


import com.makbro.core.cms.spider.core.*;
import com.makbro.core.cms.spider.core.processor.PageProcessor;

/**
 * @author code4crafter@gmail.com <br>
 * @since 0.6.0
 */
public class CnblogsProcessor2 implements PageProcessor, SpiderListener {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(1500);

    @Override
    public void process(Page page) {

        if (page.getUrl().regex(target_reg).match()) {//爬取第一页

            processField(page);
        }else if(page.getUrl().regex(page_reg).match()){//翻页
            processTargetRequest(page);
        }else{
            //第一页
            processTargetRequest(page);
        }
    }
    public void processTargetRequest(Page page){
        page.addTargetRequests(page.getHtml().xpath("//*[@id=\"mainContent\"]/div").links().all());
        //page.addTargetRequest("https://www.cnblogs.com/#p2");
    }
    public void processField(Page page){
       String title= page.getHtml().xpath("//a[@id=\"cb_post_title_url\"]/text()").toString();
        System.out.println(">>>>>>>>>>>>>>>>>"+title);
        page.putField("title",title);
    }
    @Override
    public Site getSite() {
        return site;
    }
    public static final String page_reg = "https://www.cnblogs.com/oxiaojiano/default.html?page=\\d+";
    public static final String target_reg = "https://www.cnblogs.com/oxiaojiano/p/\\d+.html";
    public static void main(String[] args) {
      //  CnblogsProcessor2 processor=new CnblogsProcessor2();
       // String url="https://www.cnblogs.com/oxiaojiano";
       // Spider.create(processor).addUrl(url).setSpiderListener(processor) .addPipeline(new FilePipeline()).thread(3).run();
        System.out.println(ReplaceRuleType.replace.name().equals("replace"));
    }
    public enum ReplaceRuleType{
        replace,
        reg,
        substrbefore,
        substrbeforeExclude,
        substrafter,
        substrafterExclude,
        substrlength
    }
    @Override
    public void onStart(Spider spider) {
        System.out.println("任务开始===》");
    }

    @Override
    public void onSuccess(Request request) {
        System.out.println("解析"+request.getUrl()+"成功!");
    }

    @Override
    public void onError(Request request) {
        System.out.println("解析"+request.getUrl()+"失败!");
    }

    @Override
    public void onFinish(Spider spider) {
        System.out.println("任务结束===》");
    }
}
