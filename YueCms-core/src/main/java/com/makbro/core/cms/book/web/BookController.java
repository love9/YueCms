package com.makbro.core.cms.book.web;

import com.makbro.core.cms.book.bean.Book;
import com.makbro.core.cms.book.service.BookService;
import com.makbro.core.cms.spider.core.processor.service.PageProcessMissonService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 书籍管理
 * Created by wujiyue on 2017-12-17 19:47:46.
 */

@Controller
@RequestMapping("/cms/book")
public class BookController extends BaseController {
    @Autowired
    protected BookService bookService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/book/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Book book, Model model){
                return "/cms/book/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Book book, Model model){
                if(book!=null&&book.getId()!=null){
            book=bookService.get(book.getId());
        }
         model.addAttribute("book",book);
         return "/cms/book/edit";
    }
    //-----------json数据接口--------------------
    
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return bookService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return bookService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return bookService.save(map);
    }

    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            bookService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            bookService.updateByMapBatch(map);
            return Msg.success("批量删除成功！");
        }catch (Exception e){
            return Msg.error("批量删除失败！");
        }
	}

    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable String id) {
    	try{
            bookService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable String[] ids) {
    	try{
            bookService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }

    @Autowired
    PageProcessMissonService pageProcessMissonService;
    @ResponseBody
    @RequestMapping(value = "/json/spiderBook/{id}", method = RequestMethod.POST)
    public Object spiderBook(@PathVariable String id) {
        try{

            Book book=bookService.get(id);
            if(book!=null){
                logger.info(">>>>>>>>>>>>>>开始趴取书籍章节>>>>>>>>>>>>>>");
                book.setStatus("2");
                bookService.update(book);
                pageProcessMissonService.spiderByMissionCode(book.getSpiderMissionCode(),book.getId(),book.getLink(),book.getBookcode());
                return Msg.success("操作成功！");
            }else{
                return Msg.error("操作失败,不存在bookId["+id+"]的记录！");
            }

        }catch (Exception e){
            return Msg.error("操作失败！");
        }
    }

    /**
     * 书籍静态化+书籍章节静态化
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/json/staticBook/{id}", method = RequestMethod.POST)
    public Object staticBook(@PathVariable String id) {
        try{

            Book book=bookService.get(id);
            if(book!=null){
                if("2".equals(book.getStatic_flag())){
                    //正在静态化
                    return Msg.error("当前正在执行静态化!");
                }
                if("0".equals(book.getStatus())){//爬虫任务尚未执行
                    return Msg.error("不能执行静态化!爬虫任务尚未执行!");
                }else if("2".equals(book.getStatus())){//爬虫任务在执行中
                    return Msg.error("不能执行静态化!爬虫任务在执行中!");
                }else if("1".equals(book.getStatus())){//爬虫任务执行完成，才能执行书籍静态化，否则没有数据
                    logger.info(">>>>>>>>>>>>>>开始书籍静态化>>>>>>>>>>>>>>");
                    Msg bookMsg=(Msg) bookService.bookStaticFreemarker(id);
                    if(bookMsg.getType()== Msg.MsgType.success){
                        //书籍静态化成功，在静态化章节
                        Msg chapterMsg=(Msg) bookService.bookChapterStaticFreemarker(id);
                        if(chapterMsg.getType()== Msg.MsgType.success){
                            return Msg.success("静态化成功!");
                        }else{
                            return chapterMsg;
                        }
                    }else{
                        return bookMsg;
                    }

                }else{
                    return Msg.error("book status字段异常!");
                }


            }else{
                return Msg.error("不存在bookId["+id+"]的记录！");
            }

        }catch (Exception e){
            return Msg.error("操作失败！"+e.getMessage());
        }
    }

}