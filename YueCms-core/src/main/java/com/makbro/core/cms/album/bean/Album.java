package com.makbro.core.cms.album.bean;


import com.markbro.base.model.AliasModel;

/**
 * 相册图集管理 bean
 * @author wujiyue
 * @date 2019-7-4 21:56:54
 */
public class Album  implements AliasModel {

	private Integer id;//相册主键
	private String yhid;//文件路径
	private String datascope;//访问类型
	private String name;//相册名称
	private String frontImgId;
	private String imageIds;//图片的id用逗号分割
	private String description;//相册描述
	private String createBy;//
	private String createTime;//
	private Integer available;//
	private Integer deleted;//
	private String albumtype;//

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getDatascope(){ return datascope ;}
	public void  setDatascope(String datascope){this.datascope=datascope; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getImageIds(){ return imageIds ;}
	public void  setImageIds(String imageIds){this.imageIds=imageIds; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	public String getFrontImgId() {
		return frontImgId;
	}

	public void setFrontImgId(String frontImgId) {
		this.frontImgId = frontImgId;
	}

	@Override
	public String toString() {
		return "Album{" +
				"id=" + id +
				", yhid='" + yhid + '\'' +
				", datascope='" + datascope + '\'' +
				", name='" + name + '\'' +
				", frontImgId='" + frontImgId + '\'' +
				", imageIds='" + imageIds + '\'' +
				", description='" + description + '\'' +
				", createBy='" + createBy + '\'' +
				", createTime='" + createTime + '\'' +
				", available=" + available +
				", deleted=" + deleted +
				", albumtype='" + albumtype + '\'' +
				'}';
	}

	public String getAlbumtype() {
		return albumtype;
	}

	public void setAlbumtype(String albumtype) {
		this.albumtype = albumtype;
	}
}
