package com.makbro.core.cms.spider.core;


import com.makbro.core.cms.spider.mission.bean.Spidermission;

/**
 * Created by Administrator on 2018/6/23.
 */
public abstract class SpiderListenerAdapter implements SpiderListener{

    @Override
    public void onStart(Spider spider) {

    }

    @Override
    public void onSuccess(Request request) {

    }

    @Override
    public void onError(Request request) {

    }

    @Override
    public void onFinish(Spider spider) {

    }
    public void onMissionFinish(Spidermission spidermission){

    }
}
