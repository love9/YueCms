package com.makbro.core.cms.project.module.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.project.module.bean.Module;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 项目模块 dao
 * @author wujiyue
 */
@Repository
public interface ModuleMapper{

    public Module get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Module module);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Module> modules);
    public void update(Module module);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Module> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Module> findByMap(PageBounds pageBounds, Map<String, Object> map);

	//public List<Module> findByParentid(PageBounds pageBounds,java.lang.String parentid);
	//public Integer findByParentidCount(@Param("parentid")String parentid);
    public List<Module> findByParentid(Map<String, Object> map);
    public Integer findByParentidCount(Map<String, Object> map);
	public Integer getChildrenCount(@Param("ids") String ids);
	public String getParentidsById(@Param("id") String id);


}
