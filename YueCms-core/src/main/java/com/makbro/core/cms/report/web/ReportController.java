package com.makbro.core.cms.report.web;


import com.makbro.core.cms.report.bean.Report;
import com.makbro.core.cms.report.service.ReportService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.annotation.Referer;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 举报管理
 * @author  wujiyue on 2018-07-27 15:22:31.
 */

@Controller
@RequestMapping("/cms/report")
public class ReportController extends BaseController {
    @Autowired
    protected ReportService reportService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/report/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Report report, Model model){
                return "/cms/report/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Report report, Model model){
                if(report!=null&&report.getId()!=null){
            report=reportService.get(report.getId());
        }
         model.addAttribute("report",report);
         return "/cms/report/edit";
    }
    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return reportService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return reportService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return reportService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除举报")
    public Object delete(@PathVariable Integer id) {
    	try{
            reportService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除举报")
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
             reportService.deleteBatch(ids);
             return Msg.success("删除成功！");
         }catch (Exception e){
             return Msg.error("删除失败！");
         }

    }


    //举报
    @ResponseBody
    @RequestMapping(value="/json/doReport",method = RequestMethod.POST)
    @Referer(methodCode = "ReportController.doReport")
    public Object doReport() {
        Map map=MyBatisRequestUtil.getMap(request);
        return reportService.doReport(map);
    }
}