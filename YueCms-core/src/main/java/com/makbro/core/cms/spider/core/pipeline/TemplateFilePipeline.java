package com.makbro.core.cms.spider.core.pipeline;


import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Task;
import com.makbro.core.cms.spider.mission.service.SpidermissionService;
import com.markbro.base.common.util.FreeMarkerUtil;
import com.markbro.base.common.util.VelocityHelper;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.annotation.ThreadSafe;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**

 */
@ThreadSafe
@Component("templateFilePipeline")
public class TemplateFilePipeline extends BasePipeline {


    public TemplateFilePipeline() {
        setPath("/data/webmagic/");
    }
    /**
     * 构造方法
     * @param templateName  模板名称
     */
    public TemplateFilePipeline(String templateName) {
        setPath("/data/webmagic/");
        this.templateName=templateName;

    }
    /**
     * 构造方法
     * @param templateDirPath
     *                              模板所在文件夹路径
     * @param templateName
     *                              模板名称
     */
    public TemplateFilePipeline(String templateDirPath , String templateName) {
        setPath("/data/webmagic/");
        this.templateDirPath=templateDirPath;
        this.templateName=templateName;
    }


    @Override
    public void process(ResultItems resultItems, Task task) {

        super.process(resultItems,task);

        String path =""; String relative_path="";
        String missionType=(String)resultItems.getAll().get("missionType");//从任务类型可以知道是何种类型比如文章、书籍、书籍章节

        //String path = this.path + PATH_SEPERATOR + task.getUUID() + PATH_SEPERATOR;
        try {
            String htmlName= DigestUtils.md5Hex(resultItems.getRequest().getUrl());
            if(StringUtil.isEmpty(relative_base_path)){
                relative_base_path = GlobalConfig.getConfig("static.relativeBathPath");
            }
            if(StringUtil.notEmpty(relative_base_path)){
                path = this.path + PATH_SEPERATOR;
                if(SpidermissionService.MissionType.book.toString().equals(missionType)){
                    relative_path=this.relative_base_path+ File.separator+"book"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"book"+File.separator+ htmlName+ ".html";//文件绝对路径
                }else if(SpidermissionService.MissionType.article.toString().equals(missionType)){
                    relative_path=this.relative_base_path+File.separator+"article"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"article"+File.separator+ htmlName+ ".html";//文件绝对路径
                }else if(SpidermissionService.MissionType.chapter.toString().equals(missionType)){
                    relative_path=this.relative_base_path+File.separator+"chapter"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"chapter"+File.separator+ htmlName+ ".html";//文件绝对路径
                }else{
                    relative_path=this.relative_base_path+File.separator+"other"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"other"+File.separator+ htmlName+ ".html";//文件绝对路径
                }
            }else{//没配置相对路径，就不考虑它
                path = this.path + PATH_SEPERATOR + task.getUUID() + PATH_SEPERATOR;
                path= path + htmlName+ ".html";//文件绝对路径
            }
            if(super.templateType==TemplateType.Freemarker){
                FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil();
                Map dataMap=new HashMap();
                for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
                    dataMap.put(entry.getKey(), entry.getValue());
                }
                freeMarkerUtil.ExecuteFile(this.templateName,dataMap,path);

            }else if(super.templateType==TemplateType.Velocity) {
                VelocityHelper helper = new VelocityHelper(super.getTemplatePath());
                helper.setTemplateFile(templateName);
                helper.setDirectoryOfOutput(path);
                helper.setFileNameOfOutput(DigestUtils.md5Hex(resultItems.getRequest().getUrl()));
                helper.setFileExtensionOfOutput(".html");
                StringBuffer contentStrSb = new StringBuffer();
                for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
                    if (entry.getValue() instanceof Iterable) {
                        Iterable value = (Iterable) entry.getValue();
                        helper.AddKeyValue(entry.getKey(), value);
                        contentStrSb.append(entry.getKey()).append(":");
                        for (Object o : value) {
                            contentStrSb.append(o).append("\r\n");
                        }
                    } else {
                        helper.AddKeyValue(entry.getKey(), entry.getValue());
                        contentStrSb.append(entry.getKey()).append(":").append(entry.getValue()).append("\r\n");
                    }
                }
                helper.AddKeyValue("content_all", contentStrSb.toString());
                helper.AddKeyValue("url", resultItems.getRequest().getUrl());
                helper.ExecuteFile();
            }else{}

        } catch (Exception e) {
            logger.warn("TemplateFilePipeline write file error", e);
        }
    }
}
