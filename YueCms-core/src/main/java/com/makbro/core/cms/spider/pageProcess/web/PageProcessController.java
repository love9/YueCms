package com.makbro.core.cms.spider.pageProcess.web;


import com.makbro.core.cms.spider.pageProcess.bean.PageProcess;
import com.makbro.core.cms.spider.pageProcess.service.PageProcessService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 页面提取配置管理
 * @author  wujiyue on 2017-11-14 13:44:58.
 */

@Controller
@RequestMapping("/spider/pageProcess")
@RequiresPermissions("spider")
public class PageProcessController extends BaseController {
    @Autowired
    protected PageProcessService pageProcessService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/spider/pageProcess/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(PageProcess pageProcess, Model model){
                return "/cms/spider/pageProcess/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(PageProcess pageProcess,Model model){
                if(pageProcess!=null&&pageProcess.getId()!=null){
            pageProcess=pageProcessService.get(pageProcess.getId());
        }
         model.addAttribute("pageProcess",pageProcess);
         return "/cms/spider/pageProcess/edit";
    }
    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return pageProcessService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return pageProcessService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return pageProcessService.save(map);
    }

    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable Integer id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            pageProcessService.updateByMap(map);
            return Msg.success("删除成功!");
        }catch (Exception e){
            return Msg.error("删除失败!");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable Integer[] ids){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            pageProcessService.updateByMapBatch(map);
            return Msg.success("批量删除成功!");
        }catch (Exception e){
            return Msg.error("批量删除失败!");
        }
	}

    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	try{
            pageProcessService.delete(id);
            return Msg.success("删除成功!");
        }catch (Exception e){
            return Msg.error("删除失败!");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
             pageProcessService.deleteBatch(ids);
            return Msg.success("删除成功!");
         }catch (Exception e){
            return Msg.error("删除失败!");
         }
    }
}