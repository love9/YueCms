package com.makbro.core.cms.template.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.resourcetype.dao.ResourcetypeMapper;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.dao.TemplateMapper;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.file.FileUtil;
import com.markbro.base.utils.string.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 模板管理 Service
 * @author  wujiyue on 2017-11-10 10:07:06.
 */
@Service
public class TemplateService{
    protected  Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private TableKeyService keyService;
    @Autowired
    private TemplateMapper templateMapper;
    @Autowired
    private ResourcetypeMapper resourcetypeMapper;
     /*基础公共方法*/
    public Template get(String id){
        return templateMapper.get(id);
    }

    public List<Template> find(PageBounds pageBounds,Map<String,Object> map){
        //兼容代码生成模板的页面传递resourcetypename的情况
        String resourcetypename=String.valueOf(map.get("resourcetypename"));
        String resourcetypeid=String.valueOf(map.get("resourcetypeid"));
        if(StringUtil.notEmpty(resourcetypename) && StringUtil.isEmpty(resourcetypeid)){
            //如果没有传递resourcetypeid但是传递了resourcetypename 目的是兼容代码生成模板的页面的情况
            String resid= resourcetypeMapper.getIdByName(resourcetypename);
            map.put("resourcetypename","");
            map.put("resourcetypeid",resid);
        }
        return templateMapper.find(pageBounds,map);
    }
    public List<Template> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return templateMapper.findByMap(pageBounds,map);
    }

    public void add(Template template){
        templateMapper.add(template);
    }
    public Object save(Map<String,Object> map){

        Template template= JSON.parseObject(JSON.toJSONString(map),Template.class);
        String basePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("template.basePath");
        String prefix=GlobalConfig.getConfig("template.path.prefix");
        String suffix=GlobalConfig.getConfig("template.path.suffix");;
        if(template.getId()==null||"".equals(template.getId().toString())){//新增操作
            String id= keyService.getStringId();
            template.setId(id);
            String filePath=template.getFilePath();
            if(StringUtil.notEmpty(filePath)){
                //先检查是否存在

                    System.out.println(basePath);
                    filePath=basePath+template.getFilePath();

                    File file=new File(filePath);
                    if(file.exists()){
                        return Msg.error("该文件已经存在!新建失败!");
                    }
                    boolean b= FileUtil.createFile(file); //创建模板文件
                    if(!b){
                        return Msg.error("保存信息成功,文件创建失败!");
                    }
            }
            templateMapper.add(template);
        }else{//更新操作
            String filePath=template.getFilePath();
            String filePathOld=String.valueOf(map.get("filePathOld"));//如果改了名称，旧文件要删除
            if(!filePathOld.endsWith(suffix)){
                filePathOld+=suffix;
            }
            if(!filePathOld.startsWith(prefix)){
                filePathOld=prefix+filePathOld;
            }
            if(StringUtil.notEmpty(filePath)&&StringUtil.notEmpty(filePathOld)){//确保这是从edit页面来的
            Template contentTemplate=get(template.getId());
            //先检查是否存在

            filePath=basePath+template.getFilePath();
            filePathOld=basePath+filePathOld;

            File file=new File(filePath);

                if(filePath.equals(filePathOld)){
                    //没改名字
                    if(!file.exists()){//文件还不存在
                        boolean createOk=FileUtil.createFile(file); //创建模板文件
                        if(!createOk){
                            return Msg.error("保存信息成功,文件创建失败!");
                        }else{
                            boolean writeOk=FileUtil.writeContentToFile(contentTemplate.getContent(),file);
                            if(!writeOk){
                                return Msg.error("新建文件成功,但写入内容失败!");
                            }
                        }
                    }
                }else{
                    //改了名字
                    if(file.exists()){
                        //改名后的文件已经存在
                        return Msg.error("保存失败!当前文件已经存在!");
                    }else{
                        //删除旧的
                        boolean delOk=FileUtil.deleteFile(filePathOld);
                        if(!delOk){
                            log.warn("删除文件失败!=>"+filePathOld);
                        }
                        boolean createOk=FileUtil.createFile(file); //创建新的文件
                        if(!createOk){
                            return Msg.error("保存信息成功,文件创建失败!");
                        }else{
                            boolean writeOk=FileUtil.writeContentToFile(contentTemplate.getContent(),file);
                            if(!writeOk){
                                return Msg.error("新建文件成功,但写入内容失败!");
                            }
                        }
                    }
                }
            }else{//这段逻辑是为了编辑模板内容。所以template对象里只有id和content有值
                //更新文件内容到文件
                Template dbTemplate=get(template.getId());
                if(StringUtil.notEmpty(dbTemplate.getFilePath())&&StringUtil.notEmpty(template.getContent())){
                    filePath=basePath+dbTemplate.getFilePath();
                    File file=new File(filePath);
                    if(file.exists()){
                        FileUtil.writeContentToFile(template.getContent(),file);
                    }
                }
            }
            templateMapper.update(template);
        }
        return Msg.success("保存信息成功");
    }

    public void addBatch(List<Template> templates){
        templateMapper.addBatch(templates);
    }
    public void update(Template template){
        templateMapper.update(template);
    }
    public void updateByMap(Map<String,Object> map){
        templateMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        templateMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        templateMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        String basePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("template.basePath");
        for(String id:ids){
            Template template=get(id);
            if(template!=null&&StringUtil.notEmpty(template.getFilePath())){
                String filePath=basePath+template.getFilePath();
                File file=new File(filePath);
                FileUtil.deleteFile(file);
            }

        }
        templateMapper.deleteBatch(ids);
    }
     /*自定义方法*/
     public Template getByName(String name){
         return templateMapper.getByName(name);
     }
}
