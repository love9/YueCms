package com.makbro.core.cms.spider.core.pipeline;


import com.makbro.core.cms.article.service.StaticService;
import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Task;
import com.makbro.core.cms.spider.core.utils.FilePersistentBase;
import com.makbro.core.cms.spider.mission.service.SpidermissionService;
import com.makbro.core.cms.tag.bean.Tags;
import com.makbro.core.cms.tag.service.TagsService;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.utils.string.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义基础Pipeline,其它继承该类的实现类Pipeline都享有该类处理的内容
 */
public abstract class BasePipeline extends FilePersistentBase implements Pipeline {

    protected Logger logger = LoggerFactory.getLogger(getClass());
    protected String relative_base_path= "";//相对路径根路径，注意和path这个绝对路径根目录对应。对应的意思是，web项目启动后，相对路径能找到绝对路径
    protected String templateName="Spiderhtml.txt";//模板名称  默认是Spiderhtml.txt   TemplateFilePipeline用到
    protected String templateDirPath="";//模板所在文件夹路径 如果不指定，默认为资源文件夹下的/template目录    VTemplateFilePipeline用到
    protected String templateString="";//模板字符串   TemplateMergerStringFilePipeline用到
    public String getTemplateDirPath() {
        return templateDirPath;
    }

    protected TemplateType templateType= TemplateType.Freemarker;//默认使用Freemarker模板引擎生成文件
    protected enum TemplateType{
        Freemarker,
        Velocity
    }
    /**
     * 针对字段field，启用下载(封面)图片到本地
     * @param resultItems
     * @param field
     * @param appendPath 追加一层目录/book或/article
     */
    private void downloadImage(ResultItems resultItems, String field, String appendPath){

        String field_value=(String)resultItems.getAll().get(field);
        if(StringUtil.notEmpty(field_value)&& StaticService.checkNetUrlExists(field_value)){
            //String local_path = StaticService.downloadImageToLocal(field_value,null,appendPath);//封面图片不在下载到本地，因为需要刷新项目才能访问到
            String local_path = StaticService.downloadImageToQinNiuOss(field_value,StringUtil.notEmpty(appendPath)?appendPath:"");
            if(StringUtil.notEmpty(local_path)&&!"error".equals(local_path)){
                resultItems.getAll().put(field,local_path);

            }
        }
    }

    @Autowired
    TagsService tagsService;

    private static final String ProcessCoverImage="processCoverImage";
    private static final String ProcessTag="processTag";
    /**
     * 专门处理封面图片下载
     * @param resultItems
     */
    public void processCoverImage(ResultItems resultItems){

        String missionType=(String)resultItems.getAll().get("missionType");//从任务类型可以知道是何种类型比如文章、书籍、书籍章节
        if(SpidermissionService.MissionType.article.toString().equals(missionType)){
            this.downloadImage(resultItems, "cover_image", "/article");//默认静态化封面图片
        }else if(SpidermissionService.MissionType.book.toString().equals(missionType)){
            this.downloadImage(resultItems, "sheet_url", "/book");//书籍封面是这个字段sheet_url
        }else{
        }

    }
    /**
     * 专门处理文章标签 认为tag为逗号分隔的汉字，对于每个标签都去cms_tags表查看
     * @param resultItems
     */
    public void processTag(ResultItems resultItems){
        String ids="";
        String tag_value=(String)resultItems.getAll().get("tag");
        if(StringUtil.notEmpty(tag_value)){

            String yhid = (String)resultItems.getAll().get(TmConstant.YHID_KEY);
            String[] tagArr=null;
            List<Tags> tagsList=new ArrayList<Tags>();
            if(tag_value.contains(",")){
                tagArr=tag_value.split(",");
            }else{
                tagArr=new String[1];
                tagArr[0]=tag_value;
            }
            if(tagArr!=null){
                Tags tmp=null;
                for(String name:tagArr){
                    //检测每个标签再数据库cms_tag表中是否存在（根据名称），如果存在记下id，不存在则新增并记下id
                    tmp=tagsService.getByName(Tags.TagType.system,name);
                    if(tmp!=null){
                        ids+=tmp.getId()+",";
                    }else{
                        tmp=new Tags();
                        tmp.setName(name);
                        tmp.setSort(1);
                        tmp.setYhid(yhid);
                        tmp.setAvailable(1);
                        tagsService.add(tmp);
                        ids+=tmp.getId()+",";
                    }
                    tagsList.add(tmp);
                }
                if(ids.endsWith(",")){
                    ids=ids.substring(0,ids.length()-1);
                }
                resultItems.getAll().put("tags",ids);//cms_article 标签字段tags存放的是标签id。这个值用于存入到cms_article表
                resultItems.getAll().put("tagsList",tagsList);//这个值用于输出模板文件的标签
                resultItems.getAll().put("tag","");//清空tag内容，防止多个Pipeline重复处理
            }
        }
    }
    /**
     * 获得模版文件根路径（classpath路径）
     * @return
     */
    public  String getTemplatePath(){

        if("".equals(this.templateDirPath)){
            String temp="";
            temp= this.getClass().getResource("/").getPath();
           /* if("".equals(rela)){
                temp= this.getClass().getResource("/").getPath();
            }else{
                temp= this.getClass().getResource(rela).getPath();
            }*/
            if(temp.startsWith("/")){
                temp=temp.substring(temp.indexOf("/")+1);
            }
            return temp;
        }else {
            return  this.templateDirPath;
        }
    }

    public void setTemplateDirPath(String templateDirPath) {
        this.templateDirPath = templateDirPath;
    }
    public String getTemplateName() {
        return templateName;
    }

    public void setRelative_base_path(String relative_base_path) {
        if (!relative_base_path.endsWith(PATH_SEPERATOR)) {
            relative_base_path += PATH_SEPERATOR;
        }
        this.relative_base_path = relative_base_path;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateString() {
        return templateString;
    }

    public void setTemplateString(String templateString) {
        this.templateString = templateString;
    }

    @Override
    public void process(ResultItems resultItems, Task task) {
        this.processCoverImage(resultItems);
        this.processTag(resultItems);
    }
}
