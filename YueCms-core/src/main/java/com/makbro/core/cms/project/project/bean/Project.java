package com.makbro.core.cms.project.project.bean;


import com.markbro.base.model.AliasModel;

/**
 * 项目 bean
 * @author wujiyue
 * @date 2019-6-29 14:33:47
 */
public class Project  implements AliasModel {

	private Integer id;//项目编号
	private String name;//项目名称
	private String code;//项目代码。团队内部简称
	private String teamname;//团队名称
	private String starttime;//
	private String endtime;//
	private Object description;//项目描述
	private String accesscontrol;//访问控制类型
	private String status;//项目状态


	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getCode(){ return code ;}
	public void  setCode(String code){this.code=code; }
	public String getTeamname(){ return teamname ;}
	public void  setTeamname(String teamname){this.teamname=teamname; }
	public String getStarttime(){ return starttime ;}
	public void  setStarttime(String starttime){this.starttime=starttime; }
	public String getEndtime(){ return endtime ;}
	public void  setEndtime(String endtime){this.endtime=endtime; }
	public Object getDescription(){ return description ;}
	public void  setDescription(Object description){this.description=description; }
	public String getAccesscontrol(){ return accesscontrol ;}
	public void  setAccesscontrol(String accesscontrol){this.accesscontrol=accesscontrol; }
	public String getStatus(){ return status ;}
	public void  setStatus(String status){this.status=status; }


	@Override
	public String toString() {
	return "Project{" +
			"id=" + id+
			", name=" + name+
			", code=" + code+
			", teamname=" + teamname+
			", starttime=" + starttime+
			", endtime=" + endtime+
			", description=" + description+
			", accesscontrol=" + accesscontrol+
			", status=" + status+
			 '}';
	}
}
