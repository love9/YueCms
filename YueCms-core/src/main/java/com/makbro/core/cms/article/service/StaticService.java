package com.makbro.core.cms.article.service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.system.oss.util.QiniuUtil;
import com.makbro.core.cms.article.bean.Article;
import com.makbro.core.cms.article.bean.ArticleRegionType;
import com.makbro.core.cms.tag.bean.Tags;
import com.makbro.core.cms.tag.service.TagsService;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.service.TemplateService;
import com.markbro.base.common.util.FreeMarkerUtil;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.common.util.cache.Cache;
import com.markbro.base.common.util.cache.CacheUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.RequestContextHolderUtil;
import com.markbro.base.utils.string.StringUtil;
import com.qiniu.storage.model.FetchRet;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 本地静态化服务
 * 可以生成本地静态页面，下载网络图片到本地
 * @author wujiyue
 * @Date 2018年8月27日
 */
@Service
public class StaticService {
    static Logger logger = LoggerFactory.getLogger(StaticService.class);
    static Cache<String, String> downloadCache = CacheUtil.newLRUCache(1000);
    @Autowired
    ArticleService articleService;
    /**
     * 检测一个网络路径文件是否存在
     * @param url
     * @return
     */
    public static boolean checkNetUrlExists(String url){
        try{
            URL u = new URL(url);
            HttpURLConnection urlcon = (HttpURLConnection) u.openConnection();
            if(urlcon.getResponseCode()==200){
                return true;
            }
        }catch (Exception ex){
        }
        return false;
    }

    /**
     * 本地化网络路径图片
     * @param urlPath 网络图片
     * @param saveBasePath 保存到本地的跟路径,一般为空，它默认为项目配置的目录static.imgPath,返回相对路径。如果指定绝对路径，那么返回绝对路径
     * @param appendPath 追加一层目录如/book或/article
     * @return 返回保存到本地的相对路径（如果指定保存绝对路径saveBasePath，则返回绝对地址）
     */
    public static String downloadImageToLocal(String urlPath,String saveBasePath,String appendPath){
        try{
            if(!checkNetUrlExists(urlPath)){
                return "";
            }
            String static_relative_path="";//相对路径是，输出路径相对于webapp的路径
            if(StringUtil.isEmpty(saveBasePath)){
                saveBasePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.webAppPath")+GlobalConfig.getConfig("static.imgPath")+(StringUtil.notEmpty(appendPath)?appendPath:"")+File.separator;//项目根目录+webapp路径+静态文件输出路径相对webapp目录
                static_relative_path= GlobalConfig.getConfig("static.imgPath")+(StringUtil.notEmpty(appendPath)?appendPath:"")+File.separator;
            }
            CloseableHttpClient httpclient = HttpClients.createDefault();
            //这里通过httpclient下载之前抓取到的图片网址，并放在对应的文件中
            //String urlPath="http://v3cdn.duoguyu.com/oss/uploads/201803/08/180308100445561.jpg";
            HttpGet httpget = new HttpGet(urlPath);
            String extName=com.google.common.io.Files.getFileExtension(urlPath);
            String name=com.google.common.io.Files.getNameWithoutExtension(urlPath);
            File dir=new File(saveBasePath);
            if(!dir.exists()){
                dir.mkdirs();
            }
            String savePath =saveBasePath+name+"."+extName;
            if(StringUtil.notEmpty(static_relative_path)){
                static_relative_path+=name+"."+extName;
                static_relative_path=static_relative_path.replaceAll("\\\\","\\/");//把相对路径的反斜杠替换为正斜杠
            }
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            InputStream in = entity.getContent();
            File file = new File(savePath);
            try {
                FileOutputStream fout = new FileOutputStream(file);
                int l = -1;
                byte[] tmp = new byte[1024];
                while ((l = in.read(tmp)) != -1) {
                    fout.write(tmp,0,l);
                }
                fout.flush();
                fout.close();
            } finally {
                in.close();
            }
            if(StringUtil.notEmpty(static_relative_path)){
                return static_relative_path;
            }else{
                return savePath;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return "error";
        }
    }


    /**
     * 下载网络图片到七牛oss
     * @param urlPath 网络图片的路径
     * @param folder  自定义额外的路径
     * @return 返回上传七牛后图片外链地址
     */
    public static String downloadImageToQinNiuOss(String urlPath,String folder){
        try{
            String cachePath= (String) downloadCache.get(urlPath);
            if(StringUtil.notEmpty(cachePath)){//先从缓存取，如果取到了，说明最近已经处理过了。
                return cachePath;
            }
            if(!folder.startsWith("/")){
                folder="/"+folder;
            }

            if(StringUtil.notEmpty(urlPath)&&urlPath.startsWith("//")){
                String tempPath= "http:"+urlPath;//有些图片链接以“//”，加上http:
                if(!checkNetUrlExists(tempPath)){
                    tempPath="https:"+urlPath;
                }
                urlPath=tempPath;
            }
            if(!checkNetUrlExists(urlPath)){
                return "";
            }
            String extName=com.google.common.io.Files.getFileExtension(urlPath);
            String name=com.google.common.io.Files.getNameWithoutExtension(urlPath);
            String  qiniuKey="";
            String  qiniuKeyOld="";
            if(!folder.equals("/article")&&!folder.equals("/book")){
                //为了规范不同来源的图片，统一在文件名之前都多加一层路径。
                //这里先判断多加一层路径的文件是否存在，存在就删除。//这段逻辑以后要删除
                //文章正文内容的图片
                qiniuKey= GlobalConfig.getConfig("static.imgPath")+(StringUtil.notEmpty(folder)?folder:"")+File.separator;
                qiniuKeyOld= GlobalConfig.getConfig("static.imgPath")+File.separator;
                qiniuKey+=name+"."+extName;
                qiniuKey=qiniuKey.replaceAll("\\\\","\\/");//把相对路径的反斜杠替换为正斜杠
                qiniuKeyOld+=name+"."+extName;
                qiniuKeyOld=qiniuKeyOld.replaceAll("\\\\","\\/");
                boolean b= QiniuUtil.deleteFile(qiniuKeyOld);
                if(b){
                    logger.info("delete qiniu file ["+qiniuKeyOld+"] success！");
                }
            }else{
                //文章和书籍封面图片
                qiniuKey= GlobalConfig.getConfig("static.imgPath")+(StringUtil.notEmpty(folder)?folder:"")+File.separator;
                qiniuKey+=name+"."+extName;
                qiniuKey=qiniuKey.replaceAll("\\\\","\\/");//把相对路径的反斜杠替换为正斜杠
            }




            FetchRet res=QiniuUtil.uploadByUrl(urlPath, qiniuKey);
            if(StringUtil.notEmpty(res.key)){
                downloadCache.put(urlPath,QiniuUtil.DefaultDomain+qiniuKey);//放入缓存
                return QiniuUtil.DefaultDomain+qiniuKey;//域名domain+key就是该图片在七牛的外链地址
            }
            return "";

        }catch (Exception ex){
            ex.printStackTrace();
            return "error";
        }
    }

    @Autowired
    TemplateService templateService;
    @Autowired
    TagsService tagsService;
    /**
     * 分页静态化文章
     * @param articleQueryMap
     * @param templateName
     */
    public Object staticArticlesByPage(Map articleQueryMap,String templateName,int pageSize){

        logger.info(">>>静态化文章列表开始：");
        Integer totalCount = articleService.getTotalCountByMap(articleQueryMap);
        if(totalCount!=null&&totalCount>0){
          if(StringUtil.isEmpty(templateName)){
              templateName = GlobalConfig.getConfig("static.page.template");
          }
          if(StringUtil.isEmpty(templateName)){
              return Msg.error("没有指定静态化模板!");
          }
          Template template=templateService.getByName(templateName);
          if(template==null){
              return Msg.error("没有取到名称为["+templateName+"]的模板!");
          }
          int totalPages=(totalCount+pageSize-1)/pageSize;
          logger.info("总记录数totalCount:{},每页记录数pageSize:{},总页数totalPages:{}",totalCount,pageSize,totalPages);
          for(int i=1;i<=totalPages;i++){
              List<Article> pageList=articleService.findByMap(new PageBounds(i,pageSize),articleQueryMap);
              if(CollectionUtils.isNotEmpty(pageList)){
                  logger.info(">>>静态化第"+i+"页数据>>>");

              }
          }
        }
        return Msg.success("静态化成功!");
    }
    /**
     * 标签静态化文章列表 staticTags
     * 按照标签id静态化文章（分页）
     * 比如第一页：/tag/id/23.html，第二页/tag/id/23/page/2.html
     * @param articleQueryMap
     * @param tag_id
     */
    public Object staticArticlesByTag(Map articleQueryMap,String tag_id,int pageSize){

        Tags tag= tagsService.get(Integer.valueOf(tag_id));
        if(tag==null){
            return Msg.error("没有tag_id["+tag_id+"]的标签!");
        }
        articleQueryMap.put("tag",tag_id);
        Integer totalCount = articleService.getTotalCountByMap(articleQueryMap);
        if(totalCount!=null&&totalCount>0){
            String templateName=GlobalConfig.getConfig("static.tag.template");
            if(StringUtil.isEmpty(templateName)){
                return Msg.error("没有指定静态化模板!");
            }
            Template template=templateService.getByName(templateName);
            if(template==null){
                return Msg.error("没有取到名称为["+templateName+"]的模板!");
            }
            FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(FreeMarkerUtil.TemplateLoaderType.StringTemplateLoader,new HashMap<String,String>(){{
                put("name",template.getContent());
            }});
            int pageGroupCount=5;//分页条显示出几页
            int totalPages=(totalCount+pageSize-1)/pageSize;
            logger.info("标签id:{},name:{}的文章总记录数totalCount:{},每页记录数pageSize:{},总页数totalPages:{}",tag.getId(),tag.getName(),totalCount,pageSize,totalPages);
            Map dataMap=new HashMap<String,Object>();
            dataMap.put("tag",tag);
            dataMap.put("totalPages",totalPages);
            dataMap.put("totalCount",totalCount);
            dataMap.put("pageSize",pageSize);
            dataMap.put("searchText",tag.getName());
            List<Article> hots = articleService.findByArticleRegionType(ArticleRegionType.REGION_HOT);
            dataMap.put("hots",hots);
            List<Tags> fullTabs=tagsService.findFullTabs();
            dataMap.put("fullTabs",fullTabs);
            //分页条首页尾页的链接
            //String serverContextPath=getServerContextPath();
            //页面绝对路径basePath
            String absoluteBasePath="";
            if(GlobalConfig.isDevMode()){//开发模式
                absoluteBasePath= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.webAppPath");
            }else{
                absoluteBasePath= ProjectUtil.getProjectPath();
            }
            String relativeBasePath=GlobalConfig.getConfig("static.relativeBathPath");
            relativeBasePath+="/search/tag/id";
            String firstPageUrl="",endPageUrl="";
            firstPageUrl=relativeBasePath+"/"+tag_id+"/1.html";
            endPageUrl=relativeBasePath+"/"+tag_id+"/"+totalPages+".html";
            dataMap.put("firstPage",firstPageUrl);
            tag.setUrl(firstPageUrl);
            tagsService.update(tag);
            dataMap.put("endPage",endPageUrl);
            int preNo=1;int nextNo=totalPages;
            for(int i=1;i<=totalPages;i++){
                List<Article> pageList=articleService.findByMap(new PageBounds(i,pageSize),articleQueryMap);
                if(CollectionUtils.isNotEmpty(pageList)){
                    logger.info(">>>标签静态化文章，第"+i+"页数据>>>");
                    dataMap.put("articles",pageList);
                    dataMap.put("pageNo",i);
                    //准备分页条数据
                    if(i>1){
                        preNo=i-1;
                    }else{
                        preNo=1;
                    }
                    if(i<totalPages){
                        nextNo=i+1;
                    }else{
                        nextNo=totalPages;
                    }
                    String prePageUrl=relativeBasePath+"/"+tag_id+"/"+preNo+".html";
                    String nextPageUrl=relativeBasePath+"/"+tag_id+"/"+nextNo+".html";
                    dataMap.put("prePage",prePageUrl);
                    dataMap.put("nextPage",nextPageUrl);
                    Integer[] slider=getSlider(i,totalPages,4);
                    if(slider!=null&&slider.length>0){
                        List<Map<String,Object>> pageGroup=new ArrayList<>();
                        Map<String,Object> temp=null;
                        for(Integer t:slider){
                            temp=new HashMap<>();
                            temp.put("num",t);
                            String url=relativeBasePath+"/"+tag_id+"/"+t+".html";
                            temp.put("url",url);

                            pageGroup.add(temp);
                        }
                        dataMap.put("pageGroup",pageGroup);
                    }
                    String filePath=absoluteBasePath+relativeBasePath+"/"+tag_id+"/"+i+".html";
                    boolean b=freeMarkerUtil.ExecuteFile("name",dataMap,filePath);
                    if(b){

                        logger.info(">>>标签id:{},name:{},静态化文章，第"+i+"页数据success!>>>filePath>>"+filePath,tag.getId(),tag.getName(),totalCount);
                    }else{
                        logger.error(">>>标签id:{},name:{},静态化文章，第"+i+"页数据fail!>>>filePath>>"+filePath,tag.getId(),tag.getName(),totalCount);
                    }
                }
            }
        }else{
            logger.warn("标签id:{},name:{}的文章记录数为:{}",tag.getId(),tag.getName(),totalCount);
        }
        return Msg.success("标签id:["+tag.getId()+"],name:["+tag.getName()+"]的文章静态化成功!");
    }

    /**
     * 获得显示的分页条
     * @param pageNo 当前页
     * @param totalPages 总页数
     * @param slidersCount 漏出几页
     * @return
     */
    public static Integer[] getSlider(int pageNo,int totalPages,int slidersCount) {
        int avg = slidersCount / 2;
        int startPageNumber = pageNo - avg;
        if (startPageNumber <= 0) {
            startPageNumber = 1;
        }
        int endPageNumber = startPageNumber + slidersCount - 1;
        if (endPageNumber > totalPages) {
            endPageNumber = totalPages;
        }
        if (endPageNumber - startPageNumber < slidersCount) {
            startPageNumber = endPageNumber - slidersCount;
            if (startPageNumber <= 0) {
                startPageNumber = 1;
            }
        }
        List<Integer> result = new ArrayList<Integer>();
        for (int i = startPageNumber; i <= endPageNumber; i++) {
            result.add(new Integer(i));
        }
        return result.toArray(new Integer[result.size()]);
    }
    public String getServerContextPath(){
        HttpServletRequest request= RequestContextHolderUtil.getRequest();
        String scheme = request.getScheme();             // http
        String serverName = request.getServerName();     // hostname.com
        int serverPort = request.getServerPort();        // 80
        String contextPath = request.getContextPath();

        StringBuffer url =  new StringBuffer();
        url.append(scheme).append("://").append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url.append(":").append(serverPort);
        }
        url.append("/").append(contextPath);
        return url.toString();
    }
    public static void main(String[] args){
        int pageNo=12;
        int totalPages=25;
        Integer[] slider=getSlider(pageNo,totalPages,4);
        for(int i=0;i<slider.length;i++){
            System.out.println(slider[i]);
        }
    }
}
