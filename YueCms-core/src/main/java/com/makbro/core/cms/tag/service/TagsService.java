package com.makbro.core.cms.tag.service;


import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.article.service.StaticService;
import com.makbro.core.cms.tag.bean.Tags;
import com.makbro.core.cms.tag.dao.TagsMapper;
import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 标签 Service
 * @author  wujiyue on 2018-07-06 15:42:13.
 */
@Service
public class TagsService implements InitCacheListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private TagsMapper tagsMapper;

     /*基础公共方法*/
    public Tags get(Integer id){
        Tags tags =(Tags)  EhCacheUtils.getSysInfo("tags", id.toString());
        if(tags!=null){
            return tags;
        }
        return tagsMapper.get(id);
    }
    public List<Tags> find(PageBounds pageBounds, Map<String,Object> map){
        return tagsMapper.find(pageBounds,map);
    }

    /**
     * 查询个人分类标签
     * @param pageBounds
     * @param map
     * @return
     */
    public List<Tags> findPersonalTags(PageBounds pageBounds, Map<String,Object> map){
        map.put("tag_type",Tags.TagType.personal.name());
        return tagsMapper.find(pageBounds,map);
    }
    public List<Map<String,Object>> findByMap(PageBounds pageBounds, Map<String,Object> map){
        return tagsMapper.findByMap(pageBounds,map);
    }
    public void add(Tags tags){
        tagsMapper.add(tags);
    }
    public Object save(Map<String,Object> map){
          Tags tags= JSON.parseObject(JSON.toJSONString(map),Tags.class);
          if(tags.getId()==null||"".equals(tags.getId().toString())){
              Integer id= keyService.getIntegerId();
              tags.setId(id);
              tagsMapper.add(tags);
          }else{
              tagsMapper.update(tags);
          }
          return Msg.success("保存信息成功");
    }


    public void update(Tags tags){
        tagsMapper.update(tags);
    }

    public void updateByMap(Map<String,Object> map){
        tagsMapper.updateByMap(map);
    }

    public void delete(Integer id){
        tagsMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        tagsMapper.deleteBatch(ids);
    }


    public Tags getByName(Tags.TagType tagType,String name){
        return tagsMapper.getByName(tagType.name(),name);
    }
    public Object saveSort(Map map){
    	try{
    		String sort = String.valueOf(map.get("sort"));
    		if(!"".equals(sort)){
    			String[] sx = sort.split(",");
    			for(int i=0;i<sx.length;i++) {
    				String[] arr = sx[i].split("_");
    				Integer id= Integer.valueOf(arr[1]);
    				tagsMapper.updateSort(id, arr[2]);
    		    }
    	    }
            return Msg.success("排序成功！");
    	}catch (Exception e){
    		return Msg.error("排序失败！");
    	}
    }

     /*自定义方法*/
     public Object findTags(Map<String,Object> map){
         try{
             List<Tags> list=tagsMapper.find(new PageBounds(),map);
             Msg msg=new Msg();
             msg.setExtend("data",list);
             msg.setType(Msg.MsgType.success);
             msg.setContent("查询成功!");
             return msg;
         }catch (Exception ex){
             ex.printStackTrace();
             return Msg.error("查询标签出现异常！");
         }
     }

    public static final String KEY_ARTICLE_TAGS="cms_tags";
    public static final String KEY_ARTICLE_TAGS_PAGES="cms_tags_pages";
    @Override
    public void onInit() {
        int pageNo=1;
        for(int i=0;i<200;i++){//可缓存1000*200=200000条数据

            List<Tags> list=tagsMapper.find(new PageBounds(pageNo,1000),null);
            if(CollectionUtils.isNotEmpty(list)){
                EhCacheUtils.putSysInfo(KEY_ARTICLE_TAGS,pageNo+"",list);//按页缓存
                pageNo++;
                for(Tags t:list){
                    EhCacheUtils.putSysInfo("tags",t.getId().toString(),t);//单条缓存
                }
            }else{
                pageNo--;
                EhCacheUtils.putSysInfo(KEY_ARTICLE_TAGS_PAGES,pageNo);
                break;
            }
        }
    }

    /**
     * 查询博客首页顶部的fullTabs标签
     * @return
     */
    public List<Tags> findFullTabs(){

        List<Tags> list=(List<Tags>) EhCacheUtils.getSysInfo("ArticleFullTabs");
        if(CollectionUtils.isEmpty(list)){
            Map map=new HashMap();
            map.put("tag_type",Tags.TagType.system.name());
            map.put("tag_module","fullTabs");
            list= tagsMapper.find(new PageBounds(),map);
            EhCacheUtils.putSysInfo("ArticleFullTabs", list);
        }
        return list;
    }
    @Autowired
    StaticService staticService;
    public Object staticTags(String... ids){
        Map map=new HashMap();
        for(String id:ids){
            staticService.staticArticlesByTag(map,id,6);
        }
        return Msg.success("一键静态化成功!");
    }
}
