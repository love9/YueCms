package com.makbro.core.cms.article.web;


import com.alibaba.fastjson.JSONObject;
import com.makbro.core.cms.article.bean.Article;
import com.makbro.core.cms.article.service.ArticleService;
import com.makbro.core.cms.resourcetype.service.ResourcetypeService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.thirdapi.baidu.baiduSite.BaiduPushTypeEnum;
import com.markbro.thirdapi.baidu.baiduSite.BaiduPushUtil;
import com.markbro.thirdapi.baidu.baiduSite.UrlBuildUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 文章管理
 * Created by admin on 2016-08-14 20:02:46.
 */
@Controller
@RequestMapping("/cms/article")
public class ArticleController extends BaseController {
    @Autowired
    protected ArticleService articleService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/cms/article/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        pushLoginUserInfo(model);
        return "/cms/article/add";
    }

    @Autowired
    ResourcetypeService resourcetypeService;
    /**
     * 跳转到编辑页面
     */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String id=(String)map.get(TmConstant.ID_KEY);
        Map<String,Object> article=articleService.getMap(id);
        model.addAttribute("article",article);

        String tags=String.valueOf(article.get("tags"));
        if(StringUtil.notEmpty(tags)){
            String names=articleService.getTags_name(tags);
            article.put("tags_names",names);
        }

        return "/cms/article/edit";
    }



    //-----------json数据接口--------------------
    
    /**
	*找到已删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/findDeleted")
	public Object findDeleted() {
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		return articleService.findByMap(getPageBounds(),map);
	}
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return articleService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return articleService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增文章")
    public void add(Article m) {
        articleService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Article m) {
        articleService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存文章")
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return  articleService.save(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            articleService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}

     @ResponseBody
     @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
     public Object delete(@PathVariable String id) {
        try{
            articleService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
     }
     @ResponseBody
     @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
     public Object deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        try{
            articleService.deleteBatch(ids);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
     }
    @ResponseBody
    @RequestMapping(value = "/json/batchPublish/{ids}", method = RequestMethod.POST)
    public Object batchPublish(@PathVariable String[] ids) {
        try{
            articleService.batchPublish(ids);
            return Msg.success("批量发布成功！");
        }catch (Exception e){
            return Msg.error("批量发布失败！");
        }
    }
    @ResponseBody
    @PostMapping(value = "/json/pushToBaidu/{type}")
    @ActionLog(description = "推送文章到百度站长平台")
    public Object pushToBaidu(@PathVariable("type") BaiduPushTypeEnum type, String[] ids) {
        if (null == ids) {
            return Msg.error("请至少选择一条记录");
        }

        String siteUrl = GlobalConfig.getSysPara("siteUrl");
        String baiduPushToken= GlobalConfig.getSysPara("baiduPushToken");
        StringBuilder params = new StringBuilder();
        for (String id : ids) {
            Article article=articleService.get(id);
            if(StringUtil.notEmpty(article.getStatic_url())){
                params.append(siteUrl).append(article.getStatic_url()).append("\n");
            }else{//TODO
                params.append(siteUrl).append("/myblog").append("/article/detail/").append(id).append("\n");
            }

        }
        // urls: 推送, update: 更新, del: 删除
        String url = UrlBuildUtil.getBaiduPushUrl(type.toString(), siteUrl, baiduPushToken);
        /**
         * success	       	int	    成功推送的url条数
         * remain	       	int	    当天剩余的可推送url条数
         * not_same_site	array	由于不是本站url而未处理的url列表
         * not_valid	   	array	不合法的url列表
         */
        // {"remain":4999997,"success":1,"not_same_site":[],"not_valid":[]}
        /**
         * error	是	int	      错误码，与状态码相同
         * message	是	string	  错误描述
         */
        //{error":401,"message":"token is not valid"}
        String result = BaiduPushUtil.doPush(url, params.toString());

        JSONObject resultJson = JSONObject.parseObject(result);

        if (resultJson.containsKey("error")) {
            return Msg.error(resultJson.getString("message"));
        }
        return Msg.success(resultJson.getString("success")+"\r\n"+resultJson.getString("remain"));
    }
    @ResponseBody
    @RequestMapping(value = "/json/staticArticle/{id}")
    public Object staticArticle(@PathVariable String id) {
          return articleService.staticArticle(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/staticArticles/{ids}")
    public Object staticArticles(@PathVariable String[] ids) {
        return articleService.staticArticles(ids);
    }

}
