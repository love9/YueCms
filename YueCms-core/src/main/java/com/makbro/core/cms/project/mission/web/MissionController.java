package com.makbro.core.cms.project.mission.web;

import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.cms.project.mission.bean.Mission;
import com.makbro.core.cms.project.mission.service.MissionService;
import com.makbro.core.cms.project.project.bean.Project;
import com.makbro.core.cms.project.project.service.ProjectService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.LoginBean;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 项目任务管理
 * @author wujiyue
 */

@Controller
@RequestMapping("/cms/project/mission")
public class MissionController extends BaseController {
    @Autowired
    protected MissionService missionService;
    @Autowired
    protected OrgTreeMapper ogTreeMapper;
    @Autowired
    protected ProjectService projectService;
    @RequestMapping(value={"","/"})
    public String index(Model model){
        LoginBean lb= MyBatisRequestUtil.getLoginUserInfo(request);
        pushLoginUserInfo(model);//通过上面2行代码，在前台页面可以通过${yhid} 和 ${yhmc} 获取登陆用户id和登陆用户名称
        Map map=MyBatisRequestUtil.getMap(request);
        Project p=projectService.get(Integer.valueOf((String.valueOf(map.get("projectId")))));
        if(p!=null){
            model.addAttribute("projectId",p.getId());
            model.addAttribute("projectName",p.getName());
            //行内按钮：编辑、指派、抢任务、提交任务按钮的用户权限
            if(MyBatisRequestUtil.hasPermission("/project/mission/edit")){
                model.addAttribute("projectMissionEdit","true");
            }
            if(MyBatisRequestUtil.hasPermission("project.mission.assignTo")){
                model.addAttribute("projectMissionAssignTo","true");
            }
            if(MyBatisRequestUtil.hasPermission("project.mission.askForMission")){
                model.addAttribute("projectMissionAskForMission","true");
            }
            if(MyBatisRequestUtil.hasPermission("project.mission.submitMission")){//提交任务
                model.addAttribute("projectMissionSubmitMission","true");
            }
        }else{
            //提示错误,前台暂时没实现这个提示
            model.addAttribute("type","error");
            model.addAttribute("msg","操作非法！");
        }
        return "/cms/project/mission/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Mission mission, Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        //model.addAttribute("projectId",String.valueOf(map.get("projectId")));
        Project p=projectService.get(Integer.valueOf((String.valueOf(map.get("projectId")))));

        if(p!=null){
            model.addAttribute("projectId",p.getId());
            model.addAttribute("projectName",p.getName());
        }else{
            //提示错误,前台目前没实现
            model.addAttribute("type","error");
            model.addAttribute("msg","操作非法！");
        }
        return "/cms/project/mission/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Mission mission, Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        if(mission!=null&&mission.getId()!=null){
            mission=missionService.get(mission.getId());
            String assignTo=mission.getAssignTo();
            assignTo= StringUtil.subEndStr(assignTo,";");
            assignTo=assignTo.replaceAll(";","','");
            List<Map<String,Object>> ls=ogTreeMapper.getYhByYhids(assignTo);
            String assignToNames="";
            if(ls!=null&&ls.size()>0){
                for(Map<String,Object> t:ls){
                    assignToNames+=String.valueOf(t.get("account"))+"("+String.valueOf(t.get("realname"))+");";
                }
            }
            model.addAttribute("assignToNames",assignToNames);
        }
         model.addAttribute("projectId",String.valueOf(map.get("projectId")));
         model.addAttribute("mission",mission);

        Project p=projectService.get(mission.getProjectId());
        if(p!=null){
            model.addAttribute("projectName",p.getName());
        }
         return "/cms/project/mission/edit";
    }
    //-----------json数据接口--------------------
    
    
    
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return missionService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return missionService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }


    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(Mission m) {

        missionService.add(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Mission m) {
        missionService.update(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return missionService.save(map);
    }





    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	Msg msg=new Msg();
    	try{

            missionService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
        		msg.setType(Msg.MsgType.error);
        		msg.setContent("删除失败！");
        }
        return msg;
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
    	try{

             missionService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }

    /**
     * 抢任务
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/json/askForMission/{id}", method = RequestMethod.POST)
    public Object askForMission(@PathVariable Integer id) {
        Msg msg=new Msg();
        try{
            Map map=MyBatisRequestUtil.getMap(request);
            int i=missionService.askForMission(map,id);
            if(i==1){
                msg.setType(Msg.MsgType.success);
                msg.setContent("抢任务成功");
            }else if(i==0){
                msg.setType(Msg.MsgType.block);
                msg.setContent("您来晚了一步，任务已经被抢！");
            }else{
                msg.setType(Msg.MsgType.error);
                msg.setContent("参数异常！");
            }

        }catch (Exception ex){
            logger.error("askForMission出现异常："+ex.toString());
            msg.setType(Msg.MsgType.error);
            msg.setContent("抢任务出现异常！");
        }
        return msg;

    }


}