package com.makbro.core.cms.spider.core.processor;

import com.alibaba.fastjson.JSON;
import com.makbro.core.cms.article.service.StaticService;
import com.makbro.core.cms.spider.core.Page;
import com.makbro.core.cms.spider.core.Site;
import com.makbro.core.cms.spider.fieldprocessrule.bean.Fieldprocessrule;
import com.makbro.core.cms.spider.mission.bean.Spidermission;
import com.makbro.core.cms.spider.mission.service.SpidermissionService;
import com.makbro.core.cms.spider.pageProcessMx.bean.PageProcessMx;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.common.util.thread.MultiThreadHandler;
import com.markbro.base.common.util.thread.exception.ChildThreadException;
import com.markbro.base.common.util.thread.parallel.ParallelTaskWithThreadPool;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A simple PageProcessor.
 *
 * @author code4crafter@gmail.com <br>
 * @since 0.1.0
 */
public class PageProcessorMission implements PageProcessor {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    private String targetUrlPattern;
    private Integer limitCount=0;//限制处理多少
    private AtomicInteger totalLimit=new AtomicInteger(0);
    private AtomicInteger index=new AtomicInteger(1);//处理的第几个
    private Site site;
    private Spidermission spidermission;//趴取任务配置
    private List<PageProcessMx> pageProcessMxs;//页面处理字段对应配置
    private static final String CSS="css";
    private static final String XPATH="xpath";
    private static final String CONSTANT="constant";//常量
    private static final String CONTENT="content";//文章正文内容
    public PageProcessorMission(Spidermission spidermission,List<PageProcessMx> pageProcessMxs) {
        this.site = Site.me().setSleepTime(spidermission.getSleep()).setRetryTimes(1);
        this.spidermission=spidermission;
        this.pageProcessMxs=pageProcessMxs;
        this.targetUrlPattern=spidermission.getTargetUrlReg();
        if(spidermission.getLimitCount()>0){
            this.limitCount=spidermission.getLimitCount();
        }
    }
    private List<PageProcessMx> getByParentid(Integer parentid){
      return   pageProcessMxs.stream().filter(mx->{return  mx.getParentid().equals(parentid);}).collect(Collectors.toList());
    }
    private  boolean  dealWithList(PageProcessMx mx, Page page){
        List<PageProcessMx> mxList=getByParentid(mx.getId());
        if(CollectionUtils.isEmpty(mxList)){
            logger.error("字段{}[{}]类型为集合，并且没有获取到集合明细配置！",mx.getField(),mx.getFieldname());
            return  false;
        }
        Elements es=null;
        //提取数据
        if(CSS.equals(mx.getExtractbytype())){
            es= page.getHtml().getDocument().select(mx.getExtractby());
        }else if("xpath".equals(mx.getExtractbytype())){
            //TODO
        }else{}
        List<Map> list=new ArrayList<>();
        Map m=null;
        if(es!=null&&es.size() > 0){
            for(Element e:es){
                m=new HashMap<String,Object>();
                for(PageProcessMx listmx:mxList){
                      if("constant".equals(listmx.getExtractbytype())){
                          m.put(listmx.getField(),listmx.getConstant_value());
                          continue;
                      }

                    if("1".equals(listmx.getExtractby_attr_flag())){//取标签上的属性
                        try{
                            m.put(listmx.getField(), getAttributeByElement(e.select(listmx.getExtractby()).get(Integer.valueOf(listmx.getExtractby_index())), listmx.getExtractby_attr()));
                        }catch (Exception ex){
                            m.put(listmx.getField(),"");
                        }
                    }else{
                        try{
                            m.put(listmx.getField(), e.select(listmx.getExtractby()).get(Integer.valueOf(listmx.getExtractby_index())).html());
                        }catch (Exception ex){
                            m.put(listmx.getField(),"");
                        }
                    }
                }
                list.add(m);
            }
        }

        page.putField(mx.getField(),list);
        return  true;
    }
    private  void dealWithText(PageProcessMx mx,Page page){

        String field=mx.getField();//有可能取得同一个字段需要多个不同规则，所以明细表同一个任务可能配置多个相同字段取值规则以便适用不同页面
        //先判断该字段是否有值，如果没有在继续处理，有了就不用处理了
        String checkVal = String.valueOf(page.getResultItems().getAll().get(field));
        if(StringUtil.notEmpty(checkVal)){
            return;
        }
        Document pageDoc =page.getHtml().getDocument();
        Elements es_temp=null;
        String str_tar="";
        String Extractbytype= mx.getExtractbytype();// css 、xpath、constant


        String extractby=mx.getExtractby();

        String extractby_index_Str=mx.getExtractby_index();//数据库配置的提取索引参数，一般为0，但可能为0-5这样的区间
        String[] extractby_index_Arr=null;//构造出提取元素的索引数组
        if(extractby_index_Str.contains("-")){
            try{
                String[] tmpArr=extractby_index_Str.split("-");
                int arr_start=Integer.valueOf(tmpArr[0]);
                int arr_end=Integer.valueOf(tmpArr[1]);
                int length=arr_end-arr_start+1;
                extractby_index_Arr=new String[length];
                for(int i=0;i<length;i++){
                    extractby_index_Arr[i]=String.valueOf(arr_start);
                    arr_start++;
                }
            }catch (Exception ex){
                ex.printStackTrace();
                throw new IllegalArgumentException("字段["+mx.getField()+"]提取索引参数配置错误!");
            }
        }else{
            extractby_index_Arr=new String[1];
            extractby_index_Arr[0]=extractby_index_Str;
        }
        //提取数据
        if(CSS.equals(Extractbytype)){
            if("1".equals( mx.getExtractby_attr_flag())){//取标签上的属性
                try{
                    if(extractby_index_Arr!=null&&extractby_index_Arr.length>1){//多个
                        for(String t:extractby_index_Arr){
                            String tempRes="";
                            try {
                                tempRes= getAttributeByElement(pageDoc.select(extractby).get(Integer.valueOf(t)),mx.getExtractby_attr());
                            }catch (Exception ex){}
                            if(StringUtil.notEmpty(tempRes)){
                                str_tar +=tempRes;
                                str_tar +=",";//多个结果用逗号分隔
                            }
                        }
                        if(str_tar.endsWith(",")){
                            str_tar=str_tar.substring(0,str_tar.length()-1);
                        }
                    }else{//一个
                        str_tar = getAttributeByElement(pageDoc.select(extractby).get(Integer.valueOf(extractby_index_Str)),mx.getExtractby_attr());
                    }

                }catch(Exception ex) {}
            }else{
                try{
                    if(extractby_index_Arr!=null&&extractby_index_Arr.length>1){//多个
                        for(String t:extractby_index_Arr){
                            String tempRes="";
                            try{
                                tempRes=  pageDoc.select(extractby).get(Integer.valueOf(t)).html();
                            }catch (Exception ex){}
                            if(StringUtil.notEmpty(tempRes)){
                                str_tar +=tempRes;
                                str_tar +=",";//多个结果用逗号分隔
                            }
                        }
                        if(str_tar.endsWith(",")){
                            str_tar=str_tar.substring(0,str_tar.length()-1);
                        }
                    }else{//一个
                        str_tar= pageDoc.select(extractby).get(Integer.valueOf(mx.getExtractby_index())).html();
                    }
                }catch(Exception ex) {}

            }
        }else if(CONSTANT.equals(Extractbytype)){//常量
            str_tar = mx.getConstant_value();
        }else if(XPATH.equals(Extractbytype)){
            str_tar=page.getHtml().xpath(mx.getExtractby()).get();
        }else{}
        str_tar=str_tar.replaceAll("&nbsp;","");
        str_tar=processReplace(str_tar,mx);//按照字段内容处理规则处理内容
        page.putField(field,str_tar);

    }

    /**
     * 下载文章正文图片,处理文章内容，下载图片标签的图片到本地
     * @param page
     */
    public void processDownloadContentImage(Page page){
        String contentSource=page.getResultItems().get(CONTENT);
        if(StringUtil.notEmpty(contentSource)){//只下载文章正文的图片
            Document doc = Jsoup.parse(contentSource);
            if(doc!=null){
               final String contentImgFolder=spidermission.getContentImgFolder();

                List<String> imgUrls=new ArrayList<String>();
                Elements srcLinks = doc.select("img");
                for (Element link : srcLinks) {
                    //:剔除标签，只剩链接路径
                    String imagesPath = link.attr("src");
                    imgUrls.add(imagesPath);
                }
                if(CollectionUtils.isNotEmpty(imgUrls)){
                    //多线程下载图片
                    Map<String, Object> result = new HashMap<String, Object>(8, 1);
                    ExecutorService service = Executors.newFixedThreadPool(3);
                    MultiThreadHandler handler = new ParallelTaskWithThreadPool(service);
                    Runnable task = null;
                    // 启动3个子线程作为要处理的并行任务，共同完成结果集resultMap
                    for(String imgUrl:imgUrls){
                        task = new Runnable() {
                            @Override
                            public void run() {
                                boolean b=false;
                                String local_path ="";
                                //local_path = StaticService.downloadImageToLocal(imgUrl,null,false);//下载到本地
                                local_path = StaticService.downloadImageToQinNiuOss(imgUrl,StringUtil.isEmpty(contentImgFolder)?"":contentImgFolder);//下载图片到七牛osss
                                if(StringUtil.notEmpty(local_path)&&!"error".equals(local_path)){
                                    b=true;
                                }

                                Map<String, Object> t=(Map<String, Object>)result.get(imgUrl);
                                if(t==null){
                                    t=new HashMap<String, Object>();
                                }
                                if(b){
                                    t.put("result", "1");
                                    t.put("path", local_path);
                                    logger.debug("图片["+imgUrl+"] 下载成功！");
                                }else{
                                    t.put("result", "0");
                                    t.put("path", "");
                                    logger.warn("图片["+imgUrl+"] 下载失败！");
                                }
                                result.put(imgUrl,t);
                            }
                        };
                        handler.addTask(task);
                    }
                    try {
                        handler.run();
                    } catch (ChildThreadException e) {
                        logger.error(e.getAllStackTraceMessage());
                    }
                    logger.debug("processDownloadArticleImage:处理结束!result:" + JSON.toJSONString(result));
                    service.shutdown();

                    //表里result结果，把下载图片成功的图片替换成本地路径
                    for(Map.Entry entry:result.entrySet()){
                        String key=String.valueOf(entry.getKey());
                        Map rMap=(Map)entry.getValue();
                        if(TmConstant.NUM_ONE.equals(String.valueOf(rMap.get("result")))){
                            //下载成功的
                            String newPath=String.valueOf(rMap.get("path"));
                            contentSource=contentSource.replaceAll(key,newPath);
                        }
                    }
                }
            }
            page.putField(CONTENT,contentSource);
        }
    }
    public enum ReplaceRuleType{
        replace,
        reg,
        substrbefore,
        substrbeforeExclude,
        substrafter,
        substrafterExclude,
        substrlength
    }
    private String processReplace(String str_tar,PageProcessMx mx){
        //处理数据
        try{
            List<Fieldprocessrule> rules=mx.getFieldprocessrules();//字段加工规则

            if(CollectionUtils.isNotEmpty(rules)){//加工处理该字段
                rules.stream().sorted(Comparator.comparing(Fieldprocessrule::getSort));//集合按照sort排序
                for(Fieldprocessrule rule:rules){
                    if(ReplaceRuleType.replace.name().equals(rule.getFieldprocessrule())){
                        String reg=rule.getReplaceReg();
                        if(StringUtil.notEmpty(reg)){
                            logger.debug("字段["+mx.getField()+"]内容需要替换，替换的正则表达式为："+reg+"\t,替换成的内容为："+rule.getReplacement());
                            str_tar=str_tar.replaceAll(reg,StringUtil.notEmpty(rule.getReplacement())?rule.getReplacement():"");
                        }
                    }else if(ReplaceRuleType.reg.name().equals(rule.getFieldprocessrule())){//按照正则表达式提取
                        Pattern pattern = Pattern.compile(rule.getReplaceReg());
                        Matcher matcher = pattern.matcher(str_tar);
                        if(matcher.find()){
                            str_tar=matcher.group(0);
                        }else{
                            logger.debug("内容："+str_tar + "，未能匹配正则[" + rule.getReplaceReg() + "]");
                        }
                    }
                    else if(ReplaceRuleType.substrbefore.name().equals(rule.getFieldprocessrule())){
                        //截取某个字符串之前的内容
                        if(StringUtil.notEmpty(rule.getSubstrtarget())){
                            logger.debug("字段["+mx.getField()+"]的内容需要截取字符串["+rule.getSubstrtarget()+"]之前的内容");
                            str_tar=StringUtil.trim_before(str_tar, rule.getSubstrtarget());
                        }
                    }else if(ReplaceRuleType.substrbeforeExclude.name().equals(rule.getFieldprocessrule())){
                        //截取某个字符串之前的内容
                        if(StringUtil.notEmpty(rule.getSubstrtarget())){
                            logger.debug("字段["+mx.getField()+"]的内容需要截取字符串["+rule.getSubstrtarget()+"]之前的内容(不包含目标字符串)");
                            str_tar=StringUtil.trim_before_exclu(str_tar, rule.getSubstrtarget());
                        }
                    }else if(ReplaceRuleType.substrafter.name().equals(rule.getFieldprocessrule())){
                        logger.debug("字段["+mx.getField()+"]的内容需要截取字符串["+rule.getSubstrtarget()+"]之后的内容");
                        str_tar=StringUtil.trim_end(str_tar, rule.getSubstrtarget());
                    }else if(ReplaceRuleType.substrafterExclude.name().equals(rule.getFieldprocessrule())){
                        logger.debug("字段["+mx.getField()+"]的内容需要截取字符串["+rule.getSubstrtarget()+"]之后的内容(不包含目标字符串)");
                        str_tar=StringUtil.trim_end_exclu(str_tar, rule.getSubstrtarget());
                    }else if(ReplaceRuleType.substrlength.name().equals(rule.getFieldprocessrule())){
                        logger.debug("字段["+mx.getField()+"]的内容需要截取为长度["+rule.getSubstrlength()+"]的内容");
                        str_tar=StringUtil.trim_width(str_tar,rule.getSubstrlength());
                    }else{}
                }
            }
        }catch (Exception e){logger.error("processReplace出现了异常！"+e.getMessage());}
        return str_tar;
    }

    /**
     * @param regex
     * 正则表达式字符串
     * @param str
     * 要匹配的字符串
     * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
     */
    private static boolean match(String regex, String str) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }
    @Override
    public void process(Page page) {
        if(limitCount>0&&index.get()>limitCount){
            page.setSkip(true);
            return;
        }
        if("json".equals(spidermission.getMissionType())){
            page.putField("json",page.getJson().toString());
        }else{
            String requestUrl=page.getRequest().getUrl();
            if(StringUtil.isEmpty(page.getHtml().getDocument().body().toString())){
                //body为空
                logger.error("趴取网页BODY为空! URL:["+requestUrl+"] skip!");
                logger.debug(page.getHtml().toString());
                return;
            }
            //获取并添加TargetRequests
            List<String> requests =null;
            if(StringUtil.notEmpty(spidermission.getTargetUrlCssSelector())){
                requests = page.getHtml().css(spidermission.getTargetUrlCssSelector()).links().regex(targetUrlPattern).all();
            }else if(StringUtil.notEmpty(spidermission.getTargetUrlXpathSelector())){
                requests = page.getHtml().xpath(spidermission.getTargetUrlXpathSelector()).links().regex(targetUrlPattern).all();
            }else{
                requests = page.getHtml().links().regex(targetUrlPattern).all();
            }

            if(CollectionUtils.isNotEmpty(requests)){
                requests=requests.stream().distinct().collect(Collectors.toList());
                int count=requests.size();
                if(this.limitCount>0){//启用限制条数功能（默认处理全部）
                    if(totalLimit.get()<limitCount) {
                        if ((totalLimit.get() + count) >= limitCount) {
                            int temp = limitCount - totalLimit.get();
                            requests = requests.stream().limit(temp).collect(Collectors.toList());
                            for (int i = 0; i < temp; i++) {
                                totalLimit.getAndIncrement();
                            }
                        } else {
                            int temp = limitCount - totalLimit.get();
                            if (count >= temp) {
                                for (int i = 0; i < temp; i++) {
                                    totalLimit.getAndIncrement();
                                }
                            } else {
                                for (int i = 0; i < count; i++) {
                                    totalLimit.getAndIncrement();
                                }
                            }
                        }
                        if(CollectionUtils.isNotEmpty(requests)){
                            page.addTargetRequests(requests);
                        }
                    }
                }else{//默认处理全部
                    page.addTargetRequests(requests);
                }

            }

            //不符合规则的url跳过下面的页面解析（一般是入口地址,入口地址是用来采集获取目标地址的）
            if(!match(targetUrlPattern,requestUrl)&&!targetUrlPattern.equals(requestUrl)){
                page.setSkip(true);
                return;
            }
            if(pageProcessMxs!=null&&pageProcessMxs.size()>0){
                List<PageProcessMx> rootList=pageProcessMxs.stream().filter(mx->{return "0".equals(mx.getParentid().toString());}).collect(Collectors.toList());
                for(PageProcessMx mx:rootList){

                    String type=mx.getDatatype();//text or list
                    if(type.equals("list")){
                        if(!dealWithList(mx, page)){
                            break;
                        }
                    }else{
                        dealWithText(mx, page);
                    }

                }

                processDownloadContentImage(page);//下载文章正文图片,处理文章内容，下载图片标签的图片到本地
                //给写入表的map附加字段
                if(SpidermissionService.MissionType.article.toString().equals(spidermission.getMissionType())){
                    page.putField("mission_id",spidermission.getId());
                    page.putField("template_name",spidermission.getTemplateName());
                    page.putField("link",page.getRequest().getUrl());
                 //   page.putField("articletypes",spidermission.getResource_type_id());//为了简化趴虫任务的配置，这个字段不在配置到爬虫任务中。同时cms_article去掉articletypes字段

                }else if(SpidermissionService.MissionType.chapter.toString().equals(spidermission.getMissionType())){
                    page.putField("link",page.getRequest().getUrl());
                    page.putField("book_id",spidermission.getRef_id());
                }else if(SpidermissionService.MissionType.book.toString().equals(spidermission.getMissionType())){
                    page.putField("source",spidermission.getMissionName());
                    page.putField("link",page.getRequest().getUrl());
                }
                else{}
                page.putField("missionType",spidermission.getMissionType());
                //page.putField("tablename",spidermission.getTablename());
                if(StringUtil.notEmpty(spidermission.getYhid())){
                    page.putField("yhid",spidermission.getYhid());
                }else{
                    String yhid= MyBatisRequestUtil.getYhid();
                    page.putField("yhid",yhid);
                }

                index.incrementAndGet();
            }else{
                logger.error("没有查询到页面元素处理相关配置！");
            }
        }
    }

    private String getAttributeByElement(Element e,String attrName){
        // 判断如果属性名是href或者src
        String res = "";
        if ("href".equals(attrName) || "src".equals(attrName)) {
            // 因为要获取他们绝对路径
            res = e.attr("abs:" + attrName);
        } else {
            //不是href或者src
            res = e.attr(attrName);
        }
        return res;
    }

    @Override
    public Site getSite() {
        return site;
    }
}
