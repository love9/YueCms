package com.makbro.core.cms.resourcetype.bean;


import com.markbro.base.model.AliasModel;

/**
 * 资源分类 bean
 * @author  wujiyue on 2016-07-21 21:21:05.
 */
public class Resourcetype  implements AliasModel {
	private String id;//主键
	private String name;//资源分类名称
	private String parentid;//资源父ID
	private String parentids;//
	private Integer sort;//排序
	private String code;//资源代码
	private String description;//资源分类描述
	private String createTime;//
	private String updateTime;//
	private String createBy;//
	private String updateBy;//
	private Integer available;//可用标志
	private Integer deleted;//删除标志

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getParentid(){ return parentid ;}
	public void  setParentid(String parentid){this.parentid=parentid; }
	public String getParentids(){ return parentids ;}
	public void  setParentids(String parentids){this.parentids=parentids; }
	public String getCode(){ return code ;}
	public void  setCode(String code){this.code=code; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Resourcetype{" +
			"id=" + id+
			", name=" + name+
			", parentid=" + parentid+
			", parentids=" + parentids+
			", code=" + code+
			", sort=" + sort+
			", description=" + description+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", createBy=" + createBy+
			", updateBy=" + updateBy+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
