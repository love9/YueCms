package com.makbro.core.cms.spider.core.processor.example;


import com.makbro.core.cms.spider.core.*;
import com.makbro.core.cms.spider.core.pipeline.FilePipeline;
import com.makbro.core.cms.spider.core.processor.PageProcessor;

import java.util.List;

/**
 * @author code4crafter@gmail.com <br>
 * @since 0.6.0
 */
public class DuanziProcessor implements PageProcessor, SpiderListener {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(1500);

    @Override
    public void process(Page page) {

        try{
            List<String> urls= page.getHtml().css("#ct > div.mn > div.bm > ul").links().regex("http://wufafuwu.com/a/ONE_wenzhang/\\d+/\\d+/\\d+.html").all();
            page.addTargetRequests(urls);

            String title= page.getHtml().xpath("//*[@id=\"ct\"]/div[1]/div/div[1]/h1/text()").get().replaceAll("\\s*作者.*","");
            String author= page.getHtml().xpath("//*[@id=\"ct\"]/div[1]/div/div[1]/h1/text()").get().replaceAll(".*作者\\/","");
            System.out.println("title==>"+title);
            System.out.println("author==>"+author);
            String time=page.getHtml().xpath("//*[@id=\"ct\"]/div[1]/div/div[1]/p/text()").regex("\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{1,2}").get();
            System.out.println("time==>"+time);
            page.putField("title",title);
          //  page.putField("content",content);

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public void doit(){
     //String[] arr={"http://www.jinjumao.club/api/jinju/list?pageIndex=1&pageSize=15","http://www.jinjumao.club/api/jinju/list?pageIndex=2&pageSize=15","http://www.jinjumao.club/api/jinju/list?pageIndex=3&pageSize=15"};
     //Spider.create(new DuanziProcessor()).addUrl("http://www.jinjumao.club/api/jinju/list?pageIndex=1&pageSize=15").setSpiderListener(this)
                //.addPipeline(new FilePipeline()).run();
        Spider.create(new DuanziProcessor()).addUrl("http://wufafuwu.com/a/ONE_wenzhang/list_1_1.html").setSpiderListener(this)
                .addPipeline(new FilePipeline()).thread(3).run();

    }

    public static void main(String[] args) {
      new  DuanziProcessor().doit();
    }

    @Override
    public void onStart(Spider spider) {
        System.out.println("任务开始===》");
    }

    @Override
    public void onSuccess(Request request) {
        System.out.println("解析"+request.getUrl()+"成功!");
    }

    @Override
    public void onError(Request request) {
        System.out.println("解析"+request.getUrl()+"失败!");
    }

    @Override
    public void onFinish(Spider spider) {
        System.out.println("任务结束===》");
    }
}
