package com.makbro.core.cms.sendEmail.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.sendEmail.bean.SendEmail;
import com.makbro.core.cms.sendEmail.dao.SendEmailMapper;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 邮件 Service
 * Created by wujiyue on 2017-09-07 16:30:44.
 */
@Service
public class SendEmailService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private SendEmailMapper sendEmailMapper;

     /*基础公共方法*/
    public SendEmail get(String id){
        return sendEmailMapper.get(id);
    }

    public List<SendEmail> find(PageBounds pageBounds, Map<String,Object> map){
        return sendEmailMapper.find(pageBounds,map);
    }
    public List<SendEmail> findByMap(PageBounds pageBounds, Map<String,Object> map){
        return sendEmailMapper.findByMap(pageBounds,map);
    }

    public void add(SendEmail sendEmail){
        sendEmailMapper.add(sendEmail);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          try{
              String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
              SendEmail sendEmail= JSON.parseObject(JSON.toJSONString(map), SendEmail.class);
              sendEmail.setFromYhid(yhid);
              sendEmail.setUserid(yhid);
            if(sendEmail.getId()==null||"".equals(sendEmail.getId().toString())){
               String id= keyService.getStringId();
               sendEmail.setId(id);
               sendEmailMapper.add(sendEmail);
            }else{
               sendEmailMapper.update(sendEmail);
            }
               msg.setType(Msg.MsgType.success);
               msg.setContent("保存信息成功");
          }catch (Exception ex){
               msg.setType(Msg.MsgType.error);
               msg.setContent("保存信息失败");
          }
          return msg;
    }
    public void addBatch(List<SendEmail> sendEmails){
        sendEmailMapper.addBatch(sendEmails);
    }
    public void update(SendEmail sendEmail){
        sendEmailMapper.update(sendEmail);
    }
    public void updateByMap(Map<String,Object> map){
        sendEmailMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        sendEmailMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        sendEmailMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        sendEmailMapper.deleteBatch(ids);
    }
     /*自定义方法*/

    //放入回收站（单条）
     public void trash(String id){
         Map map=new HashMap<String,Object>();
         map.put("status","0");
         map.put("id",id);
         sendEmailMapper.updateByMap(map);
     }
    //设置为重要 （批量）
    public void importants(String[] ids){
        Map map=new HashMap<String,Object>();
        map.put("status","2");//状态 1正常 2重要 3草稿 0垃圾
        map.put("ids",ids);
        sendEmailMapper.updateByMapBatch(map);
    }


}
