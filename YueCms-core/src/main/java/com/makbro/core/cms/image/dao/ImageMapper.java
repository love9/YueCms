package com.makbro.core.cms.image.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.image.bean.Image;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 图片 dao
 * @author wujiyue
 * @date 2019-7-4 21:58:58
 */
@Repository
public interface ImageMapper{
    public Image get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Image image);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Image> images);
    public void update(Image image);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除、有效数据的条件（deleted=0,available=1）
    public List<Map<String,Object>> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Image> findByMap(PageBounds pageBounds, Map<String, Object> map);

    public List<Image> findByImageIds(@Param("ids") String ids);

}
