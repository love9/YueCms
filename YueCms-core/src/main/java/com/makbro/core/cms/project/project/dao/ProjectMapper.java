package com.makbro.core.cms.project.project.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.project.project.bean.Project;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 项目 dao
 * @author wujiyue
 * @date 2019-6-29 14:34:23
 */
@Repository
public interface ProjectMapper{

    public Project get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Project project);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Project> projects);
    public void update(Project project);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Project> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Project> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
