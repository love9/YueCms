package com.makbro.core.cms.spider.core.pipeline.db;


import com.makbro.core.cms.book.dao.BookMapper;
import com.makbro.core.cms.book.service.BookService;
import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Task;
import com.makbro.core.cms.spider.core.pipeline.BasePipeline;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by wjy on 2017/12/21.
 */
@Component
public class BookPipeline extends BasePipeline {

    @Autowired
    BookService bookService;
    @Autowired
    BookMapper bookMapper;
    public BookPipeline() {
    }
    @Override
    public void process(ResultItems resultItems, Task task) {

        //先判断是否应该跳过处理
        Map map=resultItems.getAll();

        String bookname=(String)map.get("bookname");
        if(StringUtil.isEmpty(bookname)){
            return;
        }
        int n=bookMapper.checkExistsByBookName(bookname);
        if(n>0){
            logger.warn("bookname:{}在数据库中已经存在!", bookname);
            return;
        }
        super.processCoverImage(resultItems);

        map.put("id", null);
        map.put("available",1);
        map.put("deleted",0);

        map.put("comment_flag",0);
        map.put("up_vote",0);
        map.put("down_vote",0);

       // Map<String,Object> queryMap=new HashMap<String,Object>();
        //queryMap.put("bookname",bookname);
       // List<Book> list= bookMapper.findByMap(new PageBounds(), queryMap);
       // if(CollectionUtils.isEmpty(list)){
            bookMapper.addByMap(map);
           // System.out.println(JSON.toJSON(map));
       // }


    }
}
