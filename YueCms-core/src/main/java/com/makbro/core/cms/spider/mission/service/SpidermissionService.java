package com.makbro.core.cms.spider.mission.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.spider.mission.bean.Spidermission;
import com.makbro.core.cms.spider.mission.dao.SpidermissionMapper;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 趴取任务 Service
 * Created by wujiyue on 2017-11-16 10:52:35.
 */
@Service
public class SpidermissionService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private SpidermissionMapper spidermissionMapper;

     /*基础公共方法*/
    public Spidermission get(Integer id){
        return spidermissionMapper.get(id);
    }

    public List<Spidermission> find(PageBounds pageBounds,Map<String,Object> map){
        return spidermissionMapper.find(pageBounds,map);
    }
    public List<Spidermission> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return spidermissionMapper.findByMap(pageBounds,map);
    }

    public enum MissionType{
        article,
        book,
        chapter
    }
    public void add(Spidermission spidermission){
        spidermissionMapper.add(spidermission);
    }
    public Object save(Map<String,Object> map){

        Spidermission spidermission = JSON.parseObject(JSON.toJSONString(map),Spidermission.class);
        if(MissionType.article.toString().equals(spidermission.getMissionType())){
            spidermission.setTablename("cms_article");//指定趴取内容存储到哪个表中,写入表中这个值没什么用，只是看看，别的地方不根据这个值判断了，而是根据missionType
        }else if(MissionType.book.toString().equals(spidermission.getMissionType())){
            spidermission.setTablename("cms_book");
        }else if(MissionType.chapter.toString().equals(spidermission.getMissionType())){
            spidermission.setTablename("cms_chapter_detail");
        }else{}

        if(spidermission.getId()==null||"".equals(spidermission.getId().toString())){
            Integer id= keyService.getIntegerId();
            int sort=spidermissionMapper.getMaxSort();
            spidermission.setSort(sort+1);
            spidermission.setId(id);
            spidermissionMapper.add(spidermission);
        }else{
            spidermissionMapper.update(spidermission);
        }
        return Msg.success("保存信息成功!");
    }
    public void addBatch(List<Spidermission> spidermissions){
        spidermissionMapper.addBatch(spidermissions);
    }

    public void update(Spidermission spidermission){
        spidermissionMapper.update(spidermission);
    }

    public void updateByMap(Map<String,Object> map){
        spidermissionMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        spidermissionMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        spidermissionMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        spidermissionMapper.deleteBatch(ids);
    }


	public Object saveSort(Map map){
		try{
			String sort = String.valueOf(map.get("sort"));
			if(!"".equals(sort)){
				String[] sx = sort.split(",");
				for(int i=0;i<sx.length;i++) {
					String[] arr = sx[i].split("_");
					spidermissionMapper.updateSort(arr[1], arr[2]);
				}
			}
            return Msg.success("排序成功!");
		}catch (Exception e){
            return Msg.error("排序失败!");
		}

	}

     /*自定义方法*/
     public Spidermission getByMissonCode(String missionCode){
         return  spidermissionMapper.getByMissonCode(missionCode);
     }
    public Object checkMissionCode(Map map){
        String missionCode = String.valueOf(map.get("missionCode"));
        if(StringUtil.notEmpty(missionCode)){
            int n=spidermissionMapper.checkMissionCode(missionCode);
            if(n==0){
                return Msg.success("任务代码可以使用!");
            }
        }
        return Msg.error("任务代码不可用!");
    }

}
