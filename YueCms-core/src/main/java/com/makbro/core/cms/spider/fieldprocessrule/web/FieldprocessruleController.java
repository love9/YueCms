package com.makbro.core.cms.spider.fieldprocessrule.web;


import com.makbro.core.cms.spider.fieldprocessrule.bean.Fieldprocessrule;
import com.makbro.core.cms.spider.fieldprocessrule.service.FieldprocessruleService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 字段处理规则管理
 * Created by wujiyue on 2017-11-18 15:20:49.
 */

@Controller
@RequestMapping("/spider/fieldprocessrule")
@RequiresPermissions("spider")
public class FieldprocessruleController extends BaseController {
    @Autowired
    protected FieldprocessruleService fieldprocessruleService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/spider/fieldprocessrule/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Fieldprocessrule fieldprocessrule, Model model){
                return "/cms/spider/fieldprocessrule/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Fieldprocessrule fieldprocessrule,Model model){
        if(fieldprocessrule!=null&&fieldprocessrule.getId()!=null){
            fieldprocessrule=fieldprocessruleService.get(fieldprocessrule.getId());
        }
         model.addAttribute("fieldprocessrule",fieldprocessrule);
         return "/cms/spider/fieldprocessrule/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return fieldprocessruleService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return fieldprocessruleService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return fieldprocessruleService.save(map);
    }

    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	try{
            fieldprocessruleService.delete(id);
            return Msg.success("删除成功!");
        }catch (Exception e){
            return Msg.error("删除失败!");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {
    	try{
            fieldprocessruleService.deleteBatch(ids);
            return Msg.success("删除成功!");
         }catch (Exception e){
            return Msg.error("删除失败!");
         }
    }
}