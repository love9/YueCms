package com.makbro.core.cms.book.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.book.bean.Book;
import com.makbro.core.cms.book.dao.BookMapper;
import com.makbro.core.cms.chapterDetail.bean.ChapterDetail;
import com.makbro.core.cms.chapterDetail.service.ChapterDetailService;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.service.TemplateService;
import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.common.util.FreeMarkerUtil;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 书籍 Service
 * Created by wujiyue on 2017-12-17 19:47:46.
 */
@Service
public class BookService implements InitCacheListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private BookMapper bookMapper;

     /*基础公共方法*/
    public Book get(String id){
        return bookMapper.get(id);
    }

    public List<Book> find(PageBounds pageBounds, Map<String,Object> map){
        return bookMapper.find(pageBounds,map);
    }
    public List<Book> findByMap(PageBounds pageBounds, Map<String,Object> map){
        return bookMapper.findByMap(pageBounds,map);
    }

    public void add(Book book){
        bookMapper.add(book);
    }
    public Object save(Map<String,Object> map){

          Book book= JSON.parseObject(JSON.toJSONString(map),Book.class);
          if(book.getId()==null||"".equals(book.getId().toString())){
              String id= keyService.getStringId();
              book.setId(id);
              bookMapper.add(book);
          }else{
              bookMapper.update(book);
          }
        return Msg.success("保存信息成功");
    }
    public void addBatch(List<Book> books){
        bookMapper.addBatch(books);
    }

    public void update(Book book){
        bookMapper.update(book);
    }

    public void updateByMap(Map<String,Object> map){
        bookMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        bookMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        bookMapper.delete(id);
    }

    public void deleteBatch(String[] ids){
        bookMapper.deleteBatch(ids);
    }

    public static final String KEY_BOOK_TAGS="cms_book_tags";
    @Override
    public void onInit() {
        //为了提高效率，展示书籍标签云的时候不从数据库查询，而去缓存获得标签。而缓存的数据是项目启动时在这里加载进去的
        List<String> tags=bookMapper.findAllBookTags();
        if(CollectionUtils.isNotEmpty(tags)){
            EhCacheUtils.putSysInfo(KEY_BOOK_TAGS,tags);
        }

    }

    @Autowired
    ChapterDetailService chapterDetailService;
     /*自定义方法*/
     public Book getBookAndChapterList(String id){
         Book book= bookMapper.get(id);
         Map map=new HashMap<String,Object>();
         map.put("book_id",id);
         List<ChapterDetail> details= chapterDetailService.find(new PageBounds(), map);
         details=details.stream().sorted((chapterDetail1,chapterDetail2)->{return chapterDetail1.getSort().compareTo(chapterDetail2.getSort());}).collect(Collectors.toList());
         book.setChapterDetails(details);
         return book;
     }

    private static final String KEY_NAME="name";

    /**
     * 书籍静态化
     * @param bookId 书籍ID
     */
    public Object bookStaticFreemarker(String bookId){
        try {
            Book checkBook=this.get(bookId);
            if(checkBook==null){
                return Msg.error("不存在bookId["+bookId+"]的记录！");
            }
            if(checkBook.getStatic_flag()==2){
                return Msg.error("当前bookId["+bookId+"]正在执行静态化!");
            }
            /*if(checkBook.getStatic_flag()!=2){
                //更新static_flag=2表示正在执行静态化
                Book updateBook=new Book();
                updateBook.setId(bookId);
                updateBook.setStatic_flag(2);
                this.update(updateBook);
            }*/

            Book book = (Book)EhCacheUtils.getSysInfo("staticBook",bookId);//先从缓存中获取书籍和章节
            if(book==null){
                book = this.getBookAndChapterList(bookId);
                EhCacheUtils.putSysInfo("staticBook",bookId);
            }
            if(book==null){
                return Msg.error("获取bookId["+bookId+"]的记录为空！");
            }

            String suffix= ".html";

            String path=""; String targetPath="";
            if(GlobalConfig.isDevMode()){//开发模式
                path= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.webAppPath")+ GlobalConfig.getConfig("static.relativeBathPath");
                targetPath= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.targetPath")+ GlobalConfig.getConfig("static.relativeBathPath");
                if (!targetPath.endsWith("/")) {
                    targetPath += "/";
                }
            }else{
                path= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.relativeBathPath");
            }
            if (!path.endsWith("/")) {
                path += "/";
            }
            String relative_path="";
            String relative_base_path= GlobalConfig.getConfig("static.relativeBathPath");
            String htmlName=bookId;
            if(StringUtil.notEmpty(relative_base_path)){
                relative_path= relative_base_path+ File.separator+"book"+File.separator+bookId+File.separator+htmlName+ suffix;//文件相对路径
                path=path +"book"+File.separator+bookId+File.separator+ htmlName+ suffix;//文件绝对路径
                if(GlobalConfig.isDevMode()){
                    targetPath=targetPath +"book"+File.separator+bookId+File.separator+ htmlName+ suffix;
                }
            }else{//没配置相对路径，就不考虑它
                return Msg.error("properties文件没有配置static.relativeBathPath参数!");
            }

            //静态化模板

            String templateName="bookStatic";
            Template template= templateService.getByName(templateName);

            if(template==null){
                return Msg.error("未能获取到静态化模板["+templateName+"]！");
            }

            Map<String,String> stringTemplateMap=new HashMap<>();
            stringTemplateMap.put(KEY_NAME,template.getContent());
            FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(FreeMarkerUtil.TemplateLoaderType.StringTemplateLoader,stringTemplateMap);
            Map<String,Object> dataMap=new HashMap<>();
            dataMap.put("book", book);
            freeMarkerUtil.ExecuteFile(KEY_NAME,dataMap,path);
            if(GlobalConfig.isDevMode()){
                freeMarkerUtil.ExecuteFile(KEY_NAME,dataMap,targetPath);
            }
            //把生成的静态文件相对地址写到数据库
            Book updateBook=new Book();
            updateBook.setId(bookId);
            relative_path=relative_path.replaceAll("\\\\","\\/");//把反斜杠换成正斜杠
            updateBook.setStatic_url(relative_path);
            this.update(updateBook);
            logger.info("书籍(bookId:{})静态化成功!filepath===>"+path,bookId);
            return Msg.success("书籍bookId:"+book.getId()+",bookName:《"+book.getBookname()+"》静态化完成!");
        } catch (Exception e) {
            logger.warn("bookStaticFreemarker 出现异常:"+e.getMessage());
            return Msg.error("bookStaticFreemarker出现异常:"+e.getMessage());
        }

    }

    @Autowired
    TemplateService templateService;
    /**
     * 书籍章节静态化
     * @param bookId
     */
    public Object bookChapterStaticFreemarker(String bookId){
        try {
            Book checkBook=this.get(bookId);
            if(checkBook==null){
               return Msg.error("不存在bookId["+bookId+"]的记录！");
            }
            if(checkBook.getStatic_flag()==2){
                return Msg.error("当前bookId["+bookId+"]正在执行静态化!");
            }
            if(checkBook.getStatic_flag()!=2){
                //更新static_flag=2表示正在执行静态化
                Book updateBook=new Book();
                updateBook.setId(bookId);
                updateBook.setStatic_flag(2);
                this.update(updateBook);
            }

            Book book = (Book)EhCacheUtils.getSysInfo("staticBook",bookId);//先从缓存中获取书籍和章节
            if(book==null){
                book = this.getBookAndChapterList(bookId);
                EhCacheUtils.putSysInfo("staticBook",bookId);
            }

            if(book==null){
                return Msg.error("获取bookId["+bookId+"]的记录为空！");
            }
            List<ChapterDetail> details=book.getChapterDetails();
            if(CollectionUtils.isEmpty(details)){
                return Msg.error("获取bookId["+bookId+"]章节的记录为空！");
            }
                String absoluteBasePath="";String targetBasePath="";
                if(GlobalConfig.isDevMode()){//开发模式
                    absoluteBasePath= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.webAppPath")+ GlobalConfig.getConfig("static.relativeBathPath");
                    targetBasePath= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.targetPath")+ GlobalConfig.getConfig("static.relativeBathPath");
                    if (!targetBasePath.endsWith("/")) {
                        targetBasePath += "/";
                    }
                }else{
                    absoluteBasePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.relativeBathPath");
                }
                if (!absoluteBasePath.endsWith("/")) {
                    absoluteBasePath += "/";
                }
                String relative_path="";
                String relative_base_path= GlobalConfig.getConfig("static.relativeBathPath");

                //静态化模板

                String templateName="bookChapterStatic";
                Template template= templateService.getByName(templateName);

                if(template==null){
                    return Msg.error("未能获取到静态化模板["+templateName+"]！");
                }

                //将模板初始化到FreeMarkerUtil
                Map<String,String> stringTemplateMap=new HashMap<>();
                stringTemplateMap.put(KEY_NAME,template.getContent());
                FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(FreeMarkerUtil.TemplateLoaderType.StringTemplateLoader,stringTemplateMap);

                for(ChapterDetail tempDetail:details){
                    String path="";String targetPath="";
                    //tempDetail没有内容，只包含简单的信息，所以要从数据库取
                    ChapterDetail detail=chapterDetailService.get(tempDetail.getId());
                    if(detail==null){
                        logger.warn("书籍bookId:{},bookName:《{}》章节:《{}》,序号:{}从数据库没有获取到书籍章节信息!跳过静态化!",book.getId(),book.getBookname(),detail.getTitle(),detail.getSort());
                        continue;
                    }
                    String htmlName=detail.getSort().toString();
                    if(StringUtil.notEmpty(relative_base_path)){

                        //拼接相对路径
                        relative_path= relative_base_path+ File.separator+"book"+File.separator+bookId+File.separator+"detail"+File.separator+htmlName+ ".html";//文件相对路径
                        //拼接绝对路径
                        path=absoluteBasePath +"book"+File.separator+bookId+File.separator+"detail"+File.separator+ htmlName+ ".html";//文件绝对路径
                        if(GlobalConfig.isDevMode()){
                            targetPath=targetBasePath +"book"+File.separator+bookId+File.separator+"detail"+File.separator+ htmlName+ ".html";
                        }
                        File file=new File(path);
                        path=file.getCanonicalPath();
                    }else{//没配置相对路径，就不考虑它
                        return Msg.error("properties文件没有配置static.relativeBathPath参数!");
                    }


                    Map<String,Object> dataMap=new HashMap<>();
                    dataMap.put("book", book);
                    dataMap.put("detail", detail);



                    List<ChapterDetail> chapterDetails=book.getChapterDetails();
                    ChapterDetail pre_chapter=null;
                    ChapterDetail next_chapter=null;
                    int sort=detail.getSort();

                    if(sort>1){
                        List<ChapterDetail> tempList = chapterDetails.stream().filter(c->{
                            return   c.getSort()==(sort-1);
                        }).collect(Collectors.toList());
                        if(CollectionUtils.isNotEmpty(tempList)){
                            pre_chapter=tempList.get(0);
                        }
                    }
                    List<ChapterDetail> tempList = chapterDetails.stream().filter(c->{
                        return   c.getSort()==(sort+1);
                    }).collect(Collectors.toList());
                    if(CollectionUtils.isNotEmpty(tempList)){
                        next_chapter=tempList.get(0);
                    }
                    dataMap.put("chapterDetails", chapterDetails);
                    dataMap.put("pre_chapter", pre_chapter);
                    dataMap.put("next_chapter", next_chapter);

                    freeMarkerUtil.ExecuteFile(KEY_NAME,dataMap,path);
                    if(GlobalConfig.isDevMode()){
                        freeMarkerUtil.ExecuteFile(KEY_NAME,dataMap,targetPath);
                    }
                    logger.info("filepath===>"+path);
                    //把生成的静态文件相对地址写到数据库
                    ChapterDetail updateDetail=new ChapterDetail();
                    updateDetail.setId(detail.getId());
                    //  / front/static\book\13\detail\1.html
                    relative_path=relative_path.replaceAll("\\\\","\\/");//把反斜杠换成正斜杠
                    updateDetail.setStatic_url(relative_path);
                    chapterDetailService.update(updateDetail);
                    logger.info("书籍bookId:{},bookName:《{}》章节:《{}》,序号:{}静态化成功!",book.getId(),book.getBookname(),detail.getTitle(),detail.getSort());

                }
                //更新static_flag=1表示正在执行静态化完成
                Book updateBook=new Book();
                updateBook.setId(bookId);
                updateBook.setStatic_flag(1);
                this.update(updateBook);
                return Msg.success("书籍bookId:"+book.getId()+",bookName:《"+book.getBookname()+"》章节静态化完成!");


        } catch (Exception e) {
            //更新static_flag=3表示正在执行静态化异常
            Book updateBook=new Book();
            updateBook.setId(bookId);
            updateBook.setStatic_flag(3);
            this.update(updateBook);
            logger.warn("bookChapterStaticFreemarker出现异常:"+e.getMessage());
            return Msg.error("bookChapterStaticFreemarker出现异常:"+e.getMessage());
        }

    }
}
