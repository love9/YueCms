package com.makbro.core.cms.spider.pageProcessMx.web;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.spider.pageProcessMx.bean.PageProcessMx;
import com.makbro.core.cms.spider.pageProcessMx.service.PageProcessMxService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 页面字段配置管理
 * Created by wujiyue on 2017-11-16 11:07:50.
 */

@Controller
@RequestMapping("/spider/pageProcessMx")
@RequiresPermissions("spider")
public class PageProcessMxController extends BaseController {
    @Autowired
    protected PageProcessMxService pageProcessMxService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/spider/pageProcessMx/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(PageProcessMx pageProcessMx, Model model){
        String zbguid= getParam("zbguid");
        model.addAttribute("zbguid",zbguid);
        return "/cms/spider/pageProcessMx/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(PageProcessMx pageProcessMx,Model model){
         if(pageProcessMx!=null&&pageProcessMx.getId()!=null){
            pageProcessMx=pageProcessMxService.get(pageProcessMx.getId());
         }
         model.addAttribute("pageProcessMx",pageProcessMx);
         Map tmap=new HashMap<String,Object>();
         tmap.put("parentid",pageProcessMx.getId());
         List<PageProcessMx> lines=pageProcessMxService.find(new PageBounds(), tmap);
         model.addAttribute("lines",lines);
         return "/cms/spider/pageProcessMx/edit";
    }
    //-----------json数据接口--------------------

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return pageProcessMxService.get(id);
    }

    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return pageProcessMxService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return pageProcessMxService.save(map);
    }
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	try{
            pageProcessMxService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
        	return Msg.error("删除失败！");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
            pageProcessMxService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
}