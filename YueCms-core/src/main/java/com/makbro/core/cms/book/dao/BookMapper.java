package com.makbro.core.cms.book.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.book.bean.Book;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 书籍 dao
 * @author  wujiyue on 2018-08-29 14:03:25.
 */
@Repository
public interface BookMapper{

    public Book get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Book book);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Book> books);
    public void update(Book book);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）并且返回值类型不同
    public List<Book> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Book> findByMap(PageBounds pageBounds, Map<String, Object> map);

    public List<String> findAllBookTags();

    //查询待趴取章节内容的书籍（spiderMissionCode不为空并且status=0）
    public List<Book> findForSpider();

    public int checkExistsByBookName(String bookName);

    @Select("select id from cms_book where static_flag=0 limit 1")
    public String getBookIdForChapterStatic();//书籍章节的静态化

    @Select("select id from cms_book where static_url is null or static_url='' LIMIT 1")
    public String getBookIdForBookStatic();//书籍的静态化

}
