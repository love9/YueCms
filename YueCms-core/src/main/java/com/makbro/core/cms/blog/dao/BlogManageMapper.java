package com.makbro.core.cms.blog.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.blog.bean.BlogAuthorInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 博客访问信息 dao
 * @author  wujiyue on 2018-09-19 23:43:02.
 */
@Repository
public interface BlogManageMapper {

    public BlogAuthorInfo getBlogAuthorInfo(String id);
    public Map<String,Object> getMapBlogAuthorInfo(String id);
    public void addBlogAuthorInfo(BlogAuthorInfo blogAuthorInfo);
    public void addByMapBlogAuthorInfo(Map<String, Object> map);
    public void updateBlogAuthorInfo(BlogAuthorInfo blogAuthorInfo);
    public void updateByMapBlogAuthorInfo(Map<String, Object> map);
    public void deleteBlogAuthorInfo(String id);
    public void deleteBatchBlogAuthorInfo(String[] ids);
    public List<BlogAuthorInfo> findBlogAuthorInfo(PageBounds pageBounds, Map<String, Object> map);
    public List<BlogAuthorInfo> findByMapBlogAuthorInfo(PageBounds pageBounds, Map<String, Object> map);
    //累加访问次数
    public void updateVisitNum(@Param("yhid") String yhid, @Param("count") Integer count);

    @Update("update cms_blog_author_info set visit_yesterday = visit_today,visit_today=0,update_time=now()")
    public int updateVisitNumDay();//每天执行把cms_blog_author_info 表中visit_yesterday=visit_today,visit_today=0
    @Update("update cms_blog_author_info set visit_week=0,update_time=now()")
    public int updateVisitNumWeek();//每周一零点执行visit_week置零
    @Update("update cms_blog_author_info set visit_month=0,update_time=now()")
    public int updateVisitNumMonth();//每月第一天零点执行visit_month置零
}
