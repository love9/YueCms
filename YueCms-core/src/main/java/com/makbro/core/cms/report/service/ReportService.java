package com.makbro.core.cms.report.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.report.bean.Report;
import com.makbro.core.cms.report.dao.ReportMapper;
import com.markbro.base.model.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 举报 Service
 * @author  wujiyue on 2018-07-27 15:22:31.
 */
@Service
public class ReportService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private ReportMapper reportMapper;

     /*基础公共方法*/
    public Report get(Integer id){
        return reportMapper.get(id);
    }
    public List<Report> find(PageBounds pageBounds, Map<String,Object> map){
        return reportMapper.find(pageBounds,map);
    }
    public List<Map<String,Object>> findByMap(PageBounds pageBounds, Map<String,Object> map){
        return reportMapper.findByMap(pageBounds,map);
    }
    public void add(Report report){
        reportMapper.add(report);
    }
    public Object save(Map<String,Object> map){

          Report report= JSON.parseObject(JSON.toJSONString(map),Report.class);
          if(report.getId()==null||"".equals(report.getId().toString())){
              Integer id= keyService.getIntegerId();
              report.setId(id);
              reportMapper.add(report);
          }else{
              reportMapper.update(report);
          }
          return Msg.success("保存信息成功");
    }
    public void addBatch(List<Report> reports){
        reportMapper.addBatch(reports);
    }

    public void update(Report report){
        reportMapper.update(report);
    }

    public void updateByMap(Map<String,Object> map){
        reportMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        reportMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        reportMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        reportMapper.deleteBatch(ids);
    }
     /*自定义方法*/
    //举报
    public Object doReport(Map<String,Object> map){
        String module=String.valueOf(map.get("module"));
        String target_id=String.valueOf(map.get("target_id"));
        String report_type=String.valueOf(map.get("report_type"));
        int n=reportMapper.checkExists(module,target_id,report_type);
        if(n==0){//未存在在数据库表中
            map.put("report_yhid",String.valueOf(map.get("yhid")));
            map.put("count","1");
            map.put("deal_flag","0");
            reportMapper.addByMap(map);
        }
        return Msg.success("举报成功!");
    }
}
