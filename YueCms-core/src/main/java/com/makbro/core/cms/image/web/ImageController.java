package com.makbro.core.cms.image.web;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.image.bean.Image;
import com.makbro.core.cms.image.service.ImageService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.annotation.Referer;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 图片管理
 * Created by wujiyue on 2016-08-12 21:03:06.
 */
@Controller
@RequestMapping("/cms/image")
public class ImageController extends BaseController {
    @Autowired
    protected ImageService imageService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/cms/image/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        return "/cms/image/add";
    }
    /**
     * 跳转到编辑页面
     */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String id=(String)map.get("id");
        Map<String,Object> image=imageService.getMap(id);
        model.addAttribute("image",image);
        return "/cms/image/edit";
    }

    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return imageService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        resultMap=getPageMap(imageService.find(getPageBounds(),MyBatisRequestUtil.getMap(request)));
        return resultMap;
    }
    /**
     * 不分页查询数据
     */
    @ResponseBody
    @RequestMapping("/json/findAll")
    public Object findAll() {
        Map map=MyBatisRequestUtil.getMap(request);
        String sortString=String.valueOf(map.get("sortString"));
        PageBounds pageBounds=null;
        if(StringUtil.notEmpty(sortString)){
             pageBounds=new PageBounds(Order.formString(sortString));
        }else{
             pageBounds=new PageBounds();
        }
        resultMap=getPageMap(imageService.find(pageBounds,map));
        return resultMap;
    }
    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增图片")
    public void add(Image m) {

        imageService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Image m) {
        imageService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存图片")
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return  imageService.save(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
	Msg msg=new Msg();
	try{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		map.put("id",id);
		imageService.updateByMap(map);
		msg.setType(Msg.MsgType.success);
		msg.setContent("删除成功！");
	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("删除失败！");
	}
	return msg;
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){
	Msg msg=new Msg();
	try{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		map.put("ids",ids);
		imageService.updateByMapBatch(map);
		msg.setType(Msg.MsgType.success);
		msg.setContent("批量删除成功！");
	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("批量删除失败！");
	}
	return msg;
	}
     @ResponseBody
     @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
     @Referer(methodCode = "ImageController.delete")
     public Object delete(@PathVariable String id) {
        Msg msg=new Msg();
        try{
        	String deleteFile=(String)(MyBatisRequestUtil.getMap(request).get("deleteFile"));
            if("1".equals(deleteFile)){
                imageService.deleteFile(id,request);
            }
            imageService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
            msg.setType(Msg.MsgType.error);
            msg.setContent("删除失败！");
        }
            return msg;
     }
     @ResponseBody
     @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
     public Object deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
        try{
        	 
             imageService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
        }catch (Exception e){
             msg.setType(Msg.MsgType.error);
             msg.setContent("删除失败！");
        }
             return msg;
     }

    //上传单张图片资源
    @ResponseBody
    @RequestMapping("/uploadSingle")
    @Referer(methodCode = "ImageController.uploadSingle")//methodCode唯一确定一个方法
    public Object uploadSingle() {
        Map map = MyBatisRequestUtil.getMapGuest(request);
        PrintWriter write = null;
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("file");
        if(multipartFile != null){
            map = imageService.uploadSingle(map, request, multipartFile);
        }

        return map;
      /*  JSONArray json=JSONArray.fromObject(map);
        try {
            write = response.getWriter();
            write.write(json.toString());
            write.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response = null;
            if (write != null)
                write.close();
            write = null;
        }*/
    }
    //上传多张图片资源
    @RequestMapping("/uploadMulti")
    public void uploadMulti() {
        Map map = MyBatisRequestUtil.getMapGuest(request);
        PrintWriter write = null;
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        List<MultipartFile> multipartFiles= multipartRequest.getFiles("file");
        List<Map<String, Object>> list =null;
        if(multipartFiles != null){
           list = imageService.uploadMulti(map, request, multipartFiles);
        }
        JSONArray json=JSONArray.fromObject(list);
        try {
            write = response.getWriter();
            write.write(json.toString());
            write.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response = null;
            if (write != null){
                write.close();
            }
            write = null;
        }
    }
}
