package com.makbro.core.cms.spider.core.processor.example;


import com.makbro.core.cms.spider.core.*;
import com.makbro.core.cms.spider.core.processor.PageProcessor;

/**
 * @author code4crafter@gmail.com <br>
 * @since 0.6.0
 */
public class JsonPageProcessor implements PageProcessor, SpiderListener {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(1500);

    @Override
    public void process(Page page) {
         System.out.println(page.getJson());

        page.putField("json",page.getJson().toString());
    }

    @Override
    public Site getSite() {
        return site;
    }

    public void doit(){

        String[] arr={"http://www.jinjumao.club/api/jinju/list?pageIndex=1&pageSize=105","http://www.jinjumao.club/api/jinju/list?pageIndex=2&pageSize=15","http://www.jinjumao.club/api/jinju/list?pageIndex=3&pageSize=15"};
     Spider.create(new JsonPageProcessor()).addUrl("http://www.jinjumao.club/api/jinju/list?pageIndex=1&pageSize=1150").setSpiderListener(this)
                .run();
    }
    public static void main(String[] args) {
      new JsonPageProcessor().doit();
    }

    @Override
    public void onStart(Spider spider) {
        System.out.println("任务开始===》");
    }

    @Override
    public void onSuccess(Request request) {
        System.out.println("解析"+request.getUrl()+"成功!");
    }

    @Override
    public void onError(Request request) {
        System.out.println("解析"+request.getUrl()+"失败!");
    }

    @Override
    public void onFinish(Spider spider) {
        System.out.println("任务结束===》");
    }
}
