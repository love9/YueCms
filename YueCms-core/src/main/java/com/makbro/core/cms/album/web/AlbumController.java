package com.makbro.core.cms.album.web;

import com.makbro.core.cms.album.bean.Album;
import com.makbro.core.cms.album.service.AlbumService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 相册图集管理管理
 * @author wujiyue
 * @date 2019-7-4 21:56:54
 */

@Controller
@RequestMapping("/cms/album")
public class AlbumController extends BaseController {
    @Autowired
    protected AlbumService albumService;

    @RequestMapping(value={"","/"})
    public String index(){
        return "/cms/album/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Album album, Model model){
        return "/cms/album/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Album album, Model model){
        if(album!=null&&album.getId()!=null){
            album=albumService.get(album.getId());
        }
         model.addAttribute("album",album);
         return "/cms/album/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return albumService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        resultMap=getPageMap(albumService.find(getPageBounds(), MyBatisRequestUtil.getMap(request)));
        return resultMap;
    }


    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(Album m) {

        albumService.add(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Album m) {
        albumService.update(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save(Album m) {
           return albumService.save(m);
    }

    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable Integer id){
	Msg msg=new Msg();
	try{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		map.put("id",id);
		albumService.updateByMap(map);
		msg.setType(Msg.MsgType.success);
		msg.setContent("删除成功！");
	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("删除失败！");
	}
	return msg;
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable Integer[] ids){
	Msg msg=new Msg();
	try{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		map.put("ids",ids);
		albumService.updateByMapBatch(map);
		msg.setType(Msg.MsgType.success);
		msg.setContent("批量删除成功！");
	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("批量删除失败！");
	}
	return msg;
	}


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	Msg msg=new Msg();
    	try{

            albumService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
        		msg.setType(Msg.MsgType.error);
        		msg.setContent("删除失败！");
        }
        return msg;
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
    	try{
    	     
             albumService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }


    /**
     * 根据相册专辑ID获得图片数据
     *  $.getJSON(sys_ctx+'/cms/album/json/findImagesByAlbumId?id='+${id}, function(json){
     alert(JSON.stringify(json));
     layer.photos({
     photos: json
     ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
     });
     });
     * @return
     */
    @ResponseBody
    @RequestMapping("/json/findImagesByAlbumId")
    public Object findImagesByBannerId() {
        Map map=MyBatisRequestUtil.getMap(request);
        String id=String.valueOf(map.get("id"));
        return albumService.findImagesByAlbumId(id);
    }
    @ResponseBody
    @RequestMapping("/json/findPhotos")
    public Object findPhotos() {
        Map map=MyBatisRequestUtil.getMap(request);
        return albumService.findPhotos(getPageBounds(),map);
    }

}