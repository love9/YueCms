package com.makbro.core.cms.image.bean;


import com.markbro.base.model.AliasModel;

/**
 * 图片 bean
 * @author wujiyue
 * @date 2019-7-4 21:58:58
 */
public class Image  implements AliasModel {
	private String id;//
	private String yhid;//
	private String datascope;//数据访问类型
	private String name;//图片名称
	private String url;//
	private String jumpt_url;//点击图片跳转的url
	private String url_small;//
	private Integer size;//大小
	private String suffixes;//后缀
	private String keywords;//关键词
	private String resourcetypeid;//资源类型
	private String createBy;//
	private String createTime;//
	private String updateBy;//
	private String updateTime;//
	private Integer available;//状态
	private Integer deleted;//删除标志

	public String getJumpt_url() {
		return jumpt_url;
	}

	public void setJumpt_url(String jumpt_url) {
		this.jumpt_url = jumpt_url;
	}

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getDatascope(){ return datascope ;}
	public void  setDatascope(String datascope){this.datascope=datascope; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getUrl(){ return url ;}
	public void  setUrl(String url){this.url=url; }
	public String getUrl_small(){ return url_small ;}
	public void  setUrl_small(String url_small){this.url_small=url_small; }
	public Integer getSize(){ return size ;}
	public void  setSize(Integer size){this.size=size; }
	public String getSuffixes(){ return suffixes ;}
	public void  setSuffixes(String suffixes){this.suffixes=suffixes; }
	public String getKeywords(){ return keywords ;}
	public void  setKeywords(String keywords){this.keywords=keywords; }
	public String getResourcetypeid(){ return resourcetypeid ;}
	public void  setResourcetypeid(String resourcetypeid){this.resourcetypeid=resourcetypeid; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Image{" +
			"id=" + id+
			", yhid=" + yhid+
			", datascope=" + datascope+
			", name=" + name+
			", url=" + url+
			", url_small=" + url_small+
			", size=" + size+
			", suffixes=" + suffixes+
			", keywords=" + keywords+
			", resourcetypeid=" + resourcetypeid+
			", createBy=" + createBy+
			", createTime=" + createTime+
			", updateBy=" + updateBy+
			", updateTime=" + updateTime+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
