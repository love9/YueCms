package com.makbro.core.cms.comment.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.comment.bean.Comment;
import com.makbro.core.cms.comment.dao.CommentMapper;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.test.TestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 评论 Service
 * @author  wujiyue on 2017-12-26 09:14:39.
 */
@Service
public class CommentService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    CommentFilterService commentFilterService;

    public static final String KEY_UP="up";//顶
    public static final String KEY_DOWN="down";//踩
    public static final String KEY_PEI="pei";
    public static final String KEY_PEN_ZI="penzi";
    public static final String KEY_DOU="dou";
    public static final String KEY_GEI_LI="geili";

     /*基础公共方法*/
    public Comment get(Integer id){
        return commentMapper.get(id);
    }

    public List<Comment> find(PageBounds pageBounds, Map<String,Object> map){
        return commentMapper.find(pageBounds,map);
    }
    public List<Comment> findByMap(PageBounds pageBounds, Map<String,Object> map){
        return commentMapper.findByMap(pageBounds,map);
    }

    public void add(Comment comment){
        commentMapper.add(comment);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          Comment comment=  JSON.parseObject(JSON.toJSONString(map),Comment.class);
          if(comment.getId()==null||"".equals(comment.getId().toString())){
              Integer id= keyService.getIntegerId();
              comment.setId(id);
              Map<String,Object> res=   commentFilterService.filter(comment.getComment());
              String content_new=(String)res.get("new");
              boolean flag=(Boolean)res.get("flag");
              if(flag){
                  List<String> list = (List<String>) res.get("list");
                  logger.warn("用户id{},用户名称{}在评论中发表不良言论!target_id:{},type:{}", map.get(TmConstant.YHID_KEY), map.get("yhmc"), comment.getTarget_id(), comment.getType());
                  System.out.println("不良言论内容：");
                  TestUtil.printList(list);
              }
              commentMapper.add(comment);
          }else{
              commentMapper.update(comment);
          }
          msg.setType(Msg.MsgType.success);
          msg.setContent("保存信息成功");
          return msg;
    }
    public void addBatch(List<Comment> comments){
        commentMapper.addBatch(comments);
    }

    public void update(Comment comment){
        commentMapper.update(comment);
    }

    public void updateByMap(Map<String,Object> map){
        commentMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        commentMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        commentMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        commentMapper.deleteBatch(ids);
    }



     /*自定义方法*/
    //评论顶和踩（支持/反对）
    public void votes(Integer id,String type){
        if(CommentService.KEY_UP.equals(type)){
            commentMapper.upVote(id);
        }else if(CommentService.KEY_DOWN.equals(type)){
            commentMapper.downVote(id);
        }else{}
    }
    //评论印章
    public void yinzhang(Integer id,String type){
        if(CommentService.KEY_PEI.equals(type)){
            commentMapper.yinzhang_pei(id);
        }else if(CommentService.KEY_PEN_ZI.equals(type)){
            commentMapper.yinzhang_penzi(id);
        }else if(CommentService.KEY_DOU.equals(type)){
            commentMapper.yinzhang_dou(id);
        }else if(CommentService.KEY_GEI_LI.equals(type)){
            commentMapper.yinzhang_geili(id);
        }
    }

}
