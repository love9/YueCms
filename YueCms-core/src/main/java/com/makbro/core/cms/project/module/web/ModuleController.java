package com.makbro.core.cms.project.module.web;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.project.module.bean.Module;
import com.makbro.core.cms.project.module.service.ModuleService;
import com.makbro.core.cms.project.project.bean.Project;
import com.makbro.core.cms.project.project.service.ProjectService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.LoginBean;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


/**
 * 项目模块管理
 * @author wujiyue
 */

@Controller
@RequestMapping("/cms/project/module")
public class ModuleController extends BaseController {
    @Autowired
    protected ModuleService moduleService;

    @RequestMapping(value={"","/"})
    public String index(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        LoginBean lb= MyBatisRequestUtil.getLoginUserInfo(request);
        pushLoginUserInfo(model);
        map.put("parentid","0");
        List<Module> modules=moduleService.find(new PageBounds(),map);
        if(modules!=null&&modules.size()>0){
            model.addAttribute("projectName",modules.get(0).getName());
            model.addAttribute("parentid",modules.get(0).getId());
        }else {
            //提示错误
            model.addAttribute("type","error");
            model.addAttribute("msg","操作非法！");
        }
        model.addAttribute("projectId",String.valueOf(map.get("projectId")));
        model.addAttribute("projectName",String.valueOf(map.get("projectName")));
        return "/cms/project/module/list";
    }
    @Autowired
    ProjectService projectService;
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Module module, Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        model.addAttribute("projectId",String.valueOf(map.get("projectId")));
        Project p=projectService.get(Integer.valueOf(String.valueOf(map.get("projectId"))));
        if(p!=null){
            model.addAttribute("projectName",p.getName());
        }
        model.addAttribute("parentid",String.valueOf(map.get("parentid")));
        model.addAttribute("parentname",String.valueOf(map.get("parentname")));
        return "/cms/project/module/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Module module, Model model){
        if(module!=null&&module.getId()!=null){
            module=moduleService.get(module.getId());
        }
         model.addAttribute("module",module);
         return "/cms/project/module/edit";
    }
    //-----------json数据接口--------------------
    
    
    
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return moduleService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return moduleService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(Module m, Model model) {
        moduleService.add(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Module m) {
        moduleService.update(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return moduleService.save(map);
    }





    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable String id) {
    	Msg msg=new Msg();
    	try{

			int count=moduleService.getChildrenCount(id);
			if(count>0){
				msg.setType(Msg.MsgType.error);
				msg.setContent("删除的记录下不能有子记录！");
				return msg;
			}
            moduleService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
        		msg.setType(Msg.MsgType.error);
        		msg.setContent("删除失败！");
        }
        return msg;
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
    	try{
    	     
			String ids_str= StringUtil.arrToString(ids, ",");
			int count=moduleService.getChildrenCount(ids_str);
			if(count>0){
				msg.setType(Msg.MsgType.error);
				msg.setContent("删除的记录下不能有子记录！");
				return msg;
			}
             moduleService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }
    
	@ResponseBody
	@RequestMapping("/json/findByParentid")
	public Object findByParentid() {
        Map map=MyBatisRequestUtil.getMap(request);
		return moduleService.findByParentid(map);
	}
    @ResponseBody
    @RequestMapping("/json/ztree")
    public Object ztree() {
        Map map=MyBatisRequestUtil.getMap(request);
        return moduleService.ztree(map);
    }

    @ResponseBody
    @RequestMapping("/json/select")
    public Object select() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        resultMap.put("moduleSelect", moduleService.select(map));
        return resultMap;
    }
}
