package com.makbro.core.cms.spider.core.processor.example;


import com.makbro.core.cms.spider.core.Page;
import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Site;
import com.makbro.core.cms.spider.core.Spider;
import com.makbro.core.cms.spider.core.processor.PageProcessor;

/**
 * @author code4crafter@gmail.com <br>
 * @since 0.4.0
 */
public class BaiduBaikePageProcessor implements PageProcessor {

    private Site site = Site.me()//.setHttpProxy(new HttpHost("127.0.0.1",8888))
            .setRetryTimes(3).setSleepTime(1000).setUseGzip(true);

    @Override
    public void process(Page page) {
        System.out.println(page.getHtml());
        page.putField("name", page.getHtml().css("dl.lemmaWgt-lemmaTitle h1","text").toString());
        page.putField("description", page.getHtml().xpath("//div[@class='lemma-summary']/allText()"));
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        //single download
        Spider spider = Spider.create(new BaiduBaikePageProcessor()).thread(2);
        String urlTemplate = "http://baike.baidu.com/search/word2?word2=%s&pic=1&sug=1&enc=utf8";
        urlTemplate="https://baike.baidu.com/item/";
        ResultItems resultItems = spider.<ResultItems>get("https://www.baidu.com/link?url=aowsJY9qAhlPgw_1zt1kSaHjd-skWsGHMLPrH8MKTaKKskMacbEIDb71z8hbwSUy46wK7Zf649Gha4m8IAtzFn8kYRpJD4KrunrzQ2d3Tpy&wd=&eqid=d645a4b30000e153000000025c358a22");
        System.out.println(resultItems);

        //multidownload
       /* List<String> list = new ArrayList<String>();
        list.add(String.format(urlTemplate,"风力发电"));
        list.add(String.format(urlTemplate,"太阳能"));
        list.add(String.format(urlTemplate,"地热发电"));
        list.add(String.format(urlTemplate,"地热发电"));
        List<ResultItems> resultItemses = spider.<ResultItems>getAll(list);
        for (ResultItems resultItemse : resultItemses) {
            System.out.println(resultItemse.getAll());
        }*/
        spider.close();
    }
}
