package com.makbro.core.cms.spider.pageProcessMx.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.spider.pageProcessMx.bean.PageProcessMx;
import com.makbro.core.cms.spider.pageProcessMx.dao.PageProcessMxMapper;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 页面字段配置 Service
 * @author  wujiyue on 2017-11-16 11:07:49.
 */
@Service
public class PageProcessMxService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private PageProcessMxMapper pageProcessMxMapper;

     /*基础公共方法*/
    public PageProcessMx get(Integer id){
        return pageProcessMxMapper.get(id);
    }

    public List<PageProcessMx> find(PageBounds pageBounds,Map<String,Object> map){
        return pageProcessMxMapper.find(pageBounds,map);
    }
    public List<PageProcessMx> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return pageProcessMxMapper.findByMap(pageBounds,map);
    }

    public void add(PageProcessMx pageProcessMx){
        pageProcessMxMapper.add(pageProcessMx);
    }
    public Object save(Map<String,Object> map){
        PageProcessMx pageProcessMx = JSON.parseObject(JSON.toJSONString(map),PageProcessMx.class);
        if(pageProcessMx.getId()==null||"".equals(pageProcessMx.getId().toString())){
               pageProcessMx.setParentid(0);
               pageProcessMxMapper.add(pageProcessMx);
        }else{
               pageProcessMxMapper.update(pageProcessMx);
        }
        Integer[] delArr = new Integer[1];
        delArr[0] = pageProcessMx.getId();
        pageProcessMxMapper.deleteByParentid(delArr);
        String mxs=String.valueOf(map.get("mxs"));
        if(StringUtil.notEmpty(mxs)){
            mxs=mxs.replaceAll("line_","");
            List<PageProcessMx> mxlist= (List<PageProcessMx>)JSONArray.toCollection(JSONArray.fromObject(mxs), PageProcessMx.class);//转化为数组
            for (PageProcessMx mx : mxlist) {
                mx.setParentid(pageProcessMx.getId());
                mx.setZbguid(pageProcessMx.getZbguid());
                mx.setDatatype("text");
                if(mx.getId()==null||"".equals(mx.getId().toString())){
                    pageProcessMx.setParentid(0);
                }
                pageProcessMxMapper.add(mx);
            }
        }
        return Msg.success("保存信息成功");
    }
    public void addBatch(List<PageProcessMx> pageProcessMxs){
        pageProcessMxMapper.addBatch(pageProcessMxs);
    }

    public void update(PageProcessMx pageProcessMx){
        pageProcessMxMapper.update(pageProcessMx);
    }

    public void updateByMap(Map<String,Object> map){
        pageProcessMxMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        pageProcessMxMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        pageProcessMxMapper.delete(id);
    }
    @Transactional
    public void deleteBatch(Integer[] ids){
        pageProcessMxMapper.deleteBatch(ids);
        pageProcessMxMapper.deleteByParentid(ids);
    }
}
