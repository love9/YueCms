package com.makbro.core.cms.project.mission.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.project.mission.bean.Mission;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 项目任务 dao
 * @author wujiyue
 */
@Repository
public interface MissionMapper{

    public Mission get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Mission mission);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Mission> missions);
    public void update(Mission mission);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Mission> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Mission> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
