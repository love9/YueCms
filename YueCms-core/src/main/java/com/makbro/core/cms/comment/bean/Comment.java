package com.makbro.core.cms.comment.bean;


import com.markbro.base.model.AliasModel;

/**
 * 评论 bean
 * @author  wujiyue on 2017-12-26 09:14:38 .
 */
public class Comment  implements AliasModel {

	private Integer id;//主键
	private Integer parentid;//父ID,有值说明是评论的评论
	private String target_id;//外键。指向文章id等评论对象
	private String title;//标题
	private Integer num_pei;//印章个数
	private Integer num_penzi;//印章个数
	private Integer num_dou;//印章个数
	private Integer num_geili;//给力印章个数
	private String type;//评论对象类型
	private String yhid;//评论用户id
	private String username;//评论用户的名称
	private String usericon;//用户头像
	private String comment;//评论内容
	private Integer up_vote;//支持数
	private Integer down_vote;//反对数
	private String createTime;//
	private Integer available;//
	private Integer deleted;//


	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getTarget_id(){ return target_id ;}
	public void  setTarget_id(String target_id){this.target_id=target_id; }
	public String getTitle(){ return title ;}
	public void  setTitle(String title){this.title=title; }
	public String getType(){ return type ;}
	public void  setType(String type){this.type=type; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getUsername(){ return username ;}
	public void  setUsername(String username){this.username=username; }
	public String getUsericon(){ return usericon ;}
	public void  setUsericon(String usericon){this.usericon=usericon; }
	public String getComment(){ return comment ;}
	public void  setComment(String comment){this.comment=comment; }
	public Integer getUp_vote(){ return up_vote ;}
	public void  setUp_vote(Integer up_vote){this.up_vote=up_vote; }
	public Integer getDown_vote(){ return down_vote ;}
	public void  setDown_vote(Integer down_vote){this.down_vote=down_vote; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public Integer getNum_pei() {
		return num_pei;
	}

	public void setNum_pei(Integer num_pei) {
		this.num_pei = num_pei;
	}

	public Integer getNum_penzi() {
		return num_penzi;
	}

	public void setNum_penzi(Integer num_penzi) {
		this.num_penzi = num_penzi;
	}

	public Integer getNum_dou() {
		return num_dou;
	}

	public void setNum_dou(Integer num_dou) {
		this.num_dou = num_dou;
	}

	public Integer getNum_geili() {
		return num_geili;
	}

	public void setNum_geili(Integer num_geili) {
		this.num_geili = num_geili;
	}

	@Override
	public String toString() {
		return "Comment{" +
				"id=" + id +
				", parentid=" + parentid +
				", target_id='" + target_id + '\'' +
				", num_pei=" + num_pei +
				", num_penzi=" + num_penzi +
				", num_dou=" + num_dou +
				", num_geili=" + num_geili +
				", type='" + type + '\'' +
				", yhid='" + yhid + '\'' +
				", username='" + username + '\'' +
				", usericon='" + usericon + '\'' +
				", comment='" + comment + '\'' +
				", up_vote=" + up_vote +
				", down_vote=" + down_vote +
				", createTime='" + createTime + '\'' +
				", available=" + available +
				", deleted=" + deleted +
				'}';
	}
}
