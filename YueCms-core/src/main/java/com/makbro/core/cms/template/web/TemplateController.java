package com.makbro.core.cms.template.web;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.service.TemplateService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * 模板管理管理
 * @author  wujiyue on 2017-11-10 10:07:06.
 */

@Controller
@RequestMapping("/cms/template")
public class TemplateController extends BaseController {
    @Autowired
    protected TemplateService templateService;


    /**
     * 预览模板
     */
    @RequestMapping(value = "/preView")
    public String preView(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String id=String.valueOf(map.get("id"));
        Template template=templateService.get(id);
        String path=template.getFilePath();
        //String prefix= GlobalConfig.getConfig("template.path.prefix");
        //prefix=StringUtil.subEndStr(prefix,"/");
        //path=path.replaceAll(prefix,"");
        String suffix= GlobalConfig.getConfig("template.path.suffix");
        path=path.replaceAll(suffix,"");//移除后缀
        return path;
    }

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/template/list";
    }


    @ResponseBody
    @RequestMapping(value="/json/editContent",method = RequestMethod.POST)
    public Object editContent() throws Exception{
        Map map=MyBatisRequestUtil.getMap(request);
        String id=String.valueOf(map.get("id"));
        if(StringUtil.isEmpty(id)){
            return "";
        }
        Template template=templateService.get(id);
        if(template!=null){
            return template.getContent();
        }
        return "";
    }

    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Template template,Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        //model.addAttribute("parentid",map.get("parentid"));
        //model.addAttribute("parentname",map.get("parentname"));
        return "/cms/template/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Template template,Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        if(template!=null&&template.getId()!=null){
            template=templateService.get(template.getId());
        }
        //model.addAttribute("parentid",map.get("parentid"));
        //model.addAttribute("parentname",map.get("parentname"));
        String prefix= GlobalConfig.getConfig("template.path.prefix");
        String suffix= GlobalConfig.getConfig("template.path.suffix");
        String fileName=template.getFileName();
        if(StringUtil.notEmpty(fileName)){
            template.setFileName(fileName.replaceAll(suffix,""));
        }
        String filePath=template.getFilePath();
        if(StringUtil.notEmpty(filePath)){
            template.setFilePath(filePath.replaceAll(suffix,"").replaceAll(prefix,""));
        }
        model.addAttribute("template",template);
        return "/cms/template/edit";
    }
    /**
     * 跳转到编辑内容页面
     */
    @RequestMapping(value = "/editContent")
    public String toEditContent(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String id=String.valueOf(map.get("id"));
        Template template=templateService.get(id);
        model.addAttribute("template",template);
        return "/cms/template/editContent";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return templateService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        Map map=MyBatisRequestUtil.getMap(request);
        return templateService.find(getPageBounds(),map);
    }



    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return templateService.save(map);
    }

    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            templateService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            templateService.updateByMapBatch(map);
            return Msg.success("批量删除成功！");
        }catch (Exception e){
            return Msg.error("批量删除失败！");
        }

	}


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable String id) {
    	try{
            templateService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable String[] ids) {
    	try{
            templateService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
}