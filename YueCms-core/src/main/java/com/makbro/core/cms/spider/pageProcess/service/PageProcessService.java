package com.makbro.core.cms.spider.pageProcess.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.spider.pageProcess.bean.PageProcess;
import com.makbro.core.cms.spider.pageProcess.dao.PageProcessMapper;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 页面提取配置 Service
 * @author  wujiyue on 2017-11-14 13:44:58.
 */
@Service
public class PageProcessService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private PageProcessMapper pageProcessMapper;

     /*基础公共方法*/
    public PageProcess get(Integer id){
        return pageProcessMapper.get(id);
    }

    public List<PageProcess> find(PageBounds pageBounds,Map<String,Object> map){
        return pageProcessMapper.find(pageBounds,map);
    }
    public List<PageProcess> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return pageProcessMapper.findByMap(pageBounds,map);
    }

    public void add(PageProcess pageProcess){
        pageProcessMapper.add(pageProcess);
    }
    public Object save(Map<String,Object> map){
           PageProcess pageProcess= JSON.parseObject(JSON.toJSONString(map),PageProcess.class);
           if(pageProcess.getId()==null||"".equals(pageProcess.getId().toString())){
               Integer id= keyService.getIntegerId();
               pageProcess.setId(id);
               pageProcessMapper.add(pageProcess);
           }else{
               pageProcessMapper.update(pageProcess);
           }
           return Msg.success("保存信息成功!");
    }
    public void addBatch(List<PageProcess> pageProcesss){
        pageProcessMapper.addBatch(pageProcesss);
    }

    public void update(PageProcess pageProcess){
        pageProcessMapper.update(pageProcess);
    }

    public void updateByMap(Map<String,Object> map){
        pageProcessMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        pageProcessMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        pageProcessMapper.delete(id);
    }
    public void deleteBatch(Integer[] ids){
        pageProcessMapper.deleteBatch(ids);
    }
}
