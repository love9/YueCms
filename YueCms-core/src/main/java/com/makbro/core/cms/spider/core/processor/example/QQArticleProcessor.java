package com.makbro.core.cms.spider.core.processor.example;


import com.makbro.core.cms.spider.core.Page;
import com.makbro.core.cms.spider.core.Site;
import com.makbro.core.cms.spider.core.Spider;
import com.makbro.core.cms.spider.core.processor.PageProcessor;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author code4crafter@gmail.com <br>
 * @since 0.6.0
 */
public class QQArticleProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(500);

    @Override
    public void process(Page page) {

      //System.out.println("ssssssssssssssssssssssssss=========="+page.getHtml().toString());
      //  System.out.println("ssssssssssssssssssssssssss=========="+page.getRawText());
      List < String > urls = page.getHtml().links().all();
        urls=urls.stream().filter(s->{return  s.contains("htm_data");}).skip(2).limit(2).collect(Collectors.toList());
        urls.forEach(s->{
            System.out.println(s);
        });
       page.addTargetRequests(urls);

        page.putField("title", page.getHtml().xpath("//h1[@id='subject_tpc']/text()").toString());
        page.putField("imgs", page.getHtml().css("div.tpc_content img", "src").all());
        if (page.getResultItems().get("title")==null||((List<String>)page.getResultItems().get("imgs")).size()<5){
            //skip this page
            page.setSkip(true);
        }

        /*   List < String > urls = page.getHtml().links().all();
       // urls=urls.stream().filter(s->{return  s.contains("htm_data");}).limit(2).collect(Collectors.toList());
        urls.forEach(s->{
            System.out.println(s);
        });
     page.addTargetRequests(urls);

        page.putField("title", page.getHtml().xpath("//h1[@id='subject_tpc']/text()").toString());
        page.putField("imgs", page.getHtml().css("div.tpc_content img", "src").all());
        if (page.getResultItems().get("title")==null||((List<String>)page.getResultItems().get("imgs")).size()<5){
            //skip this page
            page.setSkip(true);
        }*/
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws IOException {
     Spider.create(new QQArticleProcessor()).addUrl("http://n2.lufi99.club/pw/htm_data/15/1806/1209319.html").run();
      // Spider.create(new QQArticleProcessor()).addUrl("http://1024.hlork9.rocks/pw/thread.php?fid=15&page=1").addPipeline(new FilePipeline()).run();
        //  String s= CaptureHtmlUtil.captureHtml("http://d2.sku117.biz/pw/thread.php?fid=78");
        //   System.out.println(s);
    }
}
