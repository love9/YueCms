package com.makbro.core.cms.blog.task;


import com.makbro.core.cms.blog.dao.BlogManageMapper;
import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.common.util.thread.MultiThreadHandler;
import com.markbro.base.common.util.thread.exception.ChildThreadException;
import com.markbro.base.common.util.thread.parallel.ParallelTaskWithThreadPool;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.date.DateUtil;
import net.sf.ehcache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;


/**
 * @author wjy
 * @date 2018年9月20日09:42:44
 */
@Component
class BlogTask implements InitCacheListener {
    @Autowired
    BlogManageMapper blogManageMapper;


    /**
     *  每过30分钟，把计入缓存的博主访问次数累计到数据库表中并且放入缓存
     */
   @Scheduled(cron="0 0/60 * * * ? ")//60分钟执行一次
    public void visitCountTask(){
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountTask===>Start");
        String[] yhids=getYhidsFromCache();
        runVisitCountTask(yhids);
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountTask===>End！");

    }

    private String[] getYhidsFromCache(){

        Cache cache =null;
        cache = EhCacheUtils.getCacheManager().getCache(EhCacheUtils.USER_CACHE);
        String prefix=UpdateBlogAuthorInfoTask.KEY_BLOG_VISIT_COUNT+"_";
        List<String> keysList = cache.getKeys();
        keysList=keysList.stream().filter(s->{return s.contains(prefix);}).collect(Collectors.toList());
        int count=keysList.size();
        String[] yhids=new String[count];
        int i=0;
        for(String s:keysList){
            yhids[i]=s.replaceAll(prefix,"");
            i++;
        }
        return yhids;
    }
    private void runVisitCountTask(String... yhids){

        Map<String, Object> resultMap = new HashMap<String, Object>(8, 1);
        //MultiThreadHandler handler = new MultiParallelThreadHandler();
        ExecutorService service = Executors.newFixedThreadPool(3);
        MultiThreadHandler handler = new ParallelTaskWithThreadPool(service);
        UpdateBlogAuthorInfoTask task = null;
        // 启动5个子线程作为要处理的并行任务，共同完成结果集resultMap
        for(String yhid:yhids){
            task = new UpdateBlogAuthorInfoTask(yhid, resultMap);
            handler.addTask(task);
        }
        try {
            handler.run();
        } catch (ChildThreadException e) {
            System.out.println(e.getAllStackTraceMessage());
        }
        System.out.println(resultMap);
        service.shutdown();
    }

    /**
     * 每天00:00执行一次，把visit_today放到visit_yesterday.visit_today置零
     */
    @Scheduled(cron="0 0 0 * * ?")
    public void visitCountDayTask(){
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountDayTask===>Start");
        Integer rows=blogManageMapper.updateVisitNumDay();
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountDayTask===>End！"+rows+" rows matched ！");
    }

    /**
     *每周一零点执行一次。把visit_week置零
     */
    @Scheduled(cron="0 0 0 ? * MON")
    public void visitCountWeekTask(){
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountWeekTask===>Start");
        Integer rows=blogManageMapper.updateVisitNumWeek();
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountWeekTask===>End！"+rows+" rows matched ！");
    }

    /**
     * 每月1号零点执行一次
     */
    @Scheduled(cron="0 0 0 1 * ?")
    public void visitCountMonthTask(){
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountMonthTask===>Start");
        Integer rows=blogManageMapper.updateVisitNumMonth();
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountMonthTask===>End！"+rows+" rows matched ！");
    }


    /**
     * 该方法是在系统启动后，初始化缓存.
     */
    @Override
    public void onInit() {

    }
}
