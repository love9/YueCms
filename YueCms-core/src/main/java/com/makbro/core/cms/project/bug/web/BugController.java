package com.makbro.core.cms.project.bug.web;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.cms.project.bug.bean.Bug;
import com.makbro.core.cms.project.bug.service.BugService;
import com.makbro.core.cms.project.project.bean.Project;
import com.makbro.core.cms.project.project.service.ProjectService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.LoginBean;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 项目Bug管理
 * @author wujiyue
 */

@Controller
@RequestMapping("/cms/project/bug")
public class BugController extends BaseController {
    @Autowired
    protected BugService bugService;
    @Autowired
    protected OrgTreeMapper ogTreeMapper;
    @Autowired
    protected ProjectService projectService;
    @RequestMapping(value={"","/"})
    public String index(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        Project p=projectService.get(Integer.valueOf((String.valueOf(map.get("projectId")))));
        if(p!=null){
            model.addAttribute("projectId",p.getId());
            model.addAttribute("projectName",p.getName());
        }else{
            //提示错误
            model.addAttribute("type","error");
            model.addAttribute("msg","操作非法！");
        }
        return "/cms/project/bug/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Bug bug, Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        LoginBean lb=MyBatisRequestUtil.getLoginUserInfo(request);
        pushLoginUserInfo(model);
        Project p=projectService.get(Integer.valueOf((String.valueOf(map.get("projectId")))));
        if(p!=null){
            model.addAttribute("projectId",p.getId());
            model.addAttribute("projectName",p.getName());
        }else{
            //提示错误
            model.addAttribute("type","error");
            model.addAttribute("msg","操作非法！");
        }
        return "/cms/project/bug/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Bug bug, Model model){
        if(bug!=null&&bug.getId()!=null){
            bug=bugService.get(bug.getId());
        }
        String assignTo=bug.getAssignTo();
        assignTo= StringUtil.subEndStr(assignTo,";");
        assignTo=assignTo.replaceAll(";","','");
        List<Map<String,Object>> ls=ogTreeMapper.getYhByYhids(assignTo);
        String assignToNames="";
        if(ls!=null&&ls.size()>0){
            for(Map<String,Object> t:ls){
                assignToNames+=String.valueOf(t.get("account"))+"("+String.valueOf(t.get("realname"))+");";
            }
        }
        model.addAttribute("assignToNames",assignToNames);
         model.addAttribute("bug",bug);
        Project p=projectService.get(bug.getProjectId());
        if(p!=null){
            //model.addAttribute("projectId",p.getId());
            model.addAttribute("projectName",p.getName());
        }

         return "/cms/project/bug/edit";
    }
    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return bugService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return bugService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }
    /**
     * 不分页查询数据
     */
    @ResponseBody
    @RequestMapping("/json/findAll")
    public Object findAll() {
        Map map=MyBatisRequestUtil.getMap(request);
        String sortString=String.valueOf(map.get("sortString"));
        PageBounds pageBounds=null;
        if(StringUtil.notEmpty(sortString)){
             pageBounds=new PageBounds(Order.formString(sortString));
        }else{
             pageBounds=new PageBounds();
        }
        resultMap=getPageMap(bugService.find(pageBounds,map));
        return resultMap;
    }

    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(Bug m) {

        bugService.add(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Bug m) {
        bugService.update(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return bugService.save(map);
    }





    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	Msg msg=new Msg();
    	try{

            bugService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
        		msg.setType(Msg.MsgType.error);
        		msg.setContent("删除失败！");
        }
        return msg;
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
    	try{
    	     
             bugService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }
}