package com.makbro.core.cms.article.web;


import com.makbro.core.cms.article.service.StaticService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class StaticController extends BaseController {

    @Autowired
    StaticService staticService;
    @RequestMapping("/sys/static/tag/{id}")
    @RequiresRoles("system")
    @ResponseBody
    public Object staticArticlesByTag(@PathVariable String id){
        Map map=new HashMap();
        return staticService.staticArticlesByTag(map,id,6);
    }

}
