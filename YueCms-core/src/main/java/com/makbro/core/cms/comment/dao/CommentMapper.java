package com.makbro.core.cms.comment.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.comment.bean.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 评论 dao
 * @author wujiyue on 2017-12-26 09:14:39.
 */
@Repository
public interface CommentMapper{

    public Comment get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Comment comment);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Comment> comments);
    public void update(Comment comment);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);

    public List<Comment> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Comment> findByMap(PageBounds pageBounds, Map<String, Object> map);

    public void upVote(Integer id);
    public void downVote(Integer id);
    public void yinzhang_penzi(Integer id); //喷子印章数+1
    public void yinzhang_dou(Integer id); //逗章数+1
    public void yinzhang_pei(Integer id); //呸印章数+1
    public void yinzhang_geili(Integer id); //给力印章数+1

}
