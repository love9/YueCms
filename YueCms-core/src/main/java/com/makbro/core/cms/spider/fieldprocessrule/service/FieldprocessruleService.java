package com.makbro.core.cms.spider.fieldprocessrule.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.spider.fieldprocessrule.bean.Fieldprocessrule;
import com.makbro.core.cms.spider.fieldprocessrule.dao.FieldprocessruleMapper;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 字段处理规则 Service
 * @author  wujiyue on 2017-11-18 15:20:49.
 */
@Service
public class FieldprocessruleService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private FieldprocessruleMapper fieldprocessruleMapper;

     /*基础公共方法*/
    public Fieldprocessrule get(Integer id){
        return fieldprocessruleMapper.get(id);
    }

    public List<Fieldprocessrule> find(PageBounds pageBounds,Map<String,Object> map){
        return fieldprocessruleMapper.find(pageBounds,map);
    }
    public List<Fieldprocessrule> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return fieldprocessruleMapper.findByMap(pageBounds,map);
    }

    public void add(Fieldprocessrule fieldprocessrule){
        fieldprocessruleMapper.add(fieldprocessrule);
    }
    public Object save(Map<String,Object> map){
           Fieldprocessrule fieldprocessrule= JSON.parseObject(JSON.toJSONString(map),Fieldprocessrule.class);
           if(fieldprocessrule.getId()==null||"".equals(fieldprocessrule.getId().toString())){
               Integer id= keyService.getIntegerId();
               fieldprocessrule.setId(id);
               fieldprocessruleMapper.add(fieldprocessrule);
           }else{
               fieldprocessruleMapper.update(fieldprocessrule);
           }
           return Msg.success("保存信息成功!");
    }
    public void addBatch(List<Fieldprocessrule> fieldprocessrules){
        fieldprocessruleMapper.addBatch(fieldprocessrules);
    }

    public void update(Fieldprocessrule fieldprocessrule){
        fieldprocessruleMapper.update(fieldprocessrule);
    }

    public void updateByMap(Map<String,Object> map){
        fieldprocessruleMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        fieldprocessruleMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        fieldprocessruleMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        fieldprocessruleMapper.deleteBatch(ids);
    }
     /*自定义方法*/
     public List<Fieldprocessrule> findByIds(String ids){
         ids=ids.replaceAll(",","','");
        return fieldprocessruleMapper.findByIds(ids);
     }
}
