package com.makbro.core.cms.tag.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.tag.bean.Tags;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 标签 dao
 * @author  wujiyue on 2018-07-06 15:42:13.
 */
@Repository
public interface TagsMapper{

    public Tags get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Tags tags);
    public void addByMap(Map<String, Object> map);
    public void update(Tags tags);
    public void updateByMap(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);

    public List<Tags> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Map<String,Object>> findByMap(PageBounds pageBounds, Map<String, Object> map);

    public int updateSort(@Param("id") Integer id, @Param("sort") String sort);
	public Integer getMaxSort();

    public Tags getByName(@Param("tag_type") String tag_type, @Param("name") String name);
}
