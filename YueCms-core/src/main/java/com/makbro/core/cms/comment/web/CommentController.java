package com.makbro.core.cms.comment.web;

import com.makbro.core.cms.comment.bean.Comment;
import com.makbro.core.cms.comment.service.CommentService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 评论管理
 * @author wjy
 */

@Controller
@RequestMapping("/cms/comment")
public class CommentController extends BaseController {
    @Autowired
    protected CommentService commentService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/comment/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Comment comment, Model model){
                return "/cms/comment/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Comment comment, Model model){
        if(comment!=null&&comment.getId()!=null){
            comment=commentService.get(comment.getId());
        }
         model.addAttribute("comment",comment);
         return "/cms/comment/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return commentService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        Map map= MyBatisRequestUtil.getMap(request);
        String myblog=(String)map.get("myblog");
        if(TmConstant.NUM_ONE.equals(myblog)){
            String book_id=String.valueOf(map.get("book_id"));
            String chapter_id=String.valueOf(map.get("chapter_id"));
            if(StringUtil.notEmpty(chapter_id)){
                map.put("type","chapter");
                map.put("target_id",chapter_id);
            }else if(StringUtil.notEmpty(book_id)){
                map.put("type","book");
                map.put("target_id",book_id);
            }else{
                map.put("type","article");
                map.put("target_id", String.valueOf(map.get("id")));
            }

            map.put("id","");
            return commentService.find(getPageBounds(),map);
        }
        return commentService.find(getPageBounds(),map);
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return commentService.save(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable Integer id){
	Msg msg=new Msg();
	try{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		map.put("id",id);
		commentService.updateByMap(map);
		msg.setType(Msg.MsgType.success);
		msg.setContent("删除成功！");
	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("删除失败！");
	}
	return msg;
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable Integer[] ids){

        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            commentService.updateByMapBatch(map);
            return Msg.success("批量删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }

	}


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	try{
            commentService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {
    	try{
             commentService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }

    /**
     * 评论的支持和反对投票
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/json/votes", method = RequestMethod.POST)
    public Object commentUpDownVote(){
        Map map=MyBatisRequestUtil.getMap(request);
        try{
            String id=String.valueOf(map.get("id"));
            Integer tarId=Integer.valueOf(id);
            String type=String.valueOf(map.get("type"));
            if(StringUtil.isEmpty(id)||(!CommentService.KEY_UP.equals(type)&&!CommentService.KEY_DOWN.equals(type))){
                return Msg.error("请求非法！");
            }
            commentService.votes(tarId,type);
            return Msg.success(CommentService.KEY_UP.equals(type)?"支持成功!":"反对成功!");
        }catch (Exception ex){
            logger.error("commentUpDownVote出现异常!请求参数:yhid:{},评论id:{},评论类型{}",map.get(TmConstant.YHID_KEY),map.get("id"),map.get("type"));

            ex.printStackTrace();
            return Msg.error("请求出现异常！");
        }

    }
    /**
     * 给评论盖印章
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/json/yinzhang", method = RequestMethod.POST)
    public Object yinzhang(){
        Map map=MyBatisRequestUtil.getMap(request);
        try{
            String id=String.valueOf(map.get("id"));
            Integer tarId=Integer.valueOf(id);
            String type=String.valueOf(map.get("type"));
            if(StringUtil.isEmpty(id)||(!CommentService.KEY_PEI.equals(type)&&!CommentService.KEY_DOU.equals(type)&&!CommentService.KEY_PEN_ZI.equals(type)&&!CommentService.KEY_GEI_LI.equals(type))){
                return Msg.error("请求非法！");
            }
            commentService.yinzhang(tarId,type);
            return Msg.success("操作成功!");
        }catch (Exception ex){
            logger.error("yinzhang出现异常!请求参数:yhid:{},target_id:{},盖章类型{}",map.get(TmConstant.YHID_KEY),map.get("id"),map.get("type"));
            ex.printStackTrace();
            return Msg.error("请求出现异常！");
        }
    }
}