package com.makbro.core.cms.article.web;


import com.makbro.core.cms.article.service.CleanDataService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CleanDataController  extends BaseController {

    @Autowired
    CleanDataService cleanDataService;
    @RequestMapping("/sys/clean/mission_id/{id}")
    @RequiresRoles("system")
    public void cleanArticleByMissionId(@PathVariable String id){
        cleanDataService.cleanDataByMissionId(id);
    }

}
