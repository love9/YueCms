package com.makbro.core.cms.project.mission.bean;


import com.markbro.base.model.AliasModel;

/**
 * 项目任务 bean
 * @author wujiyue
 */
public class Mission  implements AliasModel {

	private Integer id;//项目编号
	private Integer projectId;//项目ID
	private String moduleId;//模块ID
	private String name;//任务名称
	private Object content;//任务内容
	private Integer missiontype;//任务类型
	private Integer priority;//完成优先级。优先级比较高的排在上面。
	private String starttime;//开始时间
	private String endtime;//结束时间
	private String copyto;//任务抄送给
	private String status;//任务状态
	private String attachfiles;//附件ids
	private String assignTo;//任务指派给。多人用逗号分割
	private String assignToNames;//
	private String createBy;//创建者
	private String finishBy;//完成者
	private String estimateWorkday;//预估工作日。0.25，0.5，1，1.5等

	public String getAssignToNames() {
		return assignToNames;
	}

	public void setAssignToNames(String assignToNames) {
		this.assignToNames = assignToNames;
	}

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public Integer getProjectId(){ return projectId ;}
	public void  setProjectId(Integer projectId){this.projectId=projectId; }
	public String getModuleId(){ return moduleId ;}
	public void  setModuleId(String moduleId){this.moduleId=moduleId; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public Object getContent(){ return content ;}
	public void  setContent(Object content){this.content=content; }
	public Integer getMissiontype(){ return missiontype ;}
	public void  setMissiontype(Integer missiontype){this.missiontype=missiontype; }
	public Integer getPriority(){ return priority ;}
	public void  setPriority(Integer priority){this.priority=priority; }
	public String getStarttime(){ return starttime ;}
	public void  setStarttime(String starttime){this.starttime=starttime; }
	public String getEndtime(){ return endtime ;}
	public void  setEndtime(String endtime){this.endtime=endtime; }
	public String getCopyto(){ return copyto ;}
	public void  setCopyto(String copyto){this.copyto=copyto; }
	public String getStatus(){ return status ;}
	public void  setStatus(String status){this.status=status; }
	public String getAttachfiles(){ return attachfiles ;}
	public void  setAttachfiles(String attachfiles){this.attachfiles=attachfiles; }
	public String getAssignTo(){ return assignTo ;}
	public void  setAssignTo(String assignTo){this.assignTo=assignTo; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getFinishBy(){ return finishBy ;}
	public void  setFinishBy(String finishBy){this.finishBy=finishBy; }
	public String getEstimateWorkday(){ return estimateWorkday ;}
	public void  setEstimateWorkday(String estimateWorkday){this.estimateWorkday=estimateWorkday; }


	@Override
	public String toString() {
	return "Mission{" +
			"id=" + id+
			", projectId=" + projectId+
			", moduleId=" + moduleId+
			", name=" + name+
			", content=" + content+
			", missiontype=" + missiontype+
			", priority=" + priority+
			", starttime=" + starttime+
			", endtime=" + endtime+
			", copyto=" + copyto+
			", status=" + status+
			", attachfiles=" + attachfiles+
			", assignTo=" + assignTo+
			", createBy=" + createBy+
			", finishBy=" + finishBy+
			", estimateWorkday=" + estimateWorkday+
			 '}';
	}
}
