package com.makbro.core.cms.jobs;

import com.makbro.core.cms.book.bean.Book;
import com.makbro.core.cms.book.dao.BookMapper;
import com.makbro.core.cms.spider.core.processor.service.PageProcessMissonService;
import com.makbro.core.framework.quartz.jobs.BaseJob;
import com.markbro.base.utils.SpringContextHolder;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.List;

/**
 * @description 定时趴取cms_book表中还未趴取章节内容（missionCode不为空，status=0）的书籍。id和link分别对应了趴取任务表的ref_id和urls(入口地址)
 * @author wjy
 * @date 2018年10月6日14:42:06
 */
public class BookChapterSpiderJob extends BaseJob {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);
        BookMapper bookMapper = SpringContextHolder.getBean("bookMapper");
        List<Book> books = bookMapper.findForSpider();//cms_book表spiderMissionCode不为空并且status=0
        if(CollectionUtils.isNotEmpty(books)){
            //本次处理一个
            Book book=books.get(0);
            if(book!=null&& StringUtil.notEmpty(book.getSpiderMissionCode())&&StringUtil.notEmpty(book.getLink())){
                PageProcessMissonService pageProcessMissonService= SpringContextHolder.getBean("pageProcessMissonService");
                super.execute(jobExecutionContext);
                System.out.println(">>>>>>>>>>>>>>开始趴取书籍章节>>>>>>>>>>>>>>");
                pageProcessMissonService.spiderByMissionCode(book.getSpiderMissionCode(),book.getId(),book.getLink(),book.getBookcode());
            }
        }
    }
}
