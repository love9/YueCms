package com.makbro.core.cms.spider.core.processor.example;


import com.makbro.core.cms.spider.core.*;
import com.makbro.core.cms.spider.core.pipeline.FilePipeline;
import com.makbro.core.cms.spider.core.processor.PageProcessor;

import java.util.List;

/**
 * @author code4crafter@gmail.com <br>
 * @since 0.6.0
 */
public class CnblogsProcessor implements PageProcessor, SpiderListener {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(1500);

    @Override
    public void process(Page page) {

        if (page.getUrl().regex("^https://www\\.cnblogs\\.com$").match()) {//爬取第一页
             try {
                 page.addTargetRequests(page.getHtml().xpath("//*[@id=\"post_list\"]/div/div[@class='post_item_body']/h3/a/@href").all());
                 pageNum++;
                 page.addTargetRequest("https://www.cnblogs.com/#p2");


             } catch (Exception e) {
                 e.printStackTrace();
             }
        } else if (page.getUrl().regex(PAGE_LIST).match() && pageNum <= 2) {//爬取2-200页，一共有200页
        try {
            List<String> urls = page.getHtml().xpath("//*[@class='post_item']//div[@class='post_item_body']/h3/a/@href").all();
            page.addTargetRequests(urls);
            page.addTargetRequest("https://www.cnblogs.com/#p" + ++pageNum);
                       System.out.println("CurrPage:" + pageNum + "#######################################");
              } catch (Exception e) {
                         e.printStackTrace();
        }
        } else {
            processField(page);
                   // 获取页面需要的内容
                    System.out.println("抓取的内容：" + page.getHtml().xpath("//a[@id='cb_post_title_url']/text()").get());
         }
    }
    public void processField(Page page){
       String title= page.getHtml().xpath("//a[@id=\"cb_post_title_url\"]/text()").toString();
        System.out.println(">>>>>>>>>>>>>>>>>"+title);
        page.putField("title",title);
    }
    @Override
    public Site getSite() {
        return site;
    }
    public static final String PAGE_LIST = "https://www\\.cnblogs\\.com/#p\\d{1,3}";
    public static int pageNum = 1;
    public static void main(String[] args) {
        CnblogsProcessor processor=new CnblogsProcessor();
        String url="https://www.cnblogs.com/";
        Spider.create(processor).addUrl(url).setSpiderListener(processor)
                .addPipeline(new FilePipeline()).thread(3).run();
    }

    @Override
    public void onStart(Spider spider) {
        System.out.println("任务开始===》");
    }

    @Override
    public void onSuccess(Request request) {
        System.out.println("解析"+request.getUrl()+"成功!");
    }

    @Override
    public void onError(Request request) {
        System.out.println("解析"+request.getUrl()+"失败!");
    }

    @Override
    public void onFinish(Spider spider) {
        System.out.println("任务结束===》");
    }
}
