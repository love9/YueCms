package com.makbro.core.cms.chapterDetail.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.chapterDetail.bean.ChapterDetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 书籍章节 dao
 * @author  wujiyue on 2017-12-20 08:47:25.
 */
@Repository
public interface ChapterDetailMapper{

    public ChapterDetail get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(ChapterDetail chapterDetail);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<ChapterDetail> chapterDetails);
    public void update(ChapterDetail chapterDetail);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);

    public List<ChapterDetail> find(PageBounds pageBounds, Map<String, Object> map);
    public List<ChapterDetail> findByMap(PageBounds pageBounds, Map<String, Object> map);


	public int updateSort(@Param("id") String id, @Param("sort") String sort);
	public Integer getMaxSort();
    public Integer getMaxSortOfBook(@Param("book_id") String book_id);
    public ChapterDetail getByBookidAndSort(@Param("book_id") String book_id, @Param("sort") int sort);
    public void deleteByBookId(@Param("book_id") String book_id);

    //章节内容相关 cms_chapter_detail_content
    public String getChapterContent(Integer id);//获得内容
    public void addChapterContent(Map<String, Object> map);//插入内容
    public Integer checkContentExists(Integer id);
    public void updateChapterContent(Map<String, Object> map);//更新内容


}
