package com.makbro.core.cms.project.bug.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.project.bug.bean.Bug;
import com.makbro.core.cms.project.bug.dao.BugMapper;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 项目Bug Service
 * @author wujiyue
 */
@Service
public class BugService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private BugMapper bugMapper;
    @Autowired
    private OrgTreeMapper orgTreeMapper;
     /*基础公共方法*/
    public Bug get(Integer id){
        return bugMapper.get(id);
    }

    public List<Bug> find(PageBounds pageBounds,Map<String,Object> map){
        List<Bug> list=bugMapper.find(pageBounds,map);
        for(Bug bug:list){
            String assignTo=bug.getAssignTo();
            assignTo= StringUtil.subEndStr(assignTo, ";");
            assignTo=assignTo.replaceAll(";","','");
            List<Map<String,Object>> ls=orgTreeMapper.getYhByYhids(assignTo);
            String assignToNames="";
            if(ls!=null&&ls.size()>0){
                for(Map<String,Object> t:ls){
                    assignToNames+=String.valueOf(t.get("account"))+"("+String.valueOf(t.get("realname"))+");";
                }
            }
            bug.setAssignToNames(assignToNames);
        }
        return list;
    }
    public List<Bug> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return bugMapper.findByMap(pageBounds,map);
    }

    public void add(Bug bug){
        bugMapper.add(bug);
    }
    public Object save(Map<String,Object> map){
            Bug bug= JSON.parseObject(JSON.toJSONString(map),Bug.class);
            if(bug.getId()==null||"".equals(bug.getId().toString())){
               Integer id= keyService.getIntegerId();
               bug.setId(id);
               bugMapper.add(bug);
            }else{
               bugMapper.update(bug);
            }
            return Msg.success("保存信息成功");
    }
    public void addBatch(List<Bug> bugs){
        bugMapper.addBatch(bugs);
    }

    public void update(Bug bug){
        bugMapper.update(bug);
    }

    public void updateByMap(Map<String,Object> map){
        bugMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        bugMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        bugMapper.delete(id);
    }
    public void deleteBatch(Integer[] ids){
        bugMapper.deleteBatch(ids);
    }

}
