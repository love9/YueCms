package com.makbro.core.cms.spider.core.pipeline;


import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Task;
import com.makbro.core.cms.spider.mission.service.SpidermissionService;
import com.markbro.base.common.util.FreeMarkerUtil;
import com.markbro.base.common.util.VelocityHelper;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.annotation.ThreadSafe;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 通过鼎泰凭借模板字符串
 * 然后和数据合并组成最后需要的内容然后输出文件
 */
@ThreadSafe
@Component("templateMergerStringFilePipeline")
public class TemplateMergerStringFilePipeline extends BasePipeline{

    public TemplateMergerStringFilePipeline() {
        setPath("/data/webmagic/");
    }
    /**
     * 构造方法
     * @param templateString  模板字符串
     */
    public TemplateMergerStringFilePipeline(String templateString) {
        this.templateString=templateString;
        setPath("/data/webmagic/");
    }

    /**
     * 构造方法
     * @param path 输出绝对路径的根路径
     * @param templateString 模板字符串
     */
    public TemplateMergerStringFilePipeline(String path, String templateString) {
        this.templateString=templateString;
        setPath(path);
    }
    /**
     * 构造方法
     * @param path 输出绝对路径的根路径,这个配置优先级高于my.properties配置的 static.relativeBathPath
     * @param templateString 模板字符串
     */
    public TemplateMergerStringFilePipeline(String path, String relative_base_path, String templateString) {
        this.templateString=templateString;
        setRelative_base_path(relative_base_path);
        setPath(path);
    }
    @Override
    public void process(ResultItems resultItems, Task task) {

        super.process(resultItems,task);

        String path =""; String relative_path="";
        String missionType=(String)resultItems.getAll().get("missionType");//从任务类型可以知道是何种类型比如文章、书籍、书籍章节
        try {
            String htmlName= DigestUtils.md5Hex(resultItems.getRequest().getUrl());
            if(StringUtil.isEmpty(relative_base_path)){
                relative_base_path = GlobalConfig.getConfig("static.relativeBathPath");
            }

            if(StringUtil.notEmpty(relative_base_path)){
                path = this.path + PATH_SEPERATOR;
                if(SpidermissionService.MissionType.book.toString().equals(missionType)){
                    relative_path=this.relative_base_path+File.separator+"book"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"book"+File.separator+ htmlName+ ".html";//文件绝对路径
                }else if(SpidermissionService.MissionType.article.toString().equals(missionType)){
                    relative_path=this.relative_base_path+File.separator+"article"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"article"+File.separator+ htmlName+ ".html";//文件绝对路径
                }else if(SpidermissionService.MissionType.chapter.toString().equals(missionType)){
                    relative_path=this.relative_base_path+File.separator+"chapter"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"chapter"+File.separator+ htmlName+ ".html";//文件绝对路径
                }else{
                    relative_path=this.relative_base_path+File.separator+"other"+File.separator+htmlName+ ".html";//文件相对路径
                    path=path +"other"+File.separator+ htmlName+ ".html";//文件绝对路径
                }
            }else{//没配置相对路径，就不考虑它
                return;
            }

            if(super.templateType==TemplateType.Freemarker){

                FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(FreeMarkerUtil.TemplateLoaderType.StringTemplateLoader,new HashMap<String,String>(){{
                    put("name",templateString);//1.此处name只要和下面2.处的name对应起来就可以，随便什么字符串
                }});
                Map dataMap=new HashMap();
                for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
                    dataMap.put(entry.getKey(), entry.getValue());
                }
                freeMarkerUtil.ExecuteFile("name",dataMap,path);//2.

                logger.info("filepath===>" + path);
                resultItems.getAll().put("static_url", relative_path);
                resultItems.getAll().put("url", resultItems.getRequest().getUrl());

            }else if(super.templateType==TemplateType.Velocity) {
                File f = getFile(path);
                PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
                logger.info("filepath===>" + f.getAbsolutePath());

                VelocityHelper helper = new VelocityHelper("");
                StringBuffer contentStrSb = new StringBuffer();
                for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
                    if (entry.getValue() instanceof Iterable) {
                        Iterable value = (Iterable) entry.getValue();
                        helper.AddKeyValue(entry.getKey(), value);
                        contentStrSb.append(entry.getKey()).append(":");
                        for (Object o : value) {
                            contentStrSb.append(o).append("\r\n");
                        }
                    } else {
                        helper.AddKeyValue(entry.getKey(), entry.getValue());
                        contentStrSb.append(entry.getKey()).append(":").append(entry.getValue()).append("\r\n");
                    }
                }
                resultItems.getAll().put("static_url", relative_path);//这里如果没配置静态化相对路径就是空的

                helper.AddKeyValue("content_all", contentStrSb.toString());//所有内容
                helper.AddKeyValue("url", resultItems.getRequest().getUrl());
                String all = helper.ExecuteMergeString(templateString);
                printWriter.println(all);
                printWriter.close();
            }
        } catch (Exception e) {
            logger.warn("write file error", e);
        }
    }
}
