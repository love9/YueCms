package com.makbro.core.cms.jobs;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.book.bean.Book;
import com.makbro.core.cms.book.dao.BookMapper;
import com.makbro.core.cms.book.service.BookService;
import com.makbro.core.cms.chapterDetail.bean.ChapterDetail;
import com.makbro.core.cms.chapterDetail.service.ChapterDetailService;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.service.TemplateService;
import com.makbro.core.framework.quartz.jobs.BaseJob;
import com.markbro.base.common.util.FreeMarkerUtil;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.common.util.VelocityHelper;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.SpringContextHolder;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description 静态化书籍章节的任务
 * @author wjy
 * @date 2018年10月6日14:42:06
 */
public class BookChapterStaticJob extends BaseJob {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    BookService bookService= SpringContextHolder.getBean("bookService");
    BookMapper bookMapper= SpringContextHolder.getBean("bookMapper");
    ChapterDetailService chapterDetailService= SpringContextHolder.getBean("chapterDetailService");
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

            super.execute(jobExecutionContext);
            //从数据库查询或者从jobDataMap获得需要静态化的书籍ID
            //
            //String bookId="13";
            String bookId=bookMapper.getBookIdForChapterStatic();
            if(StringUtil.isEmpty(bookId)){
                return;
            }
            //更新static_flag=2表示正在执行静态化
            Book updateBook=new Book();
            updateBook.setId(bookId);
            updateBook.setStatic_flag(2);
            bookService.update(updateBook);

           Book book = bookService.get(bookId);
           /* if("2".equals(book.getStatus())){
                logger.warn("书籍bookId:{},bookName:《{}》正在执行静态化!跳过本次执行!",book.getId(),book.getBookname());
                return;
            } */
            //修改cms_book 的static_flag=2表示正在静态化
            book.setStatic_flag(2);
            bookService.update(book);


            Map queryMap=new HashMap<String,Object>();
            queryMap.put("book_id",bookId);
            List<ChapterDetail> list=chapterDetailService.find(new PageBounds(),queryMap);
            //String chapterId="3191";
            TemplateService templateService= SpringContextHolder.getBean("templateService");
            String templateName = jobExecutionContext.getJobDetail().getJobDataMap().getString("templateName");
            if(StringUtil.isEmpty(templateName)){
                templateName="bookChapterStatic";
            }
            Template template= templateService.getByName(templateName);
            String templateString="";//模板字符串
            if(template!=null){
                templateString=template.getContent();
            }
            if(StringUtil.isEmpty(templateString)){
                logger.warn("templateString 为空!");
                return;
            }
            if(CollectionUtils.isNotEmpty(list)){
                for(ChapterDetail detail:list){
                    bookStaticFreemarker(bookId,detail.getId().toString(),templateString);
                }
            }else{
                logger.warn("书籍bookId:{},bookName:{}没有查询到书籍章节!",book.getId(),book.getBookname());
            }
            Book updateBook2=new Book();
            updateBook2.setId(bookId);
            updateBook2.setStatic_flag(1);//完成静态化
            bookService.update(updateBook2);

    }
    public void bookStaticFreemarker(String bookId,String chapterId,String templateString){
        try {

            Book book = (Book) EhCacheUtils.getSysInfo("staticBook",bookId);
            if(book==null){
                book = bookService.getBookAndChapterList(bookId);
                EhCacheUtils.putSysInfo("staticBook",bookId);
            }

            ChapterDetail detail= chapterDetailService.get(Integer.valueOf(chapterId));

            // String path=ProjectUtil.getProjectPath()+Global.getConfig("static.webAppPath")+Global.getConfig("static.relativeBathPath");;//项目根目录+webapp路径+静态文件输出路径相对webapp目录
            //生产环境用下面的路径

            //开发环境path
            String path= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.webAppPath")+ GlobalConfig.getConfig("static.relativeBathPath");
            String relative_path="";
            String relative_base_path= GlobalConfig.getConfig("static.relativeBathPath");
            //String htmlName= DigestUtils.md5Hex(chapterDetail.getId().toString());
            String htmlName=detail.getSort().toString();
            if(StringUtil.notEmpty(relative_base_path)){
                if (!path.endsWith("/")) {
                    path += "/";
                }
                //拼接相对路径
                relative_path= relative_base_path+ File.separator+"book"+File.separator+bookId+File.separator+"detail"+File.separator+htmlName+ ".html";//文件相对路径
                //拼接绝对路径
                path=path +"book"+File.separator+bookId+File.separator+"detail"+File.separator+ htmlName+ ".html";//文件绝对路径
                File file=new File(path);

                path=file.getCanonicalPath();
            }else{//没配置相对路径，就不考虑它
                return;
            }
            File f=getFile(path);

            List<ChapterDetail> chapterDetails=book.getChapterDetails();
            ChapterDetail pre_chapter=null;
            ChapterDetail next_chapter=null;
            int sort=detail.getSort();
            if(sort>1){
                List<ChapterDetail> tempList = chapterDetails.stream().filter(c->{
                    return   c.getSort()==(sort-1);
                }).collect(Collectors.toList());
                if(CollectionUtils.isNotEmpty(tempList)){
                    pre_chapter=tempList.get(0);
                }
            }
            List<ChapterDetail> tempList = chapterDetails.stream().filter(c->{
                return   c.getSort()==(sort+1);
            }).collect(Collectors.toList());
            if(CollectionUtils.isNotEmpty(tempList)){
                next_chapter=tempList.get(0);
            }

            Map dataMap=new HashMap();
            dataMap.put("book",book);
            dataMap.put("detail",detail);
            dataMap.put("chapterDetails",chapterDetails);
            dataMap.put("pre_chapter",pre_chapter);
            dataMap.put("next_chapter",next_chapter);
            FreeMarkerUtil freeMarkerUtil=new FreeMarkerUtil(FreeMarkerUtil.TemplateLoaderType.StringTemplateLoader,new HashMap<String,String>(){{
                put("name",templateString);
            }});
            boolean flag=freeMarkerUtil.ExecuteFile("name",dataMap,path);
            if(flag){
                logger.info(">>>>>>>>static Book Chapter file path success!====>"+path);
            }else{
                logger.warn(">>>>>>>>static Book Chapter file path fail!====>"+path);
            }

            //logger.info("filepath===>"+f.getAbsolutePath());
            //把生成的静态文件相对地址写到数据库
            ChapterDetail updateDetail=new ChapterDetail();
            updateDetail.setId(detail.getId());
            //  / front/static\book\13\detail\1.html
            relative_path=relative_path.replaceAll("\\\\","\\/");//把反斜杠换成正斜杠
            updateDetail.setStatic_url(relative_path);
            chapterDetailService.update(updateDetail);
            logger.info("书籍bookId:{},bookName:《{}》章节:《{}》,序号:{}静态化成功!",book.getId(),book.getBookname(),detail.getTitle(),detail.getSort());
        } catch (Exception e) {
            logger.warn("write file error", e);
        }

    }
    public void bookStatic(String bookId,String chapterId,String templateString){
        try {

            Book book = (Book) EhCacheUtils.getSysInfo("staticBook",bookId);
            if(book==null){
                book = bookService.getBookAndChapterList(bookId);
                EhCacheUtils.putSysInfo("staticBook",bookId);
            }

            ChapterDetail detail= chapterDetailService.get(Integer.valueOf(chapterId));

           // String path=ProjectUtil.getProjectPath()+Global.getConfig("static.webAppPath")+Global.getConfig("static.relativeBathPath");;//项目根目录+webapp路径+静态文件输出路径相对webapp目录
            //生产环境用下面的路径
            String path= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.relativeBathPath");
            String relative_path="";
            String relative_base_path= GlobalConfig.getConfig("static.relativeBathPath");
            //String htmlName= DigestUtils.md5Hex(chapterDetail.getId().toString());
            String htmlName=detail.getSort().toString();
            if(StringUtil.notEmpty(relative_base_path)){
                if (!path.endsWith("/")) {
                    path += "/";
                }
                //拼接相对路径
                relative_path= relative_base_path+ File.separator+"book"+File.separator+bookId+File.separator+"detail"+File.separator+htmlName+ ".html";//文件相对路径
                //拼接绝对路径
                path=path +"book"+File.separator+bookId+File.separator+"detail"+File.separator+ htmlName+ ".html";//文件绝对路径
                File file=new File(path);

                path=file.getCanonicalPath();
            }else{//没配置相对路径，就不考虑它
                return;
            }
            File f=getFile(path);
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f),"UTF-8"));
            VelocityHelper helper=new VelocityHelper("");
            helper.AddKeyValue("book", book);
            helper.AddKeyValue("detail", detail);
            List<ChapterDetail> chapterDetails=book.getChapterDetails();
            ChapterDetail pre_chapter=null;
            ChapterDetail next_chapter=null;
            int sort=detail.getSort();

            if(sort>1){
                List<ChapterDetail> tempList = chapterDetails.stream().filter(c->{
                  return   c.getSort()==(sort-1);
                }).collect(Collectors.toList());
                if(CollectionUtils.isNotEmpty(tempList)){
                    pre_chapter=tempList.get(0);
                }
            }
            List<ChapterDetail> tempList = chapterDetails.stream().filter(c->{
                return   c.getSort()==(sort+1);
            }).collect(Collectors.toList());
            if(CollectionUtils.isNotEmpty(tempList)){
                next_chapter=tempList.get(0);
            }

            helper.AddKeyValue("chapterDetails", chapterDetails);
            helper.AddKeyValue("pre_chapter", pre_chapter);
            helper.AddKeyValue("next_chapter", next_chapter);
            String all=helper.ExecuteMergeString(templateString);
            printWriter.println(all);
            printWriter.close();
            logger.info("filepath===>"+f.getAbsolutePath());
            //把生成的静态文件相对地址写到数据库
            ChapterDetail updateDetail=new ChapterDetail();
            updateDetail.setId(detail.getId());
            //  / front/static\book\13\detail\1.html
            relative_path=relative_path.replaceAll("\\\\","\\/");//把反斜杠换成正斜杠
            updateDetail.setStatic_url(relative_path);
            chapterDetailService.update(updateDetail);
            logger.info("书籍bookId:{},bookName:《{}》章节:《{}》,序号:{}静态化成功!",book.getId(),book.getBookname(),detail.getTitle(),detail.getSort());
        } catch (Exception e) {
            logger.warn("write file error", e);
        }

    }
    public File getFile(String fullName) {
        checkAndMakeParentDirecotry(fullName);
        return new File(fullName);
    }

    public void checkAndMakeParentDirecotry(String fullName) {
        int index = fullName.lastIndexOf("/");
        if (index > 0) {
            String path = fullName.substring(0, index);
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        index = fullName.lastIndexOf("\\");
        if (index > 0) {
            String path = fullName.substring(0, index);
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
    }
}
