package com.makbro.core.cms.sendEmail.web;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.sendEmail.bean.SendEmail;
import com.makbro.core.cms.sendEmail.service.SendEmailService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/cms/sendEmail")
public class SendEmailController extends BaseController {
    @Autowired
    protected SendEmailService sendEmailService;

    @RequestMapping(value={"","/"})
    public String index(){
        return "/cms/sendEmail/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(SendEmail sendEmail, Model model){
        return "/cms/sendEmail/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(SendEmail sendEmail,Model model){
        if(sendEmail!=null&&sendEmail.getId()!=null){
            sendEmail=sendEmailService.get(sendEmail.getId());
        }
         model.addAttribute("sendEmail",sendEmail);
         return "/cms/sendEmail/edit";
    }
    //-----------json数据接口--------------------
    
    
    
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    @RequiresPermissions(value = {"cms.siteEmail","cms.sendEmail"},logical = Logical.OR)
    public Object get(@PathVariable String id) {
        return sendEmailService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    @RequiresPermissions(value = {"cms.siteEmail","cms.sendEmail"},logical = Logical.OR)
    public Object find() {

       /* var status='';
        if(from=='important'){
            status='2';
        }else if(from=='draft'){
            status='3';
        }else if(from=='rubbish'){
            status='0';
        }else{status='1';}*/

        Map map= MyBatisRequestUtil.getMap(request);
        String from=String.valueOf(map.get("from"));
        if("receive".equals(from)){
            map.put("toYhid",String.valueOf(map.get(TmConstant.YHID_KEY)));
        }else{
            //查询自己的数据
            map.put("userid",String.valueOf(map.get(TmConstant.YHID_KEY)));
            if(from.equals("important")){
                map.put("status","2");
            }else if(from.equals("draft")){
                map.put("status","3");
            }else if(from.equals("rubbish")){
                map.put("status","0");
            }else{
                map.put("status","1");
            }

        }


        resultMap=getPageMap(sendEmailService.find(getPageBounds(),map));
        return resultMap;
    }
    /**
     * 不分页查询数据
     */
    @ResponseBody
    @RequestMapping("/json/findAll")
    public Object findAll() {
        Map map= MyBatisRequestUtil.getMap(request);
        String sortString=String.valueOf(map.get("sortString"));
        PageBounds pageBounds=null;
        if(StringUtil.notEmpty(sortString)){
             pageBounds=new PageBounds(Order.formString(sortString));
        }else{
             pageBounds=new PageBounds();
        }
        resultMap=getPageMap(sendEmailService.find(pageBounds,map));
        return resultMap;
    }

    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(SendEmail m) {

        sendEmailService.add(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(SendEmail m) {
        sendEmailService.update(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @RequiresPermissions(value = {"cms.siteEmail","cms.sendEmail"},logical = Logical.OR)
    public Object save() {
           Map map= MyBatisRequestUtil.getMap(request);
           return sendEmailService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @RequiresPermissions(value = {"cms.siteEmail","cms.sendEmail"},logical = Logical.OR)
    public Object delete(@PathVariable String id) {
    	Msg msg=new Msg();
    	try{

            sendEmailService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
        		msg.setType(Msg.MsgType.error);
        		msg.setContent("删除失败！");
        }
        return msg;
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @RequiresPermissions(value = {"cms.siteEmail","cms.sendEmail"},logical = Logical.OR)
    public Object deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
    	try{

             sendEmailService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }



    @ResponseBody
    @RequestMapping(value = "/json/trash/{id}", method = RequestMethod.POST)
    @RequiresPermissions(value = {"cms.siteEmail","cms.sendEmail"},logical = Logical.OR)
    public Object trash(@PathVariable String id) {
        Msg msg=new Msg();
        try{
            sendEmailService.trash(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("放入回收站成功！");
        }catch (Exception e){
            msg.setType(Msg.MsgType.error);
            msg.setContent("放入回收站失败！");
        }
        return msg;
    }

    @ResponseBody
    @RequestMapping(value = "/json/importants/{ids}", method = RequestMethod.POST)
    @RequiresPermissions(value = {"cms.siteEmail","cms.sendEmail"},logical = Logical.OR)
    public Object importants(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
        try{
            sendEmailService.importants(ids);
            msg.setType(Msg.MsgType.success);
            msg.setContent("设置成功！");
        }catch (Exception e){
            msg.setType(Msg.MsgType.error);
            msg.setContent("设置失败！");
        }
        return msg;
    }


}