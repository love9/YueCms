package com.makbro.core.cms.project.module.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.project.module.bean.Module;
import com.makbro.core.cms.project.module.dao.ModuleMapper;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目模块 Service
 * @author wujiyue
 */
@Service
public class ModuleService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private ModuleMapper moduleMapper;

     /*基础公共方法*/
    public Module get(String id){
        return moduleMapper.get(id);
    }

    public List<Module> find(PageBounds pageBounds,Map<String,Object> map){
        return moduleMapper.find(pageBounds,map);
    }
    public List<Module> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return moduleMapper.findByMap(pageBounds,map);
    }

    public void add(Module module){
        moduleMapper.add(module);
    }
    public Object save(Map<String,Object> map){

            Module module= JSON.parseObject(JSON.toJSONString(map),Module.class);
            if(module.getId()==null||"".equals(module.getId().toString())){
                    String id= keyService.getStringId();
					String parentid=module.getParentid();
					String pids=moduleMapper.getParentidsById(parentid);
					if("null".equals(pids)||"".equals(pids)||pids==null){
						pids="0,";
					}
					pids+=id+",";
					module.setParentids(pids);
                    module.setId(id);
                    moduleMapper.add(module);
            }else{
					String parentid=module.getParentid();
					String pids=moduleMapper.getParentidsById(parentid);
					if("null".equals(pids)||"".equals(pids)||pids==null){
						pids="0,";
					}
					pids+=module.getId()+",";
					module.setParentids(pids);
               moduleMapper.update(module);
            }
            return Msg.success("保存信息成功!");
    }
    public void addBatch(List<Module> modules){
        moduleMapper.addBatch(modules);
    }

    public void update(Module module){
        moduleMapper.update(module);
    }

    public void updateByMap(Map<String,Object> map){
        moduleMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        moduleMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        moduleMapper.delete(id);
    }

    public void deleteBatch(String[] ids){
        moduleMapper.deleteBatch(ids);
    }

	/* public List<Module> findByParentid(PageBounds pageBounds,String parentid){
		return moduleMapper.findByParentid(pageBounds,parentid);
	}*/
    public List<Module> findByParentid(Map<String,Object> map){
        return moduleMapper.findByParentid(map);
    }
	public int getChildrenCount(String ids){
		 ids=ids.replaceAll(",", "','").replaceAll("~", "','");
		return moduleMapper.getChildrenCount(ids);
	}
	public List<Map<String,Object>> ztree(Map<String,Object> map){
		//String parentid= (String) map.get("parentid");
		List<Module> list=moduleMapper.findByParentid(map);
		List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
		Map<String,Object> tmap=null;
		String id=null;
		int n=0;
		for(Module t:list){
			tmap=new HashMap<String,Object>();
			ServletUtil.beanToMap(t,tmap);
			id=t.getId();
			n=moduleMapper.findByParentidCount(tmap);
			if(n>0){
				tmap.put("isParent",true);
			}
			result.add(tmap);
		}
		return result;
	}
    /*自定义方法*/
    public Object  select(Map<String,Object> map){
        List<Module> list=moduleMapper.find(new PageBounds(999),map);
        List<Map<String,Object>> res=new ArrayList<Map<String,Object>>();
        Map<String,Object> tmap=null;
        for(Module module:list){
            tmap=new HashMap<String,Object>();
            tmap.put("dm",module.getId());
            tmap.put("mc",module.getText());
            res.add(tmap);
        }
        return res;
    }

}
