package com.makbro.core.cms.project.mission.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgRole.dao.RoleMapper;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.base.recentNews.bean.RecentNews;
import com.makbro.core.base.recentNews.dao.RecentNewsMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.cms.project.mission.bean.Mission;
import com.makbro.core.cms.project.mission.dao.MissionMapper;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目任务 Service
 * Created by wujiyue on 2017-08-23 14:11:59.
 */
@Service
public class MissionService{
    @Autowired
    private OrgTreeMapper orgTreeMapper;
    @Autowired
    private TableKeyService keyService;
    @Autowired
    private MissionMapper missionMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RecentNewsMapper recentNewsMapper;
     /*基础公共方法*/
    public Mission get(Integer id){
        return missionMapper.get(id);
    }

    public List<Mission> find(PageBounds pageBounds,Map<String,Object> map){
        List<Mission> list=missionMapper.find(pageBounds,map);
        for(Mission mission:list){
            String assignTo=mission.getAssignTo();
            assignTo= StringUtil.subEndStr(assignTo, ";");
            assignTo=assignTo.replaceAll(";","','");
            List<Map<String,Object>> ls=orgTreeMapper.getYhByYhids(assignTo);
            String assignToNames="";
            if(ls!=null&&ls.size()>0){
                for(Map<String,Object> t:ls){
                    assignToNames+=String.valueOf(t.get("account"))+"("+String.valueOf(t.get("realname"))+");";
                }
            }
            mission.setAssignToNames(assignToNames);
        }
        return list;
    }
    public List<Mission> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return missionMapper.findByMap(pageBounds,map);
    }

    public void add(Mission mission){
        missionMapper.add(mission);
    }
    public Object save(Map<String,Object> map){

            Mission mission= JSON.parseObject(JSON.toJSONString(map),Mission.class);
            if(mission.getId()==null||"".equals(mission.getId().toString())){
               Integer id= keyService.getIntegerId();
               mission.setId(id);
               missionMapper.add(mission);
            }else{
                //如果指派人不为空并且status还是0那么把status置为1
                if(StringUtil.notEmpty(mission.getAssignTo())&&"0".equals(mission.getStatus())){
                    mission.setStatus("1");//指派人后任务状态变为开发中
                }
               missionMapper.update(mission);
            }
            return Msg.success("保存信息成功!");
    }
    public void addBatch(List<Mission> missions){
        missionMapper.addBatch(missions);
    }

    public void update(Mission mission){
        missionMapper.update(mission);
    }

    public void updateByMap(Map<String,Object> map){
        missionMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        missionMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        missionMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        missionMapper.deleteBatch(ids);
    }
     /*自定义方法*/
    //抢任务
     public int askForMission(Map<String,Object> map,Integer id){
         String yhid=(String)map.get(TmConstant.YHID_KEY);
         //先判断任务是否已经被抢
         Mission mission=missionMapper.get(id);
         if(mission!=null){
             if(StringUtil.notEmpty(mission.getAssignTo())){
                 Map<String,Object> mapNew=new HashMap<String,Object>();
                 mapNew.put("status","1");
                 mapNew.put("id",id);
                 missionMapper.updateByMap(mapNew);//修改任务状态
                return  0;//任务已经被抢
             }else{
                 Map<String,Object> mapNew=new HashMap<String,Object>();
                 mapNew.put("assignTo",yhid);
                 mapNew.put("status","1");
                 mapNew.put("id",id);
                 missionMapper.updateByMap(mapNew);

                 //记录动态日志。用户某某抢到了新任务XXX
                 RecentNews recentNews=new RecentNews();
                 recentNews.setCreateBy(yhid);
                 recentNews.setNewstype(TmConstant.RECENT_NEWS_TEXT);// 1	文本消息2	图文消息3	附件消息（文档） 4	申请消息 5	工作动态  0系统通知
                 recentNews.setPushscope(TmConstant.PUSH_SCOPE_RECENT_NEWS_DEPT); //0仅个人  1全部推送  2仅推送本部门
                 String yhmc=String.valueOf(EhCacheUtils.getUserInfo(TmConstant.XM_KEY,yhid));
                 recentNews.setTitle(yhmc+"抢到了新任务【"+mission.getName()+"】");
                 recentNews.setContent(yhmc + "抢到了新任务【" + mission.getName() + "】");
                 recentNews.setRefid(id.toString());
                 recentNews.setModule("project/mission");
                 recentNewsMapper.add(recentNews);

                 return 1;//抢任务成功
             }

         }else{
             return  -1;//传递的参数异常
         }

     }



}
