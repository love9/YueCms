package com.makbro.core.cms.project.module.bean;


import com.markbro.base.model.AliasModel;

/**
 * 项目模块 bean
 * @author wujiyue
 */
public class Module  implements AliasModel {

	private String id;//模块编号
	private String parentid;//
	private String parentids;//
	private Integer projectId;//项目ID
	private String name;//模块名称
	private String text;//显示文本
	private Object description;//项目描述
	private String shortname;//简称


	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getParentid(){ return parentid ;}
	public void  setParentid(String parentid){this.parentid=parentid; }
	public String getParentids(){ return parentids ;}
	public void  setParentids(String parentids){this.parentids=parentids; }
	public Integer getProjectId(){ return projectId ;}
	public void  setProjectId(Integer projectId){this.projectId=projectId; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getText(){ return text ;}
	public void  setText(String text){this.text=text; }
	public Object getDescription(){ return description ;}
	public void  setDescription(Object description){this.description=description; }
	public String getShortname(){ return shortname ;}
	public void  setShortname(String shortname){this.shortname=shortname; }


	@Override
	public String toString() {
	return "Module{" +
			"id=" + id+
			", parentid=" + parentid+
			", parentids=" + parentids+
			", projectId=" + projectId+
			", name=" + name+
			", text=" + text+
			", description=" + description+
			", shortname=" + shortname+
			 '}';
	}
}
