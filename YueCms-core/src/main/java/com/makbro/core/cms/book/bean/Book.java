package com.makbro.core.cms.book.bean;


import com.makbro.core.cms.chapterDetail.bean.ChapterDetail;
import com.markbro.base.model.AliasModel;

import java.util.List;

/**
 * 书籍 bean
 * @author  by wujiyue on 2018-08-29 14:03:24 .
 */
public class Book  implements AliasModel {

	private String id;//主键，文章ID
	private String yhid;//作者
	private String bookcode;//书籍代码，唯一，可以用来传递个趴取任务的contentImgFolder 作为文章正文图片的上层文件夹。
	private String bookname;//书名
	private String spiderMissionCode;//将会使用该趴取任务代码去进一步趴取书籍章节
	private String author;//作者
	private String keywords;//关键词
	private String description;//描述
	private String resourcetype;//资源类别
	private String source;//来源
	private String link;//章节列表链接
	private String static_url;//原始链接
	private String sheet_url;//封面url
	private String tags;//标签
	private String last_update;//最近更新
	private Integer totalWords;//总字数
	private String status;//书籍状态
	private Integer hit;//点击数
	private Integer month_hit;//月点击数
	private Integer week_hit;//
	private Integer up_vote;//点赞数
	private Integer down_vote;//差评数
	private Integer hot_flag;//热点标志
	private Integer new_flag;//新增标志
	private Integer comment_flag;//是否开启评论
	private Integer top_flag;//置顶标志
	private Integer static_flag;//静态化标志
	private Integer favourite;//收藏数
	private String createTime;//创建时间
	private String updateTime;//更新时间
	private Integer available;//状态标志
	private Integer deleted;//删除标志

	private List<ChapterDetail> chapterDetails;//扩展字段，章节列表目录

	public List<ChapterDetail> getChapterDetails() {
		return chapterDetails;
	}

	public void setChapterDetails(List<ChapterDetail> chapterDetails) {
		this.chapterDetails = chapterDetails;
	}

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getBookname(){ return bookname ;}
	public void  setBookname(String bookname){this.bookname=bookname; }
	public String getAuthor(){ return author ;}
	public void  setAuthor(String author){this.author=author; }
	public String getKeywords(){ return keywords ;}
	public void  setKeywords(String keywords){this.keywords=keywords; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getResourcetype(){ return resourcetype ;}
	public void  setResourcetype(String resourcetype){this.resourcetype=resourcetype; }
	public String getSource(){ return source ;}
	public void  setSource(String source){this.source=source; }
	public String getStatic_url(){ return static_url ;}
	public void  setStatic_url(String static_url){this.static_url=static_url; }
	public String getSheet_url(){ return sheet_url ;}
	public void  setSheet_url(String sheet_url){this.sheet_url=sheet_url; }
	public String getTags(){ return tags ;}
	public void  setTags(String tags){this.tags=tags; }
	public String getLast_update(){ return last_update ;}
	public void  setLast_update(String last_update){this.last_update=last_update; }
	public Integer getTotalWords(){ return totalWords ;}
	public void  setTotalWords(Integer totalWords){this.totalWords=totalWords; }
	public String getStatus(){ return status ;}
	public void  setStatus(String status){this.status=status; }
	public Integer getHit(){ return hit ;}
	public void  setHit(Integer hit){this.hit=hit; }
	public Integer getMonth_hit(){ return month_hit ;}
	public void  setMonth_hit(Integer month_hit){this.month_hit=month_hit; }
	public Integer getWeek_hit(){ return week_hit ;}
	public void  setWeek_hit(Integer week_hit){this.week_hit=week_hit; }
	public Integer getUp_vote(){ return up_vote ;}
	public void  setUp_vote(Integer up_vote){this.up_vote=up_vote; }
	public Integer getDown_vote(){ return down_vote ;}
	public void  setDown_vote(Integer down_vote){this.down_vote=down_vote; }
	public Integer getHot_flag(){ return hot_flag ;}
	public void  setHot_flag(Integer hot_flag){this.hot_flag=hot_flag; }
	public Integer getNew_flag(){ return new_flag ;}
	public void  setNew_flag(Integer new_flag){this.new_flag=new_flag; }
	public Integer getComment_flag(){ return comment_flag ;}
	public void  setComment_flag(Integer comment_flag){this.comment_flag=comment_flag; }
	public Integer getTop_flag(){ return top_flag ;}
	public void  setTop_flag(Integer top_flag){this.top_flag=top_flag; }
	public Integer getFavourite(){ return favourite ;}
	public void  setFavourite(Integer favourite){this.favourite=favourite; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	public String getSpiderMissionCode() {
		return spiderMissionCode;
	}

	public void setSpiderMissionCode(String spiderMissionCode) {
		this.spiderMissionCode = spiderMissionCode;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getStatic_flag() {
		return static_flag;
	}

	public String getBookcode() {
		return bookcode;
	}

	public void setBookcode(String bookcode) {
		this.bookcode = bookcode;
	}

	public void setStatic_flag(Integer static_flag) {
		this.static_flag = static_flag;
	}

	@Override
	public String toString() {
	return "Book{" +
			"id=" + id+
			", yhid=" + yhid+
			", bookcode=" + bookcode+
			", bookname=" + bookname+
			", spiderMissionCode=" + spiderMissionCode+
			", author=" + author+
			", keywords=" + keywords+
			", description=" + description+
			", resourcetype=" + resourcetype+
			", source=" + source+
			", link=" + link+
			", static_url=" + static_url+
			", sheet_url=" + sheet_url+
			", tags=" + tags+
			", last_update=" + last_update+
			", totalWords=" + totalWords+
			", status=" + status+
			", hit=" + hit+
			", month_hit=" + month_hit+
			", week_hit=" + week_hit+
			", up_vote=" + up_vote+
			", down_vote=" + down_vote+
			", hot_flag=" + hot_flag+
			", new_flag=" + new_flag+
			", comment_flag=" + comment_flag+
			", top_flag=" + top_flag+
			", static_flag=" + static_flag+
			", favourite=" + favourite+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
