package com.makbro.core.cms.jobs;

import com.makbro.core.cms.article.bean.Article;
import com.makbro.core.cms.article.bean.ArticleRegionType;
import com.makbro.core.cms.article.service.ArticleService;
import com.makbro.core.framework.quartz.jobs.BaseJob;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.date.DateUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 每过30分钟，把计入缓存的博主访问次数累计到数据库表中并且放入缓存
 * 之所以把该定时任务从BlogTask提出来，是因为，该写法可以动态改变时间和动态的触发
 */
public class BlogArticleRegionJob extends BaseJob {

    @Autowired
    ArticleService articleService;
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);
        System.out.println(DateUtil.getDatetime()+"\t执行BlogArticleRegionJob===>Start");
        for(ArticleRegionType articleRegionType:ArticleRegionType.values()){
            List<Article> list=articleService.findByArticleRegionTypeInDb(articleRegionType);
            EhCacheUtils.putSysInfo("ArticleRegionType_", articleRegionType.getVal(), list);
        }
        System.out.println(DateUtil.getDatetime()+"\t执行BlogArticleRegionJob===>End！");
    }

}
