package com.makbro.core.cms.tag.web;

import com.makbro.core.cms.tag.bean.Tags;
import com.makbro.core.cms.tag.service.TagsService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 标签管理
 * @author  wujiyue on 2018-07-06 15:42:13.
 */

@Controller
@RequestMapping("/cms/tag")
public class TagsController extends BaseController {
    @Autowired
    protected TagsService tagsService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/cms/tag/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Tags tag, Model model){
       pushLoginUserInfo(model);
       return "/cms/tag/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Tags tags, Model model){
        if(tags!=null&&tags.getId()!=null){
            tags=tagsService.get(tags.getId());
        }
         model.addAttribute("tags",tags);
         return "/cms/tag/edit";
    }
    //-----------json数据接口--------------------
    @ResponseBody
    @RequestMapping("/json/tags")
    public Object findTags() {
        Map map= MyBatisRequestUtil.getMap(request);
        return tagsService.findTags(map);
    }

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return tagsService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return tagsService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));

    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return tagsService.save(map);
    }

    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除标签")
    public Object delete(@PathVariable Integer id) {
    	try{
            tagsService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
    	    return Msg.error("删除失败！");
        }
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除标签")
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
            tagsService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return tagsService.saveSort(MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value = "/json/staticTags/{ids}")
    public Object staticTags(@PathVariable String[] ids) {
        return tagsService.staticTags(ids);
    }
}