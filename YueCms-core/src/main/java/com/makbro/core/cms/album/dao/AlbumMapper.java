package com.makbro.core.cms.album.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.album.bean.Album;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 相册图集管理 dao
 * @author wujiyue
 * @date 2019-7-4 21:56:54
 */
@Repository
public interface AlbumMapper{

    public Album get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Album album);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Album> albums);
    public void update(Album album);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Album> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Album> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
