package com.makbro.core.cms.spider.core.processor.service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.article.service.ArticleService;
import com.makbro.core.cms.book.service.BookService;
import com.makbro.core.cms.chapterDetail.dao.ChapterDetailMapper;
import com.makbro.core.cms.resourcetype.service.ResourcetypeService;
import com.makbro.core.cms.spider.core.Request;
import com.makbro.core.cms.spider.core.Spider;
import com.makbro.core.cms.spider.core.SpiderListener;
import com.makbro.core.cms.spider.core.pipeline.FilePipeline;
import com.makbro.core.cms.spider.core.pipeline.TemplateFilePipeline;
import com.makbro.core.cms.spider.core.pipeline.TemplateMergerStringFilePipeline;
import com.makbro.core.cms.spider.core.pipeline.db.ArticlePipeline;
import com.makbro.core.cms.spider.core.pipeline.db.BookPipeline;
import com.makbro.core.cms.spider.core.pipeline.db.ChapterDetailPipeline;
import com.makbro.core.cms.spider.core.processor.PageProcessorMission;
import com.makbro.core.cms.spider.fieldprocessrule.bean.Fieldprocessrule;
import com.makbro.core.cms.spider.fieldprocessrule.service.FieldprocessruleService;
import com.makbro.core.cms.spider.mission.bean.Spidermission;
import com.makbro.core.cms.spider.mission.service.SpidermissionService;
import com.makbro.core.cms.spider.pageProcessMx.bean.PageProcessMx;
import com.makbro.core.cms.spider.pageProcessMx.service.PageProcessMxService;
import com.makbro.core.cms.template.bean.Template;
import com.makbro.core.cms.template.service.TemplateService;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author  Administrator on 2017/11/16.
 */
@Service
@Scope("prototype")
public class PageProcessMissonService implements SpiderListener {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    SpidermissionService spidermissionService;
    @Autowired
    PageProcessMxService pageProcessMxService;
    @Autowired
    FieldprocessruleService fieldprocessruleService;

    @Autowired
    ResourcetypeService resourcetypeService;
    @Autowired
    ArticlePipeline articlePipeline;
    @Autowired
    BookPipeline bookPipeline;
    @Autowired
    TemplateFilePipeline templateFilePipeline;
    @Autowired
    TemplateMergerStringFilePipeline templateMergerStringFilePipeline;
    @Autowired
    ChapterDetailPipeline chapterDetailPipeline;
    @Autowired
    ChapterDetailMapper chapterDetailMapper;
    @Autowired
    TemplateService templateService;


    @Autowired
    ArticleService articleService;

    @Autowired
    BookService bookService;



    private Spidermission spidermission;

    public Object spiderByMissionCode(String missionCode){
        return spiderByMissionCode(missionCode,null,null,null);
    }

    /**
     *
     * @param missionCode 趴取任务的代码
     * @param ref_id 趴取书籍引用book_id
     * @param urls 趴取书籍入口url
     * @param contentImgFolder 内容图片文件夹
     * @return
     */
    public Object spiderByMissionCode(String missionCode,String ref_id,String urls,String contentImgFolder){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.error);
        this.spidermission= spidermissionService.getByMissonCode(missionCode);
        if(this.spidermission==null){
            return Msg.error("不存在任务代码为["+missionCode+"]的任务!");
        }
        if(StringUtil.notEmpty(ref_id)){
            this.spidermission.setRef_id(ref_id);
        }
        if(StringUtil.notEmpty(urls)){
            this.spidermission.setUrls(urls);
        }
        if(StringUtil.notEmpty(contentImgFolder)){
            this.spidermission.setContentImgFolder(contentImgFolder);
        }
        List<PageProcessMx> pageProcessMxs=null;

        //趴取任务数据准备
        if(spidermission==null){
           // throw new ServiceException("根据"+missionCode+"查询出来任务为空！");
            msg.setContent("根据"+missionCode+"查询出来任务为空！");
            return msg;
        }
        if(spidermission.getSleep()==null||spidermission.getSleep()<0){
            spidermission.setSleep(500);//500毫秒
        }
        if("json".equals(spidermission.getMissionType())){

        }else{
            Map<String,Object> tmap=new HashMap<String,Object>();
            tmap.put("zbguid",spidermission.getId());
            pageProcessMxs=pageProcessMxService.find(new PageBounds(),tmap);
            if(CollectionUtils.isEmpty(pageProcessMxs)){
                msg.setContent("pageProcessMxs is empty! 任务没有配置页面处理明细逻辑！");
                return msg;
            }else{
                //设置字段数据处理规则
                pageProcessMxs.forEach(mx->{
                    String ids=mx.getFieldprocessrule_id();
                    if(StringUtil.notEmpty(ids)){
                        try{
                            //需要加上排序序号，顺序就按照ids的顺序
                            String[] arr=ids.split(",");
                            if(arr!=null){
                                List<Fieldprocessrule> list=new ArrayList<Fieldprocessrule>();
                                Fieldprocessrule temp=null;
                                int sort=0;
                                for(String id:arr){
                                    sort++;
                                    temp=fieldprocessruleService.get(Integer.valueOf(id));
                                    if(temp!=null){
                                        temp.setSort(sort);
                                        list.add(temp);
                                    }
                                }
                                if(CollectionUtils.isNotEmpty(list)){
                                    mx.setFieldprocessrules(list);
                                }
                            }
                            //mx.setFieldprocessrules(fieldprocessruleService.findByIds(mx.getFieldprocessrule_id()));
                        }catch(Exception ex){}
                    }
                });
            }
        }

        //准备入口地址url
        String url=spidermission.getUrls();
        String[] urlArr=null;
        if(url.contains(",")){
            urlArr=url.split(",");
        }
        List<String> allUrl=new ArrayList<String>();
        if("1".equals(spidermission.getLoopFlag())){//需要翻页
            String pageParam = spidermission.getLoopParam();//翻页参数正则表达式
            if(urlArr!=null&&urlArr.length>0){
                for(int i=0;i<urlArr.length;i++){
                    String t=urlArr[i];
                    List<String> ls=pageUrls_Reg(t, pageParam, spidermission.getLoopNum());
                    allUrl.addAll(ls);
                }
            }else{
                allUrl=pageUrls_Reg(url,pageParam,spidermission.getLoopNum());
            }
        }else{
            if(urlArr!=null&&urlArr.length>0){
                allUrl=Stream.of(urlArr).collect(Collectors.toList());
            }else{
                allUrl.add(url);
            }
        }
        //创建爬虫对象
        Spider spider= Spider.create(new PageProcessorMission(spidermission,pageProcessMxs)).setSpiderListener(this);
        //向爬虫对象添加入口地址url
        if(allUrl.size()>1){
            String[] tarUrlArr= new String[allUrl.size()];
            allUrl.toArray(tarUrlArr);
            spider.addUrl(tarUrlArr);
        }else{
            spider.addUrl(allUrl.get(0));
        }

        //添加（文件）数据管道
        String outPath="";
        if(GlobalConfig.isDevMode()){//开发模式
            outPath= ProjectUtil.getProjectPath()+GlobalConfig.getConfig("static.webAppPath")+ GlobalConfig.getConfig("static.relativeBathPath");
        }else{
            outPath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("static.relativeBathPath");
        }

        if(spidermission.getPipeline().contains("TemplateMergerStringFilePipeline")){
            if(StringUtil.notEmpty(spidermission.getTemplateName())){
                Template template=templateService.getByName(spidermission.getTemplateName());
                if(template!=null){
                    templateMergerStringFilePipeline.setPath(outPath);
                    templateMergerStringFilePipeline.setTemplateString(template.getContent());
                    spider.addPipeline(templateMergerStringFilePipeline);
                }else{
                    spider.addPipeline(templateFilePipeline);
                }
            }

        }else if(spidermission.getPipeline().contains("TemplateFilePipeline")){
            if(StringUtil.notEmpty(spidermission.getTemplateName()) && spidermission.getTemplateName().contains(".")){
                //趴取任务里面，用户配置了模板名称并且模板名称里面含有 点（说明这是个完整的文件名称）
                templateFilePipeline.setPath(outPath);
                templateFilePipeline.setTemplateName(spidermission.getTemplateName());
                spider.addPipeline(templateFilePipeline);
            }else{
                spider.addPipeline(templateFilePipeline);//不传模板名称它是默认的Spiderhtml.txt
            }
        }
        else{
            spider.addPipeline(new FilePipeline());
        }
        /****************华丽的分割线*************/
        //数据库Pipeline在FilePipeline之后是英文静态化后的路径static_url是在文件Pipeline中放入的.
        /****************华丽的分割线*************/
        //根据任务类型指定（数据库）数据管道
        if(SpidermissionService.MissionType.article.toString().equals(spidermission.getMissionType())){
            spider.addPipeline(articlePipeline);
        }
        if(SpidermissionService.MissionType.book.toString().equals(spidermission.getMissionType())){
            spider.addPipeline(bookPipeline);
        }
        //如果 任务所属的资源类型 是文章 写入 cms_chapter_detail 表
        if(SpidermissionService.MissionType.chapter.toString().equals(spidermission.getMissionType())){
            String book_id = spidermission.getRef_id();
            chapterDetailMapper.deleteByBookId(book_id);//先删除该书籍的章节
            spider.addPipeline(chapterDetailPipeline);
        }






        //线程启动后结果怎么样就不知道了，不知如何改进
        new Thread(new Runnable() {
            @Override
            public void run() {
               spider.run();//启动任务
            }
        }).start();

        msg.setType(Msg.MsgType.success);
        msg.setContent("任务启动");
        return msg;
    }


    @Deprecated
    public static List<String> pageUrls(String url,String pageParam,int loopNum){
        List<String> urls=new ArrayList<String>();
        //String param = spidermission.getLoopParam();//翻页参数
        String paramTemp=pageParam+"=";
        if(url.contains(paramTemp)){
            urls.add(url);
            // System.out.println(url.substring(0,url.indexOf(paramTemp)+paramTemp.length()));
            String str_1=url.substring(0,url.indexOf(paramTemp)+paramTemp.length());
            //System.out.println(str_1);
            String str_2=url.substring(url.indexOf(paramTemp)+paramTemp.length());
            // System.out.println(str_2);
            String str_nownum=str_2;
            String str_end="";
            if(str_2.contains("&")){
                str_nownum=str_2.substring(0,str_2.indexOf("&"));
                System.out.println(str_nownum);
                str_end=str_2.substring(str_2.indexOf("&"));
                // System.out.println(str_end);
            }else{
                // System.out.println(str_nownum);
            }
            for(int i=0;i<loopNum;i++){
                str_nownum=String.valueOf((Integer.valueOf(str_nownum)+1));
                String s=str_1+str_nownum+str_end;
                urls.add(s);
            }
        }else{
            for(int i=1;i<=loopNum;i++){
                String s=url+"&"+pageParam+"="+i;
                urls.add(s);
            }
        }
        return  urls;
    }
    public static void main(String[] args){
        String reg="\\d+";
        String content="有123人";
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(content);
        if(matcher.find()){
            System.out.println(matcher.group(0));
        }
        //List<String> list=pageUrls_Reg("http://wufafuwu.com/a/ONE_wenzhang/list_1_1.html","_\\d+.html",2);
        //TestUtil.printList(list);
    }

    /**
     *获得分页URL集合
     * @param url
     *              起始的url
     * @param reg
     *              分页正则表达式
     * @param loopNum
     *              分页次数
     * @return
     */
    public static List<String> pageUrls_Reg(String url,String reg,int loopNum){
        //根据正则解析分页url
        // http://localhost:8099/sys/spider?code=&page=11
        // page=\d+
        List<String> urls=new ArrayList<String>();
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(url);
        String tarStr="";
        if(matcher.find()){
            // System.out.println(matcher.group(0));
            tarStr=matcher.group(0);//分页相关的字符串
        }else{
            return urls;
        }
        pattern = Pattern.compile("\\d+");
        matcher = pattern.matcher(tarStr);
        String nowNumStr="1";
        int nowNum=1;
        if(matcher.find()){//获得当前页码
            nowNumStr=matcher.group(0);
            //   System.out.println(nowNumStr);
            nowNum=Integer.valueOf(nowNumStr);
        }else{
            return urls;
        }
        for(int i=0;i<loopNum;i++){
            //
            tarStr=tarStr.replaceAll("\\d+",String.valueOf((Integer.valueOf(nowNum))));//把分页相关的字符串中的数字替换
            url=url.replaceAll(reg,tarStr);
            // System.out.println(url);
            urls.add(url);
            nowNum++;
        }
        return urls;
    }
    public String getFileName(Request request){
       return DigestUtils.md5Hex(request.getUrl());
    }

    @Override
    public void onStart(Spider spider) {
        logger.info(">>>>>>>>>>>>>>爬虫启动了>>>>>>>>>>>>>>>>>");
        if(spidermission!=null){

            if(spidermission.getMissionType().equals(SpidermissionService.MissionType.chapter.toString())){  //书籍
                String id=spidermission.getRef_id();
                if(StringUtil.notEmpty(id)){//更新书籍状态为2表示正在趴取中
                    Map map=new HashMap<String,Object>();
                    map.put("id", id);
                    map.put("status",2);
                    bookService.updateByMap(map);
                }
            }

            //更新spidermission的status=1表示任务在执行
            spidermission.setStatus("1");
            spidermissionService.update(spidermission);
        }
    }
    @Override
    public void onFinish(Spider spider) {//总结语
        logger.info("Congratulations! Spider {} closed! {} pages downloaded.", spider.getUUID(), spider.getPageCount());
        if(spidermission!=null){
            //书籍
            if(spidermission.getMissionType().equals(SpidermissionService.MissionType.chapter.toString())){
                String id=spidermission.getRef_id();
                if(StringUtil.notEmpty(id)){
                    //更新cms_book id=spidermission.getRef_id()记录的status=1
                    Map map=new HashMap<String,Object>();
                    map.put("id",id);
                    map.put("status",1);
                    bookService.updateByMap(map);
                }
            }else if(spidermission.getMissionType().equals(SpidermissionService.MissionType.article.toString())){
                //articleService.onMissionFinish(spidermission);
            }else{

            }

            //更新spidermission的status=0表示任务空闲
            spidermission.setStatus("0");
            spidermissionService.update(spidermission);
        }
    }
    @Override
    public void onSuccess(Request request) {

        logger.info("process page["+request.getUrl()+"]==>success!");
    }
    @Override
    public void onError(Request request) {
        logger.error("download page["+request.getUrl()+"]==>error!");
    }


}
