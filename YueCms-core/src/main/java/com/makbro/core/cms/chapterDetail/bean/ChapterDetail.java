package com.makbro.core.cms.chapterDetail.bean;


import com.markbro.base.model.AliasModel;

/**
 * 书籍章节 bean
 * @author  wujiyue on 2017-12-20 08:47:24 .
 */
public class ChapterDetail  implements AliasModel {

	private Integer id;//
	private String book_id;//书籍ID
	private String title;//章节名称
	private String content;//内容
	private Integer sort;//序号
	private Integer hit;//点击数
	private String link;//原文链接
	private String static_url;//静态链接
	private Integer up_vote;//点赞数
	private Integer down_vote;//差评数
	private String createTime;//
	private String updateTime;//
	private Integer available;//
	private Integer deleted;//

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getStatic_url() {
		return static_url;
	}

	public void setStatic_url(String static_url) {
		this.static_url = static_url;
	}

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getBook_id(){ return book_id ;}
	public void  setBook_id(String book_id){this.book_id=book_id; }
	public String getTitle(){ return title ;}
	public void  setTitle(String title){this.title=title; }
	public String getContent(){ return content ;}
	public void  setContent(String content){this.content=content; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public Integer getHit(){ return hit ;}
	public void  setHit(Integer hit){this.hit=hit; }
	public Integer getUp_vote(){ return up_vote ;}
	public void  setUp_vote(Integer up_vote){this.up_vote=up_vote; }
	public Integer getDown_vote(){ return down_vote ;}
	public void  setDown_vote(Integer down_vote){this.down_vote=down_vote; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }


	@Override
	public String toString() {
	return "ChapterDetail{" +
			"id=" + id+
			", book_id=" + book_id+
			", title=" + title+
			", content=" + content+
			", sort=" + sort+
			", hit=" + hit+
			", up_vote=" + up_vote+
			", down_vote=" + down_vote+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
