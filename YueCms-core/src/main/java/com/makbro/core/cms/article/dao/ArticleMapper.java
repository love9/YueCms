package com.makbro.core.cms.article.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.article.bean.Article;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 文章 dao
 * Created by wujiyue on 2016-08-14 20:02:46.
 */
@Repository
public interface ArticleMapper{



    public Article get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Article article);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Article> articles);
    public void update(Article article);
    public void updateByMap(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    public void batchPublish(String[] ids);
    public List<Article> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Article> findByMap(PageBounds pageBounds, Map<String, Object> map);

    //查询link符合reg表达式的
    @Deprecated
    public List<Article> findByRegxpLink(String reg);

    //从cms_article_content中获得内容
    public Map<String,Object> getArticleContent(String article_id);
    public void addArticleContent(Map<String, Object> map);
    //检查内容附表是否有该id记录
    public int checkContentExists(String article_id);
    public void updateArticleContent(Map<String, Object> map);

    public void deleteByMap(Map<String, Object> map);//删除排重

    public int checkExistsByTitleAndLink(@Param("title") String title, @Param("link") String link);

    public int getTotalCountByMap(Map<String, Object> map);
}
