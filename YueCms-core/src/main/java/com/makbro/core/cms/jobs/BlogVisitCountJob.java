package com.makbro.core.cms.jobs;

import com.makbro.core.cms.blog.task.UpdateBlogAuthorInfoTask;
import com.makbro.core.framework.quartz.jobs.BaseJob;
import com.markbro.base.common.util.thread.MultiThreadHandler;
import com.markbro.base.common.util.thread.exception.ChildThreadException;
import com.markbro.base.common.util.thread.parallel.ParallelTaskWithThreadPool;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.date.DateUtil;
import net.sf.ehcache.Cache;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * 每过30分钟，把计入缓存的博主访问次数累计到数据库表中并且放入缓存
 * 之所以把该定时任务从BlogTask提出来，是因为，该写法可以动态改变时间和动态的触发
 */
public class BlogVisitCountJob  extends BaseJob {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountTask===>Start");
        String[] yhids=getYhidsFromCache();
        runVisitCountTask(yhids);
        System.out.println(DateUtil.getDatetime()+"\t执行visitCountTask===>End！");


    }
    private void runVisitCountTask(String... yhids){

        Map<String, Object> resultMap = new HashMap<String, Object>(8, 1);
        //MultiThreadHandler handler = new MultiParallelThreadHandler();
        ExecutorService service = Executors.newFixedThreadPool(3);
        MultiThreadHandler handler = new ParallelTaskWithThreadPool(service);
        UpdateBlogAuthorInfoTask task = null;
        for(String yhid:yhids){
            task = new UpdateBlogAuthorInfoTask(yhid, resultMap);
            handler.addTask(task);
        }
        try {
            handler.run();
        } catch (ChildThreadException e) {
            System.out.println(e.getAllStackTraceMessage());
        }
        System.out.println(resultMap);
        service.shutdown();
    }
    private String[] getYhidsFromCache(){
        Cache cache =null;
        cache = EhCacheUtils.getCacheManager().getCache(EhCacheUtils.USER_CACHE);
        String prefix= UpdateBlogAuthorInfoTask.KEY_BLOG_VISIT_COUNT+"_";
        List<String> keysList = cache.getKeys();
        keysList=keysList.stream().filter(s->{return s.contains(prefix);}).collect(Collectors.toList());
        int count=keysList.size();
        String[] yhids=new String[count];
        int i=0;
        for(String s:keysList){
            yhids[i]=s.replaceAll(prefix,"");
            i++;
        }
        return yhids;
    }
}
