package com.makbro.core.cms.spider.core.pipeline.db;


import com.makbro.core.cms.article.dao.ArticleMapper;
import com.makbro.core.cms.article.service.ArticleService;
import com.makbro.core.cms.spider.core.ResultItems;
import com.makbro.core.cms.spider.core.Task;
import com.makbro.core.cms.spider.core.pipeline.BasePipeline;
import com.markbro.base.common.util.Guid;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by wjy on 2017/12/21.
 */
@Component("articlePipeline")
public class ArticlePipeline extends BasePipeline {

    @Autowired
    ArticleService articleServicer;
    @Autowired
    ArticleMapper articleMapper;
    public ArticlePipeline() {

    }



    @Override
    public void process(ResultItems resultItems, Task task) {

        super.process(resultItems,task);

        //先判断处理是否应该跳过该pipeline处理
        String title=resultItems.getAll().get("title").toString();
        String link=resultItems.getAll().get("link").toString();
        if(StringUtil.isEmpty(title)){//文章标题不写入数据库
            return;
        }


        //比如文章。标题和link在数据库中已经存在，应该跳过（之前是通过先删除排重的）。书籍通过书名bookname检测cms_book表中是否存在
        int n=articleMapper.checkExistsByTitleAndLink(title,link);
        if(n>0){
            logger.warn("title:{},link:{}在数据库中已经存在!", title, link);
            return;
        }

        Map map=resultItems.getAll();

        map.put("id", Guid.get());
        map.put("available",1);
        map.put("deleted",0);
        //map.put("static_url",path);这个字段在之前(TemplateMergerStringFilePipeline)生成，不在这里构造了
        map.put("hit",0);
        map.put("reply_num",0);
        map.put("up_vote",0);
        map.put("down_vote",0);

        String description=String.valueOf(map.get("description"));
        if(StringUtil.isEmpty(description)){
            //如果没有简介字段，那么根据正文截取40个字符串
            String content=String.valueOf(map.get("content"));
            if(StringUtil.notEmpty(content)&&content.length()>80){
                content=content.replaceAll("<p .*?>", "").replaceAll("<strong>1</strong>","").replaceAll("<br\\s*/?>", "").replaceAll("\\<.*?>", "").replaceAll("<strong .*?>1", "").replaceAll("<strong .*?>", "").replaceAll(" 1(.|、)?\\s+","").replaceAll("一\\s+","");
                map.put("description",content.substring(0,80)+"...");
            }
        }
        /*//先根据title和link排重
        Map<String,Object> delMap=new HashMap<String,Object>();
        delMap.put("title",map.get("title"));
        delMap.put("link",map.get("link"));
        articleMapper.deleteByMap(delMap);*/
        articleServicer.addByMap(map);
    }
}
