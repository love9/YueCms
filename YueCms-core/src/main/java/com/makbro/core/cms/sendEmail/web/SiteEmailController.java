package com.makbro.core.cms.sendEmail.web;


import com.makbro.core.base.orgUser.service.OrgUserService;
import com.makbro.core.cms.sendEmail.bean.SendEmail;
import com.makbro.core.cms.sendEmail.service.SendEmailService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.utils.SpringContextHolder;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * 站内邮件管理
 * Created by wujiyue on 2017-09-07 16:30:45.
 */

@Controller
@RequestMapping("/cms/siteEmail")
public class SiteEmailController extends BaseController {
    @Autowired
    protected SendEmailService sendEmailService;

    @RequestMapping(value={"","/"})
    @RequiresPermissions("cms.siteEmail")
    public ModelAndView index(){
        ModelAndView mav= new ModelAndView();
        Map map= MyBatisRequestUtil.getMap(request);
        String from=String.valueOf(map.get("from"));
        mav.addObject("from",from);
        mav.setViewName("/cms/siteEmail/mailbox");
        return mav;
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    @RequiresPermissions("cms.siteEmail")
    public ModelAndView toAdd(SendEmail sendEmail, Model model){
        ModelAndView mav= new ModelAndView();
        mav.setViewName("/cms/siteEmail/mail_add");
        return mav;
    }

   /**
    * 跳转到查看页面
    */
    @RequestMapping(value = "/view")
    @RequiresPermissions("cms.siteEmail")
    public ModelAndView toEdit(SendEmail sendEmail,Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String id= String.valueOf(map.get("id"));
        sendEmail=sendEmailService.get(id);
        if(StringUtil.isEmpty(sendEmail.getFeedback())){
            //标为已读
            map.put("feedback","1");
            sendEmailService.updateByMap(map);
        }
        ModelAndView mav= new ModelAndView();
        mav.addObject("sendEmail", sendEmail);
        mav.setViewName("/cms/siteEmail/mail_detail");
        return mav;
    }
    /**
     * 跳转到草稿编辑页面
     */
    /*@RequestMapping(value = "/editDraft")
    @RequiresPermissions("cms.siteEmail")
    public ModelAndView editDraft(SendEmail sendEmail,Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String id= String.valueOf(map.get("id"));
        sendEmail=sendEmailService.get(id);
        ModelAndView mav= new ModelAndView();
        mav.addObject("sendEmail", sendEmail);
        mav.setViewName("/cms/siteEmail/mail_add");
        return mav;
    }*/
    /**
     * 跳转到草稿编辑页面
     */
    @RequestMapping(value = "/editDraft")
    @RequiresPermissions("cms.siteEmail")
    public String editDraft(SendEmail sendEmail,Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String id= String.valueOf(map.get("id"));
        sendEmail=sendEmailService.get(id);
        if(sendEmail!=null){
            String toYhid=sendEmail.getToYhid();
            String assignToNames=null;//根据yhids取得名称
            OrgUserService orgUserService= SpringContextHolder.getBean("orgUserService");
            assignToNames=orgUserService.getYhmcsByYhids(toYhid);
            model.addAttribute("assignToNames", assignToNames);
        }

        model.addAttribute("sendEmail", sendEmail);
        return "/cms/siteEmail/mail_add";
    }

}