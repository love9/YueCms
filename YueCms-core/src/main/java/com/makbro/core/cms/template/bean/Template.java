package com.makbro.core.cms.template.bean;


import com.markbro.base.model.AliasModel;

/**
 * 模板管理 bean
 * @author  wujiyue on 2017-11-10 10:07:05 .
 */
public class Template  implements AliasModel {

	private String id;//模版编号
	private String orgid;//组织id
	private String name;//名称
	private String resourcetypeid;//资源类别id
	private String filePath;//模版路径
	private String fileName;//模版文件名
	private String content;//模版内容
	private String createBy;//创建者
	private String createTime;//创建时间
	private String updateBy;//更新者
	private String updateTime;//更新时间
	private String remarks;//备注信息
	private Integer available;//是否启用
	private Integer deleted;//删除标记（0：正常；1：删除）


	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getOrgid(){ return orgid ;}
	public void  setOrgid(String orgid){this.orgid=orgid; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getResourcetypeid(){ return resourcetypeid ;}
	public void  setResourcetypeid(String resourcetypeid){this.resourcetypeid=resourcetypeid; }
	public String getFilePath(){ return filePath ;}
	public void  setFilePath(String filePath){this.filePath=filePath; }
	public String getFileName(){ return fileName ;}
	public void  setFileName(String fileName){this.fileName=fileName; }
	public String getContent(){ return content ;}
	public void  setContent(String content){this.content=content; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public String getRemarks(){ return remarks ;}
	public void  setRemarks(String remarks){this.remarks=remarks; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }


	@Override
	public String toString() {
	return "Template{" +
			"id=" + id+
			", orgid=" + orgid+
			", name=" + name+
			", resourcetypeid=" + resourcetypeid+
			", filePath=" + filePath+
			", fileName=" + fileName+
			", content=" + content+
			", createBy=" + createBy+
			", createTime=" + createTime+
			", updateBy=" + updateBy+
			", updateTime=" + updateTime+
			", remarks=" + remarks+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
