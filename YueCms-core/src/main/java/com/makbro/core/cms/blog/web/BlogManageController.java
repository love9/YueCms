package com.makbro.core.cms.blog.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.clientinfo.bean.Clientinfo;
import com.makbro.core.base.clientinfo.service.ClientinfoService;
import com.makbro.core.base.login.service.LoginService;
import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.orgUser.dao.OrgUserMapper;
import com.makbro.core.base.orgUser.service.OrgUserService;
import com.makbro.core.cms.article.bean.Article;
import com.makbro.core.cms.article.bean.ArticleModel;
import com.makbro.core.cms.article.bean.ArticleRegionType;
import com.makbro.core.cms.article.service.ArticleService;
import com.makbro.core.cms.blog.service.BlogManageService;
import com.makbro.core.cms.book.bean.Book;
import com.makbro.core.cms.book.service.BookService;
import com.makbro.core.cms.chapterDetail.bean.ChapterDetail;
import com.makbro.core.cms.chapterDetail.service.ChapterDetailService;
import com.makbro.core.cms.comment.bean.Comment;
import com.makbro.core.cms.comment.service.CommentFilterService;
import com.makbro.core.cms.comment.service.CommentService;
import com.makbro.core.cms.resourcetype.service.ResourcetypeService;
import com.makbro.core.cms.tag.bean.Tags;
import com.makbro.core.cms.tag.service.TagsService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.SsoHelper;
import com.makbro.core.framework.authz.annotation.RequiresAnonymous;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.model.LoginBean;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.date.DateUtils;
import com.markbro.base.utils.random.RandomUtil;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.base.utils.test.TestUtil;
import com.markbro.base.utils.useragent.UserAgentUtils;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.DeviceType;
import org.apache.commons.fileupload.util.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@RequestMapping("/myblog")
@Controller
public class BlogManageController extends BaseController {

    private final static String ui="semantic";//页面风格
    private String getBasePath(){//返回视图的根路径
        if(StringUtil.isEmpty(ui)){
            return "/myblog";
        }
        return "/cms/myblog/"+ui;
    }
   @Autowired
   TagsService tagsService;
    @Autowired
    BookService bookService;
    @Autowired
    ChapterDetailService chapterDetailService;
    @Autowired
    ArticleService articleService;
    @Autowired
    OrgUserMapper orgUserMapper;
    @Autowired
    OrgUserService orgUserService;
    @Autowired
    CommentFilterService commentFilterService;
    @Autowired
    BlogManageService blogManageService;


    @Override
    protected void pushLoginUserInfo(Model model) {
        try{
            Map m= MyBatisRequestUtil.getMapGuest(request);
            if(m!=null){
                String yhid=String.valueOf(m.get("yhid"));
                Object o= EhCacheUtils.getUserInfo("userbean",yhid);
                LoginBean lb=(LoginBean)o;
                if(lb!=null){
                    model.addAttribute("front_yhid",lb.getYhid());
                    model.addAttribute("front_yhmc", lb.getXm());
                    String headerPath=(String)EhCacheUtils.getUserInfo("headerPath", lb.getYhid());
                    model.addAttribute("front_headerPath", headerPath);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * 搜索
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/search"})
    public String search(Model model){
        pushLoginUserInfo(model);
       String words=getParam("words");
        if(StringUtil.isEmpty(words)){
            model.addAttribute("msgObj", Msg.error("搜索内容不能为空!"));
            return getBasePath()+"/search_result";
        }
       //文字标题模糊搜索
        Map map=new HashMap();
        map.put("title",words);
        map.put("available","1");
        String pageNo=getParam("page");
        pageNo=StringUtil.isEmpty(pageNo)?"1":pageNo;
        int pageSize=6;//这个功能每页大小固定是6，所以不用父类的getPageBounds()方法
        PageBounds pageBounds=new PageBounds(Integer.valueOf(pageNo),pageSize);//
        List articles=articleService.find(pageBounds,map);
        //model.addAttribute("searchText",list);
        model.addAttribute("searchText",words);
        model.addAttribute("articles",articles);
        return getBasePath()+"/search_result";
    }
    /**
     * 跳转博客登录页面
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/login","/login/"})
    public String to_login(Model model){
        String redirectUrl=request.getParameter("redirectUrl");
        if(StringUtil.notEmpty(redirectUrl)){
            model.addAttribute("redirectUrl",redirectUrl);
        }else{
            model.addAttribute("redirectUrl","/myblog");//如果直接请求/myblog/login而没有传递redirectUrl会跳转到后台，为了解决直接请求该url跳转后台的问题，这里给个默认值
        }
        return getBasePath()+"/login/login";
    }
    //忘记密码
    @RequiresAnonymous
    @RequestMapping (value = {"/forget","/forget/"})
    public String to_forget(Model model){

        return getBasePath()+"/login/forget";
    }
    @Autowired
    LoginService loginService;

    @RequiresAnonymous
    @RequestMapping (value = {"/logout","/logout/"})
    public String logout(Model model){
        loginService.logout(request, response);
        SsoHelper.logout(request, response);
        return "redirect:/myblog";
    }
    /**
     * 跳转首页页面
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/index","/",""})
    public String index(Model model){
        pushLoginUserInfo(model);
        //查询各个专区文章
        //banner
        //model.addAttribute("banners",banners);
        //头条
        List<Article> tops= articleService.findByArticleRegionType(ArticleRegionType.REGION_TOP);
        model.addAttribute("tops",tops);
        //精选推荐
        List<Article> recommends= articleService.findByArticleRegionType(ArticleRegionType.REGION_RECOMMEND);
        model.addAttribute("recommends",recommends);
        //最新
        List<Article> news= articleService.findByArticleRegionType(ArticleRegionType.REGION_NEW);
        model.addAttribute("news",news);
        //热门
        List<Article> hots= articleService.findByArticleRegionType(ArticleRegionType.REGION_HOT);
        model.addAttribute("hots",hots);
        //fullTabs 标签
        List<Tags> fullTabsTags=tagsService.findFullTabs();
        model.addAttribute("fullTabs",fullTabsTags);
        return getBasePath()+"/index";
    }
    /**
     * 跳转关于我页面
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/aboutMe"})
    public String aboutMe(Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/aboutMe";
    }
    /**
     * 跳转demo页面
     */
    @RequiresAnonymous
    @RequestMapping ("/demo/{group}/{page}")
    public String demo(@PathVariable String group,@PathVariable String page,Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/demo/"+group+"/"+page;
    }
    @RequiresAuthentication
    @RequestMapping (value = {"/newblog"})
    public String newblog(Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/newblog";
    }
    @RequiresAuthentication
    @RequestMapping (value = {"/article_add"})
    public String article_add(Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/article_add";
    }
    @ResponseBody
    @RequestMapping(value="/article/json/save",method = RequestMethod.POST)
    @ActionLog(description="我的博客新增文章")
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        String copyFlag=String.valueOf(map.get("copy_flag")); //转载

        if("1".equals(copyFlag)){
            String link=String.valueOf(map.get("link"));
            String content=String.valueOf(map.get("content"));
            content+="<p style=\"text-align: right;\">\n" +
                    "    转载自：<a href=\""+link+"\" target=\"_self\">\""+link+"\"</a>\n" +
                    "</p>";
            map.put("content",content);
        }

        return  articleService.save(map);
    }
    //写博客成功后跳转的页面，让用户选择下一步进行什么
    @RequiresAuthentication
    @RequestMapping (value = {"/newblogSuccess"})
    public String newblogSuccess(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String newblogTitle=String.valueOf(map.get("title"));
        String id=String.valueOf(map.get("id"));
        if(StringUtil.notEmpty(newblogTitle) && StringUtil.notEmpty(id)){
            pushLoginUserInfo(model);
            model.addAttribute("title",newblogTitle);
            model.addAttribute("id",id);
            return getBasePath()+"/newblog_success";
        }else{
            throw new ApplicationException();
        }
    }

    /**
     * 某个用户的文章列表页面，如果userName是登录用户本人，那么跳转的页面不一样
     * @param account
     * @param model
     * @return
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/user/{account}/articles","/user/{account}/","/user/{account}"})
    public String userArticles(@PathVariable String account,Model model){
        pushLoginUserInfo(model);
        Map queryMap=MyBatisRequestUtil.getMap(request);
        queryMap.put("available","1");
        String loginYhid=String.valueOf(queryMap.get("yhid"));
        String yhid=orgUserMapper.getYhidByAccount(account);//根据传递的用户查询用户ID
        if(StringUtil.notEmpty(yhid)){
            OrgUser user=orgUserService.get(yhid);
            String headerPath=orgUserService.getHeaderPathByYhid(yhid);
            model.addAttribute("userHeaderPath",headerPath);
            model.addAttribute("userName",user.getAccount());
            if(loginYhid.equals(yhid)){
                List<Article> personalBlogs= articleService.find(getPageBounds(), queryMap);
                Map articles=  getPageMap(personalBlogs);
                model.addAttribute("articles",articles);
                List<Tags> list=tagsService.findPersonalTags(new PageBounds(),queryMap);
                model.addAttribute("personal_categorys",list);
                return getBasePath()+"/personalBlog";//个人博客页面
            }else{
                queryMap.put("yhid",yhid);
                List<Article> personalBlogs= articleService.find(getPageBounds(), queryMap);
                Map mres=  getPageMap(personalBlogs);
                model.addAttribute("articles",mres);
                queryMap.put("yhid",yhid);
                List<Tags> list=tagsService.findPersonalTags(new PageBounds(),queryMap);

                model.addAttribute("personal_categorys",list);
                this.addArticle_archives(model);
                return getBasePath()+"/userBlog";//访问别人的博客主页
            }
        }else{
            throw new ApplicationException("404","不存在该用户！");
        }

    }
    //模拟存档
    private void addArticle_archives(Model model){
        List<Map<String,Object>> article_archives=new ArrayList<Map<String,Object>>(); //article_archives 文章按照月份存档
        Map<String,Object> tmap=new HashMap<String,Object>();
        tmap.put("name","2018年1月");
        tmap.put("month","2018/01");
        article_archives.add(tmap);
        tmap=new HashMap<String,Object>();
        tmap.put("name","2017年12月");
        tmap.put("month","2017/12");
        article_archives.add(tmap);
        model.addAttribute("article_archives",article_archives);
    }
    /**
     * 文章存档
     * @param account
     *          用户账户
     * @param year
     *          年
     * @param month
     *          月
     * @param model
     * @return
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/user/{account}/articles/month/{year}/{month}"})
    public String article_archives(@PathVariable String account,@PathVariable String year,@PathVariable String month,Model model){
        if(StringUtil.isEmpty(account)||StringUtil.isEmpty(year)||StringUtil.isEmpty(month)){
            throw new ApplicationException("参数异常!");
        }else{
            String month_temp="";
            try{
                int y=Integer.valueOf(year);
                if(y>=1990&&y<=2100){
                    //年份正常
                }else{
                    throw new ApplicationException("参数异常!");
                }
                if(month.startsWith("0")){
                    month_temp=month.substring(0,1);
                }
                int m=Integer.valueOf(month_temp);
                if(m>0&&m<=12){
                    //月份在1-12月
                }else{
                    throw new ApplicationException("参数异常!");
                }
            }catch (Exception e){
                throw new ApplicationException("参数异常!");
            }
        }
        pushLoginUserInfo(model);
        Map queryMap=MyBatisRequestUtil.getMap(request);
        String loginYhid=String.valueOf(queryMap.get("yhid"));
        String yhid=orgUserMapper.getYhidByAccount(account);//根据传递的用户查询用户ID

        if(StringUtil.notEmpty(yhid)){
            OrgUser user=orgUserService.get(yhid);
            String headerPath=orgUserService.getHeaderPathByYhid(yhid);
            model.addAttribute("userHeaderPath",headerPath);
            model.addAttribute("userName",user.getAccount());
            if(loginYhid.equals(yhid)){
                queryMap.put("yhid",loginYhid);
                List<Article> personalBlogs= articleService.find(getPageBounds(), queryMap);
                Map mres=  getPageMap(personalBlogs);
                model.addAttribute("articles",mres);
                List<Tags> list=tagsService.findPersonalTags(new PageBounds(),queryMap);
                model.addAttribute("personal_categorys",list);
                return getBasePath()+"/personalBlog";//个人博客页面
            }else{
                Map<String,Object>  author_info = blogManageService.getBlogAuthorInfo(yhid);
                queryMap.put("yhid",yhid);

                List<Article> personalBlogs= articleService.find(getPageBounds(), queryMap);
                Map mres=  getPageMap(personalBlogs);
                model.addAttribute("articles",mres);
                model.addAttribute("author_info", author_info);//博客作者个人信息
                List<Tags> list=tagsService.findPersonalTags(new PageBounds(),queryMap);
                model.addAttribute("personal_categorys",list);
                this.addArticle_archives(model); //article_archives 模拟文章按照月份存档
                return getBasePath()+"/userBlog";
            }

        }else{
            throw new ApplicationException("404","不存在该用户！");
        }

    }
    //博客文章列表页面的标签云
    private void addHotTags(Model model){
        List<Tags> tags=(List<Tags>)EhCacheUtils.getSysInfo(TagsService.KEY_ARTICLE_TAGS,"_1");//取得第一页
        model.addAttribute("tags",tags);
    }
    /**
     * 个人博客管理
     * @param model
     * @return
     */
    @RequiresAuthentication
    @RequestMapping (value = {"/personalBlog"})
    public String personalBlog(Model model){
        pushLoginUserInfo(model);
        Map queryMap=MyBatisRequestUtil.getMap(request);
        queryMap.put("available","1");
        List<Article> personalBlogs= articleService.find(getPageBounds(), queryMap);
        Map articles=  getPageMap(personalBlogs);
        model.addAttribute("articles",articles);
        this.addHotTags(model);
        List<Tags> list=tagsService.findPersonalTags(new PageBounds(),queryMap);//个人分类标签
        model.addAttribute("personal_categorys",list);

        //String blog_yhid=MyBatisRequestUtil.getYhid();
        String blog_yhid=String.valueOf(queryMap.get(TmConstant.YHID_KEY));
        Map<String,Object> author_info=blogManageService.getBlogAuthorInfo(blog_yhid);
        model.addAttribute("author_info", author_info);//博客作者个人信息

        return getBasePath()+"/personalBlog";
    }
    @Autowired
    ResourcetypeService resourcetypeService;

    @RequiresAuthentication
    @RequestMapping (value = {"/personalBlog/category/{category_id}"})
    public String personalBlogByCate(@PathVariable String category_id,Model model){
        pushLoginUserInfo(model);
        Map queryMap=MyBatisRequestUtil.getMap(request);
        queryMap.put("available","1");
        queryMap.put("personal_category",category_id);
        List<Article> personalBlogs= articleService.find(getPageBounds(), queryMap);
        Map articles=  getPageMap(personalBlogs);
        model.addAttribute("articles",articles);
        this.addHotTags(model);
        queryMap.put("id",category_id);
        List<Tags> list=tagsService.findPersonalTags(new PageBounds(),queryMap);//个人分类标签
        model.addAttribute("personal_categorys",list);
        return getBasePath()+"/personalBlog";
    }
    /*********************测试页面**************************/



    /**
     * 跳转文章页面
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/article"})
    public String article(Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/article";
    }

    /**
     * 跳转图片页面
     */
    @RequiresAnonymous
    @RequestMapping (value = {"/img"})
    public String img(Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/img";
    }



    @RequiresAnonymous
    @RequestMapping ("/bloglist")
    public String bloglist(Model model){
        pushLoginUserInfo(model);
        Map<String,Object>  queryMap=MyBatisRequestUtil.getMapGuest(request);
        queryMap.put("yhid","");// 这样可以达到用户登录后查询也不会只能查到自己的记录
        queryMap.put("available","1");//只查询发布的文章
        List<Article> articles= articleService.find(getPageBounds(), queryMap);
        Map mres=  getPageMap(articles);
        model.addAttribute("articles",mres);

        this.addHotTags(model);
        return getBasePath()+"/bloglist";
    }
    @RequiresAnonymous
    @RequestMapping ("/bloglist/tag/{tag}")
    public String bloglist_tags(Model model,@PathVariable String tag){
        if("{tag}".equals(tag)||StringUtil.isEmpty(tag)){
            return "redirect:/myblog/bloglist";
        }
        pushLoginUserInfo(model);
        Map queryMap=MyBatisRequestUtil.getMapGuest(request);
        queryMap.put("yhid","");//这样防止用户登录只能查看自己的数据
        queryMap.put("available","1");
        queryMap.put("tag",tag);
        List<Article> articles= articleService.find(getPageBounds(), queryMap);
        Map mres=  getPageMap(articles);
        model.addAttribute("articles",mres);
        //模拟向前台输出标签
        this.addHotTags(model);
        return getBasePath()+"/bloglist";
    }

    @RequiresAuthentication
    @RequestMapping ("/moban/{name}")
    public String moban(Model model,@PathVariable String name){
        pushLoginUserInfo(model);
        Map<String,Object>  queryMap=MyBatisRequestUtil.getMap(request);
        return "/moban/"+name;
    }


    @Autowired
    ClientinfoService clientinfoService;

    /**
     * 用户点击文章页面，用来记录该文章被访问过
     * @return
     */
    @ResponseBody
    @RequiresAnonymous
    @RequestMapping ("/page/view")
    public String viewArticle(){

        Map<String,Object> map =MyBatisRequestUtil.getMapGuest(request);
        String referer = request.getHeader("referer"); map.put("referer",referer);
        Browser browserObj = UserAgentUtils.getBrowser(request);
        String browser=browserObj.getName();//浏览器类型
        map.put("browser",browser);
        String deviceType="Unknown";//设备类型
        DeviceType deviceType1=UserAgentUtils.getDeviceType(request);//是否pc
        if(deviceType1!=null){
            deviceType=deviceType1.getName();
        }
        map.put("deviceType",deviceType);
        String ip=MyBatisRequestUtil.getIpByHttpRequest(request);map.put("ip",ip);
        String host=request.getHeader("host");map.put("host",host);
        String uri=request.getRequestURI();map.put("uri",uri);
        String url=request.getRequestURL().toString(); map.put("currentUrl",url);
        Clientinfo clientinfo= JSON.parseObject(JSON.toJSONString(map),Clientinfo.class);
        clientinfoService.add(clientinfo);

       // blogManageService.blogVisit();
        return "1";
    }
    //动态请求 博客文章详情New
    @RequiresAnonymous
    @RequestMapping ("/article/detail/{id}")
    public String article_detail(@PathVariable String id, Model model){
        pushLoginUserInfo(model);
        Article article= articleService.get(id);
        if(article==null){
            throw new ApplicationException("404","您要访问的页面不存在!");
        }
        article.setHit(article.getHit()+1);//阅读数+1
        articleService.update(article);//阅读数+1
        Map queryMap=MyBatisRequestUtil.getMapGuest(request);
        model.addAttribute("article",article);
        blogManageService.blogVisit(article.getYhid());

        //随机一个打赏按钮的样式
        String[] rewardBtnClass={"cy-reward-btn-red","cy-reward-btn-green","cy-reward-btn-orange","cy-reward-btn-blue"};
        int index= RandomUtil.getARanNum(4)-1;
        model.addAttribute("rewardBtnClass",rewardBtnClass[index]);
        String article_model=  article.getArticle_model();//文章模型
        if(article_model.equals(ArticleModel.DUOGUYU.getVal())){
            List<Tags> fullTabs=tagsService.findFullTabs();
            model.addAttribute("fullTabs",fullTabs);
            articleService.processTag(article,model);
            Map dataMap= JSONObject.parseObject(JSON.toJSONString(article),Map.class);
            model.addAllAttributes(dataMap);
            return getBasePath()+"/article-duoguyu";
        }else{
            List<Tags> list=tagsService.findPersonalTags(new PageBounds(),queryMap);//个人分类标签
            model.addAttribute("personal_categorys", list);
            Map<String,Object> author_info=new HashMap<String,Object>();
            String blog_yhid=article.getYhid();
            author_info=blogManageService.getBlogAuthorInfo(blog_yhid);
            model.addAttribute("author_info", author_info);//博客作者个人信息
            return getBasePath()+"/article-detail";
        }


    }
    @RequiresAnonymous
    @RequestMapping ("/booklist")
    public String booklist(Model model){
        pushLoginUserInfo(model);
        Map queryMap=MyBatisRequestUtil.getMapGuest(request);
        queryMap.put("yhid","");// 这样可以达到用户登录后查询也不会只能查到自己的记录
        List<Book> bookses= bookService.find(getPageBounds(), queryMap);
        Map mres=  getPageMap(bookses);
        model.addAttribute("books",mres);
        //模拟向前台输出标签
        this.addBookYunTags(model);
        return getBasePath()+"/booklist";
    }
    @RequiresAnonymous
    @RequestMapping ("/booklist/tags/{tags}")
    public String booklist(Model model,@PathVariable String tags){
        if("{tags}".equals(tags)||StringUtil.isEmpty(tags)){
            return "redirect:/myblog/booklist";
        }
        pushLoginUserInfo(model);
        Map queryMap=MyBatisRequestUtil.getMapGuest(request);
        queryMap.put("yhid","");//这样防止用户登录只能查看自己的数据

        queryMap.put("tags",tags);
        List<Book> bookses= bookService.find(getPageBounds(), queryMap);
        Map mres=  getPageMap(bookses);
        model.addAttribute("books",mres);

        //模拟向前台输出标签
        this.addBookYunTags(model);
        return getBasePath()+"/booklist";
    }
    //书籍标签云
    private void addBookYunTags(Model model){
        List<String> tags=(List<String>) EhCacheUtils.getSysInfo(BookService.KEY_BOOK_TAGS);
        model.addAttribute("tags",tags);
    }
    //获取书籍详情
    @RequiresAnonymous
    @RequestMapping ("/book/detail/{id}")
    public String book_detail(@PathVariable String id, Model model){
        pushLoginUserInfo(model);
        Book book=bookService.get(id);
        if(book!=null){
            book = bookService.getBookAndChapterList(book.getId());
            model.addAttribute("book",book);
           /* Map map=new HashMap<String,Object>();
            map.put("book_id",id);
            List<ChapterDetail> details= chapterDetailService.find(new PageBounds(), map);
            //按在sort字段给集合排序
            details=details.stream().sorted((chapterDetail1,chapterDetail2)->{return chapterDetail1.getSort().compareTo(chapterDetail2.getSort());}).collect(Collectors.toList());
            model.addAttribute("chapterDetails",details);*/
            return getBasePath()+"/book_detail";
        }else{
            return "redirect:/myblog/booklist";
        }
    }
    //@Autowired
    //WordService wordService;
    //获取书籍章节详情
    @RequiresAnonymous
    @RequestMapping ("/chapterDetail/{id}")
    public String chapter_detail(@PathVariable String id, Model model){
        pushLoginUserInfo(model);
        ChapterDetail detail=chapterDetailService.get(Integer.valueOf(id));
        //String content=wordService.wrapperText(detail.getContent());
        //String content= ArticleReplaceUtil.wrapTextBySpan(detail.getContent());
        //detail.setContent(content);
        ChapterDetail pre_chapter=null;
        ChapterDetail next_chapter=null;
        if(detail!=null){
            model.addAttribute("detail",detail);
            Book book=bookService.get(detail.getBook_id());
            int sort=detail.getSort();
            model.addAttribute("book",book);
            if(sort>1){
                pre_chapter=chapterDetailService.getByBookidAndSort(detail.getBook_id(),sort-1);
            }
            next_chapter=chapterDetailService.getByBookidAndSort(detail.getBook_id(),sort+1);
            model.addAttribute("pre_chapter",pre_chapter);
            model.addAttribute("next_chapter",next_chapter);
            //获取章节列表
            Map map=new HashMap<String,Object>();
            map.put("book_id",detail.getBook_id());
            List<ChapterDetail> details= chapterDetailService.find(new PageBounds(), map);
            //按在sort字段给集合排序
            details=details.stream().sorted((chapterDetail1,chapterDetail2)->{return chapterDetail1.getSort().compareTo(chapterDetail2.getSort());}).collect(Collectors.toList());
            model.addAttribute("chapterDetails", details);
            return getBasePath()+"/chapter_detail";
        }else{
            return "redirect:/myblog/book/detail/"+id;
        }
    }


    //评论列表
    @RequiresAuthentication
    @ResponseBody
    @RequestMapping ("/blog/comment")
    public Object commentBlogArticle(Model model){
        Map<String,Object> res=new HashMap<String,Object>();
        try{
            Map map=MyBatisRequestUtil.getMap(request);
            LoginBean loginBean=MyBatisRequestUtil.getLoginUserInfo(request);
            res.put("type","error");

            String article_id=(String)map.get("id");
            String book_id=(String)map.get("book_id");
            String chapter_id=(String)map.get("chapter_id");
            boolean bookflag=false;
            boolean chapterFlag=false;
            if(StringUtil.notEmpty(book_id)){
                bookflag=true;
            }
            if(StringUtil.notEmpty(chapter_id)){
                chapterFlag=true;
            }
            String yhid=(String)map.get(TmConstant.YHID_KEY);
            String content=(String)map.get("content");
            if((StringUtil.isEmpty(article_id) && StringUtil.isEmpty(book_id)&& StringUtil.isEmpty(chapter_id))||StringUtil.isEmpty(yhid)||StringUtil.isEmpty(content)){
                res.put("msg","服务器异常!");
                return res;
            }
            String usericon=(String) EhCacheUtils.getUserInfo("headerPath",yhid);
            Comment comment=new Comment();
            if(bookflag){
                comment.setType("book");
                comment.setTarget_id(book_id);
            }else{
                if(chapterFlag){
                    comment.setType("chapter");
                    comment.setTarget_id(chapter_id);
                }else{
                    comment.setType("article");
                    comment.setTarget_id(article_id);
                }
            }

            comment.setYhid(yhid);
            String yhmc=(String) EhCacheUtils.getUserInfo(TmConstant.XM_KEY,yhid);
            comment.setUsername(yhmc);
            comment.setUsericon(usericon);
            comment.setComment(content);
            comment.setAvailable(1);
            comment.setDeleted(0);
            comment.setUp_vote(0);
            comment.setDown_vote(0);

            Map<String,Object> temp=   commentFilterService.filter(comment.getComment());
            String content_new=(String)temp.get("new");
            boolean flag=(Boolean)temp.get("flag");
            if(flag){
                List<String> list = (List<String>) temp.get("list");
                logger.warn("用户id:【{}】,用户名称：【{}】在评论中发表不良言论!article_id:【{}】,type:【{}】",map.get(TmConstant.YHID_KEY),map.get("yhmc"),comment.getTarget_id(),comment.getType());
                TestUtil.printList(list);
                comment.setComment(content_new);
            }

            commentService.add(comment);
            res.put("type", "success");
            res.put("msg","评论成功!");

            res.put("author", comment.getUsername());

            res.put("time", DateUtils.formatDateTime(new Date()));
            res.put("comment",comment.getComment());
            res.put("comment_id",comment.getId());
            res.put("headPath", loginBean.getUserMap().get("headerPath"));
        }catch (Exception ex){
            logger.error("commentBlogArticle出现了异常："+ex.getMessage());
            res.put("msg", "服务器异常!");
        }
        return res;
    }
    @Autowired
    CommentService commentService;
    //文章评论列表
    @RequiresAnonymous
    @ResponseBody
    @RequestMapping ("/blog/comment/list")
    public Object comment_list(Model model){
        Msg msg=new Msg();
        try{
            Map map=MyBatisRequestUtil.getMapGuest(request);
            //String myblog=(String)map.get("myblog");
                String book_id=String.valueOf(map.get("book_id"));
                String chapter_id=String.valueOf(map.get("chapter_id"));
                if(StringUtil.notEmpty(chapter_id)){
                    map.put("type","chapter");
                    map.put("target_id",chapter_id);
                }else if(StringUtil.notEmpty(book_id)){
                    map.put("type","book");
                    map.put("target_id",book_id);
                }else{
                    map.put("type","article");
                    map.put("target_id", String.valueOf(map.get("id")));
                }

                map.put("id","");
                return commentService.find(getPageBounds(),map);

        }catch (Exception ex){
            logger.error("获取评论列表出现了异常："+ex.getMessage());
            msg.setType(Msg.MsgType.error);
            msg.setContent("服务器异常!");
        }
        return msg;
    }
    //新建博客文章上传图片
    private String uploadSingle(Map<String, Object> map, HttpServletRequest req, MultipartFile mFile) {
        //资源图片路径(相对于webapp下的路径)
        String result="";
        String basePathType ="0";
        try {
            req.setCharacterEncoding("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        basePathType = GlobalConfig.getSysPara("uploadpath_type","0");
        String basepath="";
        String relativePath="";
        if("1".equals(basePathType)){
            basepath= GlobalConfig.getSysPara("uploadfile_basepath","D:/dzd");
            relativePath="/uploadresources";
        }else{
            basepath=req.getSession().getServletContext().getRealPath("/");
            relativePath= GlobalConfig.getSysPara("uploadfile_basepath","/resources/static/cms/images");
        }
        relativePath= StringUtil.subEndStr(relativePath, "/");
        File dirFile=new File(basepath+relativePath);
        if(!dirFile.exists()){
            dirFile.mkdirs();
        }
        try {
            long size = 0;
            String path = "";
            String filename = "";
            String name = "";
            String dir = "";
            String suffixes ="";
            String guid= Guid.get();
            if (!mFile.isEmpty()) {
                BufferedInputStream in = new BufferedInputStream(mFile.getInputStream());
                filename = mFile.getOriginalFilename();
                name=filename.substring(0,filename.indexOf("."));
                // 取得文件后缀
                suffixes = filename.substring(filename.lastIndexOf("."), filename.length());
                path= relativePath+"/"+ DateUtil.formatDate(new Date(), "yyyyMMdd")+"/"+guid + suffixes;//相对路径
                dir = basepath+File.separatorChar+path;// 绝对路径
                String tempDir=dir.substring(0,dir.lastIndexOf("/"));
                File tempDirFile=new File(tempDir);
                if(!tempDirFile.exists()){
                    tempDirFile.mkdirs();
                }
                File ffout = new File(dir);
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(ffout));
                Streams.copy(in, out, true);
                size = ffout.length();
            }
            map.put("url", path);
            map.put("success", "1");
            map.put("message", "图片"+name+"上传成功!");
            result="{\"success\":\"1\",\"message\":\"图片"+name+"上传成功!\",\"url\":\"\"+path+\"\"}";
        }catch (IOException e) {
            e.printStackTrace();
            map.put("success", "0");
            map.put("message", "图片上传失败!");
            result="{\"success\":\"0\",\"message\":\"图片上传异常!\"}";
        }
        return result;
    }
    //上传单张图片资源.editormd 专用
    @RequestMapping("/uploadSingle")
    @RequiresAuthentication
    public void uploadSingle() {
        String res="";
        Map map = MyBatisRequestUtil.getMapGuest(request);
        PrintWriter write = null;
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("editormd-image-file");//注意这里的文件名是editormd 专用
        if(multipartFile != null){
            res = uploadSingle(map, request, multipartFile);
        }
        try {
            write = response.getWriter();
            write.write(res);
            write.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response = null;
            if (write != null){
                write.close();
            }
            write = null;
        }
    }

    //写博客
    @RequiresAuthentication
    @ResponseBody
    @RequestMapping ("/blog/save")
    public Object saveNewBlog(Model model){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.error);
        try {
            Article article=new Article();
            Map map=MyBatisRequestUtil.getMap(request);
            String isprivate=String.valueOf(map.get("isprivate"));
            if("true".equals(isprivate)||"1".equals(isprivate)){
                isprivate="1";
            }else{
                isprivate="0";
            }
            String content=String.valueOf(map.get("content"));
            String id=String.valueOf(map.get("id"));
            String sourceContent=String.valueOf(map.get("sourceContent"));
            String title=String.valueOf(map.get("title"));
            String tags=String.valueOf(map.get("bkbq"));
            //组装成一定格式的标签内容{name:"",newflag:"1"},这样articleService.save方法才能处理标签
            String[] tagArr=tags.split(",");
            List<Map<String,String>> tagList=new ArrayList<>();
            if(tagArr!=null&&tagArr.length>0){
                for(String s:tagArr){
                    Map<String,String> temp=new HashMap<>();
                    temp.put("name",s);
                    temp.put("newflag","1");
                    tagList.add(temp);
                }
            }
            tags=JSON.toJSONString(tagList);
            article.setTags(tags);
            //String isDraft=String.valueOf(map.get("draft"));
            if(StringUtil.isEmpty(title)){
                msg.setContent("标题不能为空!");
                return msg;
            }
            if(StringUtil.isEmpty(content)){
                msg.setContent("博客内容不能为空!");
                return msg;
            }

            if(StringUtil.notEmpty(id)){
                article.setId(id);
            }
            article.setYhid(String.valueOf(map.get(TmConstant.YHID_KEY)));
            article.setComment_flag(1);
            article.setTitle(title);
            //article.setDatascope(isprivate);
            article.setContent_markdown_source(sourceContent);
            article.setContent(content);
            article.setDeleted(0);
            article.setAvailable(1);
            Map m = JSON.parseObject(JSON.toJSONString(article));
            return articleService.save(m);

        }catch (Exception ex){
            logger.error("保存博客异常!"+ex.getMessage());
            msg.setContent("保存博客异常!");
        }
        return msg;
    }

    /*********************************个人博客管理**************************************/

    //文章列表管理
    @RequiresAuthentication
    @RequestMapping (value = {"/admin/postlist"})
    public String postlist(Model model){
        pushLoginUserInfo(model);
        Map<String,Object>  queryMap=MyBatisRequestUtil.getMap(request);
        queryMap.put("available","1");
        List<Article> articles= articleService.find(getPageBounds(), queryMap);
        for(Article t:articles){
            t.setCreateTime(t.getCreateTime().substring(0,10));
        }
        Map mres=  getPageMap(articles);
        model.addAttribute("articles", mres);
        return getBasePath()+"/admin/postlist";
    }
    //文章禁止/允许评论
    @ResponseBody
    @RequiresAuthentication
    @RequestMapping (value = {"/admin/post_updateCommentFlag"})
    public Object post_updateCommentFlag(Model model){
        Map<String,Object>  queryMap=MyBatisRequestUtil.getMap(request);
        String id=String.valueOf(queryMap.get("id"));//要删除文章的ID
        String yhid=String.valueOf(queryMap.get("yhid"));//当前请求用户的yhid
        Article article= articleService.get(id);
        if(article!=null){
            if(yhid.endsWith(article.getYhid())){
                //是自己的文章才能操作
                int  n = article.getComment_flag()!=null&&article.getComment_flag()==1?1:0;
                article.setComment_flag(n == 1 ? 0 : 1);
                articleService.update(article);
                return Msg.success(n==1?"禁止评论成功!":"允许评论成功!");
            }
        }
        return Msg.error("错误的请求!");
    }
    //文章置顶
    @ResponseBody
    @RequiresAuthentication
    @RequestMapping (value = {"/admin/post_setTop"})
    public Object post_setTop(Model model){
        Map<String,Object>  queryMap=MyBatisRequestUtil.getMap(request);
        String id=String.valueOf(queryMap.get("id"));//要删除文章的ID
        String yhid=String.valueOf(queryMap.get("yhid"));//当前请求用户的yhid
        Article article= articleService.get(id);

        if(article!=null){
            if(yhid.endsWith(article.getYhid())){
                //是自己的文章才能操作
                int n = article.getTop_flag()!= null&&article.getTop_flag()==1?1:0;
                article.setTop_flag(n==1?0:1);
                articleService.update(article);
                return Msg.success(n==1?"取消置顶成功":"置顶成功!");
            }
        }
        return Msg.error("错误的请求!");
    }
    //删除文章
    @ResponseBody
    @RequiresAuthentication
    @RequestMapping (value = {"/admin/post_delete"})
    public Object post_delete(Model model){
        Map<String,Object>  queryMap=MyBatisRequestUtil.getMap(request);
        String id=String.valueOf(queryMap.get("id"));//要删除文章的ID
        String yhid=String.valueOf(queryMap.get("yhid"));//当前请求用户的yhid
        Article article= articleService.get(id);
        if(article!=null){
            if(yhid.endsWith(article.getYhid())){//是自己的文章才能删除
                articleService.delete(id);
                return Msg.success("删除成功!");
            }
        }
        return Msg.error("错误的请求!");
    }
    //个人分类管理
    @RequiresAuthentication
    @RequestMapping (value = {"/admin/category_list"})
    public String category(Model model){
        pushLoginUserInfo(model);
        Map map= MyBatisRequestUtil.getMap(request);
        List<Tags> tagses= tagsService.findPersonalTags(new PageBounds(), map);
        model.addAttribute("category_list",tagses);
        return getBasePath()+"/admin/category_list";
    }
    @RequiresAuthentication
    @RequestMapping (value = {"/admin/personal_settings"})
    public String personal_settings(Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/admin/personal_settings";
    }

    /*@ResponseBody
    @RequiresAuthentication
    @RequestMapping (value = {"/admin/category/list"})
    public Object category_list(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        List<Tags> tagses= tagsService.find(new PageBounds(), map);
        return tagses;
    }*/
    @RequiresAnonymous
    @RequestMapping (value = {"/test/blank"})
    public String test_blank(Model model){
        return getBasePath()+"/blank";
    }
    @RequiresAnonymous
    @RequestMapping (value = {"/updateLog"})
    public String updateLog(Model model){
        pushLoginUserInfo(model);
        return getBasePath()+"/updateLog";
    }

}
