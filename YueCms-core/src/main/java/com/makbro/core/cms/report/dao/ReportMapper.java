package com.makbro.core.cms.report.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.cms.report.bean.Report;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 举报 dao
 * @author  wujiyue on 2018-07-27 15:22:31.
 */
@Repository
public interface ReportMapper{

    public Report get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Report report);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Report> reports);
    public void update(Report report);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    public List<Report> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Map<String,Object>> findByMap(PageBounds pageBounds, Map<String, Object> map);


    //查询是否已经被举报过
    public int checkExists(@Param("module") String module, @Param("target_id") String target_id, @Param("report_type") String report_type);

}
