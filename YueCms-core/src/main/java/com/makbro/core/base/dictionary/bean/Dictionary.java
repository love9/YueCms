package com.makbro.core.base.dictionary.bean;


import com.markbro.base.model.AliasModel;

/**
 * 数据字典 bean
 * @author  by wujiyue on 2016-07-05 22:19:53.
 */
public class Dictionary  implements AliasModel {
	private Integer id;//
	private String type;//类型
	private String value;//值
	private String label;//内容
	private String description;//类型的描述
	private String createTime;//
	private String updateTime;//
	private String createBy;//添加者用户id
	private Integer updateBy;//添加者用户id
	private Integer sort;//排序
	private Integer available;//可用标志
	private Integer deleted;//删除标志

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getType(){ return type ;}
	public void  setType(String type){this.type=type; }
	public String getValue(){ return value ;}
	public void  setValue(String value){this.value=value; }
	public String getLabel(){ return label ;}
	public void  setLabel(String label){this.label=label; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public Integer getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(Integer updateBy){this.updateBy=updateBy; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Dictionary{" +
			"id=" + id+
			", type=" + type+
			", value=" + value+
			", label=" + label+
			", description=" + description+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", createBy=" + createBy+
			", updateBy=" + updateBy+
			", sort=" + sort+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
