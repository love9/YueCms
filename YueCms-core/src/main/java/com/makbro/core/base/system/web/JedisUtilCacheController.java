package com.makbro.core.base.system.web;

import com.makbro.core.base.system.service.JedisUtilCacheService;
import com.makbro.core.framework.BaseController;
import com.markbro.base.utils.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/sys/cache/redis")
public class JedisUtilCacheController extends BaseController {

    @Autowired
    JedisUtilCacheService jedisUtilCacheService;

    @RequestMapping(value={"","/"})
    public String index(){
        return "/sys/cache/redis/list";
    }
    @RequestMapping("/viewCacheInfoByKey")
    public String viewCacheInfo(HttpServletRequest req, Model model){

        String key=req.getParameter("key");
        if(key.contains("$")){
            key=key.replace("$","#") ;
        }
        Map<String,Object> info= jedisUtilCacheService.viewCacheInfoByKey(key);
        model.addAttribute("info",info);
        return "/sys/cache/redis/viewInfo";
    }
    @ResponseBody
    @RequestMapping("/json/getCacheInfo")
    public Object getCacheInfo(HttpServletRequest req){
        Map map= ServletUtil.getMap(req);
        return jedisUtilCacheService.getCacheInfo(map);
    }

    /**
     * 清除缓存信息
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping("/json/removeCacheByKey")
    public Object removeCacheByKey(HttpServletRequest req){
        String key=req.getParameter("key");
        if(key.contains("$")){
            key=key.replace("$","#") ;
        }
        return jedisUtilCacheService.removeCacheByKey(key);
    }
}
