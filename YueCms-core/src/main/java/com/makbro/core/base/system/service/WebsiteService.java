package com.makbro.core.base.system.service;

import com.makbro.core.base.system.dao.WebsiteMapper;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.date.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.Map;

@Service
public class WebsiteService {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    WebsiteMapper websiteMapper;
    public Map getSiteInfo(){
        Map map=websiteMapper.getSiteInfo();
        if (!CollectionUtils.isEmpty(map)) {
            // 获取建站天数

            map.put("buildSiteDate", DateUtil.differentDaysByMillisecond(GlobalConfig.buildWebsiteDate, new Date()));
        }
        return map;
    }
}
