package com.makbro.core.base.actionlog.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.actionlog.bean.Actionlog;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 操作日志 dao
 * Created by wujiyue on 2017-11-03 13:36:44.
 */
@Repository
public interface ActionlogMapper{

    public Actionlog get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Actionlog actionlog);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Actionlog> actionlogs);
    public void update(Actionlog actionlog);
    public void updateByMap(Map<String, Object> map);

    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);

    public List<Actionlog> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Actionlog> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
