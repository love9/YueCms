package com.makbro.core.base.login.bean;

import java.io.Serializable;

/**
 * 畅言 获取用户信息接口，返回的参数列表用两个Java Bean表示
 */
public class ChangYanUserinfo implements Serializable {
    public int is_login;//是否登录，0表示未登录，1表示已登录

    public ChangYanUser user; //用户信息

    public int getIs_login() {
        return is_login;
    }

    public void setIs_login(int is_login) {
        this.is_login = is_login;
    }

    public ChangYanUser getUser() {
        return user;
    }

    public void setUser(ChangYanUser user) {
        this.user = user;
    }
}
