package com.makbro.core.base.system.oss.mimeType.web;

import com.makbro.core.base.system.oss.mimeType.bean.MimeType;
import com.makbro.core.base.system.oss.mimeType.service.MimeTypeService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 文件类型管理
 * Created by wujiyue on 2018-06-27 09:33:52.
 */

@Controller
@RequestMapping("/base/mimeType")
public class MimeTypeController extends BaseController {
    @Autowired
    protected MimeTypeService mimeTypeService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/base/mimeType/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(MimeType mimeType, Model model){
                return "/base/mimeType/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(MimeType mimeType,Model model){
                if(mimeType!=null&&mimeType.getId()!=null){
            mimeType=mimeTypeService.get(mimeType.getId());
        }
         model.addAttribute("mimeType",mimeType);
         return "/base/mimeType/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return mimeTypeService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return mimeTypeService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return mimeTypeService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除文件类型")
    public Object delete(@PathVariable Integer id) {

    	try{
            mimeTypeService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除文件类型")
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组

    	try{
            mimeTypeService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }

    }
}