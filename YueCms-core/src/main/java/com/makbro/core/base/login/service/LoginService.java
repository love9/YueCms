
package com.makbro.core.base.login.service;

import com.google.common.base.Stopwatch;
import com.makbro.core.base.login.dao.LoginMapper;
import com.makbro.core.base.loginLog.service.LoginLogService;
import com.makbro.core.base.orgOrganization.dao.OrganizationMapper;
import com.makbro.core.base.orgRole.dao.RoleMapper;
import com.makbro.core.base.orgUser.dao.OrgUserMapper;
import com.makbro.core.base.permission.dao.PermissionMapper;
import com.makbro.core.base.permission.service.PermissionService;
import com.makbro.core.config.MyConfig;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.SsoHelper;
import com.makbro.core.framework.listener.MySessionListener;
import com.makbro.core.framework.mysession.MySessionHelper;
import com.makbro.core.framework.mysession.bean.MySession;
import com.makbro.core.framework.mysession.bean.Token;
import com.makbro.core.framework.mysession.dao.MySessionMapper;

import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.*;
import com.markbro.base.utils.cookie.CookieUtils;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.sso.core.user.SsoUser;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * Area service
 * Created by wujiyue on 2016-03-13 03:24:07.
 */

@Service
public class LoginService {
    protected Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private LoginMapper loginMapper;
    @Autowired
    private OrgUserMapper userMapper;
    @Autowired
    private OrganizationMapper organizationMapper;
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private LoginLogService loginLogService;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private MySessionMapper mySessionMapper;

    @Autowired
    private RoleMapper roleMapper;



    @Autowired
    private PermissionMapper qxMapper;
    



    /**
     * 后台登录验证
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Transactional
    public String login(HttpServletRequest request, HttpServletResponse response, Model model,RedirectAttributes redirectAttributes) {
        String errpage = "";
        String dlmc = StringUtil.isNull(request.getParameter(TmConstant.pageInputDd));
        String dlmm = StringUtil.isNull(request.getParameter(TmConstant.pageInputMm));
        String code = StringUtil.isNull(request.getParameter(TmConstant.pageInputCode));//验证码
        String tokenType = "";
        //String indexpage = "";
        String validateStr = "";
        try {
            errpage = GlobalConfig.getSysPara("login_errorPage");
            errpage = StringUtil.subEndStr(errpage, ".jsp");
            if (GlobalConfig.loginWithValidateCode()) {
                if (StringUtil.isEmpty(code)) {
                    if (errpage.contains(".html")) {
                        errpage = StringUtil.subEndStr(errpage, ".html");
                        model.addAttribute("msg", "请输入验证码！");
                    } else {
                        request.setAttribute(TmConstant.CODE_WARNING, "请输入验证码！");
                    }
                    return errpage;
                }
                if (!checkValidateCode(request, code)) {
                    if (errpage.contains(".html")) {
                        errpage = StringUtil.subEndStr(errpage, ".html");
                        model.addAttribute("msg", "验证码输入错误,请重新输入！");
                    } else {
                        request.setAttribute(TmConstant.CODE_WARNING, "验证码输入错误，请重新输入");
                    }
                    return errpage;
                }
            }

            errpage = StringUtil.subEndStr(errpage, ".jsp");
            validateStr = this.validateUser(request, response, model);
        } catch (Exception e) {
            e.printStackTrace();
            validateStr = "登录时发生异常!请联系管理员!";
        }
        //记录登录日志
        loginLogService.recordLogin(request,validateStr);

        if (!"".equals(validateStr) && !validateStr.equals(TmConstant.NUM_ZERO)) {

            log.info("{}试图登录系统时验证失败", dlmc);
            String from=request.getParameter("from");//查询登录来源参数用来区分是博客登陆还是后台登录（博客登录有个参数from=myblog）
            if("myblog".equals(from)){
                redirectAttributes.addFlashAttribute("msg", validateStr);
                return "redirect:/myblog/login";
            }
            if (errpage.contains(".html")) {
                errpage = StringUtil.subEndStr(errpage, ".html");
                model.addAttribute("msg", validateStr);
            }
            request.setAttribute(TmConstant.CODE_WARNING, validateStr);
            return errpage;
        }


        if (validateStr.equals(TmConstant.NUM_ZERO)) {
            request.setAttribute(TmConstant.CODE_WARNING, "您已登录，不能重复登录！");
        }
        return "";
    }


    public boolean checkValidateCode(HttpServletRequest req, String code) {
        String sessionCode = (String) req.getSession().getAttribute(TmConstant.KEY_VERYCODE);
        return code.equalsIgnoreCase(sessionCode);
    }


    //把登陆用户信息传递到前台
    public void pushLoginUserInfo(LoginBean lb, Model model){
        if(lb!=null){
            List<Map<String,String>> rolelist=lb.getJsList();
            List<Map<String,String>> bmlist=lb.getBmList();
            List<Map<String,String>> gwlist=lb.getGwList();
            if(rolelist!=null&&rolelist.size()>0){
                String jsstr="";
                for (Map<String,String> t:rolelist){
                    jsstr+=t.get(TmConstant.MC_KEY)+"|";
                }
                model.addAttribute(TmConstant.ROLE_KEY,jsstr);
            }else{
                model.addAttribute(TmConstant.ROLE_KEY,"");
            }
            if(bmlist!=null&&bmlist.size()>0){
                String bmstr="";
                for (Map<String,String> t:bmlist){
                    bmstr+=t.get(TmConstant.MC_KEY)+"|";
                }
                model.addAttribute(TmConstant.BM_KEY,bmstr);
            }else{
                model.addAttribute(TmConstant.BM_KEY,"");
            }
            if(gwlist!=null&&gwlist.size()>0){
                String gwstr="";
                for (Map<String,String> t:gwlist){
                    gwstr+=t.get(TmConstant.MC_KEY)+"|";
                }
                model.addAttribute(TmConstant.GW_KEY,gwstr);
            }else{
                model.addAttribute(TmConstant.GW_KEY,"");
            }
            if(lb.isAdmin()){
                model.addAttribute("isAdmin",TmConstant.TRUE_FLAG_STR);
            }
            model.addAttribute(TmConstant.YHID_KEY,lb.getYhid());
            model.addAttribute(TmConstant.YHMC_KEY,lb.getXm());
        }else{
            log.error("请求URL"+ RequestContextHolderUtil.getRequest().getRequestURI()+"时推送用户信息到前台页面错误:LoginBean is Null!");
        }
    }
    //直接用yhid登录。一般为第三方用户登录调用
    public String thirdLogin(String yhid,Model model){

        HttpServletRequest request= RequestContextHolderUtil.getRequest();
        HttpServletResponse response = RequestContextHolderUtil.getResponse();
        try {
            String yhxm ="";
            String ip = MyBatisRequestUtil.getIpByHttpRequest(request);//客户端真实IP
            //如果不允许重复登录
            String sysLogin = "";
            //登录控制；1:后登录用户踢出之前登录用户；2：不允许重复登录；3：无限制
            sysLogin = GlobalConfig.getSysPara("sys_login");
            if ((sysLogin.indexOf("2") != -1) && (!validateToken(yhid, ip))) {
                return "您已登录，不能重复登录！";
            }
            request.setAttribute(TmConstant.YHID_KEY, yhid);
            LoginBean lbean = this.cacheInfo(yhid);
            if(!GlobalConfig.SSO_ENABLE) {//当非sso模式才使用原来的会话管理
                MySessionHelper.login(request,response,lbean,ip);
            }
            setPermisstions(lbean, model);
            pushLoginUserInfo(lbean,model);
            loginMapper.loginCount(yhid, ip);//登录计数保存登录IP和登录时间
            request.getSession().setAttribute(TmConstant.KEY_LOGIN_USER,yhid);//把登录用户的yhid放入session
            String headerPath = (String) EhCacheUtils.getUserInfo("headerPath", yhid);
            model.addAttribute("headerPath", headerPath);
            log.info("{}授权进入系统", yhxm);
            return "";
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ApplicationException("登录时出现异常!请联系管理员!");
        }
    }

    /**
     * 验证用户
     *
     * @param request
     * @return
     * @throws Exception
     */

    private String validateUser(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        try {

            String dlmc = StringUtil.isNull(request.getParameter(TmConstant.pageInputDd));
            String dlmm = StringUtil.isNull(request.getParameter(TmConstant.pageInputMm));
            String jzmm = StringUtil.isNull(request.getParameter(TmConstant.pageRememberMeKey));

            if (dlmc.equals("")) {
                return "错误：请重新登录";
            }
            Map<String, Object> yhxx = loginMapper.queryYhidByDlmc(dlmc);
            if (yhxx == null) {
                return "用户名或密码错误！！";
            }
            String yhid = "",yhxm = "";
            if (String.valueOf(yhxx.get("yhid")).length() > 0) {
                yhid = String.valueOf(yhxx.get("yhid"));
                yhxm = String.valueOf(yhxx.get("xm"));
            } else {
                return "用户名不存在！！";
            }
            if (!validate(yhxx, dlmm)) {
                return "用户名或密码错误！！";
            }
            if (String.valueOf(yhxx.get("available")).equals("0")) {
                return "帐号状态异常！";
            }
            String ip = MyBatisRequestUtil.getIpByHttpRequest(request);//客户端真实IP

            String sysLogin ="";
            //登录控制；1:后登录用户踢出之前登录用户；2：不允许重复登录；3：无限制
            sysLogin = GlobalConfig.getSysPara("sys_login");
            if ((sysLogin.indexOf("2") != -1) && (!validateToken(yhid, ip))) {
                return TmConstant.NUM_ZERO;
            }
            request.setAttribute(TmConstant.YHID_KEY, yhid);

                    CookieUtils.setCookie(response,TmConstant.cookieNameKey, dlmc,31536000);
                    if ("1".equals(jzmm) || "on".equals(jzmm)) {//记住密码
                        CookieUtils.setCookie(response,TmConstant.cookiePassKey, dlmm,31536000);
                        CookieUtils.setCookie(response,TmConstant.pageRememberMeKey, "1",31536000);
                    } else {
                        CookieUtils.setCookie(response,TmConstant.cookiePassKey, dlmm,0);//0表示清除Cookie
                        CookieUtils.setCookie(response, TmConstant.pageRememberMeKey, "0", 31536000);
                    }
                    LoginBean lbean = this.cacheInfo(yhid);
                    if(!GlobalConfig.SSO_ENABLE){//当非sso模式才使用原来的会话管理
                        MySessionHelper.login(request,response,lbean,ip);
                    }
                    setPermisstions(lbean, model);
                    loginMapper.loginCount(yhid, ip);//登录计数保存登录IP和登录时间
                    request.getSession().setAttribute(TmConstant.KEY_LOGIN_USER,yhid);//把登录用户的yhid放入session
                    String headerPath = (String) EhCacheUtils.getUserInfo("headerPath", yhid);
                    model.addAttribute("headerPath", headerPath);

            log.info("{}授权进入系统", yhxm);
            return "";
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ApplicationException("登录时出现异常!请联系管理员!");
        }

    }

    //设置登录用户的菜单权限
    public void setPermisstions(LoginBean lbean,Model model){
        List<Map<String, Object>> qxlist = null;
        List<Map<String, Object>> newList = null;
        if ("1".equals(lbean.getYhid())) {
            //超级管理员权限
            List<Map<String, Object>> qxlist2 = permissionMapper.queryOrgAdminPermission("0");
            newList = permissionService.permissionListToMenuList(qxlist2);
        } else {
            if (lbean.isAdmin()) {
                //组织管理员权限
                List<Map<String, Object>> qxlist2 = permissionMapper.queryOrgAdminPermission(lbean.getOrgid());
                qxlist = permissionService.permissionListToMenuList(qxlist2);
            } else {
                //返回登录用户的菜单列表
                qxlist = permissionService.getUserMenus(lbean.getYhid());
            }
            if(CollectionUtils.isNotEmpty(qxlist)) {
                //管理员除外，当有代理商一级菜单时把它下面的菜单提高到一级菜单显示
                if (qxlist.stream().anyMatch(qx -> {
                    return "代理商".equals(qx.get("name"));
                }) && !lbean.isAdmin()) {
                    newList = qxlist.stream().filter(qx -> {
                        return !"代理商".equals(qx.get("name"));
                    }).collect(Collectors.toList());
                    for (Map<String, Object> t : qxlist) {
                        if ("代理商".equals(t.get("name"))) {
                            List<Map<String, Object>> childrenList = (List<Map<String, Object>>) t.get("children");
                            newList.addAll(childrenList);
                            break;
                        }
                    }
                } else {
                    newList = qxlist;
                }
            }
            if (newList == null || newList.size() == 0) {
                String pers = SysPara.getValue("common_permission","");
                if (pers != null && pers.endsWith(",")) {
                    pers = pers.substring(0, pers.length() - 1);
                    pers = pers.replaceAll(",", "','");
                    qxlist = permissionMapper.findByGuids(pers);
                }
            }
        }
        model.addAttribute("qxlist", newList);
    }


    /**
     * 将部分信息存入Cache中
     *
     * @param yhid
     * @throws Exception
     */

    public LoginBean cacheInfo(String yhid) {
        LoginBean lbean = this.getLoginBean(yhid);

        EhCacheUtils.putUserInfo("loginCount", yhid, lbean.getLoginCount());
        EhCacheUtils.putUserInfo("lastLoginTime", yhid, lbean.getLastLoginTime());
        EhCacheUtils.putUserInfo("lastLoginAddress", yhid, lbean.getLastLoginAddress());
        EhCacheUtils.putUserInfo("headerPath", yhid, String.valueOf(lbean.getUserMap().get("headerPath")));
        EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_DLMC, yhid, lbean.getDlmc());
        EhCacheUtils.putUserInfo(TmConstant.ORGID_KEY, yhid, lbean.getOrgid());
        EhCacheUtils.putUserInfo(TmConstant.BMID_KEY, yhid, lbean.getBmid());
        EhCacheUtils.putUserInfo(TmConstant.GWID_KEY, yhid, lbean.getGwid());
        EhCacheUtils.putUserInfo(TmConstant.JSID_KEY, yhid, lbean.getJsid());
        EhCacheUtils.putUserInfo(TmConstant.XM_KEY, yhid, lbean.getXm());
        EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_USERBEAN, yhid, lbean);
        EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_JS_LIST, yhid, lbean.getJsList());
        EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_BM_LIST, yhid, lbean.getBmList());
        EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_GW_LIST, yhid, lbean.getGwList());
        EhCacheUtils.putUserInfo("orgMap", yhid, lbean.getOrgMap());
        EhCacheUtils.putUserInfo("loginPage", yhid, lbean.getLoginpage());
        EhCacheUtils.putUserInfo("mainPage", yhid, lbean.getMainpage());
        if (!lbean.isAdmin()) {
            EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_ORG, yhid, this.getOrgInfo(yhid, lbean.getOrgid()));
            Map<String, List<String>> pers=this.getPermissionsByYhid(yhid);
            EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_URL, yhid,pers==null?"" : pers.get("urls"));//URL权限列表
            EhCacheUtils.putUserInfo(TmConstant.CACHE_YH_CODE, yhid,pers==null?"" :  pers.get("codes"));//权限代码列表
            EhCacheUtils.putUserInfo(TmConstant.CON_ADMIN, yhid, false);
        } else {
            EhCacheUtils.putUserInfo(TmConstant.CON_ADMIN, yhid, true);
        }
        return lbean;

    }

    public LoginBean getLoginBean(String yhid) {
        LoginBean lBean = new LoginBean();
        Map<String, Object> userMap = userMapper.queryUserMapByYhid(yhid);
        lBean.setUserMap(userMap);
        try {
            lBean.setLoginCount(Integer.valueOf(String.valueOf(userMap.get("loginCount"))));
            lBean.setLastLoginTime(String.valueOf(userMap.get("lastLoginTime")));
            lBean.setLastLoginAddress(String.valueOf(userMap.get("lastLoginIp")));
        } catch (Exception e) {
            log.error(e.toString());
        }

        if ("1".equals(yhid)) {
            //特殊的超级管理员，而不是以admin角色定义的超级管理员,他拥有的权限最大，比其他组织管理员多了特殊权限管理和组织管理的功能
            lBean.setBmid(TmConstant.NUM_ZERO);
            lBean.setGwid(TmConstant.NUM_ZERO);
            lBean.setYhid(yhid);
            lBean.setOrgid(TmConstant.NUM_ZERO);
            lBean.setState("1");
            lBean.setXm(String.valueOf(userMap.get("nickname")));
            //2017年10月29日14:42:05 下面几行代码让yhid=1的特殊用户赋予角色id=1的特殊角色。这样yhid=1的登录后页面和主页面可以从org_role表配置了
            Map<String, Object> jsMap = roleMapper.getMap("1");
            List<Map<String, String>> js = new ArrayList<Map<String, String>>();
            Map<String, String> temp = new HashMap<String, String>();
            temp.put("dm", String.valueOf(jsMap.get("id")));
            temp.put("mc", String.valueOf(jsMap.get("name")));
            temp.put("loginpage", String.valueOf(jsMap.get("loginpage")));
            temp.put("mainpage", String.valueOf(jsMap.get("mainpage")));
            js.add(temp);
            lBean.setJsList(js);
            lBean.setLoginpage(String.valueOf(jsMap.get("loginpage")));
            lBean.setMainpage(String.valueOf(jsMap.get("mainpage")));
            Map<String, Object> orgMap = loginMapper.queryOrgMapByYhid("1");
            ;
            lBean.setOrgMap(orgMap);
            lBean.setAdmin(true);
            lBean.setSystemAdmin(true);

            String xm = String.valueOf(userMap.get("nickname"));
            lBean.setXm(xm);
            String available = String.valueOf(userMap.get("available"));
            lBean.setState(available);
            String account = String.valueOf(userMap.get("account"));
            lBean.setDlmc(account);
            return lBean;
        }
        String orgid = "";
        //String zzmc = "";
        //登录用户的组织信息
        Map<String, Object> orgMap = loginMapper.queryOrgMapByYhid(yhid);
        if (orgMap != null) {
            orgid = orgMap.get("id").toString();
        } else {
            log.warn("没有查询到用户{}所属的组织信息!", yhid);
        }
        List<Map<String, String>> jsList = loginMapper.queryLoginJsList(orgid, yhid);
        //遍历角色是否有admin角色
        for (Map t : jsList) {
            String jsid = (String) t.get("dm");
            String jsmc = (String) t.get("mc");
            String loginpage = (String) t.get("loginpage");//角色表配置的登陆页面
            String mainpage = (String) t.get("mainpage");//角色表配置的登陆后页面中的主页面
            lBean.setLoginpage(loginpage);
            lBean.setMainpage(mainpage);
            if ("admin".equals(jsmc)) {
                lBean.setAdmin(true);
                break;
            }
        }
        if (userMap != null) {
            String xm = String.valueOf(userMap.get("nickname"));
            lBean.setXm(xm);
            String available = String.valueOf(userMap.get("available"));
            lBean.setState(available);
            String account = String.valueOf(userMap.get("account"));
            lBean.setDlmc(account);
        } else {
            log.warn("没有查询到用户:{}的用户信息!", yhid);
        }
        if (lBean.isAdmin()) {
            lBean.setBmid("admin");
            lBean.setGwid("admin");
        } else {
            List<Map<String, String>> bmList = loginMapper.queryLoginBmList(orgid, yhid);
            List<Map<String, String>> gwList = loginMapper.queryLoginGwList(orgid, yhid);
            lBean.setBmList(bmList);
            if (bmList.size() > 0) {
                lBean.setBmid(String.valueOf(bmList.get(0).get(TmConstant.DM_KEY)));
            }
            lBean.setGwList(gwList);
            if (gwList.size() > 0) {
                lBean.setGwid(String.valueOf(gwList.get(0).get(TmConstant.DM_KEY)));
            }
            if (jsList.size() > 0) {
                lBean.setJsid(String.valueOf(jsList.get(0).get(TmConstant.DM_KEY)));
            }
        }
        lBean.setJsList(jsList);
        lBean.setYhid(yhid);
        lBean.setOrgid(orgid);
        lBean.setOrgMap(orgMap);
        return lBean;
    }

    /**
     * 查询用户的权限返回map包含urls集合和codes集合
     *
     * @param yhid
     * @return
     */
    private Map<String, List<String>> getPermissionsByYhid(String yhid) {
        Map<String, List<String>> res = new HashMap<String, List<String>>();
        List<Map<String, Object>> qxList = permissionService.getQxByYhid(yhid);
        List<String> urls = new ArrayList<String>();
        List<String> codes = new ArrayList<String>();
        if (qxList != null && qxList.size() > 0) {
            String url = null;
            String code = null;
            for (Map<String, Object> map : qxList) {
                url = String.valueOf(map.get("url"));
                code = String.valueOf(map.get("code"));
                if (StringUtil.notEmpty(url)) {
                    urls.add(url);
                }
                if (StringUtil.notEmpty(code)) {
                    codes.add(code);
                }
            }
        }
        res.put("urls", urls);
        res.put("codes", codes);
        return res;
    }

    /**
     * 登录用户的部门岗位信息
     *
     * @param yhid
     * @return
     */
    private String getOrgInfo(String yhid, String orgid) {
        StringBuffer sb = new StringBuffer();
        //登录用户的部门岗位信息（bm_mc,gw_mc,bmid,gwid）
        List<Map<String, Object>> org = loginMapper.queryLoginOrgInfo(yhid, orgid);
        if (org.size() > 0) {
            for (int i = 0; i < org.size(); i++) {
                Map<String, Object> m = org.get(i);
                sb.append(TmConstant.SYMB_FENHAO);
                sb.append(m.get("bm_mc"));
                sb.append(TmConstant.SYMB_MH);
                sb.append(m.get("gw_mc"));
                sb.append(TmConstant.SYMB_SHUXIAN);
                sb.append(m.get(TmConstant.BMID_KEY));
                sb.append(TmConstant.SYMB_MH);
                sb.append(m.get(TmConstant.GWID_KEY));
            }
        }
        sb.replace(0, 1, "");
        return sb.toString();
    }



    /**
     * 验证token:yhid与ip对应，则正常;表示，在一个IP地址登录后，其它的地址不能登录
     *
     * @param yhid
     * @param ip
     * @return
     */
    private boolean validateToken(String yhid, String ip) {
        String yhtoken =(String)EhCacheUtils.getSysInfo(MySessionHelper.KEY_YHID_TOKEN,yhid);//根据yhid获得token
        if (yhtoken != null) {
            String str2 = (String) EhCacheUtils.getSysInfo(TmConstant.IP_KEY, yhtoken);
            if (!ip.equals(str2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 验证用户名与密码是否正确
     *
     * @param yhMap
     * @param dlmm
     * @return
     */

    public boolean validate(Map<String, Object> yhMap, String dlmm) {

        String yhid = String.valueOf(yhMap.get(TmConstant.YHID_KEY));
        String sjkdlmm = String.valueOf(yhMap.get("dlmm"));
        //加密模式
        String enc_mode = "4";//明文
        enc_mode = SysPara.getValue("sys_encryptPassword_mode","4");
        if ("1".equals(enc_mode)) {//md5(盐+密码)
            dlmm = Md5.getMd5(yhid.toString() + dlmm);
        } else if ("2".equals(enc_mode)) {//md5(密码)
            dlmm = Md5.getMd5(dlmm);
        } else if ("3".equals(enc_mode)) {//md5(md5(密码))
            dlmm = Md5.getMd5(Md5.getMd5(dlmm));
        } else {//明文
        }
        return dlmm.equalsIgnoreCase(sjkdlmm);

    }

    /**
     * 退出
     *
     * @param request
     * @param response
     * @return
     */
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        String token = CookieUtils.getCookie(request, TmConstant.TokenKey);
        String yhid = this.getYhidByToken(token);
        log.info("===========logout=========");
        log.info("yhid=====" + yhid + "\n token=" + token);
        //清除Cookie的token内容
        CookieUtils.removeCookie(request,response,TmConstant.TokenKey);

        if (StringUtil.notEmpty(token)) {
            log.info("===========deleteUserSessionToken=========");
            mySessionMapper.deleteUserSessionToken(yhid, token);
            MySessionListener tokenListener=new MySessionListener();
            MySession mySession=MySession.builder().yhid(yhid).token(token).build();
            //mySession.setYhid(yhid);
           // mySession.setToken(token);
            tokenListener.tokenDelete("deleteUserSessionToken",mySession);
            log.info("===========removeUserCacheYhid=========");
            this.removeUserCacheYhid(yhid);
            MySessionHelper.removeTokenCache(token);
        }
        request.getSession().invalidate();
    }

    /**
     * 移除登录用户的Cache
     *
     * @param yhid
     */
    private void removeUserCacheYhid(String yhid) {
        EhCacheUtils.clearUserInfo(yhid);
    }

    /**
     * 查询用户token
     * @param yhid
     * @return
     */
    public String queryLoginToken(String yhid){
       return mySessionMapper.queryLoginToken(yhid);
    }
    public void updateToken(String token,String newToken,String expires_time){
        mySessionMapper.updateToken(token,newToken,expires_time);
    }
    public String getYhidByToken(String token) {
        String str = "";
        if (token == null) {
            return str;
        }
        String yhid = (String)EhCacheUtils.getSysInfo(MySessionHelper.KEY_TOKEN_YHID,token);//根据token获得yhid
        if(StringUtil.isEmpty(yhid)){
            yhid = mySessionMapper.queryUserSession(token);
            str = StringUtil.isNull(yhid);
            if(StringUtil.notEmpty(str)){
                EhCacheUtils.putSysInfo(MySessionHelper.KEY_TOKEN_YHID,token,str);
            }
            return str;
        }else{
            return yhid;
        }

    }

    public void updateActiveTime(String token) {
        try {
            String session_storage = "";
            session_storage = GlobalConfig.getSysPara("session_storage");
            //session存储方式：1-放在sys_session中；2-放在cache中 3.存放到redis中
            if (session_storage.equals(TmConstant.NUM_ONE)) {
                //如果每次请求都更新sys_session表的active_time会浪费资源，很慢
                //mySessionMapper.updateUserSessionATime(token);
                EhCacheUtils.putSysInfo("active_time", token, Long.valueOf(System.currentTimeMillis()));
            } else {
                EhCacheUtils.putSysInfo("active_time", token, Long.valueOf(System.currentTimeMillis()));
            }
        } catch (Exception localException) {
            String str = "更新active_time时出现异常:" + localException.toString();
            log.error(str);
            throw new ApplicationException("更新sys_session数据时异常");
        }
    }
    public boolean isInvalidToken(HttpServletRequest request){
        String token = CookieUtils.getCookie(request, TmConstant.TokenKey);
        String invalid_token = (String) EhCacheUtils.getSysInfo(TmConstant.CACHE_GLOBAL_ITOKEN);
        if (StringUtil.notEmpty(invalid_token)&&token.equals(invalid_token)){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 判断当前登录状态。目前只在跳转登录界面方法和检查用户登录状态的api中2个地方使用
     * @param request
     * @param response
     * @return
     */
    public String getLoginState(HttpServletRequest request,HttpServletResponse response){
        if(!GlobalConfig.SSO_ENABLE){
            LoginBean loginBean = this.loginChecked(request,response);
            if(loginBean!=null){
                return TmConstant.NUM_ONE;//用户登录状态
            }else{
                return TmConstant.NUM_ZERO;//用户未登录状态
            }
        }else{
            SsoUser ssoUser = SsoHelper.loginChecked(request,response);
            if(ssoUser!=null){
                return TmConstant.NUM_ONE;
            }else{
                 return TmConstant.NUM_ZERO;
            }

        }
    }
    //非sso方式的登录状态检测
    public  LoginBean loginChecked(HttpServletRequest request,HttpServletResponse response){


        LoginBean loginBean=null;
        String token = CookieUtils.getCookie(request, TmConstant.TokenKey);
        String yhid = String.valueOf(request.getSession().getAttribute(TmConstant.KEY_LOGIN_USER));
        if(StringUtil.isEmpty(token)){//Cookie没有获取到
            if(StringUtil.notEmpty(yhid)){
                token =  this.queryLoginToken(yhid);//Cookie的token没有，但是上面从session中取到了yhid，这种情况是，用户手工删除token
                if(StringUtil.notEmpty(token)){ //重新生成token并写入Cookie、更新到sys_session
                    String newToken= Token.genToken(yhid);
                    CookieUtils.setCookie(response, TmConstant.TokenKey, newToken,null,"/",-1,true);
                    String  expires_time= DateUtil.formatDatetime(DateUtil.addMinute(Integer.valueOf(MyConfig.TOKEN_TIMEOUT)));
                    this.updateToken(token,newToken,expires_time);
                    token=newToken;
                }else{
                    return null;
                }
            }else{
                return null;//Cookie中没有token，session中没有登录用户记录。这种情况没有登录
            }
        }
        int ckToken= Token.checkToken(token);
        if(ckToken!=TmConstant.TOKEN_OK){
            throw new ApplicationException("token非法!");
        }
        Stopwatch watch2 = Stopwatch.createUnstarted();
        watch2.start();


        yhid = this.getYhidByToken(token);//不管前面session有没有取到，再次根据token从sys_session表中取得yhid,取不到说明被人工清除
        request.getSession().setAttribute(TmConstant.KEY_LOGIN_USER, yhid);
        Long time2 = watch2.elapsed(TimeUnit.MILLISECONDS);
        System.out.println("request uri>>>>" +request.getRequestURI()+">>>>>代码执行时长1：" + time2);
        Stopwatch watch3 = Stopwatch.createUnstarted();
        watch3.start();

        if (StringUtil.notEmpty(yhid)) {
            //还要检测缓存是否有该用户的userbean，这样可以达到重启服务器session改变而会话不丢失的目的。
            Object o=EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_USERBEAN,yhid);
            loginBean=(LoginBean)o;
            if(loginBean==null){
                this.cacheInfo(yhid);
                o=EhCacheUtils.getUserInfo(TmConstant.CACHE_YH_USERBEAN,yhid);
                loginBean=(LoginBean)o;
            }
            this.updateActiveTime(token);
            request.setAttribute(TmConstant.YHID_KEY, yhid);

            Long time3 = watch3.elapsed(TimeUnit.MILLISECONDS);
            System.out.println("request uri>>>>" +request.getRequestURI()+">>>>>代码执行时长2：" + time3);
            return loginBean;
        }else {
           return null;

           /* String tishi = "";
            String yhidToken = (String) EhCacheUtils.getSysInfo("invalid_token", token);
            if (StringUtil.notEmpty(yhidToken)){
                tishi = "您的帐号在另一地点登录，您被迫退出系统!";
            }else {
                tishi = "您已被踢出系统，请重新登录！";//这种情况是token是有效的，但从sys_session表中没有，可能是人工删除session，这属于管理员踢出用户操作
            }
            throw new NoLoginException(tishi);*/
        }

    }
}

