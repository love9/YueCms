package com.makbro.core.base.area.web;

import com.makbro.core.base.area.bean.Area;
import com.makbro.core.base.area.service.AreaService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 区域管理
 * Created by wujiyue on 2016-07-17 01:33:47.
 */
@Controller
@RequestMapping("/base/area")
public class AreaController extends BaseController {
    @Autowired
    protected AreaService areaService;
    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/base/area/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String parentid= (String) map.get("parentid");
        String parentname= (String) map.get("parentname");
        String sjareatype= (String) map.get("areatype");//这是上级区域的区域类型
        String areatype="";
        String areatypename="";
        if("ROOT".equalsIgnoreCase(sjareatype)){
            areatype="PROVINCE";
            areatypename="省、自治区、直辖市";
        }
        if("PROVINCE".equalsIgnoreCase(sjareatype)){
            areatype="CITY";
            areatypename="地级市";
        }
        if("CITY".equalsIgnoreCase(sjareatype)){
            areatype="AREA";
            areatypename="区、县";
        }
        model.addAttribute("parentid",parentid);
        model.addAttribute("parentname",parentname);
        model.addAttribute("areatype",areatype);
        model.addAttribute("areatypename",areatypename);
        return "/base/area/add";
    }
    /**
     * 跳转到编辑页面
     */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String id=(String)map.get(TmConstant.ID_KEY);
        Map<String,Object> res=areaService.getMap(id);
        String parentid=(String)res.get("parentid");
        String areatype= (String) res.get("areatype");
        if(TmConstant.NUM_ZERO.equals(parentid)){
            res.put("parentname","区域目录");
        }
        String areatypename="";
        if("PROVINCE".equalsIgnoreCase(areatype)){
            areatypename="省、自治区、直辖市";
        }else if("CITY".equalsIgnoreCase(areatype)){
            areatypename="地级市";
        }
        else{
            areatypename="区、县";
        }
        res.put("areatypename",areatypename);
        model.addAttribute("area",res);
        return "/base/area/edit";
    }


    //-----------json数据接口--------------------
    
	@ResponseBody
	@RequestMapping("/json/findByParentid/{parentid}")
	public Object findByParentid(@PathVariable String parentid) {
		return areaService.findByParentid(getPageBounds(),parentid);
	}

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return areaService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {

        return areaService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }
    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增区域")
    public void add(Area m) {

        areaService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Area m) {
        areaService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存区域")
    public Object save(Area m) {
        return  areaService.save(m);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
	Msg msg=new Msg();
	try{
        int count=areaService.getChildrenCount(id);
        if(count>0){
            msg.setType(Msg.MsgType.error);
            msg.setContent("删除的区域下不能有子区域！");
            return msg;
        }else{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            areaService.updateByMap(map);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }

	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("删除失败！");
	}
	return msg;
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){
	Msg msg=new Msg();
	try{
        String ids_str= StringUtil.arrToString(ids, ",");
        int count=areaService.getChildrenCount(ids_str);
        if(count>0){
            msg.setType(Msg.MsgType.error);
            msg.setContent("删除的区域下不能有子区域！");
            return msg;
        }else{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            areaService.updateByMapBatch(map);
            msg.setType(Msg.MsgType.success);
            msg.setContent("批量删除成功！");
        }

	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("批量删除失败！");
	}
	return msg;
	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除区域")
    public void delete(@PathVariable String id) {
        areaService.delete(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除区域")
    public void deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
         areaService.deleteBatch(ids);
    }

    @ResponseBody
    @RequestMapping("/json/tree")
    public Object tree() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return areaService.tree(map);
    }
    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return areaService.saveSort(MyBatisRequestUtil.getMap(request));
    }
    @ResponseBody
    @RequestMapping("/json/select")
    public Object select() {
        return areaService.select(MyBatisRequestUtil.getMap(request));
    }


    @ResponseBody
    @RequestMapping("/json/getPositionProvinceList")
    public Object getPositionProvinceList() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        String select_id=String.valueOf(map.get("select_id"));
        if (StringUtil.isEmpty(select_id)) {
            logger.error("请求省份列表没有传递select_id参数!");
            select_id="ProvinceList";
        }
        return areaService.getPositionProvinceList(select_id,"");
    }

    @ResponseBody
    @RequestMapping("/json/getPositionCityByProvinceId/{province_id}")
    public Object getPositionCityByProvinceId(@PathVariable String province_id) {
        if("{province_id}".equals(province_id)){
            return "";
        }
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        String select_id=String.valueOf(map.get("select_id"));
        if (StringUtil.isEmpty(select_id)) {
            logger.error("请求城市列表没有传递select_id参数!");
            select_id="CityList";
        }
        return areaService.getPositionCityByProvinceId(select_id,province_id,"");
    }
     @ResponseBody
     @RequestMapping("/json/getPositionCountryByCityId/{city_id}")
     public Object getPositionCountryByCityId(@PathVariable String city_id) {
         if("{city_id}".equals(city_id)){
             return "";
         }
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        String select_id=String.valueOf(map.get("select_id"));
        if (StringUtil.isEmpty(select_id)) {
            logger.error("请求乡镇列表没有传递select_id参数!");
            select_id="PositionList";
        }
        return areaService.getPositionCountryByCityId(select_id, city_id,"");

    }
    @ResponseBody
    @RequestMapping("/json/getPositionTownByCountryId/{country_id}")
    public Object getPositionTownByCountryId(@PathVariable String country_id) {
        if("{country_id}".equals(country_id)){
            return "";
        }
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        String select_id=String.valueOf(map.get("select_id"));
        if (StringUtil.isEmpty(select_id)) {
            logger.error("请求村庄列表没有传递select_id参数!");
            select_id="TownList";
        }
        return areaService.getPositionTownByCountryId(select_id, country_id,"");
    }

    /**
     *初始化省、市、区、村庄4个Select
     * @return
     */
    @ResponseBody
    @RequestMapping("/json/initPositionSelect")
    public Object initPositionSelect() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        String province_id=String.valueOf(map.get("province_id"));
        String city_id=String.valueOf(map.get("city_id"));
        String country_id=String.valueOf(map.get("country_id"));
        String town_id=String.valueOf(map.get("city_id"));

        String province_select_id=String.valueOf(map.get("province_select_id"));
        String city_select_id=String.valueOf(map.get("city_select_id"));
        String country_select_id=String.valueOf(map.get("country_select_id"));
        String town_select_id=String.valueOf(map.get("town_select_id"));

        return areaService.initPositionSelect(province_id,province_select_id, city_id,city_select_id,country_id,country_select_id,town_id,town_select_id);
    }
    /**
     *获得一定格式的省市区json数据字符串
     * [{value: '110000',
         text: '北京市',
         children: [{
             value: '110001',
             text: '北京市',
         }]
        }]
     * @return
     */
    @ResponseBody
    @RequestMapping("/json/getPositionJson")
    public Object getPositionJson() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        //层级 1层表示省份 2层表示省市 3层表示省市区 最多4层 表示省市区和村庄
        int layer=Integer.valueOf(String.valueOf(map.get("layer")));
        return areaService.getPositionJson(layer);
    }
}
