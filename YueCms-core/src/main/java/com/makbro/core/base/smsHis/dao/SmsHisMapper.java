package com.makbro.core.base.smsHis.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.smsHis.bean.SmsHis;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 短信发送记录 dao
 * @author wujiyue
 * @date 2018-11-06 16:08:03
 */
@Repository
public interface SmsHisMapper{

    public SmsHis get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(SmsHis smsHis);
    public void addByMap(Map<String, Object> map);
    public void update(SmsHis smsHis);
    public void updateByMap(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<SmsHis> find(PageBounds pageBounds, Map<String, Object> map);
    public List<SmsHis> findByMap(PageBounds pageBounds, Map<String, Object> map);




}
