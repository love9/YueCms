package com.makbro.core.base.system.web;

import com.makbro.core.framework.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController extends BaseController {

    /*@RequestMapping("/")
    public String index(){
        return "/login_processon/login";
    }*/

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        return "test";
    }
}
