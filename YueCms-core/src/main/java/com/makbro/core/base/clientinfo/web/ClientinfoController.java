package com.makbro.core.base.clientinfo.web;

import com.makbro.core.base.clientinfo.bean.Clientinfo;
import com.makbro.core.base.clientinfo.service.ClientinfoService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 客户端信息管理
 * @author  wujiyue on 2018-09-01 14:22:00.
 */

@Controller
@RequestMapping("/base/clientinfo")
public class ClientinfoController extends BaseController {
    @Autowired
    protected ClientinfoService clientinfoService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/base/clientinfo/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Clientinfo clientinfo, Model model){
                return "/base/clientinfo/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Clientinfo clientinfo,Model model){
        if(clientinfo!=null&&clientinfo.getId()!=null){
            clientinfo=clientinfoService.get(clientinfo.getId());
        }
         model.addAttribute("clientinfo",clientinfo);
         return "/base/clientinfo/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return clientinfoService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return clientinfoService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return clientinfoService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除客户端信息")
    public Object delete(@PathVariable Integer id) {
    	try{
            clientinfoService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
        	return Msg.error("删除失败！");
        }

    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除客户端信息")
    public Object deletes(@PathVariable Integer[] ids) {
    	try{
             clientinfoService.deleteBatch(ids);
             return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }

    }
}