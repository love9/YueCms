package com.makbro.core.base.system.web;

import com.alibaba.fastjson.JSON;
import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.system.service.AccountService;
import com.makbro.core.base.system.service.LeYunSmsCallBackImpl;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresAnonymous;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.makbro.core.framework.authz.annotation.RequiresRoles;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.PatternUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.LoginBean;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.SpringContextHolder;
import com.markbro.base.utils.SysPara;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.thirdapi.common.SmsUtil;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 账户管理
 * @author wujiyue
 */
@Controller
@RequestMapping
public class AccountController extends BaseController {
    @Autowired
    TableKeyService bmKeyService;

    @Autowired
    AccountService accountService;

    //用户输入邮箱请求重置密码
    @RequiresAnonymous
    @RequestMapping("/reset")
    @ResponseBody
    public Object reset(){
        String email=request.getParameter("email");
        if(StringUtil.isEmpty(email)){
            Msg msg=new Msg();
            msg.setType(Msg.MsgType.error);
            msg.setContent("email参数不能为空!");
            return msg;
        }
       String baseLink= getServerContextPath();
       return accountService.sendResetEmail(email, baseLink);
    }

    /**
     *  用户打开重置密码页面链接
     */
    @RequiresAnonymous
    @RequestMapping("/account/pwdreset")
    public Object pwdreset(Model model){
        String jwt=request.getParameter("token");
        /*JWTUtil.CheckResult result =  JWTUtil.validateJWT(jwt);
        if(result.isSuccess()){
            model.addAttribute("token",jwt);
            return "/login_processon/resetPwd";
        }else{
            String msg= result.getErrorMsg();
            model.addAttribute("info","重置密码请求出错!"+msg);
            return "error/500";
        }*/
        return Msg.error("功能在开发中!");
    }
    //重置密码页面提交重置密码请求
    @RequiresAnonymous
    @ResponseBody
    @RequestMapping("/account/pwdreset/change_password")
    public Object pwdreset_change_password(){
        Msg res=new Msg();
        res.setType(Msg.MsgType.error);
        res.setContent("功能在开发中!");
        String jwt=request.getParameter("token");
        /*JWTUtil.CheckResult result =  JWTUtil.validateJWT(jwt);
        if(result.isSuccess()){
            try {
                String subject=result.getSubject();//{"yhid","1","":""}
                Map jwtMap= JSON.parseObject(subject,Map.class);
                String yhid=String.valueOf(jwtMap.get("yhid"));
                String newPwd=request.getParameter("new_pass");
                String confirm=request.getParameter("confirm_pass");
                Map map=new HashMap<String,Object>();
                map.put("newPwd",newPwd);
                map.put("confirm",confirm);
                map.put("yhid",yhid);
                String oldPwd=accountService.getPasswordByYhid(yhid);
                map.put("oldPwd",oldPwd);
                return accountService.changePwd(map);
            }catch (Exception ex){
                ex.printStackTrace();
                res.setType(Msg.MsgType.error);
                res.setContent("找回密码业务处理异常!");
            }

        }else{
            String msg= "密码修改失败!"+result.getErrorMsg();
            res.setType(Msg.MsgType.error);
            res.setContent(msg);
        }*/
        return  res;
    }

    /**
     * 跳转到个人信息设置页面
     */
    @RequestMapping("/account/personalInfo")
    @RequiresPermissions("account.personalInfo")
    public String personalInfo(Model model){
        LoginBean lb= MyBatisRequestUtil.getLoginUserInfo(request);
        pushLoginUserInfo(model);
        String headerPath =(String) EhCacheUtils.getUserInfo("headerPath", lb.getYhid());
        model.addAttribute("headerPath",headerPath);
        model.addAttribute("loginCount",lb.getLoginCount());
        model.addAttribute("lastLoginTime",lb.getLastLoginTime());
        model.addAttribute("lastLoginAddress",lb.getLastLoginAddress());
        //登陆日志条数
        int loginLogCount=accountService.getLoginLogCount();
        model.addAttribute("loginLogCount",loginLogCount);
        return "/sys/account/personalInfo";
    }
    /**
     * 修改个人密码
     */
    @ResponseBody
    @RequestMapping("/account/changePwd")
    @RequiresPermissions("account.personalInfo")
    public Object changePwd(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        return accountService.changePwd(map);
    }

    /**
     * 跳转到注册页面
     */
    @RequestMapping(value="/reg",method = RequestMethod.GET)
    public String toReg(){
       String regpage= "";
        try {
            regpage = SysPara.getValue("reg_page");
        } catch (Exception e) {
            regpage = "reg";
        }
        if(regpage.indexOf('.')>0)
        {
            regpage=regpage.substring(0,regpage.indexOf('.'));
        }
        if(!regpage.startsWith("/")){
            regpage="/"+regpage;
        }
        return regpage;
    }
    /**
     * 跳转到注册页面
     */
    @RequiresAnonymous
    @RequestMapping(value="/reg/developer",method = RequestMethod.GET)
    public String toRegDeveloper(){

        return "/reg_developer";
    }
    @RequiresAnonymous
    @ResponseBody
    @RequestMapping("/reg/reg_developer_send")
    public Object reg_developer_send(Model model){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.error);
        String veryCode=request.getParameter(TmConstant.KEY_VERYCODE);
        if(StringUtil.isEmpty(veryCode)){
            msg.setExtend("error_type","verycode");
            msg.setContent("验证码参数不能为空!");
            return msg;
        }
        //String sessionCode=(String)session.getAttribute("verycode");
        String sessionCode=(String) request.getSession().getAttribute(TmConstant.KEY_VERYCODE);
        if(!veryCode.equals(sessionCode)){
            msg.setContent("验证码不正确!");
            msg.setExtend("error_type","verycode");
            return msg;
        }
        String email=request.getParameter("email");
        if(StringUtil.isEmpty(email)||!StringUtil.isEmail(email)){
            msg.setExtend("error_type","email");
            msg.setContent("email不正确!");
            return msg;
        }
        String pwd=request.getParameter("password");
        String pwd2=request.getParameter("confirm_password");
        if(!pwd.equals(pwd2)){
            msg.setExtend("error_type","confirm_password");
            msg.setContent("两次输入密码不一致!");
            return msg;
        }
        String baseLink= getServerContextPath();
        return accountService.sendRegDeveloperEmail(email,pwd,baseLink);
    }

    //用户在申请成为参与开发者后收到邮件点击激活账户
    @RequiresAnonymous
    @RequestMapping("/reg/developer/active")
    @Transactional
    public String reg_developer_active(Model model, RedirectAttributes attr){
        Msg res=new Msg();
        res.setType(Msg.MsgType.error);
        String jwt=request.getParameter("token");
        String email="";
        //JWTUtil.CheckResult result =  JWTUtil.validateJWT(jwt);
        if(true){
            try {
                String subject="";//result.getSubject();//{"email","1","":""}
                Map jwtMap= JSON.parseObject(subject,Map.class);
                String qq="";
                email=String.valueOf(jwtMap.get("email"));
                if(email.endsWith("@qq.com")){
                    qq=email.replace("@qq.com","");
                }
                model.addAttribute("qq",qq);
                String pwd=String.valueOf(jwtMap.get("password"));
                model.addAttribute("email",email);
                //检测是否注册email了，根据account和email查询用户
                OrgUser user = accountService.getUserByEmail(email);
                if(user==null){
                    user = accountService.regDeveloperAccount(email, pwd);//新注册开发者账户，注册完 还需要用户填写个人资料并且需要审核
                }
                if(user!=null){
                    //用户激活邮件创建用户成功
                    //再次生成一个JWT串，包含新建用户的ID传给页面，页面发现这个串 //跳到申请流程的第三步，允许用户填写开发者用户信息并提交审核
                    String yhid = user.getId();
                    String jwtGuid = Guid.get();
                    Map subMap = new HashMap<String, Object>();
                    subMap.put("yhid", yhid);
                    String jspjwt ="";// JWTUtil.createJWT(jwtGuid, subMap, 1000 * 60 * 60);//1小时

                    model.addAttribute("token",jspjwt);
                    model.addAttribute("info_header","恭喜，你已通过邮件创建开发者账户!");
                    model.addAttribute("info_desc","但是目前您还不是正式的开发人员，请填写一些您的基本信息，个人简历提交给管理员审核，审核通过后您才正式成为本系统开发人员！个人信息，特别是个人做项目的经历填写越详细越容易通过审核!");
                    res.setType(Msg.MsgType.success);
                    res.setContent("恭喜，你已通过邮件创建开发者账户!");
                }else{
                    res.setContent("创建您的开发者账户发生异常!请重试!");
                    //model.addAttribute("errormsg","创建您的开发者账户发生异常!请重试!");
                    //return "/reg_developer";
                }
            }catch (Exception ex){
                ex.printStackTrace();
                res.setType(Msg.MsgType.error);
                res.setContent("创建开发者账户异常!");
            }
        }else{
            /*if(result.getErrorMsg().contains("过期")){
                res.setContent("链接已经失效，请重新申请!");
                attr.addFlashAttribute("msgObj",res);
                return "redirect:/reg/developer";
            }else{
                res.setContent("创建开发者账户失败!"+result.getErrorMsg());
            }*/
        }
        model.addAttribute("msgObj",res);
        return "/reg_developer";
    }


    /**
     * 用户提交参与开发审核申请
     */
    @ResponseBody
    @RequestMapping("/reg/developer/applay")
    @RequiresAnonymous
    public Object reg_developer_applay(Model model){
        Msg res=new Msg();
        res.setType(Msg.MsgType.error);
        String jwt=request.getParameter("token");
        String yhid="";
        //JWTUtil.CheckResult result =  JWTUtil.validateJWT(jwt);
        if(true){
            try {
                String subject="";//result.getSubject();//{"yhid","1","":""}
                Map jwtMap= JSON.parseObject(subject,Map.class);

                Map saveMap= MyBatisRequestUtil.getMapGuest(request);
                yhid=String.valueOf(jwtMap.get("yhid"));
                saveMap.put("yhid",yhid);
                return  accountService.applayDeveloperAndSubmit(saveMap);
            }catch (Exception ex){
                ex.printStackTrace();
                res.setType(Msg.MsgType.error);
                res.setContent("用户参与开发申请提交异常!");
            }
        }else{
                res.setContent("无效的请求!");
        }
        model.addAttribute("msgObj",res);
        return res;
    }
    /**
     * 保存用户头像路径
     */
    @ResponseBody
    @RequestMapping("/account/saveHeader")
    @RequiresPermissions("account.personalInfo")
    //@RequiresPermissions("account.header")//2017年9月18日 11:37:24 用url路径控制权限不方便在于。如果同一个功能有多个接口url，得把多个url权限全赋予给用户才能保证该用户能使用该功能。如果换成用权限代码控制那么只需要给该用户赋予一个权限代码就可以了
    //例如用户设置头像这个功能，用到2个接口1.保存图片路径2.上传图片。如果采用权限代码。那么着个接口都对应一个权限代码即可。
    public Object saveHeader(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        return accountService.saveHeader(map);
    }
    //上传头像图片 2017年9月18日 11:43:11
    //@RequiresPermissions("account.header")
    @RequiresPermissions("account.personalInfo")
    @RequestMapping("/account/uploadHeader")
    public void uploadSingle() {
        Map map = MyBatisRequestUtil.getMapGuest(request);
        PrintWriter write = null;
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("file");
        if(multipartFile != null){
            map = accountService.uploadHeaderImage(map, request, multipartFile);
        }
        JSONArray json=JSONArray.fromObject(map);
        try {
            write = response.getWriter();
            write.write(json.toString());
            write.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response = null;
            if (write != null){
                write.close();
            }
            write = null;
        }
    }


    /**
     * 发送sms短信接口
     * @return
     */
    @RequestMapping("/sendSms")
    @ResponseBody
    @RequiresRoles("system")
    public Object sendSms(){
        Map map= MyBatisRequestUtil.getMap(request);
        String phone= PatternUtil.isNull(String.valueOf(map.get("phone")));
        String content= PatternUtil.isNull(String.valueOf(map.get("content")));
        Msg msg=new Msg();
        LeYunSmsCallBackImpl leYunSmsCallBack= SpringContextHolder.getBean("leYunSmsCallBack");
        map.put("carrieroperator","乐云短信");
        map.put("yhmc",MyBatisRequestUtil.getLoginUserInfo(request).getXm());
        leYunSmsCallBack.setParams(map);
        int r= SmsUtil.sendLeYunSms(phone,"通知",content,leYunSmsCallBack);
        if(r==1){
            msg.setType(Msg.MsgType.success);
            msg.setContent("发送短信成功！");
        }else{
            msg.setType(Msg.MsgType.error);
            msg.setContent("发送短信失败！");
        }
        return msg;
    }

    /**
     * 查询第三方登录绑定账号列表
     * @return
     */
    @RequiresAuthentication
    @RequestMapping("/account/findThirdOauthByYhid")
    @ResponseBody
    public Object findThirdOauthByYhid(){
        String yhid=MyBatisRequestUtil.getYhid();
       return accountService.findThirdOauthByYhid(yhid);
    }
    /**
     * 解绑第三方登录账号
     * @return
     */
    @RequiresAuthentication
    @RequestMapping("/account/unBindThirdLogin")
    @ResponseBody
    public Object unBindThirdLogin(){
        Map map=MyBatisRequestUtil.getMap(request);
        return accountService.unBindThirdLogin(map);
    }
}
