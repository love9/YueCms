package com.makbro.core.base.orgOrganization.web;


import com.makbro.core.base.orgOrganization.bean.Organization;
import com.makbro.core.base.orgOrganization.service.OrganizationService;
import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.makbro.core.framework.authz.annotation.RequiresRoles;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

/**
 * 组织机构管理
 * Created by wujiyue on 2016-07-18 22:52:36.
 */
@Controller
@RequestMapping("/org/organization")
@RequiresPermissions(value={"/org/organization","orgTreeManage"},logical= Logical.OR)
public class OrganizationController extends BaseController {
    @Autowired
    protected OrganizationService organizationService;
    @RequestMapping(value={"","/"})
    @RequiresRoles("system")
    public String index(){
        return "/base/organization/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        return "/base/organization/add";
    }
    /**
     * 跳转到编辑页面
     */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String id=(String)map.get("id");
        Organization organization=organizationService.get(id);
        model.addAttribute("organization",organization);
        return "/base/organization/edit";
    }
    /**
     * 跳转到组织信息维护页面
     */
    @RequestMapping(value = "/wh")
    public String wh(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        Organization organization=organizationService.get((String) map.get("orgid"));
        model.addAttribute("organization",organization);
        return "/base/organization/wh";
    }

    /**
     * 跳转到列表页面
     */
    /*@RequestMapping(value={"/list"})
    public String list(PageParam pageParam,Model model){
        Object organizations=null;
        organizations=organizationService.find(getPageBounds(pageParam), MyBatisRequestUtil.getMap(request));
        model.addAttribute("organizations",organizations);
        model.addAttribute("pageParam",pageParam);
        return "/base/organization/list";
    }*/

    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return organizationService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return  organizationService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));

    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存组织机构")
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return  organizationService.save(map);
    }

   /* @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除组织机构")
    public Object delete(@PathVariable java.lang.String id) {
      return   organizationService.delete(id);
    }*/
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除组织机构")
    public Object deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
       return   organizationService.deleteBatch(ids);
    }



    @ResponseBody
    @RequestMapping(value="/json/importOrg",method = RequestMethod.POST)
    @ActionLog(description="导入组织目录")
    @RequiresRoles(value = {"admin","system"},logical =Logical.OR )//管理会员或系统管理员才能导入组织目录功能
    public Object importOrg() {
        Map map=MyBatisRequestUtil.getMap(request);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("file");
        if(multipartFile != null){
            //上传excel文件得到本地存放路径
            //Map  upMap = imageService.uploadSingle(map, request, multipartFile);TODO
            Map  upMap =null;
            if(upMap!=null){
                String abpath=String.valueOf(upMap.get("abpath"));
                if(StringUtil.notEmpty(abpath)){
                    map.put("localpath",abpath);
                }
            }
        }
        return organizationService.importOrg(map);
    }
}
