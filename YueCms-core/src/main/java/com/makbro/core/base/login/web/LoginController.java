package com.makbro.core.base.login.web;


import com.makbro.core.base.login.dao.LoginMapper;
import com.makbro.core.base.login.service.LoginService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.SsoHelper;
import com.makbro.core.framework.authz.annotation.RequiresAnonymous;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.SysPara;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.sso.core.user.SsoUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

;

/**
 * 登陆控制器
 *
 */
@Controller
public class LoginController extends BaseController {
    @Autowired
    protected LoginService loginService;



    @RequestMapping (value = {"/admin/login","/admin"}, method = RequestMethod.GET)
    @RequiresAnonymous
     public String toLogin (Model model) {
        String redirectUrl=request.getParameter("redirectUrl");
        if(StringUtil.notEmpty(redirectUrl)){
            model.addAttribute("redirectUrl",redirectUrl);
        }

        if(loginService.getLoginState(request,response).equals("1")){//判断当前已经登录
            String indexPath="";
            try{
                Map m= MyBatisRequestUtil.getMap(request);
                String yhid=String.valueOf(m.get("yhid"));
                Object o= EhCacheUtils.getUserInfo("userbean",yhid);
                LoginBean lb=(LoginBean)o;
                if(GlobalConfig.loginWithValidateCode()){
                    model.addAttribute("loginWithValidateCode","true");
                }
                if(lb!=null){
                    pushLoginUserInfo(model);
                    String  rolePagePath=(String) EhCacheUtils.getUserInfo("loginPage",lb.getYhid());
                    if(StringUtil.notEmpty(rolePagePath)){
                        indexPath=rolePagePath;
                    }
                }
                if(StringUtil.notEmpty(redirectUrl)){
                    return "redirect:"+redirectUrl;
                }
                loginService.setPermisstions(lb,model);
            }catch (Exception ex){
                ex.printStackTrace();
            }

            return indexPath;
        }
        String loginpage= SysPara.getValue("login_page","login");

        if(loginpage.indexOf('.')>0&&!loginpage.endsWith(".html"))
        {
            loginpage=loginpage.substring(0,loginpage.indexOf('.'));
        }
        if(!loginpage.startsWith("/")&&!loginpage.startsWith("redirect:")){
            loginpage="/"+loginpage;
        }
        if(loginpage.endsWith(".html")){
            //如果是html页面不能用jsp标签
            //判断是否登陆需要验证码
            if(GlobalConfig.loginWithValidateCode()){
                model.addAttribute("loginWithValidateCode","true");
            }
            loginpage=StringUtil.subEndStr(loginpage,".html");
        }
        return loginpage;
    }

    /**
     * 注册
     * @param model
     * @return
     */
    @RequestMapping(value = "/reg")
    @RequiresAnonymous
    public String reg(Model model){
        String regpage = GlobalConfig.getSysPara("reg_page","reg");
        if(regpage.indexOf('.')>0)
        {
            regpage=regpage.substring(0,regpage.indexOf('.'));
        }
        if(!regpage.startsWith("/")){
            regpage="/"+regpage;
        }
        return regpage;
    }

    /**
     * 注册协议
     * @param model
     * @return
     */
    @RequestMapping(value = "/tos")
    @RequiresAnonymous
    public String tos(Model model){
        return "/login_processon/tos";
    }
    /**
     * 忘记密码
     * @param model
     * @return
     */
    @RequestMapping(value = "/forget")
    @RequiresAnonymous
    public String forget(Model model){
        return "/login_processon/forget";
    }
    /**
     * 登录验证
     * @param request
     * @param model
     * @return
     */
    @Autowired
    LoginMapper loginMapper;

    //@ActionLog(description = "用户登录")
    //登录日志额外记录，不算为操作日志
    @RequestMapping (value = "/login", method = RequestMethod.POST)
    public String doLogin(HttpServletRequest request, Model model,RedirectAttributes redirectAttributes) {

        String path=loginService.login(request, response,model,redirectAttributes);
        if(StringUtil.notEmpty(path)){

            return path;//登录校验失败
        }

        //上面登录校验成功才走下面
        try{
            LoginBean lb=MyBatisRequestUtil.getLoginUserInfo(request);
            if(GlobalConfig.loginWithValidateCode()){
                model.addAttribute("loginWithValidateCode","true");
            }
            if(lb!=null){
                if(GlobalConfig.SSO_ENABLE){
                    SsoUserInfo ssoUserInfo= SsoUserInfo.build().setUserid(lb.getYhid()).setUsername(lb.getDlmc());
                    String sessionId= SsoHelper.login(ssoUserInfo,false);//原有系统登录功能之上再集成sso
                }
                pushLoginUserInfo(model);
                String  rolePagePath=(String) EhCacheUtils.getUserInfo("loginPage",lb.getYhid());
                if(StringUtil.notEmpty(rolePagePath)){
                    path=rolePagePath;
                }else{
                    //没有设置角色或该角色没配置页面
                    //那么默认该用户为普通用户

                }
            }
            String redirectUrl=request.getParameter("redirectUrl");
            if(StringUtil.notEmpty(redirectUrl)){
                return "redirect:"+redirectUrl;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return path;
    }

    /**
     * 退出
     * @return
     */
    @RequestMapping(value = "/logout")
    public String logout(Model model){
        loginService.logout(request, response);
        if(GlobalConfig.SSO_ENABLE){
            SsoHelper.logout(request, response);
        }
        return "redirect:/admin/login";
    }




}
