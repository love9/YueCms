package com.makbro.core.base.permission.bean;


import com.markbro.base.model.AliasModel;

/**
 * 菜单权限 bean
 * @author wujiyue
 */
public class Permission  implements AliasModel {
	private String orgid;//组织id
	private String id;//主键
	private String parentid;//权限父ID
	private String parentids;//
	private String name;//权限名称
	private String target;//页面打开方式
	private String qxlx;//权限类型 1页面 2按钮
	private String code;//权限代码
	private String url;//权限url
	private String icon;//
	private Integer sort;//
	private String description;//权限描述
	private String createTime;//
	private String createBy;//
	private String updateBy;//
	private String updateTime;//
	private Integer available=1;//状态标志
	private Integer deleted=0;//删除标志
	private Integer ismenu;//菜单标志

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public Integer getIsmenu() {
		return ismenu;
	}

	public void setIsmenu(Integer ismenu) {
		this.ismenu = ismenu;
	}


	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getQxlx() {
		return qxlx;
	}

	public void setQxlx(String qxlx) {
		this.qxlx = qxlx;
	}

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getParentid(){ return parentid ;}
	public void  setParentid(String parentid){this.parentid=parentid; }
	public String getParentids(){ return parentids ;}
	public void  setParentids(String parentids){this.parentids=parentids; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getCode(){ return code ;}
	public void  setCode(String code){this.code=code; }
	public String getUrl(){ return url ;}
	public void  setUrl(String url){this.url=url; }
	public String getIcon(){ return icon ;}
	public void  setIcon(String icon){this.icon=icon; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }


	@Override
	public String toString() {
	return "Permission{" +
			"id=" + id+
			", parentid=" + parentid+
			", parentids=" + parentids+
			", name=" + name+
			", qxlx=" + qxlx+
			", code=" + code+
			", url=" + url+
			", icon=" + icon+
			", sort=" + sort+
			", description=" + description+
			", createTime=" + createTime+
			", createBy=" + createBy+
			", updateBy=" + updateBy+
			", updateTime=" + updateTime+
			", available=" + available+
			", deleted=" + deleted+
			", ismenu=" + ismenu+
			 '}';
	}
}
