package com.makbro.core.base.orgRole.bean;


import com.markbro.base.model.AliasModel;
import lombok.Data;

/**
 * Role bean
 * Created by wujiyue on 2016-06-12 22:35:17.
 */
@Data
public class Role  implements AliasModel {
	private String orgid;//机构、组织id
	private String id;//机构角色id、主键
	private String name;//角色名称
	private String description;//描述
	private String loginpage;//该角色用户登录后跳转的页面
	private String mainpage;//该角色用户登录后页面内的首页
	private String createTime;//创建时间
	private String updateTime;//更新时间
	private String createBy;//创建人
	private String updateBy;//更新人
	private Integer available;//可用状态
	private Integer deleted;//删除标志。1删除。0未删除。

}
