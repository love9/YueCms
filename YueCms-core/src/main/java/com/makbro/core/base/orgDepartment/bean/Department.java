package com.makbro.core.base.orgDepartment.bean;


import com.markbro.base.model.AliasModel;

/**
 * 部门 bean
 * @author wujiyue
 */
public class Department  implements AliasModel {
	private String orgid;//机构、组织id
	private String id;//主键。部门id
	private String parentid;//上级id
	private String parentids;//parentids
	private String name;//部门名称
	private Integer sort;//排序序号
	private String phone;//电话
	private String fax;//传真
	private String email;//邮箱
	private String bz;//备注
	private String createTime;//
	private String updateTime;//
	private String createBy;//
	private String updateBy;//
	private Integer available;//可用标志
	private Integer deleted;//删除标志

	public String getOrgid(){ return orgid ;}
	public void  setOrgid(String orgid){this.orgid=orgid; }
	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getParentid(){ return parentid ;}
	public void  setParentid(String parentid){this.parentid=parentid; }
	public String getParentids(){ return parentids ;}
	public void  setParentids(String parentids){this.parentids=parentids; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public String getPhone(){ return phone ;}
	public void  setPhone(String phone){this.phone=phone; }
	public String getFax(){ return fax ;}
	public void  setFax(String fax){this.fax=fax; }
	public String getEmail(){ return email ;}
	public void  setEmail(String email){this.email=email; }
	public String getBz(){ return bz ;}
	public void  setBz(String bz){this.bz=bz; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Department{" +
			"orgid=" + orgid+
			", id=" + id+
			", parentid=" + parentid+
			", parentids=" + parentids+
			", name=" + name+
			", sort=" + sort+
			", phone=" + phone+
			", fax=" + fax+
			", email=" + email+
			", bz=" + bz+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", createBy=" + createBy+
			", updateBy=" + updateBy+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
