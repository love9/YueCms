package com.makbro.core.base.area.service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.area.bean.Area;
import com.makbro.core.base.area.dao.AreaMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 区域 service
 * @author  wujiyue on 2016-07-17 01:33:47.
 */
@Service
public class AreaService{
    @Autowired
    private AreaMapper areaMapper;
    @Autowired
    private TableKeyService keyService;
     /*基础公共方法*/
    public Area get(String id){
        return areaMapper.get(id);
    }
    public Map<String,Object> getMap(String id){
        return areaMapper.getMap(id);
    }
    public List<Area> find(PageBounds pageBounds,Map<String,Object> map){
        return areaMapper.find(pageBounds,map);
    }
    public List<Area> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return areaMapper.findByMap(pageBounds,map);
    }
    public void add(Area area){
        areaMapper.add(area);
    }
    public Object save(Area area){
          Msg msg=new Msg();
                 try{
                     if(StringUtil.isEmpty(area.getId())){
                         String id= keyService.getStringId();
                         area.setId(id);
                         String parentid=area.getParentid();
                         String pids= areaMapper.getParentidsById(parentid);
                         if(StringUtil.isEmpty(pids)){
                             pids="0,";
                         }
                         pids+=id+ TmConstant.COMMA;
                         area.setParentids(pids);
                         int sort=areaMapper.getMaxSortByParentid(parentid);
                         area.setSort(sort+1);
                         areaMapper.add(area);
                     }else{
                         String parentid=area.getParentid();
                         String pids= areaMapper.getParentidsById(parentid);
                         if(StringUtil.isEmpty(pids)){
                             pids="0,";
                         }
                         pids+=area.getId()+TmConstant.COMMA;
                         area.setParentids(pids);
                         areaMapper.update(area);
                     }
                     msg.setType(Msg.MsgType.success);
                     msg.setContent("保存区域成功");
                 }catch (Exception ex){
                     msg.setType(Msg.MsgType.error);
                     msg.setContent("保存区域失败");
                 }
                return msg;
    }
    public void addBatch(List<Area> areas){
        areaMapper.addBatch(areas);
    }
    public void update(Area area){
        areaMapper.update(area);
    }
    public void updateByMap(Map<String,Object> map){
        areaMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        areaMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        areaMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        areaMapper.deleteBatch(ids);
    }
     /*自定义方法*/
     public int getChildrenCount(String ids){
         ids=ids.replaceAll(TmConstant.COMMA, "','").replaceAll("~", "','");
         return areaMapper.getChildrenCount(ids);
     }
	 public List<Area> findByParentid(PageBounds pageBounds, String parentid){
		return areaMapper.findByParentid(pageBounds,parentid);
	}

    public String tree(Map<String, Object> map){
        String parentid= (String) map.get("parentid");
        Map<String, Object> rootNode=new HashMap<String, Object>();//根节点
        List<Area> list=null;
        List<Area> childrenlist=null;//每个区域的孩子集合
        Map<String, Object> attributes=null;//每个区域的属性
        List<Map<String, Object>> nodelist =null;//要返回的节点集合
        Map<String, Object> node=null;//节点
        if("0".equals(parentid)){
            list=areaMapper.findByParentid(new PageBounds(),"0");
            nodelist = new ArrayList<Map<String,Object>>();
            //组装根节点
            rootNode.put("id", "0");
            rootNode.put("text", "区域目录");
            if(list!=null&&list.size()>0){
                rootNode.put("state", "open");

                for(Area area:list){
                    node=new HashMap<String, Object>();
                    attributes=new HashMap<String, Object>();
                    String id=area.getId();

                    node.put("id", area.getId());
                    node.put("text", area.getName());

                    attributes.put("areatype", area.getAreatype());
                    attributes.put("code",area.getCode());
                    node.put("attributes", attributes);
                    childrenlist=areaMapper.findByParentid(new PageBounds(),id);
                    if(childrenlist!=null&childrenlist.size()>0){
                        node.put("state", "closed");
                    }
                    nodelist.add(node);
                }
                rootNode.put("children", nodelist);
            }
            JsonConfig jsonConfig = new JsonConfig();
            jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);//自动为我排除circle。
            JSONArray jsonArray = JSONArray.fromObject(rootNode, jsonConfig);
            return jsonArray.toString();
        }else{
            list=areaMapper.findByParentid(new PageBounds(),String.valueOf(parentid));
            nodelist = new ArrayList<Map<String,Object>>();
            if(list!=null&&list.size()>0){
                rootNode.put("state", "open");

                for(Area area:list){
                    node=new HashMap<String, Object>();
                    attributes=new HashMap<String, Object>();
                    String id=area.getId();

                    node.put("id", area.getId());
                    node.put("text", area.getName());

                    attributes.put("areatype", area.getAreatype());
                    attributes.put("code",area.getCode());
                    node.put("attributes", attributes);
                    childrenlist=areaMapper.findByParentid(new PageBounds(),id);
                    if(childrenlist!=null&childrenlist.size()>0){
                        node.put("state", "closed");
                    }
                    nodelist.add(node);
                }
            }
            JsonConfig jsonConfig = new JsonConfig();
            jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);//自动为我排除circle。
            JSONArray jsonArray = JSONArray.fromObject(nodelist, jsonConfig);
            return jsonArray.toString();
        }
    }
    public Object saveSort(Map map){
        Msg msg=new Msg();
        try{
            String sort = String.valueOf(map.get("sort"));
            if(!"".equals(sort)){
                String[] sx = sort.split(",");
                for(int i=0;i<sx.length;i++){
                    String[] arr = sx[i].split("_");
                    areaMapper.updateSort(arr[1], arr[2]);
                }
            }
            msg.setType(Msg.MsgType.success);
            msg.setContent("排序成功！");
        }catch (Exception e){
            msg.setType(Msg.MsgType.error);
            msg.setContent("排序失败！");
        }
        return msg;
    }
    public Object select(Map map){

        try{
            List<Area> areas=null;
            String type = String.valueOf(map.get("type"));
            String pid = String.valueOf(map.get("pid"));
            if("province".equals(type)){
             areas= areaMapper.findByParentid(new PageBounds(),"0");

            }
            /*else if("city".equals(type)){

            }else if("area".equals(type)){

            }*/
            else{
                areas= areaMapper.findByParentid(new PageBounds(),pid);
            }
            List<Map<String,Object>> ls=new ArrayList<Map<String,Object>>();
            if(areas!=null&&areas.size()>0){
                Map<String,Object> t=null;
                for (Area a:areas){
                    t=new HashMap<String,Object>();
                    t.put(TmConstant.DM_KEY,a.getId());
                    t.put(TmConstant.MC_KEY,a.getName());
                    ls.add(t);
                }
            }
            Map<String,Object> rmap=new HashMap<String,Object>();
            rmap.put(type+"Select",ls);
            return rmap;
        }catch (Exception e){
            return null;
        }

    }
    public Map<String,Object> getPositionProvinceList(String select_id,String now_province_id) {
        Map<String, Object> reurnMap = new HashMap<String, Object>();
        List<Map<String, Object>> ls = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> list = areaMapper.getPositionProvinceList();
        Map<String, Object> tmap = null;
        for (Map<String, Object> m : list){
            tmap = new HashMap<String, Object>();
            if(StringUtil.notEmpty(now_province_id)&&now_province_id.equals(String.valueOf(m.get("province_id")))){
                tmap.put("selected","selected");
            }
            tmap.put(TmConstant.DM_KEY,m.get("province_id"));
            tmap.put(TmConstant.MC_KEY, m.get("province_name"));
            ls.add(tmap);
        }
        reurnMap.put(select_id,ls);
        return reurnMap;
    }

    public Map<String,Object> getPositionCityByProvinceId(String select_id,String province_id,String now_city_id){
        Map<String, Object> reurnMap = new HashMap<String, Object>();
        List<Map<String, Object>> ls = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> list =areaMapper.getPositionCityByProvinceId(province_id);
        Map<String, Object> tmap = null;
        for (Map<String, Object> m : list){
            tmap = new HashMap<String, Object>();
            if(StringUtil.notEmpty(now_city_id)&&now_city_id.equals(String.valueOf(m.get("city_id")))){
                tmap.put("selected","selected");
            }
            tmap.put(TmConstant.DM_KEY,m.get("city_id"));
            tmap.put(TmConstant.MC_KEY, m.get("city_name"));
            ls.add(tmap);
        }
        reurnMap.put(select_id,ls);
        return reurnMap;
    }

    public Map<String,Object> getPositionCountryByCityId(String select_id,String city_id,String now_country_id){
        Map<String, Object> reurnMap = new HashMap<String, Object>();
        List<Map<String, Object>> ls = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> list =areaMapper.getPositionCountryByCityId(city_id);
        Map<String, Object> tmap = null;
        for (Map<String, Object> m : list){
            tmap = new HashMap<String, Object>();
            if(StringUtil.notEmpty(now_country_id)&&now_country_id.equals(String.valueOf(m.get("country_id")))){
                tmap.put("selected","selected");
            }
            tmap.put(TmConstant.DM_KEY,m.get("country_id"));
            tmap.put(TmConstant.MC_KEY, m.get("country_name"));
            ls.add(tmap);
        }
        reurnMap.put(select_id,ls);
        return reurnMap;
    }

    public Map<String,Object> getPositionTownByCountryId(String select_id,String country_id,String now_town_id){
        Map<String, Object> reurnMap = new HashMap<String, Object>();
        List<Map<String, Object>> ls = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> list = areaMapper.getPositionTownByCountryId(country_id);
        Map<String, Object> tmap = null;
        for (Map<String, Object> m : list){
            tmap = new HashMap<String, Object>();
            if(StringUtil.notEmpty(now_town_id)&&now_town_id.equals(String.valueOf(m.get("town_id")))){
                tmap.put("selected","selected");
            }
            tmap.put(TmConstant.DM_KEY,m.get("town_id"));
            tmap.put(TmConstant.MC_KEY, m.get("town_name"));
            ls.add(tmap);
        }
        reurnMap.put(select_id,ls);
        return reurnMap;
    }

    public Map<String,Object> initPositionSelect(String province_id,String province_select_id,String city_id,String city_select_id,String country_id,String country_select_id,String town_id,String town_select_id){
        Map<String, Object> reurnMap = new HashMap<String, Object>();
        Map<String, Object> tmap=null;
        if(StringUtil.notEmpty(province_id)&&StringUtil.notEmpty(province_select_id)){
            tmap=getPositionProvinceList(province_select_id,province_id);
            reurnMap.put("province_select_id",tmap.get("province_select_id"));
            if(StringUtil.notEmpty(city_id)){
                tmap=getPositionCityByProvinceId(city_select_id, province_id,city_id);
                reurnMap.put("city_select_id",tmap.get("city_select_id"));

                if(StringUtil.notEmpty(country_id)){
                    tmap=getPositionCountryByCityId(country_select_id, city_id, country_id);
                    reurnMap.put("country_select_id",tmap.get("country_select_id"));
                    if(StringUtil.notEmpty(town_id)){
                        tmap=getPositionTownByCountryId(town_select_id, country_id,town_id);
                        reurnMap.put("town_select_id",tmap.get("town_select_id"));
                    }else{

                    }
                }else{

                }
            }else{

            }
        }else{
            reurnMap.put("result","false");
            reurnMap.put("msg","province_id或province_select_id不能为空!");
        }
        reurnMap.put("result","true");
        return reurnMap;
    }

    public Object getPositionJson(int layer){
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        if(layer==4){
            List<Map<String, Object>> list = areaMapper.getPositionProvinceList();
            List<Map<String, Object>> list2 =null;
            List<Map<String, Object>> list3 =null;
            List<Map<String, Object>> list4 =null;
            List<Map<String, Object>> childrenList=null;
            List<Map<String, Object>> childrenList2=null;
            List<Map<String, Object>> childrenList3=null;
            Map<String, Object> tmap = null;
            Map<String, Object> tmap2 = null;
            Map<String, Object> tmap3 = null;
            Map<String, Object> tmap4 = null;
            for (Map<String, Object> m : list){
                tmap = new HashMap<String, Object>();
                tmap.put("value",m.get("province_id"));
                tmap.put("text", m.get("province_name"));
                childrenList=areaMapper.getPositionCityByProvinceId(String.valueOf(m.get("province_id")));
                list2 =new ArrayList<Map<String, Object>>();
                for (Map<String, Object> mm : childrenList){
                    tmap2 = new HashMap<String, Object>();
                    tmap2.put("value",mm.get("city_id"));
                    tmap2.put("text", mm.get("city_name"));
                    childrenList2=areaMapper.getPositionCountryByCityId(String.valueOf(mm.get("city_id")));
                    list3 =new ArrayList<Map<String, Object>>();
                    for (Map<String, Object> mmm : childrenList2){
                        tmap3 = new HashMap<String, Object>();
                        tmap3.put("value",mmm.get("country_id"));
                        tmap3.put("text", mmm.get("country_name"));
                        childrenList3=areaMapper.getPositionTownByCountryId(String.valueOf(mmm.get("country_id")));
                        list4 =new ArrayList<Map<String, Object>>();
                        for (Map<String, Object> mmmm : childrenList3){
                            tmap4 = new HashMap<String, Object>();
                            tmap4.put("value",mmmm.get("town_id"));
                            tmap4.put("text", mmmm.get("town_name"));
                            list4.add(tmap4);
                        }
                        childrenList3=null;
                        tmap3.put("children",list4);
                        list3.add(tmap3);
                    }
                    childrenList2=null;
                    tmap2.put("children",list3);
                    list2.add(tmap2);
                }
                childrenList=null;
                tmap.put("children",list2);
                resultList.add(tmap);
            }
        }else if(layer==3){
            List<Map<String, Object>> list = areaMapper.getPositionProvinceList();
            List<Map<String, Object>> list2 =null;
            List<Map<String, Object>> list3 =null;
            List<Map<String, Object>> childrenList=null;
            List<Map<String, Object>> childrenList2=null;
            Map<String, Object> tmap = null;
            Map<String, Object> tmap2 = null;
            Map<String, Object> tmap3 = null;
            for (Map<String, Object> m : list){
                tmap = new HashMap<String, Object>();
                tmap.put("value",m.get("province_id"));
                tmap.put("text", m.get("province_name"));
                childrenList=areaMapper.getPositionCityByProvinceId(String.valueOf(m.get("province_id")));
                list2 =new ArrayList<Map<String, Object>>();
                for (Map<String, Object> mm : childrenList){
                    tmap2 = new HashMap<String, Object>();
                    tmap2.put("value",mm.get("city_id"));
                    tmap2.put("text", mm.get("city_name"));
                    childrenList2=areaMapper.getPositionCountryByCityId(String.valueOf(mm.get("city_id")));
                    list3 =new ArrayList<Map<String, Object>>();
                    for (Map<String, Object> mmm : childrenList2){
                        tmap3 = new HashMap<String, Object>();
                        tmap3.put("value",mmm.get("country_id"));
                        tmap3.put("text", mmm.get("country_name"));
                        list3.add(tmap3);
                    }
                    childrenList2=null;
                    tmap2.put("children",list3);
                    list2.add(tmap2);
                }
                childrenList=null;
                tmap.put("children",list2);
                resultList.add(tmap);
            }
        }else if(layer==2){
            List<Map<String, Object>> list = areaMapper.getPositionProvinceList();
            List<Map<String, Object>> list2 =null;
            List<Map<String, Object>> childrenList=null;
            Map<String, Object> tmap = null;
            Map<String, Object> tmap2 = null;
            for (Map<String, Object> m : list){
                tmap = new HashMap<String, Object>();
                tmap.put("value",m.get("province_id"));
                tmap.put("text", m.get("province_name"));
                childrenList=areaMapper.getPositionCityByProvinceId(String.valueOf(m.get("province_id")));
                list2 =new ArrayList<Map<String, Object>>();
                for (Map<String, Object> mm : childrenList){
                    tmap2 = new HashMap<String, Object>();
                    tmap2.put("value",mm.get("city_id"));
                    tmap2.put("text", mm.get("city_name"));
                    list2.add(tmap2);
                }
                tmap.put("children",list2);
                resultList.add(tmap);
            }

        }else{

            List<Map<String, Object>> list = areaMapper.getPositionProvinceList();
            Map<String, Object> tmap = null;
            for (Map<String, Object> m : list){
                tmap = new HashMap<String, Object>();
                tmap.put("value",m.get("province_id"));
                tmap.put("text", m.get("province_name"));
                resultList.add(tmap);
            }
        }
        return resultList;
    }
}
