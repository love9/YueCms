package com.makbro.core.base.login.service;

import com.makbro.core.base.login.dao.LoginMapper;
import com.makbro.core.framework.sso.server.SsoUserService;
import com.markbro.base.model.LoginBean;
import com.markbro.sso.core.entity.ReturnT;
import com.markbro.sso.core.user.SsoUserInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Sso  用户登录查找用户
 */
public class SsoUserServiceImpl implements SsoUserService {
    protected Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    LoginMapper loginMapper;

    @Autowired
    LoginService loginService;

    @Override
    public ReturnT<SsoUserInfo> findUser(String username, String password) {

        if(StringUtils.isEmpty(username)||StringUtils.isEmpty(password)){
            return new ReturnT<SsoUserInfo>(ReturnT.FAIL_CODE, "用户名或密码不能为空!");
        }

        Map<String, Object> yhMap = loginMapper.queryYhidByDlmc(username);
        if (yhMap == null) {
            return new ReturnT<SsoUserInfo>(ReturnT.FAIL_CODE, "用户名或密码错误!");
        }
        if (String.valueOf(yhMap.get("available")).equals("0")) {
            return new ReturnT<SsoUserInfo>(ReturnT.FAIL_CODE, "帐号状态异常！");
        }
        if (!loginService.validate(yhMap, password)) {
            return new ReturnT<SsoUserInfo>(ReturnT.FAIL_CODE, "用户名或密码错误!");
        }
        SsoUserInfo ssoUserInfo=SsoUserInfo.build().setUserMap(yhMap).setUserid(String.valueOf(yhMap.get("yhid"))).setUsername(username).setPassword(password).setNickname(String.valueOf(yhMap.get("nickname")));

        this.afterValidateSuccess(ssoUserInfo);
        return  new ReturnT<SsoUserInfo>(ssoUserInfo);
    }

    //用户登录验证成功
    @Override
    public void afterValidateSuccess(SsoUserInfo ssoUserInfo) {
        log.info(">>>>>>>>>>>>>>>>> 用户["+ssoUserInfo.getUsername()+"] sso login success!");
        //因不同系统而异，用户登录验证成功后有的系统往缓存存放一些该登录用户的信息，可以在这里完成
        try {
            LoginBean loginBean= loginService.cacheInfo(ssoUserInfo.getUserid());
            if(loginBean!=null){
                log.info(">>>>>>>>>>>>>>>>> 用户["+ssoUserInfo.getUsername()+"] sso login cacheInfo success! ");
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

}
