
package com.makbro.core.base.system.service;

import com.makbro.core.base.login.dao.LoginMapper;
import com.makbro.core.base.orgPosition.bean.Position;
import com.makbro.core.base.orgPosition.dao.PositionMapper;
import com.makbro.core.base.orgRole.bean.Role;
import com.makbro.core.base.orgRole.dao.RoleMapper;
import com.makbro.core.base.orgTree.bean.OrgTree;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.orgUser.dao.OrgUserMapper;
import com.makbro.core.base.permission.service.PermissionService;
import com.makbro.core.base.system.dao.AccountMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.mysession.dao.MySessionMapper;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.Md5;
import com.markbro.base.utils.RequestContextHolderUtil;
import com.markbro.base.utils.SysPara;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Account service
 * Created by wujiyue on 2017年8月30日 15:47:02
 */

@Service
public class AccountService {
    protected Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private LoginMapper loginMapper;
    @Autowired
    private OrgUserMapper userMapper;
    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private MySessionMapper mySessionMapper;
    /*@Autowired
    private RecentNewsMapper recentNewsMapper;
    @Autowired
    private MyWebsocketHandler handler;
    @Autowired
    TemplateService templateService;*/
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    PositionMapper positionMapper;
    @Autowired
    OrgTreeMapper orgTreeMapper;
    @Autowired
    private TableKeyService yhKeyService;

    @Value("${tencentCaptcha.appId}")
    private String tencentCaptcha_appId;
    @Value("${tencentCaptcha.appSecretKey}")
    private String tencentCaptcha_appSecretKey;

    public OrgUser getUserByEmail(String email){
       return   userMapper.getUserByEmail(email);
    }

    /**
     * 获得登陆日志条数
     * @return
     */
    public int getLoginLogCount(){
        String yhid=MyBatisRequestUtil.getYhid();
        int n=accountMapper.getLoginLogCount(yhid);
        return n;
    }
    @Transactional
    public OrgUser regDeveloperAccount(String email,String pwd){
        try{
            //这里最好再检查一遍是否有该email账户，检测account和email这2个字段。
            OrgUser user=new OrgUser();
            user.setOrgid("0");
            user.setAccount(email);
            user.setPassword(pwd);
            user.setUsertype("developer");
            user.setAge(18);
            user.setCreateTime(DateUtil.getDatetime());
            user.setUpdateTime(DateUtil.getDatetime());
            user.setNickname(email);
            user.setEmail(email);
            user.setEmailflag(1);
            user.setAvailable(0);
            user.setDeleted(0);
            user.setHeaderPath(TmConstant.KEY_USER_DEFAULT_HEAD_ICON);
            user.setId(yhKeyService.getStringId());
            userMapper.add(user);
            userMapper.update(user);//主要是为了更新available为0，因为新增默认是1

            //赋予用户角色权限菜单
            roleMapper.addUserRole(user.getId(),"1763");//1763是角色表中的开发人员角色ID
            //添加树级结构(默认添加到研发部门的研发岗位（岗位ID是5）)
            OrgTree tree=new OrgTree();
            tree.setOrgid("0");
            tree.setId(user.getId());
            tree.setParentid("5");//（岗位ID是5）)
            tree.setType("ry");
            tree.setSjbmid("0");
            tree.setLxid(user.getId());
            tree.setName(user.getNickname());
            tree.setAvailable(1);
            tree.setDeleted(0);
            orgTreeMapper.add(tree);
            return user;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
    //用户申请注册为开发者发送邮件
    public Object sendRegDeveloperEmail(String email,String pwd, String baseLink) {
        Msg obj = new Msg();
        obj.setType(Msg.MsgType.error);
        String msg = "";
       /* try {
            if(!email.endsWith("@qq.com")){
                msg = "很抱歉，目前只支持QQ邮箱!";
                obj.setContent(msg);
                obj.setExtend("error_type","email");
                return obj;
            }
            OrgUser user = userMapper.getUserByEmail(email);
            if (user != null) {
                //邮箱已经被占用
                msg = "邮箱已经被占用!";
                obj.setContent(msg);
                obj.setExtend("error_type","email");
                return obj;
            }
            String jwtGuid = Guid.get();
            Map subMap = new HashMap<String, Object>();
            subMap.put("email", email);
            subMap.put("password", pwd);
            String jwt = JWTUtil.createJWT(jwtGuid, subMap, 1000 * 60 * 60*24*3);//3天有效期
            String link = baseLink + "/reg/developer/active?token=" + jwt;//用户在邮件内点击这个链接
            //组装邮件内容
            Template emailTemplate = templateService.getByName("EmailRegDeveloper");//EmailRegDeveloper  是邮件模板代码
            if (emailTemplate != null) {
                VelocityHelper helper = new VelocityHelper("");
                helper.AddKeyValue("email", email).AddKeyValue("link", link);
                String templateContent = emailTemplate.getContent();
                String email_content = helper.ExecuteMergeString(templateContent);
                //发送邮件
                MailSenderInfo info = new MailSenderInfo();
                info.setMailServiceHost("smtp.qq.com");//qq邮箱服务器
                info.setMailServicePort("25");//端口号
                info.setFromAddress("747506908@qq.com");
                info.setValidate(true);
                info.setUserName("747506908@qq.com");
                info.setPassword("swigvspxyywdbehe");//qq邮箱16位安全码，作用相当于密码
                String[] to = {email};
                info.setToAddress(to);
                info.setSubject("申请参与开发");
                info.setContent(email_content);
                SimpleMailSender sender = new SimpleMailSender();
                boolean b = sender.sendHtmlMail(info);
                if (b) { //返回前台结果
                    obj.setType(Msg.MsgType.success);
                    msg = "发送邮件成功!";
                } else {
                    msg = "发送邮件失败!";
                }
            } else {
                msg = "发送邮件失败!邮件模板为空!请联系管理员!";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msg = "用户申请注册为开发者发送邮件异常!";
        }*/
        obj.setContent(msg);
        return obj;
    }

    //重置密码时，给用户发送重置密码邮件
    public Object sendResetEmail(String email, String baseLink) {
        Msg obj = new Msg();
        obj.setType(Msg.MsgType.error);
        String msg = "";
        /*try {
            OrgUser user = userMapper.getUserByEmail(email);
            if (user != null) {
                if (user.getAvailable() == 1) {
                    //存在该用户，并且正常
                    String yhid = user.getId();
                    String jwtGuid = Guid.get();
                    Map subMap = new HashMap<String, Object>();
                    subMap.put("yhid", yhid);
                    String jwt = JWTUtil.createJWT(jwtGuid, subMap, 1000 * 60 * 60);
                    String link = baseLink + "/account/pwdreset?token=" + jwt;//用户在邮件内点击这个链接
                    //组装邮件内容
                    Template emailTemplate = templateService.getByName("EmailReset");//EmailReset  是邮件模板代码
                    if (emailTemplate != null) {
                        VelocityHelper helper = new VelocityHelper("");
                        helper.AddKeyValue("username", user.getNickname())
                                .AddKeyValue("link", link);
                        String templateContent = emailTemplate.getContent();
                        String email_content = helper.ExecuteMergeString(templateContent);
                        //发送邮件
                        MailSenderInfo info = new MailSenderInfo();
                        info.setMailServiceHost("smtp.qq.com");//qq邮箱服务器
                        info.setMailServicePort("25");//端口号
                        info.setFromAddress("747506908@qq.com");
                        info.setValidate(true);
                        info.setUserName("747506908@qq.com");
                        info.setPassword("swigvspxyywdbehe");
                        String[] to = {email};
                        info.setToAddress(to);
                        info.setSubject("重置密码");
                        info.setContent(email_content);
                        //String[] attach={"D:\\1.jpg"};
                        //info.setAttachs(attach);
                        SimpleMailSender sender = new SimpleMailSender();
                        boolean b = sender.sendHtmlMail(info);

                        //返回前台结果
                        if (b) {
                            obj.setType(Msg.MsgType.success);
                            msg = "发送邮件成功!";
                        } else {
                            msg = "发送邮件失败!";
                        }
                    } else {
                        msg = "发送邮件失败!邮件模板为空!";
                    }

                } else {
                    //该用户账户状态不正常
                    msg = "该用户状态异常!";
                }
            } else {
                msg = "不存在该邮箱!";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msg = "验证邮箱异常!";
        }*/
        obj.setContent(msg);
        return obj;
    }

    //查询用户旧密码。只在找回密码中使用
    public String getPasswordByYhid(String yhid) {
        return userMapper.getPasswordByYhid(yhid);
    }

    public Object changePwd(Map<String, Object> map) {
        Msg msg = new Msg();
      /*  RecentNews news = new RecentNews();
        try {
            news = MyBatisRequestUtil.convertToBean(map, news);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        String yhid = String.valueOf(map.get(TmConstant.YHID_KEY));
        String oldPwd = String.valueOf(map.get("oldPwd"));
        String newPwd = String.valueOf(map.get("newPwd"));
        String confirm = String.valueOf(map.get("confirm"));
        //校验2次新
        if (!newPwd.equals(confirm)) {
            if (StringUtil.notEmpty(newPwd) && StringUtil.notEmpty(confirm)) {
                msg.setType(Msg.MsgType.error);
                msg.setContent("两次输入密码不一致！");
            } else {
                msg.setType(Msg.MsgType.error);
                msg.setContent("参数异常！");
            }
            return msg;
        }
        //校验旧密码是否正确
        OrgUser user = userMapper.get(yhid);
        if (user.getPassword().equals(oldPwd)) {
            loginMapper.changePwd(yhid, newPwd);
            msg.setType(Msg.MsgType.success);
            msg.setContent("密码修改成功！");
            MyMessage mymsg = new MyMessage();
            mymsg.setFromYhid("1");
            mymsg.setFromName("系统");
            mymsg.setToYhid(yhid);
            String xm = String.valueOf(EhCacheUtils.getUserInfo(TmConstant.XM_KEY, yhid));
            mymsg.setToName(xm);
            mymsg.setText(xm + "刚刚修改了密码！");
            try {
                handler.sendMessageToUser(yhid, new TextMessage(new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create().toJson(mymsg)));
            } catch (IOException e) {
                e.printStackTrace();
            }
            //记录消息日志
            news.setModule("personal");
            news.setPushscope(TmConstant.PUSH_SCOPE_RECENT_NEWS_ONLYME);// 0仅个人  1全部推送  2仅推送本部门
            news.setNewstype(TmConstant.RECENT_NEWS_TEXT);// 1	文本消息2	图文消息3	附件消息（文档） 4	申请消息 5	工作动态  0系统通知
            news.setCreateBy(yhid);
            news.setPushflag("1");
            news.setTitle(xm + "修改了账户密码！");
            //String xm=EhCacheUtils.getUserInfo(TmConstant.XM_KEY,yhid).toString();
            news.setContent(xm + "修改了账户密码！");
            recentNewsMapper.add(news);
        } else {
            msg.setType(Msg.MsgType.error);
            msg.setContent("旧密码输入错误！");
        }*/
        return msg;
    }

    public void saveRecentNewsAndPushToUser(Map<String, Object> map){
        /*RecentNews news = new RecentNews();
        try {
            news = MyBatisRequestUtil.convertToBean(map, news);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        MyMessage mymsg = new MyMessage();
        mymsg.setFromYhid("1");
        mymsg.setFromName("系统");
        mymsg.setToYhid(news.getYhid());
        String xm = String.valueOf(EhCacheUtils.getUserInfo(TmConstant.XM_KEY, news.getYhid()));
        mymsg.setToName(xm);
        mymsg.setText(news.getContent());
        try {
            handler.sendMessageToUser(news.getYhid(), new TextMessage(new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create().toJson(mymsg)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //记录消息日志
        news.setModule("system");
        news.setPushscope(TmConstant.PUSH_SCOPE_RECENT_NEWS_ALL);//   0仅个人  1全部推送  2仅推送本部门
        news.setNewstype(TmConstant.RECENT_NEWS_SYSTEM);// 1	文本消息2	图文消息3	附件消息（文档） 4	申请消息 5	工作动态  0系统通知
        news.setCreateBy(news.getYhid());
        news.setPushflag("1");
        recentNewsMapper.add(news);*/
    }
    public Object saveHeader(Map<String, Object> map) {
        Msg msg = new Msg();
        String path = String.valueOf(map.get("wjlj"));
        String yhid = String.valueOf(map.get(TmConstant.YHID_KEY));
        if (StringUtil.isEmpty(path) || StringUtil.isEmpty(yhid)) {
            msg.setType(Msg.MsgType.error);
            msg.setContent("参数非法！");
            return msg;
        }
        userMapper.saveHeaderPath(path, yhid);
        msg.setType(Msg.MsgType.success);
        msg.setContent("保存头像成功！");
        //刷新缓存信息
        EhCacheUtils.putUserInfo("headerPath", yhid, path);
        return msg;
    }

    /**
     * 验证用户名与密码是否正确
     *
     * @param dlmc
     * @param dlmm
     * @return
     */

    private boolean validate(String dlmc, String dlmm) {
        Map<String, Object> localList = loginMapper.queryYhidByDlmc(dlmc);
        String yhid = String.valueOf(localList.get(TmConstant.YHID_KEY));
        String sjkdlmm = String.valueOf(localList.get("dlmm"));


        //加密模式
        String enc_mode = "4";//明文
        try {
            enc_mode = SysPara.getValue("sys_encryptPassword_mode");
        } catch (Exception ex) {

        }
        if ("1".equals(enc_mode)) {//md5(盐+密码)
            dlmm = Md5.getMd5(yhid.toString() + dlmm);
        } else if ("2".equals(enc_mode)) {//md5(密码)
            dlmm = Md5.getMd5(dlmm);
        } else if ("3".equals(enc_mode)) {//md5(md5(密码))
            dlmm = Md5.getMd5(Md5.getMd5(dlmm));
        } else {//明文
        }
        return dlmm.equalsIgnoreCase(sjkdlmm);

    }

    /**
     * 2017年9月18日 11:44:51 by wujiyue
     * 上传用户头像
     *
     * @param map
     * @param req
     * @param mFile
     * @return
     */
    public Map<String, Object> uploadHeaderImage(Map<String, Object> map, HttpServletRequest req, MultipartFile mFile) {
        //资源图片路径(相对于webapp下的路径)
        String basePathType = SysPara.getValue("uploadpath_type","0");
        String basepath = "";
        String relativePath = "";
        if ("1".equals(basePathType)) {//绝对路径类型
            basepath = SysPara.getValue("uploadfile_basepath","D:/dzd");
            relativePath = "/uploadresources";
        } else {
            basepath = req.getSession().getServletContext().getRealPath("/");
            relativePath = SysPara.getValue("uploadfile_basepath","/resources/static/cms/images");
        }
        relativePath = StringUtil.subEndStr(relativePath, "/");
        File dirFile = new File(basepath + relativePath);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        try {
            req.setCharacterEncoding("utf-8");

            long size = 0;
            String path = "";
            String filename = "";
            String name = "";
            String dir = "";
            String suffixes = "";
            String guid = Guid.get();
            if (!mFile.isEmpty()) {
                BufferedInputStream in = new BufferedInputStream(mFile.getInputStream());
                filename = mFile.getOriginalFilename();
                name = filename.substring(0, filename.indexOf("."));
                // 取得文件后缀
                suffixes = filename.substring(filename.lastIndexOf("."), filename.length());
                path = relativePath + "/" + DateUtil.formatDate(new Date(), "yyyyMMdd") + "/" + guid + suffixes;//相对路径
                dir = basepath + File.separatorChar + path;// 绝对路径
                String tempDir = dir.substring(0, dir.lastIndexOf("/"));
                File tempDirFile = new File(tempDir);
                if (!tempDirFile.exists()) {
                    tempDirFile.mkdirs();
                }
                File ffout = new File(dir);
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(ffout));
                Streams.copy(in, out, true);
                size = ffout.length();
            }
            map.put("path", path);
            map.put("name", name);
            map.put("suffixes", suffixes);
            map.put("size", size);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
    //@Autowired
   // ApplayDeveloperMapper applayDeveloperMapper;
    //用户申请成为开发者并自动提交审核

    @Transactional
    public Object applayDeveloperAndSubmit(Map<String,Object> map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.error);
        /*try{
            ApplayDeveloper applayDeveloper= MapUtils.toObject(ApplayDeveloper.class, map);
            if(StringUtil.isEmpty(String.valueOf(map.get("id"))) || applayDeveloper.getId()==null||"".equals(applayDeveloper.getId().toString())){
                applayDeveloperMapper.add(applayDeveloper);
            }else{
                applayDeveloperMapper.update(applayDeveloper);
            }
            List<ApplayDeveloper> list=new ArrayList<ApplayDeveloper>();
            list.add(applayDeveloper);
           boolean b = submitDraft(list,String.valueOf(map.get(TmConstant.YHID_KEY)),"0");
            if(b){
                msg.setContent("提交审核成功!");
            }else{
                msg.setContent("提交审核失败!请联系管理员!");
            }
        }catch (Exception ex){
            ex.printStackTrace();
            msg.setContent("申请成为开发者出现异常!请联系管理员!");
        }*/
        return msg;
    }

    private static final String processType = "applay_developer";
    private static   String processId = "process_81";
    private static final String currentNode = "process_81_1";
    private static final String nextNode = "process_81_2";
    private static final String dealPerson = "1";

    /*@Autowired
     NodefineMapper nodefineMapper;
    @Autowired
    ProcessManageMapper processManageMapper;
    @Autowired
    ProcessMapper processMapper;*/
    @Transactional
     /* public boolean submitDraft(List<ApplayDeveloper> list,String yhid,String orgid){
      try {
            for (ApplayDeveloper sqd : list) {
                String guid= Guid.get();
                String Sign ="1";
                String finish = "0";
                String Feedback = "单据提交";
                //String currentnode = "process_81_1";
                //String nextnode = "process_81_2";
                String zbguid = sqd.getId().toString();//订单、申请流程业务表中的记录主键

                String xm= String.valueOf(EhCacheUtils.getUserInfo(TmConstant.XM_KEY, yhid));//当前登录用户名称
                if(StringUtil.isEmpty(xm)){
                    OrgUser user = userMapper.get(yhid);
                    if(user!=null){
                        xm=user.getNickname();
                    }
                }
                Map<String, Object> tempMap = new HashMap<String, Object>();
                Map<String, Object> processOrderInfoMap = new HashMap<String, Object>();
                Map<String, Object> processOrderMap = new HashMap<String, Object>();
                Map<String, Object> processOrderDealMap = new HashMap<String, Object>();

                Nodefine nodefine=null;
                //如果审批意见是同意
                if ("1".equals(Sign)) {
                    //同意
                    processOrderInfoMap.put("wantnode", nextNode);
                    processOrderDealMap.put("nextnode", nextNode);
                    processOrderDealMap.put("nextnodename", "");
                    if(!"".equals(nextNode)) {
                        nodefine = nodefineMapper.getByNodeid(nextNode,orgid);
                        if (null != nodefine) {
                            processOrderDealMap.put("nextnodename", nodefine.getNodename());
                        }
                    }
                    if ("0".equals(finish)) {
                        // 如果不结束订单
                        processOrderInfoMap.put("orderstate", "1");
                        processOrderMap.put("processstate", "1");
                        processOrderDealMap.put("processcurrentstate", "1");
                        processOrderDealMap.put("processcurrentstatememo", "审批中");
                        processOrderInfoMap.put("operator", dealPerson);
                        //TODO 发送推送消息
                        //通知消息推送
                    }
                    processOrderMap.put("processtype", processType);
                    processOrderMap.put("guid", zbguid);
                    //更新订单申请的流程处理状态
                    processMapper.updateProcessState(processOrderMap);
                    if(StringUtil.isEmpty(processId)){
                        processId=processMapper.getDefaultProcessid(processOrderMap);
                    }
                    processOrderInfoMap.put("processid", processId);
                    processOrderInfoMap.put("processcode", processId);
                    processOrderInfoMap.put("finishednode", currentNode);
                    processOrderInfoMap.put("guid", zbguid);
                    processOrderInfoMap.put("processtype", processType);

                    int count = processMapper.processOrderInfoExist(processType,zbguid);
                    if(count>0) {
                        processMapper.updateProcessOrderInfo(processOrderInfoMap);
                    }else {
                        processMapper.insertProcessOrderInfo(processOrderInfoMap);
                    }

                    processOrderDealMap.put("guid", guid);
                    processOrderDealMap.put("zbguid", zbguid);
                    processOrderDealMap.put("node", currentNode);
                    nodefine = nodefineMapper.getByNodeid(currentNode,orgid);
                    processOrderDealMap.put("nodename", nodefine.getNodename());
                    processOrderDealMap.put("sign", Sign);
                    processOrderDealMap.put("signmemo", "同意");
                    processOrderDealMap.put("dealperson", PatternUtil.isNull(yhid));
                    processOrderDealMap.put("dealpersonname", xm);
                    processOrderDealMap.put("feedback", Feedback);
                    processOrderDealMap.put("memo", "");
                    processOrderDealMap.put("processtype", processType);
                    processMapper.saveProcessOrderDealInfo(processOrderDealMap);
                    //msg.setType(Msg.MsgType.success);
                    //msg.setContent("操作成功，单据已经提交");
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ApplicationException("提交申请单草稿出现异常!");
        }
        return true;
    }*/



    /**
     * 腾讯验证码验证服务。该方法在包含腾讯验证码的登录或注册方法内调用。
     * @param tencentCaptcha_ticket    使用腾讯验证码的表单内有隐藏域ticket
     * @param tencentCaptcha_Randstr   使用腾讯验证码的表单内有隐藏域 Randstr
     * @param userIp    根据request获取客户端的用户IP
     * @return
     */
    public String ticketVerify(String tencentCaptcha_ticket,String tencentCaptcha_Randstr,String userIp) {
        try {
            long startt = System.currentTimeMillis();
            String tencentCaptcha_url="https://ssl.captcha.qq.com/ticket/verify?aid=APPID&AppSecretKey=APPSECRETKEY&Ticket=TICKET&Randstr=RAND&UserIP=USERIP";
            String _url =tencentCaptcha_url.replaceAll("APPID",tencentCaptcha_appId).replaceAll("APPSECRETKEY",tencentCaptcha_appSecretKey).replaceAll("TICKET",tencentCaptcha_ticket).replaceAll("RAND",tencentCaptcha_Randstr).replaceAll("USERIP",userIp);
            URL url = null;
            HttpURLConnection urlConn = null;
            url = new URL(_url);
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "UTF-8"));
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = rd.read()) > -1) {
                sb.append((char) ch);
            }
            rd.close();
            long end = System.currentTimeMillis();
            return sb.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    /**
     * 检测账户密码是否正确
     * @param account
     * @param password
     * @return
     */
    public boolean checkAccountAndPwd(String account,String password){
        try{
            int n=accountMapper.checkAccountAndPwd(account,password);
            if(n>0){
                return true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * 检测账户名是否能注册使用
     * @param account
     * @return true可以使用，false不能使用
     */
    public boolean checkAccount(String account){
        try{
            int n=accountMapper.checkAccount(account);
            if(n>0){
                return false;
            }
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * 绑定本系统用户和第三方用户
     * @param thirdPartyUser
     * @param yhid
     * @return
     */
    /*public boolean thirdBindUser(ThirdPartyUser thirdPartyUser,String yhid){
        try{
            Map<String,Object> insertMap=new HashMap<String,Object>();

            insertMap.put("yhid",yhid);
            insertMap.put("third_openid",thirdPartyUser.getOpenid());
            insertMap.put("bind_time", DateUtil.getDatetime());
            insertMap.put("login_type",thirdPartyUser.getProvider());//qq,weixin,sina
            accountMapper.deleteThirdOauth(yhid,thirdPartyUser.getProvider());
            accountMapper.insertThirdOauth(insertMap);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }*/


    /**
     * 解绑第三方用户
     * @param yhid 要解绑的本系统用户的yhid
     * @param type 解绑类型，qq，weixin,sina
     * @return
     */
    public boolean thirdUnBindUser(String yhid,String type){
        try{
            accountMapper.deleteThirdOauth(yhid,type);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
    //查询第三方用户是否已经绑定本系统用户
    public Map<String,Object> getThirdOauthByOpenid(String openid){
         return   accountMapper.getThirdOauthByOpenid(openid);
    }

    public String getYhidByAccount(String account){
        return  userMapper.getYhidByAccount(account);
    }



    /**
     * 注册用户
     * @param user 用户
     * @param roleId 角色ID
     * @param positionId 岗位ID
     * @return
     */
    @Transactional
    public OrgUser registeUser(OrgUser user,String roleId,String positionId){
            try{
                String id= yhKeyService.getStringId();
                user.setId(id);
                HttpServletRequest request= RequestContextHolderUtil.getRequest();
                if( user==null || StringUtil.isEmpty(roleId) || StringUtil.isEmpty(positionId) ){
                    throw new ApplicationException("注册用户参数为空!");
                }
                Role role=roleMapper.get(roleId);
                if(role==null){
                    throw new ApplicationException("注册用户角色信息参数为空!");
                }
                Position position=positionMapper.get(positionId);
                if(position==null){
                    throw new ApplicationException("注册用户岗位信息参数为空!");
                }
                //检测一些参数
                if(StringUtil.isEmpty(user.getOrgid())){
                    //用户没有指定orgid,那么role和position都有orgid信息
                    if(role.getOrgid().equals(position.getOrgid())){
                        user.setOrgid(role.getOrgid());
                    }else{
                        throw new ApplicationException("注册用户角色和岗位的orgid信息不一致!");
                    }
                }
                if(StringUtil.isEmpty(user.getHeaderPath())){
                    user.setHeaderPath(TmConstant.KEY_USER_DEFAULT_HEAD_ICON);
                }
                user.setLoginCount(0);
                user.setCreateTime(DateUtil.getDatetime());
                user.setUpdateTime(DateUtil.getDatetime());
                user.setLastLoginIp(MyBatisRequestUtil.getIpByHttpRequest(request));

                userMapper.add(user);//插入用户
                roleMapper.addUserRole(user.getId(),role.getId());  //关联用户角色

                //新建用户目录树
                OrgTree parentTree=orgTreeMapper.getByLxidAndType(positionId,"gw",user.getOrgid());
                if(parentTree==null){
                    throw new ApplicationException("注册用户岗位树为空!");
                }
                OrgTree tree=new OrgTree();
                tree.setOrgid(user.getOrgid());
                tree.setId(Guid.get());
                tree.setParentid(parentTree.getId());
                tree.setType("ry");
                tree.setSjbmid(position.getParentid());//岗位的parentid就是所在部门的部门id
                tree.setLxid(user.getId());
                tree.setName(user.getNickname());
                tree.setAvailable(1);
                tree.setDeleted(0);
                orgTreeMapper.add(tree);
                return user;
            }catch (Exception ex){
                ex.printStackTrace();
                throw new ApplicationException("注册用户时发生异常!");
            }
    }

    //查询第三方绑定账户列表
    public Object findThirdOauthByYhid(String yhid){
        Map<String,Object> res=new HashMap<String,Object>();
        List<Map<String,Object>> list=accountMapper.findThirdOauthByYhid(yhid);
        res.put("total",list.size());
        res.put("rows",list);
        return res;
    }
    public Object unBindThirdLogin(Map map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.error);
        String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
        String type=String.valueOf(map.get("type"));
        if(StringUtils.isEmpty(yhid)||StringUtils.isEmpty(type)){
            msg.setContent("解绑第三方登录用户失败!");
        }
        accountMapper.deleteThirdOauth(yhid,type);
        msg.setType(Msg.MsgType.success);
        msg.setContent("解绑成功!");
        return msg;
    }
}

