package com.makbro.core.base.recentNews.bean;


import com.markbro.base.model.AliasModel;

/**
 * 最新动态 bean
 * @author  wujiyue on 2017-08-29 13:30:54 .
 */
public class RecentNews  implements AliasModel {

	private Integer id;//
	private String orgid;//
	private String bmid;//
	private String gwid;//
	private String yhid;//
	private String module;//消息所属模块
	private String newstype;//消息类型。比如文本通知消息、图文消息、文档附件消息、申请消息等
	private String title;//消息标题
	private String content;//消息内容
	private String refid;//引用id，比如某人提交了某种申请的记录id.再比如某人接受了某个任务的任务id.
	private String attachefiles;//存储附件ids.比如图文消息的图片记录ids逗号分割。比如工作周报消息附件id
	private String createBy;//
	private String createTime;//
	private String pushscope;//该消息推送范围
	private String pushflag;//该消息是否已经推送
	private String readflag;//该消息是否已经阅读
	private String elapse;//该消息距离当前有多长时间了

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getElapse() {
		return elapse;
	}

	public void setElapse(String elapse) {
		this.elapse = elapse;
	}

	public String getPushflag() {
		return pushflag;
	}

	public void setPushflag(String pushflag) {
		this.pushflag = pushflag;
	}

	public String getReadflag() {
		return readflag;
	}

	public void setReadflag(String readflag) {
		this.readflag = readflag;
	}

	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getModule(){ return module ;}
	public void  setModule(String module){this.module=module; }
	public String getNewstype(){ return newstype ;}
	public void  setNewstype(String newstype){this.newstype=newstype; }
	public String getContent(){ return content ;}
	public void  setContent(String content){this.content=content; }
	public String getRefid(){ return refid ;}
	public void  setRefid(String refid){this.refid=refid; }
	public String getAttachefiles(){ return attachefiles ;}
	public void  setAttachefiles(String attachefiles){this.attachefiles=attachefiles; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getPushscope(){ return pushscope ;}
	public void  setPushscope(String pushscope){this.pushscope=pushscope; }

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getBmid() {
		return bmid;
	}

	public void setBmid(String bmid) {
		this.bmid = bmid;
	}

	public String getGwid() {
		return gwid;
	}

	public void setGwid(String gwid) {
		this.gwid = gwid;
	}

	public String getYhid() {
		return yhid;
	}

	public void setYhid(String yhid) {
		this.yhid = yhid;
	}

	@Override
	public String toString() {
	return "RecentNews{" +
			"id=" + id+
			", orgid=" + orgid+
			", bmid=" + bmid+
			", gwid=" + gwid+
			", yhid=" + gwid+
			", module=" + module+
			", newstype=" + newstype+
			", content=" + content+
			", refid=" + refid+
			", attachefiles=" + attachefiles+
			", createBy=" + createBy+
			", createTime=" + createTime+
			", pushscope=" + pushscope+
			 '}';
	}
}
