package com.makbro.core.base.smsHis.bean;


import com.markbro.base.model.AliasModel;

/**
 * 短信发送记录 bean
 * @author wujiyue
 * @date 2018-11-06 16:08:02
 */
public class SmsHis  implements AliasModel {

	private String orgid;//
	private String bmid;//
	private String gwid;//
	private String yhid;//
	private String yhmc;//
	private String carrieroperator;//运营商
	private Integer id;//
	private String phone;//
	private String content;//
	private String returncode;//
	private String createTime;//


	public String getOrgid(){ return orgid ;}
	public void  setOrgid(String orgid){this.orgid=orgid; }
	public String getBmid(){ return bmid ;}
	public void  setBmid(String bmid){this.bmid=bmid; }
	public String getGwid(){ return gwid ;}
	public void  setGwid(String gwid){this.gwid=gwid; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getYhmc(){ return yhmc ;}
	public void  setYhmc(String yhmc){this.yhmc=yhmc; }
	public String getCarrieroperator(){ return carrieroperator ;}
	public void  setCarrieroperator(String carrieroperator){this.carrieroperator=carrieroperator; }
	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getPhone(){ return phone ;}
	public void  setPhone(String phone){this.phone=phone; }
	public String getContent(){ return content ;}
	public void  setContent(String content){this.content=content; }
	public String getReturncode(){ return returncode ;}
	public void  setReturncode(String returncode){this.returncode=returncode; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }


	@Override
	public String toString() {
	return "SmsHis{" +
			"orgid=" + orgid+
			", bmid=" + bmid+
			", gwid=" + gwid+
			", yhid=" + yhid+
			", yhmc=" + yhmc+
			", carrieroperator=" + carrieroperator+
			", id=" + id+
			", phone=" + phone+
			", content=" + content+
			", returncode=" + returncode+
			", createTime=" + createTime+
			 '}';
	}
}
