package com.makbro.core.base.orgUser.web;


import com.makbro.core.base.orgRole.service.RoleService;
import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.orgUser.service.OrgUserService;
import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统用户管理
 * Created by wujiyue on 2016-07-05 22:52:55.
 */
@Controller
@RequestMapping("/org/user")
@RequiresPermissions(value={"/org/user","orgTreeManage"},logical= Logical.OR)
public class OrgUserController extends BaseController {
    @Autowired
    protected OrgUserService userService;
    @Autowired
    protected RoleService roleService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/base/user/list";
    }

    //展示当前系统各个角色有哪些用户并且维护用户角色映射关系
    @RequestMapping("/userRole")
    public String yhgl(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        List<Map<String,Object>> roleList=roleService.getRoleListByYh(map);//获得当前登录用户可以获得的角色列表
        model.addAttribute("roleList",roleList);
        return "base/user/userRole";
    }

    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(OrgUser user, Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        model.addAttribute("sjgwid",(String) map.get("sjgwid"));
        model.addAttribute("sjgwname",(String) map.get("sjgwname"));
        return "/base/user/add_layui";
    }

    /**
     * 跳转到列表页面
     */
   /* @RequestMapping(value={"/list"})
    public String list(PageParam pageParam,Model model){
        Object users=null;
        users=userService.find(getPageBounds(pageParam), MyBatisRequestUtil.getMap(request));
        model.addAttribute("users",users);
        model.addAttribute("pageParam",pageParam);
        return "/base/user/list";
    }*/
   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(OrgUser user,Model model){
        if(user!=null&&user.getId()!=null){
            user=userService.get(user.getId());
        }
         model.addAttribute("user",user);
         return "/base/user/edit_layui";
    }

    //-----------json数据接口--------------------

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return userService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return userService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }
    //2018年5月13日11:35:02 根据角色获得用户列表
    @ResponseBody
    @RequestMapping("/json/findByRole")
    public Object findByRole() {
        return userService.findByRole(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增系统用户")
    public void add(OrgUser m) {
        userService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(OrgUser m) {
        userService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存系统用户")
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return  userService.save(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
    @ActionLog(description="逻辑删除系统用户")
	public Object remove(@PathVariable String id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            userService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
    @ActionLog(description="批量逻辑删除系统用户")
	public Object removes(@PathVariable String[] ids){
       return   userService.removes(ids);
	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除系统用户")
    public void delete(@PathVariable String id) {
        userService.delete(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除系统用户")
    public void deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
         userService.deleteBatch(ids);
    }
}
