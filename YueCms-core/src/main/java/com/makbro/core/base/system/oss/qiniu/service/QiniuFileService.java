package com.makbro.core.base.system.oss.qiniu.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.system.oss.mimeType.bean.MimeType;
import com.makbro.core.base.system.oss.qiniu.bean.QiniuFile;
import com.makbro.core.base.system.oss.qiniu.dao.QiniuFileMapper;
import com.makbro.core.base.system.oss.util.QiniuResult;
import com.makbro.core.base.system.oss.util.QiniuUtil;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import com.qiniu.storage.model.FetchRet;
import com.qiniu.storage.model.FileInfo;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 七牛文件资源 Service
 * @author  wujiyue on 2018-06-26 17:49:09.
 */
@Service
public class QiniuFileService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private QiniuFileMapper qiniuFileMapper;

     /*基础公共方法*/
    public QiniuFile get(String id){
        return qiniuFileMapper.get(id);
    }
    public List<QiniuFile> find(PageBounds pageBounds,Map<String,Object> map){
        return qiniuFileMapper.find(pageBounds,map);
    }
    public List<Map<String,Object>> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return qiniuFileMapper.findByMap(pageBounds,map);
    }
    public void add(QiniuFile qiniuFile){
        qiniuFileMapper.add(qiniuFile);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          msg.setType(Msg.MsgType.error);
          String orgid=String.valueOf(map.get("orgid"));

          QiniuFile qiniuFile= JSON.parseObject(JSON.toJSONString(map),QiniuFile.class);
          if(orgid.equals(TmConstant.NUM_ZERO)){
          }else{
              qiniuFile.setName(orgid+qiniuFile.getName());
              qiniuFile.setQiniu_key(orgid+qiniuFile.getQiniu_key());
          }
          if(qiniuFile.getId()==null||"".equals(qiniuFile.getId().toString())){
              QiniuFile checkFile = getByKey(qiniuFile.getQiniu_key());
              if(checkFile!=null){
                  msg.setContent("KEY已经存在!");
                  return msg;
              }
              String id= Guid.get();
              qiniuFile.setId(id);
              qiniuFileMapper.add(qiniuFile);
          }else{
              qiniuFileMapper.update(qiniuFile);
          }
          msg.setType(Msg.MsgType.success);
          msg.setContent("保存信息成功");
          return msg;
    }
    public void addBatch(List<QiniuFile> qiniuFiles){
        qiniuFileMapper.addBatch(qiniuFiles);
    }

    public void update(QiniuFile qiniuFile){
        qiniuFileMapper.update(qiniuFile);
    }

    public void updateByMap(Map<String,Object> map){
        qiniuFileMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        qiniuFileMapper.updateByMapBatch(map);
    }
    public boolean delete(String id){
        QiniuFile file=qiniuFileMapper.get(id);
        if(file!=null){
           boolean b= QiniuUtil.deleteFile(file.getQiniu_key());
            if(b){
                qiniuFileMapper.delete(id);
            }
           return b;
        }
        return false;
    }

    public void deleteBatch(String[] ids){
        for(String s:ids){
            delete(s);
        }
        //qiniuFileMapper.deleteBatch(ids);
    }

     /*自定义方法*/
    public QiniuFile getByKey(String key){
        return qiniuFileMapper.getByKey(key);
    }
    //上传（重新）文件到七牛空间
    public Object qiniuUpload(Map map){
        Msg res=new Msg();
        res.setType(Msg.MsgType.error);
        try{
            String ids=String.valueOf(map.get("ids"));
            String[] arr=ids.split(",");
            String errorContent="";
            int successCount=0;
            int errorCount=0;
            if(arr!=null&&arr.length>0){
                for(String id:arr){
                    QiniuFile qiniuFile=qiniuFileMapper.get(id);
                    if(qiniuFile!=null){
                        boolean thisFileFlag=false;
                        boolean thisFileExistsFlag=false;//文件存在标志
                        logger.info("==============>开始上传文件：【"+qiniuFile.getName()+"】,id:"+qiniuFile.getId());
                        String localPath = qiniuFile.getLocalpath();
                        String netPath=qiniuFile.getUrl();//图片网络路径
                        if(StringUtil.notEmpty(localPath)){
                            File localFile=new File(localPath);
                            if(localFile.exists()){
                                thisFileExistsFlag=true;
                                logger.info("==============>上传本地文件：【"+qiniuFile.getName()+"】,localPath:"+localPath);
                                //有本地文件
                                QiniuResult result = QiniuUtil.uploadByFile(localFile, qiniuFile.getQiniu_key());
                                if(result.getCode().equals("200")&&StringUtil.notEmpty(result.getKey())){
                                    //200成功
                                    FileInfo info =  QiniuUtil.getFileInfo(result.getKey());//上传成功后自动同步到本地数据库
                                    if(info!=null){
                                        qiniuFile.setStatus(1);
                                        qiniuFile.setDeleted(0);
                                        qiniuFile.setQiniu_key(result.getKey());
                                        qiniuFile.setQiniu_url(QiniuUtil.DefaultDomain+result.getKey());
                                        qiniuFile.setFsize(info.fsize);
                                        qiniuFile.setMimeType(info.mimeType);
                                        MimeType mimeType= (MimeType) EhCacheUtils.getSysInfo("mimeType",info.mimeType);
                                        if(mimeType!=null){
                                            qiniuFile.setMimeTypeName(mimeType.getName());
                                        }
                                        qiniuFile.setQiniu_hash(info.hash);
                                        qiniuFile.setPutTime(DateUtil.getDatetime());
                                        qiniuFileMapper.update(qiniuFile);
                                        thisFileFlag=true;
                                        logger.info("==============>上传本地文件：【"+qiniuFile.getName()+"】,id:"+qiniuFile.getId()+"成功!");
                                    }
                                }else{//失败情况
                                    if(result.getCode().equals("file exists")){//文件在七牛空间已经存在
                                        logger.info("==============>上传本地文件：【"+qiniuFile.getName()+"】,id:"+qiniuFile.getId()+"文件在七牛空间已经存在！");
                                    }else{
                                        logger.info("==============>上传本地文件：【"+qiniuFile.getName()+"】,id:"+qiniuFile.getId()+"失败!msg:"+result.getMsg()+",code:"+result.getCode());
                                    }
                                }
                            }else{
                                logger.warn("==============>文件：【"+qiniuFile.getName()+"】,id:"+qiniuFile.getId()+"本地路径不存在!");
                            }
                        }
                        if(!thisFileFlag){
                            //本地文件上传没成功
                            if(StringUtil.notEmpty(netPath) && QiniuUtil.checkNetUrlExists(netPath)){
                                //存在的网络文件路径
                                    thisFileExistsFlag=true;
                                    logger.info("==============>上传网络路径文件：【"+qiniuFile.getName()+"】,url:"+netPath);
                                    FetchRet result=QiniuUtil.uploadByUrl(netPath,qiniuFile.getQiniu_key());
                                    if(result!=null&&StringUtil.notEmpty(result.hash)){
                                        //成功
                                        FileInfo info =  QiniuUtil.getFileInfo(result.key);//上传成功后自动同步到本地数据库
                                        if(info!=null){
                                            qiniuFile.setStatus(1);
                                            qiniuFile.setDeleted(0);
                                            qiniuFile.setQiniu_key(result.key);
                                            qiniuFile.setQiniu_url(QiniuUtil.DefaultDomain+result.key);
                                            qiniuFile.setFsize(info.fsize);
                                            qiniuFile.setMimeType(info.mimeType);
                                            MimeType mimeType= (MimeType)EhCacheUtils.getSysInfo("mimeType",info.mimeType);
                                            if(mimeType!=null){
                                                qiniuFile.setMimeTypeName(mimeType.getName());
                                            }
                                            qiniuFile.setQiniu_hash(info.hash);
                                            qiniuFile.setPutTime(DateUtil.getDatetime());
                                            qiniuFileMapper.update(qiniuFile);
                                            thisFileFlag=true;
                                            logger.info("==============>上传本地文件：【"+qiniuFile.getName()+"】,id:"+qiniuFile.getId()+"成功!");
                                        }
                                        logger.info("==============>上传网络路径文件：【"+qiniuFile.getName()+"】成功! url:"+QiniuUtil.DefaultDomain+result.key);
                                        thisFileFlag=true;
                                    }else{
                                        logger.info("==============>上传网络路径文件：【"+qiniuFile.getName()+"】失败!");
                                    }

                            }else{
                                logger.warn("==============>文件：【"+qiniuFile.getName()+"】,id:"+qiniuFile.getId()+"网络路径文件都不存在!");
                            }
                        }
                        if(!thisFileFlag){
                            if(!thisFileExistsFlag){
                                errorContent+="文件:name【"+qiniuFile.getName()+"】,id 【"+qiniuFile.getId()+"】不存在!\r\n";
                                //更新记录的status标志位-1表示文件不存在
                                qiniuFile.setStatus(-1);
                            }else{
                                errorContent+="文件:name【"+qiniuFile.getName()+"】,id 【"+qiniuFile.getId()+"】上传失败!\r\n";
                                //更新记录的status标志为2表示文件上传过，但失败了
                                qiniuFile.setStatus(2);
                            }
                            qiniuFileMapper.update(qiniuFile);
                            errorCount++;
                        }else{
                            successCount++;
                        }

                    }
                }
                if(StringUtil.notEmpty(errorContent)){
                    res.setType(Msg.MsgType.error);
                    res.setContent(errorContent+"\r\n成功上传"+successCount+"个文件!\r\n失败"+errorCount+"个!");
                }else{
                    res.setType(Msg.MsgType.success);
                    res.setContent("成功上传"+successCount+"个文件!");
                }
            }else{
                res.setType(Msg.MsgType.error);
                res.setContent("请选择要上传的记录!");
            }
        }catch (Exception ex){
            ex.printStackTrace();
            res.setContent("向七牛空间上传文件发生异常!");
        }
        return res;
    }
    //从七牛空间同步文件
    @Transactional
    public Object downloadFromQiniu(Map map){
        Msg res=new Msg();
        res.setType(Msg.MsgType.error);
        String orgid=String.valueOf(map.get("orgid"));
        try{
        String prefix=String.valueOf(map.get("prefix"));
        if(StringUtil.isEmpty(prefix)){
            prefix=String.valueOf(map.get("name"));
        }
        //String delimiter=String.valueOf(map.get("delimiter"));
        String delimiter="";//不知道这个参数啥用
        prefix= StringUtil.isEmpty(prefix)?"":prefix;
        String  new_prefix="";
        if(orgid.equals(TmConstant.NUM_ZERO)){//orgid=0
            new_prefix= StringUtil.isEmpty(prefix)?"":prefix;
        }else{
            new_prefix= StringUtil.isEmpty(prefix)?""+orgid:""+orgid+prefix;//其它组织的七牛文件都是以该组织的ordid开头
        }

        FileInfo[] arr = QiniuUtil.getFileListOfBucket(new_prefix, delimiter);
        if(arr!=null&&arr.length>0){
            QiniuFile qiniuFile=null;
            List<QiniuFile> new_list=new ArrayList<QiniuFile>();//本地库没有，需要新插入的列表
            for(FileInfo f:arr){
                //检测数据库中是否存在的key
                QiniuFile checkFile = getByKey(f.key);
                if(checkFile!=null){
                    checkFile.setDeleted(0);
                    checkFile.setFsize(f.fsize);
                    checkFile.setMimeType(f.mimeType);
                    MimeType mimeType= (MimeType)EhCacheUtils.getSysInfo("mimeType",f.mimeType);
                    if(mimeType!=null){
                        checkFile.setMimeTypeName(mimeType.getName());
                    }
                    checkFile.setName(f.key);
                    checkFile.setQiniu_hash(f.hash);
                    checkFile.setQiniu_key(f.key);
                    checkFile.setPutTime(DateUtil.getDatetime());
                    checkFile.setQiniu_url(QiniuUtil.DefaultDomain + f.key);
                    checkFile.setStatus(1);//已经从七牛同步
                    //checkFile.setUrl(qiniuFile.getQiniu_url());
                    qiniuFileMapper.update(checkFile);
                }else{
                    qiniuFile=new QiniuFile();
                    qiniuFile.setOrgid(orgid);
                    qiniuFile.setDeleted(0);
                    qiniuFile.setFsize(f.fsize);
                    qiniuFile.setMimeType(f.mimeType);
                    MimeType mimeType= (MimeType)EhCacheUtils.getSysInfo("mimeType",f.mimeType);
                    if(mimeType!=null){
                        qiniuFile.setMimeTypeName(mimeType.getName());
                    }
                    qiniuFile.setName(f.key);
                    qiniuFile.setQiniu_hash(f.hash);
                    qiniuFile.setQiniu_key(f.key);
                    qiniuFile.setPutTime(DateUtil.getDatetime());
                    qiniuFile.setQiniu_url(QiniuUtil.DefaultDomain + f.key);
                    qiniuFile.setStatus(1);//已经从七牛同步
                    qiniuFile.setUrl(qiniuFile.getQiniu_url());
                    qiniuFile.setSuffix("");
                    qiniuFile.setResourcetype("");
                    qiniuFile.setId(Guid.get());
                    new_list.add(qiniuFile);
                }

            }
            if(CollectionUtils.isNotEmpty(new_list)){
                qiniuFileMapper.addBatch(new_list);
            }
            res.setType(Msg.MsgType.success);
            res.setContent("同步成功!成功同步"+arr.length+"条记录!");
        }else{
            res.setContent("本次同步0条记录!");
        }
        }catch (Exception ex){
            ex.printStackTrace();
            res.setContent("从七牛空间同步文件发生异常!");
        }
        return res;
    }
}
