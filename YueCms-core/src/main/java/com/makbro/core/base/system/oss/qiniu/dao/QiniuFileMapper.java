package com.makbro.core.base.system.oss.qiniu.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.system.oss.qiniu.bean.QiniuFile;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 七牛文件资源 dao
 * @author  wujiyue on 2018-06-26 17:49:08.
 */
@Repository
public interface QiniuFileMapper{

    public QiniuFile get(String id);
    public Map<String,Object> getMap(String id);
    public void add(QiniuFile qiniuFile);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<QiniuFile> qiniuFiles);
    public void update(QiniuFile qiniuFile);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）并且返回值类型不同
    public List<QiniuFile> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Map<String,Object>> findByMap(PageBounds pageBounds, Map<String, Object> map);


    public QiniuFile getByKey(String key);

}
