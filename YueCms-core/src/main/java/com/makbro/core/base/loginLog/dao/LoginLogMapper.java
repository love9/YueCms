package com.makbro.core.base.loginLog.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import com.makbro.core.base.loginLog.bean.LoginLog;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 登录日志 dao
 * @author wujiyue
 * @date 2018-11-03 00:11:20
 */
@Repository
public interface LoginLogMapper{

    public LoginLog get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(LoginLog loginLog);
    public void addByMap(Map<String, Object> map);
    public void update(LoginLog loginLog);
    public void updateByMap(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<LoginLog> find(PageBounds pageBounds, Map<String, Object> map);
    public List<LoginLog> findByMap(PageBounds pageBounds, Map<String, Object> map);




}
