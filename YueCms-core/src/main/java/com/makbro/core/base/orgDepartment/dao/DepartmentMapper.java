package com.makbro.core.base.orgDepartment.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgDepartment.bean.Department;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 部门 dao
 * Created by wujiyue on 2016-07-17 11:52:55.
 */
@Repository
public interface DepartmentMapper{
    public Department get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Department department);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Department> departments);
    public void update(Department department);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除、有效数据的条件（deleted=0,available=1）
    public List<Department> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Department> findByMap(PageBounds pageBounds, Map<String, Object> map);

	public List<Department> findByParentid(PageBounds pageBounds, Map<String, Object> map);

    public Integer findByParentidCount(@Param("parentid") String parentid, @Param("orgid") String orgid);

    public Integer findByOrgidCount(String parentid);
    //检测要删除的记录下是否有孩子
    public Integer getChildrenCount(@Param("ids") String ids);

    public String getParentidsById(@Param("id") String id);

    public int updateSort(@Param("id") String id, @Param("sort") String sort);

    public Integer getMaxSortByParentid(@Param("parentid") String parentid, @Param("orgid") String orgid);

    //获得该部门的下级部门列表
    public List<Department> findBmListByBmid(PageBounds pageBounds, Map<String, Object> map);

    //根据部门名称查看该组织是否已经存在该部门
    public Department getByNameAndParentidAndOrgid(@Param("name") String name, @Param("parentid") String parentid, @Param("orgid") String orgid);

    public List<Department> findByParentidAndOrgid(@Param("parentid") String parentid, @Param("orgid") String orgid);
}
