package com.makbro.core.base.system.web;

import com.alibaba.fastjson.JSONArray;
import com.makbro.core.base.login.bean.ChangYanUser;
import com.makbro.core.base.login.bean.ChangYanUserinfo;
import com.makbro.core.base.login.service.LoginService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.LoginBean;
import com.markbro.base.utils.cookie.CookieUtils;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import java.util.Map;

/**
 * 专门对外提供接口
 */
@Controller
@RequestMapping("/api")
public class ApiController extends BaseController {

    @Autowired
    LoginService loginService;
    /**
     * 畅言 获取当前请求用户的登录状态
     * @return
     */
    @ResponseBody
    @RequestMapping("/loginState_ChangYan")
    public void loginStateCallback(@RequestParam(value = "callback") String callback){
        try{
            String state = loginService.getLoginState(request,response);
            ChangYanUserinfo userinfo = new ChangYanUserinfo();
            Cookie[] cookies = request.getCookies();
            boolean cookieContainsUserid=isContains("user_id", cookies);//cookie是否包含userid

                if(TmConstant.NUM_ONE.equals(state)){
                    String yhid =  MyBatisRequestUtil.getYhid();
                    if(StringUtil.notEmpty(yhid)){
                        userinfo.setIs_login(1);//用户已登录
                        LoginBean loginBean=MyBatisRequestUtil.getLoginUserInfo(request);
                        if(loginBean!=null){
                            ChangYanUser user = new ChangYanUser();
                            user.setUser_id(loginBean.getYhid()); //该值具体根据自己用户系统设定
                            user.setNickname(loginBean.getXm()); //该值具体根据自己用户系统设定
                            String headerPath="";
                            Map<String,Object> userMap= loginBean.getUserMap();
                            if(userMap!=null){
                                headerPath=String.valueOf(userMap.get("headerPath"));
                            }
                            headerPath=StringUtil.notEmpty(headerPath)?headerPath: TmConstant.KEY_USER_DEFAULT_HEAD_ICON;
                            user.setImg_url(headerPath); //该值具体根据自己用户系统设定，可以为空
                            user.setProfile_url("");//该值具体根据自己用户系统设定，可以为空
                            user.setSign("*"); //签名已弃用，任意赋值即可
                            userinfo.setUser(user);
                            if(!cookieContainsUserid){
                                CookieUtils.setCookie(response,"user_id",yhid,"/");
                            }
                        }
                    }else{
                        userinfo.setIs_login(0);//用户未登录
                    }
                }else{
                    userinfo.setIs_login(0);//用户未登录
                }


            response.setContentType("application/x-javascript");//指定传输格式为js
            String outStr=callback+"("+ JSONArray.toJSONString(userinfo)+")";
            response.getWriter().write(outStr);//拼接成jsonp格式
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @RequestMapping("/logout_ChangYan")
    public void logout_ChangYan(@RequestParam(value = "callback") String callback) throws Exception{
        //清除自己网站cookies信息,同时前端logout.js代码用来清理畅言cookie
        loginService.logout(request,response);
        response.getWriter().write(callback + "({\"code\":\"1\",reload_page:0, js-src:logout.js})");
    }

    //该方法判断cookie中是否存在键值为key的value值
    public boolean isContains(String key, Cookie[] cookies){
        for(Cookie cookie : cookies){
            if(cookie.getName().equals(key)){
                if(cookie.getValue()!=null){
                    return true;
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    @ResponseBody
    @RequestMapping("/loginState")
    public Object loginState(){
        try{
            String state = loginService.getLoginState(request,response);
            return  state;
        }catch (Exception ex){
            ex.printStackTrace();
            return "-1";//异常
        }
    }
}
