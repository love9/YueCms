package com.makbro.core.base.orgUser.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgRole.dao.RoleMapper;
import com.makbro.core.base.orgTree.bean.OrgTree;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.orgUser.dao.OrgUserMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.listener.ChangeCacheListener;
import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.DbUtil;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统用户 service
 * Created by wujiyue on 2016-07-05 22:52:55.
 */
@Service
public class OrgUserService implements InitCacheListener, ChangeCacheListener {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private static final String tableName="sys_user";

    @Autowired
    private OrgUserMapper userMapper;
    @Autowired
    private OrgTreeMapper orgTreeMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private TableKeyService yhKeyService;
     /*基础公共方法*/
    public OrgUser get(String id){
        OrgUser user=(OrgUser) EhCacheUtils.getUserInfo("user",id);
        if(user!=null){
            return user;
        }
        return userMapper.get(id);
    }
    public List<OrgUser> find(PageBounds pageBounds,Map<String,Object> map){
        return userMapper.find(pageBounds,map);
    }
    //用户角色（映射）管理功能中根据一个角色查询这个角色下面的用户列表
    //如果不是管理员那么还要登录用户的部门作为查询条件筛选本部门的用户
    public List<OrgUser> findByRole(PageBounds pageBounds,Map<String,Object> map){
        String bmid=String.valueOf(map.get("bmid"));
        String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
        if("admin".equals(bmid)||"804".equals(yhid)){//李萍萍A单独授权该功能时可以查询所有代理商用户
            return userMapper.findByRole(pageBounds,map);
        }else{
            return userMapper.findByRoleAndBm(pageBounds,map);
        }
    }

    public List<OrgUser> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return userMapper.findByMap(pageBounds,map);
    }
    public void add(OrgUser user){
        userMapper.add(user);
        onAdd(user.getId());
    }
    @Transactional
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
                 try{
                     OrgUser user= JSON.parseObject(JSON.toJSONString(map), OrgUser.class);
                     if(StringUtil.isEmpty(user.getBirthday())){
                         user.setBirthday("1990-01-01");
                     }
                     String emailflag=(String)map.get("emailflag");
                     String phoneflag=(String)map.get("phoneflag");
                     if("on".equals(emailflag)||"1".equals("emailflag"))
                     {
                         user.setEmailflag(1);
                     }else{
                         user.setEmailflag(0);
                     }
                     if("on".equals(phoneflag)||"1".equals(phoneflag))
                     {
                         user.setPhoneflag(1);
                     }else{
                         user.setPhoneflag(0);
                     }
                     if(user.getId()==null||"".equals(user.getId().toString())){
                         String id= yhKeyService.getStringId();
                         user.setId(id);
                         userMapper.add(user);
                         onAdd(id);
                     }else{
                         userMapper.update(user);
                         onUpdate(user.getId());
                     }

                     String sjgwid=(String)map.get("sjgwid");
                     if(StringUtil.notEmpty(sjgwid)){
                         //org_tree
                         String orgid= (String) map.get(TmConstant.ORGID_KEY);
                         OrgTree treeTemp= orgTreeMapper.getByLxidAndType(sjgwid, "gw",orgid);
                         OrgTree tree=new OrgTree();
                         tree.setOrgid(orgid);
                         tree.setId(Guid.get());
                         tree.setParentid(treeTemp.getId());
                         tree.setType("ry");
                         tree.setSjbmid(treeTemp.getSjbmid());
                         tree.setLxid(user.getId());
                         tree.setName(user.getAccount());
                         orgTreeMapper.add(tree);
                     }
                     String jsid=(String)map.get("jsid");//角色id
                     if(StringUtil.notEmpty(jsid)){
                         if(jsid.contains(",")){
                             String[] jsArr=jsid.split(",");
                             for(String js:jsArr){
                                 roleMapper.addUserRole(user.getId(),js);
                             }
                         }else{
                             roleMapper.addUserRole(user.getId(),jsid);
                         }
                     }
                     msg.setType(Msg.MsgType.success);
                     msg.setContent("保存信息成功");

                 }catch (Exception ex){
                     ex.printStackTrace();
                     msg.setType(Msg.MsgType.error);
                     msg.setContent("保存信息失败");
                 }
                return msg;
    }
    public void addBatch(List<OrgUser> users){
        userMapper.addBatch(users);
    }
    public void update(OrgUser user){
        userMapper.update(user);
    }
    public void updateByMap(Map<String,Object> map){
        userMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        userMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        userMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        userMapper.deleteBatch(ids);
    }
     /*自定义方法*/
    @Transactional
     public Object removes(String[] ids){
         Msg msg=new Msg();
         try{
             for(int i=0;i<ids.length;i++)
             {
                 OrgUser user=this.get(ids[i]);
                 orgTreeMapper.deleteByLxidAndType(user.getId(),"ry","");
             }
             Map<String,Object> map=new HashMap<String,Object>();
             map.put("deleted",1);
             map.put("ids",ids);
             this.updateByMapBatch(map);
             msg.setType(Msg.MsgType.success);
             msg.setContent("批量删除成功！");
         }catch (Exception e){
             msg.setType(Msg.MsgType.error);
             msg.setContent("批量删除失败！");
         }
         return msg;
     }

    //根据yhid获得用户头像
    public String getHeaderPathByYhid(String yhid){
        String path=userMapper.getHeaderPathByYhid(yhid);
        if(StringUtil.isEmpty(path)){
            path= TmConstant.KEY_USER_DEFAULT_HEAD_ICON;//没有设置头像就给个默认头像
        }
        return path;
    }

    /**
     * 根据yhids获得yhmcs(多个yhid用户逗号分隔)
     * @param yhids
     * @return
     */
    public String getYhmcsByYhids(String yhids){
        if(StringUtil.isEmpty(yhids)){
            return "";
        }
        StringBuffer sb=new StringBuffer();
        if(yhids.contains(",")){
            String[] arr=yhids.split(",");
            for(String yhid:arr){
              OrgUser user=  get(yhid);
                if(user!=null){
                    sb.append(user.getNickname()).append(",");
                }
            }
        }else{
            OrgUser user=  get(yhids);
            if(user!=null){
                sb.append(user.getNickname()).append(",");
            }
        }
        String res=sb.toString();
        res=StringUtil.subEndStr(res,",");
        return res;
    }
    public OrgUser getByOpenid(String openid){
        return userMapper.getByOpenid(openid);
    }

    @Override
    public void onInit() {
        int pageSize=1000;
        String countSql="select count(1) from "+tableName;
        int count= DbUtil.getJtN().queryForObject(countSql,Integer.class);
        int pages=(count+pageSize-1)/pageSize;
        logger.info("缓存表["+tableName+"]的信息：总记录数：{}，每页：{},总页数：{}",count,pageSize,pages);
        for(int i=1;i<=pages;i++){
            logger.info("缓存表["+tableName+"]的信息：第【"+i+"】页.");
            List<OrgUser> users=userMapper.find(new PageBounds(i,pageSize),null);
            if(CollectionUtils.isNotEmpty(users)){
                for(OrgUser user:users){
                    EhCacheUtils.putUserInfo("user",user.getId(),user);
                }
            }
        }
    }

    @Override
    public void onAdd(Object id) {
        OrgUser user=get((String)id);
        if(user!=null){
            EhCacheUtils.putUserInfo("user",user.getId(),user);
        }
    }

    @Override
    public void onUpdate(Object id) {
        OrgUser user=get((String)id);
        if(user!=null){
            EhCacheUtils.putUserInfo("user",user.getId(),user);
        }
    }

    @Override
    public void onDelete(Object id) {
        EhCacheUtils.removeUserInfo("user",(String)id);
    }


}
