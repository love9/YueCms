package com.makbro.core.base.system.service;

import com.markbro.base.exception.ApplicationException;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.sso.core.util.JedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JedisUtilCacheService {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public Object removeCacheByKey(String key){
        if(JedisUtil.exists(key)){
            del(key);
            return Msg.success("清除缓存成功!");
        }
        return Msg.error("不存在key为["+key+"]的缓存!");
    }
    public Map<String,Object> viewCacheInfoByKey(String key){
        Map<String,Object> res=new HashMap();
        if(JedisUtil.exists(key)){
            res.put("key",key);
            String value=JedisUtil.getStringValue(key);
            res.put("value",value);
        }
        return res;
    }
    /**
     * 获取缓存分页信息
     * @param map
     * @return
     */
    public Object getCacheInfo(Map<String, Object> map) {
        try {
            String keyContent = String.valueOf(map.get("keyContent"));

            String page_str = (String) map.get("page");//当前页
            String limit_str = (String) map.get("limit");//每页几条数据
            page_str = StringUtil.assertNotNullOrEmpty(page_str, "1");
            limit_str = StringUtil.assertNotNullOrEmpty(limit_str, "10");
            int page = Integer.parseInt(page_str);
            int limit = Integer.parseInt(limit_str);
            int start = page - 1;
            if (page == 1 || page == 0) {
                start = 0;
            }
            if (StringUtil.isEmpty(keyContent)) {
                keyContent = "*";
            } else {
                keyContent = "*" + keyContent + "*";
            }
            List<String> keysList = this.keys(keyContent);
            int keysCount = keysList.size();
            List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
            Map<String, Object> temp = null;
            for (int i = start * limit; i < keysCount && i < (start + 1) * limit; i++) {
                temp = new HashMap<>();
                String key = keysList.get(i);
                String type = JedisUtil.type(key);
                long ttl = JedisUtil.ttl(key);
                temp.put("key", key);
                temp.put("type", type);
                temp.put("ttl", ttl);
                mapList.add(temp);
            }
            Map<String, Object> m = new HashMap<String, Object>();
            m.put("total", keysCount);
            m.put("rows", mapList);
            return m;
        }catch (Exception ex){
            ex.printStackTrace();
            return Msg.error("获取缓存分页信息失败!");
        }
    }
    public String setStringValue(String key, String value) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.setStringValue(key,value);
    }
    public String setStringValue(String key, String value, int seconds) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.setStringValue(key,value,seconds);
    }

    public String getStringValue(String key) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.getStringValue(key);
    }
    public Object getObjectValue(String key) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.getObjectValue(key);
    }

    public Object getObjectValue(String key,Class cls) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.getObjectValue(key,cls);
    }

    public Long del(String key) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.del(key);
    }

    public boolean exists(String key) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.exists(key);
    }

    public long expire(String key, int seconds) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.expire(key,seconds);
    }

    public long expireAt(String key, long unixTime) {
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.expireAt(key,unixTime);
    }

    public List<String> keys(String pattern){
        if(!JedisUtil.initFlag){
            throw new ApplicationException("ShardedJedisPool未初始化!");
        }
        return JedisUtil.keys(pattern);
    }


}
