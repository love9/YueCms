package com.makbro.core.base.orgPosition.web;

import com.makbro.core.base.orgPosition.bean.Position;
import com.makbro.core.base.orgPosition.service.PositionService;
import com.makbro.core.base.orgTree.service.OrgTreeService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 岗位管理管理
 * Created by wujiyue on 2017-02-08 23:04:11.
 */

@Controller
@RequestMapping("/org/position")
@RequiresPermissions(value={"/org/position","orgTreeManage"},logical= Logical.OR)
public class PositionController extends BaseController {
    @Autowired
    protected PositionService positionService;
	@Autowired
	private OrgTreeService orgTreeService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/base/position/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Position position, Model model){
		Map map= MyBatisRequestUtil.getMap(request);
		model.addAttribute("parentid",(String) map.get("parentid"));
		model.addAttribute("parentname",(String) map.get("parentname"));
        return "/base/position/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Position position,Model model){
        if(position!=null&&position.getId()!=null){
            position=positionService.get(position.getId());
        }
         model.addAttribute("position",position);
         return "/base/position/edit";
    }
    //-----------json数据接口--------------------

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return positionService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return positionService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }


    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(Position m) {
        
        positionService.add(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Position m) {
        positionService.update(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
		Map map=MyBatisRequestUtil.getMap(request);
        return positionService.save(map);
    }

    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            int count=positionService.getChildrenCount(id);
            if(count>0){
                return Msg.error("删除的记录下不能有子记录！");
            }
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            positionService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }

	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){
		return positionService.removes(ids);
	}


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable String id) {
    	try{
			int count=positionService.getChildrenCount(id);
			if(count>0){
                return Msg.error("删除的记录下不能有子记录！");
			}
            positionService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
			String ids_str= StringUtil.arrToString(ids, ",");
			int count=positionService.getChildrenCount(ids_str);
			if(count>0){
                return Msg.error("删除的记录下不能有子记录！");
			}
            positionService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
    
	@ResponseBody
	@RequestMapping("/json/findByParentid/{parentid}")
	public Object findByParentid(@PathVariable String parentid) {
		Map<String, Object> map = MyBatisRequestUtil.getMap(request);
		map.put("parentid",parentid);
		return positionService.findByParentid(getPageBounds(),map);
	}

	//easyui 动态树
	@ResponseBody
	@RequestMapping("/json/tree")
	public Object tree() {
		Map<String, Object> map = MyBatisRequestUtil.getMap(request);
		return positionService.tree(map);
	}

    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return positionService.saveSort(MyBatisRequestUtil.getMap(request));
    }

}