package com.makbro.core.base.permission.web;


import com.makbro.core.base.permission.bean.Permission;
import com.makbro.core.base.permission.service.PermissionService;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 菜单权限管理
 * @author wujiyue
 */
@Controller
@RequestMapping("/base/permission")
public class PermissionController extends BaseController {
    @Autowired
    protected PermissionService permissionService;
    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/base/permission/manager";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String parentid= (String) map.get("parentid");
        String parentname= (String) map.get("parentname");
        model.addAttribute("parentid",parentid);
        model.addAttribute("parentname",parentname);
        return "/base/permission/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String id= (String) map.get("id");
        model.addAttribute("permission",permissionService.getMap(id));
         return "/base/permission/edit";
    }

    //-----------json数据接口--------------------
    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return permissionService.saveSort(MyBatisRequestUtil.getMap(request));
    }
	@ResponseBody
	@RequestMapping("/json/findByParentid/{parentid}")
	public Object findByParentid(@PathVariable String parentid) {
        Map map=MyBatisRequestUtil.getMap(request);
        map.put("parentid",parentid);
		return permissionService.findByParentid(getPageBounds(),map);
	}

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return permissionService.getMap(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        Map map=MyBatisRequestUtil.getMap(request);
        return permissionService.find(getPageBounds(),map);
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存菜单权限")
    public Object save(Permission m) {
        Map map=MyBatisRequestUtil.getMap(request);
        return  permissionService.save(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){

        try{
            int count=permissionService.getChildrenCount(id);
            if(count>0){
                return Msg.error("删除的菜单下不能有子菜单！");
            }else{
                Map<String,Object> map=new HashMap<String,Object>();
                map.put("deleted",1);
                map.put("id",id);
                permissionService.updateByMap(map);
                return Msg.success("删除成功！");
            }

        }catch (Exception e){
            return Msg.error("删除失败！");
        }

	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){
        try{

            String ids_str= StringUtil.arrToString(ids,",");
            int count=permissionService.getChildrenCount(ids_str);
            if(count>0){
                return Msg.error("删除的菜单下不能有子菜单！");
            }else{
                Map<String,Object> map=new HashMap<String,Object>();
                map.put("deleted",1);
                map.put("ids",ids);
                permissionService.updateByMapBatch(map);
                return Msg.success("批量删除成功！");
            }

        }catch (Exception e){
            return Msg.error("批量删除失败！");
        }

	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除菜单权限")
    public void delete(@PathVariable String id) {
        permissionService.delete(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除菜单权限")
    public void deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
         permissionService.deleteBatch(ids);
    }


    //easyui动态树
    @ResponseBody
      @RequestMapping("/json/tree")
      public Object tree() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return permissionService.tree(map);
    }
    //easyui静态树
    @ResponseBody
    @RequestMapping("/json/staticTree")
    public Object staticTree() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return permissionService.staticTree(map);
    }


    //保存角色授权，传入jsid和权限id字符串用逗号分割
    @ResponseBody
    @RequestMapping("/json/saveRolePermissions")
    public Object saveRolePermissions() {
        Map map=MyBatisRequestUtil.getMap(request);
        return permissionService.saveRolePermissions(map);
    }

    //后台登录查询用户菜单
    @ResponseBody
    @RequestMapping("/json/getUserMenus")
    public Object getUserMenus() {
        Map map=MyBatisRequestUtil.getMap(request);
        String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("qxlist", permissionService.getUserMenus(yhid));
        return m;
    }

    //更新菜单的上级
    @ResponseBody
    @RequestMapping("/json/modifySjid")
    public Object modifySjid() {
        Map map=MyBatisRequestUtil.getMap(request);
        return permissionService.modifySjid(map);
    }
}
