package com.makbro.core.base.loginLog.bean;


import com.markbro.base.model.AliasModel;

/**
 * 登录日志 bean
 * @author wujiyue
 * @date 2018-11-03 00:11:19
 */
public class LoginLog  implements AliasModel {

	private Integer id;//
	private String yhid;//登录用户ID
	private String account;//登录账户
	private String ip;//
	private String location;//登录地点
	private String deviceType;//设备类型。PC,手机，平板
	private String explore;//浏览器
	private String os;//操作系统
	private String result;//登录结果
	private String msg;//
	private String createTime;//操作时间
	private String params;//参数

	public static LoginLog build() {
		return new LoginLog();
	}

	public String getDeviceType() {
		return deviceType;
	}

	public LoginLog setDeviceType(String deviceType) {
		this.deviceType = deviceType; return this;
	}

	public Integer getId(){ return id ;}
	public LoginLog  setId(Integer id){
		this.id=id;
		return this;
	}
	public String getYhid(){ return yhid ;}
	public LoginLog  setYhid(String yhid){this.yhid=yhid; return this;}
	public String getAccount(){ return account ;}
	public LoginLog  setAccount(String account){this.account=account;return this; }
	public String getIp(){ return ip ;}
	public LoginLog  setIp(String ip){this.ip=ip; return this;}
	public String getLocation(){ return location ;}
	public LoginLog  setLocation(String location){this.location=location; return this;}
	public String getExplore(){ return explore ;}
	public LoginLog  setExplore(String explore){this.explore=explore; return this;}
	public String getOs(){ return os ;}
	public LoginLog  setOs(String os){this.os=os;return this; }
	public String getResult(){ return result ;}
	public LoginLog  setResult(String result){this.result=result; return this;}
	public String getMsg(){ return msg ;}
	public LoginLog  setMsg(String msg){this.msg=msg; return this;}
	public String getCreateTime(){ return createTime ;}
	public LoginLog  setCreateTime(String createTime){this.createTime=createTime; return this;}
	public String getParams(){ return params ;}
	public LoginLog  setParams(String params){this.params=params; return this;}


	@Override
	public String toString() {
	return "LoginLog{" +
			"id=" + id+
			", yhid=" + yhid+
			", account=" + account+
			", ip=" + ip+
			", location=" + location+
			", explore=" + explore+
			", os=" + os+
			", result=" + result+
			", msg=" + msg+
			", createTime=" + createTime+
			", params=" + params+
			 '}';
	}
}
