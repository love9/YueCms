package com.makbro.core.base.system.dao;

import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * 站点信息相关
 */
@Repository
public interface WebsiteMapper {
    public Map getSiteInfo();
}
