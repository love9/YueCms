package com.makbro.core.base.clientinfo.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.clientinfo.bean.Clientinfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 客户端信息 dao
 * @author  wujiyue on 2018-09-01 14:22:00.
 */
@Repository
public interface ClientinfoMapper{

    public Clientinfo get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(Clientinfo clientinfo);
    public void addByMap(Map<String, Object> map);
    public void update(Clientinfo clientinfo);
    public void updateByMap(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Clientinfo> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Clientinfo> findByMap(PageBounds pageBounds, Map<String, Object> map);




}
