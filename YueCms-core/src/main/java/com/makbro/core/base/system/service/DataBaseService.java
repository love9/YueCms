package com.makbro.core.base.system.service;


import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.utils.DbUtil;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 数据库相关
 */
@Service
public class DataBaseService implements InitCacheListener {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public String getCatalog(){
        String  db_catalog=(String) EhCacheUtils.getSysInfo("db_catalog");
        if(StringUtil.isEmpty(db_catalog)){
            db_catalog= DbUtil.getCatalog();
            EhCacheUtils.putSysInfo("db_catalog",db_catalog);
        }
        return db_catalog;
    }
    public String getSchema(){
        String  db_schema=(String)EhCacheUtils.getSysInfo("db_schema");//mysql获取 schema总是返回空字符串，因为它异常了
        if(StringUtil.isEmpty(db_schema)){
            db_schema=DbUtil.getSchema();
            EhCacheUtils.putSysInfo("db_schema",db_schema);
        }
        return db_schema;
    }
    public List<String> getTables(){
        List<String> db_tables=(List<String>)EhCacheUtils.getSysInfo("db_tables");
        if(CollectionUtils.isEmpty(db_tables)){
            db_tables=DbUtil.getTables();
            EhCacheUtils.putSysInfo("db_tables",db_tables);
        }
        return db_tables;
    }
    public Map getDBInfo(){
        Map map=(Map)EhCacheUtils.getSysInfo("db_dbinfo");
        if(map==null){
            map=DbUtil.getDBInfo();
            EhCacheUtils.putSysInfo("db_dbinfo",map);
        }
        return map;
    }


    @Override
    public void onInit() {
        String catalog=getCatalog();
        EhCacheUtils.putSysInfo("db_catalog",catalog);

        String schema=getSchema();
        EhCacheUtils.putSysInfo("db_schema",schema);


        List<String> db_tables=getTables();
        EhCacheUtils.putSysInfo("db_tables",db_tables);
        //System.out.println("DataBaseService==>getTables:"+db_tables.size());
        //TestUtil.printList(db_tables);
        Map map=getDBInfo();
        EhCacheUtils.putSysInfo("db_dbinfo",map);
        //System.out.println("DataBaseService==>getDBInfo:");
        //TestUtil.printMap(map);
    }

}
