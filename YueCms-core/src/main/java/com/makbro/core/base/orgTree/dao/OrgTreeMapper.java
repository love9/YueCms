package com.makbro.core.base.orgTree.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgTree.bean.OrgTree;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 组织目录 dao
 * Created by wujiyue on 2016-07-14 12:01:01.
 */
@Repository
public interface OrgTreeMapper {
    public OrgTree get(String id);
    public Map<String,Object> getMap(String id);
    public void add(OrgTree tree);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<OrgTree> trees);
    public void update(OrgTree tree);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除、有效数据的条件（deleted=0,available=1）
    public List<OrgTree> find(PageBounds pageBounds, Map<String, Object> map);
    public List<OrgTree> findByMap(PageBounds pageBounds, Map<String, Object> map);

	public List<OrgTree> findByOrgid(PageBounds pageBounds, String orgid);
    public List<OrgTree> findByParentid(PageBounds pageBounds, String parentid);
	public List<Map<String,Object>> findZtreeData(PageBounds pageBounds, Map<String, Object> map);
    public List<Map<String,Object>> getYhByYhids(@Param("ids") String ids);

    public Integer findByParentidCount(@Param("parentid") String parentid, @Param("orgid") String orgid);

    public OrgTree getByLxidAndType(@Param("lxid") String lxid, @Param("type") String type, @Param("orgid") String orgid);

    public void deleteByLxidAndType(@Param("lxid") String lxid, @Param("type") String type, @Param("orgid") String orgid);

    public int updateTreeSort(@Param("lxid") String lxid, @Param("sort") String sort, @Param("type") String type);

    public List<OrgTree> findBmYhByYhid(String yhid, String orgid);

    //获得上级部门id和名称
    public Map<String,Object> getSjbmidAndMc(@Param("bmid") String bmid);
    //获得一个组织根节点（orgid和parentid=0）
    public OrgTree getOrgRootTree(@Param("orgid") String orgid);
}
