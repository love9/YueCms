package com.makbro.core.base.login.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by wujiyue on 2016-03-13 03:24:07.
 */
@Repository
public interface LoginMapper {

   public Map<String,Object> queryYhidByDlmc(@Param("dlmc") String dlmc);
    //查询登录用户的组织信息
    public Map<String,Object> queryOrgMapByYhid(@Param("yhid") String yhid);

    //查询登录用户的角色信息
    public List<Map<String,String>> queryLoginJsList(@Param("orgid") String orgid, @Param("yhid") String yhid);
    //查询登录用户的岗位信息
    public List<Map<String,String>> queryLoginGwList(@Param("orgid") String orgid, @Param("yhid") String yhid);
    //查询登录用户的部门信息
    public List<Map<String,String>> queryLoginBmList(@Param("orgid") String orgid, @Param("yhid") String yhid);
    //登录用户的部门岗位信息（bm_mc,gw_mc,bmid,gwid）
    public List<Map<String, Object>> queryLoginOrgInfo(@Param("yhid") String yhid, @Param("orgid") String orgid);

    //查询上次登录时间和登录IP
    public List<Map<String,Object>> queryLastLogin(@Param("yhid") String yhid);
    //登录计数、保存登录时间和登录IP之前
    public void loginCount(@Param("yhid") String yhid, @Param("lastLoginIp") String loginip);
    //修改密码
    public void changePwd(@Param("yhid") String yhid, @Param("newpwd") String newpwd);

   //插入短信历史表
   public void addSmsHis(Map<String, Object> map);
}
