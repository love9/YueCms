package com.makbro.core.base.orgTree.service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgDepartment.bean.Department;
import com.makbro.core.base.orgDepartment.dao.DepartmentMapper;
import com.makbro.core.base.orgPosition.bean.Position;
import com.makbro.core.base.orgPosition.dao.PositionMapper;
import com.makbro.core.base.orgTree.bean.OrgTree;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.orgUser.dao.OrgUserMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 组织目录 service
 * Created by wujiyue on 2016-07-14 12:01:01.
 */
@Service
public class OrgTreeService {
    @Autowired
    private OrgTreeMapper treeMapper;
    @Autowired
    private PositionMapper positionMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private OrgUserMapper orgUserMapper;
    @Autowired
    private TableKeyService keyService;
     /*基础公共方法*/
    public OrgTree get(String id){
        return treeMapper.get(id);
    }
    public Map<String,Object> getMap(String id){
        return treeMapper.getMap(id);
    }
    public List<OrgTree> find(PageBounds pageBounds,Map<String,Object> map){
        return treeMapper.find(pageBounds,map);
    }
    public List<OrgTree> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return treeMapper.findByMap(pageBounds,map);
    }
    public void add(OrgTree tree){
        treeMapper.add(tree);
    }
    public Object save(OrgTree tree){
          Msg msg=new Msg();
                 try{
                     if(tree.getId()==null||"".equals(tree.getId().toString())){
                         String id= keyService.getStringId();
                         tree.setId(id);
                         treeMapper.add(tree);
                     }else{
                         treeMapper.update(tree);
                     }
                     msg.setType(Msg.MsgType.success);
                     msg.setContent("保存信息成功");
                 }catch (Exception ex){
                     msg.setType(Msg.MsgType.error);
                     msg.setContent("保存信息失败");
                 }
                return msg;
    }
    public void addBatch(List<OrgTree> trees){
        treeMapper.addBatch(trees);
    }
    public void update(OrgTree tree){
        treeMapper.update(tree);
    }
    public void updateByMap(Map<String,Object> map){
        treeMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        treeMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        treeMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        treeMapper.deleteBatch(ids);
    }
     /*自定义方法*/

	 public List<OrgTree> findByOrgid(PageBounds pageBounds, String orgid){
		return treeMapper.findByOrgid(pageBounds,orgid);
	}
	 public List<OrgTree> findByParentid(PageBounds pageBounds, String parentid){
		return treeMapper.findByParentid(pageBounds,parentid);
	}
    public  Map<String,Object> getZtreeRoot(Map<String, Object> map){
        String orgid= (String) map.get("orgid");

        //String parentid= (String) map.get("parentid");
        String parentid="0";
        Map<String,Object> tmap=new HashMap<String,Object>();
        tmap.put("parentid",parentid);
        tmap.put("orgid",orgid);
        List<Map<String,Object>> list=treeMapper.findZtreeData(new PageBounds(), tmap);
        if(list!=null&&list.size()>0){
            return  list.get(0);
        }else{
            return null;
        }
    }
    public List<Map<String,Object>> ztree(Map<String,Object> map){
        List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
        String orgid= (String) map.get("orgid");

        String treeType=(String) map.get("treeType");
        String parentid= (String) map.get("parentid");
        if(StringUtil.isEmpty(parentid)){
            parentid="0";
        }
        Map<String,Object> tmap=new HashMap<String,Object>();
        tmap.put("parentid",parentid);
        tmap.put("orgid",orgid);
        List<Map<String,Object>> list=treeMapper.findZtreeData(new PageBounds(), tmap);

       // Map<String,Object> tmap=null;
        String id=null;
        int n=0;
        for(Map<String,Object> t:list){
           // tmap=new HashMap<String,Object>();
           // tmap= MyBatisRequestUtil.beanConvert2Map(t);
            id=String.valueOf(t.get("id"));
            n=treeMapper.findByParentidCount(id,orgid);
            if(n>0){
                t.put("isParent",true);
            }
            String type= (String) t.get("type");
            /*if("zz".equals(type)){
                t.put("iconSkin","zz");
            }
            if("bm".equals(type)){
                t.put("iconSkin","bm");
            }
            if("gw".equals(type)){
                t.put("iconSkin","gw");
            }
            if("ry".equals(type)){
                t.put("iconSkin","ry");
            }*/

            if("bm".equals(treeType)){
                if("bm".equals(type)){
                    result.add(t);
                }
            }else if("gw".equals(treeType)){
                if ("bm".equals(type)||"gw".equals(type) ){
                    result.add(t);
                }
            }else{
                result.add(t);
            }
        }
        return result;
    }

    //获得组织目录数据
    public List<Map<String,Object>> zTreeStatic(Map<String,Object> map){

        String orgid= (String) map.get("orgid");
        String treeType=(String) map.get("treeType");//bm gw ry
        Map<String,Object> tmap=new HashMap<String,Object>();
        tmap.put("orgid",orgid);
        List<Map<String,Object>> list=treeMapper.findZtreeData(new PageBounds(), tmap);
        for(Map<String,Object> t:list){
            String type= (String) t.get("type");
            if("zz".equals(type)){
                t.put("iconSkin","zz");
            }
            if("bm".equals(type)){
                t.put("iconSkin","bm");
            }
            if("gw".equals(type)){
                t.put("iconSkin","gw");
            }
            if("ry".equals(type)){
                t.put("iconSkin","ry");
            }
        }

        if("bm".equals(treeType)){
            //过滤掉岗位和人员数据
            list=list.stream().filter(a->{
                return !("gw".equals(a.get("type"))||"ry".equals(a.get("type")));
            }).collect(Collectors.toList());
        }else if("gw".equals(treeType)){
            //过滤人员数据
            list=list.stream().filter(a->{
                return !"ry".equals(a.get("type"));
            }).collect(Collectors.toList());
        }

        return list;
    }
    public void deleteByLxidAndType(String lxid,String type){
        treeMapper.deleteByLxidAndType(lxid,type,"");
    }
    /**
     * 获得该部门下岗位list
     * @param pageBounds
     * @param map
     * @return
     */
    public List<Position>  findGwListByBmid(PageBounds pageBounds, Map<String,Object> map){
        return  positionMapper.findGwListByBmid(pageBounds,map);
    }
    public List<Department>  findBmListByBmid(PageBounds pageBounds, Map<String,Object> map){
        return  departmentMapper.findBmListByBmid(pageBounds, map);
    }
    public List<OrgUser>  findRyListByGwid(PageBounds pageBounds, Map<String,Object> map){
        return  orgUserMapper.findRyListByGwid(pageBounds, map);
    }

}
