package com.makbro.core.base.orgOrganization.bean;


import com.markbro.base.model.AliasModel;

/**
 * 组织机构 bean
 * @author  wujiyue on 2016-07-18 22:52:34.
 */
public class Organization  implements AliasModel {
	/**
	 * 主键。机构、组织id
	 */
	private String id;
	/**
	 * 机构名称
	 */
	private String name;
	/**
	 * 机构类型id
	 */
	private String orgtypeid;
	/**
	 * 机构类型名称
	 */
	private String orgtypename;
	/**
	 * 电话
	 */
	private String phone;
	/**
	 * 传真
	 */
	private String fax;
	/**
	 * 机构描述
	 */
	private String description;
	/**
	 * 创建类型
	 */
	private String createtype;
	/**
	 * 创建时间
	 */
	private String createTime;
	private String updateTime;
	private String createBy;
	private String updateBy;
	private Integer available;
	private Integer deleted;

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getOrgtypeid(){ return orgtypeid ;}
	public void  setOrgtypeid(String orgtypeid){this.orgtypeid=orgtypeid; }
	public String getOrgtypename(){ return orgtypename ;}
	public void  setOrgtypename(String orgtypename){this.orgtypename=orgtypename; }
	public String getPhone(){ return phone ;}
	public void  setPhone(String phone){this.phone=phone; }
	public String getFax(){ return fax ;}
	public void  setFax(String fax){this.fax=fax; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public String getCreatetype(){ return createtype ;}
	public void  setCreatetype(String createtype){this.createtype=createtype; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Organization{" +
			"id=" + id+
			", name=" + name+
			", orgtypeid=" + orgtypeid+
			", orgtypename=" + orgtypename+
			", phone=" + phone+
			", fax=" + fax+
			", description=" + description+
			", createtype=" + createtype+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", createBy=" + createBy+
			", updateBy=" + updateBy+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
