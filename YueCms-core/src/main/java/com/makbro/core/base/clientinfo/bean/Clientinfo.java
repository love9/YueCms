package com.makbro.core.base.clientinfo.bean;

import com.markbro.base.model.AliasModel;

/**
 * 客户端信息 bean
 * @author  wujiyue on 2018-09-01 14:22:00 .
 */
public class Clientinfo  implements AliasModel {

	private Integer id;//主键
	private Integer yhid;//用户id
	private String screen;//屏幕分辨率
	private String browser;//浏览器
	private String colorDepth;//位深度
	private String lang;//语言
	private String charset;//字符集
	private String javaAppletEnabled;//
	private String cookieEnabled;//是否启用cookie
	private String referer;//referer
	private String os;//操作系统
	private String title;//title
	private String domain;//domain
	private String lastModified;//
	private String currentUrl;//
	private String uri;//
	private String host;//
	private String deviceType;//
	private String timeZone;//时区
	private String ip;//ip地址
	private String location;//用户所在地
	private String createTime;//


	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public Integer getYhid(){ return yhid ;}
	public void  setYhid(Integer yhid){this.yhid=yhid; }
	public String getScreen(){ return screen ;}
	public void  setScreen(String screen){this.screen=screen; }
	public String getBrowser(){ return browser ;}
	public void  setBrowser(String browser){this.browser=browser; }
	public String getColorDepth(){ return colorDepth ;}
	public void  setColorDepth(String colorDepth){this.colorDepth=colorDepth; }
	public String getLang(){ return lang ;}
	public void  setLang(String lang){this.lang=lang; }
	public String getCharset(){ return charset ;}
	public void  setCharset(String charset){this.charset=charset; }
	public String getJavaAppletEnabled(){ return javaAppletEnabled ;}
	public void  setJavaAppletEnabled(String javaAppletEnabled){this.javaAppletEnabled=javaAppletEnabled; }
	public String getCookieEnabled(){ return cookieEnabled ;}
	public void  setCookieEnabled(String cookieEnabled){this.cookieEnabled=cookieEnabled; }
	public String getReferer(){ return referer ;}
	public void  setReferer(String referer){this.referer=referer; }
	public String getOs(){ return os ;}
	public void  setOs(String os){this.os=os; }
	public String getTitle(){ return title ;}
	public void  setTitle(String title){this.title=title; }
	public String getDomain(){ return domain ;}
	public void  setDomain(String domain){this.domain=domain; }
	public String getLastModified(){ return lastModified ;}
	public void  setLastModified(String lastModified){this.lastModified=lastModified; }
	public String getCurrentUrl(){ return currentUrl ;}
	public void  setCurrentUrl(String currentUrl){this.currentUrl=currentUrl; }
	public String getUri(){ return uri ;}
	public void  setUri(String uri){this.uri=uri; }
	public String getHost(){ return host ;}
	public void  setHost(String host){this.host=host; }
	public String getDeviceType(){ return deviceType ;}
	public void  setDeviceType(String deviceType){this.deviceType=deviceType; }
	public String getTimeZone(){ return timeZone ;}
	public void  setTimeZone(String timeZone){this.timeZone=timeZone; }
	public String getIp(){ return ip ;}
	public void  setIp(String ip){this.ip=ip; }
	public String getLocation(){ return location ;}
	public void  setLocation(String location){this.location=location; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }


	@Override
	public String toString() {
	return "Clientinfo{" +
			"id=" + id+
			", yhid=" + yhid+
			", screen=" + screen+
			", browser=" + browser+
			", colorDepth=" + colorDepth+
			", lang=" + lang+
			", charset=" + charset+
			", javaAppletEnabled=" + javaAppletEnabled+
			", cookieEnabled=" + cookieEnabled+
			", referer=" + referer+
			", os=" + os+
			", title=" + title+
			", domain=" + domain+
			", lastModified=" + lastModified+
			", currentUrl=" + currentUrl+
			", uri=" + uri+
			", host=" + host+
			", deviceType=" + deviceType+
			", timeZone=" + timeZone+
			", ip=" + ip+
			", location=" + location+
			", createTime=" + createTime+
			 '}';
	}
}
