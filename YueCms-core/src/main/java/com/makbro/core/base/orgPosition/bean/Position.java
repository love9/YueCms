package com.makbro.core.base.orgPosition.bean;


import com.markbro.base.model.AliasModel;

/**
 * 岗位管理 bean
 * @author wujiyue
 */
public class Position  implements AliasModel {

	private String orgid;//机构组织id
	private String id;//主键
	private String parentid;//上级id
	private String parentids;//parentids
	private String name;//岗位名称
	private String description;//岗位描述
	private Integer sort;//排序
	private String createTime;//创建时间
	private String updateTime;//更新时间
	private String createBy;//创建人
	private String updateBy;//更新人
	private Integer available;//可用状态
	private Integer deleted;//删除标志


	public String getOrgid(){ return orgid ;}
	public void  setOrgid(String orgid){this.orgid=orgid; }
	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getParentid(){ return parentid ;}
	public void  setParentid(String parentid){this.parentid=parentid; }
	public String getParentids(){ return parentids ;}
	public void  setParentids(String parentids){this.parentids=parentids; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }


	@Override
	public String toString() {
	return "Position{" +
			"orgid=" + orgid+
			", id=" + id+
			", parentid=" + parentid+
			", parentids=" + parentids+
			", name=" + name+
			", description=" + description+
			", sort=" + sort+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", createBy=" + createBy+
			", updateBy=" + updateBy+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
