package com.makbro.core.base.system.web;


import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.authz.annotation.RequiresAnonymous;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.makbro.core.framework.authz.annotation.RequiresRoles;
import com.makbro.core.framework.listener.MySessionListener;
import com.makbro.core.framework.mysession.bean.MySession;
import com.makbro.core.framework.mysession.dao.MySessionMapper;
import com.markbro.base.annotation.DataScope;
import com.markbro.base.common.pdfutil.PdfUtil;
import com.makbro.core.framework.BaseController;
import com.markbro.base.common.util.ExcelUtil;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.LoginBean;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.*;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Controller
public class SysController extends BaseController {

    @Autowired
    TableKeyService bmKeyService;
    @Autowired
    TableKeyService gwKeyService;
    @Autowired
    MySessionMapper mySessionMapper;

    @RequestMapping("/sys/json/testTableKey")
    @ResponseBody
    public String testTableKey(){
        String bm=   bmKeyService.getStringId()+"===="+bmKeyService.getIntegerId();
        String gw=   gwKeyService.getStringId()+"===="+gwKeyService.getIntegerId();
        return bm+"$$$"+gw;
    }
    /**
     * 跳转到敏感词页面
     */
    @RequestMapping("/sys/sensitivewords")
    public String sensitivewords(){
        return "/base/util/sensitivewords";
    }
    @ResponseBody
    @RequestMapping("/sys/test")
    public Object test(){
        Integer num=mySessionMapper.queryOnlineNum();
        return num;
    }
    @ResponseBody
    @RequestMapping("/sys/test2")
    public Object test2(){
        String   mysqlVersion= DbUtil.getJtN().queryForObject("select version()", String.class);
        return mysqlVersion;
    }
    @ResponseBody
    @RequestMapping("/sys/session/json/find")
    @DataScope
    public Object session_list(){
        Map map= MyBatisRequestUtil.getMap(request);
        map.put("yhid","");//要查询所有的，所以这个字段需要置空
        return mySessionMapper.find(getPageBounds(),map);
    }
    @ResponseBody
    @RequestMapping("/sys/session/json/delete")
    @RequiresRoles("admin")
    //踢出用户
    public Object session_delete(){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.error);
        try{
            Map map=MyBatisRequestUtil.getMap(request);
            String token=String.valueOf(map.get(TmConstant.TOKEN));
            mySessionMapper.deleteByToken(token);
            EhCacheUtils.putSysInfo(TmConstant.CACHE_GLOBAL_ITOKEN, token);

            msg.setType(Msg.MsgType.success);
            msg.setContent("删除会话成功!");
            MySessionListener tokenListener=new MySessionListener();
            MySession mySession=MySession.builder().yhid(String.valueOf(map.get(TmConstant.YHID_KEY))).token(String.valueOf(map.get(TmConstant.TOKEN))).build();
            tokenListener.tokenDelete("kickout",mySession);
        }catch (Exception ex){
            ex.printStackTrace();
            msg.setContent("删除会话异常!");
        }
        return msg;

    }
    //会话管理页面
    @RequestMapping("/sys/session")
    //@RequiresRoles("admin")
    public String session(){
        if(GlobalConfig.SSO_ENABLE){
            return "redirect:/sso/session";
        }else{
            return "/sys/session/list";
        }
    }

    @RequestMapping("/sys/testhtml")
    public ModelAndView testhtml(){
        ModelAndView mav= new ModelAndView();
        mav.addObject("city","test");
        mav.setViewName("testhtml");
        return mav;
    }



    /**
     * 跳转到首页页面
     * 2017年10月3日 11:18:15根据角色配置登陆后的主页面
     */
    @RequestMapping("/sys/front")
    @RequiresAuthentication
    public String front(){
        LoginBean lb= MyBatisRequestUtil.getLoginUserInfo(request);
        String mainPage="";
        //String yhid=lb.getYhid();
        String yhmc=lb.getXm();
        mainPage= lb.getMainpage();
        if(StringUtil.isEmpty(mainPage)){
            mainPage= SysPara.getValue("sys_frontpage","/main");
        }
        logger.info("用户【"+yhmc+"】登录后主页面====>" + mainPage);
        return mainPage;
    }
    /**
     * 开发人员的首页页面
     */
    @RequestMapping("/sys/developer")
    public String developer(Model model){
        String s="/main_developer";
        return s;
    }

    /**
     * 测试导出excel
     */
    @RequestMapping("/sys/excel")
    @RequiresAnonymous
    public void testExcel(Model model){
        String[] title=new String[] {"custname:购方名称", "sh:购方税号",
                "dzdh:购方地址电话", "gfyhzh:购方银行帐号", "bh:单据编号", "skr:收款人", "fhr:复核人","wlmc:商品名称",
                "spec:规格","spsm:商品税目", "taxrate:税率",
                "tax:税额","jgfs:价格方式","salenumber:数量",
                "unitbasicname:基本单位",
                "saleprice:单价",
                "saleamount:金额"};
        List<Map<String, Object>> mxlist =new ArrayList<Map<String, Object>>();
        Map<String, Object> tmpMap=null;
        for(int i=0;i<10;i++){
            tmpMap=new HashMap<String, Object>();
            tmpMap.put("bh", i);//单据编号
            tmpMap.put("custname", "custname"+i);//购方名称
            tmpMap.put("sh","sh"+i);//购方税号
            tmpMap.put("dzdh", "dzdh"+i);//购方地址电话
            tmpMap.put("gfyhzh","gfyhzh"+i);//购方银行帐号
            tmpMap.put("skr", "skr"+i);//收款人
            tmpMap.put("fhr", "fhr"+i);//复核人
            tmpMap.put("jgfs", i%2==0?"含税":"不含税");//价格方式
            tmpMap.put("spsm", "spsm"+i);//商品税目
            mxlist.add(tmpMap);
        }
        ExcelUtil.exportExcelWithSimpleFormat(mxlist, title, "销售开票", response);
    }
    /**
     * 测试到处pdf
     */
    @RequestMapping("/sys/pdf")
    @RequiresAnonymous
    public void testpdf(){
        Map<String,Object> dataMap=new HashMap<String,Object>();
        dataMap.put("fileName","我的订单");
        dataMap.put("mainTitle","mainTitle");
        String[] subTitleArr = new String[] {"bh:销售单号", "custname:客户名称",
                "linkman:联系人","custdh:联系电话", "kuozhan2:合同名称","signingtime:签约时间",
                "yfcdmc:运费承担","fkfsmc:付款方式","shr:收货人","lxfs:联系方式","fhdhsj:发货时间",
                "hyfsmc:货运方式","shdz:收货地址","sfhs:是否含税","hj_jshj:价税合计"
        };
        dataMap.put("subTitleArr", subTitleArr);
        Map<String,Object> subTitleMap=new HashMap<String,Object>();
        subTitleMap.put("bh", "BH-001");
        subTitleMap.put("custname", "阿里巴巴集团");
        subTitleMap.put("linkman", "马云");
        subTitleMap.put("custdh", "i don't know ");
        subTitleMap.put("kuozhan2", "合同名称");
        subTitleMap.put("signingtime", "2017年11月13日15:46:27");
        subTitleMap.put("shr", "聂风");
        subTitleMap.put("lxfs", "天下会风云堂");
        subTitleMap.put("fhdhsj", "2017年11月13日");
        subTitleMap.put("shdz", "天下会风云堂");
        subTitleMap.put("sfhs", "否");
        subTitleMap.put("hj_jshj", "99999999.99");

        subTitleMap.put("fkfsmc", "付款方式");
        subTitleMap.put("yfcdmc", "运费承担");
        subTitleMap.put("fkfsmc", "付款方式");
        dataMap.put("subTitleMap", subTitleMap);

        String[] pageFooterArr = new String[] { "salesman:业务员",
                "bz:备注", "generateDate:制单日期:5:10" };

        dataMap.put("pageFooterArr", pageFooterArr);
        Map<String,Object> pageFooterMap=new HashMap<String,Object>();
        pageFooterMap=subTitleMap;
        dataMap.put("pageFooterMap", pageFooterMap);
        String[] columParamArr = new String[] { "wldm:货物编号:10:center",
                "wlmc:货物名称:14:center", "spec:规格:10:center",
                "unitbasicname:单位:8:center",
                "taxrate:税率(%):7:center",
                "tax:税额:7:center",
                "basenumber:数量:8:center",
                "saleprice:单价:7:center",
                "saleamount:金额:10:center" };
        // 合计列名称
        String[] sumColumnArr = new String[] { "tax","basenumber","saleamount" };

        dataMap.put("columParamArr", columParamArr);
        dataMap.put("sumColumnArr", sumColumnArr);
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        dataMap.put("dataList", dataList);
        Map<String,Object> paperMap=new HashMap<String,Object>();
        paperMap.put("width","180");
        paperMap.put("height","110");
        paperMap.put("leftmargin","0.2");
        paperMap.put("rightmargin","0.2");
        paperMap.put("guid","11111");
        PdfUtil pdf = new PdfUtil(paperMap);
        try {
            pdf.exportPdf(dataMap, request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 跳转到基础设置页面
     */
    @RequestMapping("/sys/settings")
    public String settings(){
        return "/sys/settings";
    }
    /**
     * 跳转到系统日志页面
     */
    @RequestMapping("/sys/syslog")
    public String syslog(){
        return "/sys/syslog";
    }


    @RequestMapping("/sys/websleep")
    @ResponseBody
    public Object websleep(){
        System.out.println("====controller ====sleep()====");
        return  new Msg(Msg.MsgType.success,"成功测试controllerAspect！");
    }
    @RequestMapping("/sys/myProperties")
    @ResponseBody
    public Object getAllProperty(){
        Map<String, Object> map = new HashMap<String, Object>();
        //map.putAll(PropertiesListenerConfig.getAllProperty());
        map.putAll(GlobalConfig.getAllProperty());
        return map;
    }
    @RequestMapping("/sys/getAllSysPara")
    @ResponseBody
    public Object getAllSysPara(){
        Map<String, Object> map = new HashMap<String, Object>();
        map.putAll(GlobalConfig.getAllSysPara());
        return map;
    }
    @RequestMapping("/sys/getAllDict")
    @ResponseBody
    public Object getAllDict(){
        Map<String, Object> map = new HashMap<String, Object>();
        map.putAll(GlobalConfig.getAllDict());
        return map;
    }
    @RequestMapping("/sys/testEx")
    @ResponseBody
    public Object testEx(){

      //  test.testEx();
        return  new Msg(Msg.MsgType.success,"成功测试testEx！");
    }
    @RequestMapping("/sys/testEx2")
    @ResponseBody
    public Object testEx2(){

      //  test.testEx2();
        return  new Msg(Msg.MsgType.success,"成功测试testEx2！");

    }
   /* @RequestMapping("/testSendQueueMsg")
    @ResponseBody
    public Object testSendQueueMsg(){
        //  com.markbro.dzd.aspect.Test t=new com.markbro.dzd.aspect.Test();
        test.testSendQueueMsg();
        return  new Msg(Msg.MsgType.success,"成功测试testSendQueueMsg！");
    }*/
    /**
     * 获得系统配置
     * @param request
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/sys/getSysConfig")
    public Object getSysConfig(HttpServletRequest request, Model model) {
        //Map result= Global.getSysConfig(request);
        Map result= GlobalConfig.getSysConfig(request);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/sys/sessionValues")
    public Object sessionValues(HttpServletRequest request, Model model) {
        session.setAttribute("rnd",System.currentTimeMillis());
        String[] keys = RequestContextHolderUtil.getSessionKeys();
        Map<String,Object> sessions=new HashMap<>();
        for(String s:keys){
            sessions.put(s,RequestContextHolderUtil.getSession(s));
        }
        session.invalidate();
        return sessions;
    }


    /**
     * 跳转到用户选择页面
     */
    @RequestMapping("/base/util/yhxz")
    public String yhxz(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String idInput= (String) map.get("idInput");
        String treeType= (String) map.get("treeType");
        String mcInput= (String) map.get("mcInput");
        String limit= String.valueOf(map.get("limit"));
        String separator= String.valueOf(map.get("separator"));
        if(StringUtil.isEmpty(separator)){
            separator=";";
        }
        if(StringUtil.isEmpty(limit)){
            limit="1";
        }
        model.addAttribute("treeType",treeType);
        model.addAttribute("idInput",idInput);
        model.addAttribute("mcInput",mcInput);
        model.addAttribute("limit",limit);
        model.addAttribute("separator",separator);
        return "/base/util/yhxz";
    }
    /**
     * 图标选择
     */
    @RequestMapping(value = "/base/util/iconxz")
    public String iconselect(HttpServletRequest request, Model model) {
        Map map=MyBatisRequestUtil.getMap(request);
        String value= (String) map.get("value");
        String idInput= (String) map.get("idInput");
        model.addAttribute("value", value);
        model.addAttribute("idInput", idInput);
        return "/base/util/iconxz";
    }

    /**
     * 地区选择
     * @param model
     * @return
     */
    @RequestMapping(value={"/base/util/dqxz"})
    public String dqxz( Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String mcInput= (String) map.get("mcInput");
        String idInput= (String) map.get("idInput");
        model.addAttribute("idInput",idInput);
        model.addAttribute("mcInput",mcInput);
        return "/base/util/dqxz";
    }
    /**
     * 权限选择
     * @param model
     * @return
     */
    @RequestMapping(value={"/base/util/qxxz"})
    public String qxxz( Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String mcInput= (String) map.get("mcInput");
        String idInput= (String) map.get("idInput");
        model.addAttribute("idInput",idInput);
        model.addAttribute("mcInput",mcInput);
        return "/base/util/qxxz";
    }

    /**
     * 表单构建
     */
    @RequestMapping(value = "/base/util/formBuilder")
    public String formBuilder(HttpServletRequest request, Model model) {

        return "/base/util/formBuilder";
    }

    /**
     * 跳转相册2
     * @param model
     * @return
     */
    @RequestMapping("/cms/photos")
    public String photos(Model model){
        return "cms/album/photos";
    }
}
