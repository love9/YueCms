package com.makbro.core.base.orgUser.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgUser.bean.OrgUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 系统用户 dao
 * Created by wujiyue on 2016-07-05 22:52:54.
 */
@Repository
public interface OrgUserMapper {
    public OrgUser get(String id);
    public void add(OrgUser user);
    public void addBatch(List<OrgUser> users);
    public void update(OrgUser user);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除、有效数据的条件（deleted=0,available=1）
    public List<OrgUser> find(PageBounds pageBounds, Map<String, Object> map);
    public List<OrgUser> findByMap(PageBounds pageBounds, Map<String, Object> map);

	public List<OrgUser> findByOrgid(PageBounds pageBounds, String orgid);

    //查询用户Map
    public Map<String,Object> queryUserMapByYhid(@Param("yhid") String yhid);

    //根据局岗位查询该岗位下的人员列表
    public List<OrgUser> findRyListByGwid(PageBounds pageBounds, Map<String, Object> map);
    //保存用户头像路径
    public void saveHeaderPath(@Param("headerPath") String headerPath, @Param("id") String yhid);

    //根据用户唯一名称（登录名称账号）获得 yhid   博客系统-userBlog用到
    public String getYhidByAccount(String account);
    public String getHeaderPathByYhid(String account);//根据用户id获得用户头像路径

    public OrgUser getByOpenid(String openid);
    //根据角色获得用户列表
    public List<OrgUser> findByRole(PageBounds pageBounds, Map<String, Object> map);
    public List<OrgUser> findByRoleAndBm(PageBounds pageBounds, Map<String, Object> map);

    //根据用户输入邮箱发起重置密码请求时，验证该邮箱是否有效
    public OrgUser getUserByEmail(String email);
    //查询用户密码。只在找回密码时使用
    public String getPasswordByYhid(String yhid);

}
