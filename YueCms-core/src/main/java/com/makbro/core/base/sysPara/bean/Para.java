package com.makbro.core.base.sysPara.bean;


import com.markbro.base.model.AliasModel;

/**
 * 系统参数 bean
 * @author  wujiyue on 2016-07-03 19:25:35.
 */
public class Para  implements AliasModel {
	private String id;//
	private Integer mk_dm;//
	private String csdm;//参数代码
	private String csz;//
	private String cssm;//参数说明
	private String createTime;//
	private Integer deleted;//

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public Integer getMk_dm(){ return mk_dm ;}
	public void  setMk_dm(Integer mk_dm){this.mk_dm=mk_dm; }
	public String getCsdm(){ return csdm ;}
	public void  setCsdm(String csdm){this.csdm=csdm; }
	public String getCsz(){ return csz ;}
	public void  setCsz(String csz){this.csz=csz; }
	public String getCssm(){ return cssm ;}
	public void  setCssm(String cssm){this.cssm=cssm; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Para{" +
			"id=" + id+
			", mk_dm=" + mk_dm+
			", csdm=" + csdm+
			", csz=" + csz+
			", cssm=" + cssm+
			", createTime=" + createTime+
			", deleted=" + deleted+
			 '}';
	}
}
