package com.makbro.core.base.orgRole.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgRole.bean.Role;
import com.makbro.core.base.orgRole.dao.RoleMapper;
import com.makbro.core.base.permission.dao.PermissionMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.common.util.PatternUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Role service
 * Created by wujiyue on 2016-06-12 22:35:18.
 */
@Service
public class RoleService{
    @Autowired
    private TableKeyService keyService;
    @Autowired
    PermissionMapper permissionMapper;
    @Autowired
    private RoleMapper roleMapper;
     /*基础公共方法*/
    public Role get(String id){
        return roleMapper.get(id);
    }
    public Map<String,Object> getMap(String id){
        return roleMapper.getMap(id);
    }
    public List<Role> find(PageBounds pageBounds,Map<String,Object> map){
        return roleMapper.find(pageBounds,map);
    }
    public List<Role> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return roleMapper.findByMap(pageBounds,map);
    }
    public void add(Role role){
        roleMapper.add(role);
    }
    public void addBatch(List<Role> roles){
        roleMapper.addBatch(roles);
    }
    public void update(Role role){
        roleMapper.update(role);
    }
    public void updateByMap(Map<String,Object> map){
        roleMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        roleMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        roleMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        roleMapper.deleteBatch(ids);
    }

     public Object save(Map<String,Object> map){
         Msg msg=new Msg();
         try{
             Role role= JSON.parseObject(JSON.toJSONString(map), Role.class);
             if(role.getId()==null||"".equals(role.getId().toString())){
                 String id=keyService.getStringId();
                 role.setId(id);
                 roleMapper.add(role);
                 msg.setExtend("id",id);
             }else{
                 roleMapper.update(role);
             }
             msg.setType(Msg.MsgType.success);
             msg.setContent("保存信息成功");
         }catch (Exception ex){
             msg.setType(Msg.MsgType.error);
             msg.setContent("保存信息失败");
         }
        return msg;
     }
    //保存角色并且保存角色的权限
    @Transactional
    public Object saveRoleAndRolePermissions(Map<String,Object> map){
        Msg msg=new Msg();
        try{
            String id= (String) map.get("id");
            id= PatternUtil.isNull(id);
            String ids= (String) map.get("ids");//权限id用逗号分割的字符串
            if("".equals(id)){
                id=keyService.getStringId();
                map.put("id",id);
                roleMapper.addMap(map);
                //保存角色的权限
                if(ids.indexOf(",")>0){
                    String[] arr=ids.split(",");
                    int len=arr.length;
                    if(len>0){
                        permissionMapper.deleteRolePermission(id);
                        for(int i=0;i<len;i++){
                            permissionMapper.addRolePermission(arr[i],id);
                        }
                    }
                    msg.setType(Msg.MsgType.success);
                    msg.setContent("新增角色并授权成功");
                }else{
                    msg.setType(Msg.MsgType.success);
                    msg.setContent("新增角色成功");
                }

            }else{
                roleMapper.updateByMap(map);
                //保存角色的权限
                if(ids.indexOf(",")>0){
                    String[] arr=ids.split(",");
                    int len=arr.length;
                    if(len>0){
                        permissionMapper.deleteRolePermission((String) map.get("id"));
                        for(int i=0;i<len;i++){
                            permissionMapper.addRolePermission(arr[i],(String) map.get("id"));
                        }
                    }
                    msg.setType(Msg.MsgType.success);
                    msg.setContent("更新角色和角色权限成功");
                }else {
                    msg.setType(Msg.MsgType.success);
                    msg.setContent("更新角色成功");
                }

            }

        }catch (Exception ex){
            msg.setType(Msg.MsgType.error);
            msg.setContent("保存角色和角色权限信息失败");
        }
        return msg;
    }

    //获得当前登录用户可以获得的角色列表  返回结果map包含  角色ID,角色名称，该角色用户数量。方法供用户角色管理页面使用
   public List<Map<String,Object>> getRoleListByYh(Map map){
       List<Map<String,Object>> list=null;
       String bmid=String.valueOf(map.get(TmConstant.BMID_KEY));
       String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
       String gwid=String.valueOf(map.get(TmConstant.GWID_KEY));
       if("admin".equals(bmid)||"admin".equals(gwid)){
           //是管理员查询该orgid的所有角色
           Map<String,Object> tmap=new HashMap<String,Object>();
           tmap.put(TmConstant.ORGID_KEY,String.valueOf(map.get(TmConstant.ORGID_KEY)));
           list=roleMapper.getRoleListByYh(tmap);
       }else if("6060".equals(String.valueOf(map.get(TmConstant.ORGID_KEY)))){//这是腾蛟起凤组织id

           //如果当前用户是运维角色（李萍萍A）,那么查询所有代理商
           if("804".equals(yhid)){
               list=roleMapper.getZcptDlsRoleListAll();
           }else{//查询当前代理商
               list=roleMapper.getZcptDlsRoleListByDls(map);
           }
       }
       return list;
   }
}
