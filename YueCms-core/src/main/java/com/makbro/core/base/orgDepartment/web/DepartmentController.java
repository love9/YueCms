package com.makbro.core.base.orgDepartment.web;


import com.makbro.core.base.orgDepartment.bean.Department;
import com.makbro.core.base.orgDepartment.dao.DepartmentMapper;
import com.makbro.core.base.orgDepartment.service.DepartmentService;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 部门管理
 * Created by wujiyue on 2016-07-17 11:52:55.
 */
@Controller
@RequestMapping("/org/department")
@RequiresPermissions(value = {"/org/department","orgTreeManage"},logical= Logical.OR)
public class DepartmentController extends BaseController {
    @Autowired
    protected DepartmentService departmentService;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private OrgTreeMapper orgTreeMapper;
    @RequestMapping(value={"","/"})
    public String index(){
        return "base/department/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        model.addAttribute("parentid",(String) map.get("parentid"));
        model.addAttribute("parentname",(String) map.get("parentname"));
        return "/base/department/add";
    }
    /**
     * 跳转到编辑页面
     */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map=MyBatisRequestUtil.getMap(request);
        String id=(String)map.get("id");
        Map<String,Object> res=departmentService.getMap(id);
        String parentid= (String) res.get("parentid");
        if("0".equals(parentid)){
            String yhid= (String) map.get(TmConstant.YHID_KEY);
            Map<String,Object> orgMap= (Map<String, Object>) EhCacheUtils.getUserInfo("orgMap",yhid);
            res.put("parentname",(String)orgMap.get("name"));
        }
        model.addAttribute("department",res);
        return "base/department/edit";
    }

    /**
     * 跳转到列表页面
     */
    /*@RequestMapping(value={"/list"})
    public String list(PageParam pageParam,Model model){
        Object departments=null;
        departments=departmentService.find(getPageBounds(pageParam), MyBatisRequestUtil.getMap(request));
        model.addAttribute("departments",departments);
        model.addAttribute("pageParam",pageParam);
        return "base/department/list";
    }*/

    //-----------json数据接口--------------------
    
	@ResponseBody
	@RequestMapping("/json/findByParentid")
	public Object findByParentid() {
        Map map=MyBatisRequestUtil.getMap(request);
		return departmentService.findByParentid(getPageBounds(),map);
	}

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return departmentService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return departmentService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));

    }
    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增部门")
    public void add(Department m) {
        
        departmentService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Department m) {
        departmentService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存部门")
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        return  departmentService.save(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            int count=departmentService.getChildrenCount(id);
            if(count>0){
                return Msg.error("删除的部门下不能有子部门！");
            }else{
                Map<String,Object> map=new HashMap<String,Object>();
                map.put("deleted",1);
                map.put("id",id);
                departmentService.updateByMap(map);
                return Msg.success("删除成功！");
            }
        }catch (Exception e){
            return Msg.error("删除失败！");
        }

	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
    @Transactional
	public Object removes(@PathVariable String[] ids){
        try{
            for(int i=0;i<ids.length;i++)
            {
                Department department=departmentMapper.get(ids[i]);
                orgTreeMapper.deleteByLxidAndType(department.getId(),"bm","");
            }
            String ids_str= StringUtil.arrToString(ids, ",");
            int count=departmentService.getChildrenCount(ids_str);
            if(count>0){
                return Msg.error("删除的部门下不能有子部门！");
            }else{
                Map<String,Object> map=new HashMap<String,Object>();
                map.put("deleted",1);
                map.put("ids",ids);
                departmentService.updateByMapBatch(map);
                return Msg.success("批量删除成功！");
            }
        }catch (Exception e){
            return Msg.error("批量删除失败！");
        }
	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除部门")
    public void delete(@PathVariable String id) {
        departmentService.delete(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除部门")
    public void deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组

         departmentService.deleteBatch(ids);
    }
    //easyui 动态树
    @ResponseBody
    @RequestMapping("/json/tree")
    public Object tree() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return departmentService.tree(map);
    }

    @ResponseBody
    @RequestMapping("/json/saveSort")
    public Object saveSort() {
        return departmentService.saveSort(MyBatisRequestUtil.getMap(request));
    }
}
