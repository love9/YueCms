package com.makbro.core.base.orgRole.dao;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import com.makbro.core.base.orgRole.bean.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Role dao
 * Created by wujiyue on 2016-06-12 22:35:18.
 */
@Repository
public interface RoleMapper{
    public Role get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Role role);
    public void addMap(Map<String, Object> map);
    public void addBatch(List<Role> roles);
    public void update(Role role);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除、有效数据的条件（deleted=0,available=1）
    public List<Role> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Role> findByMap(PageBounds pageBounds, Map<String, Object> map);
    //给一个用户添加角色
    public void addUserRole(@Param("yhid") String yhid, @Param("jsid") String jsid);

    public List<Map<String,Object>> getRoleListByYh(Map<String, Object> map);

    //专门让注册平台代理商用，能查询到 代理商管理员、代理商普通用户、试用注册用户这3个角色
    public List<Map<String,Object>> getZcptDlsRoleListByDls(Map<String, Object> map);
    //查询所有代理商角色用户
    public List<Map<String,Object>> getZcptDlsRoleListAll();

    public Role getByNameAndOrgid(@Param("name") String name, @Param("orgid") String orgid);
}
