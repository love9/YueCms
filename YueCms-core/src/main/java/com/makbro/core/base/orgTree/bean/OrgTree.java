package com.makbro.core.base.orgTree.bean;


import com.markbro.base.model.AliasModel;

/**
 * 组织目录 bean
 * @author wujiyue
 */
public class OrgTree implements AliasModel {
	private String orgid;//组织id
	private String id;//机构角色id、主键
	private String parentid;//上级id
	private String type;//树中该节点的类型，bm部门，gw岗位，ry人员
	private String sjbmid;//上级部门id
	private String lxid;//人、部门、岗位对应的真实id
	private String name;//角色名称
	private String py;//
	private Integer sort;//排序序号
	private String bz;//备注
	private String createTime;//创建时间
	private String updateTime;//更新时间
	private String createBy;//创建人
	private String updateBy;//更新人
	private Integer available;//可用状态
	private Integer deleted;//删除标志。1删除。0未删除。

	public String getOrgid(){ return orgid ;}
	public void  setOrgid(String orgid){this.orgid=orgid; }
	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getParentid(){ return parentid ;}
	public void  setParentid(String parentid){this.parentid=parentid; }
	public String getType(){ return type ;}
	public void  setType(String type){this.type=type; }
	public String getSjbmid(){ return sjbmid ;}
	public void  setSjbmid(String sjbmid){this.sjbmid=sjbmid; }
	public String getLxid(){ return lxid ;}
	public void  setLxid(String lxid){this.lxid=lxid; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getPy(){ return py ;}
	public void  setPy(String py){this.py=py; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public String getBz(){ return bz ;}
	public void  setBz(String bz){this.bz=bz; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(String updateBy){this.updateBy=updateBy; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Tree{" +
			"orgid=" + orgid+
			", id=" + id+
			", parentid=" + parentid+
			", type=" + type+
			", sjbmid=" + sjbmid+
			", lxid=" + lxid+
			", name=" + name+
			", py=" + py+
			", sort=" + sort+
			", bz=" + bz+
			", createTime=" + createTime+
			", updateTime=" + updateTime+
			", createBy=" + createBy+
			", updateBy=" + updateBy+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
