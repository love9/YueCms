package com.makbro.core.base.recentNews.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.recentNews.bean.RecentNews;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 最新动态 dao
 * @author  wujiyue on 2017-08-29 13:30:55.
 */
@Repository
public interface RecentNewsMapper{

    public RecentNews get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(RecentNews recentNews);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<RecentNews> recentNewss);
    public void update(RecentNews recentNews);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<RecentNews> find(PageBounds pageBounds, Map<String, Object> map);
    public List<RecentNews> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
