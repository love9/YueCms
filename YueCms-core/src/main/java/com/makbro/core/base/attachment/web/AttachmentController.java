package com.makbro.core.base.attachment.web;

import com.makbro.core.base.attachment.bean.Attachment;
import com.makbro.core.base.attachment.service.AttachmentService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.model.Msg;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * 附件管理
 * @author wujiyue on 2017-09-19 16:45:31.
 */

@Controller
@RequestMapping("/base/attachment")
public class AttachmentController extends BaseController {
    @Autowired
    protected AttachmentService attachmentService;

    @RequestMapping(value={"","/"})
    @RequiresPermissions("base.attachment")
    public String index(){
        return "/base/attachment/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    @RequiresPermissions("base.attachment")
    public String toAdd(Attachment attachment, Model model){
        return "/base/attachment/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    @RequiresPermissions("base.attachment")
    public String toEdit(Attachment attachment, Model model){
        if(attachment!=null&&attachment.getId()!=null){
            attachment=attachmentService.get(attachment.getId());
        }
         model.addAttribute("attachment",attachment);
         return "/base/attachment/edit";
    }
    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return attachmentService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return attachmentService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(Attachment m) {
        attachmentService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Attachment m) {
        attachmentService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @RequiresAuthentication//用户登录即可有权调用
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return attachmentService.save(map);
    }
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable String id) {
    	Msg msg=new Msg();
    	try{
            attachmentService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
    	    msg.setType(Msg.MsgType.error);
    	    msg.setContent("删除失败！");
        }
        return msg;
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)

    public Object deletes(@PathVariable String[] ids) {
        Msg msg=new Msg();
    	try{
             attachmentService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }

    /**
     * 上传单张图片资源
     */
    @RequestMapping("/uploadSingle")
    @RequiresAuthentication
    public void uploadSingle() {
        Map map = MyBatisRequestUtil.getMapGuest(request);
        PrintWriter write = null;
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("file");
        if(multipartFile != null){
            map = attachmentService.uploadSingle(map, request, multipartFile);
        }
        JSONArray json=JSONArray.fromObject(map);
        try {
            write = response.getWriter();
            write.write(json.toString());
            write.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response = null;
            if (write != null){
                write.close();
            }
            write = null;
        }
    }
}