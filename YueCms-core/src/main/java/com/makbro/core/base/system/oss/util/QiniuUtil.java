package com.makbro.core.base.system.oss.util;

import com.alibaba.fastjson.JSON;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import com.qiniu.cdn.CdnManager;
import com.qiniu.cdn.CdnResult;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.BatchStatus;
import com.qiniu.storage.model.FetchRet;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.util.Auth;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Administrator on 2018/6/24.
 */
public class QiniuUtil {
    public static void main(String[] args) throws IOException {
     //   FetchRet ret=uploadByUrl("https://ss2.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=a74c804f17d5ad6eb5f962eab1ca39a3/8718367adab44aed5b24056fbf1c8701a08bfbd7.jpg", "11123");
      //  System.out.println(JSONObject.toJSON(ret));
        System.out.println(JSON.toJSON(getFileInfo("11123")));
       /* URL url = new URL("http://jieerkangxidi.com/girl-1");
        HttpURLConnection urlcon = (HttpURLConnection) url.openConnection();
        System.out.println(urlcon.getResponseCode()+"============"+urlcon.getContentLength());*/
      //File file = new File("D:\\girl\\girl-2.jpg");
       // System.out.println(JSONObject.toJSON(uploadByFile(file,"girl-2")).toString());

        //FileInfo fileInfo = getFileInfo("girl-1");
        //System.out.println(fileInfo);

        //uploadByUrl("https://mp.weixin.qq.com/debug/wxadoc/gitbook/gitbook.js?t=2018625","test");

       // deleteFile("test");
    FileInfo[] arr=   getFileListOfBucket("/dzd","");
        for(FileInfo f:arr){
            System.out.println(f.key);
        }
    }
    private static Logger logger = Logger.getLogger(QiniuUtil.class);
    public  final static String DefaultDomain2= GlobalConfig.getConfig("qiniu.DefaultDomain2");//外链默认域名          它+key=文件外部链接
    public  final static String DefaultDomain= GlobalConfig.getConfig("qiniu.DefaultDomain");
    private final static String AK = GlobalConfig.getConfig("qiniu.AK");
    private final static String SK = GlobalConfig.getConfig("qiniu.SK");

    private final static String BUCKET_DEFAULT =GlobalConfig.getConfig("qiniu.BUCKET_DEFAULT");
    //构造一个带指定Zone对象的配置类
    private static  Configuration cfg = new Configuration(Zone.zone0());
    private static UploadManager uploadManager = new UploadManager(cfg);
    private static Auth auth = Auth.create(AK, SK);



    /********************************文件上传相关***************************************/
    /**
     * 检测网络路径是否存在
     * @param url
     * @return
     */
    public static boolean checkNetUrlExists(String url){
        try{
            //URL u = new URL("http://jieerkangxidi.com/girl-1");
            URL u = new URL(url);
            HttpURLConnection urlcon = (HttpURLConnection) u.openConnection();
            //System.out.println(urlcon.getResponseCode()+"============"+urlcon.getContentLength());
            if(urlcon.getResponseCode()==200){
                return true;
            }
        }catch (Exception ex){
        }
        return false;
    }
    /**
     * @Title: uploadByFile
     * @Description: TODO 上传(本地)文件到七牛
     * @param @param file
     * @param @return    设定文件
     * @return QiniuResult    返回类型
     * @throws
     */
    public static QiniuResult uploadByFile(File file,String key) {

        try {
            return uploadByByte(FileUtils.readFileToByteArray(file),key);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * @Title: uploadByUrl
     * @Description: TODO 上传(网络)文件到七牛
     * @param @param remoteSrcUrl
     * @param @return    文件网络路径
     * @return FetchRet    返回类型
     * @throws
     */
    public static FetchRet uploadByUrl(String remoteSrcUrl,String keyName) {
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            FetchRet fetchRet = bucketManager.fetch(remoteSrcUrl, BUCKET_DEFAULT, keyName);
            logger.info("hash:" + fetchRet.hash+",key:"+fetchRet.key+",mimeType"+fetchRet.mimeType+",fsize:"+fetchRet.fsize);
            return fetchRet;
        } catch (QiniuException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    /**
     *
     *
     * @Title: uploadByByte
     * @Description: TODO 上传数组到七牛
     * @param @param bte
     * @param @return    设定文件
     * @return QiniuResult    返回类型
     * @throws
     */
    public static QiniuResult uploadByByte(byte[] bte,String key) {
        // 默认不指定key的情况下，以文件内容的hash值作为文件名
        key= StringUtil.isEmpty(key)?null:key;
        String upToken = auth.uploadToken(BUCKET_DEFAULT);
        try {
            Response response = uploadManager.put(bte, key, upToken);
            // 解析上传成功的结果
            logger.info("qiniu Response body=> " + response.bodyString());
            return QiniuResult.forBoject(response);
        } catch (QiniuException e) {
            e.getStackTrace();
            logger.error(e.getMessage(), e);
            if(e.getMessage().contains("file exists")){
                QiniuResult result=new QiniuResult();
                result.setCode("file exists");
                result.setMsg("文件已经存在!");
                return result;
            }
        }
        return null;
    }

    /**
     *
     *
     * @Title: uploadByInputStream
     * @Description: TODO 上传文件流到七牛
     * @param @param inp
     * @param @return    设定文件
     * @return QiniuResult    返回类型
     * @throws
     */
    public static QiniuResult uploadByInputStream(InputStream inp,String key) {

        // 默认不指定key的情况下，以文件内容的hash值作为文件名
        key= StringUtil.isEmpty(key)?null:key;
        //Auth auth = Auth.create(AK, SK);
        String upToken = auth.uploadToken(BUCKET_DEFAULT);
        try {
            Response response = uploadManager.put(inp, key, upToken, null, null);
            // 解析上传成功的结果
            logger.info("qiniu Response body=> " + response.bodyString());
            return QiniuResult.forBoject(response);
        } catch (QiniuException e) {
            e.getStackTrace();
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    /********************************下载文件相关***************************************/
    //对于公开空间

    //对于公开空间，其访问的链接主要是将空间绑定的域名（可以是七牛空间的默认域名或者是绑定的自定义域名）拼接上空间里面的文件名即可访问，标准情况下需要在拼接链接之前，将文件名进行urlencode以兼容不同的字符。

    //获取空间文件列表
    public static FileInfo[] getFileListOfBucket(String prefix,String delimiter){
        BucketManager bucketManager = new BucketManager(auth, cfg);
        //文件名前缀
        //每次迭代的长度限制，最大1000，推荐值 1000
        int limit = 1000;
        //指定目录分隔符，列出所有公共前缀（模拟列出目录效果）。缺省值为空字符串
        //列举空间文件列表
        BucketManager.FileListIterator fileListIterator = bucketManager.createFileListIterator(BUCKET_DEFAULT, prefix, limit, delimiter);
        while (fileListIterator.hasNext()) {
            FileInfo[] items = fileListIterator.next();
            /*for (FileInfo item : items) {
                System.out.println(item.key);
                System.out.println(item.hash);
                System.out.println(item.fsize);
                System.out.println(item.mimeType);
                System.out.println(item.putTime);
                System.out.println(item.endUser);
            }*/
            return items;
        }
        return null;
    }

    /********************************文件操作相关***************************************/

    //获得文件信息
    public static FileInfo getFileInfo(String key){
        //Auth auth = Auth.create(AK, SK);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            FileInfo fileInfo = bucketManager.stat(BUCKET_DEFAULT, key);
            return fileInfo;
        } catch (QiniuException ex) {
            //ex.printStackTrace();
            logger.error(ex.response.toString());
            return null;
        }
    }
    //重命名


    //删除空间中的文件
    public static boolean deleteFile(String key){
        //Auth auth = Auth.create(AK, SK);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(BUCKET_DEFAULT, key);
            logger.info("delete file ["+key+"]成功!");
            return true;
        } catch (QiniuException ex) {
            if("no such file or directory".equals(ex.response.error.toString())){
                logger.warn("no such file or directory====>"+key);
                return true;
            }else{
                ex.printStackTrace();
            }
            return  false;
        }
    }
    //批量删除文件
    public static boolean deleteFileBatch(String... keys){
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            batchOperations.addDeleteOp(BUCKET_DEFAULT, keys);
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < keys.length; i++) {
                BatchStatus status = batchStatusList[i];
                String key = keys[i];
                if (status.code == 200) {
                    logger.info(key + "delete success");
                } else {
                    logger.error(status.data.error);
                }
            }
            return true;
        } catch (QiniuException ex) {
            ex.printStackTrace();
            return  false;
        }
    }
    //批量移动文件
    public static boolean moveFileBatch(String... keys){
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            //单次批量请求的文件数量不得超过1000
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            for (String key : keys) {
                batchOperations.addMoveOp(BUCKET_DEFAULT, key, BUCKET_DEFAULT, key + "_move");
            }
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < keys.length; i++) {
                BatchStatus status = batchStatusList[i];
                String key = keys[i];
                if (status.code == 200) {
                    logger.info(key + ": move success");
                } else {
                    logger.error(status.data.error);
                }
            }
            return true;
        } catch (QiniuException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /********************************CDN相关***************************************/

    //1.文件刷新
    public static CdnResult.RefreshResult refreshUrls(String... urls) {
       /* String[] urls = new String[]{
                "http://javasdk.qiniudn.com/gopher1.jpg",
                "http://javasdk.qiniudn.com/gopher2.jpg",
                //....
        };*/
        CdnManager c = new CdnManager(auth);
        try {
            //单次方法调用刷新的链接不可以超过100个
            CdnResult.RefreshResult result = c.refreshUrls(urls);
            logger.info(result.code);
            return result;
        } catch (QiniuException e) {
            logger.error(e.response.toString());
            return null;
        }
    }
    //2.目录刷新
    public static CdnResult.RefreshResult refreshDirs(String... dirs) {
        CdnManager c = new CdnManager(auth);
        try {
            //单次方法调用刷新的目录不可以超过10个，另外刷新目录权限需要联系技术支持开通
            CdnResult.RefreshResult result = c.refreshDirs(dirs);
            logger.info(result.code);
            return result;
        } catch (QiniuException e) {
            logger.error(e.response.toString());
            return null;
        }
    }
    //3.获取域名流量
    public static  CdnResult.FluxResult getDomainFluxData(String fromDate,String toDate,String... domains){
        CdnManager c = new CdnManager(auth);
        String granularity = "day";
        //String fromDate = "2017-01-02";
        //String toDate = "2017-01-10";
        //数据粒度，支持的取值为 5min ／ hour ／day
        try {
            CdnResult.FluxResult fluxResult = c.getFluxData(domains, fromDate, toDate, granularity);
            return fluxResult;
        } catch (QiniuException e) {
            logger.error(e.response.toString());
            return null;
        }
    }
    //4.获取域名带宽
    //5.获取日志下载链接
    //6.构建时间戳防盗链访问链接
    /*public static String createTimestampAntiLeechUrl(String fileName){
       //fileName = "基本概括.mp4";
        StringMap queryStringMap = new StringMap();
        queryStringMap.put("name", "七牛");
        queryStringMap.put("year", 2017);
        queryStringMap.put("年龄", 28);
        //链接过期时间
        long deadline = System.currentTimeMillis() / 1000 + 3600;
        //签名密钥，从后台域名属性中获取
        String encryptKey = "xxx";？？？
        String signedUrl;
        try {
            signedUrl = CdnManager.createTimestampAntiLeechUrl(DefaultDomain, fileName,queryStringMap, encryptKey, deadline);
            System.out.println(signedUrl);
            return signedUrl;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }*/


}
