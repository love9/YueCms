package com.makbro.core.base.attachment.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.attachment.bean.Attachment;
import com.makbro.core.base.attachment.dao.AttachmentMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.SysPara;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.fileupload.util.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 附件 Service
 * @author  wujiyue on 2017-09-19 16:45:31.
 */
@Service
public class AttachmentService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private AttachmentMapper attachmentMapper;

     /*基础公共方法*/
    public Attachment get(String id){
        return attachmentMapper.get(id);
    }

    public List<Attachment> find(PageBounds pageBounds,Map<String,Object> map){
        return attachmentMapper.find(pageBounds,map);
    }
    public List<Attachment> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return attachmentMapper.findByMap(pageBounds,map);
    }

    public void add(Attachment attachment){
        attachmentMapper.add(attachment);
    }
    public Object save(Map<String,Object> map){

          Msg msg=new Msg();
          try{
             Attachment attachment= JSON.parseObject(JSON.toJSONString(map), Attachment.class);
            if(attachment.getId()==null||"".equals(attachment.getId().toString())){
                String id= Guid.get();
                String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
                attachment.setId(id);
                attachment.setYhid(yhid);
                attachment.setCreateBy(yhid);
                attachmentMapper.add(attachment);
            }else{
                attachmentMapper.update(attachment);
            }
               msg.setType(Msg.MsgType.success);
               msg.setContent("保存信息成功");
             }catch (Exception ex){
               msg.setType(Msg.MsgType.error);
               msg.setContent("保存信息失败");
            }
            return msg;
    }
    public void addBatch(List<Attachment> attachments){
        attachmentMapper.addBatch(attachments);
    }

    public void update(Attachment attachment){
        attachmentMapper.update(attachment);
    }

    public void updateByMap(Map<String,Object> map){
        attachmentMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        attachmentMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        attachmentMapper.delete(id);
    }

    public void deleteBatch(String[] ids){
        attachmentMapper.deleteBatch(ids);
    }

     /*自定义方法*/
     public Map<String, Object> uploadSingle(Map<String, Object> map, HttpServletRequest req, MultipartFile mFile) {
         //资源图片路径(相对于webapp下的路径)
         String basePathType = SysPara.getValue("uploadpath_type","0");
         String basepath = "";
         String relativePath = "";
         if ("1".equals(basePathType)) {//绝对路径类型
             basepath = SysPara.getValue("uploadfile_basepath","D:/dzd");
             relativePath = "/uploadresources";
         } else {
             basepath = req.getSession().getServletContext().getRealPath("/");
             relativePath = SysPara.getValue("uploadfile_basepath","/resources/static/cms/images");
         }
         relativePath= StringUtil.subEndStr(relativePath, "/");
         File dirFile=new File(basepath+relativePath);
         if(!dirFile.exists()){
             dirFile.mkdirs();
         }
         try {
             long size = 0;
             String path = "";
             String filename = "";
             String name = "";
             String dir = "";
             String suffixes ="";
             String guid= Guid.get();
             if (!mFile.isEmpty()) {
                 BufferedInputStream in = new BufferedInputStream(mFile.getInputStream());
                 filename = mFile.getOriginalFilename();
                 name=filename.substring(0,filename.indexOf("."));
                 // 取得文件后缀
                 suffixes = filename.substring(filename.lastIndexOf("."), filename.length());
                 path= relativePath+"/"+ DateUtil.formatDate(new Date(), "yyyyMMdd")+"/"+guid + suffixes;//相对路径
                 dir = basepath+File.separatorChar+path;// 绝对路径
                 String tempDir=dir.substring(0,dir.lastIndexOf("/"));
                 File tempDirFile=new File(tempDir);
                 if(!tempDirFile.exists()){
                     tempDirFile.mkdirs();
                 }
                 File ffout = new File(dir);
                 BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(ffout));
                 Streams.copy(in, out, true);
                 size = ffout.length();
             }
             map.put("path", path);
             map.put("name", name);
             map.put("suffixes", suffixes);
             map.put("size", size);
         }catch (IOException e) {
             e.printStackTrace();
         }
         return map;
     }
}
