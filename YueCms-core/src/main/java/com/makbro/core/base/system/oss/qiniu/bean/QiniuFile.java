package com.makbro.core.base.system.oss.qiniu.bean;


import com.markbro.base.model.AliasModel;

/**
 * 七牛文件资源 bean
 * @author  wujiyue on 2018-06-26 17:49:07 .
 */
public class QiniuFile  implements AliasModel {

	private String orgid;
	private String id;//
	private String name;//文件名称
	private String localpath;//本地路径
	private String url;//文件网络路径
	private String qiniu_url;//上传七牛后的外链路径
	private String suffix;//文件后缀
	private String mimeType;//文件类型
	private String mimeTypeName;
	private Long fsize;//文件大小
	private String putTime;//上传时间
	private String qiniu_key;//上传到七牛返回的key
	private String qiniu_hash;//上传到七牛返回的hash
	private Integer status;//文件状态
	private String resourcetype;//资源类型引用cms_resourcetype的ID
	private Integer deleted;//

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getLocalpath(){ return localpath ;}
	public void  setLocalpath(String localpath){this.localpath=localpath; }
	public String getUrl(){ return url ;}
	public void  setUrl(String url){this.url=url; }
	public String getQiniu_url(){ return qiniu_url ;}
	public void  setQiniu_url(String qiniu_url){this.qiniu_url=qiniu_url; }
	public String getSuffix(){ return suffix ;}
	public void  setSuffix(String suffix){this.suffix=suffix; }
	public String getMimeType(){ return mimeType ;}
	public void  setMimeType(String mimeType){this.mimeType=mimeType; }
	public Long getFsize(){ return fsize ;}
	public void  setFsize(Long fsize){this.fsize=fsize; }
	public String getPutTime(){ return putTime ;}
	public void  setPutTime(String putTime){this.putTime=putTime; }

	public String getMimeTypeName() {
		return mimeTypeName;
	}

	public void setMimeTypeName(String mimeTypeName) {
		this.mimeTypeName = mimeTypeName;
	}

	public Integer getStatus(){ return status ;}
	public void  setStatus(Integer status){this.status=status; }
	public String getResourcetype(){ return resourcetype ;}
	public void  setResourcetype(String resourcetype){this.resourcetype=resourcetype; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	public String getQiniu_key() {
		return qiniu_key;
	}

	public void setQiniu_key(String qiniu_key) {
		this.qiniu_key = qiniu_key;
	}

	public String getQiniu_hash() {
		return qiniu_hash;
	}

	public void setQiniu_hash(String qiniu_hash) {
		this.qiniu_hash = qiniu_hash;
	}

	@Override
	public String toString() {
		return "QiniuFile{" +
				"orgid=" + orgid +
				"id=" + id +
				", name='" + name + '\'' +
				", localpath='" + localpath + '\'' +
				", url='" + url + '\'' +
				", qiniu_url='" + qiniu_url + '\'' +
				", suffix='" + suffix + '\'' +
				", mimeType='" + mimeType + '\'' +
				", mimeTypeName='" + mimeTypeName + '\'' +
				", fsize=" + fsize +
				", putTime='" + putTime + '\'' +
				", qiniu_key='" + qiniu_key + '\'' +
				", qiniu_hash='" + qiniu_hash + '\'' +
				", status=" + status +
				", resourcetype='" + resourcetype + '\'' +
				", deleted=" + deleted +
				'}';
	}
}
