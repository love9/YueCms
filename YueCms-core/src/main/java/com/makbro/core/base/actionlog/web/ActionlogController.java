package com.makbro.core.base.actionlog.web;


import com.makbro.core.base.actionlog.bean.Actionlog;
import com.makbro.core.base.actionlog.service.ActionlogService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.markbro.base.annotation.DataScope;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志管理
 * Created by wujiyue on 2017-11-03 13:36:45.
 */

@Controller
@RequestMapping("/base/actionlog")
public class ActionlogController extends BaseController {
    @Autowired
    protected ActionlogService actionlogService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/base/actionlog/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Actionlog actionlog, Model model){
        return "/base/actionlog/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Actionlog actionlog, Model model){
        if(actionlog!=null&&actionlog.getId()!=null){
            actionlog=actionlogService.get(actionlog.getId());
        }
         model.addAttribute("actionlog",actionlog);
         return "/base/actionlog/edit";
    }
    //-----------json数据接口--------------------
    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return actionlogService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    @RequiresAuthentication
    @DataScope
    public Object find() {
        return actionlogService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map= MyBatisRequestUtil.getMap(request);
           return actionlogService.save(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable Integer id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            actionlogService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public Object delete(@PathVariable Integer id) {
    	try{
            actionlogService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    public Object deletes(@PathVariable Integer[] ids) {
    	try{
            actionlogService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
}