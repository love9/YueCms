package com.makbro.core.base.system.oss.mimeType.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.system.oss.mimeType.bean.MimeType;
import com.makbro.core.base.system.oss.mimeType.dao.MimeTypeMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 文件类型 Service
 * @author  by wujiyue on 2018-06-27 09:33:52.
 */
@Service
public class MimeTypeService implements InitCacheListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private MimeTypeMapper mimeTypeMapper;

     /*基础公共方法*/
    public MimeType get(Integer id){
        return mimeTypeMapper.get(id);
    }
    public List<MimeType> find(PageBounds pageBounds,Map<String,Object> map){
        return mimeTypeMapper.find(pageBounds,map);
    }
    public List<Map<String,Object>> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return mimeTypeMapper.findByMap(pageBounds,map);
    }
    public void add(MimeType mimeType){
        mimeTypeMapper.add(mimeType);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          MimeType mimeType= JSON.parseObject(JSON.toJSONString(map),MimeType.class);
          if(mimeType.getId()==null||"".equals(mimeType.getId().toString())){
              Integer id= keyService.getIntegerId();
              mimeType.setId(id);
              mimeTypeMapper.add(mimeType);
          }else{
              mimeTypeMapper.update(mimeType);
          }
          msg.setType(Msg.MsgType.success);
          msg.setContent("保存信息成功");
          return msg;
    }
    public void addBatch(List<MimeType> mimeTypes){
        mimeTypeMapper.addBatch(mimeTypes);
    }

    public void update(MimeType mimeType){
        mimeTypeMapper.update(mimeType);
    }

    public void updateByMap(Map<String,Object> map){
        mimeTypeMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        mimeTypeMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        mimeTypeMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        mimeTypeMapper.deleteBatch(ids);
    }


     /*自定义方法*/
     @Override
     public void onInit() {
         //初始化时把表中记录添加至缓存中
         List<MimeType> list=mimeTypeMapper.find(new PageBounds(),null);
         if(CollectionUtils.isNotEmpty(list)){
             for(MimeType type:list){
                 EhCacheUtils.putSysInfo("mimeType",type.getMimeType(),type);
             }
             //logger.info("=======>成功向缓存中添加【"+list.size()+"】条MimeType记录");
         }
     }
}
