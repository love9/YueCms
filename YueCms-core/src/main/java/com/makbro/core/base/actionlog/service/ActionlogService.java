package com.makbro.core.base.actionlog.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.actionlog.bean.Actionlog;
import com.makbro.core.base.actionlog.dao.ActionlogMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 操作日志 Service
 * Created by wujiyue on 2017-11-03 13:36:44.
 */
@Service
public class ActionlogService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private ActionlogMapper actionlogMapper;

     /*基础公共方法*/
    public Actionlog get(Integer id){
        return actionlogMapper.get(id);
    }

    public List<Actionlog> find(PageBounds pageBounds, Map<String,Object> map){
        return actionlogMapper.find(pageBounds,map);
    }
    public List<Actionlog> findByMap(PageBounds pageBounds, Map<String,Object> map){
        return actionlogMapper.findByMap(pageBounds,map);
    }

    public void add(Actionlog actionlog){
        actionlogMapper.add(actionlog);
    }
    public Object save(Map<String,Object> map){

          Msg msg=new Msg();
          try{
           Actionlog actionlog= JSON.parseObject(JSON.toJSONString(map),Actionlog.class);
            if(actionlog.getId()==null||"".equals(actionlog.getId().toString())){
               Integer id= keyService.getIntegerId();
               actionlog.setId(id);
               actionlogMapper.add(actionlog);
            }else{
               actionlogMapper.update(actionlog);
            }
               msg.setType(Msg.MsgType.success);
               msg.setContent("保存信息成功");
             }catch (Exception ex){
               msg.setType(Msg.MsgType.error);
               msg.setContent("保存信息失败");
            }
            return msg;
    }
    public void addBatch(List<Actionlog> actionlogs){
        actionlogMapper.addBatch(actionlogs);
    }

    public void update(Actionlog actionlog){
        actionlogMapper.update(actionlog);
    }

    public void updateByMap(Map<String,Object> map){
        actionlogMapper.updateByMap(map);
    }

    public void delete(Integer id){
        actionlogMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        actionlogMapper.deleteBatch(ids);
    }



     /*自定义方法*/

}
