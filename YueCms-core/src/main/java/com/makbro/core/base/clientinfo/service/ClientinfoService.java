package com.makbro.core.base.clientinfo.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.clientinfo.bean.Clientinfo;
import com.makbro.core.base.clientinfo.dao.ClientinfoMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.model.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 客户端信息 Service
 * @author  wujiyue on 2018-09-01 14:22:00.
 */
@Service
public class ClientinfoService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private ClientinfoMapper clientinfoMapper;

     /*基础公共方法*/
    public Clientinfo get(Integer id){
        return clientinfoMapper.get(id);
    }
    public List<Clientinfo> find(PageBounds pageBounds,Map<String,Object> map){
        return clientinfoMapper.find(pageBounds,map);
    }
    public List<Clientinfo> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return clientinfoMapper.findByMap(pageBounds,map);
    }
    public void add(Clientinfo clientinfo){
        clientinfoMapper.add(clientinfo);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          Clientinfo clientinfo= JSON.parseObject(JSON.toJSONString(map),Clientinfo.class);
          if(clientinfo.getId()==null||"".equals(clientinfo.getId().toString())){
              Integer id= keyService.getIntegerId();
              clientinfo.setId(id);
              clientinfoMapper.add(clientinfo);
          }else{
              clientinfoMapper.update(clientinfo);
          }
          msg.setType(Msg.MsgType.success);
          msg.setContent("保存信息成功");
          return msg;
    }
    public void update(Clientinfo clientinfo){
        clientinfoMapper.update(clientinfo);
    }

    public void updateByMap(Map<String,Object> map){
        clientinfoMapper.updateByMap(map);
    }

    public void delete(Integer id){
        clientinfoMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        clientinfoMapper.deleteBatch(ids);
    }

     /*自定义方法*/
}
