package com.makbro.core.base.loginLog.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.loginLog.bean.LoginLog;
import com.makbro.core.base.loginLog.dao.LoginLogMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.base.utils.useragent.UserAgentUtils;
import com.markbro.thirdapi.tencent.TencentApiUtil;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.DeviceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 登录日志 Service
 * @author wujiyue
 * @date 2018-11-03 00:11:20
 */
@Service
public class LoginLogService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private LoginLogMapper loginLogMapper;

     /*基础公共方法*/
    public LoginLog get(Integer id){
        return loginLogMapper.get(id);
    }
    public List<LoginLog> find(PageBounds pageBounds,Map<String,Object> map){
        return loginLogMapper.find(pageBounds,map);
    }
    public List<LoginLog> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return loginLogMapper.findByMap(pageBounds,map);
    }
    public void add(LoginLog loginLog){
        loginLogMapper.add(loginLog);
    }
    public Object save(Map<String,Object> map){
          LoginLog loginLog=JSON.parseObject(JSON.toJSONString(map),LoginLog.class);
          if(loginLog.getId()==null||"".equals(loginLog.getId().toString())){
              Integer id= keyService.getIntegerId();
              loginLog.setId(id);
              loginLogMapper.add(loginLog);
          }else{
              loginLogMapper.update(loginLog);
          }
          return Msg.success("保存信息成功");
    }
    public void update(LoginLog loginLog){
        loginLogMapper.update(loginLog);
    }

    public void updateByMap(Map<String,Object> map){
        loginLogMapper.updateByMap(map);
    }

    public void delete(Integer id){
        loginLogMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        loginLogMapper.deleteBatch(ids);
    }



     /*自定义方法*/

    /**
     * 记录登录日志
     * @param request
     * @param msg  登录验证结果 为空说明登录成功，不为空是登录失败的信息
     */

    public void recordLogin(HttpServletRequest request,String msg){
        String account = StringUtil.isNull(request.getParameter(TmConstant.pageInputDd));
        String yhid= MyBatisRequestUtil.getYhid();
        String ip= MyBatisRequestUtil.getIpByHttpRequest(request);
        String location=(String) EhCacheUtils.getSysInfo("IP_LOCATION", ip);//IP对应的地址(先从缓存中取，取不到再调用接口查询)
        if("127.0.0.1".equals(ip)||"0:0:0:0:0:0:0:1".equals(ip)){
            location="本地";
        }
        if(StringUtil.isEmpty(location)){
            //调用接口查询ip对应的地址
            if(StringUtil.notEmpty(ip)){
                location= TencentApiUtil.queryIpLocation(ip);
                if(StringUtil.notEmpty(location)){
                    EhCacheUtils.putSysInfo("IP_LOCATION",ip,location);
                }
            }
        }
        Browser browserObj = UserAgentUtils.getBrowser(request);
        String browser=browserObj.getName();//浏览器类型
        String deviceType="Unknown";//设备类型
        DeviceType deviceType1=UserAgentUtils.getDeviceType(request);//是否pc
        if(deviceType1!=null){
            deviceType=deviceType1.getName();
        }
        String os=UserAgentUtils.getUserAgent(request).getOperatingSystem().getName();
        Map paramMap=MyBatisRequestUtil.getMapGuest(request);
        String params=JSON.toJSONString(paramMap);
        LoginLog loginLog=LoginLog.build().setYhid(yhid).setAccount(account).setIp(ip).setLocation(location).setExplore(browser).setDeviceType(deviceType).setResult(StringUtil.isEmpty(msg)?"1":"0").setMsg(StringUtil.isEmpty(msg)?"登录成功":msg).setOs(os).setParams(params);
        loginLogMapper.add(loginLog);
    }
}
