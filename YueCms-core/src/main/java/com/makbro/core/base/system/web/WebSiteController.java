package com.makbro.core.base.system.web;

import com.makbro.core.base.system.service.WebsiteService;
import com.makbro.core.framework.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 网站站点控制转发器。
 */
@Controller
public class WebSiteController extends BaseController {

    private static final String BASE_PATH="/website";

    @Autowired
    WebsiteService websiteService;
    //控制站点首页跳转到哪个页面
    @RequestMapping(value = "/")
    public String index(Model model){
        return "redirect:/index";
    }

    @RequestMapping(value = "/index")
    public String index2(Model model){
        return BASE_PATH+"/index";
    }
    //技术文档
    @RequestMapping(value = "/doc")
    public String doc(Model model){
        return BASE_PATH+"/doc";
    }

    @RequestMapping(value = "/website/siteInfo")
    @ResponseBody
    public Object siteInfo(Model model){
        return websiteService.getSiteInfo();
    }
}
