package com.makbro.core.base.system.oss.mimeType.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.system.oss.mimeType.bean.MimeType;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 文件类型 dao
 * @author  wujiyue on 2018-06-27 09:33:52.
 */
@Repository
public interface MimeTypeMapper{

    public MimeType get(Integer id);
    public Map<String,Object> getMap(Integer id);
    public void add(MimeType mimeType);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<MimeType> mimeTypes);
    public void update(MimeType mimeType);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(Integer id);
    public void deleteBatch(Integer[] ids);
    //find与findByMap的区别是在find方法在where条件中多了未删除的条件（deleted=0）并且返回值类型不同
    public List<MimeType> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Map<String,Object>> findByMap(PageBounds pageBounds, Map<String, Object> map);




}
