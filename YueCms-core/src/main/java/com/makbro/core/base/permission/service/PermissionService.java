package com.makbro.core.base.permission.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.permission.bean.Permission;
import com.makbro.core.base.permission.bean.PermissionVo;
import com.makbro.core.base.permission.dao.PermissionMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.makbro.core.framework.listener.ChangeCacheListener;
import com.makbro.core.framework.listener.InitCacheListener;
import com.markbro.base.common.util.Guid;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.PatternUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.EhCacheUtils;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.base.utils.tree.TreeUtils;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 菜单权限 service
 * @author wujiyue
 */
@Service
public class PermissionService implements InitCacheListener, ChangeCacheListener {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private TableKeyService keyService;
     /*基础公共方法*/
    public Map<String,Object> getMap(String id){
        return permissionMapper.getMap(id);
    }
    public List<Permission> find(PageBounds pageBounds, Map<String,Object> map){
        return permissionMapper.find(pageBounds, map);
    }

    public void add(Permission permission){
        permissionMapper.add(permission);
    }
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          try{
              Permission permission= JSON.parseObject(JSON.toJSONString(map), Permission.class);
              if(permission.getId()==null||"".equals(permission.getId().toString())){
                  String id= keyService.getStringId();
                  permission.setId(id);
                  String parentid=permission.getParentid();
                  String pids= permissionMapper.getParentidsById(parentid);
                  if("null".equals(pids)||pids==null){
                      pids="0,";
                  }
                  pids+=id+",";
                  permission.setParentids(pids);
                  int sort=permissionMapper.getMaxSortByParentid(parentid);
                  permission.setSort(sort+1);
                  if(permission.getAvailable()==null){
                      permission.setAvailable(1);
                  }
                  permissionMapper.add(permission);
              }else{
                  String parentid=permission.getParentid();
                  String pids= permissionMapper.getParentidsById(parentid);
                  if("null".equals(pids)||pids==null){
                      pids="0,";
                  }
                  pids+=permission.getId()+",";
                  permission.setParentids(pids);
                  permissionMapper.update(permission);
              }
              msg.setType(Msg.MsgType.success);
              msg.setContent("保存信息成功");
          }catch (Exception ex){
              msg.setType(Msg.MsgType.error);
              msg.setContent("保存信息失败");
          }
        return msg;
    }
    public void addBatch(List<Permission> permissions){
        permissionMapper.addBatch(permissions);
    }
    public void update(Permission permission){
        permissionMapper.update(permission);
    }
    public void updateByMap(Map<String,Object> map){
        permissionMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        permissionMapper.updateByMapBatch(map);
    }
    public void delete(String id){
        permissionMapper.delete(id);
    }
    public void deleteBatch(String[] ids){
        permissionMapper.deleteBatch(ids);
    }
     /*自定义方法*/

	 public List<Permission> findByParentid(PageBounds pageBounds,Map<String,Object> map){
		return permissionMapper.findByParentid(pageBounds,map);
	}
    public int getChildrenCount(String ids){
        ids=ids.replaceAll(",", "','").replaceAll("~", "','");
        return permissionMapper.getChildrenCount(ids);
    }
    public Object saveSort(Map map){
        Msg msg=new Msg();
        try{
            String sort = String.valueOf(map.get("sort"));
            if(!"".equals(sort)){
                String[] sx = sort.split(",");
                for(int i=0;i<sx.length;i++){
                    String[] arr = sx[i].split("_");
                    permissionMapper.updateMenuSort(arr[1], arr[2]);
                }
            }
            msg.setType(Msg.MsgType.success);
            msg.setContent("排序成功！");
        }catch (Exception e){
            msg.setType(Msg.MsgType.error);
            msg.setContent("排序失败！");
        }
        return msg;
    }
    /**
     * easyui 编辑角色时初始化静态权限树
     * @param map
     * @return
     */
    public String staticTreeByRole(Map<String, Object> map){
        String[] columns={"id","name","parentid","checked"};
        String rootName="权限系统";
        String jsid=(String)map.get("jsid");
        List<String> jsqxs=permissionMapper.findPermissionsByRole(jsid);
        map.put("available","1");
        map.put("deleted","0");
        List<Map<String,Object>> allList= permissionMapper.findMap(new PageBounds(),map);
        for(Map<String,Object> temp:allList){
            if(StringUtil.isContains((String) temp.get("id"), jsqxs)){
                temp.put("checked","1");
            }else{
                temp.put("checked","0");
            }
        }
        String jsonString= TreeUtils.getTreeJsonString(allList,columns,rootName);
        return jsonString;
    }
    /**
     * easyui 静态权限树
     * @param map
     * @return
     */
    public String staticTree(Map<String, Object> map){
        String[] columns={"id","name","parentid",""};
        String rootName="权限系统";
        List<Map<String,Object>> allList= permissionMapper.findMap(new PageBounds(), map);
        String jsonString= TreeUtils.getTreeJsonString(allList, columns, rootName);
        return jsonString;
    }
    //zTree静态树
    public Object zTreeStatic(Map<String, Object> map){
        String jsid=(String)map.get("jsid");//角色id
        List<String> jsqxs=null;
        if(StringUtil.notEmpty(jsid)){
             jsqxs=permissionMapper.findPermissionsByRole(jsid);//查询该角色的权限
        }


        String orgid= MyBatisRequestUtil.getLoginUserInfo().getOrgid();
        List<Permission> permissions_all=(List<Permission>) EhCacheUtils.getSysInfo("permissions",orgid);
        if(CollectionUtils.isEmpty(permissions_all)){
            onInit();
            permissions_all=(List<Permission>)EhCacheUtils.getSysInfo("permissions",orgid);
        }
        List<Permission> permissions= permissions_all.stream().filter(p->{
            return ("1".equals(String.valueOf(p.getAvailable()))&&"1".equals(String.valueOf(p.getIsmenu())));
        }).collect(Collectors.toList());
        List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
        Map<String,Object> temp=null;
        for(Permission t:permissions){
            temp=new HashMap<String,Object>();
            temp.put("name",t.getName());
            temp.put("title",t.getName());
            temp.put("id",t.getId());
            temp.put("parentid",t.getParentid());
            if(CollectionUtils.isNotEmpty(jsqxs)){
                if(StringUtil.isContains((String) temp.get("id"), jsqxs)){
                    temp.put("checked",true);
                }else{
                    temp.put("checked",false);
                }
            }
            list.add(temp);
        }
        return list;
    }
    /**
     * easyui 动态树
     * @param map
     * @return
     */
    public String tree(Map<String, Object> map){
        // return areaMapper.findByParentid(pageBounds,parentid);
        String parentid= (String) map.get("parentid");
        Map<String, Object> rootNode=new HashMap<String, Object>();//根节点
        List<Permission> list=null;
        List<Permission> childrenlist=null;//每个区域的孩子集合
        Map<String, Object> attributes=null;//每个区域的属性
        List<Map<String, Object>> nodelist =null;//要返回的节点集合
        Map<String, Object> node=null;//节点
        Map tmap=new HashMap<String,Object>();
        tmap.put("orgid",(String)map.get("orgid"));

        if("0".equals(parentid)){
            tmap.put("parentid","0");
            list=permissionMapper.findByParentid(new PageBounds(),tmap);
            nodelist = new ArrayList<Map<String,Object>>();
            //组装根节点
            rootNode.put("id", "0");
            rootNode.put("text", "权限系统");
            if(list!=null&&list.size()>0){
                rootNode.put("state", "open");
                for(Permission area:list){
                    node=new HashMap<String, Object>();
                    attributes=new HashMap<String, Object>();
                    String id=area.getId();

                    node.put("id", area.getId());
                    node.put("text", area.getName());

                    attributes.put("link", area.getUrl());
                    attributes.put("code",area.getCode());
                    node.put("attributes", attributes);
                    tmap.put("parentid",id);
                    childrenlist=permissionMapper.findByParentid(new PageBounds(),tmap);
                    if(childrenlist!=null&childrenlist.size()>0){
                        node.put("state", "closed");
                    }
                    nodelist.add(node);
                }
                rootNode.put("children", nodelist);
            }
            JsonConfig jsonConfig = new JsonConfig();
            jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);//自动为我排除circle。
            JSONArray jsonArray = JSONArray.fromObject(rootNode, jsonConfig);
            return jsonArray.toString();
        }else{

            tmap.put("parentid",parentid);
            list=permissionMapper.findByParentid(new PageBounds(),tmap);
            nodelist = new ArrayList<Map<String,Object>>();
            if(list!=null&&list.size()>0){
                rootNode.put("state", "open");
                for(Permission area:list){
                    node=new HashMap<String, Object>();
                    attributes=new HashMap<String, Object>();
                    String id=area.getId();

                    node.put("id", area.getId());
                    node.put("text", area.getName());

                    attributes.put("link", area.getUrl());
                    attributes.put("code",area.getCode());
                    node.put("attributes", attributes);
                    tmap.put("parentid",id);
                    childrenlist=permissionMapper.findByParentid(new PageBounds(),tmap);
                    if(childrenlist!=null&childrenlist.size()>0){
                        node.put("state", "closed");
                    }
                    nodelist.add(node);
                }
            }
            JsonConfig jsonConfig = new JsonConfig();
            jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);//自动为我排除circle。
            JSONArray jsonArray = JSONArray.fromObject(nodelist, jsonConfig);
            return jsonArray.toString();
        }
    }


    public void checkSelected(List<String> jsqxs,List<PermissionVo> list){
        if(CollectionUtils.isNotEmpty(list)){
            for(PermissionVo t:list){
                if(StringUtil.isContains(t.getId(),jsqxs)){
                    t.setChecked("1");
                }
                if(CollectionUtils.isNotEmpty(t.getChildren())){
                    checkSelected(jsqxs,t.getChildren());
                }
            }
        }
    }

    public Object saveRolePermissions(Map map){
        Msg msg=new Msg();
        try {
            String ids= (String) map.get("ids");
            String jsid= (String) map.get("jsid");
            String[] arr=ids.split(",");
            int len=arr.length;
            if(len>0){
                permissionMapper.deleteRolePermission(jsid);
                for(int i=0;i<len;i++){
                    permissionMapper.addRolePermission(arr[i],jsid);
                }
            }

            msg.setType(Msg.MsgType.success);
            msg.setContent("保存角色权限成功！");
        }catch (Exception e){
            msg.setType(Msg.MsgType.error);
            msg.setContent("保存角色权限失败！");
        }
        return msg;
    }

    /**
     * 用户的权限列表
     * @param yhid
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> getQxByYhid(String yhid) {
        List<Map<String,Object>> qxList=permissionMapper.queryQxByYhid(yhid);

        return qxList;
    }
    /**
     * 用户的菜单列表
     * @param yhid
     * @return
     */
    public List<Map<String, Object>> getUserMenus(String yhid) {

        List<Map<String,Object>> qxList2=permissionMapper.queryQxByYhid(yhid);//
        List<Map<String,Object>> qxList= qxList2.stream().filter(qx->{
            return (!"2".equals(qx.get("qxlx"))&&!"0".equals(String.valueOf(qx.get("ismenu"))));}
            //不是菜单的去掉，qxlx=2表示时按钮权限，这个过滤目的是取得目录和页面菜单 comment by wujiyue on 2017年10月27日15:39:43
        ).sorted((qx1, qx2) -> ((Integer) qx1.get("sort")).compareTo((Integer) qx2.get("sort")))//排序
        .collect(Collectors.toList());//收集

        Map<String, List<Map<String,Object>>> cacheListMap = new LinkedHashMap<String, List<Map<String,Object>>>();

        for(Map<String,Object> tt:qxList){
            String pid= (String) tt.get("parentid");
            if(cacheListMap.get(pid)==null){
                List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
                list.add(tt);
                cacheListMap.put(pid,list);
            }else{
                List<Map<String,Object>> list =cacheListMap.get(pid);
                list.add(tt);
                cacheListMap.put(pid,list);
            }
        }

        for (Map.Entry<String, List<Map<String,Object>>> entry : cacheListMap.entrySet()) {
            List<Map<String,Object>> smallTreeList = new ArrayList<Map<String,Object>>();
            smallTreeList = entry.getValue();
            int nodeListSize = smallTreeList.size();
            for (int i = 0; i < nodeListSize; i++) {
                String findID = (String) smallTreeList.get(i).get("id");
                List<Map<String,Object>> findList = cacheListMap.get(findID);
                if(findList!=null&&findList.size()>0){
                    List<Map<String,Object>>  list=findList.stream().sorted((qx1, qx2) -> ((Integer) qx1.get("sort")).compareTo((Integer) qx2.get("sort"))).collect(Collectors.toList());
                    smallTreeList.get(i).put("children",list);
                }
            }
        }
        List<Map<String, Object>> result=cacheListMap.get("0");
        return result ;
    }

    /**
     * 将数据库表查询出来的权限格式化成菜单列表
     * @param qxList2
     * @return
     */
    public List<Map<String, Object>> permissionListToMenuList(List<Map<String, Object>> qxList2) {

        List<Map<String,Object>> qxList= qxList2.stream().filter(qx->{
                    return (!"2".equals(qx.get("qxlx"))&&!"0".equals(String.valueOf(qx.get("ismenu"))));}
                //不是菜单的去掉，qxlx=2表示时按钮权限，这个过滤目的是取得目录和页面菜单 comment by wujiyue on 2017年10月27日15:39:43
        ).sorted((qx1, qx2) -> ((Integer) qx1.get("sort")).compareTo((Integer) qx2.get("sort")))//排序
                .collect(Collectors.toList());//收集

        Map<String, List<Map<String,Object>>> cacheListMap = new LinkedHashMap<String, List<Map<String,Object>>>();

        for(Map<String,Object> tt:qxList){
            String pid= (String) tt.get("parentid");
            if(cacheListMap.get(pid)==null){
                List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
                list.add(tt);
                cacheListMap.put(pid,list);
            }else{
                List<Map<String,Object>> list =cacheListMap.get(pid);
                list.add(tt);
                cacheListMap.put(pid,list);
            }
        }

        for (Map.Entry<String, List<Map<String,Object>>> entry : cacheListMap.entrySet()) {
            List<Map<String,Object>> smallTreeList = new ArrayList<Map<String,Object>>();
            smallTreeList = entry.getValue();
            int nodeListSize = smallTreeList.size();
            for (int i = 0; i < nodeListSize; i++) {
                String findID = (String) smallTreeList.get(i).get("id");
                List<Map<String,Object>> findList = cacheListMap.get(findID);
                if(findList!=null&&findList.size()>0){
                    List<Map<String,Object>>  list=findList.stream().sorted((qx1, qx2) -> ((Integer) qx1.get("sort")).compareTo((Integer) qx2.get("sort"))).collect(Collectors.toList());
                    smallTreeList.get(i).put("children",list);
                }
            }
        }
        List<Map<String, Object>> result=cacheListMap.get("0");
        return result ;
    }


    /**
     * 调整菜单上级ID
     * @param map
     * @return
     *
     */
    public Object modifySjid(Map<String, Object> map) {
        Map<String, Object> m = new HashMap<String, Object>();
        Msg msg=new Msg();
        String menu = String.valueOf(map.get("ids"));
        String sjid = String.valueOf(map.get("sjid"));
        if(PatternUtil.isNull(menu).equals("") || PatternUtil.isNull(sjid).equals("")){
            msg.setType(Msg.MsgType.error);
            msg.setContent("请选择要调整的目标菜单！");
            return msg;
        }
        String[] menus = menu.split("~");
        for(int i=0;i<menus.length;i++){
            permissionMapper.updateMenuSjid(sjid, menus[i]);
        }
        msg.setType(Msg.MsgType.success);
        msg.setContent("菜单调整成功！");
        return msg;
    }

    /**
     * 新建组织时把权限复制一份并返回新组织权限id列表
     * @param newOrgid
     * @return
     */
    public List<String> copyPermissionsToNewOrg(String newOrgid){
        try{
            List<String> newKeyList=new ArrayList<String>();
            Map map=new HashMap<String,Object>();
            map.put("orgid","0");
            map.put("available","1");
            map.put("deleted","0");
            List<Map<String,Object>> allList= permissionMapper.findMap(new PageBounds(),map);

            Map<String,String> idMap=new HashMap<String,String>();
            //替换新的id
            for(Map<String,Object> m:allList){
                m.put("orgid",newOrgid);
                String id=(String)m.get("id");
                String newId= Guid.get();
                newKeyList.add(newId);
                idMap.put(id,newId);
                m.put("id",newId);
            }
            //替换新的parentid
            for(Map<String,Object> m:allList){
                String parentid=(String)m.get("parentid");
                if(!"0".equals(parentid)){
                    m.put("parentid",idMap.get(parentid));
                }
            }
            Permission permission=null;
            List<Permission> permissionList=new ArrayList<Permission>();
            for(Map<String,Object> m:allList){
                permission= JSON.parseObject(JSON.toJSONString(m), Permission.class);
                permissionList.add(permission);
            }
            permissionMapper.addBatch(permissionList);
            return  newKeyList;
        }catch (Exception ex){
            ex.printStackTrace();
            return  null;
        }
    }

    /**
     * 给角色赋权限
     * @param pers 权限ids
     * @param jsid 角色id
     */
    @Transactional
    public boolean addRolePermissions(List<String> pers,String jsid){
        try{
            for(String id:pers){
                permissionMapper.addRolePermission(id,jsid);
            }
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public void onAdd(Object id) {
        Permission permission=permissionMapper.get(String.valueOf(id));
        String orgid=MyBatisRequestUtil.getLoginUserInfo().getOrgid();
        List<Permission> permissions =(List<Permission>)EhCacheUtils.getSysInfo("permissions", orgid);
        permissions.add(permission);
        EhCacheUtils.putSysInfo("permissions", orgid, permissions);
    }

    @Override
    public void onUpdate(Object id) {
        onDelete(id);
        onAdd(id);
    }

    @Override
    public void onDelete(Object id) {
        String orgid=MyBatisRequestUtil.getLoginUserInfo().getOrgid();
        List<Permission> permissions =(List<Permission>)EhCacheUtils.getSysInfo("permissions", orgid);
        permissions=permissions.stream().filter(p->{
            return !p.getId().equals(String.valueOf(id));
        }).collect(Collectors.toList());
        EhCacheUtils.putSysInfo("permissions", orgid, permissions);
    }
    private static final String tableName="sys_permission";
    //系统启动后缓存所有权限
    @Override
    public void onInit() {

            logger.info("缓存表["+tableName+"]的信息：");
            Map queryMap=new HashMap<String,Object>();
            String orgid="0";
            queryMap.put("orgid",orgid);
            queryMap.put("deleted","0");
            List<Permission> permissions=permissionMapper.find(new PageBounds(),null);
            EhCacheUtils.putSysInfo("permissions", orgid, permissions);

    }
}
