package com.makbro.core.base.system.web;


import com.makbro.core.base.system.service.CacheMonitorService;
import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresRoles;
import com.makbro.core.framework.BaseController;
import com.markbro.base.utils.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 缓存监控
 * Created by Administrator on 2015/10/30.
 */
@Controller
@RequestMapping("/sys/cache")
@RequiresRoles(value = {"system","admin"},logical = Logical.OR )
public class CacheMonitorController extends BaseController {
    @Autowired
    CacheMonitorService cacheMonitorService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/sys/cache/list";
    }

    @ResponseBody
    @RequestMapping("/json/sysCacheState")
    public Object sysCacheState(){
        return cacheMonitorService.sysCacheState();
    }
    @ResponseBody
    @RequestMapping("/json/userCacheState")
    public Object userCacheState(){
        return cacheMonitorService.userCacheState();
    }

  @ResponseBody
    @RequestMapping("/json/getSysCacheByKey")
    public Object getSysCacheByKey(HttpServletRequest req){
        Map map= ServletUtil.getMap(req);
        return cacheMonitorService.getSysCacheByKey(map);
    }

    @ResponseBody
    @RequestMapping("/json/getCacheInfo")
    public Object getCacheInfo(HttpServletRequest req){
        Map map= ServletUtil.getMap(req);
        return cacheMonitorService.getCacheInfo(map);
    }

    /**
     * 清除缓存信息
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping("/json/removeCacheByKey")
    @RequiresRoles("system")
    public Object removeCacheByKey(HttpServletRequest req){
        Map map= ServletUtil.getMap(req);
        return cacheMonitorService.removeCacheByKey(map);
    }

    /**
     * 获得指定key的缓存信息
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping("/json/viewCacheInfoByKey")
    public Object viewCacheInfoByKey(HttpServletRequest req){
        Map map= ServletUtil.getMap(req);
        return cacheMonitorService.viewCacheInfoByKey(map);
    }
    @RequestMapping("/viewCacheInfoByKey")
    public String viewCacheInfo(HttpServletRequest req,Model model){
        Map map= ServletUtil.getMap(req);
        Map<String,Object> info= cacheMonitorService.viewCacheInfoByKey(map);
        model.addAttribute("info",info);
        return "/sys/cache/viewInfo";
    }

}
