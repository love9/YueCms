package com.makbro.core.base.system.web;


import com.alibaba.fastjson.JSON;
import com.makbro.core.base.system.backup.BackupFactory;
import com.makbro.core.base.system.backup.IBackup;
import com.makbro.core.base.system.service.DataBaseService;
import com.makbro.core.framework.authz.annotation.RequiresAnonymous;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.*;

/**
 * 数据库备份
 * Created by zengchao on 2017-03-17.
 */
@Controller
@RequestMapping("/sys/backup")
public class BackupController extends BaseController {


    @Autowired
    private DataBaseService dataBaseService;

    /**
     * 数据库备份首页
     * 1. 获取当前系统连接数据库的类型、用户名密码等信息
     * 2. 检测当前数据库类型是否可以支持备份操作（系统是否支持，备份程序是否可用）
     * 3. 获取表
     * @param model
     * @return
     */
    @RequestMapping(value={"","/"})
    public String index(Model model){
        String catalog = dataBaseService.getCatalog();
        List<String> tableList1 = dataBaseService.getTables();
        List<Map<String,Object>> tableList=new ArrayList<Map<String,Object>>();
        Map<String,Object> temp=new HashMap<String, Object>();
        temp.put("name","数据库:"+catalog);
        tableList.add(temp);

        for(String s:tableList1){
            temp=new HashMap<String, Object>();
            temp.put("name",s);
            tableList.add(temp);
        }

        model.addAttribute("catalog", catalog);
        model.addAttribute("count", tableList.size()-1);
        model.addAttribute("tableList", JSON.toJSON(tableList));
        IBackup backup = createBackup();
        if(backup == null){
            Msg msgObj =   Msg.error("当前系统使用数据库不支持备份");
            model.addAttribute("msgObj",msgObj);
        }else if(!backup.canBackup()){
            Msg msgObj =   Msg.error("当前备份程序不可用，请设置备份程序路径后点击");
            model.addAttribute("msgObj",msgObj);
        }else{
            Msg msgObj =   Msg.success("查询成功!");
            model.addAttribute("msgObj",msgObj);
        }

        return "/sys/backup/index";
    }

    /**
     * 配置路径
     * @param path 执行文件路径
     * @return
     */
    /*  @RequestMapping("config")
    @ResponseBody
  public JSONObject config(String path){
        JRelaxSystemConfigHelper.put("system.backup.exec", path);
        IBackup backup = createBackup();
        if(backup != null && backup.canBackup()){
            configService.save("system.backup.exec", path);
        }
        return WebResult.success();
    }*/

    /**
     * 备份整库或者单表
     * @return
     */
    @RequestMapping("/backup")
    @ResponseBody
    public Object backup(){
        IBackup backup = createBackup();
        Map map= MyBatisRequestUtil.getMap(request);
        String table=String.valueOf(map.get("table"));
        if(backup == null){
            return Msg.error("当前系统使用数据库不支持备份");
        }else if(!backup.canBackup()){
            return Msg.error("当前备份程序不可用，请设置备份程序路径后点击");
        }else{
            if(StringUtils.isEmpty(table)) {
                backup.backup();
            } else {
                backup.backup(table);
            }
        }
        return Msg.success("备份成功");
    }

    /**
     * 查看备份文件列表
     * @param table 表名，为null时 删除库备份文件
     * @return
     */
    @RequestMapping("/backUpFileList")
    public String list(Model model, String table){
        List<String> list = new ArrayList<>();
        IBackup backup = createBackup();
        if(backup != null && backup.canBackup()){
            if(StringUtils.isEmpty(table)) {
                list = backup.list();
            } else {
                list = backup.list(table);
            }
        }
        List<Map<String,Object>> fileList=new ArrayList<Map<String,Object>>();
        Map<String,Object> temp=new HashMap<String, Object>();
        for(String s:list){
            temp=new HashMap<String, Object>();
            temp.put("name",s);
            fileList.add(temp);
        }
        model.addAttribute("fileList", JSON.toJSON(fileList));
        model.addAttribute("table", table);
        return "/sys/backup/backUpFileList";
    }
    @RequestMapping("/test")
    @ResponseBody
    @RequiresAnonymous
    public void test(){
       dataBaseService.onInit();
    }
    /**
     * 删除备份文件
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(){
        IBackup backup = createBackup();
        Map map=MyBatisRequestUtil.getMap(request);
        String table=String.valueOf(map.get("table"));
        String name=String.valueOf(map.get("name"));
        File file = null;
        if(backup != null && backup.canBackup()){
            if(StringUtils.isEmpty(table)) {
                file = backup.getFile(name);
            } else {
                file = backup.getFile(table, name);
            }
        }
        if(file != null && file.exists()){
            if(!file.delete()){
                return Msg.error("备份文件删除失败！");
            }
        }else{
            return Msg.error("备份文件不存在");
        }
        return Msg.success("备份文件删除成功！");
    }

    private static final String DB_TYPE="type";
    private IBackup createBackup(){

        IBackup backup = null;
        Map<String, String> dbInfoMap = dataBaseService.getDBInfo();
        String type = dbInfoMap.get(DB_TYPE);

        Properties config = new Properties();
        config.setProperty("url", GlobalConfig.getConfig("system.backup.url"));
        config.setProperty("username", GlobalConfig.getConfig("system.backup.username"));
        config.setProperty("password",GlobalConfig.getConfig("system.backup.password"));

        backup = BackupFactory.create(type);
        if(backup != null){
            backup.setProperties(config);
            backup.setExecPath(GlobalConfig.getConfig("system.backup.exec"));
            backup.setSavePath(GlobalConfig.getConfig("system.backup.savePath"));
        }
        return backup;
    }
}
