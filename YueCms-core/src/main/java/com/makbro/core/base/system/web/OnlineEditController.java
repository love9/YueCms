package com.makbro.core.base.system.web;

import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.common.util.ProjectUtil;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.file.FileUtil;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 页面在线编辑
 */
@Controller
public class OnlineEditController extends BaseController {

    /**
     * 新版在线编辑模板文件
     * @return
     */
    @RequestMapping(value={"/sys/onlineEdit"})
    public String onlineEdit(Model model)throws Exception{
        //列出文件名称
        String basePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("template.basePath");//模板文件夹路径
        List<String> rootFolderList=new ArrayList<String>();
        List<String> files =new ArrayList<String>();
        Map fileTree=new HashMap<String,Object>();

        List<Map<String,Object>> rootFolders =new ArrayList<Map<String,Object>>();
        fileTree.put("rootFiles",files);

        Files.list(Paths.get(basePath))
                .map(path -> path.getFileName().toString()).forEach(s->{
            if(s.contains(".")){
                files.add(s);
            }else{
                rootFolderList.add(s);
            }
        });

        if(CollectionUtils.isNotEmpty(rootFolderList)){
            Map temp=null;
            Map temp2=null;
            Map temp3=null;Map temp4=null;
            String path="";String path2="";String path3="";String path4="";
            for(String s:rootFolderList){
                temp=new HashMap<String,Object>();
                path=basePath+ File.separator+s;

                //文件
                List<String> fs=Files.list(Paths.get(path)) .map(p -> p.getFileName().toString()).filter(p -> p.contains(".")).collect(Collectors.toList());
                temp.put("files",fs);
                temp.put("name",s);
                //文件夹
                File[] folders= FileUtil.getFolders(path);
                List<Map<String,String>> folderList=new ArrayList<>();
                for(File file:folders){
                    temp2=new HashMap<String,Object>();
                    temp2.put("name",file.getName());

                    path2=path+ File.separator+file.getName();
                    //文件
                    List<String> fs2=Files.list(Paths.get(path2)) .map(p -> p.getFileName().toString()).filter(p -> p.contains(".")).collect(Collectors.toList());
                    temp2.put("files",fs2);
                    //文件夹
                    File[] folders2=FileUtil.getFolders(path2);
                    List<Map<String,String>> folderList2=new ArrayList<>();
                    for(File f3:folders2){
                        temp3=new HashMap<String,Object>();
                        temp3.put("name",f3.getName());

                        path3=path2+ File.separator+f3.getName();
                        //文件
                        List<String> fs3=Files.list(Paths.get(path3)) .map(p -> p.getFileName().toString()).filter(p -> p.contains(".")).collect(Collectors.toList());
                        temp3.put("files",fs3);
                        //文件夹
                        File[] folders3=FileUtil.getFolders(path3);
                        List<Map<String,String>> folderList3=new ArrayList<>();
                        for(File f4:folders3){
                            temp4=new HashMap<String,Object>();
                            temp4.put("name",f4.getName());

                            path4=path3+ File.separator+f4.getName();
                            //文件
                            List<String> fs4=Files.list(Paths.get(path3)) .map(p -> p.getFileName().toString()).filter(p -> p.contains(".")).collect(Collectors.toList());
                            temp3.put("files",fs4);
                            //文件夹
                            //File[] folders4=FileUtil.getFolders(path4);


                            folderList3.add(temp4);
                        }

                        temp3.put("folders",folderList3);

                        folderList2.add(temp3);
                    }

                    temp2.put("folders",folderList2);
                    folderList.add(temp2);
                }
                temp.put("folders",folderList);
                rootFolders.add(temp);
            }
        }
        fileTree.put("rootFolders",rootFolders);//认为根目录下只有一层文件夹
        model.addAttribute("fileTree",fileTree);
        return "/sys/onlineEdit/onlineEdit";
    }
    @ResponseBody
    @RequestMapping(value="/sys/json/getFileContent",method = RequestMethod.POST)
    public Object getFileContent() throws Exception{
        Map map= MyBatisRequestUtil.getMap(request);
        String folderName=String.valueOf(map.get("folderName"));
        folderName=folderName.replaceAll("#","/");
        String fileName=String.valueOf(map.get("fileName"));
        if(StringUtil.isEmpty(fileName)){
            return "";
        }
        String basePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("template.basePath");
        //basePath=basePath.replaceAll("//","/")+"/WEB-INF/pages/moban";//模板文件夹路径
        if(StringUtil.notEmpty(folderName)){
            basePath=basePath+File.separator+folderName;
        }
        //读取文件内容
        String filePath  = basePath + File.separatorChar + fileName;
        String content   = Files.readAllLines(Paths.get(filePath)).stream().collect(Collectors.joining("\n"));
        return content;
    }

    @ResponseBody
    @RequestMapping("/sys/json/saveFileContent")
    public Object saveFileContent() throws Exception{
        Map map=MyBatisRequestUtil.getMap(request);

        Msg msg=new Msg();
        msg.setType(Msg.MsgType.success);
        String folderName=String.valueOf(map.get("folderName"));
        folderName=folderName.replaceAll("#","/");

        String fileName=String.valueOf(map.get("fileName"));
        String content=String.valueOf(map.get("content"));
        if(StringUtil.isEmpty(fileName)){
            msg.setType(Msg.MsgType.error);
            msg.setContent("参数fileName不能为空!");
            return msg;
        }
        String basePath= ProjectUtil.getProjectPath()+ GlobalConfig.getConfig("template.basePath");
        //basePath=basePath.replaceAll("//","/")+"/WEB-INF/pages/moban";//模板文件夹路径
        if(StringUtil.notEmpty(folderName)){
            basePath=basePath+File.separator+folderName;
        }

        String filePath  = basePath + File.separatorChar + fileName;//文件路径
        if (Files.exists(Paths.get(filePath))) {
            byte[] rf_wiki_byte = content.getBytes("UTF-8");
            Files.write(Paths.get(filePath), rf_wiki_byte);
        } else {
            Files.createFile(Paths.get(filePath));
            byte[] rf_wiki_byte = content.getBytes("UTF-8");
            Files.write(Paths.get(filePath), rf_wiki_byte);
        }
        msg.setContent("文件["+fileName+"]保存成功!");
        return msg;
    }
}
