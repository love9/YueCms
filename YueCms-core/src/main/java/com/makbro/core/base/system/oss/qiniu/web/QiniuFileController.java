package com.makbro.core.base.system.oss.qiniu.web;

import com.makbro.core.base.system.oss.qiniu.bean.QiniuFile;
import com.makbro.core.base.system.oss.qiniu.service.QiniuFileService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.annotation.DataScope;
import com.markbro.base.common.util.Guid;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.fileupload.util.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 七牛文件资源管理
 * @author  wujiyue on 2018-06-26 17:49:09.
 */

@Controller
@RequestMapping("/sys/oss/qiniu")
public class QiniuFileController extends BaseController {
    @Autowired
    protected QiniuFileService qiniuFileService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/sys/oss/qiniu/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(QiniuFile qiniuFile, Model model){
                return "/sys/oss/qiniu/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(QiniuFile qiniuFile,Model model){
        if(qiniuFile!=null&&qiniuFile.getId()!=null){
            qiniuFile=qiniuFileService.get(qiniuFile.getId());
        }
         model.addAttribute("qiniuFile",qiniuFile);
         return "/sys/oss/qiniu/edit";
    }
    //-----------json数据接口--------------------
    @RequiresPermissions("oss.qiniu")
    @ResponseBody
    @RequestMapping(value = "/json/downloadFromQiniu")
    public Object downloadFromQiniu() {
        Map map= MyBatisRequestUtil.getMap(request);
        return qiniuFileService.downloadFromQiniu(map);
    }

    @RequiresPermissions("oss.qiniu")
    @ResponseBody
    @RequestMapping(value = "/json/qiniuUpload")
    public Object qiniuUpload() {
        Map map= MyBatisRequestUtil.getMap(request);
        return qiniuFileService.qiniuUpload(map);
    }

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return qiniuFileService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    @DataScope
    public Object find() {
        return qiniuFileService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("file");
        if(multipartFile != null){
            Map  upMap = this.uploadSingle(map, request, multipartFile);
            if(upMap!=null){
                String abpath=String.valueOf(upMap.get("abpath"));
                if(StringUtil.notEmpty(abpath)){
                    map.put("localpath",abpath);
                }
            }
        }
        return qiniuFileService.save(map);
    }
    public Map<String, Object> uploadSingle(Map<String, Object> map, HttpServletRequest req, MultipartFile mFile) {
        Map<String, Object> rmap=new HashMap<String, Object>();
        String basePathType = GlobalConfig.getSysPara("uploadpath_type","0");
        String basepath = "";
        String relativePath = "";
        if ("1".equals(basePathType)) {//绝对路径类型
            basepath = GlobalConfig.getSysPara("uploadfile_basepath","D:/dzd");
            relativePath = "/uploadresources";
        } else {
            basepath = req.getSession().getServletContext().getRealPath("/");
            relativePath = GlobalConfig.getSysPara("uploadfile_basepath", "/resources/static/images");
        }
        relativePath= StringUtil.subEndStr(relativePath,"/");
        File dirFile=new File(basepath+relativePath);
        if(!dirFile.exists()){
            dirFile.mkdirs();
        }
        try {
            long size = 0;
            String path = "";
            String filename = "";
            String name = "";
            String dir = "";
            String suffixes ="";
            String guid= Guid.get();
            if (!mFile.isEmpty()) {
                BufferedInputStream in = new BufferedInputStream(mFile.getInputStream());
                filename = mFile.getOriginalFilename();
                name=filename.substring(0,filename.indexOf("."));
                // 取得文件后缀
                suffixes = filename.substring(filename.lastIndexOf("."), filename.length());
                path= relativePath+"/"+ DateUtil.formatDate(new Date(),"yyyyMMdd")+"/"+guid + suffixes;//相对路径
                dir = basepath+File.separatorChar+path;// 绝对路径
                String tempDir=dir.substring(0,dir.lastIndexOf("/"));
                File tempDirFile=new File(tempDir);
                if(!tempDirFile.exists()){
                    tempDirFile.mkdirs();
                }
                File ffout = new File(dir);
                rmap.put("abpath", ffout.getCanonicalPath());//服务器绝对路径
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(ffout));
                Streams.copy(in, out, true);
                size = ffout.length();
            }

            rmap.put("path", path);
            rmap.put("name", name);
            rmap.put("suffixes", suffixes);
            rmap.put("size", size);

        }catch (IOException e) {
            e.printStackTrace();
        }
        return rmap;
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable Integer id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            qiniuFileService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}

	/**
    * 批量逻辑删除的数据
    */
    @ResponseBody
    @RequestMapping("/json/removes/{ids}")
    public Object removes(@PathVariable String[] ids){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            qiniuFileService.updateByMapBatch(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除七牛文件资源")
    public Object delete(@PathVariable String id) {
    	try{
            qiniuFileService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }

    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除七牛文件资源")
    public Object deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
    	    qiniuFileService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
}