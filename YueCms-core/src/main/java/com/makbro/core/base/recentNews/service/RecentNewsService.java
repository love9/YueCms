package com.makbro.core.base.recentNews.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.recentNews.bean.RecentNews;
import com.makbro.core.base.recentNews.dao.RecentNewsMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 最新动态 Service
 * @author  wujiyue on 2017-08-29 13:30:56.
 */
@Service
public class RecentNewsService{

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private RecentNewsMapper recentNewsMapper;

     /*基础公共方法*/
    public RecentNews get(Integer id){
        return recentNewsMapper.get(id);
    }

    public List<RecentNews> find(PageBounds pageBounds,Map<String,Object> map){
        List<RecentNews> list=recentNewsMapper.findByMap(pageBounds,map);
        try {
            list.stream().forEach(recentNews -> {
                Date d1 = DateUtils.parseDate(recentNews.getCreateTime());
                String s= DateUtil.simpleformatIntervalTime(DateUtil.now(),d1);
                recentNews.setElapse(s);
            });
        }catch (Exception ex){
        }
        return list;
    }
    public List<RecentNews> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return recentNewsMapper.find(pageBounds,map);
    }

    public void add(RecentNews recentNews){
        recentNewsMapper.add(recentNews);
    }
    public Object save(Map<String,Object> map){
        RecentNews recentNews= JSON.parseObject(JSON.toJSONString(map),RecentNews.class);
        if(recentNews.getId()==null||"".equals(recentNews.getId().toString())){
            Integer id= keyService.getIntegerId();
            recentNews.setId(id);
            recentNewsMapper.add(recentNews);
        }else{
            recentNewsMapper.update(recentNews);
        }
        return Msg.success("保存信息成功");
    }
    public void addBatch(List<RecentNews> recentNewss){
        recentNewsMapper.addBatch(recentNewss);
    }

    public void update(RecentNews recentNews){
        recentNewsMapper.update(recentNews);
    }

    public void updateByMap(Map<String,Object> map){
        recentNewsMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        recentNewsMapper.updateByMapBatch(map);
    }
    public void delete(Integer id){
        recentNewsMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        recentNewsMapper.deleteBatch(ids);
    }
     /*自定义方法*/
     public Object readMsg(Map<String,Object> map){
         Msg msg=new Msg();
         try{
             String id=String.valueOf(map.get("id"));
             RecentNews recentNews=recentNewsMapper.get(Integer.valueOf(id));
            //该消息设置为已读
             RecentNews up=new RecentNews();
             up.setId(Integer.valueOf(id));
             up.setReadflag("1");
             recentNewsMapper.update(up);
             msg.setType(Msg.MsgType.success);
             msg.setContent("成功");
             msg.setExtend("recentNews",recentNews);
         }catch (Exception ex){
             msg.setType(Msg.MsgType.error);
             msg.setContent("失败");
         }
         return msg;
     }

}
