package com.makbro.core.base.smsHis.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.smsHis.bean.SmsHis;
import com.makbro.core.base.smsHis.dao.SmsHisMapper;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.model.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 短信发送记录 Service
 * @author wujiyue
 * @date 2018-11-06 16:08:03
 */
@Service
public class SmsHisService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TableKeyService keyService;
    @Autowired
    private SmsHisMapper smsHisMapper;

     /*基础公共方法*/
    public SmsHis get(Integer id){
        return smsHisMapper.get(id);
    }
    public List<SmsHis> find(PageBounds pageBounds,Map<String,Object> map){
        return smsHisMapper.find(pageBounds,map);
    }
    public List<SmsHis> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return smsHisMapper.findByMap(pageBounds,map);
    }
    public void add(SmsHis smsHis){
        smsHisMapper.add(smsHis);
    }
    public Object save(Map<String,Object> map){
        Msg msg=new Msg();
        SmsHis smsHis= JSON.parseObject(JSON.toJSONString(map), SmsHis.class);
        if(smsHis.getId()==null||"".equals(smsHis.getId().toString())){
        Integer id= keyService.getIntegerId();

        smsHis.setId(id);
        smsHisMapper.add(smsHis);
       }else{
         smsHisMapper.update(smsHis);
       }
        msg.setType(Msg.MsgType.success);
            msg.setContent("保存信息成功");
            return msg;
    }
    public void update(SmsHis smsHis){
        smsHisMapper.update(smsHis);
    }

    public void updateByMap(Map<String,Object> map){
        smsHisMapper.updateByMap(map);
    }

    public void delete(Integer id){
        smsHisMapper.delete(id);
    }

    public void deleteBatch(Integer[] ids){
        smsHisMapper.deleteBatch(ids);
    }



     /*自定义方法*/

}
