package com.makbro.core.base.system.oss.mimeType.bean;


import com.markbro.base.model.AliasModel;

/**
 * 文件类型 bean
 * @author  wujiyue on 2018-06-27 09:33:52 .
 */
public class MimeType  implements AliasModel {

	private Integer id;//
	private String mimeType;//
	private String suffix;//后缀
	private String name;//中文名称
	private String icon;//图标


	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getMimeType(){ return mimeType ;}
	public void  setMimeType(String mimeType){this.mimeType=mimeType; }
	public String getSuffix(){ return suffix ;}
	public void  setSuffix(String suffix){this.suffix=suffix; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public String getIcon(){ return icon ;}
	public void  setIcon(String icon){this.icon=icon; }


	@Override
	public String toString() {
	return "MimeType{" +
			"id=" + id+
			", mimeType=" + mimeType+
			", suffix=" + suffix+
			", name=" + name+
			", icon=" + icon+
			 '}';
	}
}
