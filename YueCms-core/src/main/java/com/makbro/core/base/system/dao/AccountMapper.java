package com.makbro.core.base.system.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 账户相关
 */
@Repository
public interface AccountMapper {


    //第三方登录检测 账户名是否已经存在
    public Integer checkAccountAndPwd(@Param("account") String account, @Param("password") String password);
    public Integer checkAccount(String account);

    //查询openid是否绑定过本系统用户
    public Map<String,Object> getThirdOauthByOpenid(String openid);
    //绑定用户，插入第三方授权表sys_third_oauth
    public void insertThirdOauth(Map<String, Object> map);

    //解绑用户，删除第三方授权表.传入解绑用户的yhid和要解绑的类型。qq,weixin,sina
    public void deleteThirdOauth(@Param("yhid") String yhid, @Param("login_type") String login_type);

    //查询用户的所有第三方授权绑定记录
    public List<Map<String,Object>> findThirdOauthByYhid(String yhid);
    //个人主页获得登陆日志条数
    public int getLoginLogCount(@Param("yhid") String yhid);
}
