package com.makbro.core.base.area.bean;


import com.markbro.base.model.AliasModel;

/**
 * 区域 bean
 * @author  wujiyue on 2016-07-17 01:33:46.
 */
public class Area  implements AliasModel {
	private String id;//
	private String parentid;//父级编号
	private String parentids;//所有父级编号
	private String code;//区域编码
	private String name;//区域名称
	private Integer sort;//排序
	private String areatype;//区域类型
	private Integer createBy;//
	private String createTime;//
	private Integer updateBy;//
	private String updateTime;//
	private Integer available;//
	private Integer deleted;//

	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getParentid(){ return parentid ;}
	public void  setParentid(String parentid){this.parentid=parentid; }
	public String getParentids(){ return parentids ;}
	public void  setParentids(String parentids){this.parentids=parentids; }
	public String getCode(){ return code ;}
	public void  setCode(String code){this.code=code; }
	public String getName(){ return name ;}
	public void  setName(String name){this.name=name; }
	public Integer getSort(){ return sort ;}
	public void  setSort(Integer sort){this.sort=sort; }
	public String getAreatype(){ return areatype ;}
	public void  setAreatype(String areatype){this.areatype=areatype; }
	public Integer getCreateBy(){ return createBy ;}
	public void  setCreateBy(Integer createBy){this.createBy=createBy; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public Integer getUpdateBy(){ return updateBy ;}
	public void  setUpdateBy(Integer updateBy){this.updateBy=updateBy; }
	public String getUpdateTime(){ return updateTime ;}
	public void  setUpdateTime(String updateTime){this.updateTime=updateTime; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }

	@Override
	public String toString() {
	return "Area{" +
			"id=" + id+
			", parentid=" + parentid+
			", parentids=" + parentids+
			", code=" + code+
			", name=" + name+
			", sort=" + sort+
			", areatype=" + areatype+
			", createBy=" + createBy+
			", createTime=" + createTime+
			", updateBy=" + updateBy+
			", updateTime=" + updateTime+
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
