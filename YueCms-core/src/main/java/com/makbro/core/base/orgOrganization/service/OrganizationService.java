package com.makbro.core.base.orgOrganization.service;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.orgDepartment.bean.Department;
import com.makbro.core.base.orgDepartment.dao.DepartmentMapper;
import com.makbro.core.base.orgDepartment.service.DepartmentService;
import com.makbro.core.base.orgOrganization.bean.Organization;
import com.makbro.core.base.orgOrganization.dao.OrganizationMapper;
import com.makbro.core.base.orgPosition.bean.Position;
import com.makbro.core.base.orgPosition.dao.PositionMapper;
import com.makbro.core.base.orgPosition.service.PositionService;
import com.makbro.core.base.orgRole.bean.Role;
import com.makbro.core.base.orgRole.dao.RoleMapper;
import com.makbro.core.base.orgRole.service.RoleService;
import com.makbro.core.base.orgTree.bean.OrgTree;
import com.makbro.core.base.orgTree.dao.OrgTreeMapper;
import com.makbro.core.base.orgUser.bean.OrgUser;
import com.makbro.core.base.orgUser.dao.OrgUserMapper;
import com.makbro.core.base.orgUser.service.OrgUserService;
import com.makbro.core.base.permission.service.PermissionService;
import com.makbro.core.base.tablekey.service.TableKeyService;
import com.markbro.base.common.excel.ExcelUtils;
import com.markbro.base.common.util.Guid;
import com.markbro.base.common.util.Pinyin;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.exception.ApplicationException;
import com.markbro.base.model.Msg;
import com.markbro.base.utils.date.DateUtils;
import com.markbro.base.utils.string.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

/**
 * 组织机构 service
 * Created by wujiyue on 2016-07-18 22:52:36.
 */
@Service
public class OrganizationService{
    protected Logger log = LoggerFactory.getLogger(getClass());
    /*@Autowired
    private MyWebsocketHandler handler;*/
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private OrganizationMapper organizationMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private PositionMapper positionMapper;
    @Autowired
    private PositionService positionService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    private OrgTreeMapper orgTreeMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleService roleService;
    @Autowired
    private OrgUserMapper orgUserMapper;
    @Autowired
    private OrgUserService orgUserService;
    @Autowired
    private TableKeyService keyService;
    @Autowired
    private TableKeyService bmKeyService;
     /*基础公共方法*/
    public Organization get(String id){
        return organizationMapper.get(id);
    }
    public Map<String,Object> getMap(String id){
        return organizationMapper.getMap(id);
    }
    public List<Organization> find(PageBounds pageBounds, Map<String,Object> map){
        return organizationMapper.find(pageBounds,map);
    }
    public List<Organization> findByMap(PageBounds pageBounds,Map<String,Object> map){
        return organizationMapper.findByMap(pageBounds,map);
    }
    public void add(Organization organization){
        organizationMapper.add(organization);
    }

    public void pushSysMessage(Map<String,Object> map,String msg){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info(msg+"\tparams:"+JSON.toJSONString(map));
        String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
            /*MyMessage mymsg=new MyMessage();
            mymsg.setFromYhid("1");
            mymsg.setFromName("系统");
            mymsg.setToYhid(yhid);
            String xm= String.valueOf(EhCacheUtils.getUserInfo(TmConstant.XM_KEY, yhid));
            mymsg.setToName(xm);
            mymsg.setText(msg);
            handler.sendMessageToUser(yhid, new TextMessage(new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create().toJson(mymsg)));*/
    }
    @Transactional
    public Object save(Map<String,Object> map){
          Msg msg=new Msg();
          String errorMsg="新建组织失败！";
        try{
            Organization organization = JSON.parseObject(JSON.toJSONString(map),Organization.class);
            if(organization.getId()==null||"".equals(organization.getId().toString())){

                pushSysMessage(map,"系统正新建组织...");

                String orgid= keyService.getStringId();
                organization.setId(orgid);
                organizationMapper.add(organization);

                pushSysMessage(map,"新建组织完成，准备新建部门...");

                //新增组织后同时往部门表和组织树表新增一条同名称的部门记录

                Department department=new Department();
                department.setId(bmKeyService.getStringId());
                department.setAvailable(1);
                department.setDeleted(0);
                department.setName(organization.getName());
                department.setOrgid(orgid);
                department.setParentid("0");
                department.setParentids("0,"+department.getId()+",");
                department.setSort(1);
                department.setFax(organization.getFax());
                department.setPhone(organization.getPhone());
                departmentMapper.add(department);
                //创建2个下属部门作为测试数据（部门一和部门二）
                Department department_1=new Department();
                department_1.setId(bmKeyService.getStringId());
                department_1.setAvailable(1);
                department_1.setDeleted(0);
                department_1.setName("部门一");
                department_1.setOrgid(orgid);
                department_1.setParentid(department.getId());
                department_1.setParentids("0," + department.getId() + ","+department_1.getId()+",");
                department_1.setSort(1);
                department_1.setFax(organization.getFax());
                department_1.setPhone(organization.getPhone());
                departmentMapper.add(department_1);
                Department department_2=new Department();
                department_2.setId(bmKeyService.getStringId());
                department_2.setAvailable(1);
                department_2.setDeleted(0);
                department_2.setName("部门二");
                department_2.setOrgid(orgid);
                department_2.setParentid(department.getId());
                department_2.setParentids("0," + department.getId() + ","+department_2.getId()+",");
                department_2.setSort(2);
                department_2.setFax(organization.getFax());
                department_2.setPhone(organization.getPhone());
                departmentMapper.add(department_2);
                pushSysMessage(map,"新建部门完成，准备新建默认管理员角色...");
                //新增该组织的管理员角色
                Role role=new Role();
                role.setId(keyService.getStringId());
                role.setOrgid(orgid);
                role.setAvailable(1);
                role.setDeleted(0);
                role.setDescription(organization.getName() + "超级管理员角色");
                role.setName("admin");
                role.setLoginpage("index_developer");
                role.setMainpage("/main_developer");
                roleMapper.add(role);

                pushSysMessage(map,"新建角色完成，准备新建默认管理员用户...");
                //给该组织增加默认的管理员用户
                OrgUser user=new OrgUser();
                String userId=keyService.getStringId();
                user.setId(userId);
                String account= Pinyin.cn2Spell(organization.getName());
                user.setAccount(account);
                user.setOrgid(orgid);
                user.setPassword("1");
                user.setUsertype("1");
                user.setAge(18);
                user.setNickname(organization.getName());
                user.setRealname(organization.getName());
                user.setGender("0");
                user.setCreatetype("0");
                user.setLoginCount(0);
                user.setHeaderPath("\\resources\\public\\images\\portraits\\5.jpg");
                user.setAvailable(1);
                user.setDeleted(0);
                orgUserMapper.add(user);
                //给用户赋予该管理员角色
                roleMapper.addUserRole(user.getId(),role.getId());

                pushSysMessage(map, "新建管理员用户完成，准备新建组织目录树...");
                OrgTree tree=new OrgTree();
                tree.setOrgid(orgid);
                tree.setId(Guid.get());
                tree.setParentid("0");
                tree.setType("bm");
                tree.setSjbmid("0");
                tree.setLxid(department.getId());
                tree.setName(department.getName());
                tree.setAvailable(1);
                tree.setDeleted(0);
                orgTreeMapper.add(tree);

                pushSysMessage(map, "新建组织目录树完成，准备角色授权...");
                //将 sys_permission 表的 orgid=0的权限复制一份给该组织
                List<String> idList=permissionService.copyPermissionsToNewOrg(orgid);
                if(idList != null && idList.size()>0){
                 //给角色赋予权限
                    boolean b = permissionService.addRolePermissions(idList,role.getId());
                    if(!b){
                        errorMsg+="\r\n角色授权失败！";
                        throw  new Exception();
                    }
                }else{
                    errorMsg+="\r\n拷贝权限失败！";
                    throw  new Exception();
                }
                pushSysMessage(map, "角色授权完成...");
                Thread.sleep(500);
                msg.setType(Msg.MsgType.success);
                msg.setContent("新建组织成功！");
                pushSysMessage(map, "组织创建完成!");
            }else{
                organizationMapper.update(organization);
                msg.setType(Msg.MsgType.success);
                msg.setContent("更新成功！");
            }

        }catch (Exception ex){
            ex.printStackTrace();
            msg.setType(Msg.MsgType.error);
            errorMsg+="\r\n请联系开发人员！";
            msg.setContent(errorMsg);
        }
        return msg;
    }
    public void addBatch(List<Organization> organizations){
        organizationMapper.addBatch(organizations);
    }
    public void update(Organization organization){
        organizationMapper.update(organization);
    }
    public void updateByMap(Map<String,Object> map){
        organizationMapper.updateByMap(map);
    }
    public void updateByMapBatch(Map<String,Object> map){
        organizationMapper.updateByMapBatch(map);
    }
    public void delete(String id){

        if(!"0".equals(id)){
            organizationMapper.delete(id);//调用存储过程deleteOrg
        }

    }
    public Object deleteBatch(String[] ids){
        Msg msg=new Msg();
        if(StringUtil.isContains("0", Arrays.asList(ids))){
            msg.setType(Msg.MsgType.error);
            msg.setContent("您不能删除系统默认的id=0组织!");
        }else{
            for(String id:ids){
                organizationMapper.delete(id);
            }
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功");
        }
        return msg;
    }

    /**
     * 导入组织目录
     * @param map
     * @return
     */
    @Transactional
    public Object importOrg(Map<String,Object> map){
        Msg msg=new Msg();
        msg.setType(Msg.MsgType.error);
        String orgid=String.valueOf(map.get(TmConstant.ORGID_KEY));
        String bmid=String.valueOf(map.get(TmConstant.BMID_KEY));
        String gwid=String.valueOf(map.get(TmConstant.GWID_KEY));
        String yhid=String.valueOf(map.get(TmConstant.YHID_KEY));
        Map<String,Object> saveDepMap=new HashMap<String,Object>();
        Map<String,Object> saveGwMap=new HashMap<String,Object>();
        Map<String,Object> saveRoleMap=new HashMap<String,Object>();
        Map<String,Object> saveUserMap=new HashMap<String,Object>();
        saveDepMap.put(TmConstant.ORGID_KEY,orgid);
        saveDepMap.put(TmConstant.GWID_KEY,gwid);
        saveDepMap.put(TmConstant.BMID_KEY,bmid);
        saveDepMap.put(TmConstant.YHID_KEY,yhid);
        saveGwMap.putAll(saveDepMap);
        saveRoleMap.putAll(saveDepMap);
        saveUserMap.putAll(saveDepMap);
        try{
           String localpath=String.valueOf(map.get("localpath"));//用户上传的导入组织目录文件本地路径
            File file=new File(localpath);
            if(StringUtil.notEmpty(localpath)&&file.exists()){
                FileInputStream is = new FileInputStream(file);
                HSSFWorkbook wbs = new HSSFWorkbook(is);
                HSSFSheet childSheet = wbs.getSheetAt(0);
                int rowsCount=childSheet.getLastRowNum();
                List<Map<String,String>> infors=new ArrayList<Map<String,String>>();
                Map<String,String> tmap=null;
                //收集excel信息
                for (int j = 2; j <= rowsCount; j++) {//从第三行开始
                    HSSFRow row = childSheet.getRow(j);
                    if (null != row) {
                        tmap=new HashMap<String,String>();
                        //部门信息
                        String sjbm= ExcelUtils.getCellValue(row.getCell(0));tmap.put("sjbm",sjbm);
                        String bmmc= ExcelUtils.getCellValue(row.getCell(1));tmap.put("bmmc",bmmc);
                        String bmdh=ExcelUtils.getCellValue(row.getCell(2));tmap.put("bmdh",bmdh);
                        String bmfax=ExcelUtils.getCellValue(row.getCell(3));tmap.put("bmfax",bmfax);
                        String bmemail=ExcelUtils.getCellValue(row.getCell(4));tmap.put("bmemail",bmemail);
                        String bmbz=ExcelUtils.getCellValue(row.getCell(5));tmap.put("bmbz",bmbz);
                        //岗位信息
                        String gwmc= ExcelUtils.getCellValue(row.getCell(6));tmap.put("gwmc",gwmc);//岗位名称
                        String gwms= ExcelUtils.getCellValue(row.getCell(7));tmap.put("gwms",gwms);//岗位描述
                        //用户信息
                        String account= ExcelUtils.getCellValue(row.getCell(8));tmap.put("account",account);
                        String nickname= ExcelUtils.getCellValue(row.getCell(9));tmap.put("nickname",nickname);
                        String gender= ExcelUtils.getCellValue(row.getCell(10));tmap.put("gender",gender);
                        String birthday= ExcelUtils.getCellValue(row.getCell(11));tmap.put("birthday",birthday);
                        String age= ExcelUtils.getCellValue(row.getCell(12));tmap.put("age",age);
                        String realname= ExcelUtils.getCellValue(row.getCell(13));tmap.put("realname",realname);
                        String idcard= ExcelUtils.getCellValue(row.getCell(14));tmap.put("idcard",idcard);
                        String qq= ExcelUtils.getCellValue(row.getCell(15));tmap.put("qq",qq);
                        String email= ExcelUtils.getCellValue(row.getCell(16));tmap.put("email",email);
                        String phone= ExcelUtils.getCellValue(row.getCell(17));tmap.put("phone",phone);
                        String remark= ExcelUtils.getCellValue(row.getCell(18));tmap.put("remark",remark);
                        //角色信息
                        String jsmc= ExcelUtils.getCellValue(row.getCell(19));tmap.put("jsmc",jsmc);
                        infors.add(tmap);
                    }
                }
                int successRowCount=0;
                String errorMsg="";
                //处理信息
                if(CollectionUtils.isNotEmpty(infors)){
                    int i=0;
                    for(Map<String,String> t:infors){
                        i++;
                        System.out.println(t.toString());
                        String sjbm=t.get("sjbm");
                        String bmmc=t.get("bmmc");
                        String sjbmid="0";
                        if(StringUtil.notEmpty(sjbm)){
                            //检测上级部门是否存在，不存在则创建
                            Department depParent=  departmentMapper.getByNameAndParentidAndOrgid(sjbm, sjbmid, orgid);
                            if(depParent==null){
                                //新建该上级部门
                                saveDepMap.put("parentid","0");
                                saveDepMap.put("name",sjbm);
                                Msg res=(Msg) departmentService.save(saveDepMap);//保存上级部门
                                sjbmid=String.valueOf(msg.getData().get("id"));
                            }else{
                                sjbmid=depParent.getId();
                            }
                        }
                        String gwParentid="";
                        if(StringUtil.notEmpty(bmmc)){
                            Department dep_this=  departmentMapper.getByNameAndParentidAndOrgid(bmmc, sjbmid, orgid);
                            if(dep_this==null){
                                saveDepMap.put("parentid",sjbmid);
                                saveDepMap.put("name",bmmc);
                                saveDepMap.put("phone",t.get("bmdh"));
                                saveDepMap.put("fax",t.get("bmfax"));
                                saveDepMap.put("email",t.get("bmemail"));
                                saveDepMap.put("bz",t.get("bmbz"));
                                Msg res=(Msg) departmentService.save(saveDepMap);//保存本部门
                                gwParentid=String.valueOf(res.getData().get("id"));
                            }else{
                                gwParentid=dep_this.getId();
                            }
                        }

                        //处理岗位
                        String gwmc=t.get("gwmc");
                        if(StringUtil.isEmpty(gwmc)){
                            errorMsg+="第"+(i+2)+"行，岗位名称不能为空!\r\n";
                            continue;
                        }

                        String gwms=t.get("gwms");
                        String this_gwid="";

                        if("".equals(gwParentid)){
                            //说明该岗位直接挂在根组织上
                            //先获得跟组织的id作为岗位的parentid(根据orgid和parentid=0)
                            List<Department> rootDep=departmentMapper.findByParentidAndOrgid("0",orgid);
                            if(CollectionUtils.isEmpty(rootDep)){
                                throw new ApplicationException("导入组织目录异常!该组织根节点为空!");//部门表没有该组织根节点记录
                            }
                            gwParentid=rootDep.get(0).getId();
                        }
                        Position position=positionMapper.getByNameAndParentidAndOrgid(gwmc,gwParentid,orgid);
                        if(position==null){
                            saveGwMap.put("parentid",gwParentid);
                            saveGwMap.put("name",gwmc);
                            saveGwMap.put("description",gwms);
                            Msg res=(Msg)positionService.save(saveGwMap);
                            this_gwid=String.valueOf(res.getData().get("id"));
                        }else{
                            this_gwid=position.getId();
                        }

                        //处理角色
                        String jsid="";
                        String jsmc=t.get("jsmc");
                        String jsbz="通过excel导入，请管理员授权该角色。";
                        Role role =  roleMapper.getByNameAndOrgid(jsmc,orgid);
                        if(role==null){
                            saveRoleMap.put("name",jsmc);
                            saveRoleMap.put("description",jsbz);
                            Msg res=(Msg)roleService.save(saveRoleMap);
                            jsid=String.valueOf(res.getData().get("id"));
                        }else{
                            jsid=role.getId();
                        }

                        //处理用户
                        String account=t.get("account");
                        if(StringUtil.isEmpty(account)){
                            errorMsg+="第"+(i+2)+"行，账户不能为空!\r\n";
                            continue;
                        }
                        String checkYhid = orgUserMapper.getYhidByAccount(account);//检测该用户账户是否存在
                        if(StringUtil.notEmpty(checkYhid)){
                            errorMsg+="账户【"+account+"】在系统中已经存在!\r\n";
                        }else{
                            String nickname=t.get("nickname");
                            String gender=t.get("gender").equals("男")?"1":"0";
                            String birthday=t.get("birthday");
                            String age=t.get("age");
                            String realname=t.get("realname");
                            String idcard=t.get("idcard");
                            String qq=t.get("qq");
                            String email=t.get("email");
                            String phone=t.get("phone");
                            String remark=t.get("remark");
                            saveUserMap.put("nickname",nickname);
                            saveUserMap.put("gender",gender);
                            saveUserMap.put("birthday",birthday);
                            saveUserMap.put("age",age);
                            saveUserMap.put("realname",realname);
                            saveUserMap.put("idcard",idcard);
                            saveUserMap.put("email",email);
                            saveUserMap.put("phone",phone);
                            saveUserMap.put("remark",remark);
                            saveUserMap.put("loginCount","0");
                            saveUserMap.put("headerPath","/resources/images/logo/logo.png");
                            saveUserMap.put("createTime", DateUtils.getDateTime());
                            saveUserMap.put("updateTime", DateUtils.getDateTime());
                            saveUserMap.put("available",1);
                            saveUserMap.put("deleted",0);
                            saveUserMap.put("account",account);
                            saveUserMap.put("password","1");
                            saveUserMap.put("sjgwid",this_gwid);//用户岗位
                            saveUserMap.put("jsid",jsid);//用户角色
                            Msg res = (Msg)orgUserService.save(saveUserMap);
                            if(res.getType()== Msg.MsgType.success){
                                successRowCount++;
                            }
                        }
                    }
                }
                boolean b =  file.delete();
                System.out.println(b);
                msg.setType(Msg.MsgType.success);
                if(StringUtil.notEmpty(errorMsg)){
                    msg.setContent("导入成功!成功导入["+successRowCount+"]条信息!\r\n"+errorMsg);
                }else{
                    msg.setContent("导入成功!成功导入["+successRowCount+"]条信息!");
                }

            }else{
                msg.setContent("导入组织目录失败!上传文件异常!");
            }
        }catch (Exception ex){
            ex.printStackTrace();
            msg.setContent("导入组织目录异常!");
        }
        return msg;
    }

}
