package com.makbro.core.base.system.web;

import com.alibaba.fastjson.JSON;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.markbro.base.annotation.Referer;
import com.makbro.core.framework.BaseController;
import com.markbro.base.common.util.Guid;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.date.DateUtil;
import com.markbro.base.utils.string.StringUtil;
import com.markbro.thirdapi.baidu.api.BaiduAi;
import com.markbro.thirdapi.baidu.bean.face.FaceDetectResult;
import com.markbro.thirdapi.baidu.bean.imgClassify.AnimalResult;
import com.markbro.thirdapi.baidu.bean.imgClassify.CarResult;
import com.markbro.thirdapi.baidu.bean.imgClassify.DishResult;
import com.markbro.thirdapi.baidu.bean.imgClassify.PlantResult;
import com.markbro.thirdapi.baidu.bean.ocr.*;
import com.markbro.thirdapi.baidu.service.FaceService;
import com.markbro.thirdapi.baidu.service.ImageClassifyService;
import com.markbro.thirdapi.baidu.service.OcrService;
import org.apache.commons.fileupload.util.Streams;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wujiyue
 */
@Controller
@RequestMapping("/ai")
public class AiController extends BaseController {

    @RequestMapping(value={"","/"})
    public String index(){
        return "/sys/ai/index";
    }

    @RequestMapping("/faceDetect")
    public String toFindDeleted(){
        return "/sys/ai/faceDeleted";
    }
    @RequestMapping("/plant")
    public String toPlant(){
        return "/sys/ai/plant";
    }
    @RequestMapping("/bankCard")
    public String toBankCard(){
        return "/sys/ai/bankCard";
    }
    @RequestMapping("/plate")
    public String toPlate(){
        return "/sys/ai/plate";
    }
    @RequestMapping("/idCard")
    public String toIdCard(){
        return "/sys/ai/idCard";
    }
    @RequestMapping("/driver")
    public String toDriver(){
        return "/sys/ai/driver";
    }
    @RequestMapping("/animal")
    public String toAnimal(){
        return "/sys/ai/animal";
    }
    @RequestMapping("/car")
    public String toCar(){
        return "/sys/ai/car";
    }
    @RequestMapping("/dish")
    public String toDish(){
        return "/sys/ai/dish";
    }
    @RequestMapping("/general_basic")
    //通用文字识别
    public String general_basic(){
        return "/sys/ai/general_basic";
    }


    //上传单张图片资源
    @ResponseBody
    @RequestMapping("/uploadSingle")
    @Referer(methodCode = "AiController.uploadSingle")
    @RequiresAuthentication
    public Object uploadSingle() {
        Map resultMap =new HashMap<String,Object>();
        Map map = MyBatisRequestUtil.getMapGuest(request);
        String aiType=String.valueOf(map.get("type"));
        if(StringUtil.isEmpty(aiType)){
            resultMap.put("result","失败!type参数不能为空!");
            return resultMap;
        }
        PrintWriter write = null;
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("file");
        if(multipartFile != null){
            map = uploadSingle(map, request, multipartFile);
        }
        String localPath=String.valueOf(map.get("localPath"));
        resultMap.put("path",String.valueOf(map.get("path")));
        if(BaiduAi.AiType.faceDetect.name().equals(aiType)){
            FaceDetectResult result = FaceService.faceDetect_localPath(localPath);
            if(result.isRequestOk()){
                resultMap.put("result","成功");
                resultMap.put("age",result.getAge());
                resultMap.put("gender","female".equals(result.getGender())?"女":"男");
                resultMap.put("beauty",result.getBeauty());
                resultMap.put("expression",result.getExpression());
                resultMap.put("hasGlasses",result.isHasGlasses()?"有":"无");
            }else{
                resultMap.put("result","失败"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.plant.name().equals(aiType)){
            PlantResult result= ImageClassifyService.plant(localPath);
            if(result.isRequestOk()){
                //请求成功
               if(result.isPlant()){
                   //确定是植物
                   resultMap.put("result","成功");
                   resultMap.put("name",result.getResult_name());
                   resultMap.put("probability",result.getResult_probability());
               }else{
                   //不能判定是否植物
                   resultMap.put("result","非植物");
                   resultMap.put("resultJson", JSON.toJSONString(result.getResult()));
               }
            }else{
                //请求失败
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.bankCard.name().equals(aiType)){
            BankCardOcrResult result = OcrService.bankCardOcr(localPath);
            if(result.isRequestOk()){
                resultMap.put("result","成功");
                resultMap.put("cardNum",result.getBank_card_number());
                resultMap.put("bankName",result.getBank_name());
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.idCard.name().equals(aiType)){
            IdCardOcrResult result = OcrService.idcardOcr_Z(localPath);
            if(result.isRequestOk()){
                resultMap.put("result","成功");
                resultMap.put("gender",result.getGender());
                resultMap.put("address",result.getAddress());
                resultMap.put("birthday",result.getBirthday());
                resultMap.put("ID",result.getID());
                resultMap.put("name",result.getName());
                resultMap.put("minzhu",result.getMinZhu());
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.plate.name().equals(aiType)){
            //识别车牌号
            PlateNumberResult result = OcrService.license_plateOcr(localPath);
            if(result.isRequestOk()){
                resultMap.put("result","成功");
                resultMap.put("number",result.getPlateNumber());
                resultMap.put("color",result.getColor());
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.driver.name().equals(aiType)){
            //驾驶证识别
            DriveLicenseOcrResult result = OcrService.drivingLicenseOcr(localPath);
            if(result.isRequestOk()){
                resultMap.put("result","成功");
                resultMap.put("ID",result.getID());
                resultMap.put("name",result.getName());
                resultMap.put("gender",result.getGender());
                resultMap.put("address",result.getAddress());
                resultMap.put("birthday",result.getBirthday());
                resultMap.put("country",result.getCountry());
                resultMap.put("time",result.getStarttime()+"-"+result.getEndtime());
                resultMap.put("type",result.getType());//C1
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.animal.name().equals(aiType)){
            //动物识别
            AnimalResult result = ImageClassifyService.animal(localPath);
            if(result.isRequestOk()){
                if(result.isAnimal()){
                    resultMap.put("result","成功");
                    resultMap.put("name",result.getResult_name());
                    resultMap.put("probability",result.getResult_probability());
                }else{
                    resultMap.put("result","非动物");
                    resultMap.put("resultJson",JSON.toJSONString(result.getResult()));
                }
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.car.name().equals(aiType)){
            //车型识别
            CarResult result = ImageClassifyService.car(localPath);
            if(result.isRequestOk()){
                if(result.isCar()){
                    resultMap.put("result","成功");
                    resultMap.put("name",result.getResult_name());
                    resultMap.put("probability",result.getResult_probability());
                    resultMap.put("year",result.getResult_year());
                }else{
                    resultMap.put("result","非车辆");
                    resultMap.put("resultJson",JSON.toJSONString(result.getResult()));
                }
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.dish.name().equals(aiType)){
            //菜品识别
            DishResult result = ImageClassifyService.dish(localPath);
            if(result.isRequestOk()){
                if(result.isDish()){
                    resultMap.put("result","成功");
                    resultMap.put("name",result.getResult_name());
                    resultMap.put("probability",result.getResult_probability());
                    resultMap.put("calorie",result.getResult_calorie());
                }else{
                    resultMap.put("result","非菜");
                    resultMap.put("resultJson",JSON.toJSONString(result.getResult()));
                }
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }else if(BaiduAi.AiType.general_basic.name().toString().equals(aiType)){
            //通用文字识别
            GeneralBasicIOcrResult result= OcrService.general_basic(localPath);
            if(result.isRequestOk()){
                resultMap.put("result","成功");
                resultMap.put("words_result_num",result.getWords_result_num());
                resultMap.put("words",result.getWords());
                resultMap.put("paragraph",result.getParagraph());
            }else{
                resultMap.put("result","识别失败!"+result.getError_msg());
            }
        }
        return resultMap;
    }

    public Map<String, Object> uploadSingle(Map<String, Object> map, HttpServletRequest req, MultipartFile mFile) {

        String basePathType = GlobalConfig.getSysPara("uploadpath_type","0");
        String basepath = "";
        String relativePath = "";
        if ("1".equals(basePathType)) {//绝对路径类型
            basepath = GlobalConfig.getSysPara("uploadfile_basepath","D:/dzd");
            relativePath = "/uploadresources";
        } else {
            basepath = req.getSession().getServletContext().getRealPath("/");
            relativePath = GlobalConfig.getSysPara("uploadfile_basepath","/resources/static/cms/images");
        }
        relativePath= StringUtil.subEndStr(relativePath, "/");
        File dirFile=new File(basepath+relativePath);
        if(!dirFile.exists()){
            dirFile.mkdirs();
        }
        try {
            long size = 0;
            String path = "";
            String filename = "";
            String name = "";
            String dir = "";
            String suffixes ="";
            String guid= Guid.get();
            if (!mFile.isEmpty()) {
                BufferedInputStream in = new BufferedInputStream(mFile.getInputStream());
                filename = mFile.getOriginalFilename();
                name=filename.substring(0,filename.indexOf("."));
                // 取得文件后缀
                suffixes = filename.substring(filename.lastIndexOf("."), filename.length());
                path= relativePath+"/"+ DateUtil.formatDate(new Date(), "yyyyMMdd")+"/"+guid + suffixes;//相对路径
                dir = basepath+File.separatorChar+path;// 绝对路径
                String tempDir=dir.substring(0,dir.lastIndexOf("/"));
                File tempDirFile=new File(tempDir);
                if(!tempDirFile.exists()){
                    tempDirFile.mkdirs();
                }
                File ffout = new File(dir);
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(ffout));
                Streams.copy(in, out, true);
                size = ffout.length();
            }
            map.put("path", path);
            map.put("localPath", dir);
            map.put("name", name);
            map.put("suffixes", suffixes);
            map.put("size", size);
        }catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
