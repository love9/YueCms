package com.makbro.core.base.loginLog.web;


import com.makbro.core.base.loginLog.bean.LoginLog;
import com.makbro.core.base.loginLog.service.LoginLogService;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.annotation.DataScope;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 登录日志管理
 * @author wujiyue
 * @date 2018-11-03 00:11:20
 */

@Controller
@RequestMapping("/base/loginLog")
public class LoginLogController extends BaseController {
    @Autowired
    protected LoginLogService loginLogService;

    @RequestMapping(value={"","/","/list"})
    public String index(Model model){
        /*String fullPath=getFullUrl();
        String path=getServerContextPath();
        model.addAttribute("fullPath",fullPath);
        model.addAttribute("path",path);
        model.addAttribute("sys_ctx",request.getContextPath());*/
        return "/base/loginLog/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(LoginLog loginLog, Model model){
                return "/base/loginLog/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(LoginLog loginLog,Model model){
                if(loginLog!=null&&loginLog.getId()!=null){
            loginLog=loginLogService.get(loginLog.getId());
        }
         model.addAttribute("loginLog",loginLog);
         return "/base/loginLog/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return loginLogService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    //加注解@DataScope效果是：系统管理员可以查看所有记录，超级管理员可以查看本组织所有记录，其它人只能查看自己的记录
    @DataScope
    @RequiresAuthentication
    public Object find() {
        return  loginLogService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }



    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return loginLogService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除登录日志")
    public Object delete(@PathVariable Integer id) {

    	try{
            loginLogService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除登录日志")
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组

    	try{
            loginLogService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }

    }
}