package com.makbro.core.base.permission.dao;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import com.makbro.core.base.permission.bean.Permission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 菜单权限 dao
 * @author wujiyue
 */
@Repository
public interface PermissionMapper{
    public Permission get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Permission permission);
    public void addBatch(List<Permission> permissions);
    public void update(Permission permission);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);

    public List<Permission> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Map<String,Object>> findMap(PageBounds pageBounds, Map<String, Object> map);

	public List<Permission> findByParentid(PageBounds pageBounds, Map<String, Object> map);

    public List<Map<String,Object>> queryQxByYhid(String yhid);

    //组织管理员的菜单权限
    public List<Map<String,Object>> queryOrgAdminPermission(String orgid);

    public Integer getChildrenCount(@Param("ids") String ids);

    public String getParentidsById(@Param("id") String id);

    public int updateMenuSort(@Param("id") String id, @Param("sort") String sort);

    public Integer getMaxSortByParentid(@Param("parentid") String parentid);

    //给一个角色添加一条权限
    public void addRolePermission(@Param("qxid") String qxid, @Param("jsid") String jsid);
    //删除一个角色的所有权限
    public void deleteRolePermission(@Param("jsid") String jsid);
    //查询一个角色拥有的权限
    public List<String> findPermissionsByRole(@Param("jsid") String jsid);

    //更新菜单的上级
    public int updateMenuSjid(@Param("sjid") String sjid, @Param("id") String id);



    //根据guids查询所有权限
    public List<Map<String,Object>> findByGuids(@Param("guids") String guids);
}
