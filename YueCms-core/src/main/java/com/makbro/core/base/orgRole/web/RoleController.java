package com.makbro.core.base.orgRole.web;


import com.makbro.core.base.orgRole.bean.Role;
import com.makbro.core.base.orgRole.service.RoleService;
import com.makbro.core.base.permission.service.PermissionService;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Role管理
 * Created by wujiyue on 2016-06-12 22:35:18.
 */
@Controller
@RequestMapping("/org/role")
@ActionLog(description="角色管理")
@RequiresPermissions(value={"/org/role"})
public class RoleController extends BaseController {
    @Autowired
    protected RoleService roleService;
    @Autowired
    protected PermissionService permissionService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/base/role/list";
    }


    /**
     * 跳转到列表页面
     */
    /*@RequestMapping(value={"/list"})
    public String list(PageParam pageParam,Model model){
        Object roles=null;
        roles=roleService.find(getPageBounds(pageParam),MyBatisRequestUtil.getMap(request));
        model.addAttribute("roles",roles);
        model.addAttribute("pageParam",pageParam);
        return "/base/role/list";
    }*/
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        // List<PermissionVo> permissions= permissionService.findPermissionsListForShouquan(map);
        //model.addAttribute("permissions",permissions);
        return "/base/role/add";
    }
   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Model model){
        Map map= MyBatisRequestUtil.getMap(request);
        String id= (String) map.get("id");

        Role role=roleService.get(id);
       // List<PermissionVo> permissions= permissionService.findPermissionsListForShouquanEdit(id,map);
       // model.addAttribute("permissions",permissions);
        model.addAttribute("role",role);
         return "/base/role/edit";
    }

    //-----------json数据接口--------------------

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return roleService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
      /*  PageList list= (PageList) roleService.find(getPageBounds(),MyBatisRequestUtil.getMap(request)));
        Paginator paginator= list.getPaginator();
        resultMap.put("total",paginator.getTotalCount());
        resultMap.put("rows",list);*/
        return roleService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));

    }

    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
        Map map=MyBatisRequestUtil.getMap(request);
       return roleService.save(map);
    }
    @ResponseBody
    @RequestMapping(value="/json/saveRoleAndRolePermissions",method = RequestMethod.POST)
    public Object saveRoleAndRolePermissions() {
        Map map=MyBatisRequestUtil.getMap(request);
        return roleService.saveRoleAndRolePermissions(map);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            roleService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
    @ActionLog(description="删除角色")
	public Object removes(@PathVariable String[] ids){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            roleService.updateByMapBatch(map);
            return Msg.success("批量删除成功！");
        }catch (Exception e){
            return Msg.error("批量删除失败！");
        }
	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    public void delete(@PathVariable String id) {
        roleService.delete(id);
    }

    //编辑时初始化权限树
    @ResponseBody
    @RequestMapping("/json/initTreeForEdit")
    public Object initTreeForEdit() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return permissionService.staticTreeByRole(map);
    }

    @ResponseBody
    @RequestMapping("/json/zTreeStatic")
    public Object zTreeStatic() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return permissionService.zTreeStatic(map);
    }
}
