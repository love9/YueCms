package com.makbro.core.base.actionlog.bean;


import com.markbro.base.model.AliasModel;

/**
 * 操作日志 bean
 * Created by wujiyue on 2017-11-03 13:36:44 .
 */
public class Actionlog  implements AliasModel {

	private String orgid;//组织机构id
	private Integer id;//
	private String yhid;//请求人Id
	private String yhmc;//请求人
	private String uri;//请求的uri
	private Object params;//请求参数
	private String method;//方法
	private String description;//描述
	private Object result;//执行结果
	private String createTime;//请求时间
	private Integer actionTime;//消耗的时间
	private String type;//类型
	private String ip;//IP地址
	private Integer available;//
	private Integer deleted;//

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getId(){ return id ;}
	public void  setId(Integer id){this.id=id; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getYhmc(){ return yhmc ;}
	public void  setYhmc(String yhmc){this.yhmc=yhmc; }
	public String getUri(){ return uri ;}
	public void  setUri(String uri){this.uri=uri; }
	public Object getParams(){ return params ;}
	public void  setParams(Object params){this.params=params; }
	public String getMethod(){ return method ;}
	public void  setMethod(String method){this.method=method; }
	public String getDescription(){ return description ;}
	public void  setDescription(String description){this.description=description; }
	public Object getResult(){ return result ;}
	public void  setResult(Object result){this.result=result; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public Integer getActionTime(){ return actionTime ;}
	public void  setActionTime(Integer actionTime){this.actionTime=actionTime; }
	public String getType(){ return type ;}
	public void  setType(String type){this.type=type; }
	public Integer getAvailable(){ return available ;}
	public void  setAvailable(Integer available){this.available=available; }
	public Integer getDeleted(){ return deleted ;}
	public void  setDeleted(Integer deleted){this.deleted=deleted; }


	@Override
	public String toString() {
	return "Actionlog{" +
			"orgid=" + orgid+
			"id=" + id+
			", yhid=" + yhid+
			", yhmc=" + yhmc+
			", uri=" + uri+
			", params=" + params+
			", method=" + method+
			", description=" + description+
			", result=" + result+
			", createTime=" + createTime+
			", actionTime=" + actionTime+
			", type=" + type+
			", ip=" + ip +
			", available=" + available+
			", deleted=" + deleted+
			 '}';
	}
}
