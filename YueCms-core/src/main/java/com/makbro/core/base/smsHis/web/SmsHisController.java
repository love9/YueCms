package com.makbro.core.base.smsHis.web;


import com.makbro.core.base.smsHis.bean.SmsHis;
import com.makbro.core.base.smsHis.service.SmsHisService;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 短信发送记录管理
 * @author wujiyue
 * @date 2018-11-06 16:08:03
 */

@Controller
@RequestMapping("/base/smsHis")
public class SmsHisController extends BaseController {
    @Autowired
    protected SmsHisService smsHisService;

    @RequestMapping(value={"","/","/list"})
    public String index(){
        return "/base/smsHis/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(SmsHis smsHis, Model model){
                return "/base/smsHis/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(SmsHis smsHis,Model model){
                if(smsHis!=null&&smsHis.getId()!=null){
            smsHis=smsHisService.get(smsHis.getId());
        }
         model.addAttribute("smsHis",smsHis);
         return "/base/smsHis/edit";
    }
    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return smsHisService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return smsHisService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }



    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map= MyBatisRequestUtil.getMap(request);
           return smsHisService.save(map);
    }


    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description = "删除短信发送记录")
    public Object delete(@PathVariable Integer id) {
    	try{
            smsHisService.delete(id);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description = "批量删除短信发送记录")
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
    	try{
            smsHisService.deleteBatch(ids);
            return Msg.success("删除成功！");
         }catch (Exception e){
            return Msg.error("删除失败！");
         }
    }
}