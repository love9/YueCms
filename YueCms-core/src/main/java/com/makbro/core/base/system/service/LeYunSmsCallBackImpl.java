package com.makbro.core.base.system.service;

import com.google.common.collect.Maps;
import com.makbro.core.base.smsHis.service.SmsHisService;
import com.markbro.base.model.ICallBack;
import com.markbro.base.utils.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author wujiyue
 */
@Service("leYunSmsCallBack")
public class LeYunSmsCallBackImpl implements ICallBack {

    @Autowired
    SmsHisService smsHisService;
    Map params= Maps.newConcurrentMap();

    @Override
    public void onSuccess() {

        String returncode="1";
        params.put("returncode",returncode);

        String prefix=String.valueOf(params.get("prefix"));
        String content=String.valueOf(params.get("content"));
        if(StringUtil.notEmpty(prefix)){
            prefix="【"+prefix+"】";
            content=prefix+content;
            params.put("content",content);
        }

        smsHisService.save(params);
    }

    @Override
    public void onFail() {
        String returncode="0";
        params.put("returncode",returncode);
        smsHisService.save(params);
    }

    @Override
    public Map setParams(Map map) {
        params.clear();
        params.putAll(map);
        return params;
    }
}
