package com.makbro.core.base.dictionary.web;


import com.makbro.core.base.dictionary.bean.Dictionary;
import com.makbro.core.base.dictionary.service.DictionaryService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.annotation.ActionLog;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据字典管理
 * @author  wujiyue on 2016-07-05 22:19:55.
 */
@Controller
@RequestMapping("/base/dictionary")
public class DictionaryController extends BaseController {
    @Autowired
    protected DictionaryService dictionaryService;
    @RequestMapping(value={"","/"})
    public String index(){
        return "/base/dictionary/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(Dictionary dictionary, Model model){
        return "/base/dictionary/add";
    }


   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(Dictionary dictionary, Model model){
        if(dictionary!=null&&dictionary.getId()!=null){
            dictionary=dictionaryService.get(dictionary.getId());
        }
         model.addAttribute("dictionary",dictionary);
         return "/base/dictionary/edit";
    }

    //-----------json数据接口--------------------
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return dictionaryService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {

        return dictionaryService.find(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }
    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增数据字典")
    public void add(Dictionary m) {

        dictionaryService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(Dictionary m) {
        dictionaryService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存数据字典")
    public Object save(Dictionary m) {
        return  dictionaryService.save(m);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable Integer id){
	Msg msg=new Msg();
	try{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		map.put("id",id);
		dictionaryService.updateByMap(map);
		msg.setType(Msg.MsgType.success);
		msg.setContent("删除成功！");
	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("删除失败！");
	}
	return msg;
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable Integer[] ids){
	Msg msg=new Msg();
	try{
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("deleted",1);
		map.put("ids",ids);
		dictionaryService.updateByMapBatch(map);
		msg.setType(Msg.MsgType.success);
		msg.setContent("批量删除成功！");
	}catch (Exception e){
		msg.setType(Msg.MsgType.error);
		msg.setContent("批量删除失败！");
	}
	return msg;
	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除数据字典")
    public void delete(@PathVariable Integer id) {
        dictionaryService.delete(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除数据字典")
    public void deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
         dictionaryService.deleteBatch(ids);
    }

    @ResponseBody
    @RequestMapping(value = "/json/select")
    public Object select() {
        Map map=MyBatisRequestUtil.getMap(request);
        return dictionaryService.select(map);
    }
}
