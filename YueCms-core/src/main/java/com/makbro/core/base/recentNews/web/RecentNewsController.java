package com.makbro.core.base.recentNews.web;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.makbro.core.base.recentNews.bean.RecentNews;
import com.makbro.core.base.recentNews.service.RecentNewsService;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.markbro.base.annotation.DataScope;
import com.markbro.base.common.util.TmConstant;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 最新动态管理
 * @author  wujiyue on 2017-08-29 13:30:56.
 */

@Controller
@RequestMapping("/base/recentNews")
public class RecentNewsController extends BaseController {
    @Autowired
    protected RecentNewsService recentNewsService;

    @RequestMapping(value={"","/"})
    public String index(){
        return "/base/recentNews/list";
    }
    /**
     * 跳转到新增页面
     */
    @RequestMapping("/add")
    public String toAdd(RecentNews recentNews, Model model){
        return "/base/recentNews/add";
    }

   /**
    * 跳转到编辑页面
    */
    @RequestMapping(value = "/edit")
    public String toEdit(RecentNews recentNews, Model model){
        if(recentNews!=null&&recentNews.getId()!=null){
            recentNews=recentNewsService.get(recentNews.getId());
        }
         model.addAttribute("recentNews",recentNews);
         return "/base/recentNews/edit";
    }
    //-----------json数据接口--------------------
    
    
    
    

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable Integer id) {
        return recentNewsService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    @RequiresAuthentication
    @DataScope
    public Object find() {
        Map map= MyBatisRequestUtil.getMap(request);
        List<RecentNews> list=recentNewsService.find(getPageBounds(),map);
        return list;
    }

    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/findRecentNews")
    @RequiresAuthentication
    @DataScope
    public Object findRecentNews() {
        Map map= MyBatisRequestUtil.getMap(request);

        List<RecentNews> list=recentNewsService.find(getPageBounds(),map);
        PageList ls=(PageList)list;
        Paginator paginator= ls.getPaginator();
        Map resultMap=new HashMap();
        resultMap.put(TmConstant.PAGE_TOTAL_KEY,paginator.getTotalCount());
        resultMap.put(TmConstant.PAGE_TOTAL_PAGES_KEY,paginator.getTotalPages());
        resultMap.put(TmConstant.PAGE_PAGE_KEY, paginator.getPage());
        resultMap.put(TmConstant.PAGE_ROWS_KEY,list);
        List<RecentNews> listAll=recentNewsService.find(new PageBounds(),map);
        Long n=listAll.stream().filter(r->{return  (r.getReadflag().equals("0"));}).count();//未读信息数目
        resultMap.put("unread",n);
        return resultMap;
    }

    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    public void add(RecentNews m) {
        recentNewsService.add(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(RecentNews m) {
        recentNewsService.update(m);
    }


    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    public Object save() {
           Map map=MyBatisRequestUtil.getMap(request);
           return recentNewsService.save(map);
    }
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @RequiresAuthentication
    public Object delete(@PathVariable Integer id) {
    	Msg msg=new Msg();
    	try{
            recentNewsService.delete(id);
            msg.setType(Msg.MsgType.success);
            msg.setContent("删除成功！");
        }catch (Exception e){
        		msg.setType(Msg.MsgType.error);
        		msg.setContent("删除失败！");
        }
        return msg;
    }


    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @RequiresAuthentication
    public Object deletes(@PathVariable Integer[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
        Msg msg=new Msg();
    	try{
             recentNewsService.deleteBatch(ids);
             msg.setType(Msg.MsgType.success);
             msg.setContent("删除成功！");
         }catch (Exception e){
         	 msg.setType(Msg.MsgType.error);
         	 msg.setContent("删除失败！");
         }
         return msg;
    }

    @ResponseBody
    @RequestMapping(value="/json/readMsg")
    @RequiresAuthentication
    public Object readMsg() {
        Map map=MyBatisRequestUtil.getMap(request);
        return recentNewsService.readMsg(map);
    }
}