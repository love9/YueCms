package com.makbro.core.base.attachment.dao;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.makbro.core.base.attachment.bean.Attachment;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 附件 dao
 * Created by wujiyue on 2017-09-19 16:45:30.
 */
@Repository
public interface AttachmentMapper{

    public Attachment get(String id);
    public Map<String,Object> getMap(String id);
    public void add(Attachment attachment);
    public void addByMap(Map<String, Object> map);
    public void addBatch(List<Attachment> attachments);
    public void update(Attachment attachment);
    public void updateByMap(Map<String, Object> map);
    public void updateByMapBatch(Map<String, Object> map);
    public void delete(String id);
    public void deleteBatch(String[] ids);
    //find与findByMap的唯一的区别是在find方法在where条件中多了未删除的条件（deleted=0）
    public List<Attachment> find(PageBounds pageBounds, Map<String, Object> map);
    public List<Attachment> findByMap(PageBounds pageBounds, Map<String, Object> map);



}
