package com.makbro.core.base.orgTree.web;


import com.makbro.core.base.orgTree.bean.OrgTree;
import com.makbro.core.base.orgTree.service.OrgTreeService;
import com.makbro.core.framework.authz.annotation.Logical;
import com.makbro.core.framework.authz.annotation.RequiresAuthentication;
import com.makbro.core.framework.authz.annotation.RequiresPermissions;
import com.markbro.base.annotation.ActionLog;
import com.makbro.core.framework.BaseController;
import com.makbro.core.framework.MyBatisRequestUtil;
import com.markbro.base.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 组织目录管理
 * Created by wujiyue on 2016-07-14 12:01:01.
 */
@Controller
@RequestMapping("/org/tree")
@RequiresPermissions(value={"/org/tree","orgTreeManage"},logical= Logical.OR)
public class OrgTreeController extends BaseController {
    @Autowired
    protected OrgTreeService treeService;
    @RequestMapping(value={"","/","/list"})
    public String index(){

        return "/base/orgTree/list";
    }


    //-----------json数据接口--------------------
    
	@ResponseBody
	@RequestMapping("/json/findByOrgid/{orgid}")
	public Object findByOrgid(@PathVariable String orgid) {
		return treeService.findByOrgid(getPageBounds(),orgid);
	}
	@ResponseBody
	@RequestMapping("/json/findByParentid/{parentid}")
	public Object findByParentid(@PathVariable String parentid) {
		return treeService.findByParentid(getPageBounds(), parentid);
	}

    /**
     * 根据主键获得数据
     */
    @ResponseBody
    @RequestMapping(value = "/json/get/{id}")
    public Object get(@PathVariable String id) {
        return treeService.get(id);
    }
    /**
     * 获得分页json数据
     */
    @ResponseBody
    @RequestMapping("/json/find")
    public Object find() {
        return treeService.find(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }
    @ResponseBody
    @RequestMapping(value="/json/add",method = RequestMethod.POST)
    @ActionLog(description="新增组织目录")
    public void add(OrgTree m) {
        treeService.add(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/update",method = RequestMethod.POST)
    public void update(OrgTree m) {
        treeService.update(m);
    }
    @ResponseBody
    @RequestMapping(value="/json/save",method = RequestMethod.POST)
    @ActionLog(description="保存组织目录")
    public Object save(OrgTree m) {
        return  treeService.save(m);
    }
    /**
	* 逻辑删除的数据（deleted=1）
	*/
	@ResponseBody
	@RequestMapping("/json/remove/{id}")
	public Object remove(@PathVariable String id){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("id",id);
            treeService.updateByMap(map);
            return Msg.success("删除成功！");
        }catch (Exception e){
            return Msg.error("删除失败！");
        }
	}
    /**
	* 批量逻辑删除的数据
	*/
	@ResponseBody
	@RequestMapping("/json/removes/{ids}")
	public Object removes(@PathVariable String[] ids){
        try{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("deleted",1);
            map.put("ids",ids);
            treeService.updateByMapBatch(map);
            return Msg.success("批量删除成功！");
        }catch (Exception e){
            return Msg.error("批量删除失败！");
        }
	}
    @ResponseBody
    @RequestMapping(value = "/json/delete/{id}", method = RequestMethod.POST)
    @ActionLog(description="物理删除组织目录")
    public void delete(@PathVariable String id) {
        treeService.delete(id);
    }
    @ResponseBody
    @RequestMapping(value = "/json/deletes/{ids}", method = RequestMethod.POST)
    @ActionLog(description="批量物理删除组织目录")
    public void deletes(@PathVariable String[] ids) {//前端传送一个用逗号隔开的id字符串，后端用数组接收，springMVC就可以完成自动转换成数组
         treeService.deleteBatch(ids);
    }

    //zTree简单数据格式的 动态树
    @ResponseBody
    @RequestMapping("/json/ztree")
    @RequiresAuthentication
    public Object tree() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return treeService.ztree(map);
    }
    //zTree简单数据格式的 静态树获取所有数据
    @ResponseBody
    @RequestMapping("/json/zTreeStatic")
    @RequiresAuthentication
    public Object zTreeStatic() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        List<Map<String, Object>> list=treeService.zTreeStatic(map);
        return list;
    }


    //获取ztree树的根节点数据 （这一步本可以省去，但是前台找不到好的解决办法获得根节点数据）
    @ResponseBody
    @RequestMapping("/json/getZtreeRoot")
    @RequiresAuthentication
    public Object getZtreeRoot() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return treeService.getZtreeRoot(map);
    }

    @ResponseBody
    @RequestMapping("/json/findGwListByBmid")
    public Object findGwListByBmid() {
        Map<String, Object> map = MyBatisRequestUtil.getMap(request);
        return treeService.findGwListByBmid(getPageBounds(),map);

    }
    @ResponseBody
    @RequestMapping("/json/findBmListByBmid")
    public Object findBmListByBmid() {
        return treeService.findBmListByBmid(getPageBounds(), MyBatisRequestUtil.getMap(request));
    }
    @ResponseBody
    @RequestMapping("/json/findRyListByGwid")
    public Object findRyListByGwid() {
        return treeService.findRyListByGwid(getPageBounds(),MyBatisRequestUtil.getMap(request));
    }
}
