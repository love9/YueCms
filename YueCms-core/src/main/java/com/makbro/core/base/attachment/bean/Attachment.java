package com.makbro.core.base.attachment.bean;


import com.markbro.base.model.AliasModel;

/**
 * 附件 bean
 * @author wujiyue on 2017-09-19 16:45:29 .
 */
public class Attachment  implements AliasModel {

	private String id;//id
	private String zid;//附件组唯一ID
	private String yhid;//
	private String module;//所属模块
	private String filename;//文件名称
	private String filepath;//文件路径
	private Integer size;//文件空间
	private String createBy;//用户ID
	private String createTime;//
	private String suffixes;//后缀


	public String getId(){ return id ;}
	public void  setId(String id){this.id=id; }
	public String getZid(){ return zid ;}
	public void  setZid(String zid){this.zid=zid; }
	public String getYhid(){ return yhid ;}
	public void  setYhid(String yhid){this.yhid=yhid; }
	public String getModule(){ return module ;}
	public void  setModule(String module){this.module=module; }
	public String getFilename(){ return filename ;}
	public void  setFilename(String filename){this.filename=filename; }
	public String getFilepath(){ return filepath ;}
	public void  setFilepath(String filepath){this.filepath=filepath; }
	public Integer getSize(){ return size ;}
	public void  setSize(Integer size){this.size=size; }
	public String getCreateBy(){ return createBy ;}
	public void  setCreateBy(String createBy){this.createBy=createBy; }
	public String getCreateTime(){ return createTime ;}
	public void  setCreateTime(String createTime){this.createTime=createTime; }
	public String getSuffixes(){ return suffixes ;}
	public void  setSuffixes(String suffixes){this.suffixes=suffixes; }


	@Override
	public String toString() {
	return "Attachment{" +
			"id=" + id+
			", zid=" + zid+
			", yhid=" + yhid+
			", module=" + module+
			", filename=" + filename+
			", filepath=" + filepath+
			", size=" + size+
			", createBy=" + createBy+
			", createTime=" + createTime+
			", suffixes=" + suffixes+
			 '}';
	}
}
