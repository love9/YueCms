package com.markbro.thirdapi.baidu;

import com.alibaba.fastjson.JSON;
import com.markbro.thirdapi.baidu.util.Base64Util;
import com.markbro.thirdapi.baidu.util.FileUtil;
import com.markbro.thirdapi.baidu.util.HttpUtil;


import java.util.HashMap;
import java.util.Map;


/**
 * 仅参考。具体文档自行查看官网说明  ai.baidu.com
 * @author 小帅丶
 * @类名称  FaceDetectTest
 * @remark 
 * @date  2017-9-12
 */
public class FaceDetectTest {
	/**
	 * SDK人脸检测方式
	 * @param args
	 * @throws java.io.IOException
	 */
	/*public static void main2(String[] args) throws IOException {
		//初始化一个FaceClient
		AipFace face = new AipFace("你自己的APPID", "你自己的APIKEY", "你自己的SERCETKEY");
		//可选：设置网络连接参数
		face.setConnectionTimeoutInMillis(60000);
		face.setSocketTimeoutInMillis(60000);
		//调用API
		HashMap map = new HashMap();
		//
		map.put("face_fields", "age,beauty,expression,gender,glasses,race,qualities");
		String path = "图片本地目录路径";
		AipRequest aipRequest = new AipRequest();
		aipRequest.setBody(map);
		JSONObject result = face.detect(FileUtil.readFileByBytes(path),map);
		System.out.println(result.toString(2));
	}*/
	/**
	 * API调用人脸检测方式
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String access_token="24.2299b3329131e80552ebee19e0f3b37d.2592000.1531402175.282335-11389225";
		/****************/
		String Filepath = "C:\\Users\\Administrator\\Desktop\\11.jpg";
		String image = Base64Util.encode(FileUtil.readFileByBytes(Filepath));
		String url = "https://aip.baidubce.com/rest/2.0/face/v3/detect";
		Map<String, Object> map = new HashMap<>();
		String url1="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1528821260802&di=c3fa7991da1e87524587327dcd402449&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2Fb7003af33a87e9508dc95d791b385343faf2b4cd.jpg";
		String url2="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1528821260804&di=cd76d75310e9c0050dd0e601c85f0114&imgtype=0&src=http%3A%2F%2Fold.bz55.com%2Fuploads%2Fallimg%2F150210%2F139-150210134411-50.jpg";
		map.put("image", url1);
		map.put("face_field", "age,beauty,expression,gender,glasses,race,qualities");
		map.put("image_type", "URL");

		String params = JSON.toJSONString(map);
		//String params =  URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(image, "UTF-8")+"&face_fields=age,beauty,expression,gender,glasses,race,qualities";
		try {
			HttpUtil httpUtil = new HttpUtil();
			String result = httpUtil.post(url, access_token, params);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
}
}
