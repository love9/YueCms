package com.markbro.thirdapi.baidu.baiduSite;

import com.markbro.base.exception.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Date;

/**
 * 百度站长推送工具类
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @website https://www.zhyd.me
 * @date 2018/4/16 16:26
 * @since 1.0
 */
@Slf4j
public class BaiduPushUtil extends RestClientUtil {
    /**
     * 自行登录百度站长平台后获取响应的cookie
     */
    private static final String COOKIE = "BAIDUID=62D013F30EB249376A908455253788EC:SL=0:NR=10:FG=1;BDORZ=B490B5EBF6F3CD402E515D22BCDA1598;BDRCVFR[feWj1Vr5u3D]=I67x6TjHwwYf0;BDSFRCVID=hF8sJeCCxG3DPBTwX9BcONhojqfpzuq1UpSA3J;BDSVRTM=168;BDUSS=NVM2NkclV0bm9POFdYcnZEbTJTeW1lbDJkQzZGNzBvdXd6czdlTUZPd3VWRVpkSUFBQUFBJCQAAAAAAAAAAAEAAACK-YEFd3VkZXllcXUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC7HHl0uxx5dTm;BD_CK_SAM=1;BD_UPN=1a314753;BIDUPSID=96390680F5770D4A1B71FF951A34704B;COOKIE_SESSION=8779_5_6_0_22_15_0_0_0_8_2_0_0_0_182_4_1562298466_1562298288_1562307063%7C9%23216_6_1562298284%7C3;HMACCOUNT=B7A4372AE571B2E6;HMVT=24f17767262929947cc3631f99bfd274|1562308372|;H_BDCLCKID_SF=JJkO_D_atKvjDbTnMITHh-F-5fIX5-RLf2uJoPOF5lOTJh0RQh3I3b-RbG8OWb5UWm7Q0tJLb4DaStJbLjbke6jBjaLJJTK8f5vfL5r22Rj_JJjk-P7Hq4tehHRLJM5eWDTm5-nTthb0eRc6WMP5yUuE5R3WXTOraR7koq7oLh45OCF4j68aj5OBeHRf-b-XbI5KWtTqaJOsHjrnhPF32x-0KP6-3MJO3b7RXJQ_bMncER6mWxQKXhcQXMFeX6j92a7rohFLtC8WhD-xj6RV5-TH-xQ0KnLXKKOLVhka2h7keq8CDxJZqq4I5lJv-RO7We5X-q5HalQMOfb2y5jHhpFnLPJ4alRJWjnEbhPM2UQpsIJMM4DWbT8U5ecgJDuqaKvia-TEBMb1qlbDBT5h2M4qMxtOLR3pWDTm_q5TtUt5OCFlDTAaj5OBeUcJaD6aKC5bL6rJabC3KqnGXU6qLT5XQnuLyMrqbabt0Mc2LhcUfIozQROz3h0njxQyXxvm32T0M-bFbPc6OKJd3xonDh8qbG7MJUntHGAHsCQO5hvvJJ6O3M7KQMKmDloOW-TB5bbPLUQF5l8-sq0x05bke65WeaDHt6tsKKJ03bk8KRREJt5kq4bohjPJQG7eBtQm05bxohbx0fbboKTt-UnFM-4qKfjjhJQDtGTjbhDbWDcjqR8Zj5_aj55P;H_PS_645EC=2f59Kpm0wdZe5P7qsV907l8EWzcLoBbLMOeBdtetfHHy0In2vaLJwwfXqAo9l2va0zxO;H_PS_PSSID=1437_21107_18559_29135_29238_28518_29099_28834_29221_26350_29071_20719;Hm_lpvt_6f6d5bc386878a651cb8c9e1b4a3379a=1562308758;Hm_lvt_6f6d5bc386878a651cb8c9e1b4a3379a=1562298312,1562298489,1562298634;ORIGIN=0;PSINO=2;PSTM=1558946758;SITEMAPSESSID=lbed54ec882rms2qjtgo9mi464;__cas__id__=0;__cas__st__=NLI;bdime=0;delPer=0;ispeed_lsm=2;lastIdentity=PassUserIdentity;sug=3;sugstore=0;";

    /**
     * 推送链接到百度站长平台
     *
     * @param urlString
     *         百度站长平台地址
     * @param params
     *         待推送的链接
     * @return api接口响应内容
     */
    public static String doPush(String urlString, String params) {
        if (StringUtils.isEmpty(COOKIE)) {
            throw new ApplicationException("尚未设置百度站长平台的Cookie信息，该功能不可用！");
        }
        log.info("{} REST url: {}", new Date(), urlString);
        HttpURLConnection connection = null;
        try {
            connection = openConnection(urlString);
            connection.setRequestMethod("POST");
            // (如果不设此项,json数据 ,当WEB服务默认的不是这种类型时可能抛java.io.EOFException)
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            connection.setRequestProperty("Action", "1000");
            connection.setRequestProperty("User-Agent", USER_AGENT);
            connection.setRequestProperty("Connection", "keep-alive");
            connection.setRequestProperty("Cookie", COOKIE);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            // 设置连接超时时间，单位毫秒
            connection.setConnectTimeout(10000);
            // 设置读取数据超时时间，单位毫秒
            connection.setReadTimeout(10000);
            // Post 请求不能使用缓存
            connection.setUseCaches(false);
            if (params != null) {
                final OutputStream outputStream = connection.getOutputStream();
                writeOutput(outputStream, params);
                outputStream.close();
            }
            log.info("RestClientUtil response: {} : {}", connection.getResponseCode(), connection.getResponseMessage());
            if (connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                return readInput(connection.getInputStream(), "UTF-8");
            } else {
                return readInput(connection.getErrorStream(), "UTF-8");
            }
        } catch (Exception e) {
            log.error("推送到百度站长平台发生异常！", e);
            return "";
        } finally {
            if (null != connection) {
                connection.disconnect();
            }
        }
    }
}
