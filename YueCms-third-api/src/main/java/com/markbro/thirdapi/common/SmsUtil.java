package com.markbro.thirdapi.common;


import com.markbro.base.model.ICallBack;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 发送短信工具类
 * @author wujiyue
 */
public class SmsUtil {
    protected static Logger logger = LoggerFactory.getLogger(SmsUtil.class);

    private static String leYunAccount= GlobalConfig.getConfig("sms.LeYun.account");

    private static String leYunPassword= GlobalConfig.getConfig("sms.LeYun.password");

    /**
     * 乐云短信
     * @param phone     发送的号码
     * @param content   发送短信内容
     * @param prefix    内容前缀该字符串会被“【】”包裹并添加到content最前面
     * @return
     */
    public static Integer sendLeYunSms(String phone,String prefix, String content) {
        try {
            long start = System.currentTimeMillis();
            if(StringUtil.notEmpty(prefix)){
                prefix="【"+prefix+"】";
                content=prefix+content;
            }
            content = java.net.URLEncoder.encode(content, "UTF-8");// 用UTF-8编码执行URLEncode
            String _url = "http://www.lehuo520.cn/a/sms/api/send"; //接口地址
            String _param = "username="+leYunAccount+"&password="+leYunPassword + "&phone=" + phone+ "&content=" + content+ "&type=1";
            URL url = null;
            HttpURLConnection urlConn = null;
            url = new URL(_url);
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestMethod("POST");
            urlConn.setDoOutput(true);           OutputStream out = urlConn.getOutputStream();
            out.write(_param.getBytes("UTF-8"));
            out.flush();
            out.close();
            BufferedReader rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "UTF-8"));
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = rd.read()) > -1) {
                sb.append((char) ch);
            }
            System.out.println(sb);
            rd.close();
            long end = System.currentTimeMillis();
            logger.info("sendLeYunSms发送 短信 耗时：" + (end - start));
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
        return 1;
    }
    /**
     * 乐云短信
     * @param phone     发送的号码
     * @param content   发送短信内容
     * @param callBack  回调函数，传入该接口的一个实现类并定义成功和失败后执行的方法
     * @return
     */
    public static Integer sendLeYunSms(String phone, String prefix, String content, ICallBack callBack){
        int res=sendLeYunSms(phone,prefix, content);
        if(res==1){
            callBack.onSuccess();
        }else{
            callBack.onFail();
        }
        return res;
    }


}
