package com.markbro.thirdapi.kdniao;

/**
 * 快递鸟支持的快递公司（有很多，这里只列几个常用的）
 */
public enum KdCompany {
    ZTO("中通快递"),
    YTO("圆通速递"),
    YD("韵达速递"),
    EMS("EMS"),
    JD("京东快递"),
    UC("优速快递"),
    DBL("德邦快递"),
    ZJS("宅急送"),
    TNT("TNT快递"),
    UPS("UPS"),
    DHL("DHL");
    private String desc;
    private KdCompany(String desc) {
        this.desc = desc;
    }
    public String getDesc(){
        return desc;
    }
}
