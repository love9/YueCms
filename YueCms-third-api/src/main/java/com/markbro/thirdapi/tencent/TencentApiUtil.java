package com.markbro.thirdapi.tencent;

import com.alibaba.fastjson.JSONObject;
import com.markbro.base.utils.GlobalConfig;
import com.markbro.base.utils.string.StringUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class TencentApiUtil {

    private static final String MY_IP_KEY= GlobalConfig.getConfig("tencent.lbs.ip_key");
    private static String URL_IP="https://apis.map.qq.com/ws/location/v1/ip?ip=IP&key=KEY";
    public static String queryIpLocation(String ip){
        String res="";
        String url=URL_IP.replace("IP",ip).replace("KEY",MY_IP_KEY);
        String data=  sendRequest(url, RequestType.GET);
        //System.out.println(data);
        JSONObject jsonObject=JSONObject.parseObject(data);
        String status=String.valueOf(jsonObject.get("status"));
        if("0".equals(status)){
            //查询成功
            JSONObject ad_infoObj=jsonObject.getJSONObject("result").getJSONObject("ad_info");
            String nation=String.valueOf(ad_infoObj.get("nation"));//国家
            String province=String.valueOf(ad_infoObj.get("province"));//省
            String city=String.valueOf(ad_infoObj.get("city"));//市
            String district=String.valueOf(ad_infoObj.get("district"));//区
            if("中国".equals(nation)){
                res+= (StringUtil.notEmpty(province)?province.replace("省",""):"")+(StringUtil.notEmpty(city)?"-"+city.replaceAll("市",""):"")+(StringUtil.notEmpty(district)?"-"+district:"");
            }else{
                res+= (StringUtil.notEmpty(nation)?nation:"")+(StringUtil.notEmpty(province)?"-"+province:"")+(StringUtil.notEmpty(city)?"-"+city:"")+(StringUtil.notEmpty(district)?"-"+district:"");
            }
            return res;
        }else{
            //失败的情况
            return res;
        }

    }
    public static void main(String[] args){
       String res= queryIpLocation("112.232.19.111");
        System.out.println(res);
    }
    public enum RequestType{
        POST,
        GET
    }
    private static String sendRequest(String requestUrl,RequestType type){
        try {
            URL url = new URL(requestUrl);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(type.name());
            // 设置通用的请求属性
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setDoInput(true);

            // 得到请求的输出流对象
            //DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            //out.writeBytes(params);
            //out.flush();
            //out.close();

            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> headers = connection.getHeaderFields();
            // 遍历所有的响应头字段
           // for (String key : headers.keySet()) {
                //System.out.println(key + "--->" + headers.get(key));
            //}
            // 定义 BufferedReader输入流来读取URL的响应
            BufferedReader in = null;

            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

            String result = "";
            String getLine;
            while ((getLine = in.readLine()) != null) {
                result += getLine;
            }
            in.close();
            // System.out.println("result:" + result);
            return result;
        }catch (Exception ex){
            ex.printStackTrace();
            return "";
        }
    }
}
