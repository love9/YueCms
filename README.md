# YueCms

#### 介绍
YueCms是一个通用的后台管理系统+我的博客前台。
系统是springboot+mybatis架构从零开始写的，麻雀虽小，五脏俱全。本后台系统可以实现的功能有：
 **系统模块：** 一键代码生成、真正的轻量级权限控制(使用SpringMVC的拦截器实现)、自己设计的会话管理，在线用户列表，踢出用户（登录时生成的token存放到t_sesion表中实现）、简单的数据范围授权、读写连接池分离、统一异常处理、防止表单重复提交、日志管理、区域管理、省市区选择、通用文件上传管理、字典管理、组织目录管理、角色管理、系统用户管理、系统参数管理、EhCache缓存、动态配置任务调度、系统内部事件机制
 **内容管理模块：** 文章管理(博客系统)、书籍管理(博客系统)、书籍章节(博客系统)、评论管理(博客系统)、标签管理(博客系统)、资源分类管理、七牛文件上传管理、模板管理、站内邮件、时间轴等。 
 **其他功能：** 邮件发送、webmagic爬虫、简单的审核流程、定时任务、封装百度人工智能API 等等。
 **补充：** 以上列出的并不是Java后台实现的全部功能,我本人能力和时间有限，有很多功能已经融合进去但并未完整实现使用，还有很多地方并不完美需要调整，就没有列出来。

#### 软件架构
软件架构说明
#### 软件环境


1. Java8+
1. SpringBoot2.1.1
1. Spring5.1.3
1. Marven3.2.5
1. IntelliJ IDEA 2018


#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)