package com.markbro.sso.core.store;

import com.markbro.sso.core.user.SsoUser;

/**
 * @author wujiyue
 */
public interface ISsoSessionIdHelper {
    public String makeSessionId(SsoUser ssoUser);
    public String parseStoreKey(String sessionId);
    public String parseVersion(String sessionId);
}
