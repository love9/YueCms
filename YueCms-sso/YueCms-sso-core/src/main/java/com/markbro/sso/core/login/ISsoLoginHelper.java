package com.markbro.sso.core.login;



import com.markbro.sso.core.user.SsoUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wujiyue
 */
public interface ISsoLoginHelper {

    void login(HttpServletRequest request, HttpServletResponse response, String sessionId, SsoUser ssoUser, boolean ifRemember);
    void logout(HttpServletRequest request, HttpServletResponse response);
    SsoUser loginCheck(HttpServletRequest request, HttpServletResponse response);
    boolean invalidSession(String sessionId);
    boolean isInvalidSession(String sessionId);//判断sessionId是否失效
    String getSessionId(HttpServletRequest request);
}
