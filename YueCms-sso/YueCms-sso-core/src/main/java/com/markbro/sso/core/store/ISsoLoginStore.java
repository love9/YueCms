package com.markbro.sso.core.store;

import com.markbro.sso.core.user.SsoUser;

/**
 * @author wujiyue
 */
public interface ISsoLoginStore {

    int getExpireMinite();
    SsoUser get(String storeKey);
    void remove(String storeKey);
    void put(String storeKey, SsoUser ssoUser);

    void putCache(String key, Object v);
    Object getCache(String key);
}
