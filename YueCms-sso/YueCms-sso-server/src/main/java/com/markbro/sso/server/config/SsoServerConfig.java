package com.markbro.sso.server.config;


import com.markbro.sso.core.config.SsoBaseConfig;
import com.markbro.sso.core.store.*;
import com.markbro.sso.server.service.DemoSsoUserServiceImpl;
import com.markbro.sso.server.service.SsoUserService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "config/sso.properties")
public class SsoServerConfig extends SsoBaseConfig {

    /*******配置 SsoUserService********/
    @ConditionalOnMissingBean(SsoUserService .class)
    @Bean("ssoUserService")
    public SsoUserService getSsoUserService(){
        log.info("4.>>>>>>>>>>>ssoUserService is missing， return new DemoSsoUserServiceImpl()!");
        return new DemoSsoUserServiceImpl();
    }

}
